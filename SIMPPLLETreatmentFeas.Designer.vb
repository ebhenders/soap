<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SIMPPLLETreatmentFeas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Button4 = New System.Windows.Forms.Button
        Me.tbTreatmentFileList = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.CompileButton = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(109, 37)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(405, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Looks at SIMPPLLE output files to see whether treatments were successfully applie" & _
            "d"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(161, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(306, 24)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "SIMPPLLE Treatment Feasibility"
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(758, 84)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 19)
        Me.Button4.TabIndex = 24
        Me.Button4.Text = "Browse"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'tbTreatmentFileList
        '
        Me.tbTreatmentFileList.Location = New System.Drawing.Point(143, 83)
        Me.tbTreatmentFileList.Name = "tbTreatmentFileList"
        Me.tbTreatmentFileList.Size = New System.Drawing.Size(596, 20)
        Me.tbTreatmentFileList.TabIndex = 22
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(47, 83)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(90, 31)
        Me.Label4.TabIndex = 23
        Me.Label4.Text = "Treatment and LS file lists"
        '
        'CompileButton
        '
        Me.CompileButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CompileButton.Location = New System.Drawing.Point(388, 169)
        Me.CompileButton.Name = "CompileButton"
        Me.CompileButton.Size = New System.Drawing.Size(96, 32)
        Me.CompileButton.TabIndex = 25
        Me.CompileButton.Text = "GO"
        '
        'SIMPPLLETreatmentFeas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(910, 249)
        Me.Controls.Add(Me.CompileButton)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.tbTreatmentFileList)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "SIMPPLLETreatmentFeas"
        Me.Text = "SIMPPLLE Treatment Feasibility"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents tbTreatmentFileList As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents CompileButton As System.Windows.Forms.Button
End Class
