Public Class clSearchForThreshVals
    'takes an input dataset with a column of values, a goal input file, and searches for a value threshhold that meets the goals
    Dim ValColName As String
    'Dim ValColInd As Integer
    Dim AreaColName As String
    Dim PolyIDColName As String

    Dim NIters As Integer
    Dim NGoals As Integer
    Dim NColsGoal() As Integer 'by NGoals - counts the number of columns used to define the goal
    Dim MaxColsGoalDef As Integer 'maximum number of columns used in a goal definition
    Dim GoalCols(,) As String 'by NGoals, MaxColsGoalDef
    Dim GoalColValue(,) As String 'by NGoals, MaxColsGoalDef - stores the value in the column used to define the goal
    Dim GoalMin() As Single 'minimum value of the goal
    Dim GoalMax() As Single 'maximum value of the goal
    Dim GoalOutputVal() As String 'output value to associate with the polygon if it meets the criteria
    Dim GoalThreshVal(,) As Single 'the threshold 
    Dim NOutputVals As Integer
    Dim OutputVal() As String 'by NOutputVals - the value of the unique output. 

    Dim GoalTotal(,) As Single 'sum of the area value for the goal - by iteration, goal
    Dim GoalDeviations(,) As Single

    'when we read the dataset into memory, perhaps we only want to store the column values that are used to define the goals - along with the area column and the value column
    Dim NRecords As Integer
    Dim NColsToStore As Integer
    Dim ColName() As String 'name of the stored column - by NColsToStore
    Dim ColInd() As Integer 'column index of the column with relevant values
    Dim PolyID() As String 'by NRecords
    'Dim ColValue(,) As String 'by NRecords, NColsToStore

    Dim GoalMatch(,) As Byte 'by nrecords, ngoals - indicates whether the record can contribute to the goal or not

    Dim AreaVal() As Single 'by NRecords
    Dim ValueVal() As Single 'by NRecords

    'for iterating
    ' Dim CurrGoalVal() As Single 'by NGoals - the current threshold value
    Dim AdjFactor() As Single 'by NGoals - magnitude of the adjustment - starts at 1, doubles if you are outside of the goal, halves if you over-shoot or are within
    Dim LastDevType() As String 'by NGoals

    Dim PolyOutputs(,) 'by NRecords, NOutputvals)

    Public Sub RunThreshVals(ByVal goalfile As String, ByVal datafile As String, ByVal niterations As Integer)
        NIters = niterations

        ReadGoalInput(goalfile)
        ReadDataFile(datafile)
        ReDim GoalTotal(NIters, NGoals), GoalDeviations(NIters, NGoals)
        ReDim AdjFactor(NGoals), LastDevType(NGoals)
        For jgoal As Integer = 1 To NGoals
            AdjFactor(jgoal) = 1
            LastDevType(jgoal) = "below"
        Next
        Iterate(NIters)
        WriteGoalOutputs(DirName(goalfile) & "GoalOutputs.txt", DirName(goalfile) & FName(goalfile) & "_iter.txt")
        WritePolyOutputs(DirName(datafile) & FName(datafile) & "_out.txt")


    End Sub
    Private Sub Iterate(ByVal NIters As Integer)
        'Steps:
        '1. Tally the acres that are above the threshhold
        '2. Test to see whether they meet the goal
        '3. If they don't, adjust the threshhold value up or down accordingly


        '1. Tally the acres that are above the threshhold
        For jiter As Integer = 1 To NIters
            AreaTally(jiter)
            Deviations(jiter)
            If jiter < NIters Then Adjust(jiter)
        Next jiter

    End Sub

    Private Sub AreaTally(ByVal kiter As Integer)
        If kiter = NIters Then ReDim PolyOutputs(NRecords, NOutputVals)

        Dim kvalue As Integer
        'populating the GoalTotal
        Dim polythresh() As Single 'the threshhold value for the polygon

        For jpoly As Integer = 1 To NRecords
            ReDim polythresh(NOutputVals)
            For jgoal As Integer = 1 To NGoals
                'if it does match, then increase the polythresh...
                If GoalMatch(jpoly, jgoal) Then
                    kvalue = GetStrIndex(OutputVal, GoalOutputVal(jgoal))
                    polythresh(kvalue) = polythresh(kvalue) + GoalThreshVal(kiter, jgoal)
                End If
            Next
            For jgoal As Integer = 1 To NGoals
                If GoalMatch(jpoly, jgoal) Then
                    kvalue = GetStrIndex(OutputVal, GoalOutputVal(jgoal))
                    If ValueVal(jpoly) >= polythresh(kvalue) Then
                        If kiter = NIters Then PolyOutputs(jpoly, kvalue) = kvalue
                        GoalTotal(kiter, jgoal) = GoalTotal(kiter, jgoal) + AreaVal(jpoly)
                    End If
                End If
            Next jgoal
        Next
    End Sub

    Private Sub Deviations(ByVal kiter As Integer)
        'tallies up area above or below desired range
        For jgoal As Integer = 1 To NGoals
            GoalDeviations(kiter, jgoal) = 0 'default
            If GoalTotal(kiter, jgoal) < GoalMin(jgoal) Then
                GoalDeviations(kiter, jgoal) = GoalTotal(kiter, jgoal) - GoalMin(jgoal) 'negative value
            ElseIf GoalTotal(kiter, jgoal) > GoalMax(jgoal) Then
                GoalDeviations(kiter, jgoal) = GoalTotal(kiter, jgoal) - GoalMax(jgoal) 'positive value
            End If
        Next
    End Sub

    Private Sub Adjust(ByVal kiter As Integer)
        'figures out what the new goalthreshhold should be
        For jgoal As Integer = 1 To NGoals
            If GoalDeviations(kiter, jgoal) > 0 Then 'above
                If LastDevType(jgoal) <> "above" Then
                    AdjFactor(jgoal) = AdjFactor(jgoal) / 2 'be less aggressive
                Else
                    AdjFactor(jgoal) = AdjFactor(jgoal) * 1.1
                End If
                GoalThreshVal(kiter + 1, jgoal) = GoalThreshVal(kiter, jgoal) + AdjFactor(jgoal) 'have to raise it to lower the total
                LastDevType(jgoal) = "above"
            ElseIf GoalDeviations(kiter, jgoal) < 0 Then
                If LastDevType(jgoal) <> "below" Then
                    AdjFactor(jgoal) = AdjFactor(jgoal) / 2
                Else
                    AdjFactor(jgoal) = AdjFactor(jgoal) * 1.1
                End If
                GoalThreshVal(kiter + 1, jgoal) = Math.Max(GoalThreshVal(kiter, jgoal) - AdjFactor(jgoal), -50) 'have to lower the tolerance to raise the total
                LastDevType(jgoal) = "below"
            Else
                GoalThreshVal(kiter + 1, jgoal) = GoalThreshVal(kiter, jgoal) 'keep it the same - within the range you want.
                LastDevType(jgoal) = "within"
                AdjFactor(jgoal) = AdjFactor(jgoal) / 2 'temper this adjustment factor
            End If
        Next
    End Sub

    Private Sub ReadDataFile(ByVal infile As String)
        Dim fnum As Integer
        Dim arr() As String
        Dim dummy As String
        Dim areaind As Integer
        Dim valueind As Integer
        Dim polyidind As Integer
        Dim kpoly As Integer
        Dim tColVals() As String
        Dim bmatch As Byte

        'first count the records
        fnum = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        dummy = LineInput(fnum)
        NRecords = 0
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If Len(Trim(dummy)) < 2 Then Exit Do
            NRecords = NRecords + 1
        Loop
        FileClose(fnum)

        'now redim all this stuff
        ReDim ColInd(NColsToStore)
        'ReDim ColValue(NRecords, NColsToStore)
        ReDim GoalMatch(NRecords, NGoals)
        ReDim AreaVal(NRecords)
        ReDim ValueVal(NRecords)
        ReDim PolyID(NRecords)

        'open again to enter values
        fnum = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        dummy = LineInput(fnum)
        'get the column indices
        arr = Split(dummy, ",")
        For j As Integer = 0 To arr.Length - 1
            If arr(j) = ValColName Then valueind = j
            If arr(j) = AreaColName Then areaind = j
            If arr(j) = PolyIDColName Then polyidind = j
            For jj As Integer = 1 To NColsToStore
                If arr(j) = ColName(jj) Then
                    ColInd(jj) = j
                    Exit For
                End If
            Next
        Next
        kpoly = 0
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If Len(Trim(dummy)) < 2 Then Exit Do
            kpoly = kpoly + 1
            arr = Split(dummy, ",")
            AreaVal(kpoly) = arr(areaind)
            ValueVal(kpoly) = arr(valueind)
            PolyID(kpoly) = arr(polyidind)
            ReDim tColVals(NColsToStore)
            For jj As Integer = 1 To NColsToStore
                tColVals(jj) = arr(ColInd(jj))
            Next
            'check for goal matching
            For jgoal As Integer = 1 To NGoals
                'do the attribute values match this goal?
                bmatch = True
                For jcol As Integer = 1 To NColsGoal(jgoal)
                    bmatch = 0
                    For jj As Integer = 1 To NColsToStore
                        If tColVals(jj) = GoalColValue(jgoal, jcol) Then
                            bmatch = 1
                            Exit For
                        End If
                    Next
                    If bmatch = 0 Then Exit For 'missed a column
                Next
                GoalMatch(kpoly, jgoal) = bmatch
            Next
        Loop
        FileClose(fnum)



    End Sub
    Private Sub ReadGoalInput(ByVal infile As String)
        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String
        Dim kgoal As Integer
        Dim kcol As Integer
        Dim kval As Integer

        fnum = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        dummy = LineInput(fnum)
        dummy = LineInput(fnum)
        dummy = LineInput(fnum)
        dummy = LineInput(fnum)
        dummy = LineInput(fnum)
        dummy = LineInput(fnum)
        dummy = LineInput(fnum)
        NGoals = 0
        MaxColsGoalDef = -1
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If Len(dummy) < 2 Then Exit Do
            NGoals = NGoals + 1
            arr = Split(dummy, ",")
            If arr(0) > MaxColsGoalDef Then MaxColsGoalDef = arr(0)
        Loop
        FileClose(fnum)

        'redim all the arrays to fill
        ReDim NColsGoal(NGoals)
        ReDim GoalCols(NGoals, MaxColsGoalDef)
        ReDim GoalColValue(NGoals, MaxColsGoalDef)
        ReDim GoalMin(NGoals)
        ReDim GoalMax(NGoals)
        ReDim GoalOutputVal(NGoals), GoalThreshVal(NIters, NGoals)

        'do it again to poulate the goal information
        fnum = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        dummy = LineInput(fnum)
        dummy = LineInput(fnum)
        ValColName = Trim(dummy)
        dummy = LineInput(fnum)
        dummy = LineInput(fnum)
        AreaColName = Trim(dummy)
        dummy = LineInput(fnum)
        dummy = LineInput(fnum)
        PolyIDColName = Trim(dummy)
        dummy = LineInput(fnum)
        kgoal = 0
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If Len(dummy) < 2 Then Exit Do
            kgoal = kgoal + 1
            kcol = 0
            arr = Split(dummy, ",")
            NColsGoal(kgoal) = arr(0)
            'load the column name
            For jcol As Integer = 1 To NColsGoal(kgoal)
                kcol = kcol + 1
                GoalCols(kgoal, jcol) = arr(kcol)
            Next
            'load the column value
            For jcol As Integer = 1 To NColsGoal(kgoal)
                kcol = kcol + 1
                GoalColValue(kgoal, jcol) = arr(kcol)
            Next
            kcol = kcol + 1
            GoalMin(kgoal) = arr(kcol)
            kcol = kcol + 1
            GoalMax(kgoal) = arr(kcol)
            kcol = kcol + 1
            GoalOutputVal(kgoal) = arr(kcol)
            kcol = kcol + 1
            GoalThreshVal(1, kgoal) = arr(kcol)
        Loop
        FileClose(fnum)

        'now populate the number of unique values to output - create one output per value
        NOutputVals = 0
        ReDim OutputVal(NOutputVals)
        'and populate the number and names of the columns to store
        NColsToStore = 0
        ReDim ColName(NColsToStore)
        For jgoal As Integer = 1 To NGoals
            kval = GetStrIndex(OutputVal, GoalOutputVal(jgoal))
            If kval < 0 Then
                NOutputVals = NOutputVals + 1
                ReDim Preserve OutputVal(NOutputVals)
                OutputVal(NOutputVals) = GoalOutputVal(jgoal)
            End If
            For jcol As Integer = 1 To NColsGoal(jgoal)
                kval = GetStrIndex(ColName, GoalCols(jgoal, jcol))
                If kval < 0 Then
                    NColsToStore = NColsToStore + 1
                    ReDim Preserve ColName(NColsToStore)
                    ColName(NColsToStore) = GoalCols(jgoal, jcol)
                End If
            Next
        Next

    End Sub

    Private Sub WriteGoalOutputs(ByVal outfile As String, ByVal newgoalfilename As String)
        Dim fnum As Integer
        Dim dummy As String

        fnum = FreeFile()
        FileOpen(fnum, outfile, OpenMode.Output)
        dummy = "GoalNumber,GoalMin,GoalMax,OutputValue"
        For jiter As Integer = 1 To NIters
            dummy = dummy & ",I_" & jiter ' & GoalTotal(jiter, jgoal)
        Next
        PrintLine(fnum, dummy)
        For jgoal As Integer = 1 To NGoals
            dummy = jgoal & "," & GoalMin(jgoal) & "," & GoalMax(jgoal) & "," & GoalOutputVal(jgoal)
            For jiter As Integer = 1 To NIters
                dummy = dummy & "," & Math.Round(GoalTotal(jiter, jgoal), 0)
            Next
            PrintLine(fnum, dummy)
        Next
        FileClose(fnum)

        fnum = FreeFile()
        FileOpen(fnum, newgoalfilename, OpenMode.Output)

        PrintLine(fnum, "Continuous Value Column: Values in this column will be evaluated for above/below the thresholds for inclusion in the total")
        PrintLine(fnum, ValColName)
        PrintLine(fnum, "Area Column: Column with values per polygon that are used to meet the goals")
        PrintLine(fnum, AreaColName)
        PrintLine(fnum, "PolyID Column: Stores information about the polygon unique identifier to use as a spatial join later on")
        PrintLine(fnum, PolyIDColName)
        PrintLine(fnum, "NCols,Col1_etc,Colval1_etc,MinVal,MaxVal,OutputVal,InitialThreshhold")
        For jgoal As Integer = 1 To NGoals
            dummy = NColsGoal(jgoal) & ","
            For jcol As Integer = 1 To NColsGoal(jgoal)
                dummy = dummy & GoalCols(jgoal, jcol) & ","
            Next
            For jcol As Integer = 1 To NColsGoal(jgoal)
                dummy = dummy & GoalColValue(jgoal, jcol) & ","
            Next
            dummy = dummy & GoalMin(jgoal) & "," & GoalMax(jgoal) & "," & GoalOutputVal(jgoal) & "," & GoalThreshVal(NIters, jgoal)
            PrintLine(fnum, dummy)
        Next

        FileClose(fnum)
    End Sub
    Private Sub WritePolyOutputs(ByVal outfile As String)
        Dim fnum As Integer
        Dim dummy As String

        fnum = FreeFile()
        FileOpen(fnum, outfile, OpenMode.Output)
        dummy = PolyIDColName & "," & AreaColName & "," & ValColName
        For jval As Integer = 1 To NOutputVals
            dummy = dummy & ",Output_" & OutputVal(jval)
        Next
        PrintLine(fnum, dummy)
        For jpoly As Integer = 1 To NRecords
            dummy = PolyID(jpoly) & "," & AreaVal(jpoly) & "," & ValueVal(jpoly)
            For jval As Integer = 1 To NOutputVals
                dummy = dummy & "," & PolyOutputs(jpoly, jval)
            Next
            PrintLine(fnum, dummy)
        Next

        FileClose(fnum)
    End Sub

End Class
