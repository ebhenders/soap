Public Class FireSpread
    Dim NPolys As Integer
    Dim TimeSteps As Double
    Dim NFires As Integer

    Dim BurnQueueNow() As Byte
    Dim BurnQueueNext() As Byte
    Dim Burned() As Byte
    Dim TimeToBurn() As Double
    Dim PolyBurnedTime() As Single
    Dim Nrows As Integer
    Dim Ncols As Integer
    Dim PolyRow() As Integer
    Dim PolyCol() As Integer
    Dim ColRowPoly(,) As Integer
    Dim SpreadRateFrom(,) As Double
    Dim SpreadRateTo() As Double 'optional - the spread rate to get TO a polygon - can be used to distinguish water, NF, fuel breaks, etc.
    Dim BurnIter() As Integer
    Dim MaxTimePer As Integer
    Dim FireCells() As Integer



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'navigate to the file to load
        Dim infile As String
        Dim outfile As String
        Dim burnpoly As Integer
        Dim arr() As String
        Dim arr2() As String


        'IDENTIFY THE file OF INITIAL CONDITIONS file names
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Load a fire spread direction file"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        infile = openfiledialog2.FileName
        openfiledialog2 = Nothing
        outfile = DirName(infile) & FName(infile) & "_spread.txt"

        TimeSteps = tbTimeSteps.Text
        NFires = 0
        ReDim FireCells(NFires)

        MaxTimePer = 1

        LoadInput(infile)
        'simulation cells  
        If rbCellsToStart.Checked = True Then
            arr = Split(tbCellsToStart.Text, ",")
            For jstep As Integer = 0 To arr.Length - 1
                arr2 = Split(arr(jstep), "-")
                For jstep2 As Integer = CType(Trim(arr2(0)), Integer) To CType(Trim(arr2(arr2.Length - 1)), Integer)
                    NFires = NFires + 1
                    ReDim Preserve FireCells(NFires)
                    FireCells(NFires) = jstep2 'CType(Trim(arr2(NTimestep - 1)), Integer)
                Next
            Next
        Else
            NFires = tbNFires.Text
            ReDim FireCells(NFires)
            For jfire As Integer = 1 To NFires
                FireCells(jfire) = Math.Round(Rnd() * NPolys, 0)
            Next
        End If

        ReDim Burned(NPolys) 'universal; used to simulate multiple fires
        For jfire As Integer = 1 To NFires
            burnpoly = FireCells(jfire)
            DoBurn(burnpoly, TimeSteps)
        Next
        'debug
        'DoBurn(837, TimeSteps)
        WriteOutput(outfile)

        MsgBox("Done with fire spread")

    End Sub

    Private Sub DoBurn(ByVal StartPoly As Integer, ByVal BurnTime As Double)
        Dim BurnedTime As Double = 0
        Dim currmintime As Double
        Dim nextmintime As Double
        Dim kcount As Integer = 1


        ReDim BurnQueueNow(NPolys), BurnQueueNext(NPolys)
        ReDim TimeToBurn(NPolys)
        For jpoly As Integer = 1 To NPolys
            TimeToBurn(jpoly) = 999999
        Next

        'initiate the fire
        currmintime = 999999
        nextmintime = 0
        BurnIter(StartPoly) = kcount + SpreadRateTo(StartPoly) 'allows for fire starts in fuel breaks, NF, etc. to be delayed or not burn at all
        If BurnIter(StartPoly) <= BurnTime Then BurnPoly(StartPoly, nextmintime, currmintime)
        BurnedTime = BurnIter(StartPoly)
        PolyBurnedTime(StartPoly) = BurnedTime
        For jpoly As Integer = 1 To NPolys
            BurnQueueNow(jpoly) = BurnQueueNext(jpoly)
            BurnQueueNext(jpoly) = 0
        Next
        nextmintime = 999999

        Do Until BurnedTime >= BurnTime
            kcount = kcount + 1
            MaxTimePer = Math.Max(kcount, MaxTimePer)
            For jpoly As Integer = 1 To NPolys
                If BurnQueueNow(jpoly) = 1 Then
                    'check to see if it should be burned
                    If TimeToBurn(jpoly) <= currmintime And Burned(jpoly) = 0 Then
                        'burn this guy!
                        BurnPoly(jpoly, currmintime, nextmintime)
                        BurnIter(jpoly) = kcount
                        PolyBurnedTime(jpoly) = BurnedTime
                    Else
                        'bank it for next time around
                        BurnQueueNext(jpoly) = 1
                        BurnQueueNow(jpoly) = 0
                        TimeToBurn(jpoly) = TimeToBurn(jpoly) - currmintime
                        nextmintime = Math.Min(nextmintime, TimeToBurn(jpoly))
                    End If
                End If
            Next

            For jpoly As Integer = 1 To NPolys
                BurnQueueNow(jpoly) = BurnQueueNext(jpoly)
                BurnQueueNext(jpoly) = 0
            Next
            BurnedTime = BurnedTime + currmintime
            currmintime = nextmintime
            nextmintime = 999999
        Loop
    End Sub

    Private Sub LoadInput(ByVal infile As String)
        Dim fnum As Integer
        Dim arr() As String
        fnum = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        arr = Split(LineInput(fnum), ",")
        NPolys = arr(1)
        arr = Split(LineInput(fnum), ",")
        Ncols = arr(1)
        arr = Split(LineInput(fnum), ",")
        Nrows = arr(1)
        ReDim PolyRow(NPolys), PolyCol(NPolys), ColRowPoly(Ncols + 1, Nrows + 1), SpreadRateFrom(NPolys, 7), BurnIter(NPolys), PolyBurnedTime(NPolys), SpreadRateTo(NPolys)
        'dummy header row
        arr = Split(LineInput(fnum), ",")
        For jpoly As Integer = 1 To NPolys
            arr = Split(LineInput(fnum), ",")
            PolyCol(jpoly) = arr(1)
            PolyRow(jpoly) = arr(2)
            ColRowPoly(PolyCol(jpoly), PolyRow(jpoly)) = jpoly
            For j As Integer = 0 To 7
                SpreadRateFrom(jpoly, j) = arr(j + 3)
            Next
            SpreadRateTo(jpoly) = arr(11)
        Next


        FileClose(fnum)
    End Sub

    Private Sub BurnPoly(ByVal originpoly As Integer, ByVal currmintime As Double, ByRef nextmintime As Double)
        Dim kpoly As Integer
        Burned(originpoly) = 1
        BurnQueueNow(originpoly) = 0 'burned, so take it out
        BurnQueueNext(originpoly) = 0
        'n
        kpoly = ColRowPoly(PolyCol(originpoly), PolyRow(originpoly) + 1)
        AddBurnQueue(originpoly, kpoly, 0, currmintime, nextmintime)
        'ne
        kpoly = ColRowPoly(PolyCol(originpoly) + 1, PolyRow(originpoly) + 1)
        AddBurnQueue(originpoly, kpoly, 1, currmintime, nextmintime)
        'e
        kpoly = ColRowPoly(PolyCol(originpoly) + 1, PolyRow(originpoly))
        AddBurnQueue(originpoly, kpoly, 2, currmintime, nextmintime)
        'se
        kpoly = ColRowPoly(PolyCol(originpoly) + 1, PolyRow(originpoly) - 1)
        AddBurnQueue(originpoly, kpoly, 3, currmintime, nextmintime)
        's
        kpoly = ColRowPoly(PolyCol(originpoly), PolyRow(originpoly) - 1)
        AddBurnQueue(originpoly, kpoly, 4, currmintime, nextmintime)
        'sw
        kpoly = ColRowPoly(PolyCol(originpoly) - 1, PolyRow(originpoly) - 1)
        AddBurnQueue(originpoly, kpoly, 5, currmintime, nextmintime)
        'w
        kpoly = ColRowPoly(PolyCol(originpoly) - 1, PolyRow(originpoly))
        AddBurnQueue(originpoly, kpoly, 6, currmintime, nextmintime)
        'nw
        kpoly = ColRowPoly(PolyCol(originpoly) - 1, PolyRow(originpoly) + 1)
        AddBurnQueue(originpoly, kpoly, 7, currmintime, nextmintime)
    End Sub
    Private Sub AddBurnQueue(ByVal originpoly As Integer, ByVal kpoly As Integer, ByVal kdir As Integer, ByVal currmintime As Double, ByRef nextmintime As Double)
        'would this poly have burned anyway?
        'if BurnQueueNow = 1 and timetoburn < currmintime then YES - exit the sub
        If BurnQueueNow(kpoly) = 1 And TimeToBurn(kpoly) <= currmintime Then Exit Sub 'it will burn when you get there...

        If kpoly > 0 And Burned(kpoly) = 0 Then

            'have to update the burn time - decrease by the current burn time. This is done for both BurnQueNow = 0 and 1, and only is relevant when BurnQueNow = 1
            'Once BurnQueueNext is set to 1 it means you have decreased TimeToBurn by the currmintime - either here or in the BurnPoly sub
            If BurnQueueNext(kpoly) = 0 Then TimeToBurn(kpoly) = TimeToBurn(kpoly) - currmintime 'need to set this first to evaluate rate of spread from the current set of burning polygons

            'This is where you set the initial time to burn for the poly that is something other than the default Big-M. 
            '  This can also set the minimum time to burn from different directions if more than 1 adjacent polygon starts burning in this iteration
            '  or in the case where a newly burning polygon is adjacent to a polygon already in the queue and gets fire faster from the newly burning polygon than the old one
            If SpreadRateFrom(originpoly, kdir) + SpreadRateTo(kpoly) < TimeToBurn(kpoly) Then TimeToBurn(kpoly) = SpreadRateFrom(originpoly, kdir) + SpreadRateTo(kpoly)
            BurnQueueNext(kpoly) = 1
            BurnQueueNow(kpoly) = 0 'it wouldn't have burned now anyway
            nextmintime = Math.Min(TimeToBurn(kpoly), nextmintime)
        End If

    End Sub

    Private Sub WriteOutput(ByVal outfile As String)
        Dim fnum As Integer = FreeFile()
        Dim dummy As String
        Dim bOrigin As Boolean
        Dim Started() As Boolean

        ReDim Started(NPolys)
        FileOpen(fnum, outfile, OpenMode.Output)
        dummy = "Poly"
        For j As Integer = 1 To TimeSteps
            dummy = dummy & ",B_" & j
        Next
        PrintLine(fnum, dummy)
        For jpoly As Integer = 1 To NPolys
            'If jpoly = 15663 Then Stop
            dummy = jpoly
            bOrigin = False
            For jstart As Integer = 1 To NFires
                If jpoly = FireCells(jstart) Then
                    bOrigin = True
                    Exit For
                End If
            Next
            For j As Integer = 1 To TimeSteps 'MaxTimePer
                If Burned(jpoly) = 0 Then
                    dummy = dummy & "," & SpreadRateTo(jpoly)
                Else
                    If j < PolyBurnedTime(jpoly) Then dummy = dummy & "," & SpreadRateTo(jpoly)

                    If j >= PolyBurnedTime(jpoly) And PolyBurnedTime(jpoly) < j + 1 And Started(jpoly) = False Then
                        dummy = dummy & ",1"
                        Started(jpoly) = True
                    End If

                    If j > PolyBurnedTime(jpoly) And bOrigin = False Then dummy = dummy & ",2"
                    If j > PolyBurnedTime(jpoly) And bOrigin = True Then dummy = dummy & ",3"
                End If
            Next
            PrintLine(fnum, dummy)
        Next

        FileClose(fnum)
    End Sub
End Class