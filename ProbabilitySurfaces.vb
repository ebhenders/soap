Public Class ProbabilitySurfaces

    Dim NRuns As Integer
    Dim NTimePers As Integer
    Dim NValues As Integer
    Dim NCells As Integer

    Dim HabName As String

    'Dim MaxValues As Integer = 20
    Dim ValueVal() As Integer 'if unique values are not sequential, this can bin up the total number of unique values into a smaller array
    Dim FileNames() As String
    Dim bAcrossTime As Boolean
    Dim bAcrossRuns As Boolean
    Dim bAcrossTimeAndRuns As Boolean

    Dim bSpecificTimePers As Boolean
    Dim UsePer() As Integer

    Dim CountCellRunVal(,,) As Long 'count of a value by cell,run,value
    Dim CountCellTimeVal(,,) As Long 'count of a value by cell,time period,value
    Dim CountCellVal(,) As Long 'count of a value by cell,value


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim folderdialog2 As New FolderBrowserDialog
        Dim openfiledialog2 As New OpenFileDialog
        Dim arr() As String
        Dim arr2() As String
        Dim TempItems() As String
        Dim foldername As String
        Dim fnum As Integer
        Dim fname As String

        '1. Browse to folder with maps to process

        'IDENTIFY THE DIRECTORY WITH MODEL DEFINITION FILES IN IT

        NumItems = 0
        Dim i As Integer = 0

        With folderdialog2
            .SelectedPath = foldername
            .Description = "Folder that contains run outputs to process. You will then be asked which files within here to use in the processing"
            '.Title = "Select a folder that has the "
            '.Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        foldername = folderdialog2.SelectedPath
        folderdialog2 = Nothing

        'populate the possible runs to process for the selector and then call the selector if you find a list of them...
        Dim fldr As New System.IO.DirectoryInfo(foldername)
        Dim files As System.IO.FileInfo() = fldr.GetFiles("*.txt*")
        Dim fi As System.IO.FileInfo
        NumItems = files.Length
        ReDim TempItems(NumItems)
        i = 0
        For Each fi In files
            i = i + 1
            arr = Split(fi.Name, ".")
            TempItems(i) = fi.Name
        Next
        NumItems = i
        If NumItems = 0 Then
            MsgBox("Folder contains no map files")
            Exit Sub
        End If

        ReDim ItemName(NumItems)
        For i = 1 To NumItems
            ItemName(i) = TempItems(i)
        Next

        '2. Select the maps in the folder to process
        Dim SelectorFrm As New Selector
        SelectorText = "Choose Map Files To Process"
        SelectorFrm.ShowDialog()
        SelectorFrm = Nothing

        If FrmCancel = False Then
            '3. Write the file with names of the maps you chose
            FileListFN = foldername & "\MapFilesForProcessing.txt"
            fnum = FreeFile()
            FileOpen(fnum, FileListFN, OpenMode.Output)
            For j As Integer = 1 To ItemsToRead.Length - 1
                PrintLine(fnum, foldername & "\" & ItemsToRead(j))
            Next
            FileClose(fnum)
        Else
            Exit Sub
        End If

        openfiledialog2 = Nothing

        Dim savefiledialog As New SaveFileDialog
        With savefiledialog
            .InitialDirectory = foldername
            .Title = "Save the output file as"
            .Filter = "TextFile(.txt) |*.txt"
            .FileName = "ProbSurface"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        OutputFile = savefiledialog.FileName
        savefiledialog = Nothing

        bAcrossTime = cbAcrossTime.Checked
        bAcrossRuns = cbAcrossRuns.Checked
        bAcrossTimeAndRuns = cbAcrossTimeAndRuns.Checked
        bSpecificTimePers = rbTimePers.Checked

        If bSpecificTimePers Then
            'timestep stuff
            NTimestep = 0
            'NTimestep = arr.Length
            ReDim TimestepIndex(0)
            TimestepIndex(0) = 0
            arr = Split(tbTimePers.Text, ",")
            For jstep As Integer = 0 To arr.Length - 1
                arr2 = Split(arr(jstep), "-")
                For jstep2 As Integer = CType(Trim(arr2(0)), Integer) To CType(Trim(arr2(arr2.Length - 1)), Integer)
                    NTimestep = NTimestep + 1
                    ReDim Preserve TimestepIndex(NTimestep)
                    TimestepIndex(NTimestep) = jstep2 'CType(Trim(arr2(NTimestep - 1)), Integer)
                Next
            Next
        End If

        If bAcrossTime Or bAcrossRuns Or bAcrossTimeAndRuns Then
            GetFileNames(FileListFN)
            GetFileInfo(FileNames(NRuns))
            If bSpecificTimePers = False Then
                NTimestep = NTimePers
                ReDim TimestepIndex(NTimestep)
                For jper As Integer = 0 To NTimestep
                    TimestepIndex(jper) = jper
                Next
            End If

        Else
            Exit Sub
        End If


        'populate the appropriate arrays
        If bAcrossTime Then ReDim CountCellRunVal(NCells, NRuns, NValues)
        If bAcrossRuns Then ReDim CountCellTimeVal(NCells, NTimestep, NValues)
        If bAcrossTimeAndRuns Then ReDim CountCellVal(NCells, NValues)

        'collect the data
        GetDataToProcess()

        'write the stuff out
        If bAcrossTime = True Then WriteAcrossTime()
        If bAcrossRuns = True Then WriteAcrossRuns()
        If bAcrossTimeAndRuns = True Then WriteAcrossTimeAndRuns()

        If cbWriteColorFile.Checked = True Then WriteColorFile()

        MsgBox("Done")

    End Sub

    Private Sub WriteAcrossTime()
        Dim fnum As Integer
        Dim dummy As String

        'new map for each value
        For jval As Integer = 1 To NValues
            fnum = FreeFile()
            FileOpen(fnum, DirName(OutputFile) & FName(OutputFile) & "_Output" & ValueVal(jval) & "_AcrossTime.txt", OpenMode.Output)
            'write the header line
            dummy = "SLINK"
            For jrun As Integer = 1 To NRuns
                dummy = dummy & "," & HabName & "_" & jrun
            Next
            PrintLine(fnum, dummy)
            'print out values for each cell
            For jcell As Integer = 1 To NCells
                dummy = jcell
                For jrun As Integer = 1 To NRuns
                    dummy = dummy & "," & Math.Round(CountCellRunVal(jcell, jrun, jval) / NTimestep, 3)
                Next
                PrintLine(fnum, dummy)
            Next

            FileClose(fnum)
        Next

    End Sub

    Private Sub WriteAcrossRuns()
        Dim fnum As Integer
        Dim dummy As String

        'new map for each value
        For jval As Integer = 1 To NValues
            fnum = FreeFile()
            FileOpen(fnum, DirName(OutputFile) & FName(OutputFile) & "_Output" & ValueVal(jval) & "_AcrossRuns.txt", OpenMode.Output)
            'write the header line
            dummy = "SLINK"
            For jper As Integer = 0 To NTimestep
                dummy = dummy & "," & HabName & "_" & TimestepIndex(jper)
            Next
            PrintLine(fnum, dummy)
            'print out values for each cell
            For jcell As Integer = 1 To NCells
                dummy = jcell
                For jper As Integer = 0 To NTimestep
                    dummy = dummy & "," & Math.Round(CountCellTimeVal(jcell, jper, jval) / NRuns, 3)
                Next
                PrintLine(fnum, dummy)
            Next

            FileClose(fnum)
        Next
    End Sub

    Private Sub WriteAcrossTimeAndRuns()
        Dim fnum As Integer
        Dim dummy As String

        'new map for each value
        For jval As Integer = 1 To NValues
            fnum = FreeFile()
            FileOpen(fnum, DirName(OutputFile) & FName(OutputFile) & "_Output" & ValueVal(jval) & "_AcrossRunsAndTime.txt", OpenMode.Output)
            'write the header line
            dummy = "SLINK," & HabName & "_1"

            PrintLine(fnum, dummy)
            'print out values for each cell
            For jcell As Integer = 1 To NCells
                dummy = jcell & "," & Math.Round(CountCellVal(jcell, jval) / (NTimestep * NRuns), 3)
                PrintLine(fnum, dummy)
            Next

            FileClose(fnum)
        Next
    End Sub

    Private Sub WriteColorFile()
        Dim nbreaks As Integer = CType(tbNBreaks.Text, Integer)
        Dim bZeroClass As Boolean = cbZeroClass.Checked
        Dim rgb As String
        Dim fnum As Integer
        Dim r As Integer
        Dim g As Integer
        Dim b As Integer
        Dim primary As Integer
        Dim secondary As Integer
        Dim redn As Integer
        Dim begclass As Single
        Dim endclass As Single
        Dim classsize As Single
        Dim label As String
        Dim reduction As Integer = Math.Round(200 / (nbreaks / 2), 0)
        classsize = Math.Round(1 / nbreaks, 2)

        If rbRed.Checked = True Then rgb = "red"
        If rbGreen.Checked = True Then rgb = "green"
        If rbBlue.Checked = True Then rgb = "blue"
        If bZeroClass = True Then nbreaks = nbreaks + 1
        fnum = FreeFile()

        FileOpen(fnum, DirName(OutputFile) & "MapColors_" & rgb & "_Ramp_" & nbreaks & "_Breaks.clr", OpenMode.Output)
        PrintLine(fnum, "NColors")
        PrintLine(fnum, nbreaks)
        PrintLine(fnum, "LabelColorIndex(i),ColmnName(s),LabelString(s),LabelMinVal(i),LabelMaxVal(i),R,G,B")
        begclass = 0
        redn = 0
        primary = 255
        secondary = 255 + reduction

        For jcolor As Integer = 1 To nbreaks
            secondary = Math.Max(secondary - reduction, 0)
            endclass = Math.Round(begclass + classsize, 2)
            If bZeroClass = True And jcolor = 1 Then endclass = 0
            If jcolor = nbreaks Then endclass = 1
            If jcolor > nbreaks / 2 Then
                primary = Math.Max(primary - (reduction * redn), 0)
                secondary = 0
                redn = 1
            End If
            If rgb = "red" Then
                r = primary
                g = secondary
                b = secondary
            ElseIf rgb = "green" Then
                r = secondary
                g = primary
                b = secondary
            ElseIf rgb = "blue" Then
                r = secondary
                g = secondary
                b = primary
            End If
            label = Math.Round(begclass * 100, 0) & "-" & Math.Round(endclass * 100, 0)
            PrintLine(fnum, jcolor & "," & HabName & "," & label & "," & begclass & "," & endclass & "," & r & "," & g & "," & b)
            begclass = endclass
        Next
        FileClose(fnum)

    End Sub
    Private Sub GetDataToProcess()
        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String
        Dim val As Integer
        Dim kper As Integer = -1

        For jrun As Integer = 1 To NRuns
            fnum = FreeFile()
            FileOpen(fnum, FileNames(jrun), OpenMode.Input)
            dummy = LineInput(fnum) 'header line of the file; also contains the name of the
            For jcell As Integer = 1 To NCells
                kper = -1
                dummy = LineInput(fnum)
                arr = Split(dummy, ",")
                For jper As Integer = 0 To arr.Length - 2
                    'test for valid time period
                    If UsePer(jper) >= 0 Then
                        kper = kper + 1
                        val = GetIntIndex(ValueVal, Trim(arr(jper + 1)))
                        If bAcrossTime = True And jper > 0 Then CountCellRunVal(jcell, jrun, val) = CountCellRunVal(jcell, jrun, val) + 1 'value at time 0 is the same for all runs, so don't bias the result by including this value in the calculation
                        If bAcrossRuns = True Then CountCellTimeVal(jcell, kper, val) = CountCellTimeVal(jcell, kper, val) + 1
                        If bAcrossTimeAndRuns = True And jper > 0 Then CountCellVal(jcell, val) = CountCellVal(jcell, val) + 1 'value at time 0 is the same for all runs, so don't bias the result by including this value in the calculation
                    End If
                Next
            Next

            FileClose(fnum)
        Next
    End Sub

    Private Sub GetFileInfo(ByVal file As String)
        'gets data on the number of time periods and possible values to generate maps for
        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String
        Dim arr2() As String
        Dim ttime As Integer
        Dim kind As Integer = -1 '0 is always output
        Dim tind As Integer

        fnum = FreeFile()
        FileOpen(fnum, file, OpenMode.Input)
        dummy = LineInput(fnum) 'header line
        arr = Split(dummy, ",")
        NTimePers = arr.Length - 2 'first col. is the poly ID
        ReDim UsePer(NTimePers)
        arr2 = Split(arr(1), "_")
        HabName = arr2(0)
        For j As Integer = 1 To arr2.Length - 2
            HabName = HabName & "_" & arr2(j)
        Next
        For jper As Integer = 0 To arr.Length - 2
            If bSpecificTimePers = True Then
                arr2 = Split(arr(jper + 1), "_")
                ttime = arr2(arr2.Length - 1)
                tind = GetIntIndex(TimestepIndex, ttime)
                UsePer(jper) = tind
            Else
                UsePer(jper) = 1
            End If
        Next

        NCells = 0
        NValues = 0
        ReDim ValueVal(NValues)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If Trim(dummy).Length = 0 Then Exit Do
            NCells = NCells + 1
            arr = Split(dummy, ",")
            For j As Integer = 1 To arr.Length - 1
                If GetIntIndex(ValueVal, Trim(arr(j))) < 0 Then
                    NValues = NValues + 1
                    ReDim Preserve ValueVal(NValues)
                    ValueVal(NValues) = Trim(arr(j))
                End If
            Next
        Loop
        FileClose(fnum)

    End Sub


    Private Sub GetFileNames(ByVal file As String)
        Dim fnum As Integer = FreeFile()
        Dim dummy As String
        Dim arr() As String

        FileOpen(fnum, file, OpenMode.Input)
        NRuns = 0
        ReDim FileNames(NRuns)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                NRuns = NRuns + 1
                ReDim Preserve FileNames(NRuns)
                FileNames(NRuns) = Trim(dummy)
            End If
        Loop
        FileClose(fnum)
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        MsgBox("This takes a map file or series of map files and produces a map of the percentage of the time a value occurs in each polygon. This assumes multiple maps are for the same area and have exactly the same number of records.")
    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbAcrossTimeAndRuns.CheckedChanged

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        MsgBox("For each run in the batch file, you will get a probability surface that represents all time periods in each run. This actually can be used for multiple geographic areas without the same number of records.")
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        MsgBox("For each time period, you will get probability surface generated from all runs in the batch file. This needs all files to be the same number of records long")
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        MsgBox("This generates a probability surface for all time periods, all runs. This requires all datasets in the batch file to be the same number of records. This is the most generalized depiction of the choices")
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged

    End Sub

    Private Sub rbBlue_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbBlue.CheckedChanged

    End Sub
End Class