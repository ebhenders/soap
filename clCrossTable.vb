Public Class clCrossTable
    Dim NValues As Integer
    Dim ValueCode() As String 'by NValues
    Dim NGroups As Integer
    Dim GroupCode() As String
    Dim CrossTabVal(,,) As Integer 'by ngroups,nvalues, nvalues
    Dim InputFile As String
    Dim OutputFile As String
    Dim NLines As Integer
    Dim NCodesInGroup() As Integer

 
   Public Sub RunCrossTab()

        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select the file to do a crosstab"
            .Filter = "Text files(.csv) |*.csv"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        InputFile = openfiledialog2.FileName

        openfiledialog2 = Nothing

        Dim savefiledialog As New SaveFileDialog

        With savefiledialog
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Save the output file as"
            .Filter = "Comma-delimited files(.csv) |*.csv"
            .FileName = "output"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With



        OutputFile = savefiledialog.FileName
        savefiledialog = Nothing
        ReadValues(InputFile)
        PopCrossTabVals(InputFile)
        WriteOutputFile(OutputFile)

        MsgBox("Done")

    End Sub

    Private Sub WriteOutputFile(ByVal outfile As String)
        Dim dumout As String
        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, outfile, OpenMode.Output)
        'write the header line

        PrintLine(fnum, "Output for: " & InputFile)
        For jgroup As Integer = 1 To NGroups
            PrintLine(fnum, "Group: " & GroupCode(jgroup) & "," & NCodesInGroup(jgroup) & " records in this group")
            dumout = ""
            For j As Integer = 1 To NValues
                dumout = dumout & "," & ValueCode(j)
            Next
            PrintLine(fnum, dumout)
            For j As Integer = 1 To NValues
                dumout = ValueCode(j)
                For jj As Integer = 1 To NValues
                    dumout = dumout & "," & CrossTabVal(jgroup, j, jj)
                Next
                PrintLine(fnum, dumout)
            Next
        Next jgroup

        FileClose(fnum)

    End Sub
    Private Sub PopCrossTabVals(ByVal infile As String)


        Dim dummy As String
        Dim lastparent As String = ""
        Dim thisparent As String
        Dim thisgroupind As Integer
        Dim lastgroupind As Integer
        Dim ind1 As Integer
        Dim ind2 As Integer
        Dim arr() As String
        Dim NValsToCrossTab As Integer
        Dim ValsToCrossTab() As String
        Dim kline As Integer = 0

        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)

        ReDim CrossTabVal(NGroups, NValues, NValues), ValsToCrossTab(NValues), NCodesInGroup(NGroups)
        'header col
        dummy = LineInput(fnum)
        'groupind = 0
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            kline = kline + 1
            arr = Split(dummy, ",")
            thisparent = Trim(arr(1))
            thisgroupind = GetStrIndex(GroupCode, Trim(arr(0)))
            If thisparent <> lastparent Or kline = NLines Then 'write out when  you get to the last line, too
                If kline = NLines Then
                    NValsToCrossTab = NValsToCrossTab + 1
                    ValsToCrossTab(NValsToCrossTab) = Trim(arr(2))
                    lastgroupind = thisgroupind
                End If
                'populate the crosstab values
                For j As Integer = 1 To NValsToCrossTab
                    For jj As Integer = 1 To NValsToCrossTab
                        ind1 = GetStrIndex(ValueCode, ValsToCrossTab(j))
                        ind2 = GetStrIndex(ValueCode, ValsToCrossTab(jj))
                        'If ind1 <> ind2 Then
                        CrossTabVal(lastgroupind, ind1, ind2) = CrossTabVal(lastgroupind, ind1, ind2) + 1
                        'End If

                    Next
                Next
                'load in the information for this record
                NValsToCrossTab = 1
                ValsToCrossTab(NValsToCrossTab) = Trim(arr(2))

                'count ncodes in group
                NCodesInGroup(lastgroupind) = NCodesInGroup(lastgroupind) + 1
            Else
                NValsToCrossTab = NValsToCrossTab + 1
                ValsToCrossTab(NValsToCrossTab) = Trim(arr(2))

            End If
            lastparent = thisparent
            lastgroupind = thisgroupind 'groupind = GetStrIndex(GroupCode, Trim(arr(0)))
        Loop
        FileClose(fnum)

    End Sub
    Private Sub ReadValues(ByVal infile As String)


        Dim dummy As String
        Dim ind As Integer
        Dim groupind As Integer
        Dim arr() As String

        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)

        NValues = 0
        ReDim ValueCode(NValues)
        NGroups = 0
        ReDim GroupCode(NGroups)

        NLines = 0

        'header col
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            NLines = NLines + 1
            arr = Split(dummy, ",")
            ind = GetStrIndex(ValueCode, Trim(arr(2)))
            groupind = GetStrIndex(GroupCode, Trim(arr(0)))
            If ind < 0 Then
                NValues = NValues + 1
                ReDim Preserve ValueCode(NValues)
                ValueCode(NValues) = Trim(arr(2))
            End If
            If groupind < 0 Then
                NGroups = NGroups + 1
                ReDim Preserve GroupCode(NGroups)
                GroupCode(NGroups) = Trim(arr(0))
            End If
        Loop
        FileClose(fnum)
    End Sub
End Class
