Public Class ProbabilitySurfaces_Overlay

    Dim FNBaseProbFile As String

    Dim NRuns As Integer
    Dim NTimePers As Integer
    'Dim NValues As Integer
    Dim NCells As Integer
    Dim IncreaseFactor As Single
    Dim DecreaseFactor As Single
    Dim Threshhold As Single
    Dim CellSize As Double

    Dim BaseProb() As Single 'this is dimmed by NCells and contains the base map information for the base probability

    Dim HabName As String
    Dim HabTotal(,) As Single 'tallies the acres that meet the min. threshhold, after the adjustments are made. Dimmed by run, time

    Dim FileNames() As String
   
    Dim bSpecificTimePers As Boolean
    Dim UsePer() As Integer


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim openfiledialog2 As New OpenFileDialog
        Dim arr() As String
        Dim arr2() As String

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select a file with the base probabilities to process"
            .Filter = "Text files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        FNBaseProbFile = openfiledialog2.FileName

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select a file with the names of the overlay files to process"
            .Filter = "Text files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        FileListFN = openfiledialog2.FileName

        openfiledialog2 = Nothing

        Dim savefiledialog As New SaveFileDialog
        With savefiledialog
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Save the output file as"
            .Filter = "TextFile(.txt) |*.txt"
            .FileName = "ProbSurface_Overlay"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        OutputFile = savefiledialog.FileName
        OutputFile = DirName(OutputFile) & FName(OutputFile) ' OutputFile.Substring(0, OutputFile.Length - 5)
        savefiledialog = Nothing


        IncreaseFactor = CType(tbIncreaseFactor.Text, Single)
        DecreaseFactor = CType(tbDecreaseFactor.Text, Single)
        Threshhold = CType(tbThreshhold.Text, Single)
        CellSize = CType(tbCellSize.Text, Double)

        bSpecificTimePers = rbTimePers.Checked

        If bSpecificTimePers Then
            'timestep stuff
            NTimestep = 0
            'NTimestep = arr.Length
            ReDim TimestepIndex(0)
            TimestepIndex(0) = 0
            arr = Split(tbTimePers.Text, ",")
            For jstep As Integer = 0 To arr.Length - 1
                arr2 = Split(arr(jstep), "-")
                For jstep2 As Integer = CType(Trim(arr2(0)), Integer) To CType(Trim(arr2(arr2.Length - 1)), Integer)
                    NTimestep = NTimestep + 1
                    ReDim Preserve TimestepIndex(NTimestep)
                    TimestepIndex(NTimestep) = jstep2 'CType(Trim(arr2(NTimestep - 1)), Integer)
                Next
            Next
        End If

        'If bAcrossTime Or bAcrossRuns Or bAcrossTimeAndRuns Then
        GetFileNames(FileListFN)
        LoadFileInfo(FileNames(1)) 'use the first file to figure out the number of cells in the base map
        If bSpecificTimePers = False Then
            NTimestep = NTimePers
            ReDim TimestepIndex(NTimestep)
            For jper As Integer = 0 To NTimestep
                TimestepIndex(jper) = jper
            Next
        End If

        LoadBaseMap(FNBaseProbFile) 'get the base probabilities to process

        'collect the data
        GetDataToProcess()

        WriteOutputs()


        MsgBox("Done")

    End Sub
    Private Sub LoadFileInfo(ByVal infile As String)
        'finds the NCells

        Dim dummy As String
        Dim arr() As String
        Dim arr2() As String
        Dim ttime As Integer
        Dim kind As Integer = -1 '0 is always output
        Dim tind As Integer
        NCells = 0
        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        dummy = LineInput(fnum)
        ''get the hab name while we're here
        arr = Split(dummy, ",")
        NTimePers = arr.Length - 2 'first col. is the poly ID
        ReDim UsePer(NTimePers)
        arr2 = Split(arr(1), "_")
        HabName = arr2(0)
        For j As Integer = 1 To arr2.Length - 2
            HabName = HabName & "_" & arr2(j)
        Next
        For jper As Integer = 0 To arr.Length - 2
            If bSpecificTimePers = True Then
                arr2 = Split(arr(jper + 1), "_")
                ttime = arr2(1)
                tind = GetIntIndex(TimestepIndex, ttime)
                UsePer(jper) = tind
            Else
                UsePer(jper) = 1
            End If
        Next
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length = 0 Then Exit Do
            NCells = NCells + 1
        Loop
        FileClose(fnum)


    End Sub

    Private Sub LoadBaseMap(ByVal infile As String)
        'this sub loads the probability values of the base map
        ReDim BaseProb(NCells)
        Dim dummy As String
        Dim arr() As String
        Dim SlinkField As Integer = -1
        Dim MeanField As Integer = -1
        Dim slink As Long
        Dim mean As Single
        'have to find the SLINK field, and the MEAN field
        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        dummy = LineInput(fnum)
        arr = Split(dummy, ",")
        For j As Integer = 0 To arr.Length - 1
            If arr(j) = "SLINK" Then SlinkField = j
            If arr(j) = "MEAN" Then MeanField = j
            If SlinkField >= 0 And MeanField >= 0 Then Exit For
        Next
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length < 1 Then Exit Do
            arr = Split(dummy, ",")
            slink = CType(arr(SlinkField), Long)
            mean = CType(arr(MeanField), Single)
            BaseProb(slink) = mean

        Loop
        FileClose(fnum)


    End Sub

    Private Sub WriteOutputs()
        'writes out the hab totals
        Dim fnum As Integer
        Dim printstring As String

        fnum = FreeFile()
        FileOpen(fnum, OutputFile & "Summary.txt", OpenMode.Output)
        printstring = "RunNumber,Habitat"
        For jper As Integer = 1 To NTimePers
            printstring = printstring & "," & jper
        Next
        PrintLine(fnum, printstring)
        For jrun As Integer = 1 To NRuns
            printstring = ",Run_" & jrun
        Next
        For jrun As Integer = 1 To NRuns
            printstring = jrun & "," & HabName
            For jper As Integer = 1 To NTimePers
                printstring = printstring & "," & Math.Round(CellSize * HabTotal(jrun, jper), 0)
            Next
        Next
        PrintLine(fnum, printstring)
        FileClose(fnum)
    End Sub

    Private Sub GetDataToProcess()
        Dim fnum As Integer
        Dim fnumOut As Integer
        Dim dummy As String
        Dim arr() As String
        Dim val As Integer
        Dim tval As Single
        Dim cellnum As Long
        Dim kper As Integer = -1
        Dim printstring As String

        ReDim HabTotal(NRuns, NTimestep)

        For jrun As Integer = 1 To NRuns
            fnum = FreeFile()
            FileOpen(fnum, FileNames(jrun), OpenMode.Input)
            fnumOut = FreeFile()
            FileOpen(fnumOut, OutputFile & jrun & ".txt", OpenMode.Output)
            dummy = LineInput(fnum)
            PrintLine(fnumOut, dummy)
            For jcell As Integer = 1 To NCells
                kper = -1
                dummy = LineInput(fnum)
                arr = Split(dummy, ",")

                cellnum = CType(Trim(arr(0)), Long)
                printstring = cellnum
                For jper As Integer = 0 To arr.Length - 2
                    'test for valid time period
                    If UsePer(jper) >= 0 Then
                        kper = kper + 1
                        val = CType(Trim(arr(jper + 1)), Single)
                        If val >= 1 Then
                            tval = Math.Min(BaseProb(cellnum) * IncreaseFactor, 1)
                        Else
                            tval = BaseProb(cellnum) * DecreaseFactor
                        End If
                        printstring = printstring & "," & tval
                        If tval >= Threshhold Then
                            HabTotal(jrun, kper) = HabTotal(jrun, kper) + 1
                        End If
                    End If
                Next
                PrintLine(fnumOut, printstring)
            Next

            FileClose(fnum)
            FileClose(fnumOut)
        Next
    End Sub


    Private Sub GetFileNames(ByVal file As String)
        Dim fnum As Integer = FreeFile()
        Dim dummy As String
        Dim arr() As String

        FileOpen(fnum, file, OpenMode.Input)
        NRuns = 0
        ReDim FileNames(NRuns)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                NRuns = NRuns + 1
                ReDim Preserve FileNames(NRuns)
                FileNames(NRuns) = Trim(dummy)
            End If
        Loop
        FileClose(fnum)
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        MsgBox("This takes a base probability surface and modifies it according to a 0/1 mask. One can choose the magnitude of increase for a 1 and decrease for a 0.The base map of probabilities must contain a SLINK field and a MEAN field")
    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        MsgBox("The base Prob factor surface will be multiplied by this value when there is an overlay value of 1. Suggest this value is >=1.")
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        MsgBox("The base Prob factor surface will be multiplied by this value when there is an overlay value of 0. Suggest this value is <=1.")
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        MsgBox("This generates a probability surface for all time periods, all runs. This requires all datasets in the batch file to be the same number of records. This is the most generalized depiction of the choices")
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged

    End Sub

    Private Sub Button5_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        MsgBox("After constructing the probability map with the adjustments from the overlay, pixels/cells with a value >= the threshhold will be counted as habitat and tallied")
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        MsgBox("The number of area units each cell in the overlay represents. Probably express in acres")
    End Sub
End Class