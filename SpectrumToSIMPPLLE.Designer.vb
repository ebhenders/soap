<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SpectrumToSIMPPLLE
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button4 = New System.Windows.Forms.Button
        Me.tbAcresByGeoArea = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Button5 = New System.Windows.Forms.Button
        Me.tbModelDefFolder = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.CompileButton = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.tbNTic = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.tbSpectrumOutputFile = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.tbSIMPPLLECellSize = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.tbGISCellSize = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Button2 = New System.Windows.Forms.Button
        Me.cbbZones = New System.Windows.Forms.ComboBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.rbOWN = New System.Windows.Forms.RadioButton
        Me.rbSA = New System.Windows.Forms.RadioButton
        Me.SuspendLayout()
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(768, 142)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 19)
        Me.Button4.TabIndex = 26
        Me.Button4.Text = "Browse"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'tbAcresByGeoArea
        '
        Me.tbAcresByGeoArea.Location = New System.Drawing.Point(153, 141)
        Me.tbAcresByGeoArea.Name = "tbAcresByGeoArea"
        Me.tbAcresByGeoArea.Size = New System.Drawing.Size(596, 20)
        Me.tbAcresByGeoArea.TabIndex = 24
        Me.tbAcresByGeoArea.Text = "D:\R1\Revisions\KIPZ\KIPZ_SIMPPLLE\Oct2011_Runs\Scenario1\BULLSIMEX_S1-simulation" & _
            "\textdata"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(27, 141)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(120, 20)
        Me.Label4.TabIndex = 25
        Me.Label4.Text = "Acreage by Geo Area"
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(768, 106)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(75, 20)
        Me.Button5.TabIndex = 23
        Me.Button5.Text = "Browse"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'tbModelDefFolder
        '
        Me.tbModelDefFolder.Location = New System.Drawing.Point(153, 105)
        Me.tbModelDefFolder.Name = "tbModelDefFolder"
        Me.tbModelDefFolder.Size = New System.Drawing.Size(596, 20)
        Me.tbModelDefFolder.TabIndex = 21
        Me.tbModelDefFolder.Text = "D:\R1\Revisions\KIPZ\KIPZ_SIMPPLLE\Oct2011_Runs\Scenario1\BULLSIMEX_S1-simulation" & _
            "\textdata"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(27, 105)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 32)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "Model Definition Folder"
        '
        'CompileButton
        '
        Me.CompileButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CompileButton.Location = New System.Drawing.Point(500, 271)
        Me.CompileButton.Name = "CompileButton"
        Me.CompileButton.Size = New System.Drawing.Size(96, 32)
        Me.CompileButton.TabIndex = 20
        Me.CompileButton.Text = "GO"
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(113, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(445, 40)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Translate Spectrum to SIMPPLLE"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(27, 281)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(120, 31)
        Me.Label3.TabIndex = 27
        Me.Label3.Text = "N Time Periods To Translate"
        '
        'tbNTic
        '
        Me.tbNTic.Location = New System.Drawing.Point(153, 278)
        Me.tbNTic.Name = "tbNTic"
        Me.tbNTic.Size = New System.Drawing.Size(56, 20)
        Me.tbNTic.TabIndex = 28
        Me.tbNTic.Text = "5"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(768, 181)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 19)
        Me.Button1.TabIndex = 31
        Me.Button1.Text = "Browse"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'tbSpectrumOutputFile
        '
        Me.tbSpectrumOutputFile.Location = New System.Drawing.Point(153, 180)
        Me.tbSpectrumOutputFile.Name = "tbSpectrumOutputFile"
        Me.tbSpectrumOutputFile.Size = New System.Drawing.Size(596, 20)
        Me.tbSpectrumOutputFile.TabIndex = 29
        Me.tbSpectrumOutputFile.Text = "D:\R1\Revisions\KIPZ\KIPZ_SIMPPLLE\Oct2011_Runs\Scenario1\BULLSIMEX_S1-simulation" & _
            "\textdata"
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(27, 180)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(120, 20)
        Me.Label5.TabIndex = 30
        Me.Label5.Text = "Spectrum Output File"
        '
        'tbSIMPPLLECellSize
        '
        Me.tbSIMPPLLECellSize.Location = New System.Drawing.Point(153, 206)
        Me.tbSIMPPLLECellSize.Name = "tbSIMPPLLECellSize"
        Me.tbSIMPPLLECellSize.Size = New System.Drawing.Size(56, 20)
        Me.tbSIMPPLLECellSize.TabIndex = 33
        Me.tbSIMPPLLECellSize.Text = "6"
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(27, 209)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(120, 31)
        Me.Label6.TabIndex = 32
        Me.Label6.Text = "SIMPPLLE Cell Size (Acres)"
        '
        'tbGISCellSize
        '
        Me.tbGISCellSize.Location = New System.Drawing.Point(153, 237)
        Me.tbGISCellSize.Name = "tbGISCellSize"
        Me.tbGISCellSize.Size = New System.Drawing.Size(56, 20)
        Me.tbGISCellSize.TabIndex = 35
        Me.tbGISCellSize.Text = "150"
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(27, 240)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(120, 31)
        Me.Label7.TabIndex = 34
        Me.Label7.Text = "GIS Cell Size (m)"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(215, 221)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(36, 29)
        Me.Button2.TabIndex = 36
        Me.Button2.Text = "?"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'cbbZones
        '
        Me.cbbZones.FormattingEnabled = True
        Me.cbbZones.Location = New System.Drawing.Point(288, 277)
        Me.cbbZones.Name = "cbbZones"
        Me.cbbZones.Size = New System.Drawing.Size(195, 21)
        Me.cbbZones.TabIndex = 37
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(285, 212)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(129, 13)
        Me.Label8.TabIndex = 38
        Me.Label8.Text = "Spectrum Report Defines:"
        '
        'rbOWN
        '
        Me.rbOWN.AutoSize = True
        Me.rbOWN.Checked = True
        Me.rbOWN.Location = New System.Drawing.Point(284, 230)
        Me.rbOWN.Name = "rbOWN"
        Me.rbOWN.Size = New System.Drawing.Size(75, 17)
        Me.rbOWN.TabIndex = 39
        Me.rbOWN.TabStop = True
        Me.rbOWN.Text = "Ownership"
        Me.rbOWN.UseVisualStyleBackColor = True
        '
        'rbSA
        '
        Me.rbSA.AutoSize = True
        Me.rbSA.Location = New System.Drawing.Point(284, 249)
        Me.rbSA.Name = "rbSA"
        Me.rbSA.Size = New System.Drawing.Size(85, 17)
        Me.rbSA.TabIndex = 40
        Me.rbSA.Text = "Special Area"
        Me.rbSA.UseVisualStyleBackColor = True
        '
        'SpectrumToSIMPPLLE
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(894, 320)
        Me.Controls.Add(Me.rbSA)
        Me.Controls.Add(Me.rbOWN)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.cbbZones)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.tbGISCellSize)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.tbSIMPPLLECellSize)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.tbSpectrumOutputFile)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.tbNTic)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.tbAcresByGeoArea)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.tbModelDefFolder)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CompileButton)
        Me.Controls.Add(Me.Label2)
        Me.Name = "SpectrumToSIMPPLLE"
        Me.Text = "SpectrumToSIMPPLLE"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents tbAcresByGeoArea As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents tbModelDefFolder As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CompileButton As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tbNTic As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents tbSpectrumOutputFile As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tbSIMPPLLECellSize As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tbGISCellSize As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents cbbZones As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents rbOWN As System.Windows.Forms.RadioButton
    Friend WithEvents rbSA As System.Windows.Forms.RadioButton
End Class
