Public Class clASCIIMapOverlay
    'set up to take 2 ARCGIS exported ASCII files and report summary statistics
    Dim NCells As Long
    Dim NStands As Integer
    Dim CellSize As Integer
    Dim CellStand(,) As Integer 'the cell's stand ID in the Stand ASCII
    Dim CellValue As Single 'the cell's value in the value ASCII
    Dim sNCols As Integer 'stand number of columms
    Dim sNRows As Integer 'stand number of rows
    Dim sxllcorner As Long
    Dim syllcorner As Long
    Dim bintype As String 'either continuous or discrete
    Dim NBins As Integer
    Dim BinVal() As Single
    Dim NODATA As Long

    'output stats
    Dim Average() As Single
    Dim Min() As Single
    Dim Max() As Single
    Dim Total() As Single
    Dim BinTotal(,) As Single 'area of the stand in the bin
    Dim BinPct(,) As Single 'percentage of the stand's area in the bin
    Dim NCellsInStand() As Integer 'by stand - how many cells are in the stand?
    Dim StandSize() As Single

    Public Sub RunOverlay(ByVal standfile As String, ByVal valuefile As String, ByVal binfile As String)
        ReadBins(binfile)
        ReadStandASCII(standfile)
        ReadValASCII(valuefile)
        WriteStats(DirName(valuefile) & "StandStatsOutput.txt")
    End Sub

    Private Sub ReadStandASCII(ByVal fname As String)
        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String
        Dim cs As Integer


        Dim MaxStandID As Long



        fnum = FreeFile()
        FileOpen(fnum, fname, OpenMode.Input)
        dummy = LineInput(fnum)
        arr = Split(dummy, " ")
        sNCols = arr(arr.Length - 1)
        dummy = LineInput(fnum)
        arr = Split(dummy, " ")
        sNRows = arr(arr.Length - 1)
        dummy = LineInput(fnum)
        arr = Split(dummy, " ")
        sxllcorner = arr(arr.Length - 1)
        dummy = LineInput(fnum)
        arr = Split(dummy, " ")
        syllcorner = arr(arr.Length - 1)
        dummy = LineInput(fnum)
        arr = Split(dummy, " ")
        CellSize = arr(arr.Length - 1)
        dummy = LineInput(fnum)
        arr = Split(dummy, " ")
        NODATA = arr(arr.Length - 1)
        MaxStandID = NODATA
        ReDim CellStand(sNCols, sNRows)
        For j As Integer = 1 To sNRows
            dummy = LineInput(fnum)
            arr = Split(dummy, " ")
            For c As Integer = 1 To sNCols
                cs = arr(c - 1)
                CellStand(c, j) = cs 'arr(c - 1)
                MaxStandID = Math.Max(MaxStandID, cs) 'CellStand(c, j))
            Next
        Next
        FileClose(fnum)
        NStands = MaxStandID

    End Sub

    Private Sub ReadValASCII(ByVal fname As String)
        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String
        Dim nrows As Integer
        Dim ncols As Integer
        Dim xllcorner As Long
        Dim yllcorner As Long
        Dim xShift As Integer
        Dim yShift As Integer
        Dim kstand As Integer
        Dim kbin As Integer

        Dim Ndummyrows As Integer 'at the beginning of the file, there may be some extra rows that don't line up




        fnum = FreeFile()
        FileOpen(fnum, fname, OpenMode.Input)
        dummy = LineInput(fnum)
        arr = Split(dummy, " ")
        ncols = arr(arr.Length - 1)
        dummy = LineInput(fnum)
        arr = Split(dummy, " ")
        nrows = arr(arr.Length - 1)
        dummy = LineInput(fnum)
        arr = Split(dummy, " ")
        xllcorner = arr(arr.Length - 1)
        dummy = LineInput(fnum)
        arr = Split(dummy, " ")
        yllcorner = arr(arr.Length - 1)
        dummy = LineInput(fnum)
        arr = Split(dummy, " ")
        CellSize = arr(arr.Length - 1)
        dummy = LineInput(fnum)
        arr = Split(dummy, " ")
        NODATA = arr(arr.Length - 1)

        'figure xshift and yshift
        xShift = Math.Abs((sxllcorner - xllcorner) / CellSize)
        yShift = Math.Abs((syllcorner - yllcorner) / CellSize)
        'calculate the number of dummy rows
        Ndummyrows = nrows - sNRows - yShift
        ReDim Average(NStands), Min(NStands), Total(NStands), Max(NStands), BinTotal(NStands, NBins), BinPct(NStands, NBins), NCellsInStand(NStands), StandSize(NStands)
        For jstand As Integer = 1 To NStands
            Min(jstand) = 99999999
            Max(jstand) = -99999999
        Next

        'ReDim CellValue(sNCols, sNRows)
        'read in dummy rows
        For j As Integer = 1 To Ndummyrows
            dummy = LineInput(fnum)
        Next
        For j As Integer = 1 To sNRows
            dummy = LineInput(fnum)
            arr = Split(dummy, " ")
            For c As Integer = 1 To sNCols
                CellValue = arr(c + xShift - 1)
                kstand = CellStand(c, j)
                If kstand <> NODATA Then
                    Total(kstand) = Total(kstand) + CellValue
                    Max(kstand) = Math.Max(Max(kstand), CellValue)
                    Min(kstand) = Math.Min(Min(kstand), CellValue)
                    kbin = DetermineBin(CellValue, BinVal)
                    BinTotal(kstand, kbin) = BinTotal(kstand, kbin) + CellSize ^ 2
                    StandSize(kstand) = StandSize(kstand) + CellSize ^ 2
                    NCellsInStand(kstand) = NCellsInStand(kstand) + 1
                End If

            Next
        Next
        FileClose(fnum)

  
    End Sub
    Private Sub ReadBins(ByVal fname As String)
        Dim fnum As Integer
        Dim dummy As String
        NBins = 0

        fnum = FreeFile()
        FileOpen(fnum, fname, OpenMode.Input)
        dummy = LineInput(fnum) 'header
        bintype = Trim(LineInput(fnum))
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            NBins = NBins + 1
            ReDim Preserve BinVal(NBins)
            BinVal(NBins) = dummy
        Loop


        FileClose(fnum)
    End Sub


    Private Sub WriteStats(ByVal outfile As String)
        Dim fnum As Integer
        Dim dummy As String
        fnum = FreeFile()

        FileOpen(fnum, outfile, OpenMode.Output)
        dummy = "Stand,Size,Pixels,Avg,Min,Max"
        For jbin As Integer = 1 To NBins
            dummy = dummy & ",Bin_" & BinVal(jbin) & "_Pct"
        Next
        PrintLine(fnum, dummy)
        For jstand As Integer = 1 To NStands
            dummy = jstand & "," & StandSize(jstand) & "," & NCellsInStand(jstand) & "," & Math.Round(Total(jstand) / NCellsInStand(jstand), 2) & "," & Min(jstand) & "," & Max(jstand)
            For jbin As Integer = 1 To NBins
                dummy = dummy & "," & Math.Round(BinTotal(jstand, jbin) / Standsize(jstand), 2)
            Next
            PrintLine(fnum, dummy)
        Next
        FileClose(fnum)

    End Sub
    Private Function DetermineBin(ByVal value As Single, ByVal Bins() As Single) As Integer
        ' bin value
        DetermineBin = 0
        For j As Integer = 1 To Bins.Length - 1
            If bintype = "continuous" Then
                'assumes a continuous bin value indicates the maximum value of the bin. Therefore, find the first bin where the value is less than the 
                If value < Bins(j) Then
                    DetermineBin = j
                    Exit For
                End If
            ElseIf bintype = "discrete" Then
                'a discrete bin must match the value exactly
                If value = Bins(j) Then
                    DetermineBin = j
                    Exit For
                End If
            End If
        Next
    End Function





End Class
