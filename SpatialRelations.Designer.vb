<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SpatialRelations
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tbColumnDefFN = New System.Windows.Forms.TextBox
        Me.tbPolygonTableFN = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.tbWindDirectionFN = New System.Windows.Forms.TextBox
        Me.tbWindSpeedFN = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.tbMinExtremeWindSpeed = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.Label6 = New System.Windows.Forms.Label
        Me.Button2 = New System.Windows.Forms.Button
        Me.cbbDefaultWindDirection = New System.Windows.Forms.ComboBox
        Me.Button3 = New System.Windows.Forms.Button
        Me.Label7 = New System.Windows.Forms.Label
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.Button7 = New System.Windows.Forms.Button
        Me.Label8 = New System.Windows.Forms.Label
        Me.Button8 = New System.Windows.Forms.Button
        Me.Button9 = New System.Windows.Forms.Button
        Me.Label9 = New System.Windows.Forms.Label
        Me.tbSpatialrelateFN = New System.Windows.Forms.TextBox
        Me.Button10 = New System.Windows.Forms.Button
        Me.Label10 = New System.Windows.Forms.Label
        Me.tbCoordinatesFN = New System.Windows.Forms.TextBox
        Me.Button11 = New System.Windows.Forms.Button
        Me.Button12 = New System.Windows.Forms.Button
        Me.cbSplitGAs = New System.Windows.Forms.CheckBox
        Me.tbGAField = New System.Windows.Forms.TextBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cbRandomVeg = New System.Windows.Forms.CheckBox
        Me.Button13 = New System.Windows.Forms.Button
        Me.Button14 = New System.Windows.Forms.Button
        Me.Button15 = New System.Windows.Forms.Button
        Me.Button16 = New System.Windows.Forms.Button
        Me.rb8WayAdj = New System.Windows.Forms.RadioButton
        Me.rb4WayAdj = New System.Windows.Forms.RadioButton
        Me.rbHexAdj = New System.Windows.Forms.RadioButton
        Me.Label11 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.rbKeaneFireSpread = New System.Windows.Forms.RadioButton
        Me.rbDefaultFireSpread = New System.Windows.Forms.RadioButton
        Me.Button17 = New System.Windows.Forms.Button
        Me.Button19 = New System.Windows.Forms.Button
        Me.tbWindSpeed = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.Button18 = New System.Windows.Forms.Button
        Me.rb9xGeneralize = New System.Windows.Forms.RadioButton
        Me.rb4xGeneralize = New System.Windows.Forms.RadioButton
        Me.rbAllPolys = New System.Windows.Forms.RadioButton
        Me.Button20 = New System.Windows.Forms.Button
        Me.Button21 = New System.Windows.Forms.Button
        Me.Button22 = New System.Windows.Forms.Button
        Me.Button23 = New System.Windows.Forms.Button
        Me.tbWindDir = New System.Windows.Forms.TextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.Button24 = New System.Windows.Forms.Button
        Me.cbTorus = New System.Windows.Forms.CheckBox
        Me.Button25 = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'tbColumnDefFN
        '
        Me.tbColumnDefFN.Location = New System.Drawing.Point(118, 72)
        Me.tbColumnDefFN.Name = "tbColumnDefFN"
        Me.tbColumnDefFN.Size = New System.Drawing.Size(462, 20)
        Me.tbColumnDefFN.TabIndex = 0
        '
        'tbPolygonTableFN
        '
        Me.tbPolygonTableFN.Location = New System.Drawing.Point(118, 98)
        Me.tbPolygonTableFN.Name = "tbPolygonTableFN"
        Me.tbPolygonTableFN.Size = New System.Drawing.Size(462, 20)
        Me.tbPolygonTableFN.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(28, 72)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(84, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Column Def File:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(28, 98)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Polygon Table:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(28, 297)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(136, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Wind Speed File (Optional):"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(28, 261)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(147, 13)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Wind Direction File (Optional):"
        '
        'tbWindDirectionFN
        '
        Me.tbWindDirectionFN.Location = New System.Drawing.Point(118, 274)
        Me.tbWindDirectionFN.Name = "tbWindDirectionFN"
        Me.tbWindDirectionFN.Size = New System.Drawing.Size(462, 20)
        Me.tbWindDirectionFN.TabIndex = 6
        Me.tbWindDirectionFN.Text = "<not used>"
        '
        'tbWindSpeedFN
        '
        Me.tbWindSpeedFN.Location = New System.Drawing.Point(118, 313)
        Me.tbWindSpeedFN.Name = "tbWindSpeedFN"
        Me.tbWindSpeedFN.Size = New System.Drawing.Size(462, 20)
        Me.tbWindSpeedFN.TabIndex = 7
        Me.tbWindSpeedFN.Text = "<not used>"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 369)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(130, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Min Extreme Wind Speed:"
        '
        'tbMinExtremeWindSpeed
        '
        Me.tbMinExtremeWindSpeed.Location = New System.Drawing.Point(161, 366)
        Me.tbMinExtremeWindSpeed.Name = "tbMinExtremeWindSpeed"
        Me.tbMinExtremeWindSpeed.Size = New System.Drawing.Size(22, 20)
        Me.tbMinExtremeWindSpeed.TabIndex = 9
        Me.tbMinExtremeWindSpeed.Text = "0"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(204, 366)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(23, 19)
        Me.Button1.TabIndex = 10
        Me.Button1.Text = "?"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(25, 406)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(117, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Default Wind Direction:"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(204, 400)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(23, 19)
        Me.Button2.TabIndex = 13
        Me.Button2.Text = "?"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'cbbDefaultWindDirection
        '
        Me.cbbDefaultWindDirection.FormattingEnabled = True
        Me.cbbDefaultWindDirection.Items.AddRange(New Object() {"N", "W", "E", "S", "NE", "NW", "SW", "SE"})
        Me.cbbDefaultWindDirection.Location = New System.Drawing.Point(141, 402)
        Me.cbbDefaultWindDirection.Name = "cbbDefaultWindDirection"
        Me.cbbDefaultWindDirection.Size = New System.Drawing.Size(57, 21)
        Me.cbbDefaultWindDirection.TabIndex = 14
        Me.cbbDefaultWindDirection.Text = "W"
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(328, 384)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(74, 39)
        Me.Button3.TabIndex = 15
        Me.Button3.Text = "GO"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(70, 19)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(567, 26)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "Write SIMPPLLE Input Files (.spatialrealate, .attributesall)"
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(586, 73)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 19)
        Me.Button4.TabIndex = 17
        Me.Button4.Text = "Browse"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(586, 99)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(75, 19)
        Me.Button5.TabIndex = 18
        Me.Button5.Text = "Browse"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(586, 274)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(75, 19)
        Me.Button6.TabIndex = 19
        Me.Button6.Text = "Browse"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(586, 314)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(75, 19)
        Me.Button7.TabIndex = 20
        Me.Button7.Text = "Browse"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(25, 29)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(288, 26)
        Me.Label8.TabIndex = 21
        Me.Label8.Text = "Update Existing spatialrelate"
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(328, 35)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(23, 19)
        Me.Button8.TabIndex = 22
        Me.Button8.Text = "?"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(565, 77)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(75, 19)
        Me.Button9.TabIndex = 25
        Me.Button9.Text = "Browse"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(7, 76)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(82, 13)
        Me.Label9.TabIndex = 24
        Me.Label9.Text = "spatialrelate file:"
        '
        'tbSpatialrelateFN
        '
        Me.tbSpatialrelateFN.Location = New System.Drawing.Point(97, 76)
        Me.tbSpatialrelateFN.Name = "tbSpatialrelateFN"
        Me.tbSpatialrelateFN.Size = New System.Drawing.Size(462, 20)
        Me.tbSpatialrelateFN.TabIndex = 23
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(565, 113)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(75, 19)
        Me.Button10.TabIndex = 28
        Me.Button10.Text = "Browse"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(7, 112)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(81, 13)
        Me.Label10.TabIndex = 27
        Me.Label10.Text = "coordinates file:"
        '
        'tbCoordinatesFN
        '
        Me.tbCoordinatesFN.Location = New System.Drawing.Point(97, 112)
        Me.tbCoordinatesFN.Name = "tbCoordinatesFN"
        Me.tbCoordinatesFN.Size = New System.Drawing.Size(462, 20)
        Me.tbCoordinatesFN.TabIndex = 26
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(318, 138)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(44, 39)
        Me.Button11.TabIndex = 29
        Me.Button11.Text = "GO"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button12
        '
        Me.Button12.Location = New System.Drawing.Point(667, 72)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(52, 20)
        Me.Button12.TabIndex = 30
        Me.Button12.Text = "Define"
        Me.Button12.UseVisualStyleBackColor = True
        '
        'cbSplitGAs
        '
        Me.cbSplitGAs.AutoSize = True
        Me.cbSplitGAs.Location = New System.Drawing.Point(456, 384)
        Me.cbSplitGAs.Name = "cbSplitGAs"
        Me.cbSplitGAs.Size = New System.Drawing.Size(180, 17)
        Me.cbSplitGAs.TabIndex = 31
        Me.cbSplitGAs.Text = "Split Geographic Areas (optional)"
        Me.cbSplitGAs.UseVisualStyleBackColor = True
        '
        'tbGAField
        '
        Me.tbGAField.Enabled = False
        Me.tbGAField.Location = New System.Drawing.Point(458, 403)
        Me.tbGAField.Name = "tbGAField"
        Me.tbGAField.Size = New System.Drawing.Size(177, 20)
        Me.tbGAField.TabIndex = 32
        Me.tbGAField.Text = "<set when split GA is checked>"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button11)
        Me.GroupBox1.Controls.Add(Me.Button10)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.tbCoordinatesFN)
        Me.GroupBox1.Controls.Add(Me.Button9)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.tbSpatialrelateFN)
        Me.GroupBox1.Controls.Add(Me.Button8)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Location = New System.Drawing.Point(31, 455)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(665, 185)
        Me.GroupBox1.TabIndex = 33
        Me.GroupBox1.TabStop = False
        '
        'cbRandomVeg
        '
        Me.cbRandomVeg.AutoSize = True
        Me.cbRandomVeg.Location = New System.Drawing.Point(120, 124)
        Me.cbRandomVeg.Name = "cbRandomVeg"
        Me.cbRandomVeg.Size = New System.Drawing.Size(227, 17)
        Me.cbRandomVeg.TabIndex = 34
        Me.cbRandomVeg.Text = "Optional: Random Vegetation Assignments"
        Me.cbRandomVeg.UseVisualStyleBackColor = True
        '
        'Button13
        '
        Me.Button13.Location = New System.Drawing.Point(353, 122)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(23, 19)
        Me.Button13.TabIndex = 35
        Me.Button13.Text = "?"
        Me.Button13.UseVisualStyleBackColor = True
        '
        'Button14
        '
        Me.Button14.Location = New System.Drawing.Point(6, 14)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(58, 31)
        Me.Button14.TabIndex = 36
        Me.Button14.Text = "Help"
        Me.Button14.UseVisualStyleBackColor = True
        '
        'Button15
        '
        Me.Button15.Location = New System.Drawing.Point(427, 382)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(23, 19)
        Me.Button15.TabIndex = 37
        Me.Button15.Text = "?"
        Me.Button15.UseVisualStyleBackColor = True
        '
        'Button16
        '
        Me.Button16.Location = New System.Drawing.Point(486, 148)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(23, 19)
        Me.Button16.TabIndex = 39
        Me.Button16.Text = "?"
        Me.Button16.UseVisualStyleBackColor = True
        '
        'rb8WayAdj
        '
        Me.rb8WayAdj.AutoSize = True
        Me.rb8WayAdj.Checked = True
        Me.rb8WayAdj.Location = New System.Drawing.Point(369, 168)
        Me.rb8WayAdj.Name = "rb8WayAdj"
        Me.rb8WayAdj.Size = New System.Drawing.Size(147, 17)
        Me.rb8WayAdj.TabIndex = 40
        Me.rb8WayAdj.TabStop = True
        Me.rb8WayAdj.Text = "8-way Adjacency (default)"
        Me.rb8WayAdj.UseVisualStyleBackColor = True
        '
        'rb4WayAdj
        '
        Me.rb4WayAdj.AutoSize = True
        Me.rb4WayAdj.Enabled = False
        Me.rb4WayAdj.Location = New System.Drawing.Point(369, 185)
        Me.rb4WayAdj.Name = "rb4WayAdj"
        Me.rb4WayAdj.Size = New System.Drawing.Size(106, 17)
        Me.rb4WayAdj.TabIndex = 41
        Me.rb4WayAdj.Text = "4-way Adjacency"
        Me.rb4WayAdj.UseVisualStyleBackColor = True
        '
        'rbHexAdj
        '
        Me.rbHexAdj.AutoSize = True
        Me.rbHexAdj.Enabled = False
        Me.rbHexAdj.Location = New System.Drawing.Point(369, 202)
        Me.rbHexAdj.Name = "rbHexAdj"
        Me.rbHexAdj.Size = New System.Drawing.Size(129, 17)
        Me.rbHexAdj.TabIndex = 42
        Me.rbHexAdj.Text = "Hexagonal Adjacency"
        Me.rbHexAdj.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(359, 149)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(121, 16)
        Me.Label11.TabIndex = 43
        Me.Label11.Text = "Adjacency Type"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Button25)
        Me.GroupBox3.Controls.Add(Me.cbTorus)
        Me.GroupBox3.Controls.Add(Me.rbKeaneFireSpread)
        Me.GroupBox3.Controls.Add(Me.rbDefaultFireSpread)
        Me.GroupBox3.Controls.Add(Me.Button17)
        Me.GroupBox3.Location = New System.Drawing.Point(564, 154)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(148, 79)
        Me.GroupBox3.TabIndex = 52
        Me.GroupBox3.TabStop = False
        '
        'rbKeaneFireSpread
        '
        Me.rbKeaneFireSpread.AutoSize = True
        Me.rbKeaneFireSpread.Location = New System.Drawing.Point(9, 34)
        Me.rbKeaneFireSpread.Name = "rbKeaneFireSpread"
        Me.rbKeaneFireSpread.Size = New System.Drawing.Size(113, 17)
        Me.rbKeaneFireSpread.TabIndex = 51
        Me.rbKeaneFireSpread.Text = "Keane Fire Spread"
        Me.rbKeaneFireSpread.UseVisualStyleBackColor = True
        '
        'rbDefaultFireSpread
        '
        Me.rbDefaultFireSpread.AutoSize = True
        Me.rbDefaultFireSpread.Checked = True
        Me.rbDefaultFireSpread.Location = New System.Drawing.Point(9, 14)
        Me.rbDefaultFireSpread.Name = "rbDefaultFireSpread"
        Me.rbDefaultFireSpread.Size = New System.Drawing.Size(116, 17)
        Me.rbDefaultFireSpread.TabIndex = 49
        Me.rbDefaultFireSpread.TabStop = True
        Me.rbDefaultFireSpread.Text = "Default Fire Spread"
        Me.rbDefaultFireSpread.UseVisualStyleBackColor = True
        '
        'Button17
        '
        Me.Button17.Location = New System.Drawing.Point(198, 37)
        Me.Button17.Name = "Button17"
        Me.Button17.Size = New System.Drawing.Size(23, 19)
        Me.Button17.TabIndex = 48
        Me.Button17.Text = "?"
        Me.Button17.UseVisualStyleBackColor = True
        '
        'Button19
        '
        Me.Button19.Location = New System.Drawing.Point(204, 344)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(23, 19)
        Me.Button19.TabIndex = 55
        Me.Button19.Text = "?"
        Me.Button19.UseVisualStyleBackColor = True
        '
        'tbWindSpeed
        '
        Me.tbWindSpeed.Location = New System.Drawing.Point(161, 344)
        Me.tbWindSpeed.Name = "tbWindSpeed"
        Me.tbWindSpeed.Size = New System.Drawing.Size(22, 20)
        Me.tbWindSpeed.TabIndex = 54
        Me.tbWindSpeed.Text = "5"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(12, 347)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(135, 13)
        Me.Label12.TabIndex = 53
        Me.Label12.Text = "Default Wind Speed (mph):"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Button18)
        Me.GroupBox4.Controls.Add(Me.rb9xGeneralize)
        Me.GroupBox4.Controls.Add(Me.rb4xGeneralize)
        Me.GroupBox4.Controls.Add(Me.rbAllPolys)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(224, 149)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(109, 70)
        Me.GroupBox4.TabIndex = 56
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Generalization"
        '
        'Button18
        '
        Me.Button18.Location = New System.Drawing.Point(86, -2)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(23, 19)
        Me.Button18.TabIndex = 59
        Me.Button18.Text = "?"
        Me.Button18.UseVisualStyleBackColor = True
        '
        'rb9xGeneralize
        '
        Me.rb9xGeneralize.AutoSize = True
        Me.rb9xGeneralize.Location = New System.Drawing.Point(8, 49)
        Me.rb9xGeneralize.Name = "rb9xGeneralize"
        Me.rb9xGeneralize.Size = New System.Drawing.Size(89, 17)
        Me.rb9xGeneralize.TabIndex = 4
        Me.rb9xGeneralize.TabStop = True
        Me.rb9xGeneralize.Text = "9x Generalize"
        Me.rb9xGeneralize.UseVisualStyleBackColor = True
        '
        'rb4xGeneralize
        '
        Me.rb4xGeneralize.AutoSize = True
        Me.rb4xGeneralize.Location = New System.Drawing.Point(8, 32)
        Me.rb4xGeneralize.Name = "rb4xGeneralize"
        Me.rb4xGeneralize.Size = New System.Drawing.Size(89, 17)
        Me.rb4xGeneralize.TabIndex = 3
        Me.rb4xGeneralize.TabStop = True
        Me.rb4xGeneralize.Text = "4x Generalize"
        Me.rb4xGeneralize.UseVisualStyleBackColor = True
        '
        'rbAllPolys
        '
        Me.rbAllPolys.AutoSize = True
        Me.rbAllPolys.Checked = True
        Me.rbAllPolys.Location = New System.Drawing.Point(8, 16)
        Me.rbAllPolys.Name = "rbAllPolys"
        Me.rbAllPolys.Size = New System.Drawing.Size(77, 17)
        Me.rbAllPolys.TabIndex = 2
        Me.rbAllPolys.TabStop = True
        Me.rbAllPolys.Text = "All (default)"
        Me.rbAllPolys.UseVisualStyleBackColor = True
        '
        'Button20
        '
        Me.Button20.Location = New System.Drawing.Point(20, 168)
        Me.Button20.Name = "Button20"
        Me.Button20.Size = New System.Drawing.Size(100, 22)
        Me.Button20.TabIndex = 57
        Me.Button20.Text = "Convert To Raw"
        Me.Button20.UseVisualStyleBackColor = True
        '
        'Button21
        '
        Me.Button21.Location = New System.Drawing.Point(103, 182)
        Me.Button21.Name = "Button21"
        Me.Button21.Size = New System.Drawing.Size(23, 19)
        Me.Button21.TabIndex = 58
        Me.Button21.Text = "?"
        Me.Button21.UseVisualStyleBackColor = True
        '
        'Button22
        '
        Me.Button22.Location = New System.Drawing.Point(20, 207)
        Me.Button22.Name = "Button22"
        Me.Button22.Size = New System.Drawing.Size(100, 40)
        Me.Button22.TabIndex = 59
        Me.Button22.Text = "DEM From spatialrelate"
        Me.Button22.UseVisualStyleBackColor = True
        '
        'Button23
        '
        Me.Button23.Location = New System.Drawing.Point(103, 228)
        Me.Button23.Name = "Button23"
        Me.Button23.Size = New System.Drawing.Size(23, 19)
        Me.Button23.TabIndex = 60
        Me.Button23.Text = "?"
        Me.Button23.UseVisualStyleBackColor = True
        '
        'tbWindDir
        '
        Me.tbWindDir.Location = New System.Drawing.Point(161, 426)
        Me.tbWindDir.Name = "tbWindDir"
        Me.tbWindDir.Size = New System.Drawing.Size(37, 20)
        Me.tbWindDir.TabIndex = 61
        Me.tbWindDir.Text = "270"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(43, 429)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(112, 13)
        Me.Label13.TabIndex = 63
        Me.Label13.Text = "Wind degrees (1-360):"
        '
        'Button24
        '
        Me.Button24.Location = New System.Drawing.Point(204, 425)
        Me.Button24.Name = "Button24"
        Me.Button24.Size = New System.Drawing.Size(23, 19)
        Me.Button24.TabIndex = 64
        Me.Button24.Text = "?"
        Me.Button24.UseVisualStyleBackColor = True
        '
        'cbTorus
        '
        Me.cbTorus.AutoSize = True
        Me.cbTorus.Location = New System.Drawing.Point(16, 53)
        Me.cbTorus.Name = "cbTorus"
        Me.cbTorus.Size = New System.Drawing.Size(105, 17)
        Me.cbTorus.TabIndex = 52
        Me.cbTorus.Text = "Wrap Adjacency"
        Me.cbTorus.UseVisualStyleBackColor = True
        '
        'Button25
        '
        Me.Button25.Location = New System.Drawing.Point(119, 51)
        Me.Button25.Name = "Button25"
        Me.Button25.Size = New System.Drawing.Size(23, 19)
        Me.Button25.TabIndex = 65
        Me.Button25.Text = "?"
        Me.Button25.UseVisualStyleBackColor = True
        '
        'SpatialRelations
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(724, 652)
        Me.Controls.Add(Me.tbWindDir)
        Me.Controls.Add(Me.Button24)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Button23)
        Me.Controls.Add(Me.Button22)
        Me.Controls.Add(Me.Button21)
        Me.Controls.Add(Me.Button20)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.Button19)
        Me.Controls.Add(Me.tbWindSpeed)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.rbHexAdj)
        Me.Controls.Add(Me.rb4WayAdj)
        Me.Controls.Add(Me.rb8WayAdj)
        Me.Controls.Add(Me.Button16)
        Me.Controls.Add(Me.Button15)
        Me.Controls.Add(Me.Button14)
        Me.Controls.Add(Me.Button13)
        Me.Controls.Add(Me.cbRandomVeg)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.tbGAField)
        Me.Controls.Add(Me.cbSplitGAs)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.cbbDefaultWindDirection)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.tbMinExtremeWindSpeed)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.tbWindSpeedFN)
        Me.Controls.Add(Me.tbWindDirectionFN)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tbPolygonTableFN)
        Me.Controls.Add(Me.tbColumnDefFN)
        Me.Name = "SpatialRelations"
        Me.Text = "Generate SIMPPLLE Input Files"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tbColumnDefFN As System.Windows.Forms.TextBox
    Friend WithEvents tbPolygonTableFN As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tbWindDirectionFN As System.Windows.Forms.TextBox
    Friend WithEvents tbWindSpeedFN As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tbMinExtremeWindSpeed As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents cbbDefaultWindDirection As System.Windows.Forms.ComboBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents tbSpatialrelateFN As System.Windows.Forms.TextBox
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents tbCoordinatesFN As System.Windows.Forms.TextBox
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents cbSplitGAs As System.Windows.Forms.CheckBox
    Friend WithEvents tbGAField As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cbRandomVeg As System.Windows.Forms.CheckBox
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEvents rb8WayAdj As System.Windows.Forms.RadioButton
    Friend WithEvents rb4WayAdj As System.Windows.Forms.RadioButton
    Friend WithEvents rbHexAdj As System.Windows.Forms.RadioButton
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents rbKeaneFireSpread As System.Windows.Forms.RadioButton
    Friend WithEvents rbDefaultFireSpread As System.Windows.Forms.RadioButton
    Friend WithEvents Button17 As System.Windows.Forms.Button
    Friend WithEvents Button19 As System.Windows.Forms.Button
    Friend WithEvents tbWindSpeed As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents rb9xGeneralize As System.Windows.Forms.RadioButton
    Friend WithEvents rb4xGeneralize As System.Windows.Forms.RadioButton
    Friend WithEvents rbAllPolys As System.Windows.Forms.RadioButton
    Friend WithEvents Button20 As System.Windows.Forms.Button
    Friend WithEvents Button21 As System.Windows.Forms.Button
    Friend WithEvents Button18 As System.Windows.Forms.Button
    Friend WithEvents Button22 As System.Windows.Forms.Button
    Friend WithEvents Button23 As System.Windows.Forms.Button
    Friend WithEvents tbWindDir As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Button24 As System.Windows.Forms.Button
    Friend WithEvents Button25 As System.Windows.Forms.Button
    Friend WithEvents cbTorus As System.Windows.Forms.CheckBox
End Class
