<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ProcessSIMPPLLEDisturbances
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button4 = New System.Windows.Forms.Button
        Me.tbInitCondFiles = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Button5 = New System.Windows.Forms.Button
        Me.tbBatchFile = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.CompileButton = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(753, 73)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 19)
        Me.Button4.TabIndex = 25
        Me.Button4.Text = "Browse"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'tbInitCondFiles
        '
        Me.tbInitCondFiles.Location = New System.Drawing.Point(138, 72)
        Me.tbInitCondFiles.Name = "tbInitCondFiles"
        Me.tbInitCondFiles.Size = New System.Drawing.Size(596, 20)
        Me.tbInitCondFiles.TabIndex = 23
        Me.tbInitCondFiles.Text = "D:\R1\Revisions\KIPZ\KIPZ_SIMPPLLE\Oct2011_Runs\Scenario1\BULLSIMEX_S1-simulation" & _
            "\textdata"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(12, 61)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(120, 31)
        Me.Label4.TabIndex = 24
        Me.Label4.Text = "Initial Conditions file"
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(753, 113)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(75, 19)
        Me.Button5.TabIndex = 22
        Me.Button5.Text = "Browse"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'tbBatchFile
        '
        Me.tbBatchFile.Location = New System.Drawing.Point(138, 112)
        Me.tbBatchFile.Name = "tbBatchFile"
        Me.tbBatchFile.Size = New System.Drawing.Size(596, 20)
        Me.tbBatchFile.TabIndex = 20
        Me.tbBatchFile.Text = "D:\R1\Revisions\KIPZ\KIPZ_SIMPPLLE\Oct2011_Runs\Scenario1\BULLSIMEX_S1-simulation" & _
            "\textdata"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(12, 101)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 31)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Batch File for Runs To Process"
        '
        'CompileButton
        '
        Me.CompileButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CompileButton.Location = New System.Drawing.Point(372, 172)
        Me.CompileButton.Name = "CompileButton"
        Me.CompileButton.Size = New System.Drawing.Size(96, 32)
        Me.CompileButton.TabIndex = 19
        Me.CompileButton.Text = "GO"
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(238, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(496, 40)
        Me.Label2.TabIndex = 26
        Me.Label2.Text = "Process SIMPPLLE Disturbances"
        '
        'ProcessSIMPPLLEDisturbances
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(841, 264)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.tbInitCondFiles)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.tbBatchFile)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CompileButton)
        Me.Name = "ProcessSIMPPLLEDisturbances"
        Me.Text = "ProcessSIMPPLLEDisturbances"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents tbInitCondFiles As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents tbBatchFile As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CompileButton As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
