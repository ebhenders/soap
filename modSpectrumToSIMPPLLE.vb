


Module modSpectrumToSIMPPLLE
    Public SpecToSIMModDefFolder As String
    Public AAByGeoAreaFN As String
    Public SpectrumOutputFN As String


    'for tracking suitability
    Dim NOWN As Integer
    Dim NSpecialArea As Integer
    Dim TrtSpecialAreaSuit(,) As Integer '0/1, indexed by NTrt, n OWNERSHIP/SUITABILITY
    Dim TrtSpecialAreaCode() As String 'the actual code in the input file BY TREATMENT
    Dim TrtOWNSuit(,) As Integer '0/1 indexed by NTrt,  NSuit
    Dim TrtOWNCode() As String 'actual code in the treatment input file BY TREATMENT
    Dim TrtSizeSuit(,) As Integer '0/1 indexed by NTrt, NSize
    Dim TrtHTGCode() As String 'actual HTG code() in the treatment input file BY TREATMENT
    Dim TrtHTGSuit(,) As Byte '0/1 indexed by Ntrt,NHTG
    Dim TrtDensityString() As String 'the string to put in the density field - may include multiple codes
    Dim TrtRoadStatus() As String
    Dim TrtTimestepsBeforeFollowUp() As Integer
    Dim TrtFollowUpTrt() As String
    Dim TrtAllowReTreat() As String
    Dim TrtTimestepsBeforeRetreat() As Integer
    Dim TrtFollowUpTrtName() As String
    Dim TrtPreviousProcess() As String
    Dim TrtMinSize() As Single
    Dim TrtMaxSize() As Single
    Dim TrtExceptionString() As String
    Dim OwnName() As String
    Dim SpecialAreaName() As String

    'need to know the following information
    Dim SpecSpeciesLUTFN As String
    Dim SpecSizeLUTFN As String
    Dim SpecHTGLUTFN As String
    Dim GALUTFN As String
    Dim TrtLUTFN As String
    Dim HTGXWFN As String 'SIMPPLLE_HTG,SPECTRUM_ECOGROUP
    'Dim HTGLUTFN As String 'SPECTRUM_ECOGROUP
    Dim HTG_Species_XWFN As String 'SPECTRUM_SPECIES, SPECTRUM_ECOGROUP
    'Dim TrtDefLUTFN As String
    Dim RptLUTFN As String
    Dim SpecialAreaLUTFN As String
    Dim SuitLUTFN As String
    Dim FieldNameLUTFN As String
    Dim SizeXWFN As String 'crosswalk from Spectrum to SIMPPLLE 
    Dim ZonePrint As String
    'Dim SpeciesXWFN As String 'crosswalk from Spectrum to SIMPPLLE


    'Dim SpecHTGOWNL5L6TrtAcres(,,,,) As Single 'by HTG,OWNERSHIP, level 5, level 6, treatment  - total acres in each category - treatment defines suitable acres available for the treatment
    ''Dim SpecSAL5L6SpecSuitAcres(,,,,) As Single 'by specialarea, level 5, level 6, special area, suited  - total acres in each category - treatment defines suitable acres available for the treatment
    'Dim SpecHTGOWNL5L6GATrtAcres(,,,,,) As Single 'by HTG,OWNERSHIP, l5, l6, GA, treatment - to portion out treatment amounts

    Dim SpecOWNL5L6TrtAcres(,,,) As Single 'by OWNERSHIP, level 5, level 6, treatment  - total acres in each category - treatment defines suitable acres available for the treatment
    'Dim SpecSAL5L6SpecSuitAcres(,,,,) As Single 'by specialarea, level 5, level 6, special area, suited  - total acres in each category - treatment defines suitable acres available for the treatment
    Dim SpecOWNL5L6GATrtAcres(,,,,) As Single 'by OWNERSHIP, l5, l6, GA, treatment - to portion out treatment amounts


    'Dim SpecSAL5L6Acres(,,) As Single 'by specialarea, level 5, level 6,   - total acres in each category - treatment defines suitable acres available for the treatment
    'Dim SpecSAL5L6GAAcres(,,,) As Single 'by specialarea, l5, l6, GA - to portion out treatment amounts

    Dim SpecOWNL5L6TrtAcresTreated(,,,,) As Single 'by HTG,OWNERSHIP, l5, l6, treatment type,decade - the spectrum solution
    Dim SIMPPLLECellSize As Integer 'acres (integer) of the SIMPPLLE cell 
    Dim GISCellSize As Single 'in meters per side, the size of the cell size in GIS
    Dim AcreAdjFactor As Double 'based on differences between GIS cell size and SIMPPLLE cell size
    Dim CellAdjFactor As Single 'used to reconcile differences between round-acre SIMPPLLE sizes and GIS actual sizes.

    Dim GAL5AcresTreated(,,,) As Single 'by GA, L5,treatment type, tic - just an accounting variable

    Dim SpecSpeciesName() As String 'by nl5 - store the names
    Dim SpecSizeName() As String 'by NL6
    Dim GAName() As String
    Dim SpecHTGName() As String
    Dim SpecTrtName() As String
    Dim SpecSpeciesEcoGroup() As String 'For each spectrum species, the associated eco group
    Dim SIMPPLLETrtName() As String
    Dim SIMPPLLETrtNameList() As String 'bank of just unique names
    Dim RptName() As String
    'Dim RptSpecialArea() As String
    Dim RptOwnership() As String
    Dim RptL5() As String
    Dim RptL6() As String
    Dim RptTrt() As String

    Dim SIMPPLLESpeciesName() As String
    Dim SIMPPLLESpectrumSpecies() As String 'the SPECTRUM species name for the SIMPPLLE species
    Dim SIMPPLLESizeName() As String
    Dim SIMPPLLESizeNameUnq() As String
    Dim SIMPPLLESpectrumHTG() As String
    Dim SIMPPLLEHTGName() As String
    Dim SIMPPLLESpectrumSize() As String 'the SPECTRUM size name for the SIMPPLLE size

    Dim NSpecies As Integer 'number of l5 categories
    Dim NSize As Integer 'number of L6 categories
    Dim NGA As Integer 'number of geographic areas
    Dim NSpecHTG As Integer ' number of habitat type groups to use to split up the treatments
    Dim NTrt As Integer 'number of unique treatment types
    Dim NSIMPPLLETrt As Integer
    'Dim MaxTrtDefs As Integer
    'Dim NTrtDefs() As Integer 'number of definitions associated with each treatment
    Public NTic As Integer 'number of timesteps to 
    Dim NRpt As Integer 'number of reports to search through
    Dim NSIMPPLLESpecies As Integer
    Dim NSIMPPLLESize As Integer
    Dim NSIMPPLLESizeDefs As Integer
    Dim NSIMPPLLEHTG As Integer


    Dim NTrtsPerGA() As Integer

    'field names
    Dim GAfieldName As String
    Dim SpecialAreaFieldName As String
    Dim HTGFieldName As String
    Dim SuitFieldName As String
    Dim SpeciesfieldName As String
    Dim SizefieldName As String
    Dim AcreFieldName As String

    Public Sub TranslateSpectrumToSIMPPLLE(ByVal outputfilenameBase As String, ByVal NTimePers As Integer, ByVal SCellSize As Single, ByVal GCellSize As Single, ByVal Zone As String)
        Dim actualacres As Double
        NTic = NTimePers
        SIMPPLLECellSize = SCellSize
        GISCellSize = GCellSize
        actualacres = (GISCellSize ^ 2) / 4046.856
        AcreAdjFactor = SIMPPLLECellSize / actualacres
        ZonePrint = Zone

        LUTFileNames(SpecToSIMModDefFolder)

        LoadLUTs()

        LoadAAFile()
        LoadSpecResultsFile()
        PortionGATreatmentAcres()

        WriteSIMPPLLEtrtfiles(outputfilenameBase)

        MsgBox("Done")

    End Sub

    Private Sub WriteSIMPPLLEtrtfiles(ByVal outputfilenamebase As String)
        Dim fnum As Integer
        Dim ktic As Integer
        Dim str As String
        Dim substr As String
        Dim teststr As String
        Dim trtacres As Single
        Dim testacres2 As Single
        Dim TrtmtAcres(,) As Double
        Dim TrtAcresByGA(,,) As Double 'by GA, trt, tic
        Dim STrtAcres(,) As Double 'by SIMPPLLE trt, tic
        Dim ktrt As Integer
        Dim ksimsize As Integer
        Dim SizeUsed() As Byte
        'Dim sizind As Integer

        ' ReDim TrtmtAcres(NTrt, MaxTrtDefs, NTic)
        ReDim TrtmtAcres(NTrt, NTic)
        ReDim TrtAcresByGA(NGA, NSIMPPLLETrt, NTic), STrtAcres(NSIMPPLLETrt, NTic)

        For jga As Integer = 1 To NGA
            fnum = FreeFile()
            FileOpen(fnum, outputfilenamebase & GAName(jga) & "Treatments.txt", OpenMode.Output)
            PrintLine(fnum, "3")
            PrintLine(fnum, ZonePrint)
            PrintLine(fnum, Trim(NTrtsPerGA(jga)))
            ktic = NTic + 1
            For jtic As Integer = 1 To NTic
                ktic = ktic - 1
                For jtrt As Integer = 1 To NTrt
                    ktrt = GetStrIndex(SIMPPLLETrtNameList, SIMPPLLETrtName(jtrt))
                    For jl5 As Integer = 1 To NSpecies
                        'need to determine if this treatment matches the HTG of jlr5

                        str = ""
                        If GAL5AcresTreated(jga, jl5, jtrt, ktic) > 0 Then
                            trtacres = Math.Round(GAL5AcresTreated(jga, jl5, jtrt, ktic) * AcreAdjFactor, 0)
                            testacres2 = trtacres Mod SIMPPLLECellSize
                            trtacres = trtacres + SIMPPLLECellSize - testacres2
                            TrtmtAcres(jtrt, ktic) = TrtmtAcres(jtrt, ktic) + Math.Round(GAL5AcresTreated(jga, jl5, jtrt, ktic), 0) 'trtacres
                            TrtAcresByGA(jga, ktrt, ktic) = TrtAcresByGA(jga, ktrt, ktic) + trtacres
                            STrtAcres(ktrt, ktic) = STrtAcres(ktrt, ktic) + trtacres
                            str = ktic & "," & SIMPPLLETrtName(jtrt) & "," & trtacres & "00,"
                            'determine special areas to apply this to
                            substr = ""
                            If TrtSpecialAreaCode(jtrt) <> "?" Then
                                For jspecarea As Integer = 1 To NSpecialArea
                                    If TrtSpecialAreaSuit(jtrt, jspecarea) = 1 Then
                                        substr = substr & SpecialAreaName(jspecarea) & ":"
                                    End If
                                Next
                            End If
                            If substr.Length > 0 Then substr = substr.Substring(0, substr.Length - 1) 'lop off the last ";"
                            If substr.Length = 0 Then substr = "?"
                            str = str & substr & ","
                            'road status, timesteps before follow-up treatment, t/f for a follow up treatment, t/f for retreatment, N Timesteps before unit is available for retreatment
                            str = str & TrtRoadStatus(jtrt) & "," & TrtTimestepsBeforeFollowUp(jtrt) & "," _
                              & TrtFollowUpTrt(jtrt) & "," & TrtAllowReTreat(jtrt) & "," _
                              & TrtTimestepsBeforeRetreat(jtrt) & "," & TrtFollowUpTrtName(jtrt) & "," '"?,0,false,false,5,NO-TREATMENT,"
                            'determine ownership to apply this to
                            substr = ""
                            If TrtOWNCode(jtrt) <> "?" Then
                                For jsuit As Integer = 1 To NOWN
                                    If TrtOWNSuit(jtrt, jsuit) = 1 Then
                                        substr = substr & OwnName(jsuit) & ":"
                                    End If
                                Next
                            End If
                            If substr.Length > 0 Then substr = substr.Substring(0, substr.Length - 1) 'lop off the last ":"
                            If substr.Length = 0 Then substr = "?"
                            str = str & substr
                            PrintLine(fnum, str)

                            'SECOND LINE:
                            'HTG,species,SIZE,DENSITY,PREVIOUS-PROCESS,SIZE-RANGE,T/F-FOR-EXCEPTIONS(5)
                            substr = ""
                            If TrtHTGCode(jtrt) <> "?" Then
                                For jhtg As Integer = 1 To NSpecHTG
                                    If TrtHTGSuit(jtrt, jhtg) = 1 Then
                                        For jsimhtg As Integer = 1 To NSIMPPLLEHTG
                                            If SIMPPLLESpectrumHTG(jsimhtg) = SpecHTGName(jhtg) Then
                                                substr = substr & SIMPPLLEHTGName(jsimhtg) & ":"
                                            End If
                                        Next jsimhtg
                                    End If
                                Next
                            End If
                            If substr.Length > 0 Then substr = substr.Substring(0, substr.Length - 1) 'lop off the last ":"
                            If substr.Length = 0 Then substr = "?"
                            str = substr & ","
                            'determine species to apply this to
                            substr = ""
                            teststr = SpecSpeciesName(jl5)
                            For jspec As Integer = 1 To NSIMPPLLESpecies
                                If SIMPPLLESpectrumSpecies(jspec) = teststr Then
                                    substr = substr & SIMPPLLESpeciesName(jspec) & ":"
                                End If
                            Next
                            If substr.Length > 0 Then substr = substr.Substring(0, substr.Length - 1) 'lop off the last ";"
                            If substr.Length = 0 Then substr = "?"
                            str = str & substr & ","

                            'determine size class to apply this to - filter for duplicates
                            substr = ""
                            ReDim SizeUsed(NSIMPPLLESize)
                            For jsize As Integer = 1 To NSize
                                teststr = SpecSizeName(jsize)
                                If TrtSizeSuit(jtrt, jsize) = 1 Then 'defined for this treatment, size
                                    For jsimsize As Integer = 1 To NSIMPPLLESizeDefs
                                        ksimsize = Array.IndexOf(SIMPPLLESizeNameUnq, SIMPPLLESizeName(jsimsize))
                                        If SIMPPLLESpectrumSize(jsimsize) = teststr And SizeUsed(ksimsize) = 0 Then
                                            substr = substr & SIMPPLLESizeNameUnq(ksimsize) & ":"
                                            SizeUsed(ksimsize) = 1
                                        End If
                                    Next
                                End If
                            Next jsize
                            If substr.Length > 0 Then substr = substr.Substring(0, substr.Length - 1) 'lop off the last ";"
                            If substr.Length = 0 Then substr = "?"
                            str = str & substr & ","

                            'default density, previous process:
                            str = str & TrtDensityString(jtrt) & "," & TrtPreviousProcess(jtrt) & "," _
                               & TrtMinSize(jtrt) & ":" & TrtMaxSize(jtrt) & "," & TrtExceptionString(jtrt) '"2:3:4,?"
                            PrintLine(fnum, str)

                            'THIRD LINE - DEFAULT NIL
                            PrintLine(fnum, "NIL")
                            'FOURTH LINE - DEFAULT NIL
                            PrintLine(fnum, "NIL")
                        End If
                    Next
                    'Next jdef
                Next
            Next
            FileClose(fnum)
        Next
        'print out debug file
        fnum = FreeFile()
        FileOpen(fnum, outputfilenamebase & "TreatmentsSummary.txt", OpenMode.Output)
        PrintLine(fnum, "Treatment,Ownership,tic1,tic2,tic3,tic4,tic5")
        For jtrt As Integer = 1 To NTrt
            'For jdef As Integer = 1 To NTrtDefs(jtrt)
            str = SpecTrtName(jtrt) & "," & TrtOWNCode(jtrt)
            For jtic As Integer = 1 To NTic
                str = str & "," & Math.Round(TrtmtAcres(jtrt, jtic), 0)
            Next
            PrintLine(fnum, str)
            'Next jdef
        Next
        FileClose(fnum)

        'print out other debug files
        Dim bprintline As Boolean
        Dim bprintline2 As Boolean
        Dim str2 As String
        fnum = FreeFile()
        FileOpen(fnum, outputfilenamebase & "AcresSummary.txt", OpenMode.Output)
        PrintLine(fnum, "Species,Size,Treatment,Ownership,GA,GAAcresOfType,TotalAcresOfType,Treat1,TotTrt1OnForest,Treat2,TotTrt2OnForest,Treat3,TotTrt3OnForest,Treat4,TotTrt4OnForest,Treat5,TotTrt5OnForest,")
        'For jhtg As Integer = 1 To NHTG
        For jown As Integer = 1 To NOWN
            For jl5 As Integer = 1 To NSpecies
                For jl6 As Integer = 1 To NSize
                    For jtrt As Integer = 1 To NTrt
                        'need to determine if this treatment matches the HTG of jlr5

                        For jga As Integer = 1 To NGA
                            bprintline = False
                            bprintline2 = False
                            str = SpecSpeciesName(jl5) & "," & SpecSizeName(jl6) & "," & SpecTrtName(jtrt) & "," & TrtOWNCode(jtrt) & "," & GAName(jga) & "," & _
                                  Math.Round(SpecOWNL5L6GATrtAcres(jown, jl5, jl6, jga, jtrt), 0) & "," & Math.Round(SpecOWNL5L6TrtAcres(jown, jl5, jl6, jtrt), 0)
                            str2 = SpecSpeciesName(jl5) & "," & SpecSizeName(jl6) & "," & SpecTrtName(jtrt) & ","
                            For jtic As Integer = 1 To NTic
                                trtacres = SpecOWNL5L6GATrtAcres(jown, jl5, jl6, jga, jtrt) / SpecOWNL5L6TrtAcres(jown, jl5, jl6, jtrt) * SpecOWNL5L6TrtAcresTreated(jown, jl5, jl6, jtrt, jtic)
                                If trtacres > 0 Then bprintline = True
                                If SpecOWNL5L6TrtAcresTreated(jown, jl5, jl6, jtrt, jtic) > 0 Then bprintline2 = True
                                str = str & "," & Math.Round(trtacres, 0) & "," & Math.Round(SpecOWNL5L6TrtAcresTreated(jown, jl5, jl6, jtrt, jtic), 0)
                                str2 = str2 & "," & Math.Round(SpecOWNL5L6TrtAcresTreated(jown, jl5, jl6, jtrt, jtic), 0)
                            Next
                            If bprintline = True Then PrintLine(fnum, str)

                        Next

                    Next
                Next
            Next
        Next jown
        'Next jhtg
        FileClose(fnum)

        'print out LS type file for comparison
        fnum = FreeFile()
        FileOpen(fnum, outputfilenamebase & "LSFormat_forCompare.txt", OpenMode.Output)
        For jga As Integer = 1 To NGA
            PrintLine(fnum, GAName(jga))
            For jtrt As Integer = 1 To NSIMPPLLETrt
                bprintline = False
                For jtic As Integer = 0 To NTic
                    If Math.Round(TrtAcresByGA(jga, jtrt, jtic), 0) > 0 Then
                        bprintline = True
                        Exit For
                    End If
                Next
                If bprintline = True Then
                    PrintLine(fnum, "TIME " & SIMPPLLETrtName(jtrt))
                    For jtic As Integer = 0 To NTic
                        PrintLine(fnum, jtic & " " & Math.Round(TrtAcresByGA(jga, jtrt, jtic), 0))
                    Next
                End If
            Next
        Next
        'now print out the totals
        PrintLine(fnum, "Totals")
        For jtrt As Integer = 1 To NSIMPPLLETrt
            bprintline = False
            For jtic As Integer = 0 To NTic
                If Math.Round(STrtAcres(jtrt, jtic), 0) > 0 Then
                    bprintline = True
                    Exit For
                End If
            Next
            If bprintline = True Then
                PrintLine(fnum, "TIME " & SIMPPLLETrtName(jtrt))
                For jtic As Integer = 0 To NTic
                    PrintLine(fnum, jtic & " " & Math.Round(STrtAcres(jtrt, jtic), 0))
                Next
            End If
        Next

        FileClose(fnum)

    End Sub
    Private Sub PortionGATreatmentAcres()
        'figures out how many acres to treat by GA
        'collapses the L6 code and portions out acres based on how many l5/l6 acres are in the GA compared to the total.
        'Rounds to 5 acres, since SIMPPLLE is in 5-acre blocks
        Dim trtacres As Single

        'ReDim GAL5AcresTreated(NGA, NSpecies, NTrt, MaxTrtDefs, NTic)
        ReDim GAL5AcresTreated(NGA, NSpecies, NTrt, NTic)
        ReDim NTrtsPerGA(NGA)

        'For jhtg As Integer = 1 To NHTG
        For jown As Integer = 1 To NOWN
            For jl5 As Integer = 1 To NSpecies
                For jl6 As Integer = 1 To NSize
                    For jtrt As Integer = 1 To NTrt
                        'For jdef As Integer = 1 To NTrtDefs(jtrt)
                        For jga As Integer = 1 To NGA
                            For jtic As Integer = 1 To NTic
                                trtacres = 0
                                If SpecOWNL5L6TrtAcres(jown, jl5, jl6, jtrt) > 0 Then
                                    trtacres = SpecOWNL5L6GATrtAcres(jown, jl5, jl6, jga, jtrt) / SpecOWNL5L6TrtAcres(jown, jl5, jl6, jtrt) * SpecOWNL5L6TrtAcresTreated(jown, jl5, jl6, jtrt, jtic)
                                    'If trtacres > 0 Then Stop
                                End If
                                GAL5AcresTreated(jga, jl5, jtrt, jtic) += trtacres
                            Next jtic
                        Next jga
                        'Next jdef
                    Next jtrt
                Next jl6
            Next jl5
        Next jown
        'Next jhtg
        'count the number of treatments per GA

        'For jhtg As Integer = 1 To NHTG
        For jl5 As Integer = 1 To NSpecies
            For jtrt As Integer = 1 To NTrt
                'For jdef As Integer = 1 To NTrtDefs(jtrt)
                For jga As Integer = 1 To NGA
                    For jtic As Integer = 1 To NTic
                        If GAL5AcresTreated(jga, jl5, jtrt, jtic) > 0 Then
                            NTrtsPerGA(jga) = NTrtsPerGA(jga) + 1
                        End If
                    Next jtic
                Next jga
                'Next jdef
            Next jtrt
        Next jl5
        ' Next jhtg


    End Sub

    Private Sub LoadAAFile()
        'loads the AA definition files to get acreage information for the AAs - to portion out by L5/L6
        Dim l5field As Integer
        Dim l6field As Integer
        Dim GAfield As Integer
        Dim HTGfield As Integer
        Dim AcreField As Integer = -1
        Dim OwnField As Integer = -1
        Dim SpecialAreaField As Integer = -1

        Dim SuitableForTrt As Integer '1/0 based on whether the Special Area and Suitability fields align to be suitable for the treatment

        Dim fnum As Integer = FreeFile()
        Dim dummy As String
        Dim dum2 As String
        Dim arr() As String
        Dim saind As Integer
        'Dim htgind As Integer
        Dim l5ind As Integer
        Dim l6ind As Integer
        Dim gaind As Integer
        Dim ownind As Integer
        Dim specareaind As Integer
        Dim SASuit As Integer
        Dim SSuit As Integer
        Dim HSuit As Integer
        Dim SACode As String
        Dim SCode As String

        'LOAD AA FILE

        ReDim SpecOWNL5L6TrtAcres(NOWN, NSpecies, NSize, NTrt) 'by level 5, level 6 - total acres in each category - need to add suitabilty dimension?
        'ReDim SpecL5L6GATrtAcres(NSpecies, NSize, NGA, NTrt, MaxTrtDefs) 
        ReDim SpecOWNL5L6GATrtAcres(NOWN, NSpecies, NSize, NGA, NTrt)  'by l5, l6, GA - to portion out treatment amounts
        FileOpen(fnum, AAByGeoAreaFN, OpenMode.Input)
        dummy = LineInput(fnum) 'header line
        'find the field names
        arr = Split(dummy, ",")
        For j As Integer = 0 To arr.Length - 1
            dum2 = Trim(StripQuotes(arr(j)))
            If dum2 = HTGFieldName Then HTGfield = j
            If dum2 = GAfieldName Then GAfield = j
            If dum2 = SpecialAreaFieldName Then SpecialAreaField = j
            If dum2 = SuitFieldName Then OwnField = j
            If dum2 = SpeciesfieldName Then l5field = j
            If dum2 = SizefieldName Then l6field = j
            If dum2 = AcreFieldName Then AcreField = j

        Next
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                'htgind = GetStrIndex(SpecHTGName, Trim(StripQuotes(arr(HTGfield))))
                l5ind = GetStrIndex(SpecSpeciesName, Trim(StripQuotes(arr(l5field))))
                l6ind = GetStrIndex(SpecSizeName, Trim(StripQuotes(arr(l6field))))
                gaind = GetStrIndex(GAName, Trim(StripQuotes(arr(GAfield))))
                SCode = Trim(StripQuotes(arr(OwnField)))
                ownind = GetStrIndex(OwnName, SCode)
                SACode = Trim(StripQuotes(arr(SpecialAreaField)))
                specareaind = GetStrIndex(SpecialAreaName, SACode)

                If l5ind >= 0 And l6ind >= 0 And gaind >= 0 And ownind >= 0 And specareaind >= 0 Then 'poly is defined
                    For jtrt As Integer = 1 To NTrt
                        'For jdef As Integer = 1 To NTrtDefs(jtrt)
                        SASuit = TrtSpecialAreaSuit(jtrt, specareaind)
                        SSuit = TrtOWNSuit(jtrt, ownind) 'has to be defined as suitable in both
                        'HSuit = TrtHTGSuit(jtrt, htgind)
                        SuitableForTrt = SASuit * SSuit '* HSuit
                        SpecOWNL5L6TrtAcres(ownind, l5ind, l6ind, jtrt) = SpecOWNL5L6TrtAcres(ownind, l5ind, l6ind, jtrt) + arr(AcreField) * SuitableForTrt
                        SpecOWNL5L6GATrtAcres(ownind, l5ind, l6ind, gaind, jtrt) = SpecOWNL5L6GATrtAcres(ownind, l5ind, l6ind, gaind, jtrt) + arr(AcreField) * SuitableForTrt
                        'Next
                    Next
                End If
            End If
        Loop

        FileClose(fnum)

    End Sub

    Private Sub LoadSpecResultsFile()
        'loads the Spectrum results file 
        Dim TotalPers As Integer = 15

        Dim MaxIndex As Integer = 0
        Dim fnum As Integer = FreeFile()
        Dim dummy As String
        Dim arr() As String
        Dim rptdefind As Integer
        'Dim SAind As Integer 'special area index
        Dim ownind As Integer
        Dim l5ind As Integer
        Dim l6ind As Integer
        Dim trtind As Integer
        Dim rpnum As String
        Dim rptrt As String
        Dim rpttype As String
        Dim htgind As Integer
        Dim htgname As String

        'LOAD SPEC COMADEL

        ReDim SpecOWNL5L6TrtAcresTreated(NOWN, NSpecies, NSize, NTrt, NTic) 'goal is to populate this array
        FileOpen(fnum, SpectrumOutputFN, OpenMode.Input)
        dummy = LineInput(fnum) 'header line
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                rpttype = Trim(StripQuotes(arr(6)))
                If rpttype = "ACRE" Then
                    rpnum = Trim(StripQuotes(arr(2)))
                    rptrt = Trim(StripQuotes(arr(4)))
                    rptdefind = GetStrIndex(RptName, rpnum & rptrt)
                    ownind = GetStrIndex(OwnName, RptOwnership(rptdefind))
                    'SAind = GetStrIndex(SpecialAreaName, RptSpecialArea(rptdefind))
                    l5ind = GetStrIndex(SpecSpeciesName, RptL5(rptdefind))
                    l6ind = GetStrIndex(SpecSizeName, RptL6(rptdefind))
                    'find the Spectrum Ecogroup for this lr5ind
                    htgname = SpecSpeciesEcoGroup(l5ind)
                    htgind = GetStrIndex(SpecHTGName, htgname)

                    'This trtind is the index in the SPEC_TRT_LUT and needs to match on treatment name and special area
                    trtind = -1
                    For jtrt As Integer = 1 To NTrt
                        'treatment has to be feasible for the special area code of the treatment
                        If TrtOWNSuit(jtrt, ownind) = 1 And SpecTrtName(jtrt) = RptTrt(rptdefind) And TrtHTGSuit(jtrt, htgind) = 1 Then
                            'here is where you match special area code for the treatment. SHOULD ALSO MATCH ON THE HTG CODE
                            trtind = jtrt
                            Exit For
                        End If
                    Next jtrt
                    If trtind >= 0 Then
                        For jtic As Integer = 1 To NTic
                            SpecOWNL5L6TrtAcresTreated(ownind, l5ind, l6ind, trtind, jtic) = CType(Trim(arr(6 + jtic)), Single)
                            'If jtic = 1 And trtind = 4 And SpecL5L6TrtAcresTreated(l5ind, l6ind, trtind, jtic) > 0 Then Stop
                        Next
                    End If
                End If
            End If
            'If TotalPers > 20 Then dummy = LineInput(fnum) 'sloppy code; assume NTic < 20, etc.
            'dummy = LineInput(fnum) 'amt
            'If TotalPers > 20 Then dummy = LineInput(fnum) 'sloppy code; assume NTic < 20, etc.
        Loop

        FileClose(fnum)



    End Sub


    Private Sub LoadLUTs()

        Dim MaxIndex As Integer = 0
        Dim fnum As Integer = FreeFile()
        Dim dummy As String
        Dim arr() As String
        Dim arr2() As String
        Dim kind As Integer
        Dim ktrt As Integer
        Dim kdef As Integer
        Dim kspecies As Integer
        Dim ksize As Integer


        'LOAD GA LUT
        MaxIndex = 0
        ReDim GAName(MaxIndex)
        FileOpen(fnum, GALUTFN, OpenMode.Input)
        dummy = LineInput(fnum) 'header line
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                MaxIndex = MaxIndex + 1
                ReDim Preserve GAName(MaxIndex)
                GAName(MaxIndex) = CType(arr(0), String)
            End If
        Loop
        NGA = MaxIndex
        FileClose(fnum)

        'LOAD FIELD NAMES
        FileOpen(fnum, FieldNameLUTFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                If arr(0) = "GAfieldName" Then GAfieldName = arr(1)
                If arr(0) = "SpecialAreaFieldName" Then SpecialAreaFieldName = arr(1)
                If arr(0) = "SuitFieldName" Then SuitFieldName = arr(1)
                If arr(0) = "SpeciesfieldName" Then SpeciesfieldName = arr(1)
                If arr(0) = "SizefieldName" Then SizefieldName = arr(1)
                If arr(0) = "AcreFieldName" Then AcreFieldName = arr(1)
            End If
        Loop
        FileClose(fnum)


        'LOAD SPEC SPECIES LUT
        MaxIndex = 0
        ReDim SpecSpeciesName(MaxIndex)
        FileOpen(fnum, SpecSpeciesLUTFN, OpenMode.Input)
        dummy = LineInput(fnum) 'header line
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                MaxIndex = MaxIndex + 1
                ReDim Preserve SpecSpeciesName(MaxIndex)
                SpecSpeciesName(MaxIndex) = CType(arr(0), String)
            End If
        Loop
        NSpecies = MaxIndex
        FileClose(fnum)

        'LOAD SPEC SIZE LUT
        MaxIndex = 0
        ReDim SpecSizeName(MaxIndex)
        FileOpen(fnum, SpecSizeLUTFN, OpenMode.Input)
        dummy = LineInput(fnum) 'header line
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                MaxIndex = MaxIndex + 1
                ReDim Preserve SpecSizeName(MaxIndex)
                SpecSizeName(MaxIndex) = CType(arr(0), String)
            End If
        Loop
        NSize = MaxIndex
        FileClose(fnum)

        'LOAD SPECIAL AREA LUT
        MaxIndex = 0
        ReDim SpecialAreaName(MaxIndex)
        FileOpen(fnum, SpecialAreaLUTFN, OpenMode.Input)
        dummy = LineInput(fnum) 'header line
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                MaxIndex = MaxIndex + 1
                ReDim Preserve SpecialAreaName(MaxIndex)
                SpecialAreaName(MaxIndex) = CType(arr(0), String)
            End If
        Loop
        NSpecialArea = MaxIndex
        FileClose(fnum)

        'LOAD SUITABILITY LUT
        MaxIndex = 0
        ReDim OwnName(MaxIndex)
        FileOpen(fnum, SuitLUTFN, OpenMode.Input)
        dummy = LineInput(fnum) 'header line
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                MaxIndex = MaxIndex + 1
                ReDim Preserve OwnName(MaxIndex)
                OwnName(MaxIndex) = CType(arr(0), String)
            End If
        Loop
        NOWN = MaxIndex
        FileClose(fnum)

        'LOAD ECOGROUP LUT
        MaxIndex = 0
        ReDim SpecHTGName(MaxIndex)
        FileOpen(fnum, SpecHTGLUTFN, OpenMode.Input)
        dummy = LineInput(fnum) 'header line
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                MaxIndex = MaxIndex + 1
                ReDim Preserve SpecHTGName(MaxIndex)
                SpecHTGName(MaxIndex) = CType(arr(0), String)
            End If
        Loop
        NSpecHTG = MaxIndex
        FileClose(fnum)

        'LOAD SPEC trt LUT
        MaxIndex = 0
        NSIMPPLLETrt = 0
        ReDim SpecTrtName(MaxIndex), SIMPPLLETrtName(MaxIndex), SIMPPLLETrtNameList(NSIMPPLLETrt)
        FileOpen(fnum, TrtLUTFN, OpenMode.Input)
        dummy = LineInput(fnum) 'header line
        'dummy = LineInput(fnum) 'second header line
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                MaxIndex = MaxIndex + 1
                ReDim Preserve SpecTrtName(MaxIndex), SIMPPLLETrtName(MaxIndex)
                SpecTrtName(MaxIndex) = CType(arr(1), String)
                SIMPPLLETrtName(MaxIndex) = CType(arr(2), String)
                'dummy = LineInput(fnum) 'second line of the treatment definition - not used here
                kdef = GetStrIndex(SIMPPLLETrtNameList, SIMPPLLETrtName(MaxIndex))
                If kdef < 0 Then
                    NSIMPPLLETrt = NSIMPPLLETrt + 1
                    ReDim Preserve SIMPPLLETrtNameList(NSIMPPLLETrt)
                    SIMPPLLETrtNameList(NSIMPPLLETrt) = SIMPPLLETrtName(MaxIndex)
                End If
            End If
        Loop
        NTrt = MaxIndex
        FileClose(fnum)

        'read again to get the good stuff about treatments and such...
        ReDim TrtHTGSuit(NTrt, NSpecHTG), TrtSizeSuit(NTrt, NSize), TrtRoadStatus(NTrt), TrtTimestepsBeforeFollowUp(NTrt), TrtFollowUpTrt(NTrt)
        ReDim TrtAllowReTreat(NTrt), TrtTimestepsBeforeRetreat(NTrt), TrtFollowUpTrtName(NTrt), TrtPreviousProcess(NTrt)
        ReDim TrtDensityString(NTrt), TrtMinSize(NTrt), TrtMaxSize(NTrt), TrtExceptionString(NTrt)
        ReDim TrtSpecialAreaSuit(NTrt, NSpecialArea), TrtOWNSuit(NTrt, NOWN), TrtSpecialAreaCode(NTrt), TrtOWNCode(NTrt), TrtHTGCode(NTrt)
        FileOpen(fnum, TrtLUTFN, OpenMode.Input)
        dummy = LineInput(fnum) 'header line
        'dummy = lineinput(fnum) 'second header line
        ktrt = 0
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                ktrt = ktrt + 1
                arr = Split(dummy, ",")
                'load special area info.
                TrtSpecialAreaCode(ktrt) = Trim(arr(4))
                arr2 = Split(arr(4), ";")
                If TrtSpecialAreaCode(ktrt) <> "?" Then
                    For j2 As Integer = 0 To arr2.Length - 1
                        kind = GetStrIndex(SpecialAreaName, Trim((arr2(j2))))
                        TrtSpecialAreaSuit(ktrt, kind) = 1
                    Next

                Else
                    'if you have a ? then all special areas are suitable
                    For jsa As Integer = 0 To NSpecialArea
                        TrtSpecialAreaSuit(ktrt, jsa) = 1
                    Next
                End If
                'road status
                TrtRoadStatus(ktrt) = Trim(arr(5))
                TrtTimestepsBeforeFollowUp(ktrt) = Trim(arr(6))
                TrtFollowUpTrt(ktrt) = Trim(arr(7))
                TrtAllowReTreat(ktrt) = Trim(arr(8))
                TrtTimestepsBeforeRetreat(ktrt) = Trim(arr(9))
                TrtFollowUpTrtName(ktrt) = Trim(arr(10))

                'load suitability info.
                TrtOWNCode(ktrt) = Trim(arr(11))
                arr2 = Split(arr(11), ";")
                If TrtOWNCode(ktrt) <> "?" Then
                    For j2 As Integer = 0 To arr2.Length - 1
                        kind = GetStrIndex(OwnName, Trim((arr2(j2))))
                        TrtOWNSuit(ktrt, kind) = 1
                    Next
                Else
                    'if you have a ? then all suitability codes are included
                    For jsu As Integer = 0 To NOWN
                        TrtOWNSuit(ktrt, jsu) = 1
                    Next
                End If

                'SECOND LINE OF THE TREATMENT INFORMATION
                'dummy = LineInput(fnum)
                arr = Split(dummy, ",")
                TrtHTGCode(ktrt) = arr(12)
                'load HTG info.
                arr2 = Split(arr(12), ";")
                If arr2(0) <> "?" Then
                    For j2 As Integer = 0 To arr2.Length - 1
                        kind = GetStrIndex(SpecHTGName, (arr2(j2)))
                        TrtHTGSuit(ktrt, kind) = 1
                    Next
                Else
                    For jhtg As Integer = 1 To NSpecHTG
                        TrtHTGSuit(ktrt, jhtg) = 1
                    Next
                End If
                'load size info.
                arr2 = Split(arr(13), ";")
                For j2 As Integer = 0 To arr2.Length - 1
                    kind = GetStrIndex(SpecSizeName, (arr2(j2)))
                    TrtSizeSuit(ktrt, kind) = 1
                Next
                'density information
                arr2 = Split(arr(14), ";")
                TrtDensityString(ktrt) = arr2(0)
                For j2 As Integer = 1 To arr2.Length - 1
                    TrtDensityString(ktrt) = TrtDensityString(ktrt) & ":" & arr2(j2)
                Next
                TrtPreviousProcess(ktrt) = arr(15)
                arr2 = Split(arr(16), ";")
                TrtMinSize(ktrt) = arr2(0)
                TrtMaxSize(ktrt) = arr2(1)
                TrtExceptionString(ktrt) = arr(17) & "," & arr(18) & "," & arr(19) & "," & arr(20) & "," & arr(21)

            End If
        Loop
        FileClose(fnum)



        'LOAD SPEC rpt LUT
        MaxIndex = 0
        ReDim RptName(MaxIndex), RptOwnership(MaxIndex), RptL5(MaxIndex), RptL6(MaxIndex), RptTrt(MaxIndex)
        FileOpen(fnum, RptLUTFN, OpenMode.Input)
        dummy = LineInput(fnum) 'header line
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                MaxIndex = MaxIndex + 1
                ReDim Preserve RptName(MaxIndex), RptOwnership(MaxIndex), RptL5(MaxIndex), RptL6(MaxIndex), RptTrt(MaxIndex)
                RptName(MaxIndex) = CType(arr(0), String) & Trim(arr(1))
                RptOwnership(MaxIndex) = CType(arr(2), String)
                RptL5(MaxIndex) = CType(arr(3), String)
                RptL6(MaxIndex) = CType(arr(4), String)
                RptTrt(MaxIndex) = CType(arr(5), String)
            End If
        Loop
        NRpt = MaxIndex
        FileClose(fnum)

        'load SpeciesXWFN
        NSIMPPLLESpecies = 0
        ReDim SIMPPLLESpeciesName(NSIMPPLLESpecies), SIMPPLLESpectrumSpecies(NSIMPPLLESpecies)

        FileOpen(fnum, SpeciesXWFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                NSIMPPLLESpecies = NSIMPPLLESpecies + 1
                ReDim Preserve SIMPPLLESpeciesName(NSIMPPLLESpecies), SIMPPLLESpectrumSpecies(NSIMPPLLESpecies)
                SIMPPLLESpeciesName(NSIMPPLLESpecies) = arr(0)
                SIMPPLLESpectrumSpecies(NSIMPPLLESpecies) = arr(1)
            End If
        Loop
        FileClose(fnum)

        'load SizeXWFN
        NSIMPPLLESize = 0
        NSIMPPLLESizeDefs = 0
        ReDim SIMPPLLESizeName(NSIMPPLLESizeDefs), SIMPPLLESpectrumSize(NSIMPPLLESizeDefs), SIMPPLLESizeNameUnq(NSIMPPLLESize)

        FileOpen(fnum, SizeXWFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                NSIMPPLLESizeDefs = NSIMPPLLESizeDefs + 1
                ReDim Preserve SIMPPLLESpectrumSize(NSIMPPLLESizeDefs), SIMPPLLESizeName(NSIMPPLLESizeDefs)
                ksize = Array.IndexOf(SIMPPLLESizeNameUnq, arr(0))
                If ksize < 0 Then
                    NSIMPPLLESize += 1
                    ReDim Preserve SIMPPLLESizeNameUnq(NSIMPPLLESize)
                    SIMPPLLESizeNameUnq(NSIMPPLLESize) = arr(0)
                End If
                SIMPPLLESizeName(NSIMPPLLESizeDefs) = arr(0)
                SIMPPLLESpectrumSize(NSIMPPLLESizeDefs) = arr(1)
            End If
        Loop
        FileClose(fnum)

        'load EcoGroupXWFN
        NSIMPPLLEHTG = 0
        ReDim SIMPPLLEHTGName(NSIMPPLLEHTG), SIMPPLLESpectrumHTG(NSIMPPLLEHTG)

        FileOpen(fnum, HTGXWFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                NSIMPPLLEHTG = NSIMPPLLEHTG + 1
                ReDim Preserve SIMPPLLEHTGName(NSIMPPLLEHTG), SIMPPLLESpectrumHTG(NSIMPPLLEHTG)
                SIMPPLLEHTGName(NSIMPPLLEHTG) = arr(0)
                SIMPPLLESpectrumHTG(NSIMPPLLEHTG) = arr(1)
            End If
        Loop
        FileClose(fnum)

        'load SPEC_SPECIES_ECOGROUP_XW
        ReDim SpecSpeciesEcoGroup(NSpecies) 'SIMPPLLEHTGName(NSIMPPLLEHTG), SIMPPLLESpectrumHTG(NSIMPPLLEHTG)

        FileOpen(fnum, HTG_Species_XWFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                'determine the index of this species
                kspecies = Array.IndexOf(SpecSpeciesName, Trim(arr(0)))
                SpecSpeciesEcoGroup(kspecies) = Trim(arr(1))
            End If
        Loop
        FileClose(fnum)


    End Sub

    Private Sub LUTFileNames(ByVal basefolder As String)

        Dim letter As String
        letter = basefolder.Substring(basefolder.Length - 1, 1)
        If letter <> "\" Then basefolder = basefolder & "\"

        SpecSpeciesLUTFN = basefolder & "SPEC_SPECIES_LUT.txt"
        SpecSizeLUTFN = basefolder & "SPEC_SIZE_LUT.txt"
        SpecHTGLUTFN = basefolder & "SPEC_ECOGROUP_LUT.txt"
        GALUTFN = basefolder & "GA_LUT.txt"
        TrtLUTFN = basefolder & "SPEC_TRT_LUT.txt"
        RptLUTFN = basefolder & "SPEC_RPT_LUT.txt"
        SuitLUTFN = basefolder & "SUIT_LUT.txt"
        SpecialAreaLUTFN = basefolder & "SPECIAL_AREA_LUT.txt"
        FieldNameLUTFN = basefolder & "FIELD_NAMES.txt"
        SpeciesXWFN = basefolder & "SPECIES_XW.txt"
        SizeXWFN = basefolder & "SIZE_XW.txt"
        HTGXWFN = basefolder & "ECOGROUP_XW.txt"
        'HTGLUTFN = basefolder & "SPEC_ECOGROUP_LUT.txt" 'SPECTRUM_ECOGROUP
        HTG_Species_XWFN = basefolder & "SPEC_SPECIES_ECOGROUP_XW.txt"
        'TrtDefLUTFN = basefolder & "TRT_SPECAREA_SUIT_DEFS.txt"


    End Sub

End Module
