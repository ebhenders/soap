<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PathwayEdit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.rbAttributesall = New System.Windows.Forms.RadioButton
        Me.rbRawData = New System.Windows.Forms.RadioButton
        Me.Label3 = New System.Windows.Forms.Label
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.Label4 = New System.Windows.Forms.Label
        Me.Button5 = New System.Windows.Forms.Button
        Me.Label5 = New System.Windows.Forms.Label
        Me.Button6 = New System.Windows.Forms.Button
        Me.Label6 = New System.Windows.Forms.Label
        Me.Button7 = New System.Windows.Forms.Button
        Me.Button8 = New System.Windows.Forms.Button
        Me.Label7 = New System.Windows.Forms.Label
        Me.Button9 = New System.Windows.Forms.Button
        Me.Button10 = New System.Windows.Forms.Button
        Me.Button11 = New System.Windows.Forms.Button
        Me.Label8 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Button12 = New System.Windows.Forms.Button
        Me.Button13 = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 88)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(242, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Try Reassigning HTG, Species, Sizes or Densities"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(7, 106)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(69, 35)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "GO"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(7, 237)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(69, 35)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "GO"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 219)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(116, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Try Copying a Pathway"
        '
        'rbAttributesall
        '
        Me.rbAttributesall.AutoSize = True
        Me.rbAttributesall.Checked = True
        Me.rbAttributesall.Location = New System.Drawing.Point(92, 110)
        Me.rbAttributesall.Name = "rbAttributesall"
        Me.rbAttributesall.Size = New System.Drawing.Size(119, 17)
        Me.rbAttributesall.TabIndex = 4
        Me.rbAttributesall.TabStop = True
        Me.rbAttributesall.Text = ".attributesall Update"
        Me.rbAttributesall.UseVisualStyleBackColor = True
        '
        'rbRawData
        '
        Me.rbRawData.AutoSize = True
        Me.rbRawData.Location = New System.Drawing.Point(92, 133)
        Me.rbRawData.Name = "rbRawData"
        Me.rbRawData.Size = New System.Drawing.Size(221, 17)
        Me.rbRawData.TabIndex = 5
        Me.rbRawData.Text = "Raw Data Update (comma separated .txt)"
        Me.rbRawData.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(4, 286)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(117, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Try Deleting a Pathway"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(6, 299)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(69, 35)
        Me.Button3.TabIndex = 7
        Me.Button3.Text = "GO"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(142, 237)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(69, 35)
        Me.Button4.TabIndex = 9
        Me.Button4.Text = "GO"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(138, 219)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(113, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Try Copying a Process"
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(7, 364)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(69, 35)
        Me.Button5.TabIndex = 11
        Me.Button5.Text = "GO"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(5, 351)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(111, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Write Pathways to DB"
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(6, 35)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(69, 35)
        Me.Button6.TabIndex = 13
        Me.Button6.Text = "GO"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(2, 17)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(204, 13)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "List Illogical Vegetation/Missing Pathways"
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(57, 128)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(24, 25)
        Me.Button7.TabIndex = 14
        Me.Button7.Text = "?"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(16, 30)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(69, 35)
        Me.Button8.TabIndex = 16
        Me.Button8.Text = "GO"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(14, 17)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(112, 13)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "List Succession Times"
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(74, 52)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(27, 24)
        Me.Button9.TabIndex = 17
        Me.Button9.Text = "?"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(209, 51)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(27, 24)
        Me.Button10.TabIndex = 20
        Me.Button10.Text = "?"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(151, 29)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(69, 35)
        Me.Button11.TabIndex = 19
        Me.Button11.Text = "GO"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(149, 16)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(159, 13)
        Me.Label8.TabIndex = 18
        Me.Label8.Text = "Count Species Usage in Results"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button10)
        Me.GroupBox1.Controls.Add(Me.Button11)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Button9)
        Me.GroupBox1.Controls.Add(Me.Button8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Location = New System.Drawing.Point(21, 405)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(324, 85)
        Me.GroupBox1.TabIndex = 21
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Debugging Tools"
        '
        'Button12
        '
        Me.Button12.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button12.Location = New System.Drawing.Point(71, 156)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(76, 38)
        Me.Button12.TabIndex = 22
        Me.Button12.Text = "Write Blank Changes File"
        Me.Button12.UseVisualStyleBackColor = True
        '
        'Button13
        '
        Me.Button13.Location = New System.Drawing.Point(174, 156)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(66, 37)
        Me.Button13.TabIndex = 23
        Me.Button13.Text = "Create Field LUT"
        Me.Button13.UseVisualStyleBackColor = True
        '
        'PathwayEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(365, 502)
        Me.Controls.Add(Me.Button13)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.rbRawData)
        Me.Controls.Add(Me.rbAttributesall)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label1)
        Me.Name = "PathwayEdit"
        Me.Text = "Clean SIMPPLLE Input Data"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents rbAttributesall As System.Windows.Forms.RadioButton
    Friend WithEvents rbRawData As System.Windows.Forms.RadioButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents Button13 As System.Windows.Forms.Button
End Class
