Public Class clSpatialReassign
    'intent of this class is to reassign a single attribute (parent) of a polygon based on its spatial proximity to other polygons with the same attribute.
    'read in 5 files:
    ' 1. Polygon attribute file
    ' 2. Polygon adjacency file
    ' 3. Other Attribute values that must match in order to reassign the value of the parent attribute.
    ' 4. List of legal changes that can be made
    ' 5. List of total acreage desired in each parent class (goals)

    Dim BigM As Single = 99999999

    Dim PolyAttFN As String
    Dim PolyAdjFN As String
    Dim MatchCriteriaFN As String
    Dim LegalChangesFN As String
    Dim AcreGoalsFN As String
    Dim ParentFieldName As String
    Dim AcreFieldName As String
    Dim ParentFieldInd As Integer
    Dim AcreFieldInd As Integer

    'stuff about attributes
    Dim NAtts As Integer
    Dim AttName() As String
    Dim NPolys As Integer
    Dim PolyAtts(,) As String
    Dim OrigParentVal() As String 'original parent value
    Dim IterStartParentVal() As String 'parent value at the start of this iteration
    Dim CurrParentVal() As String
    Dim PolyAcres() As Single
    Dim PolyAdj(,) As Integer 'dimmed by poly ID, max Adj - holds values of the index of the adjacent polygon
    Dim PolyAdjLen(,) As Single 'dimmed by poly ID, max Adj - holds the values of the side length between the two polygons
    Dim NAdjPolys() As Integer 'number of polygons adjacent to this one
    Dim MaxAdj As Integer
    Dim NParentVals As Integer
    Dim ParentVals() As String
    Dim ParentValByIter(,) As String 'by poly, by iteration

    'stuff about matching
    Dim MaxAttVals As Integer
    Dim MatchAttVals(,,) As String 'by Parent,NAtts,MaxAttVals
    Dim NAttVals() As Integer 'number of unique attribute values for the attribute 
    Dim RequiredMatch(,) As Integer 'by NParentVals,Natts - stores 1 if you have to have a match on that attribute

    'legal switches
    Dim LegalSwitch(,) As Byte 'by parent,parent
    Dim bLegalSwitch() As Boolean


    'goals
    Dim ParentAreaGoal() As Single 'by nparents
    Dim ParentCurrentAcres() As Single 'by nparents
    Dim ParentIterStartAcres() As Single
    Dim ParentOriginalAcres() As Single
    Dim MaxLegalSwitches(,) As Single 'by nparents,nparents
    Dim LegalSwitchTracking(,) As Single 'by nparents,nparents
    Dim TargetValue(,,) As Long 'by nparentvals,natts, maxatts
    Dim TrackingArea(,,) As Long 'by nparentvals,natts,maxatts

    'iteration controls
    Dim NIters As Integer
    Dim IterTrackingAcres(,) As Single
    Dim Changed() As Byte
    Dim ChangedAcres() As Single
    Dim MaxIterSwitch As Single



    Public Sub RunSpatialReassign(ByVal modelfile As String, ByVal niterations As Integer, ByVal maxareaswitch As Single)

        Dim err As Boolean = False
        Dim startval As Integer
        Dim incrval As Integer

        NIters = niterations
        MaxIterSwitch = maxareaswitch

        'read all the input file stuff
        ReadModelFile(modelfile, err)
        If err = True Then
            MsgBox("Error in model file. Check file pathways.")
            Exit Sub
        End If
        ReadPolyAtts()
        ReadAdjFile()
        ReadLegalMatchCriteria()
        ReadLegalSwitches()
        ReadParentGoals()

        PopulateTrackingArea()

        'now go through the logic of doing the reassignment
        ReDim IterTrackingAcres(NIters, NParentVals) ', ParentValByIter(NPolys, NIters)
        TallyAcres(ParentOriginalAcres, "original")
        TallyAcres(ParentCurrentAcres, "current")
        TallyAcres(ParentIterStartAcres, "current")
        startval = 0
        incrval = 1
        ReDim ChangedAcres(NIters)
        For jiter As Integer = 1 To NIters
            AttributeReassignment(startval, incrval, jiter)
            'what are the outcome?
            TallyAcres(ParentCurrentAcres, "current")
            'TallyAcres(ParentIterStartAcres, "current")
            TallyAcreSwitches()
            For jparent As Integer = 1 To NParentVals
                IterTrackingAcres(jiter, jparent) = ParentCurrentAcres(jparent)
            Next
            startval = startval + (NPolys + 1) * incrval 'if you are at 0, nex time is NPolys +1, if you are at NPolys +1, next is 0
            incrval = incrval * -1

        Next jiter
        'TallyAcres(ParentCurrentAcres, "current")
        'For jparent As Integer = 1 To NParentVals
        '    IterTrackingAcres(NIters, jparent) = ParentCurrentAcres(jparent)
        'Next
        WriteResults(DirName(modelfile) & FName(modelfile))


    End Sub

    Private Sub AttributeReassignment(ByVal StartVal As Integer, ByVal incrval As Integer, ByVal kiter As Integer)
        'for each polygon, look at its Original Parent value. Are there rules to legally change it?
        'if it can be legally changed, find the adjacent polygon with the longest side. 
        '   Are the acres of the adjacent polygon's PARENT currently under represented?
        '   Are the attributes of this polygon legal to reassign to the PARENT of the adjacent polygon?
        'after all polygons are reassigned, tally up the total acres in each PARENT and compare them to the goals
        Dim klongestpoly As Integer
        Dim klongestparent As Integer
        Dim polyevaluated() As Byte
        Dim korigparent As Integer
        Dim kadjparent As Integer
        Dim kcurrparent As Integer
        Dim kpoly As Integer
        Dim AttVal As String 'attribute value to compare to
        'Dim kparent As Integer
        Dim neval As Integer
        Dim bswitched As Boolean
        Dim maxlegalswitch As Single
        Dim Evaluated(NPolys) As Byte

        ReDim Changed(NPolys), Evaluated(NPolys)

        kpoly = StartVal
        kpoly = Rnd(NPolys)
        ' Initialize the random-number generator.
        Randomize()
        For jpoly As Integer = 1 To NPolys

            If ChangedAcres(kiter) > MaxIterSwitch Then Exit For
            ' Generate random value between 1 and NPOLYS. 
            kpoly = CInt(Int((NPolys * Rnd()) + 1))
            Do
                If Evaluated(kpoly) = 0 Then
                    Evaluated(kpoly) = 1
                    Exit Do
                End If
                kpoly = kpoly + incrval
                If kpoly > NPolys Then kpoly = 1
                If kpoly < 1 Then kpoly = NPolys
            Loop
            If bLegalSwitch(kpoly) = True Then 'even worth evaluating?
                korigparent = GetStrIndex(ParentVals, OrigParentVal(kpoly))
                ReDim polyevaluated(NAdjPolys(kpoly))
                neval = 0
                bswitched = False
                Do Until neval >= NAdjPolys(kpoly) Or bswitched = True
                    neval = neval + 1
                    'find the polygon with the longest adjacent side
                    '###############DEBUG - USED TO BE FINDLONGESTVAL
                    klongestpoly = FindLongestVal(kpoly, polyevaluated)

                    'is the switch to this neighbor's PARENT value legal? Do you even need to do the switch TO the adjacent parent value? Is the neighbor's parent value different?
                    '################DEBUG - USED TO HAVE TO FIND THE PARENT VALUE OF THE ADJACENT PARENT
                    kadjparent = GetStrIndex(ParentVals, CurrParentVal(PolyAdj(kpoly, klongestpoly)))
                    'kadjparent = FindLongestBorder(kpoly, polyevaluated)
                    kcurrparent = GetStrIndex(ParentVals, CurrParentVal(kpoly))
                    maxlegalswitch = MaxLegalSwitches(korigparent, kadjparent)
                    If maxlegalswitch = 0 Then maxlegalswitch = BigM


                    '###############DEBUG - COMMENTED OUT CHECKING ADJACENT POLY SWITCH
                    If LegalSwitch(korigparent, kadjparent) = 1 And ParentCurrentAcres(kadjparent) < ParentAreaGoal(kadjparent) And LegalSwitchTracking(korigparent, kadjparent) < maxlegalswitch And kadjparent <> kcurrparent And Changed(klongestpoly) = 0 Then 'has to be both legal and underrepresented
                        'If LegalSwitch(korigparent, kadjparent) And LegalSwitchTracking(korigparent, kadjparent) < maxlegalswitch And kadjparent <> kcurrparent Then 'has to be both legal and underrepresented
                        'If LegalSwitch(korigparent, kadjparent) And LegalSwitchTracking(korigparent, kadjparent) < maxlegalswitch Then 'has to be both legal and underrepresented

                        'are the attributes of this polygon legal to switch to the parent value of this adjacent polygon?
                        'If MatchAtts(kpoly, kadjparent) = True Then
                        If MatchAttsAreaLegal(kpoly, kadjparent) = True Then
                            'do the switch!
                            CurrParentVal(kpoly) = ParentVals(kadjparent) 'IterStartParentVal(kadjparent)
                            bswitched = True 'should trigger end of looping through adjacent polygons
                            Changed(kpoly) = 1
                            'modify current acres
                            ParentCurrentAcres(kadjparent) = ParentCurrentAcres(kadjparent) + PolyAcres(kpoly)
                            ParentCurrentAcres(kcurrparent) = ParentCurrentAcres(kcurrparent) - PolyAcres(kpoly)
                            LegalSwitchTracking(korigparent, kadjparent) = LegalSwitchTracking(korigparent, kadjparent) + PolyAcres(kpoly)
                            If LegalSwitchTracking(korigparent, kcurrparent) > 0 Then LegalSwitchTracking(korigparent, kcurrparent) = LegalSwitchTracking(korigparent, kcurrparent) - PolyAcres(kpoly)
                            If kadjparent <> korigparent Then ChangedAcres(kiter) = ChangedAcres(kiter) + PolyAcres(kpoly)
                            'if you match and you are less than constraint acres, do the switch and modify the acres
                            For jatt As Integer = 1 To NAtts
                                AttVal = PolyAtts(kpoly, jatt)
                                For jval As Integer = 1 To NAttVals(jatt) ' MaxAttVals
                                    If AttVal = MatchAttVals(kadjparent, jatt, jval) Then TrackingArea(kadjparent, jatt, jval) = TrackingArea(kadjparent, jatt, jval) + PolyAcres(kpoly)
                                    If AttVal = MatchAttVals(kcurrparent, jatt, jval) Then TrackingArea(kcurrparent, jatt, jval) = TrackingArea(kcurrparent, jatt, jval) - PolyAcres(kpoly)
                                Next
                            Next

                        End If
                    End If
                Loop
            End If
        Next
        'set up for next iteration
        For jpoly As Integer = 1 To NPolys
            IterStartParentVal(jpoly) = CurrParentVal(jpoly)
            'ParentValByIter(jpoly, kiter) = CurrParentVal(jpoly)
        Next


    End Sub



    Private Sub ReadModelFile(ByVal infile As String, ByRef err As Boolean)

        Dim dummy As String
        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        dummy = Trim(LineInput(fnum)) 'header line with instructions
        dummy = Trim(LineInput(fnum))
        PolyAttFN = dummy
        If System.IO.File.Exists(PolyAttFN) = False Then GoTo errorhandler
        dummy = Trim(LineInput(fnum))
        PolyAdjFN = dummy
        If System.IO.File.Exists(PolyAdjFN) = False Then GoTo errorhandler
        dummy = Trim(LineInput(fnum))
        MatchCriteriaFN = dummy
        If System.IO.File.Exists(MatchCriteriaFN) = False Then GoTo errorhandler
        dummy = Trim(LineInput(fnum))
        LegalChangesFN = dummy
        If System.IO.File.Exists(LegalChangesFN) = False Then GoTo errorhandler
        dummy = Trim(LineInput(fnum))
        AcreGoalsFN = dummy
        If System.IO.File.Exists(AcreGoalsFN) = False Then GoTo errorhandler
        dummy = Trim(LineInput(fnum))
        ParentFieldName = dummy
        dummy = Trim(LineInput(fnum))
        AcreFieldName = dummy

        FileClose(fnum)
        Exit Sub

ErrorHandler:
        err = True
        FileClose(fnum)
        Exit Sub
    End Sub
    Private Sub ReadPolyAtts()
        Dim fnum As Integer
        Dim dummy As String
        Dim atts() As String
        Dim kpoly As Integer
        Dim kparent As Integer

        fnum = FreeFile()
        FileOpen(fnum, PolyAttFN, OpenMode.Input)
        dummy = LineInput(fnum)
        atts = Split(dummy, ",")
        NAtts = atts.Length
        ReDim AttName(NAtts)
        For jatt As Integer = 1 To NAtts
            AttName(jatt) = atts(jatt - 1)
            If AttName(jatt) = ParentFieldName Then ParentFieldInd = jatt - 1
            If AttName(jatt) = AcreFieldName Then AcreFieldInd = jatt - 1
        Next
        'next count the number of polygons
        NPolys = 0
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length > 0 Then
                NPolys = NPolys + 1
            End If
        Loop
        FileClose(fnum)

        ReDim PolyAtts(NPolys, NAtts), OrigParentVal(NPolys), CurrParentVal(NPolys), PolyAcres(NPolys), IterStartParentVal(NPolys)

        fnum = FreeFile()
        FileOpen(fnum, PolyAttFN, OpenMode.Input)
        dummy = LineInput(fnum) 'header
        kpoly = 0
        NParentVals = 0
        ReDim ParentVals(NParentVals)
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            atts = Split(dummy, ",")
            kpoly = kpoly + 1
            OrigParentVal(kpoly) = atts(ParentFieldInd)
            kparent = GetStrIndex(ParentVals, OrigParentVal(kpoly))
            If kparent < 0 Then
                NParentVals = NParentVals + 1
                ReDim Preserve ParentVals(NParentVals)
                ParentVals(NParentVals) = OrigParentVal(kpoly)
            End If
            CurrParentVal(kpoly) = OrigParentVal(kpoly)
            IterStartParentVal(kpoly) = OrigParentVal(kpoly)
            PolyAcres(kpoly) = atts(AcreFieldInd)
            For jatt As Integer = 1 To NAtts
                PolyAtts(kpoly, jatt) = atts(jatt - 1)
            Next
        Loop
        FileClose(fnum)

    End Sub
    Private Sub ReadAdjFile()
        Dim dummy As String
        Dim arr() As String
        Dim fnum As Integer

        Dim currind As Integer
        Dim testind As Integer
        Dim currNAdj As Integer 'count of adjacent polygons to this one

        fnum = FreeFile()
        FileOpen(fnum, PolyAdjFN, OpenMode.Input)
        dummy = LineInput(fnum) 'header
        currind = 0
        MaxAdj = 0
        currNAdj = 0
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length = 0 Then Exit Do
            arr = Split(dummy, ",")
            testind = arr(0)
            If testind <> currind Then
                MaxAdj = Math.Max(MaxAdj, currNAdj)
                currNAdj = 1
                currind = testind
            Else
                currNAdj = currNAdj + 1
            End If
        Loop
        FileClose(fnum)

        ReDim PolyAdj(NPolys, MaxAdj), PolyAdjLen(NPolys, MaxAdj), NAdjPolys(NPolys)

        fnum = FreeFile()
        FileOpen(fnum, PolyAdjFN, OpenMode.Input)
        dummy = LineInput(fnum) 'header
        currind = 0
        'MaxAdj = 0
        currNAdj = 0
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length = 0 Then Exit Do
            arr = Split(dummy, ",")
            testind = arr(0)
            If testind <> currind Then
                'MaxAdj = Math.Max(MaxAdj, currNAdj)
                currNAdj = 1
                currind = testind
            Else
                currNAdj = currNAdj + 1
            End If
            PolyAdj(currind, currNAdj) = arr(1)
            PolyAdjLen(currind, currNAdj) = arr(2)
            NAdjPolys(currind) = currNAdj
        Loop
        FileClose(fnum)



    End Sub
    Private Sub ReadLegalMatchCriteria()
        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String
        Dim kparent As Integer
        Dim katt As Integer
        Dim kval As Integer
        Dim AttValBin() As String
        Dim NAttValBin As Integer
        Dim attkcounter(,) As Integer 'by parent, att - index of the attribute value you are on for the parent/attribute that is a legal value

        ReDim RequiredMatch(NParentVals, NAtts), NAttVals(NAtts)
        NAttValBin = 0
        ReDim AttValBin(NAttValBin)

        fnum = FreeFile()
        FileOpen(fnum, MatchCriteriaFN, OpenMode.Input)
        dummy = LineInput(fnum) 'header line
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            arr = Split(dummy, ",")
            kparent = GetStrIndex(ParentVals, arr(0))
            katt = GetStrIndex(AttName, arr(1))
            RequiredMatch(kparent, katt) = 1 'in order to reassign the parent value, this attribute has to have a legal matching value
            kval = GetStrIndex(AttValBin, arr(2))
            If kval < 1 Then
                NAttValBin = NAttValBin + 1
                ReDim Preserve AttValBin(NAttValBin)
                AttValBin(NAttValBin) = arr(2)
                NAttVals(katt) = NAttVals(katt) + 1
            End If
        Loop
        FileClose(fnum)

        'find max attval
        MaxAttVals = 0
        For jatt As Integer = 1 To NAtts
            MaxAttVals = Math.Max(MaxAttVals, NAttVals(jatt))
        Next

        ReDim MatchAttVals(NParentVals, NAtts, MaxAttVals), attkcounter(NParentVals, NAtts)
        ReDim TargetValue(NParentVals, NAtts, MaxAttVals), TrackingArea(NParentVals, NAtts, MaxAttVals)

        'go through again to populate the list of legal attribute values
        fnum = FreeFile()
        FileOpen(fnum, MatchCriteriaFN, OpenMode.Input)
        dummy = LineInput(fnum) 'header line
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))

            arr = Split(dummy, ",")
            kparent = GetStrIndex(ParentVals, arr(0))
            katt = GetStrIndex(AttName, arr(1))
            attkcounter(kparent, katt) = attkcounter(kparent, katt) + 1
            MatchAttVals(kparent, katt, attkcounter(kparent, katt)) = arr(2)
            TargetValue(kparent, katt, attkcounter(kparent, katt)) = 0
            If arr(3) <> "" Then TargetValue(kparent, katt, attkcounter(kparent, katt)) = CType(arr(3), Long)
        Loop
        FileClose(fnum)

    End Sub
    Private Sub ReadLegalSwitches()
        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String
        Dim kparent As Integer
        Dim kparent2 As Integer
        Dim max As Single

        ReDim LegalSwitch(NParentVals, NParentVals), MaxLegalSwitches(NParentVals, NParentVals), LegalSwitchTracking(NParentVals, NParentVals)

        fnum = FreeFile()
        FileOpen(fnum, LegalChangesFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            arr = Split(dummy, ",")
            kparent = GetStrIndex(ParentVals, arr(0))
            kparent2 = GetStrIndex(ParentVals, arr(1))
            LegalSwitch(kparent, kparent2) = 1
            max = 0
            If arr(2) <> "" Then max = CType(arr(2), Single)
            MaxLegalSwitches(kparent, kparent2) = max

        Loop
        FileClose(fnum)

        'see which polygons have a legal switch
        ReDim bLegalSwitch(NPolys)

        For jpoly As Integer = 1 To NPolys
            kparent = GetStrIndex(ParentVals, OrigParentVal(jpoly))
            'is there a potential switch?
            bLegalSwitch(jpoly) = False
            For jpar As Integer = 1 To NParentVals
                If LegalSwitch(kparent, jpar) = 1 Then
                    bLegalSwitch(jpoly) = True
                    Exit For
                End If
            Next
        Next

    End Sub
    Private Sub ReadParentGoals()
        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String
        Dim kparent As Integer

        ReDim ParentAreaGoal(NParentVals)

        fnum = FreeFile()
        FileOpen(fnum, AcreGoalsFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            arr = Split(dummy, ",")
            If Trim(arr(0)) = "" Then Exit Do
            kparent = GetStrIndex(ParentVals, arr(0))
            ParentAreaGoal(kparent) = arr(1)
        Loop
        FileClose(fnum)
    End Sub

    Private Function FindLongestVal(ByVal kpoly As Integer, ByRef polyevaluated() As Byte) As Integer
        Dim longestval As Single = 0
        FindLongestVal = 0
        For jjpoly As Integer = 1 To NAdjPolys(kpoly)
            'find the longest polygon side
            If PolyAdjLen(kpoly, jjpoly) > longestval And polyevaluated(jjpoly) = 0 Then
                longestval = PolyAdjLen(kpoly, jjpoly)
                FindLongestVal = jjpoly
            End If
        Next
        polyevaluated(FindLongestVal) = 1
    End Function

    Private Function FindLongestBorder(ByVal kpoly As Integer, ByRef polyevaluated() As Byte) As Integer

        Dim ParentBorderVal() As Single
        Dim kparent As Integer
        Dim longestkparent As Integer
        Dim maxborder As Single
        ReDim ParentBorderVal(NParentVals)
        Dim longestval As Single = 0
        FindLongestBorder = 0

        For jjpoly As Integer = 1 To NAdjPolys(kpoly)
            If polyevaluated(jjpoly) = 0 Then
                kparent = GetStrIndex(ParentVals, IterStartParentVal(PolyAdj(kpoly, jjpoly)))
                ParentBorderVal(kparent) = ParentBorderVal(kparent) + PolyAdjLen(kpoly, jjpoly)
            End If
        Next
        'find the parent value associated with the longest side
        maxborder = -9999 'large low value
        For jparent As Integer = 1 To NParentVals
            If ParentBorderVal(jparent) > maxborder Then
                longestkparent = jparent
                maxborder = ParentBorderVal(jparent)
            End If
        Next

        FindLongestBorder = longestkparent

        'mark off that you have evaluated these polygons
        For jjpoly As Integer = 1 To NAdjPolys(kpoly)
            If polyevaluated(jjpoly) = 0 Then
                kparent = GetStrIndex(ParentVals, IterStartParentVal(PolyAdj(kpoly, jjpoly)))
                If kparent = longestkparent Then polyevaluated(jjpoly) = 1
            End If
        Next

    End Function

    Private Sub TallyAcres(ByRef ParentAcres() As Single, ByVal CurrOrig As String)
        Dim kparent As Integer
        ReDim ParentAcres(NParentVals)
        For jpoly As Integer = 1 To NPolys
            If CurrOrig = "current" Then kparent = GetStrIndex(ParentVals, CurrParentVal(jpoly))
            If CurrOrig = "original" Then kparent = GetStrIndex(ParentVals, OrigParentVal(jpoly))
            ParentAcres(kparent) = ParentAcres(kparent) + PolyAcres(jpoly)
        Next
    End Sub

    Private Sub TallyAcreSwitches()
        Dim korigparent As Integer
        Dim knewparent As Integer
        ReDim LegalSwitchTracking(NParentVals, NParentVals)
        For jpoly As Integer = 1 To NPolys
            knewparent = GetStrIndex(ParentVals, CurrParentVal(jpoly))
            korigparent = GetStrIndex(ParentVals, OrigParentVal(jpoly))
            If knewparent <> korigparent Then LegalSwitchTracking(korigparent, knewparent) = LegalSwitchTracking(korigparent, knewparent) + PolyAcres(jpoly)
        Next

    End Sub

    Private Function MatchAtts(ByVal kpoly As Integer, ByVal proposedkparent As Integer) As Boolean
        'purpose: see if the kpoly has attributes that are required to legally switch to the proposed parent value
        MatchAtts = True 'default; look for an exception
        Dim battmatch As Boolean = False
        Dim AttVal As String 'attribute value to compare to
        'RequiredMatch(kparent, katt) = 1 'in order to reassign the parent value, this attribute has to have a legal matching value
        'MatchAttVals(NParentVals, NAtts, MaxAttVals)

        'loop through all attributes
        For jatt As Integer = 1 To NAtts
            'is a match required?
            If RequiredMatch(proposedkparent, jatt) = 1 Then
                battmatch = False
                AttVal = PolyAtts(kpoly, jatt)
                For jval As Integer = 1 To NAttVals(jatt) 'MaxAttVals
                    If AttVal = MatchAttVals(proposedkparent, jatt, jval) Then
                        battmatch = True
                        Exit For
                    End If
                Next
                If battmatch = False Then 'means this attribute did not have a legal value. Move along...
                    MatchAtts = False
                    Exit Function
                End If
            End If
        Next

    End Function

    Private Function MatchAttsAreaLegal(ByVal kpoly As Integer, ByVal proposedkparent As Integer) As Boolean
        'Switch to it if the current acres are less than the goal, but not if more...
        MatchAttsAreaLegal = True 'default; look for an exception
        Dim battmatch As Boolean = False
        Dim tval As Double
        Dim AttVal As String 'attribute value to compare to

        'loop through all attributes
        For jatt As Integer = 1 To NAtts
            'is a match required?
            If RequiredMatch(proposedkparent, jatt) = 1 Then
                battmatch = False
                AttVal = PolyAtts(kpoly, jatt)
                For jval As Integer = 1 To NAttVals(jatt) 'MaxAttVals
                    tval = TargetValue(proposedkparent, jatt, jval)
                    If tval = 0 Then tval = BigM
                    'If tval < BigM Then Stop
                    If AttVal = MatchAttVals(proposedkparent, jatt, jval) And TrackingArea(proposedkparent, jatt, jval) < tval Then 'TargetValue(proposedkparent, jatt, jval) Then
                        battmatch = True
                        Exit For
                    End If
                Next
                If battmatch = False Then 'means this attribute did not have a legal value. Move along...
                    MatchAttsAreaLegal = False
                    Exit Function
                End If
            End If
        Next



    End Function

    Private Sub WriteResults(ByVal outfilebase As String)
        Dim fnum As Integer
        Dim dummy As String
        fnum = FreeFile()
        FileOpen(fnum, outfilebase & "_tallies.csv", OpenMode.Output)
        dummy = "Value,GoalAcres,OriginalAcres"
        For jiter As Integer = 1 To NIters
            dummy = dummy & ",Iter_" & jiter
        Next
        PrintLine(fnum, dummy)
        For jparent As Integer = 1 To NParentVals
            If ParentAreaGoal(jparent) > 0 Then
                dummy = ParentVals(jparent) & "," & Math.Round(ParentAreaGoal(jparent), 0) & "," & Math.Round(ParentOriginalAcres(jparent), 0) '& "," & Math.Round(ParentCurrentAcres(jparent), 0)
                For jiter As Integer = 1 To NIters
                    dummy = dummy & "," & Math.Round(IterTrackingAcres(jiter, jparent), 0)
                Next
                PrintLine(fnum, dummy)
            End If
        Next
        dummy = "changed_area,,"
        For jiter As Integer = 1 To NIters
            dummy = dummy & "," & Math.Round(ChangedAcres(jiter))
        Next
        PrintLine(fnum, dummy)

        'now print out the tracking acres
        PrintLine(fnum, "") 'dummy line
        PrintLine(fnum, "Parent,Attribute,AttributeValue,Goal,Acres")
        For jparent As Integer = 1 To NParentVals
            For jatt As Integer = 1 To NAtts
                For jval As Integer = 1 To NAttVals(jatt)
                    If MatchAttVals(jparent, jatt, jval) <> "" Then
                        dummy = ParentVals(jparent) & "," & AttName(jatt) & "," & MatchAttVals(jparent, jatt, jval) & "," & TargetValue(jparent, jatt, jval) & "," & TrackingArea(jparent, jatt, jval)
                        PrintLine(fnum, dummy)
                    End If
                Next
            Next

        Next
        FileClose(fnum)

        fnum = FreeFile()
        FileOpen(fnum, outfilebase & "_reassigned.csv", OpenMode.Output)
        PrintLine(fnum, "PolyID,Acres,OriginalValue,CurrentValue")
        For jpoly As Integer = 1 To NPolys
            dummy = jpoly & "," & PolyAcres(jpoly) & "," & OrigParentVal(jpoly) & "," & CurrParentVal(jpoly)
            'dummy = jpoly & "," & PolyAcres(jpoly) & "," & OrigParentVal(jpoly)
            'For jiter As Integer = 1 To NIters
            '    dummy = dummy + "," & ParentValByIter(jpoly, jiter)
            'Next jiter
            PrintLine(fnum, dummy)
        Next
        FileClose(fnum)
    End Sub

    Private Sub PopulateTrackingArea()
        'populates TrackingArea(NParentVals, NAtts, MaxAttVals)
        Dim AttVal As String
        Dim kparent As Integer
        For jpoly As Integer = 1 To NPolys
            'loop through all attributes
            kparent = GetStrIndex(ParentVals, OrigParentVal(jpoly))
            For jatt As Integer = 1 To NAtts

                AttVal = PolyAtts(jpoly, jatt)
                For jval As Integer = 1 To NAttVals(jatt) ' MaxAttVals
                    If AttVal = MatchAttVals(kparent, jatt, jval) Then
                        TrackingArea(kparent, jatt, jval) = TrackingArea(kparent, jatt, jval) + PolyAcres(jpoly)
                        Exit For
                    End If
                Next
            Next
        Next

    End Sub

End Class
