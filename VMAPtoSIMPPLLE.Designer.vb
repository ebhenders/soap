<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class VMAPtoSIMPPLLE
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button4 = New System.Windows.Forms.Button
        Me.tbVMAPtoSIMPPLLEMasterFile = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.CompileButton = New System.Windows.Forms.Button
        Me.cbKeepGoodCodes = New System.Windows.Forms.CheckBox
        Me.cbIterate = New System.Windows.Forms.CheckBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.tbNIterations = New System.Windows.Forms.TextBox
        Me.tbNSaveIterations = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.tbGoalTolerance = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(728, 89)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 19)
        Me.Button4.TabIndex = 28
        Me.Button4.Text = "Browse"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'tbVMAPtoSIMPPLLEMasterFile
        '
        Me.tbVMAPtoSIMPPLLEMasterFile.Location = New System.Drawing.Point(117, 89)
        Me.tbVMAPtoSIMPPLLEMasterFile.Name = "tbVMAPtoSIMPPLLEMasterFile"
        Me.tbVMAPtoSIMPPLLEMasterFile.Size = New System.Drawing.Size(596, 20)
        Me.tbVMAPtoSIMPPLLEMasterFile.TabIndex = 26
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(42, 89)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(69, 31)
        Me.Label4.TabIndex = 27
        Me.Label4.Text = "Model File:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(186, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(352, 24)
        Me.Label2.TabIndex = 25
        Me.Label2.Text = "Translate VMAP to SIMPPLLE Codes"
        '
        'CompileButton
        '
        Me.CompileButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CompileButton.Location = New System.Drawing.Point(423, 210)
        Me.CompileButton.Name = "CompileButton"
        Me.CompileButton.Size = New System.Drawing.Size(96, 32)
        Me.CompileButton.TabIndex = 29
        Me.CompileButton.Text = "GO"
        '
        'cbKeepGoodCodes
        '
        Me.cbKeepGoodCodes.AutoSize = True
        Me.cbKeepGoodCodes.Location = New System.Drawing.Point(121, 114)
        Me.cbKeepGoodCodes.Name = "cbKeepGoodCodes"
        Me.cbKeepGoodCodes.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cbKeepGoodCodes.Size = New System.Drawing.Size(142, 17)
        Me.cbKeepGoodCodes.TabIndex = 30
        Me.cbKeepGoodCodes.Text = "Keep Good Assignments"
        Me.cbKeepGoodCodes.UseVisualStyleBackColor = True
        '
        'cbIterate
        '
        Me.cbIterate.AutoSize = True
        Me.cbIterate.Location = New System.Drawing.Point(121, 137)
        Me.cbIterate.Name = "cbIterate"
        Me.cbIterate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cbIterate.Size = New System.Drawing.Size(56, 17)
        Me.cbIterate.TabIndex = 31
        Me.cbIterate.Text = "Iterate"
        Me.cbIterate.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(180, 163)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 17)
        Me.Label1.TabIndex = 32
        Me.Label1.Text = "Iterations"
        '
        'tbNIterations
        '
        Me.tbNIterations.Location = New System.Drawing.Point(121, 160)
        Me.tbNIterations.Name = "tbNIterations"
        Me.tbNIterations.Size = New System.Drawing.Size(53, 20)
        Me.tbNIterations.TabIndex = 33
        Me.tbNIterations.Text = "0"
        '
        'tbNSaveIterations
        '
        Me.tbNSaveIterations.Location = New System.Drawing.Point(121, 186)
        Me.tbNSaveIterations.Name = "tbNSaveIterations"
        Me.tbNSaveIterations.Size = New System.Drawing.Size(53, 20)
        Me.tbNSaveIterations.TabIndex = 34
        Me.tbNSaveIterations.Text = "10"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(180, 189)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(194, 17)
        Me.Label3.TabIndex = 35
        Me.Label3.Text = "Write Results For Last X Iterations"
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(179, 215)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(238, 27)
        Me.Label5.TabIndex = 37
        Me.Label5.Text = "Tolerance; areas within this percentage (0-100) of goal will be accepted"
        '
        'tbGoalTolerance
        '
        Me.tbGoalTolerance.Location = New System.Drawing.Point(121, 212)
        Me.tbGoalTolerance.Name = "tbGoalTolerance"
        Me.tbGoalTolerance.Size = New System.Drawing.Size(53, 20)
        Me.tbGoalTolerance.TabIndex = 36
        Me.tbGoalTolerance.Text = "20"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(12, 162)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(86, 54)
        Me.Button1.TabIndex = 38
        Me.Button1.Text = "Create Pathway Crosstable"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(76, 196)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(22, 20)
        Me.Button2.TabIndex = 39
        Me.Button2.Text = "?"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'VMAPtoSIMPPLLE
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(841, 265)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.tbGoalTolerance)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.tbNSaveIterations)
        Me.Controls.Add(Me.tbNIterations)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cbIterate)
        Me.Controls.Add(Me.cbKeepGoodCodes)
        Me.Controls.Add(Me.CompileButton)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.tbVMAPtoSIMPPLLEMasterFile)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Name = "VMAPtoSIMPPLLE"
        Me.Text = "VMAPtoSIMPPLLE"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents tbVMAPtoSIMPPLLEMasterFile As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents CompileButton As System.Windows.Forms.Button
    Friend WithEvents cbKeepGoodCodes As System.Windows.Forms.CheckBox
    Friend WithEvents cbIterate As System.Windows.Forms.CheckBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tbNIterations As System.Windows.Forms.TextBox
    Friend WithEvents tbNSaveIterations As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tbGoalTolerance As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
End Class
