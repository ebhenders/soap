Public Class BuildBatchFile
    'This leads you through the creation of the batch files used for SIMPPLLE output processing.
    'First, you need to define which runs you are going to process.
    'Once that is complete, you point to the initial conditions files you use to pair with those runs.

    Dim RunFolderName() As String
    Dim RunOutFileName() As String
    Dim RunNum() As Integer
    Dim NRunsToProcess As Integer
    Dim NICFileSets As Integer
    Dim NFilesICFileSet() As Integer
    Dim ICFileSetName() As String
    Dim AttributesallFiles() As String
    Dim SpatialrelateFiles() As String
    Dim UnzipSIMPPLLEFName() As String
    Dim foldername As String
    Dim BegInd As Integer = 1
    Dim bSaved As Boolean = False

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'IDENTIFY THE DIRECTORY WITH MODEL DEFINITION FILES IN IT
        Dim folderdialog2 As New FolderBrowserDialog
        Dim TempItems() As String
        Dim arr() As String
        Dim fldrarr() As String
        Dim fldrarr2() As String
        Dim runstr As String

        NumItems = 0
        Dim i As Integer = 0

        With folderdialog2
            .SelectedPath = foldername
            .Description = "Folder that contains run outputs to process (usually 'textdata')"
            '.Title = "Select a folder that has the "
            '.Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        foldername = folderdialog2.SelectedPath
        folderdialog2 = Nothing

        'populate the possible runs to process for the selector and then call the selector if you find a list of them...
        Dim fldr As New System.IO.DirectoryInfo(foldername)
        Dim files As System.IO.FileInfo() = fldr.GetFiles("*EVU_SIM_DATA*")
        Dim fi As System.IO.FileInfo
        NumItems = files.Length
        ReDim TempItems(NumItems)
        i = 0
        For Each fi In files
            arr = Split(fi.Name, ".")
            If cbBuildUnzipSIMPPLLE.Checked = False Then
                If arr(arr.Length - 1) <> "gz" Then 'filter out the zip files for the selector box
                    i = i + 1
                    TempItems(i) = fi.Name
                End If
            Else
                If arr(arr.Length - 1) = "gz" Then 'filter out the zip files for the selector box
                    i = i + 1
                    TempItems(i) = fi.Name
                End If
            End If
        Next
        NumItems = i
        If NumItems = 0 Then
            MsgBox("Folder contains no EVUSIMDATA files")
            Exit Sub
        End If

        ReDim ItemName(NumItems)
        For i = 1 To NumItems
            ItemName(i) = TempItems(i)
        Next
        runstr = NICFileSets
        fldrarr = Split(foldername, "\")
        If fldrarr.Length >= 2 Then
            fldrarr2 = Split(fldrarr(fldrarr.Length - 2), "-")
            runstr = fldrarr2(0)
        End If

        NICFileSets = NICFileSets + 1
        ReDim Preserve ICFileSetName(NICFileSets), NFilesICFileSet(NICFileSets)
        ICFileSetName(NICFileSets) = InputBox("Specify Run Name", runstr, runstr).ToString

        Dim SelectorFrm As New Selector
        SelectorText = "Choose Files To Process"
        SelectorFrm.ShowDialog()
        SelectorFrm = Nothing

        If FrmCancel = False Then
            ReDim Preserve RunFolderName(NRunsToProcess + ItemsToRead.Length - 1), RunOutFileName(NRunsToProcess + ItemsToRead.Length - 1), RunNum(NRunsToProcess + ItemsToRead.Length - 1)
            If cbBuildUnzipSIMPPLLE.Checked = True Then ReDim Preserve UnzipSIMPPLLEFName(NRunsToProcess + ItemsToRead.Length - 1)
            For j As Integer = BegInd To BegInd + ItemsToRead.Length - 2
                RunFolderName(j) = foldername '& "\" & ItemsToRead(j - BegInd + 1)
                RunOutFileName(j) = ICFileSetName(NICFileSets)
                arr = Split(ItemsToRead(j - BegInd + 1), ".")
                RunNum(j) = arr(0).Substring(12, arr(0).Length - 12) 'EVU_SIM_DATA is 11 long, so the number starts at 12, .txt is 4 long
                If cbBuildUnzipSIMPPLLE.Checked = True Then
                    UnzipSIMPPLLEFName(j) = ItemsToRead(j - BegInd + 1) 'RunFolderName(j) & "\" & ItemsToRead(j - BegInd + 1)
                End If
            Next
            NRunsToProcess = NRunsToProcess + ItemsToRead.Length - 1
            NFilesICFileSet(NICFileSets) = ItemsToRead.Length - 1
            BegInd = NRunsToProcess + 1
        Else
            NICFileSets = NICFileSets - 1
        End If


    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click

        If MsgBox("Are you sure?", MsgBoxStyle.OkCancel, "Confirm Cancellation") = MsgBoxResult.Cancel Then Exit Sub

        BegInd = 1
        NICFileSets = 0
        ReDim ICFileSetName(NICFileSets), NFilesICFileSet(NICFileSets)
        NRunsToProcess = 0
        ReDim RunFolderName(NRunsToProcess), RunOutFileName(NRunsToProcess), RunNum(NRunsToProcess)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'saves the initial conditions to a file
        Dim SaveFileDialog1 As New SaveFileDialog
        Dim UnzipSIMPPLLEFileName As String
        Dim GZipProgramName As String

        With SaveFileDialog1
            .Title = "Save the Runs To Process"
            .FileName = "RunsToProcess"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With


        lblRunsToProcessFile.Text = SaveFileDialog1.FileName
        OutFilesToProcessFileName = SaveFileDialog1.FileName
        QuerySIMPPLLEoutput.tbBatchFile.Text = OutFilesToProcessFileName

        SaveFileDialog1 = Nothing
        bSaved = True
        btnDefineICFiles.Enabled = True
        btnICFilesClear.Enabled = True

        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, OutFilesToProcessFileName, OpenMode.Output)
        For j As Integer = 1 To NRunsToProcess
            PrintLine(fnum, RunFolderName(j) & "," & RunOutFileName(j) & "," & RunNum(j))
        Next
        FileClose(fnum)
        If cbBuildUnzipSIMPPLLE.Checked = True Then
            Dim OpenFileDialog1 As New OpenFileDialog
            With OpenFileDialog1
                .Title = "Identify the GZIP.exe Program"
                .Filter = "Executables(.exe) |*.exe"
                If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
            End With
            GZipProgramName = OpenFileDialog1.FileName
            OpenFileDialog1 = Nothing


            UnzipSIMPPLLEFileName = DirName(OutFilesToProcessFileName) & FName(OutFilesToProcessFileName) & ".bat"
            QuerySIMPPLLEoutput.tbUnzipSIMPPLLE.Text = UnzipSIMPPLLEFileName
            fnum = FreeFile()
            FileOpen(fnum, UnzipSIMPPLLEFileName, OpenMode.Output)
            'PrintLine(fnum, "C:")

            For j As Integer = 1 To NRunsToProcess
                PrintLine(fnum, "cd " & RunFolderName(j))
                PrintLine(fnum, RunFolderName(j).Substring(0, 2))
                PrintLine(fnum, "copy " & UnzipSIMPPLLEFName(j) & " " & UnzipSIMPPLLEFName(j) & ".1")
                PrintLine(fnum, GZipProgramName & " -d -f " & UnzipSIMPPLLEFName(j))
                PrintLine(fnum, "ren " & UnzipSIMPPLLEFName(j) & ".1" & " " & UnzipSIMPPLLEFName(j))
            Next
            FileClose(fnum)
        End If


    End Sub

    Private Sub btnDefineICFiles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDefineICFiles.Click
        Dim OpenFileDialog1 As New OpenFileDialog
        'scroll through each set identified to select the .attributesall and .spatialrelate files for each
        ReDim AttributesallFiles(NICFileSets), SpatialrelateFiles(NICFileSets)
        For jset As Integer = 1 To NICFileSets
            With OpenFileDialog1
                .Title = "Select the .attributesall file associated with " & ICFileSetName(jset)
                .Filter = "Attributesall files (.attributesall) |*.attributesall"
                If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
            End With
            AttributesallFiles(jset) = OpenFileDialog1.FileName
            SpatialrelateFiles(jset) = DirName(AttributesallFiles(jset)) & FName(AttributesallFiles(jset)) & ".spatialrelate"
        Next
        OpenFileDialog1 = Nothing

        Dim SaveFileDialog1 As New SaveFileDialog

        With SaveFileDialog1
            .InitialDirectory = DirName(OutFilesToProcessFileName) '(System.IO.DirectoryInfo(OutFilesToProcesFileName)
            .Title = "Save the Initial Conditions File"
            .FileName = "ICFile"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        lblICFile.Text = SaveFileDialog1.FileName
        ICDefFileName = SaveFileDialog1.FileName
        QuerySIMPPLLEoutput.tbInitCondFiles.Text = ICDefFileName

        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, ICDefFileName, OpenMode.Output)
        PrintLine(fnum, "AttributesAll File Names")
        For jset As Integer = 1 To NICFileSets
            For jfile As Integer = 1 To NFilesICFileSet(jset)
                PrintLine(fnum, AttributesallFiles(jset))
            Next
        Next
        PrintLine(fnum, "Spatial Relate File Names")
        For jset As Integer = 1 To NICFileSets
            For jfile As Integer = 1 To NFilesICFileSet(jset)
                PrintLine(fnum, SpatialrelateFiles(jset))
            Next
        Next
        FileClose(fnum)

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        MsgBox("Define the Runs To Process one at a time. Click Save and name the runs. Then, click 'Define IC Files' and naviagate to the .attributesall file for each run")

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        MsgBox("SIMPPLLE outputs (EVUSIMDATA) sometimes come as zip (.gz) files that need to be extracted before processing. Checking this box will create a batch file to unzip all .gz files selected for result processing. However, if these files have already been unzipped, there is no need to unzip them again.")
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Me.Close()
    End Sub
End Class