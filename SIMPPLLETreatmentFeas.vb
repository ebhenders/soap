Public Class SIMPPLLETreatmentFeas
    Dim FNLSFiles As String
    Dim FNTreatmentFileList As String
    Dim FNDeficits As String

    Dim LSFiles() As String
    Dim NLSFiles As Integer
    Dim TreatmentFileList() As String
    Dim NTreatmentFiles As Integer

    Dim TimePers As Integer
    Dim NAggTrtTotal As Integer
    Dim AggTrtName() As String
    'Dim AggTrtAcres(,) As Integer


    Private Sub RunTreatmentFeasibility()
        Dim Outfile As String
        Dim fnum As Integer

        'load file information - runs to process and treatment files
        LoadFileStuff(FNTreatmentFileList, "Treatment Files", "LS Files", NTreatmentFiles, TreatmentFileList)
        LoadFileStuff(FNLSFiles, "LS Files", "", NLSFiles, LSFiles)

        'find the list of unique treatments in this run
        NAggTrtTotal = 0
        ReDim AggTrtName(NAggTrtTotal)
        For jfile As Integer = 1 To NLSFiles
            FindTotalListOfTreatments(TreatmentFileList(jfile))
        Next
        'assume these are matched pairs
        fnum = FreeFile()
        Outfile = DirName(FNDeficits) & FName(FNDeficits) & "_Filenames.txt"
        FileOpen(fnum, Outfile, OpenMode.Output)
        For jfile As Integer = 1 To NLSFiles
            Outfile = DirName(FNDeficits) & FName(FNDeficits) & "_" & FName(LSFiles(jfile)) & ".txt"
            PrintLine(fnum, Outfile)
            AssessTreatmentFeasibility(TreatmentFileList(jfile), LSFiles(jfile), Outfile)
        Next
        FileClose(fnum)

        MsgBox("Done")
    End Sub

    Private Sub AssessTreatmentFeasibility(ByVal TreatFN As String, ByVal LSFN As String, ByVal Outfile As String)
        Dim NTrtTotal As Integer
        Dim TrtName(,) As String
        Dim TrtAcres(,) As Integer
        Dim trtcount As Integer
        Dim trtindex As Integer
        Dim SubTimePers As Integer
        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String
        'Dim TimePers As Integer
        Dim ttime As Integer
        Dim ttrt As String
        Dim NSimulations As Integer
        Dim AggTrtAcres(,) As Integer
        Dim TrtAcresApplied(,,) As Single 'by treatment, time period, simulation
        'Dim Outfile As String
        Dim TotalDeficit As Single
        Dim TotalDeficitAllPeriods As Single



        trtcount = 0
        'load the information about what SHOULD have been treated
        fnum = FreeFile()
        FileOpen(fnum, TreatFN, OpenMode.Input)
        dummy = LineInput(fnum) '3
        dummy = LineInput(fnum) 'Westside Region One
        dummy = Trim(LineInput(fnum))
        NTrtTotal = dummy
        'first set is to get additional information
        dummy = LineInput(fnum)
        arr = Split(dummy, ",")
        SubTimePers = Trim(arr(0))
        trtcount = trtcount + 1
        ReDim TrtName(NTrtTotal, SubTimePers), TrtAcres(NTrtTotal, SubTimePers)
        TrtName(trtcount, SubTimePers) = Trim(arr(1))
        TrtAcres(trtcount, SubTimePers) = arr(2) / 100
        dummy = LineInput(fnum)
        dummy = LineInput(fnum)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length = 0 Then Exit Do
            arr = Split(dummy, ",")
            ttime = Trim(arr(0))
            trtcount = trtcount + 1
            TrtName(trtcount, ttime) = Trim(arr(1))
            TrtAcres(trtcount, ttime) = arr(2) / 100
            dummy = LineInput(fnum)
            If dummy.Length = 0 Then Exit Do
            dummy = LineInput(fnum)
            If dummy.Length = 0 Then Exit Do
            dummy = LineInput(fnum)
            If dummy.Length = 0 Then Exit Do
        Loop
        FileClose(fnum)


        'populate the acres treated according to the aggregated treatment names
        ReDim AggTrtAcres(NAggTrtTotal, TimePers)
        For jtrt As Integer = 1 To NTrtTotal
            For jper As Integer = 1 To SubTimePers
                trtindex = GetStrIndex(AggTrtName, TrtName(jtrt, jper))
                AggTrtAcres(trtindex, jper) = AggTrtAcres(trtindex, jper) + TrtAcres(jtrt, jper)
            Next
        Next

        'Now, load the LS file stuff
        fnum = FreeFile()
        FileOpen(fnum, LSFN, OpenMode.Input)
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            If dummy = "TREATMENT" Then Exit Do
        Loop
        'go through the treatment stuff...
        NSimulations = 0
        ReDim TrtAcresApplied(NAggTrtTotal, TimePers, NSimulations)
        If dummy = "TREATMENT" Then
            Do Until EOF(fnum)
                dummy = Trim(LineInput(fnum))
                If dummy = "" Then Exit Do
                If dummy = "SUPPRESSIONCOST" Then Exit Do
                'assume you have hit on a header line
                arr = Split(dummy, " ")
                If NSimulations = 0 Then
                    NSimulations = arr.Length - 1
                    ReDim TrtAcresApplied(NAggTrtTotal, TimePers, NSimulations)
                End If
                ttrt = arr(1).Substring(0, arr(1).Length - 2)
                trtindex = GetStrIndex(AggTrtName, ttrt)
                If trtindex > 0 Then
                    dummy = LineInput(fnum) 'time 0 not used
                    For jper As Integer = 1 To TimePers
                        dummy = LineInput(fnum)
                        arr = Split(dummy, " ")
                        For jsim As Integer = 1 To NSimulations
                            TrtAcresApplied(trtindex, jper, jsim) = Trim(arr(jsim))
                        Next
                    Next

                Else
                    For j As Integer = 0 To TimePers
                        dummy = Trim(LineInput(fnum))
                        If dummy = "" Then Exit Do
                    Next
                End If
            Loop
        End If
        FileClose(fnum)

        'Now, write out the deficit files...
        fnum = FreeFile()
        FileOpen(fnum, Outfile, OpenMode.Output)
        'PrintLine(fnum, "TREATMENT DEFICITS")
        dummy = ""
        For jtrt As Integer = 1 To NAggTrtTotal
            dummy = "TIME,"
            For jsim As Integer = 1 To NSimulations
                dummy = dummy & AggTrtName(jtrt) & "-" & jsim & ","
            Next
            dummy = dummy & "TARGET_ACRES,AVERAGE_DEFICIT"
            PrintLine(fnum, Trim(dummy))

            For jper As Integer = 1 To TimePers
                TotalDeficit = 0
                dummy = jper & ","
                For jsim As Integer = 1 To NSimulations
                    dummy = dummy & AggTrtAcres(jtrt, jper) - TrtAcresApplied(jtrt, jper, jsim) & ","
                    TotalDeficit = TotalDeficit + AggTrtAcres(jtrt, jper) - TrtAcresApplied(jtrt, jper, jsim)
                Next
                dummy = dummy & AggTrtAcres(jtrt, jper) & "," & Math.Round(TotalDeficit / NSimulations, 0)
                PrintLine(fnum, Trim(dummy))
            Next
        Next
        FileClose(fnum)
    End Sub

    Private Sub FindTotalListOfTreatments(ByVal infile As String)
        'this populates the AggTrtName and NAggTrtTotal stuff
        Dim fnum As Integer
        Dim dummy As String
        Dim trtcount As Integer
        Dim arr() As String
        Dim NTrtTotal As Integer
        Dim TrtName(,) As String
        Dim SubTimePers As Integer
        Dim ttime As Integer
        Dim trtindex As Integer

        trtcount = 0
        'load the information about what SHOULD have been treated
        fnum = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        dummy = LineInput(fnum) '3
        dummy = LineInput(fnum) 'Westside Region One
        dummy = Trim(LineInput(fnum))
        NTrtTotal = dummy
        'first set is to get additional information
        dummy = LineInput(fnum)
        arr = Split(dummy, ",")
        SubTimePers = Trim(arr(0))
        If SubTimePers > timepers Then timepers = SubTimePers
        trtcount = trtcount + 1
        ReDim TrtName(NTrtTotal, SubTimePers) ', TrtAcres(NTrtTotal, SubTimePers)
        TrtName(trtcount, SubTimePers) = Trim(arr(1))
        'TrtAcres(trtcount, SubTimePers) = arr(2) / 100
        dummy = LineInput(fnum)
        dummy = LineInput(fnum)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length = 0 Then Exit Do
            arr = Split(dummy, ",")
            ttime = Trim(arr(0))
            trtcount = trtcount + 1
            TrtName(trtcount, ttime) = Trim(arr(1))
            'TrtAcres(trtcount, ttime) = arr(2) / 100
            dummy = LineInput(fnum)
            If dummy.Length = 0 Then Exit Do
            dummy = LineInput(fnum)
            If dummy.Length = 0 Then Exit Do
            dummy = LineInput(fnum)
            If dummy.Length = 0 Then Exit Do
        Loop
        FileClose(fnum)
        'find Unique treatment names
        'NAggTrtTotal = 0
        'ReDim AggTrtName(NAggTrtTotal)
        For jtrt As Integer = 1 To NTrtTotal
            For jper As Integer = 1 To SubTimePers
                trtindex = GetStrIndex(AggTrtName, TrtName(jtrt, jper))
                If trtindex < 0 Then
                    NAggTrtTotal = NAggTrtTotal + 1
                    ReDim Preserve AggTrtName(NAggTrtTotal)
                    AggTrtName(NAggTrtTotal) = TrtName(jtrt, jper)
                End If
            Next
        Next
    End Sub
    Private Sub LoadFileStuff(ByVal filename As String, ByVal StartSearchString As String, ByVal EndSearchString As String, ByRef NItems As Integer, ByRef ItemNames() As String)
        Dim fnum As Integer
        Dim dummy As String
        Dim count As Integer
        fnum = FreeFile()
        NItems = 0
        FileOpen(fnum, filename, OpenMode.Input)
        dummy = ""
        Do Until dummy = StartSearchString
            dummy = Trim(LineInput(fnum))
            If dummy.Length = 0 Then Exit Do
            If dummy = EndSearchString Then Exit Do
        Loop
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length = 0 Then Exit Do
            If dummy = EndSearchString Then Exit Do
            NItems = NItems + 1
        Loop
        FileClose(fnum)

        'load the item names
        ReDim ItemNames(NItems)
        fnum = FreeFile()
        count = 0
        FileOpen(fnum, filename, OpenMode.Input)
        dummy = ""
        Do Until dummy = StartSearchString
            dummy = Trim(LineInput(fnum))
            If dummy.Length = 0 Then Exit Do
            If dummy = EndSearchString Then Exit Do
        Loop
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length = 0 Then Exit Do
            If dummy = EndSearchString Then Exit Do
            count = count + 1
            ItemNames(count) = dummy
        Loop
        FileClose(fnum)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'IDENTIFY THE FILE WITH TRANSLATION RULES
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select a file with names of the SIMPPLLE Treatment Input Files"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        tbTreatmentFileList.Text = openfiledialog2.FileName
        openfiledialog2 = Nothing
    End Sub




    Private Sub CompileButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CompileButton.Click
        'FIGURE OUT WHERE TO DUMP THE OUTPUTS
        Dim savefiledialog1 As New SaveFileDialog

        With savefiledialog1
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Save the outputs to:"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            .FileName = "TrtDeficits"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        FNDeficits = savefiledialog1.FileName

        FNLSFiles = tbTreatmentFileList.Text 'tbRunsToProcess.Text
        FNTreatmentFileList = tbTreatmentFileList.Text

        Call RunTreatmentFeasibility()
    End Sub
End Class

