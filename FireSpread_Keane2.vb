Imports System.Text
Public Class FireSpread_Keane2
    Dim NPolys As Integer
    Dim TimeSteps As Double
    Dim NFires As Integer

    Dim BurnQueueNow() As Byte
    Dim BurnQueueNext() As Byte
    Dim Burned() As Byte
    Dim TimeToBurn() As Double
    Dim PolyBurnedTime() As Single
    Dim Nrows As Integer
    Dim Ncols As Integer
    Dim PolyRow() As Integer
    Dim PolyCol() As Integer
    Dim ColRowPoly(,) As Integer
    Dim SpreadRateFrom(,) As Double
    Dim SpreadRateTo() As Double 'optional - the spread rate to get TO a polygon - can be used to distinguish water, NF, fuel breaks, etc.
    Dim BurnIter() As Integer
    Dim MaxTimePer As Integer
    Dim FireCells() As Integer
    Dim PolyQueueTime(,) As Integer

    Dim PolyElev() As Integer
    Dim SpreadTo(,) As Integer 'by source poly, max # neighbors
    Dim SpreadToDir(,) As Integer 'by source poly, max # neighbors - the degree of the spread direction
    Dim SpreadSlope(,) As Double 'by source poly, max neighbors
    Dim SpreadDist(,) As Double 'by source poly, max neighbors
    Dim PolyWindSpeed() As Integer 'by poly
    Dim PolyWindDir() As Integer 'by poly
    Dim PolyAcres() As Double



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'navigate to the file to load
        Dim infile As String
        Dim infile2 As String
        Dim outfile As String
        Dim burnpoly As Integer
        Dim arr() As String
        Dim arr2() As String



        'IDENTIFY THE file OF INITIAL CONDITIONS file names
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Load a Keane-formatted .spatialrelate file"
            .Filter = "Spatial Relate (.spatialrelate) |*.spatialrelate"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        infile = openfiledialog2.FileName
        openfiledialog2 = Nothing
        infile2 = DirName(infile) & FName(infile) & ".attributesall"
        outfile = DirName(infile) & FName(infile) & "_spread.txt"

        TimeSteps = tbTimeSteps.Text
        NFires = 0
        ReDim FireCells(NFires)

        MaxTimePer = 1

        LoadInput(infile, infile2)
        ReDim PolyQueueTime(NPolys, TimeSteps)
        'simulation cells  
        If rbCellsToStart.Checked = True Then
            arr = Split(tbCellsToStart.Text, ",")
            For jstep As Integer = 0 To arr.Length - 1
                arr2 = Split(arr(jstep), "-")
                For jstep2 As Integer = CType(Trim(arr2(0)), Integer) To CType(Trim(arr2(arr2.Length - 1)), Integer)
                    NFires = NFires + 1
                    ReDim Preserve FireCells(NFires)
                    FireCells(NFires) = jstep2 'CType(Trim(arr2(NTimestep - 1)), Integer)
                Next
            Next
        Else
            NFires = tbNFires.Text
            ReDim FireCells(NFires)
            For jfire As Integer = 1 To NFires
                FireCells(jfire) = Math.Round(Rnd() * NPolys, 0)
            Next
        End If

        ReDim Burned(NPolys) 'universal; used to simulate multiple fires
        For jfire As Integer = 1 To NFires
            burnpoly = FireCells(jfire)
            DoBurn(burnpoly, TimeSteps)
        Next
        'debug
        'DoBurn(837, TimeSteps)
        WriteOutput(outfile)

        MsgBox("Done with fire spread")

    End Sub

    Private Sub DoBurn(ByVal StartPoly As Integer, ByVal BurnTime As Double)
        Dim BurnedTime As Double = 0
        Dim currmintime As Double
        Dim nextmintime As Double
        Dim kcount As Integer = 1


        ReDim BurnQueueNow(NPolys), BurnQueueNext(NPolys)
        ReDim TimeToBurn(NPolys)
        For jpoly As Integer = 1 To NPolys
            TimeToBurn(jpoly) = 999999
        Next

        'initiate the fire
        currmintime = 999999
        nextmintime = 0
        BurnIter(StartPoly) = kcount + SpreadRateTo(StartPoly) 'allows for fire starts in fuel breaks, NF, etc. to be delayed or not burn at all
        If BurnIter(StartPoly) <= BurnTime Then BurnPoly(StartPoly, nextmintime, currmintime)
        BurnedTime = BurnIter(StartPoly)
        PolyBurnedTime(StartPoly) = BurnedTime
        For jpoly As Integer = 1 To NPolys
            BurnQueueNow(jpoly) = BurnQueueNext(jpoly)
            BurnQueueNext(jpoly) = 0
            PolyQueueTime(jpoly, BurnedTime) = TimeToBurn(jpoly) 'iSPIXVal(jpoly)
        Next
        nextmintime = 999999

        Do Until BurnedTime >= BurnTime
            kcount = kcount + 1
            MaxTimePer = Math.Max(kcount, MaxTimePer)
            For jpoly As Integer = 1 To NPolys
                If BurnQueueNow(jpoly) = 1 Then
                    'check to see if it should be burned
                    If TimeToBurn(jpoly) <= currmintime And Burned(jpoly) = 0 Then
                        'burn this guy!
                        BurnPoly(jpoly, currmintime, nextmintime)
                        BurnIter(jpoly) = kcount
                        PolyBurnedTime(jpoly) = BurnedTime
                    Else
                        'bank it for next time around
                        BurnQueueNext(jpoly) = 1
                        BurnQueueNow(jpoly) = 0
                        TimeToBurn(jpoly) = Math.Round(TimeToBurn(jpoly) - currmintime, 2)
                        nextmintime = Math.Min(nextmintime, TimeToBurn(jpoly))
                    End If
                End If
            Next

            For jpoly As Integer = 1 To NPolys
                BurnQueueNow(jpoly) = BurnQueueNext(jpoly)
                BurnQueueNext(jpoly) = 0
                PolyQueueTime(jpoly, BurnedTime) = TimeToBurn(jpoly) 'iSPIXVal(jpoly)
            Next
            BurnedTime = BurnedTime + currmintime
            currmintime = Math.Max(nextmintime, 0.1)
            currmintime = Math.Round(currmintime, 2)
            nextmintime = 999999
        Loop
    End Sub

    Private Sub LoadInput(ByVal infile As String, ByVal infile2 As String)
        Dim dummy As String
        Dim d1 As Integer
        Dim d2 As Integer
        Dim d3 As Integer
        Dim fnum As Integer
        Dim arr() As String
        Dim maxSLINK As Integer = -1
        Dim maxROW As Integer = -1
        Dim maxCOL As Integer = -1
        Dim lastSLINK As Integer
        Dim kToSLINK As Integer
        Dim kpoly As Integer

        fnum = FreeFile()
        FileOpen(fnum, infile2, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Or dummy = "END" Then Exit Do
            arr = Split(dummy, ",")
            d1 = CType(arr(0), Integer)
            maxSLINK = Math.Max(d1, maxSLINK)
            d2 = CType(arr(1), Integer)
            maxCOL = Math.Max(d2, maxCOL)
            d3 = CType(arr(2), Integer)
            maxROW = Math.Max(d3, maxROW)
        Loop
        NPolys = maxSLINK
        Ncols = maxCOL
        Nrows = maxROW
        FileClose(fnum)

        ReDim ColRowPoly(Ncols + 1, Nrows + 1), SpreadRateTo(NPolys), PolyRow(NPolys), PolyCol(NPolys), PolyAcres(NPolys)

        fnum = FreeFile()
        FileOpen(fnum, infile2, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Or dummy = "END" Then Exit Do
            arr = Split(dummy, ",")
            d1 = CType(arr(0), Integer)
            d2 = CType(arr(1), Integer) 'col
            d3 = CType(arr(2), Integer) 'row
            PolyCol(d1) = d2
            PolyRow(d1) = d3
            ColRowPoly(d2, d3) = d1
            SpreadRateTo(d1) = 0
            If arr(8) = "NF" Then SpreadRateTo(d1) = 8888
            If arr(8) = "WATER" Then SpreadRateTo(d1) = 9999
            PolyAcres(d1) = CType(arr(4), Double)
        Loop
        FileClose(fnum)

        ReDim BurnIter(NPolys), PolyBurnedTime(NPolys) ', SpreadRateTo(NPolys) ', SpreadRateFrom(NPolys, 7), SpreadRateTo(NPolys)
        ReDim PolyElev(NPolys), SpreadTo(NPolys, 7), SpreadToDir(NPolys, 7), PolyWindSpeed(NPolys), PolyWindDir(NPolys), SpreadSlope(NPolys, 7), SpreadDist(NPolys, 7)


        fnum = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        dummy = LineInput(fnum)
        lastSLINK = 0
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Or dummy = "END" Then Exit Do
            arr = Split(dummy, ",")
            d1 = CType(arr(0), Integer)
            If d1 <> lastSLINK Then
                kToSLINK = -1
                lastSLINK = d1
            End If
            kToSLINK = kToSLINK + 1
            PolyElev(d1) = CType(arr(2), Integer)
            SpreadTo(d1, kToSLINK) = CType(arr(1), Integer) 'slink index we are spreading to
            SpreadToDir(d1, kToSLINK) = CType(arr(3), Integer)
            PolyWindSpeed(d1) = CType(arr(4), Integer)
            PolyWindDir(d1) = CType(arr(5), Integer)
        Loop
        FileClose(fnum)

        'calculate stuff
        For jpoly As Integer = 1 To NPolys
            For jjpoly As Integer = 0 To 7
                kpoly = SpreadTo(jpoly, jjpoly)
                If kpoly > 0 Then
                    If SpreadToDir(jpoly, jjpoly) = 0 Or SpreadToDir(jpoly, jjpoly) = 90 Or SpreadToDir(jpoly, jjpoly) = 180 Or SpreadToDir(jpoly, jjpoly) = 270 Or SpreadToDir(jpoly, jjpoly) = 360 Then
                        SpreadDist(jpoly, jjpoly) = Math.Sqrt(PolyAcres(jpoly) * 4046.856)
                    Else
                        SpreadDist(jpoly, jjpoly) = Math.Sqrt(PolyAcres(jpoly) * 4046.856) * Math.Sqrt(2)
                    End If
                    SpreadSlope(jpoly, jjpoly) = 0
                    If (PolyElev(kpoly) - PolyElev(jpoly)) <> 0 Then SpreadSlope(jpoly, jjpoly) = (PolyElev(kpoly) - PolyElev(jpoly)) / SpreadDist(jpoly, jjpoly) 'convert to percent slope
                End If
            Next
        Next




    End Sub


    Private Sub BurnPoly(ByVal originpoly As Integer, ByVal currmintime As Double, ByRef nextmintime As Double)
        Dim kpoly As Integer
        Burned(originpoly) = 1
        BurnQueueNow(originpoly) = 0 'burned, so take it out
        BurnQueueNext(originpoly) = 0
        'n
        kpoly = ColRowPoly(PolyCol(originpoly), PolyRow(originpoly) + 1)
        AddBurnQueue(originpoly, kpoly, 0, currmintime, nextmintime)
        'ne
        kpoly = ColRowPoly(PolyCol(originpoly) + 1, PolyRow(originpoly) + 1)
        AddBurnQueue(originpoly, kpoly, 1, currmintime, nextmintime)
        'e
        kpoly = ColRowPoly(PolyCol(originpoly) + 1, PolyRow(originpoly))
        AddBurnQueue(originpoly, kpoly, 2, currmintime, nextmintime)
        'se
        kpoly = ColRowPoly(PolyCol(originpoly) + 1, PolyRow(originpoly) - 1)
        AddBurnQueue(originpoly, kpoly, 3, currmintime, nextmintime)
        's
        kpoly = ColRowPoly(PolyCol(originpoly), PolyRow(originpoly) - 1)
        AddBurnQueue(originpoly, kpoly, 4, currmintime, nextmintime)
        'sw
        kpoly = ColRowPoly(PolyCol(originpoly) - 1, PolyRow(originpoly) - 1)
        AddBurnQueue(originpoly, kpoly, 5, currmintime, nextmintime)
        'w
        kpoly = ColRowPoly(PolyCol(originpoly) - 1, PolyRow(originpoly))
        AddBurnQueue(originpoly, kpoly, 6, currmintime, nextmintime)
        'nw
        kpoly = ColRowPoly(PolyCol(originpoly) - 1, PolyRow(originpoly) + 1)
        AddBurnQueue(originpoly, kpoly, 7, currmintime, nextmintime)
    End Sub
    Private Sub AddBurnQueue(ByVal originpoly As Integer, ByVal kpoly As Integer, ByVal kdir As Integer, ByVal currmintime As Double, ByRef nextmintime As Double)

        'need to calculate the SPIX for this originpoly to poly combination
        Dim dSPIX As Double
        'Dim iSPIX As Integer
        Dim spixdiff As Double
        Dim windf As Double
        Dim slopef As Double
        Dim kkpoly As Integer
        Dim coeff As Double
        Dim lwr As Double

        Dim windSpeed As Integer
        Dim windDirection As Integer
        Dim spreadDirection As Integer
        Dim slope As Double

        'would this poly have burned anyway?
        'if BurnQueueNow = 1 and timetoburn < currmintime then YES - exit the sub
        If BurnQueueNow(kpoly) = 1 And TimeToBurn(kpoly) <= currmintime Then Exit Sub 'it will burn when you get there...

        'find spreadTo poly
        kkpoly = -1
        For jjpoly As Integer = 0 To 7
            If SpreadTo(originpoly, jjpoly) = kpoly Then
                kkpoly = jjpoly
                Exit For
            End If
        Next
        'If kkpoly = -1 Then Stop


        If kpoly > 0 And Burned(kpoly) = 0 Then

            windSpeed = Math.Min(PolyWindSpeed(originpoly), 30)
            windSpeed = Math.Max(windSpeed, 0) 'not less than 0
            windDirection = PolyWindDir(originpoly)
            If windDirection > 360 Then windDirection = windDirection - 360
            If windDirection < 0 Then windDirection = 360 + windDirection
            spreadDirection = SpreadToDir(originpoly, kkpoly)
            slope = SpreadSlope(originpoly, kkpoly)

            If windSpeed > 0.5 Then
                coeff = (Math.PI / 180) * Math.Abs(windDirection - spreadDirection)
                lwr = 1 + (0.125 * windSpeed)
                coeff = (Math.Cos(coeff) + 1) / 2
                windf = lwr * Math.Pow(coeff, Math.Pow(windSpeed, 0.6))
            Else
                windf = 1
            End If

            If slope > 0 Then
                slopef = 5 / (1 + 3.5 * Math.Exp(-10 * slope))
            Else
                slopef = Math.Exp(-3 * slope * slope)
            End If
            dSPIX = windf + slopef
            If spreadDirection = 45 Or spreadDirection = 135 Or spreadDirection = 225 Or spreadDirection = 315 Then
                dSPIX = dSPIX / Math.Sqrt(2)
            End If



            'have to update the burn time - decrease by the current burn time. This is done for both BurnQueNow = 0 and 1, and only is relevant when BurnQueNow = 1
            'Once BurnQueueNext is set to 1 it means you have decreased TimeToBurn by the currmintime - either here or in the BurnPoly sub
            If BurnQueueNext(kpoly) = 0 Then TimeToBurn(kpoly) = TimeToBurn(kpoly) - currmintime 'need to set this first to evaluate rate of spread from the current set of burning polygons

            'This is where you set the initial time to burn for the poly that is something other than the default Big-M. 
            '  This can also set the minimum time to burn from different directions if more than 1 adjacent polygon starts burning in this iteration
            '  or in the case where a newly burning polygon is adjacent to a polygon already in the queue and gets fire faster from the newly burning polygon than the old one
            'If SpreadRateFrom(originpoly, kdir) + SpreadRateTo(kpoly) < TimeToBurn(kpoly) Then TimeToBurn(kpoly) = SpreadRateFrom(originpoly, kdir) + SpreadRateTo(kpoly)
            TimeToBurn(kpoly) = Math.Min(TimeToBurn(kpoly), Math.Round(1 / dSPIX, 2) + SpreadRateTo(kpoly))
            TimeToBurn(kpoly) = Math.Round(TimeToBurn(kpoly), 2)
            BurnQueueNext(kpoly) = 1
            BurnQueueNow(kpoly) = 0 'it wouldn't have burned now anyway
            nextmintime = Math.Min(TimeToBurn(kpoly), nextmintime)
        End If



    End Sub

    Private Sub WriteOutput(ByVal outfile As String)
        Dim fnum As Integer = FreeFile()
        Dim dummy As String
        Dim dummy2 As String
        Dim sdummy As Stringbuilder
        Dim sdummy2 As stringbuilder
        Dim bOrigin As Boolean
        Dim Started() As Boolean

        ReDim Started(NPolys)
        FileOpen(fnum, outfile, OpenMode.Output)
        Dim fnum2 As Integer = FreeFile()
        FileOpen(fnum2, DirName(outfile) & FName(outfile) & "_bq.txt", OpenMode.Output)
        dummy = "Poly"
        For j As Integer = 1 To TimeSteps
            dummy = dummy & ",B_" & j
        Next
        PrintLine(fnum, dummy)
        PrintLine(fnum2, dummy)
        For jpoly As Integer = 1 To NPolys
            'If jpoly = 15663 Then Stop
            sdummy = New StringBuilder(jpoly.ToString, 300)
            sdummy2 = New StringBuilder(jpoly.ToString, 300)
            bOrigin = False
            For jstart As Integer = 1 To NFires
                If jpoly = FireCells(jstart) Then
                    bOrigin = True
                    Exit For
                End If
            Next
            For j As Integer = 1 To TimeSteps 'MaxTimePer
                If Burned(jpoly) = 0 Then
                    sdummy.Append("," & SpreadRateTo(jpoly))
                    sdummy2.Append("," & PolyQueueTime(jpoly, j))
                Else
                    If j < PolyBurnedTime(jpoly) Then
                        sdummy.Append("," & SpreadRateTo(jpoly))
                        sdummy2.Append("," & PolyQueueTime(jpoly, j))
                    End If


                    If j >= PolyBurnedTime(jpoly) And PolyBurnedTime(jpoly) < j + 1 And Started(jpoly) = False Then
                        sdummy.Append(",1")
                        sdummy2.Append(",-1")
                        Started(jpoly) = True
                    End If

                    If j > PolyBurnedTime(jpoly) And bOrigin = False Then
                        sdummy.Append(",2")
                        sdummy2.Append(",-2")
                    End If

                    If j > PolyBurnedTime(jpoly) And bOrigin = True Then
                        sdummy.Append(",3")
                        sdummy2.Append(",-3")
                    End If

                End If

            Next

            PrintLine(fnum, sdummy.ToString)
            PrintLine(fnum2, sdummy2.ToString)
        Next

        FileClose(fnum)
        FileClose(fnum2)
    End Sub
End Class