Public Class frmBuildBatchFile2
    Dim foldername As String
    Dim NRunsToProcess As Integer = 0
    Dim RunSetName() As String
    Dim RunName() As String
    Dim RunICFileName() As String
    Dim RunOutFileName() As String
    Dim bSaved As Boolean
    Dim outfile As String


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'IDENTIFY THE DIRECTORY WITH MODEL DEFINITION FILES IN IT
        Dim folderdialog2 As New FolderBrowserDialog
        Dim TempItems() As String
        Dim arr() As String



        NumItems = 0
        Dim i As Integer = 0

        With folderdialog2
            .SelectedPath = foldername
            .Description = "Folder that contains run outputs to query (usually 'textdata')"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        foldername = folderdialog2.SelectedPath
        folderdialog2 = Nothing

        'populate the possible runs to process for the selector and then call the selector if you find a list of them...
        Dim fldr As New System.IO.DirectoryInfo(foldername)
        Dim files As System.IO.FileInfo() = fldr.GetFiles("*EVU_SIM_DATA*")
        Dim fi As System.IO.FileInfo
        NumItems = files.Length
        ReDim TempItems(NumItems)
        i = 0
        For Each fi In files
            arr = Split(fi.Name, ".")

            If arr(arr.Length - 1) = "txt" Or arr(arr.Length - 1) = "csv" Then 'filter out stuff that is not a run report
                i = i + 1
                TempItems(i) = fi.Name
            End If

        Next
        NumItems = i
        If NumItems = 0 Then
            MsgBox("Folder contains no EVU_SIM_DATA files")
            Exit Sub
        End If

        ReDim ItemName(NumItems)
        For i = 1 To NumItems
            ItemName(i) = TempItems(i)
        Next

        Dim SelectorFrm As New Selector
        SelectorText = "Choose Files To Process"
        SelectorFrm.ShowDialog()
        SelectorFrm = Nothing

        If FrmCancel = False And ItemsToRead.Length > 0 Then
            For j As Integer = 1 To ItemsToRead.Length - 1
                lvRunsToQuery.Items.Add(foldername & "\" & ItemsToRead(j))
            Next j

        End If

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim fnum As Integer
        '
        Dim outdirectory As String

        outdirectory = DirName(tbQueryFile.Text)

        If System.IO.Directory.Exists(outdirectory) = False Then
            SaveQueryBatchFile()
            If bSaved = False Then Exit Sub
        End If


        outfile = tbQueryFile.Text

        fnum = FreeFile()
        FileOpen(fnum, outfile, OpenMode.Output) 'could overwrite an existing file of the same name
        For Each item As ListViewItem In lvRunsToQuery.Items
            PrintLine(fnum, item.Text)
        Next
        FileClose(fnum)

        frmQuerySIMPPLLEoutputNEW.tbBatchFile.Text = outfile

        Me.Close()


    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        For Each item As ListViewItem In lvRunsToQuery.SelectedItems
            item.Remove()
        Next
    End Sub

    Private Sub SaveQueryBatchFile()
        bSaved = False
        'saves the initial conditions to a file
        Dim SaveFileDialog1 As New SaveFileDialog
      

        With SaveFileDialog1
            .Title = "Save the Run Query Batch File"
            .FileName = "RunsToQuery"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With


        tbQueryFile.Text = SaveFileDialog1.FileName
        outfile = SaveFileDialog1.FileName
        If System.IO.File.Exists(outfile) Then LoadQueryBatchFile(outfile)

        SaveFileDialog1 = Nothing
        bSaved = True

    End Sub

    Private Sub frmBuildBatchFile2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        tbQueryFile.Text = frmQuerySIMPPLLEoutputNEW.tbBatchFile.Text

        If System.IO.File.Exists(tbQueryFile.Text) Then
            LoadQueryBatchFile(tbQueryFile.Text)
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        SaveQueryBatchFile()
    End Sub

    Private Sub LoadQueryBatchFile(ByVal infile As String)
        Dim dummy As String
        Dim fnum As Integer = FreeFile()

        lvRunsToQuery.Items.Clear() 'empty the current list

        FileOpen(fnum, infile, OpenMode.Input)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If Trim(dummy).Length > 0 Then
                lvRunsToQuery.Items.Add(dummy)
            Else
                FileClose(fnum)
                Exit Do
            End If
        Loop
        FileClose(fnum)
    End Sub
End Class
