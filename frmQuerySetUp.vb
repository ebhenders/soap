Imports System.Text
Public Class frmQuerySetUp
    Dim foldername As String
    Dim ProcessNameLUT() As String
    Dim NProcessLUT As Integer
    Dim SizeClassNameLUT() As String
    Dim NSizeClassLUT As Integer
    Dim DensityNameLUT() As String
    Dim NDensityLUT As Integer
    Dim HabGroupNameLUT() As String
    Dim NHabGroupLUT As Integer
    Dim SpeciesNameLUT() As String
    Dim NSpeciesLUT As Integer
    Dim FireProcessNameLUT() As String
    Dim NFireProcessLUT As Integer
    Dim OwnershipName() As String
    Dim NOwnerships As Integer
    Dim SpecialAreaName() As String
    Dim NSpecialAreas As Integer

    'for writing query files
    Dim HTGNameLUTq() As String
    Dim NHTGLUTq As Integer
    Dim SizeClassNameLUTq() As String
    Dim NSizeClassLUTq As Integer
    Dim DensityNameLUTq() As String
    Dim NDensityLUTq As Integer
    Dim HabGroupNameLUTq() As String
    Dim NHabGroupLUTq As Integer
    Dim SpeciesNameLUTq() As String
    Dim NSpeciesLUTq As Integer

    Dim bCheckModelDefs As Boolean
    Dim bCheckSIMPPLLEOutputs As Boolean
    Dim bCheckAttributesallFile As Boolean

    'Dim NewCodes() As String
    'Dim NNewCodes As Integer

    Dim CodesAddedFN As String

    Dim NGroups As Integer
    Dim GroupName() As String
    Dim GroupInd() As Integer
    Dim QueryDefGroup() As Integer 'group index of the line definition
    Dim NQueryDefs As Integer
    Dim QueryDefOwn() As String 'the ownership field name of the query definition
    Dim QueryDefSA() As String 'the special area filed name of the query definition
    'Dim MaxIndex As Integer
    Dim allflag As String = "<all>"

    Dim NewSA() As String
    Dim UpdateInd As Integer



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        MsgBox("This helps make sure you have all the necessary files and codes to run the SIMPPLLE query model" & Chr(13) & Chr(13) _
        & "Step 1 is required, Steps 2-4 will fill default values if not completed at this time")
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'IDENTIFY THE DIRECTORY WITH MODEL DEFINITION FILES IN IT
        Dim folderdialog2 As New FolderBrowserDialog
        'Dim bDensity As Boolean = False
        'Dim bFire As Boolean = False
        'Dim bHab As Boolean = False
        'Dim bOwnership As Boolean = False
        'Dim bProcess As Boolean = False
        'Dim bSizeClass As Boolean = False
        'Dim bSpecialArea As Boolean = False
        'Dim bSpecies As Boolean = False
        'Dim bSuiteCodesToUse As Boolean = False
        Dim bConDef As Boolean = False
        Dim bDiscrete As Boolean = False

        Dim fnum As Integer

        foldername = ""
        NumItems = 0
        Dim i As Integer = 0

        With folderdialog2
            .SelectedPath = foldername
            .Description = "Folder that contains query definition files"
            '.Title = "Select a folder that has the "
            '.Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        foldername = folderdialog2.SelectedPath
        folderdialog2 = Nothing
        QuerySIMPPLLEoutput.tbModelDefsDir.Text = foldername 'automatically populate this on the main form

        'track newly added codes
        CodesAddedFN = foldername & "\" & "CODESADDED.txt"

        'populate the possible runs to process for the selector and then call the selector if you find a list of them...
        Dim fldr As New System.IO.DirectoryInfo(foldername)
        Dim files As System.IO.FileInfo() = fldr.GetFiles '("*EVU_SIM_DATA*")
        Dim fi As System.IO.FileInfo
        NumItems = files.Length
        ReDim ItemName(NumItems)
        If NumItems = 0 Then  'And cbCreateWLDefinitionFiles.Checked = False Then
            MsgBox("Folder contains no  files")
            Exit Sub
        End If
        For Each fi In files
            i = i + 1
            ItemName(i) = fi.Name

            If ItemName(i) = "WildlifeContinuousDefinitions.csv" Then bConDef = True
            If ItemName(i) = "WildlifeDiscreteDefinitions.csv" Then bDiscrete = True

        Next
        'If cbCreateWLDefinitionFiles.Checked = False Then
        'these two are killers - can't make them for you
        If bDiscrete = False Then
            MsgBox("Could not find WildlifeDiscreteDefinitions.csv")
            Exit Sub
        End If
        If bConDef = False Then
            MsgBox("Could not find WildlifeContinuousDefinitions.csv")
            Exit Sub
        End If

        bCheckModelDefs = cbCheckModelDefs.Checked
        bCheckSIMPPLLEOutputs = cbCheckSIMPPLLEOutputs.Checked
        bCheckAttributesallFile = cbCheckAttributesallFile.Checked


        DefineLUTFileNames(foldername)
        LoadLUTs()
        fnum = FreeFile()
        FileOpen(fnum, CodesAddedFN, OpenMode.Output)
        PrintLine(fnum, "Codes added that were not in the initial ModelDef Files")
        PrintLine(fnum, "")
        If bCheckModelDefs = True Then FillcodesWLDefs(fnum)
        If bCheckSIMPPLLEOutputs = True Then FillCodesRunOutput(fnum)
        If bCheckAttributesallFile = True Then FillCodesAttributesAll(fnum)

        FileClose(fnum)
        MsgBox("Done")
        'Else
        'FillCombosUsedRunOutput(foldername & "\WildlifeDiscreteDefinitions.csv")
        'MsgBox("Done. File written to: " & foldername & "\WildlifeDiscreteDefinitions.csv")
        'End If

    End Sub

    Private Sub FillCodesRunOutput(ByVal fnumnewcodes As Integer)
        'uses the output files from a run to fill in information about processes, species, density, habitat groups, size classes
        Dim dummy As String
        Dim arr() As String
        Dim dummyL As String
        Dim k As Integer
        Dim fnum As Integer
        Dim runoutfolder As String
        Dim ext As String = ".txt"

        Dim folderdialog2 As New FolderBrowserDialog
        With folderdialog2
            .SelectedPath = foldername
            .Description = "Folder that has SIMPPLLE run outputs (usually /textdata)"
            '.Title = "Select a folder that has the "
            '.Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Exit Sub
        End With




        runoutfolder = folderdialog2.SelectedPath
        folderdialog2 = Nothing
        PrintLine(fnumnewcodes, "Codes Added from SIMPPLLE output")

        'test the file extension
        If System.IO.File.Exists(runoutfolder & "\" & "DENSITY.csv") Then ext = "csv"

        'read the density file
        fnum = FreeFile()
        FileOpen(fnum, runoutfolder & "\" & "DENSITY." & ext, OpenMode.Input)
        dummy = LineInput(fnum)

        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length = 0 Then Exit Do
            arr = Split(dummy, ",")
            dummy = Trim(arr(1))
            k = GetStrIndex(DensityNameLUT, dummy) 'CType(arr(3), Integer) 'GetIndex(DensityNameLUT, Trim(arr(3)))
            If k < 0 Then
                NDensityLUT = NDensityLUT + 1
                'NNewCodes = NNewCodes + 1
                ReDim Preserve DensityNameLUT(NDensityLUT) ', NewCodes(NNewCodes)
                DensityNameLUT(NDensityLUT) = dummy
                ' NewCodes(NNewCodes) = dummy
                dummyL = NDensityLUT & "," & dummy
                AddLine(DensityLUTFN, dummyL)
                PrintLine(fnumnewcodes, "DENSITY," & dummy)
            End If

        Loop
        FileClose(fnum)

        ''read the ecogroup file
        'fnum = FreeFile()
        'FileOpen(fnum, runoutfolder & "\" & "ECOGROUP.txt", OpenMode.Input)
        'dummy = LineInput(fnum)

        'Do Until EOF(fnum)
        '    dummy = Trim(LineInput(fnum))
        '    If dummy.Length = 0 Then Exit Do
        '    arr = Split(dummy, ",")
        '    dummy = Trim(arr(1))
        '    k = GetStrIndex(HabGroupNameLUT, dummy)
        '    If k < 0 Then
        '        NHabGroupLUT = NHabGroupLUT + 1
        '        'NNewCodes = NNewCodes + 1
        '        ReDim Preserve HabGroupNameLUT(NHabGroupLUT) ', NewCodes(NNewCodes)
        '        HabGroupNameLUT(NHabGroupLUT) = dummy
        '        'NewCodes(NNewCodes) = dummy
        '        dummyL = NHabGroupLUT & "," & dummy
        '        AddLine(HabGroupLUTFN, dummyL)
        '        PrintLine(fnumnewcodes, "ECOGROUP," & dummy)
        '    End If

        'Loop
        'FileClose(fnum)

        'read the process file
        fnum = FreeFile()
        FileOpen(fnum, runoutfolder & "\" & "PROCESS." & ext, OpenMode.Input)
        dummy = LineInput(fnum)

        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length = 0 Then Exit Do
            arr = Split(dummy, ",")
            dummy = Trim(arr(1))
            k = GetStrIndex(ProcessNameLUT, dummy)
            If k < 0 Then
                NProcessLUT = NProcessLUT + 1
                ' NNewCodes = NNewCodes + 1
                ReDim Preserve ProcessNameLUT(NProcessLUT) ', NewCodes(NNewCodes)
                ProcessNameLUT(NProcessLUT) = dummy
                'NewCodes(NNewCodes) = dummy
                dummyL = NProcessLUT & "," & dummy
                AddLine(AllProcessLUTFN, dummyL)
                PrintLine(fnumnewcodes, "PROCESS," & dummy)
            End If

        Loop
        FileClose(fnum)

        'read the sizeclass file
        fnum = FreeFile()
        FileOpen(fnum, runoutfolder & "\" & "SIZECLASS." & ext, OpenMode.Input)
        dummy = LineInput(fnum)

        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length = 0 Then Exit Do
            arr = Split(dummy, ",")
            dummy = Trim(arr(1))
            k = GetStrIndex(SizeClassNameLUT, dummy)
            If k < 0 Then
                NSizeClassLUT = NSizeClassLUT + 1
                'NNewCodes = NNewCodes + 1
                ReDim Preserve SizeClassNameLUT(NSizeClassLUT) ', NewCodes(NNewCodes)
                SizeClassNameLUT(NSizeClassLUT) = dummy
                'NewCodes(NNewCodes) = dummy
                dummyL = NSizeClassLUT & "," & dummy
                AddLine(SizeClassLUTFN, dummyL)
                PrintLine(fnumnewcodes, "SIZECLASS," & dummy)
            End If

        Loop
        FileClose(fnum)

        'read the species file
        fnum = FreeFile()
        FileOpen(fnum, runoutfolder & "\" & "SPECIES." & ext, OpenMode.Input)
        dummy = LineInput(fnum)

        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length = 0 Then Exit Do
            arr = Split(dummy, ",")
            dummy = Trim(arr(1))
            k = GetStrIndex(SpeciesNameLUT, dummy)
            If k < 0 Then
                NSpeciesLUT = NSpeciesLUT + 1
                'NNewCodes = NNewCodes + 1
                ReDim Preserve SpeciesNameLUT(NSpeciesLUT) ', NewCodes(NNewCodes)
                SpeciesNameLUT(NSpeciesLUT) = dummy
                'NewCodes(NNewCodes) = dummy
                dummyL = NSpeciesLUT & "," & dummy
                AddLine(SpeciesLUTFN, dummyL)
                PrintLine(fnumnewcodes, "SPECIES," & dummy)
            End If

        Loop
        PrintLine(fnumnewcodes, "")
        FileClose(fnum)
    End Sub
    Private Sub FillCombosUsedRunOutput(ByVal outfile As String)
        Dim dummy As String
        Dim arr() As String
        Dim arr2() As String
        Dim khtg As Integer
        Dim kspec As Integer
        Dim ksize As Integer
        Dim kdensity As Integer
        Dim HTG As String
        Dim i As Integer
        Dim NFiles As Integer
        Dim FileName() As String
        Dim fnum As Integer
        Dim fnum2 As Integer
        Dim folder As String
        Dim UseCombo(,,,) As Byte
        Dim cdfilename As String

        'do a folderbrowser to get the folder with pathway files
        Dim folderdialog2 As New FolderBrowserDialog
        With folderdialog2
            .SelectedPath = foldername
            .Description = "Folder that has exported .txt pathway files (1 per EcoGroup)"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Exit Sub
        End With

        folder = folderdialog2.SelectedPath

        Dim fldr As New System.IO.DirectoryInfo(folder)
        Dim files As System.IO.FileInfo() = fldr.GetFiles '("*EVU_SIM_DATA*")
        Dim fi As System.IO.FileInfo


        'initiate these arrays
        NHTGLUTq = 0
        ReDim HTGNameLUTq(NHTGLUTq)
        NSizeClassLUTq = 0
        ReDim SizeClassNameLUTq(NSizeClassLUTq)
        SizeClassNameLUTq(0) = "" 'have to give it a value...
        NDensityLUTq = 0
        ReDim DensityNameLUTq(NDensityLUTq)
        NHabGroupLUTq = 0
        ReDim HabGroupNameLUTq(NHabGroupLUTq)
        NSpeciesLUTq = 0
        ReDim SpeciesNameLUTq(NSpeciesLUTq)


        NFiles = 0
        ReDim FileName(NFiles)
        For Each fi In files
            i = i + 1
            If fi.Extension = ".txt" Then
                NFiles = NFiles + 1
                ReDim Preserve FileName(NFiles)
                FileName(NFiles) = fi.FullName
            End If
        Next

        'first identify all possible species, size, and density values from the pathway files
        For jfile As Integer = 1 To NFiles
            fnum = FreeFile()
            FileOpen(fnum, FileName(jfile), OpenMode.Input)
            'header line contains the habitat type group
            dummy = LineInput(fnum)
            arr = Split(dummy, " ")
            HTG = Trim(arr(1))
            khtg = GetStrIndex(HTGNameLUTq, HTG)
            If khtg < 0 Then
                NHTGLUTq = NHTGLUTq + 1
                ReDim Preserve HTGNameLUTq(NHTGLUTq)
                HTGNameLUTq(NHTGLUTq) = HTG
            End If
            Do Until EOF(fnum)
                dummy = Trim(LineInput(fnum))
                If dummy.Length > 0 Then
                    arr = Split(dummy, " ")
                    If arr(0) = "VEGETATIVE-TYPE" Then
                        arr2 = Split(arr(1), "/")
                        kspec = GetStrIndex(SpeciesNameLUTq, arr2(0))
                        If kspec < 0 Then
                            NSpeciesLUTq = NSpeciesLUTq + 1
                            ReDim Preserve SpeciesNameLUTq(NSpeciesLUTq)
                            SpeciesNameLUTq(NSpeciesLUTq) = arr2(0)
                        End If

                        ksize = GetSizeInd(SizeClassNameLUTq, arr2(1))
                        If ksize < 0 Then
                            NSizeClassLUTq = NSizeClassLUTq + 1
                            ReDim Preserve SizeClassNameLUTq(NSizeClassLUTq)
                            SizeClassNameLUTq(NSizeClassLUTq) = arr2(1)
                        End If

                        kdensity = GetStrIndex(DensityNameLUTq, arr2(2))
                        If kdensity < 0 Then
                            NDensityLUTq = NDensityLUTq + 1
                            ReDim Preserve DensityNameLUTq(NDensityLUTq)
                            DensityNameLUTq(NDensityLUTq) = arr2(2)
                        End If
                    End If
                End If
            Loop
            FileClose(fnum)
        Next

        'then, identify the pathway HTG/Species/Size/Density combinations that are in the model.

        ReDim UseCombo(NHTGLUTq, NSpeciesLUTq, NSizeClassLUTq, NDensityLUTq)
        For jfile As Integer = 1 To NFiles
            fnum = FreeFile()
            FileOpen(fnum, FileName(jfile), OpenMode.Input)
            'header line contains the habitat type group
            dummy = LineInput(fnum)
            arr = Split(dummy, " ")
            HTG = Trim(arr(1))
            khtg = GetStrIndex(HTGNameLUTq, HTG)

            Do Until EOF(fnum)
                dummy = Trim(LineInput(fnum))
                If dummy.Length > 0 Then
                    arr = Split(dummy, " ")
                    If arr(0) = "VEGETATIVE-TYPE" Then
                        arr2 = Split(arr(1), "/")
                        kspec = GetStrIndex(SpeciesNameLUTq, arr2(0))
                        ksize = GetSizeInd(SizeClassNameLUTq, arr2(1))
                        kdensity = GetStrIndex(DensityNameLUTq, arr2(2))
                        UseCombo(khtg, kspec, ksize, kdensity) = 1
                    End If
                End If
            Loop
            FileClose(fnum)
            'write outputs

        Next
        fnum2 = FreeFile()
        FileOpen(fnum2, outfile, OpenMode.Output)
        PrintLine(fnum2, "HTG,SPECIES,SIZE,DENSITY,SAMPLE_QUERY_SIZE")
        For jhtg As Integer = 1 To NHTGLUTq
            For jspec As Integer = 1 To NSpeciesLUTq
                For jsize As Integer = 1 To NSizeClassLUTq
                    For jdensity As Integer = 1 To NDensityLUTq
                        If UseCombo(jhtg, jspec, jsize, jdensity) = 1 Then
                            PrintLine(fnum2, HTGNameLUTq(jhtg) & "," & SpeciesNameLUTq(jspec) & "," & SizeClassNameLUTq(jsize) & "," & DensityNameLUTq(jdensity) & "," & SizeClassNameLUTq(jsize))
                        End If
                    Next
                Next
            Next
        Next jhtg
        FileClose(fnum2)

        'create the WildlifeContinuousDefinitions file

        cdfilename = DirName(outfile) & "WildlifeContinuousDefinitions.csv"
        fnum = FreeFile()
        FileOpen(fnum, cdfilename, OpenMode.Output)
        PrintLine(fnum, "Metric,SAMPLE_QUERY_SIZE,")
        PrintLine(fnum, "min elevation,")
        PrintLine(fnum, "max elevation,")
        PrintLine(fnum, "min patch size,")
        PrintLine(fnum, "max patch size,")
        PrintLine(fnum, "min periods since process,")
        PrintLine(fnum, "max periods since process,")
        PrintLine(fnum, "Use Conditions Before Process (0/1),")
        FileClose(fnum)

        'create LUTs for DENSITY, HTG, SIZECLASS, SPECIES
        cdfilename = DirName(outfile) & "DENSITY_LUT.txt"
        fnum = FreeFile()
        FileOpen(fnum, cdfilename, OpenMode.Output)
        PrintLine(fnum, "INDEX,DENSITYCLASS")
        For jdensity As Integer = 1 To NDensityLUTq
            PrintLine(fnum, jdensity & "," & DensityNameLUTq(jdensity))
        Next
        FileClose(fnum)

        cdfilename = DirName(outfile) & "HABGROUP_LUT.txt"
        fnum = FreeFile()
        FileOpen(fnum, cdfilename, OpenMode.Output)
        PrintLine(fnum, "Index,HabGrp")
        For jhtg As Integer = 1 To NHTGLUTq
            PrintLine(fnum, jhtg & "," & HTGNameLUTq(jhtg))
        Next
        FileClose(fnum)

        cdfilename = DirName(outfile) & "SIZECLASS_LUT.txt"
        fnum = FreeFile()
        FileOpen(fnum, cdfilename, OpenMode.Output)
        PrintLine(fnum, "ID,SIZECLASS")
        For jsize As Integer = 1 To NSizeClassLUTq
            PrintLine(fnum, jsize & "," & SizeClassNameLUTq(jsize))
        Next
        FileClose(fnum)

        cdfilename = DirName(outfile) & "SPECIES_LUT.txt"
        fnum = FreeFile()
        FileOpen(fnum, cdfilename, OpenMode.Output)
        PrintLine(fnum, "Index,SPECIES")
        For jspec As Integer = 1 To NSpeciesLUTq
            PrintLine(fnum, jspec & "," & SpeciesNameLUTq(jspec))
        Next
        FileClose(fnum)




    End Sub



    Private Sub FillCodesAttributesAll(ByVal fnumnewcodes As Integer)
        'reads an attributesall file to fill in Ownership and Special Area files
        Dim MaxIndex As Integer
        Dim fnum As Integer = FreeFile()
        Dim dummy As String
        Dim dummyL As String
        Dim arr() As String
        Dim slink As Integer
        Dim ko As Integer
        Dim ks As Integer
        Dim adj As Integer = 0 'column index adjustment
        Dim oldnew As String = "old" 'indicates format of the attributesall file
        Dim defaultacres As Integer = 10
        Dim InitCondFN As String

        Dim OpenFileDialog1 As New OpenFileDialog
        'scroll through each set identified to select the .attributesall and .spatialrelate files for each

        With OpenFileDialog1
            .Title = "Select the .attributesall file associated with the query"
            .Filter = "Attributesall files (.attributesall) |*.attributesall"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        InitCondFN = OpenFileDialog1.FileName
        OpenFileDialog1 = Nothing

        PrintLine(fnumnewcodes, "Codes Added from Reading the .attributesall file")

        'need to figure out the name of the file to open

        'determine file format
        fnum = FreeFile()
        FileOpen(fnum, InitCondFN, OpenMode.Input)
        dummy = LineInput(fnum)
        dummy = LineInput(fnum)
        arr = Split(dummy, ",")
        If Trim(arr(4)) <> "#" Then
            oldnew = "new"
            adj = 2
        End If
        FileClose(fnum)
        'Then, get TimeZero metrics

        MaxIndex = 0
        fnum = FreeFile()
        FileOpen(fnum, InitCondFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If Trim(dummy) = "END" Then Exit Do
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                dummy = arr(15 + adj)
                ko = GetStrIndex(OwnershipName, dummy)
                If ko < 0 Then
                    NOwnerships = NOwnerships + 1
                    ReDim Preserve OwnershipName(NOwnerships)
                    OwnershipName(NOwnerships) = dummy
                    dummyL = NOwnerships & "," & dummy
                    AddLine(OwnershipLUTFN, dummyL)
                    PrintLine(fnumnewcodes, "OWNERSHIP," & dummy)
                End If
                dummy = arr(19 + adj)
                ks = GetStrIndex(SpecialAreaName, dummy)
                If ks < 0 Then
                    NSpecialAreas = NSpecialAreas + 1
                    ReDim Preserve SpecialAreaName(NSpecialAreas)
                    SpecialAreaName(NSpecialAreas) = dummy
                    dummyL = NSpecialAreas & "," & dummy
                    AddLine(SpecialAreaLUTFN, dummyL)
                    PrintLine(fnumnewcodes, "SPECIAL_AREA," & dummy)
                End If

            End If
        Loop
        PrintLine(fnumnewcodes, "")
        FileClose(fnum)

    End Sub
    Private Sub FillcodesWLDefs(ByVal fnumnewcodes As Integer)
        Dim dummy As String
        Dim dummyl As String
        Dim fnum As Integer
        Dim arr() As String
        Dim khg As Integer
        Dim kct As Integer
        Dim ksc As Integer
        Dim kdn As Integer

        'NNewCodes = 0
        'ReDim NewCodes(NNewCodes)

        PrintLine(fnumnewcodes, "Codes Added from Reading the WildlifeQueryDefinitions file")

        'LOAD WILDLIFE QUERY FILE - discrete values
        NWLSpecies = 0
        fnum = FreeFile()
        FileOpen(fnum, WLDiscreteDefsLUTFN, OpenMode.Input)
        dummy = LineInput(fnum)


        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length = 0 Then Exit Do
            arr = Split(dummy, ",")
            dummy = Trim(arr(0))
            khg = GetStrIndex(HabGroupNameLUT, dummy)
            If khg < 0 Then
                NHabGroupLUT = NHabGroupLUT + 1
                'NNewCodes = NNewCodes + 1
                ReDim Preserve HabGroupNameLUT(NHabGroupLUT) ', NewCodes(NNewCodes)
                HabGroupNameLUT(NHabGroupLUT) = dummy
                'NewCodes(NNewCodes) = dummy
                dummyl = NHabGroupLUT & "," & dummy
                AddLine(HabGroupLUTFN, dummyl)
                PrintLine(fnumnewcodes, "ECOGROUP," & dummy)
            End If
            dummy = Trim(arr(1))
            kct = GetStrIndex(SpeciesNameLUT, dummy)
            If kct < 0 Then
                NSpeciesLUT = NSpeciesLUT + 1
                'NNewCodes = NNewCodes + 1
                ReDim Preserve SpeciesNameLUT(NSpeciesLUT) ', NewCodes(NNewCodes)
                SpeciesNameLUT(NSpeciesLUT) = dummy
                'NewCodes(NNewCodes) = dummy
                dummyl = NSpeciesLUT & "," & dummy
                AddLine(SpeciesLUTFN, dummyl)
                PrintLine(fnumnewcodes, "SPECIES," & dummy)
            End If
            dummy = Trim(arr(2))
            ksc = GetStrIndex(SizeClassNameLUT, dummy)
            If ksc < 0 Then
                NSizeClassLUT = NSizeClassLUT + 1
                ' NNewCodes = NNewCodes + 1
                ReDim Preserve SizeClassNameLUT(NSizeClassLUT) ', NewCodes(NNewCodes)
                SizeClassNameLUT(NSizeClassLUT) = dummy
                'NewCodes(NNewCodes) = dummy
                dummyl = NSizeClassLUT & "," & dummy
                AddLine(SizeClassLUTFN, dummyl)
                PrintLine(fnumnewcodes, "SIZECLASS," & dummy)
            End If
            dummy = Trim(arr(3))
            kdn = GetStrIndex(DensityNameLUT, dummy) 'CType(arr(3), Integer) 'GetIndex(DensityNameLUT, Trim(arr(3)))
            If kdn < 0 Then
                NDensityLUT = NDensityLUT + 1
                'NNewCodes = NNewCodes + 1
                ReDim Preserve DensityNameLUT(NDensityLUT) ', NewCodes(NNewCodes)
                DensityNameLUT(NDensityLUT) = dummy
                'NewCodes(NNewCodes) = dummy
                dummyl = NDensityLUT & "," & dummy
                AddLine(DensityLUTFN, dummyl)
                PrintLine(fnumnewcodes, "DENSITY," & dummy)
            End If

        Loop
        PrintLine(fnumnewcodes, "")
        FileClose(fnum)


    End Sub
    Private Sub LoadLUTs()
        'files with full lists of all potential values...

        Dim MaxIndex As Integer
        Dim dummy As String
        Dim arr() As String
        Dim fnum As Integer = FreeFile()


        If System.IO.File.Exists(AllProcessLUTFN) = True Then
            'load ALLPROCESSLUT
            MaxIndex = 0
            ReDim ProcessNameLUT(MaxIndex)
            fnum = FreeFile()
            FileOpen(fnum, AllProcessLUTFN, OpenMode.Input)
            dummy = LineInput(fnum)
            Do Until EOF(fnum)
                dummy = LineInput(fnum)
                If dummy.Length > 0 Then
                    arr = Split(dummy, ",")
                    MaxIndex = MaxIndex + 1
                    ReDim Preserve ProcessNameLUT(MaxIndex)
                    ProcessNameLUT(MaxIndex) = CType(Trim(arr(1)), String)
                End If
            Loop
            NProcessLUT = MaxIndex
            FileClose(fnum)
        Else
            fnum = FreeFile()
            FileOpen(fnum, AllProcessLUTFN, OpenMode.Output)
            PrintLine(fnum, "ID,PROCESS")
            FileClose(fnum)
            ReDim ProcessNameLUT(0)
        End If

        'load SizeClassLUTFN
        If System.IO.File.Exists(SizeClassLUTFN) = True Then
            MaxIndex = 0

            NSizeClassLUT = 0
            ReDim SizeClassNameLUT(MaxIndex) ', SizeClassSpecName(NSpecSizeClass) ', SizeClassSpecNameLUT(MaxIndex)
            fnum = FreeFile()
            FileOpen(fnum, SizeClassLUTFN, OpenMode.Input)
            dummy = LineInput(fnum)
            Do Until EOF(fnum)
                dummy = LineInput(fnum)
                If dummy.Length > 0 Then
                    arr = Split(dummy, ",")
                    MaxIndex = MaxIndex + 1
                    ReDim Preserve SizeClassNameLUT(MaxIndex) ', SizeClassSpecNameLUT(MaxIndex)
                    SizeClassNameLUT(MaxIndex) = CType(arr(1), String)
                End If
            Loop
            NSizeClassLUT = MaxIndex
            FileClose(fnum)
        Else
            fnum = FreeFile()
            FileOpen(fnum, SizeClassLUTFN, OpenMode.Output)
            PrintLine(fnum, "ID,SIZECLASS")
            FileClose(fnum)
            ReDim SizeClassNameLUT(0)
        End If

        'LOAD DENSITY LUT
        If System.IO.File.Exists(DensityLUTFN) = True Then
            MaxIndex = 0
            ReDim DensityNameLUT(MaxIndex)
            fnum = FreeFile()
            FileOpen(fnum, DensityLUTFN, OpenMode.Input)
            dummy = LineInput(fnum)
            Do Until EOF(fnum)
                dummy = LineInput(fnum)
                If dummy.Length > 0 Then
                    arr = Split(dummy, ",")
                    MaxIndex = MaxIndex + 1
                    ReDim Preserve DensityNameLUT(MaxIndex)
                    DensityNameLUT(MaxIndex) = CType(arr(1), String)
                End If
            Loop
            NDensityLUT = MaxIndex
            FileClose(fnum)
        Else
            fnum = FreeFile()
            FileOpen(fnum, DensityLUTFN, OpenMode.Output)
            PrintLine(fnum, "Index,DENSITY")
            FileClose(fnum)
            ReDim DensityNameLUT(0)
        End If

        'LOAD HABITAT GROUP LUT
        If System.IO.File.Exists(HabGroupLUTFN) = True Then
            MaxIndex = 0
            ReDim HabGroupNameLUT(MaxIndex)
            fnum = FreeFile()
            FileOpen(fnum, HabGroupLUTFN, OpenMode.Input)
            dummy = LineInput(fnum)
            Do Until EOF(fnum)
                dummy = LineInput(fnum)
                If dummy.Length > 0 Then
                    arr = Split(dummy, ",")
                    MaxIndex = MaxIndex + 1
                    ReDim Preserve HabGroupNameLUT(MaxIndex)
                    HabGroupNameLUT(MaxIndex) = CType(arr(1), String)
                End If

            Loop
            NHabGroupLUT = MaxIndex
            FileClose(fnum)
        Else
            fnum = FreeFile()
            FileOpen(fnum, HabGroupLUTFN, OpenMode.Output)
            PrintLine(fnum, "Index,HabGrp")
            FileClose(fnum)
            ReDim HabGroupNameLUT(0)
        End If

        'LOAD SPECIES LUT
        If System.IO.File.Exists(SpeciesLUTFN) = True Then
            MaxIndex = 0
            ReDim SpeciesNameLUT(MaxIndex)
            fnum = FreeFile()
            FileOpen(fnum, SpeciesLUTFN, OpenMode.Input)
            dummy = LineInput(fnum)
            Do Until EOF(fnum)
                dummy = LineInput(fnum)
                If dummy.Length > 0 Then
                    arr = Split(dummy, ",")
                    MaxIndex = MaxIndex + 1
                    ReDim Preserve SpeciesNameLUT(MaxIndex)
                    SpeciesNameLUT(MaxIndex) = CType(Trim(arr(1)), String)
                End If
            Loop
            NSpeciesLUT = MaxIndex
            FileClose(fnum)
        Else
            fnum = FreeFile()
            FileOpen(fnum, SpeciesLUTFN, OpenMode.Output)
            PrintLine(fnum, "Index,SPECIES")
            FileClose(fnum)
            ReDim SpeciesNameLUT(0)
        End If

        'LOAD FIRE PROCESS LUT
        If System.IO.File.Exists(FireProcessLUTFN) = True Then
            MaxIndex = 0
            ReDim FireProcessNameLUT(MaxIndex)
            fnum = FreeFile()
            FileOpen(fnum, FireProcessLUTFN, OpenMode.Input)
            dummy = LineInput(fnum)
            Do Until EOF(fnum)
                dummy = LineInput(fnum)
                If dummy.Length > 0 Then
                    arr = Split(dummy, ",")
                    MaxIndex = MaxIndex + 1
                    ReDim Preserve FireProcessNameLUT(MaxIndex)
                    FireProcessNameLUT(MaxIndex) = CType(arr(1), String)
                End If
            Loop
            NFireProcessLUT = MaxIndex
            FileClose(fnum)
        Else
            fnum = FreeFile()
            FileOpen(fnum, FireProcessLUTFN, OpenMode.Output)
            PrintLine(fnum, "ID,PROCESS")
            FileClose(fnum)
            ReDim FireProcessNameLUT(0)
        End If

        'LOAD OWNERSHIP LUT FILE
        If System.IO.File.Exists(OwnershipLUTFN) = True Then
            MaxIndex = 0
            ReDim OwnershipName(MaxIndex)
            fnum = FreeFile()
            FileOpen(fnum, OwnershipLUTFN, OpenMode.Input)
            dummy = LineInput(fnum)
            Do Until EOF(fnum)
                dummy = LineInput(fnum)
                If dummy.Length > 0 Then
                    arr = Split(dummy, ",")
                    MaxIndex = MaxIndex + 1
                    ReDim Preserve OwnershipName(MaxIndex)
                    OwnershipName(MaxIndex) = CType(arr(1), String)
                End If
            Loop
            NOwnerships = MaxIndex
            FileClose(fnum)
        Else
            fnum = FreeFile()
            FileOpen(fnum, OwnershipLUTFN, OpenMode.Output)
            PrintLine(fnum, "ID,OWNERSHIP")
            FileClose(fnum)
            ReDim OwnershipName(0)
        End If

        'LOAD SPECIAL AREA LUT FILE
        If System.IO.File.Exists(SpecialAreaLUTFN) = True Then
            MaxIndex = 0
            ReDim SpecialAreaName(MaxIndex)
            fnum = FreeFile()
            FileOpen(fnum, SpecialAreaLUTFN, OpenMode.Input)
            dummy = LineInput(fnum)
            Do Until EOF(fnum)
                dummy = LineInput(fnum)
                If dummy.Length > 0 Then
                    arr = Split(dummy, ",")
                    MaxIndex = MaxIndex + 1
                    ReDim Preserve SpecialAreaName(MaxIndex)
                    SpecialAreaName(MaxIndex) = CType(Trim(arr(1)), String)
                End If
            Loop
            NSpecialAreas = MaxIndex
            FileClose(fnum)
        Else
            fnum = FreeFile()
            FileOpen(fnum, SpecialAreaLUTFN, OpenMode.Output)
            PrintLine(fnum, "INDEX,SPECIAL-AREA-NAME")
            FileClose(fnum)
            ReDim SpecialAreaName(0)
        End If


    End Sub
    Private Sub AddLine(ByVal filename As String, ByVal line As String)
        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, filename, OpenMode.Append)
        PrintLine(fnum, line)
        FileClose(fnum)


    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        MsgBox("Scrolls through the WildlifeDiscreteDefinitions file, a SIMPPLLE output folder, and/or an .attributesall file to collect unique codes used by SOAP. Creates a 'CODESADDED' file that lists codes that were found that weren't already in the LUT files in the ModelDefs folder.")

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        MsgBox("Looks through the Processes file in the ModelDefs Directory to allow you to define which ones should be considered in ContinuousDefinitions 'time since disturbance' query")
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim folderdialog2 As New FolderBrowserDialog
        Dim fnum As Integer
        Dim dummy As String
        Dim atts() As String

        If foldername = "" Then
            With folderdialog2
                .SelectedPath = foldername
                .Description = "Folder that contains query definition files"
                '.Title = "Select a folder that has the "
                '.Filter = "Comma-delimited files(.txt) |*.txt"
                If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
            End With

            foldername = folderdialog2.SelectedPath
            folderdialog2 = Nothing
        End If

        'have to populate ItemsName and count NumItems
        NumItems = 0
        ReDim ItemName(NumItems)
        fnum = FreeFile()
        FileOpen(fnum, foldername & "\PROCESS_LUT.txt", OpenMode.Input)
        dummy = LineInput(fnum) 'header line
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length = 0 Then Exit Do
            atts = dummy.Split(",")
            NumItems = NumItems + 1
            ReDim Preserve ItemName(NumItems)
            ItemName(NumItems) = Trim(atts(1))
        Loop
        FileClose(fnum)

        Dim SelectorFrm As New Selector
        SelectorText = "Choose Processes to use in Constraints"
        SelectorFrm.ShowDialog()
        SelectorFrm = Nothing

        If FrmCancel = False Then
            fnum = FreeFile()
            FileOpen(fnum, foldername & "\FIRE_PROCESS_LUT.txt", OpenMode.Output)
            PrintLine(fnum, "ID,PROCESS")

            For j As Integer = 1 To ItemsToRead.Length - 1
                PrintLine(fnum, j & "," & ItemsToRead(j))
            Next
        End If

        FileClose(fnum)
        MsgBox("Fire file written")

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim folderdialog2 As New FolderBrowserDialog
        Dim fnum As Integer
        Dim tind As Integer
        Dim j As Integer
        Dim dummy As String
        Dim atts() As String
        Dim arr() As String


        If foldername = "" Then
            With folderdialog2
                .SelectedPath = foldername
                .Description = "Folder that contains query definition files"
                '.Title = "Select a folder that has the "
                '.Filter = "Comma-delimited files(.txt) |*.txt"
                If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
            End With

            foldername = folderdialog2.SelectedPath
            folderdialog2 = Nothing
        End If

        'LOAD OWNERSHIP LUT FILE
        If System.IO.File.Exists(foldername & "\OWNERSHIP_LUT.txt") = True Then
            NOwnerships = 0
            ReDim OwnershipName(NOwnerships)
            fnum = FreeFile()
            FileOpen(fnum, foldername & "\OWNERSHIP_LUT.txt", OpenMode.Input)
            dummy = LineInput(fnum)
            Do Until EOF(fnum)
                dummy = LineInput(fnum)
                If dummy.Length > 0 Then
                    NOwnerships = NOwnerships + 1
                    arr = Split(dummy, ",")
                    'If arr(0) > MaxIndex Then
                    '    MaxIndex = arr(0)
                    ReDim Preserve OwnershipName(NOwnerships)
                    'End If
                    OwnershipName(NOwnerships) = CType(arr(1), String)
                End If
            Loop
            'NOwnerships = MaxIndex
            FileClose(fnum)
        Else
            MsgBox("Please create the OWNERSHIP_LUT.txt file in this folder: " & foldername)
            Exit Sub
        End If

        'LOAD SPECIAL AREA LUT FILE
        If System.IO.File.Exists(foldername & "\SPECIAL_AREA_LUT.txt") = True Then
            NSpecialAreas = 0
            ReDim SpecialAreaName(NSpecialAreas)
            fnum = FreeFile()
            FileOpen(fnum, foldername & "\SPECIAL_AREA_LUT.txt", OpenMode.Input)
            dummy = LineInput(fnum)
            Do Until EOF(fnum)
                dummy = LineInput(fnum)
                If dummy.Length > 0 Then
                    arr = Split(dummy, ",")
                    'If Trim(arr(0)) > MaxIndex Then
                    '    MaxIndex = Trim(arr(0))
                    NSpecialAreas = NSpecialAreas + 1
                    ReDim Preserve SpecialAreaName(NSpecialAreas)
                    'End If
                    SpecialAreaName(NSpecialAreas) = CType(Trim(arr(1)), String)
                End If
            Loop
            'NSpecialAreas = MaxIndex
            FileClose(fnum)
        Else
            MsgBox("Please create the SPECIAL_AREA_LUT.txt file in this folder: " & foldername)
            Exit Sub
        End If

        NGroups = 0
        ReDim GroupName(NGroups), GroupInd(NGroups)
        NQueryDefs = 0
        ReDim QueryDefGroup(NQueryDefs), QueryDefOwn(NQueryDefs), QueryDefSA(NQueryDefs)
        'have to populate ItemsName and count NumItems
        If System.IO.File.Exists(foldername & "\SUIT_CODES_TO_USE_LUT.txt") Then
            fnum = FreeFile()
            FileOpen(fnum, foldername & "\SUIT_CODES_TO_USE_LUT.txt", OpenMode.Input)
            dummy = LineInput(fnum) 'header line
            Do Until EOF(fnum)
                dummy = Trim(LineInput(fnum))
                If dummy.Length = 0 Then Exit Do
                atts = dummy.Split(",")
                NQueryDefs = NQueryDefs + 1
                ReDim Preserve QueryDefGroup(NQueryDefs), QueryDefOwn(NQueryDefs), QueryDefSA(NQueryDefs)
                tind = GetStrIndex(GroupName, Trim(atts(2)))
                If tind < 0 Then
                    NGroups = NGroups + 1
                    ReDim Preserve GroupName(NGroups), GroupInd(NGroups)
                    tind = NGroups
                    GroupName(NGroups) = Trim(atts(2))
                    GroupInd(NGroups) = Trim(atts(1))
                End If
                j = GetStrIndex(OwnershipName, Trim(atts(3)))
                If Trim(atts(3)) = "" Then j = 0 'blank code means use them all
                If j < 0 Then
                    MsgBox("Ownership Code " & Trim(atts(3)) & " is not defined in OWNERSHIP_LUT.txt. Either add it to OWNERSHIP_LUT.txt or redefine the query definition in SUIT_CODES_TO_USE.txt")
                    FileClose(fnum)
                    Exit Sub
                End If
                j = GetStrIndex(SpecialAreaName, Trim(atts(4)))
                If Trim(atts(4)) = "" Then j = 0 'blank code means use them all
                If j < 0 Then
                    MsgBox("Special Area Code " & Trim(atts(4)) & " is not defined in SPECIAL_AREA_LUT.txt. Either add it to SPECIAL_AREA_LUT.txt or redefine the query definition in SUIT_CODES_TO_USE.txt")
                    FileClose(fnum)
                    Exit Sub
                End If
                QueryDefGroup(NQueryDefs) = tind
                QueryDefOwn(NQueryDefs) = Trim(atts(3))
                If QueryDefOwn(NQueryDefs) = "" Then QueryDefOwn(NQueryDefs) = allflag
                QueryDefSA(NQueryDefs) = Trim(atts(4))
                If QueryDefSA(NQueryDefs) = "" Then QueryDefSA(NQueryDefs) = allflag
            Loop
            FileClose(fnum)
        Else
            fnum = FreeFile()
            FileOpen(fnum, foldername & "\SUIT_CODES_TO_USE_LUT.txt", OpenMode.Output)
            PrintLine(fnum, "Index,GroupInd,GroupName,OwnName,SpecialAreaName")
            FileClose(fnum)
        End If

        'now populate the combo boxes
        clbGroupNames.Items.Clear()
        cbbOwnership.Items.Clear()
        cbbSpecialArea.Items.Clear()

        clbGroupNames.Items.Add("Create New Group", True)
        For jgroup As Integer = 1 To NGroups
            clbGroupNames.Items.Add(GroupName(jgroup), False)
        Next
        cbbOwnership.Items.Add(allflag)
        cbbOwnership.Text = cbbOwnership.Items.Item(0)
        For jown As Integer = 1 To NOwnerships
            cbbOwnership.Items.Add(OwnershipName(jown))
        Next
        cbbSpecialArea.Items.Add(allflag)
        cbbSpecialArea.Text = cbbSpecialArea.Items.Item(0)
        For jsa As Integer = 1 To NSpecialAreas
            cbbSpecialArea.Items.Add(SpecialAreaName(jsa))
        Next

        WriteRTBQueryDefs()

    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Dim o As Object
        Dim str As String
        Dim j As Integer
        Dim bnewgroup As Boolean
        Dim kgroup As Integer
        Dim grpname As String
        'add the combination to the group list
        If Trim(tbGroupName.Text).Length = 0 Then
            MsgBox("Group name cannot be blank")
            Exit Sub
        End If
        'check to make sure it's a unique name if it's been identified as a new name
        bnewgroup = False
        For Each o In clbGroupNames.CheckedItems
            str = Trim(tbGroupName.Text)
            j = GetStrIndex(GroupName, str)
            If o.ToString = "Create New Group" Then
                bnewgroup = True
                If j >= 0 Then
                    MsgBox("Group name " & str & " has already been used to define a group. Use a different name.")
                    Exit Sub
                End If
                'kgroup set below
            Else
                grpname = o.ToString
                kgroup = GetStrIndex(GroupName, str)
            End If
        Next

        'if it has made it this far
        If bnewgroup = True Then
            NGroups = NGroups + 1
            kgroup = NGroups
            ReDim Preserve GroupName(NGroups)
            GroupName(NGroups) = Trim(tbGroupName.Text)
            clbGroupNames.Items.Add(GroupName(NGroups))
        Else
            'check to make sure this definition hasn't been used before
            For jqd As Integer = 1 To NQueryDefs
                If QueryDefGroup(jqd) = kgroup And QueryDefOwn(jqd) = Trim(cbbOwnership.Text) And QueryDefSA(jqd) = Trim(cbbSpecialArea.Text) Then
                    MsgBox("This query definition is a duplicate of one that has already been used. Choose a different group or a different Ownership/Special Area combination")
                    Exit Sub
                End If
            Next
        End If

        NQueryDefs = NQueryDefs + 1
        ReDim Preserve QueryDefGroup(NQueryDefs), QueryDefOwn(NQueryDefs), QueryDefSA(NQueryDefs)
        QueryDefGroup(NQueryDefs) = kgroup
        QueryDefOwn(NQueryDefs) = Trim(cbbOwnership.Text)
        If QueryDefOwn(NQueryDefs) = "" Then QueryDefOwn(NQueryDefs) = allflag
        QueryDefSA(NQueryDefs) = Trim(cbbSpecialArea.Text)
        If QueryDefSA(NQueryDefs) = "" Then QueryDefSA(NQueryDefs) = allflag

        WriteRTBQueryDefs()
        'pbCaution.Visible = True
        btnWriteQueryGroupFile.BackColor = Color.Red

    End Sub

    Private Sub clbGroupNames_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles clbGroupNames.ItemCheck
        If e.NewValue = CheckState.Checked Then

            For i As Integer = 0 To Me.clbGroupNames.Items.Count - 1 Step 1
                If i <> e.Index Then
                    Me.clbGroupNames.SetItemChecked(i, False)
                Else
                    tbGroupName.Text = clbGroupNames.Items.Item(i)
                End If
            Next i
        End If


    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnWriteQueryGroupFile.Click
        Dim fnum As Integer
        Dim counter As Integer = 0
        Dim pOwn As String
        Dim pSA As String

        fnum = FreeFile()
        FileOpen(fnum, foldername & "\SUIT_CODES_TO_USE_LUT.txt", OpenMode.Output)
        PrintLine(fnum, "Index,GroupInd,GroupName,OwnName,SpecialAreaName")
        For jgroup As Integer = 1 To NGroups
            For jqd As Integer = 1 To NQueryDefs
                If QueryDefGroup(jqd) = jgroup Then
                    counter = counter + 1
                    pOwn = QueryDefOwn(jqd)
                    If QueryDefOwn(jqd) = allflag Then pOwn = ""
                    pSA = QueryDefSA(jqd)
                    If pSA = allflag Then pSA = ""
                    PrintLine(fnum, counter & "," & jgroup & "," & GroupName(jgroup) & "," & pOwn & "," & pSA)
                End If
            Next
        Next jgroup

        FileClose(fnum)

        WriteRTBQueryDefs()
        'pbCaution.Visible = False
        btnWriteQueryGroupFile.BackColor = Color.Lime
        MsgBox("file written")
    End Sub

    Private Sub WriteRTBQueryDefs()
        rtbQueryDefs.SelectAll()
        rtbQueryDefs.Clear()

        Dim grpname As String
        Dim own As String

        Dim counter As Integer = 0
        Dim str As String

        rtbQueryDefs.Text = "Index GroupInd GroupName       OwnName         Special Area Name" & Chr(13)

        For jgroup As Integer = 1 To NGroups
            For jqd As Integer = 1 To NQueryDefs
                If QueryDefGroup(jqd) = jgroup Then
                    counter = counter + 1
                    grpname = GroupName(jgroup)
                    own = QueryDefOwn(jqd)

                    If grpname.Length > 15 Then grpname = grpname.Substring(0, 15)
                    If own.Length > 15 Then own = own.Substring(0, 15)

                    str = counter.ToString.PadRight(6) & jgroup.ToString.PadRight(9) & grpname.PadRight(16) & own.PadRight(16) & QueryDefSA(jqd)
                    rtbQueryDefs.Text = rtbQueryDefs.Text & str & Chr(13)
                End If
            Next
        Next jgroup
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Dim delline As Integer = CType(tbDeleteLine.Text, Integer)
        Dim counter As Integer = 0

        For jgroup As Integer = 1 To NGroups
            For jqd As Integer = 1 To NQueryDefs
                If QueryDefGroup(jqd) = jgroup Then
                    counter = counter + 1
                    If counter = delline Then
                        QueryDefGroup(jqd) = -1
                        QueryDefOwn(jqd) = ""
                        QueryDefSA(jqd) = ""

                    End If
                End If
            Next
        Next jgroup
        WriteRTBQueryDefs()
        pbCaution.Visible = True
        btnWriteQueryGroupFile.BackColor = Color.OrangeRed
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        Dim AttsAllFile As String
        Dim NewSpecialAreaFile As String

        Dim OpenFileDialog1 As New OpenFileDialog
        'scroll through each set identified to select the .attributesall and .spatialrelate files for each

        With OpenFileDialog1
            .Title = "Select the .attributesall file to update Special Area Field"
            .Filter = "Attributesall files (.attributesall) |*.attributesall"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        AttsAllFile = OpenFileDialog1.FileName

        With OpenFileDialog1
            .Title = "Select the .csv file with new special area field: SLINK,SpecialArea"
            .Filter = "text files (.csv) |*.csv"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        NewSpecialAreaFile = OpenFileDialog1.FileName
        OpenFileDialog1 = Nothing

        'now run the stuff
        CountSLINKS(AttsAllFile)

        WriteNewAttributesall(AttsAllFile)

        MsgBox("New .attributesall file written")
    End Sub
    Private Sub CountSLINKS(ByVal infile As String)
        'opens the original .attributesall file to count how many SLINKS there are in total
        Dim fnum As Integer = FreeFile()
        Dim dummy As String
        Dim kslink As Integer
        Dim arr() As String

        FileOpen(fnum, infile, OpenMode.Input)
        dummy = LineInput(fnum)
        arr = Split(dummy, ",")
        NumItems = arr.Length
        ReDim ItemName(NumItems)
        For j As Integer = 1 To NumItems
            ItemName(j) = arr(j - 1)
        Next

        Dim SelectorFrm As New Selector
        SelectorText = "Choose Field to replace Special Area in the .attributesall file"
        SelectorFrm.ShowDialog()
        SelectorFrm = Nothing

        If FrmCancel = False Then
            'find which field was chosen
            For j As Integer = 1 To NumItems
                If ItemName(j) = ItemsToRead(1) Then
                    UpdateInd = j - 1
                    Exit For
                End If
            Next
        Else
            Exit Sub
        End If

        NSLINK = 0
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Or dummy = "END" Then Exit Do
            NSLINK = NSLINK + 1
        Loop
        FileClose(fnum)

        ReDim NewSA(NSLINK)

        FileOpen(fnum, infile, OpenMode.Input)
        LineInput(fnum)
        NSLINK = 0
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Or dummy = "END" Then Exit Do
            arr = Split(dummy, ",")
            kslink = arr(0)
            NewSA(kslink) = arr(UpdateInd)
        Loop
        FileClose(fnum)

    End Sub




    Private Sub WriteNewAttributesall(ByVal InFile As String)
        'open the old; read line-by-line and write updated one at the same time.
        Dim atts() As String
        Dim writedum As String
        Dim indum As String
        Dim kslink As Integer

        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, InFile, OpenMode.Input)
        Dim fnum2 As Integer = FreeFile()
        FileOpen(fnum2, InFile & ".tmp", OpenMode.Output)

        'header line
        writedum = LineInput(fnum)
        PrintLine(fnum2, writedum)

        For jslink As Integer = 1 To NSLINK
            indum = LineInput(fnum)
            atts = Split(indum, ",")
            kslink = Trim(atts(0))

            writedum = ""
            For jatt As Integer = 0 To 19
                writedum = writedum & atts(jatt) & ","
            Next
            writedum = writedum & NewSA(kslink)
            For jatt As Integer = 21 To atts.Length - 1
                writedum = writedum & "," & atts(jatt)
            Next
            PrintLine(fnum2, writedum)

        Next

        'last line
        writedum = LineInput(fnum)
        PrintLine(fnum2, writedum)

        System.IO.File.Delete(InFile)
        System.IO.File.Copy(InFile & ".tmp", InFile)

        FileClose(fnum)
        FileClose(fnum2)
    End Sub



    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        MsgBox("Looks through a folder with SIMPPLLE pathway files (text) to identify all POSSIBLE combinations of habitat group, species, size, density and writes these to a WLDiscreteDefinition file. If checked, this is the only process that will be run." & Chr(13) & Chr(13) _
        & "As of 5/2016 this is the only routine of this Query Set Up screen that requires completing - the other steps (2-4) will fill in default values if you don't run them now.")

    End Sub
    Private Function GetSizeInd(ByVal CompareArr() As String, ByVal strval As String) As Integer
        Dim secondcompareval As String
        GetSizeInd = -1
        'If strval = "SS" Then Stop
        For j As Integer = 0 To CompareArr.Length - 1
            If CompareArr(j).Length >= strval.Length - 2 And strval.Length >= CompareArr(j).Length And CompareArr(j).Length > 0 Then 'gets you through 99 size classes
                secondcompareval = strval.Substring(0, CompareArr(j).Length)
                If CompareArr(j) = secondcompareval Then
                    GetSizeInd = j
                    Exit Function
                End If
            End If
        Next
    End Function


    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        Dim folderdialog2 As New FolderBrowserDialog
        foldername = ""
        NumItems = 0
        Dim i As Integer = 0

        With folderdialog2
            .SelectedPath = foldername
            .Description = "Folder that contains query definition files"
            '.Title = "Select a folder that has the "
            '.Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        foldername = folderdialog2.SelectedPath
        folderdialog2 = Nothing
        QuerySIMPPLLEoutput.tbModelDefsDir.Text = foldername 'automatically populate this on the main form

        'track newly added codes
        CodesAddedFN = foldername & "\" & "CODESADDED.txt"

        FillCombosUsedRunOutput(foldername & "\WildlifeDiscreteDefinitions.csv")
        MsgBox("Done. File written to: " & foldername & "\WildlifeDiscreteDefinitions.csv")

    End Sub

    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        Me.Close()
    End Sub

    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click
        Dim WLDFile As String
        Dim OpenFileDialog1 As New OpenFileDialog
        Dim NHTG As Integer
        Dim HTGName() As String
        Dim NSpecies As Integer
        Dim SpeciesName() As String
        Dim fnum As Integer
        Dim DUMMY As String
        Dim arr() As String
        Dim arr2() As String
        Dim NLines As Integer
        Dim kline As Integer
        Dim LineInfo(,) As String 'by line, by column
        Dim HTGSpeciesCombo(,) As Byte 'flag whether the htg species combination exists
        Dim strb As StringBuilder
        Dim khtg As Integer
        Dim kspecies As Integer

        If cbSpecies.Checked = False And cbSpeciesHTG.Checked = False Then
            MsgBox("Check either Species or Species By HTG")
            Exit Sub
        End If
        With OpenFileDialog1
            .Title = "Select the WildlifeDiscreteDefinitions File to populate"
            .Filter = "CSV files (.csv) |WildlifeDiscreteDefinitions.csv"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        WLDFile = OpenFileDialog1.FileName
        OpenFileDialog1 = Nothing

        NHTG = 0
        NSpecies = 0
        ReDim HTGName(NHTG)
        ReDim SpeciesName(NSpecies)
        fnum = FreeFile()
        FileOpen(fnum, WLDFile, OpenMode.Input)
        DUMMY = Trim(LineInput(fnum)) 'header
        NLines = 0
        Do Until EOF(fnum)
            DUMMY = Trim(LineInput(fnum))
            If DUMMY.Length = 0 Then Exit Do
            NLines += 1
        Loop
        FileClose(fnum)

        ReDim HTGName(NLines)
        ReDim SpeciesName(NLines)
        ReDim LineInfo(NLines, 4) 'HTG,SPECIES,SIZE,DENSITY
        fnum = FreeFile()
        FileOpen(fnum, WLDFile, OpenMode.Input)
        DUMMY = Trim(LineInput(fnum)) 'header
        kline = 0
        Do Until EOF(fnum)
            DUMMY = Trim(LineInput(fnum))
            If DUMMY.Length = 0 Then Exit Do
            kline += 1
            arr = Split(DUMMY, ",")
            If Array.IndexOf(HTGName, arr(0)) < 0 Then
                NHTG += 1
                HTGName(NHTG) = arr(0)
            End If
            arr2 = Split(arr(1), "-")
            For j As Integer = 0 To arr2.Length - 1
                If Array.IndexOf(SpeciesName, arr2(j)) < 0 Then
                    NSpecies += 1
                    SpeciesName(NSpecies) = arr2(j)
                End If
            Next
            For j As Integer = 1 To 4
                LineInfo(kline, j) = arr(j - 1)
            Next
        Loop
        FileClose(fnum)


        'see if the combination exists
        ReDim HTGSpeciesCombo(NHTG, NSpecies)
        For jline As Integer = 1 To NLines
            khtg = Array.IndexOf(HTGName, LineInfo(jline, 1))
            arr2 = Split(LineInfo(jline, 2), "-")
            For js As Integer = 0 To arr2.Length - 1
                kspecies = Array.IndexOf(SpeciesName, arr2(js))
                HTGSpeciesCombo(khtg, kspecies) = 1
            Next
        Next



        'one more time to write it all out
        FileOpen(fnum, WLDFile, OpenMode.Output)
        DUMMY = "HTG,SPECIES,SIZE,DENSITY"
        If cbSpecies.Checked = True Then
            For jspecies As Integer = 1 To NSpecies
                DUMMY = DUMMY & "," & SpeciesName(jspecies)
            Next
        End If
        If cbSpeciesHTG.Checked = True Then
            'For jhtg As Integer = 1 To NHTG
            For jspecies As Integer = 1 To NSpecies
                DUMMY = DUMMY & ",HTG_" & SpeciesName(jspecies)
            Next
            'Next
        End If
        PrintLine(fnum, DUMMY)
        For jline As Integer = 1 To NLines
            strb = New StringBuilder(LineInfo(jline, 1), 600)
            strb.Append("," & LineInfo(jline, 2) & "," & LineInfo(jline, 3) & "," & LineInfo(jline, 4))
            If cbSpecies.Checked = True Then

                For jspecies As Integer = 1 To NSpecies
                    strb.Append(",")
                    'is the species in Lineinfo(2)?
                    arr2 = Split(LineInfo(jline, 2), "-")
                    For js As Integer = 0 To arr2.Length - 1
                        If arr2(js) = SpeciesName(jspecies) Then
                            strb.Append(SpeciesName(jspecies))
                            Exit For
                        End If
                    Next
                Next
            End If
            If cbSpeciesHTG.Checked = True Then

                'For jhtg As Integer = 1 To NHTG
                For jspecies As Integer = 1 To NSpecies
                    'If HTGSpeciesCombo(jhtg, jspecies) = 1 And LineInfo(jline, 1) = HTGName(jhtg) Then
                    strb.Append(",")
                    'is the species in Lineinfo(2)?
                    arr2 = Split(LineInfo(jline, 2), "-")
                    For js As Integer = 0 To arr2.Length - 1
                        If arr2(js) = SpeciesName(jspecies) Then
                            strb.Append(LineInfo(jline, 1) & "_" & SpeciesName(jspecies))
                            Exit For
                        End If
                    Next
                    ' End If
                Next
                'Next
            End If
            PrintLine(fnum, strb.ToString)
        Next
        FileClose(fnum)

        MsgBox("Done")









    End Sub

    Private Sub Button15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button15.Click
        MsgBox("This function creates queries to list each species separately, or species by Habitat Type Group")
    End Sub
End Class