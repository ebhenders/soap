Imports System.Text
Public Class clProcessSimpplle


    'Dim BaseFolder As String

    Dim NSeason As Integer

    Dim NLifeForm As Integer
    Dim NSpecies As Integer
    Dim NSpectrumSpecies As Integer
    Dim NSizeClassXW As Integer 'specific to the run
    Dim NSizeClassLUT As Integer 'overall number of potential SIMPPLLE size classes
    Dim NSpecSizeClass As Integer
    Dim NAgeClass As Integer
    Dim NDensity As Integer
    Dim NProcess As Integer
    Dim NAllProcess As Integer 'complete list; not just unique to this geo area
    Dim NOwnership As Integer
    Dim NSpecialArea As Integer
    Dim NHabGroup As Integer

    Dim NSpecLevel5 As Integer
    Dim NSpecDFC As Integer
    Dim NSuitClasses As Integer 'specific to the run


    'look-up table indices
    Dim LifeFormName() As String
    Dim SpeciesName() As String
    Dim SpectrumSpeciesLevel5LUT(,) As String
    Dim SpectrumSpeciesDFCLUT(,) As String
    Dim SpectrumSpeciesLevel5() As String
    Dim SpectrumSpeciesDFC() As String
    Dim SizeClassNameXW() As String 'name on the list of size classes specific to this run
    Dim SizeClassNameLUT() As String 'name on the list of overall potential size classes
    Dim SizeClassSpecNameLUT() As String 'spectrum name associated with SIMPPLLE size class index
    Dim SizeClassSpecName() As String 'unique names of spectrum size classes

    Dim SuitabilityLUT(,) As String 'by ownership, special area
    Dim SuitabilityName() As String 'by unique suitability classes

    Dim AgeClassName() As String
    Dim DensityName() As String
    Dim ProcessName() As String
    Dim AllProcessName() As String
    Dim OwnershipName() As String
    Dim SpecialAreaName() As String
    Dim HabGroupName() As String



    'forest-level results
    Dim SpectrumProcessMetrics(,,) As Double 'by species/size, process, timestep

    'FOR COMBINING OUTPUT FILES

    Dim FileNames() As String
    Dim FileIndex() As String
    Dim NFiles As Integer
    Dim NIndices As Integer 'for processing multiple runs

    Public Sub RunSummarizer(ByVal ICFileNames As String, ByVal OutputFileNames As String)

        Dim OutBase As String
        'Dim InBase As String

        'Dim outfilename As String
        Dim txtcsv As String
        Dim SpecLev5FN As String, SpecDFCFN As String, SpecSizeClassFN As String, SpecSuitFN As String

        ReadICfileNames(ICFileNames)
        ReadOutputsToPocess(OutputFileNames)
        If NICFiles <> NOutFiles Then
            MsgBox("Check Definition Files - number of IC files does not match number of output files to process")
            Exit Sub
        End If
        Dim NRuns As Integer = NICFiles

        OutBase = DirName(OutputFileNames)
        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, OutBase & "SpecLevel5Files.txt", OpenMode.Output)
        Dim fnum1 As Integer = FreeFile()
        FileOpen(fnum1, OutBase & "SpecDFCFiles.txt", OpenMode.Output)
        Dim fnum2 As Integer = FreeFile()
        FileOpen(fnum2, OutBase & "SpecSizeClassFiles.txt", OpenMode.Output)
        Dim fnum3 As Integer = FreeFile()
        FileOpen(fnum3, OutBase & "SpecSuitFiles.txt", OpenMode.Output)
        txtcsv = "csv" 'default
        If System.IO.File.Exists(OutFilesToProcessDir(1) & "EVU_SIM_DATA" & RunToUse & ".txt") Then txtcsv = "txt"
        For jrun As Integer = 1 To NRuns
            'first, get basic LUT information
            RunToUse = OutFilesToProcessNumber(jrun)
            GeoAreaName = OutFilesToProcessName(jrun)
            DefineLUTFileNames(OutFilesToProcessDir(jrun))
            DefineFileNames(OutFilesToProcessDir(jrun), RunToUse, txtcsv)
            LoadFiles(ICFileName(jrun))
            'LoadSimCrosswalkFiles()
            'LoadSIMPPLLEOutput(ICFileName(jrun), SpatialRelFileName(jrun))
            'outfilename = DirName(HabOutputFN) & OutFilesToProcessName(jrun) & OutFilesToProcessNumber(jrun) & ".txt"
            'outfilename = DirName(HabOutputFN) & OutFilesToProcessName(jrun) & OutFilesToProcessNumber(jrun) & ".txt"
            SpecLev5FN = OutFilesToProcessDir(jrun) & "\" & GeoAreaName & RunToUse & "SpectrumLevel5Output.txt"
            SpecDFCFN = OutFilesToProcessDir(jrun) & "\" & GeoAreaName & RunToUse & "SpectrumDFCOutput.txt"
            SpecSizeClassFN = OutFilesToProcessDir(jrun) & "\" & GeoAreaName & RunToUse & "SpectrumSizeClassOutput.txt"
            SpecSuitFN = OutFilesToProcessDir(jrun) & "\" & GeoAreaName & RunToUse & "SpectrumSuitabilityOutput.txt"
            PrintLine(fnum, SpecLev5FN)
            PrintLine(fnum1, SpecDFCFN)
            PrintLine(fnum2, SpecSizeClassFN)
            PrintLine(fnum3, SpecSuitFN)
            'patchbasefn = DirName(HabOutputFN) & FName(HabOutputFN) & OutFilesToProcessName(jrun) & OutFilesToProcessNumber(jrun)
            'CodePolygonHabitats(outfilename, haboutfilename, patchbasefn)

            SummarizeResults(SpecLev5FN, SpecDFCFN, SpecSizeClassFN, SpecSuitFN)

        Next
        FileClose(fnum)
        FileClose(fnum1)
        FileClose(fnum2)
        FileClose(fnum3)

        MsgBox("Done")

    End Sub


    Private Sub SummarizeResults(ByVal SpecLev5FN As String, ByVal SpecDFCFN As String, ByVal SpecSizeClassFN As String, ByVal SpecSuitFN As String)
        Dim str As String = ""
        Dim fnum As Integer = FreeFile()
        'write out Size Class disturbance metrics as well as species disturbance metrics

        'Summarize by Spectrum Level 5
        ReDim SpectrumProcessMetrics(NSpecLevel5, NAllProcess, NTimestep)
        QuerySpec5Results()

        FileOpen(fnum, SpecLev5FN, OpenMode.Output)
        For jspec As Integer = 1 To NSpecLevel5
            For jprocess As Integer = 0 To NAllProcess
                str = ""
                str = SpectrumSpeciesLevel5(jspec) & "," & AllProcessName(jprocess)
                For jper As Integer = 1 To NTimestep
                    str = str & "," & SpectrumProcessMetrics(jspec, jprocess, jper)
                Next
                PrintLine(fnum, str)
            Next
        Next
        FileClose(fnum)

        'Summarize by Spectrum DFC
        ReDim SpectrumProcessMetrics(NSpecDFC, NAllProcess, NTimestep)
        QuerySpecDFCResults()

        FileOpen(fnum, SpecDFCFN, OpenMode.Output)
        For jspec As Integer = 1 To NSpecDFC
            For jprocess As Integer = 0 To NAllProcess
                str = ""
                str = SpectrumSpeciesDFC(jspec) & "," & AllProcessName(jprocess)
                For jper As Integer = 1 To NTimestep
                    str = str & "," & SpectrumProcessMetrics(jspec, jprocess, jper)
                Next
                PrintLine(fnum, str)
            Next
        Next
        FileClose(fnum)

        'Summarize by Size Class
        ReDim SpectrumProcessMetrics(NSpecSizeClass, NAllProcess, NTimestep)
        QuerySpecSizeResults()

        FileOpen(fnum, SpecSizeClassFN, OpenMode.Output)
        For jspec As Integer = 1 To NSpecSizeClass
            For jprocess As Integer = 0 To NAllProcess
                str = ""
                str = SizeClassSpecName(jspec) & "," & AllProcessName(jprocess)
                For jper As Integer = 1 To NTimestep
                    str = str & "," & SpectrumProcessMetrics(jspec, jprocess, jper)
                Next
                PrintLine(fnum, str)
            Next
        Next
        FileClose(fnum)

        'Summarize by Suitability
        ReDim SpectrumProcessMetrics(NSuitClasses, NAllProcess, NTimestep)
        QuerySuitabilityResults()

        FileOpen(fnum, SpecSuitFN, OpenMode.Output)
        For jspec As Integer = 1 To NSuitClasses
            For jprocess As Integer = 0 To NAllProcess
                str = ""
                str = SuitabilityName(jspec) & "," & AllProcessName(jprocess)
                For jper As Integer = 1 To NTimestep
                    str = str & "," & SpectrumProcessMetrics(jspec, jprocess, jper)
                Next
                PrintLine(fnum, str)
            Next
        Next
        FileClose(fnum)

    End Sub

    Private Sub QuerySpec5Results()
        Dim field1 As Integer
        Dim field2 As Integer 'process
        Dim khab As Integer
        Dim kspecies As Integer
        Dim kprocess As Integer
        Dim slev5 As String
        Dim tsuit As String

        ' Dim fnum2 As Integer = FreeFile()

        'FileOpen(fnum2, BaseFolder & GeoAreaName & RunToUse & "DebugQuery.txt", OpenMode.Output)

        'PrintLine(fnum2, "kaa, kspecies, khab, kprocess, field1, field2, AAOwnership, AASpecialArea, tsuit, Acres")

        For jaa As Integer = 1 To NSLINK
            For jtime As Integer = 1 To NTimestep
                'determine the Spectrum Level 5 label for this AA, timestep
                kspecies = AASpecies(jaa, jtime - 1) 'process occurred to species call in last timestep
                khab = AAHabGroup(jaa)
                If kspecies < 0 Or khab < 0 Then
                    Exit For
                End If

                slev5 = SpectrumSpeciesLevel5LUT(khab, kspecies)
                field1 = GetStrIndex(SpectrumSpeciesLevel5, slev5)
                'If field1 = 0 Then Stop

                'determine the Process label for this AA, timestep
                kprocess = AAProcess(jaa, jtime)
                field2 = GetStrIndex(AllProcessName, ProcessName(kprocess)) 'AAProcess(jaa, jtime)

                If AAOwnership(jaa) < 0 Or AASpecialArea(jaa) < 0 Then
                    Exit For
                End If

                tsuit = Trim(SuitabilityLUT(AAOwnership(jaa), AASpecialArea(jaa)))
                'filter to national forest ownership only
                If tsuit <> "NNF" Then SpectrumProcessMetrics(field1, field2, jtime) = SpectrumProcessMetrics(field1, field2, jtime) + SIMAAAcres
                'If jtime = 1 Then PrintLine(fnum2, jaa & "," & kspecies & "," & khab & "," & kprocess & "," & field1 & "," & field2 & "," & AAOwnership(jaa) & "," & AASpecialArea(jaa) & "," & tsuit & "," & AAAcres(jaa))
            Next
        Next jaa

        'FileClose(fnum2)

    End Sub

    Private Sub QuerySpecDFCResults()
        Dim field1 As Integer
        Dim field2 As Integer 'process
        Dim khab As Integer
        Dim kspecies As Integer
        Dim kprocess As Integer
        Dim sdfc As String
        Dim tsuit As String

        For jaa As Integer = 1 To NSLINK
            For jtime As Integer = 1 To NTimestep
                'determine the Spectrum Level 5 label for this AA, timestep
                kspecies = AASpecies(jaa, jtime - 1) 'process occurred to species call in last timestep
                khab = AAHabGroup(jaa)
                If kspecies < 0 Or khab < 0 Then Exit For
                sdfc = SpectrumSpeciesDFCLUT(khab, kspecies)
                field1 = GetStrIndex(SpectrumSpeciesDFC, sdfc)

                'determine the Process label for this AA, timestep
                kprocess = AAProcess(jaa, jtime)
                field2 = GetStrIndex(AllProcessName, ProcessName(kprocess))
                If AAOwnership(jaa) < 0 Or AASpecialArea(jaa) < 0 Then Exit For
                tsuit = Trim(SuitabilityLUT(AAOwnership(jaa), AASpecialArea(jaa)))
                'filter to national forest ownership only
                If tsuit <> "NNF" And tsuit <> "" Then SpectrumProcessMetrics(field1, field2, jtime) = SpectrumProcessMetrics(field1, field2, jtime) + SIMAAAcres
            Next
        Next jaa

    End Sub


    Private Sub QuerySpecSizeResults()
        Dim field1 As Integer
        Dim field2 As Integer 'process
        Dim tsize As String
        Dim tsuit As String
        Dim kprocess As Integer

        For jaa As Integer = 1 To NSLINK
            For jtime As Integer = 1 To NTimestep
                If AASizeClass(jaa, jtime - 1) < 0 Then Exit For
                tsize = SizeClassSpecNameLUT(GetStrIndex(SizeClassNameLUT, SizeClassNameXW(AASizeClass(jaa, jtime - 1))))
                field1 = GetStrIndex(SizeClassSpecName, tsize)
                'determine the Process label for this AA, timestep
                kprocess = AAProcess(jaa, jtime)
                field2 = GetStrIndex(AllProcessName, ProcessName(kprocess))
                If AAOwnership(jaa) < 0 Or AASpecialArea(jaa) < 0 Then Exit For
                tsuit = Trim(SuitabilityLUT(AAOwnership(jaa), AASpecialArea(jaa)))
                'filter to national forest ownership only
                If tsuit <> "NNF" And tsuit <> "" Then SpectrumProcessMetrics(field1, field2, jtime) = SpectrumProcessMetrics(field1, field2, jtime) + SIMAAAcres

            Next
        Next jaa

    End Sub

    Private Sub QuerySuitabilityResults()
        Dim field1 As Integer
        Dim field2 As Integer 'process
        Dim tsuit As String
        Dim kprocess As Integer

        For jaa As Integer = 1 To NSLINK
            For jtime As Integer = 1 To NTimestep
                If AAOwnership(jaa) < 0 Or AASpecialArea(jaa) < 0 Then Exit For
                tsuit = SuitabilityLUT(AAOwnership(jaa), AASpecialArea(jaa))
                field1 = GetStrIndex(SuitabilityName, tsuit)
                'determine the Process label for this AA, timestep
                kprocess = AAProcess(jaa, jtime)
                field2 = GetStrIndex(AllProcessName, ProcessName(kprocess))
                SpectrumProcessMetrics(field1, field2, jtime) = SpectrumProcessMetrics(field1, field2, jtime) + SIMAAAcres

            Next
        Next jaa
    End Sub

    Private Sub ReadICfileNames(ByVal filename As String)
        Dim fnum As Integer = FreeFile()
        Dim dummy As String
        FileOpen(fnum, filename, OpenMode.Input)
        dummy = Trim(LineInput(fnum))
        NICFiles = 0
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            NICFiles = NICFiles + 1
            ReDim Preserve ICFileName(NICFiles)
            ICFileName(NICFiles) = dummy
        Loop

        FileClose(fnum)

    End Sub

    Private Sub ReadOutputsToPocess(ByVal filename As String)
        Dim fnum As Integer = FreeFile()
        Dim dummy As String
        Dim arr() As String

        'Dim OutFilesToProcess() As String
        'Dim NOutFiles As Integer
        FileOpen(fnum, filename, OpenMode.Input)
        NOutFiles = 0
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            arr = Split(dummy, ",")
            NOutFiles = NOutFiles + 1
            ReDim Preserve OutFilesToProcessDir(NOutFiles), OutFilesToProcessName(NOutFiles), OutFilesToProcessNumber(NOutFiles)
            OutFilesToProcessDir(NOutFiles) = arr(0)
            OutFilesToProcessName(NOutFiles) = arr(1)
            OutFilesToProcessNumber(NOutFiles) = arr(2)
        Loop
        FileClose(fnum)
    End Sub

    Private Sub LoadFiles(ByVal InitCondFN As String)
        Dim MaxIndex As Integer = 0
        Dim fnum As Integer = FreeFile()
        Dim dummy As String
        Dim arr() As String
        Dim tind As Integer
        Dim slink As Integer
        Dim timestep As Integer

        'load PROCESS
        MaxIndex = 0
        ReDim ProcessName(MaxIndex)
        FileOpen(fnum, ProcessXWFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                If arr(0) > MaxIndex Then
                    MaxIndex = arr(0)
                    ReDim Preserve ProcessName(MaxIndex)
                End If
                ProcessName(arr(0)) = CType(Trim(arr(1)), String)
            End If
        Loop
        NProcess = MaxIndex
        FileClose(fnum)

        'load ALLPROCESSLUT
        MaxIndex = 0
        ReDim AllProcessName(MaxIndex)
        FileOpen(fnum, AllProcessLUTFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                If arr(0) > MaxIndex Then
                    MaxIndex = arr(0)
                    ReDim Preserve AllProcessName(MaxIndex)
                End If
                AllProcessName(arr(0)) = CType(Trim(arr(1)), String)
            End If
        Loop
        NAllProcess = MaxIndex
        FileClose(fnum)

        'load SizeClassXW
        MaxIndex = 0
        ReDim SizeClassNameXW(MaxIndex)
        FileOpen(fnum, SizeClassXWFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                If arr(0) > MaxIndex Then
                    MaxIndex = arr(0)
                    ReDim Preserve SizeClassNameXW(MaxIndex)
                End If
                SizeClassNameXW(arr(0)) = CType(arr(1), String)
            End If
        Loop
        NSizeClassXW = MaxIndex
        FileClose(fnum)

        'load SizeClassLUTFN
        MaxIndex = 0

        NSpecSizeClass = 0
        ReDim SizeClassNameLUT(MaxIndex), SizeClassSpecNameLUT(MaxIndex), SizeClassSpecName(NSpecSizeClass)
        FileOpen(fnum, SizeClassLUTFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                If arr(0) > MaxIndex Then
                    MaxIndex = arr(0)
                    ReDim Preserve SizeClassSpecNameLUT(MaxIndex), SizeClassNameLUT(MaxIndex)
                End If
                tind = GetStrIndex(SizeClassSpecName, arr(2))
                If tind < 0 Then
                    NSpecSizeClass = NSpecSizeClass + 1
                    ReDim Preserve SizeClassSpecName(NSpecSizeClass)
                    tind = NSpecSizeClass
                End If
                SizeClassNameLUT(arr(0)) = CType(arr(1), String)
                SizeClassSpecNameLUT(arr(0)) = CType(arr(2), String)
                SizeClassSpecName(tind) = CType(arr(2), String)
            End If
        Loop
        NSizeClassLUT = MaxIndex
        FileClose(fnum)

        'load SpeciesXWFN
        MaxIndex = 0
        ReDim SpeciesName(MaxIndex)
        FileOpen(fnum, SpeciesXWFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                If arr(0) > MaxIndex Then
                    MaxIndex = arr(0)
                    ReDim Preserve SpeciesName(MaxIndex)
                End If
                SpeciesName(arr(0)) = CType(arr(1), String)
            End If
        Loop
        NSpecies = MaxIndex
        FileClose(fnum)

        'load LifeFormXWFN
        MaxIndex = 0
        ReDim LifeFormName(MaxIndex)
        FileOpen(fnum, LifeFormXWFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                If arr(0) > MaxIndex Then
                    MaxIndex = arr(0)
                    ReDim Preserve LifeFormName(MaxIndex)
                End If
                LifeFormName(arr(0)) = CType(arr(1), String)
            End If
        Loop
        NLifeForm = MaxIndex
        FileClose(fnum)


        'load DensityXWFN
        MaxIndex = 0
        ReDim DensityName(MaxIndex)
        FileOpen(fnum, DensityXWFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                If arr(0) > MaxIndex Then
                    MaxIndex = arr(0)
                    ReDim Preserve DensityName(MaxIndex)
                End If
                DensityName(arr(0)) = CType(arr(1), String)
            End If
        Loop
        NDensity = MaxIndex
        FileClose(fnum)

        'load HabGroupXWFN
        MaxIndex = 0
        ReDim HabGroupName(MaxIndex)
        FileOpen(fnum, HabGroupXWFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                If arr(0) > MaxIndex Then
                    MaxIndex = arr(0)
                    ReDim Preserve HabGroupName(MaxIndex)
                End If
                HabGroupName(arr(0)) = CType(arr(1), String)
            End If
        Loop
        NHabGroup = MaxIndex
        FileClose(fnum)

        'load SpectrumSpeciesLUTFN

        NSpecLevel5 = 0
        NSpecDFC = 0
        ReDim SpectrumSpeciesLevel5(NSpecLevel5), SpectrumSpeciesDFC(NSpecDFC)
        ReDim SpectrumSpeciesLevel5LUT(NHabGroup, NSpecies), SpectrumSpeciesDFCLUT(NHabGroup, NSpecies)
        Dim grpind As Integer
        Dim speciesind As Integer
        Dim specind As Integer
        FileOpen(fnum, SpectrumSpeciesLUTFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                grpind = GetStrIndex(HabGroupName, arr(0))
                speciesind = GetStrIndex(SpeciesName, arr(1))
                If grpind >= 0 And speciesind >= 0 Then
                    SpectrumSpeciesLevel5LUT(grpind, speciesind) = Trim(arr(2))
                    SpectrumSpeciesDFCLUT(grpind, speciesind) = Trim(arr(3))
                End If
                'look up whether these are newly defined spectrum codes...
                specind = GetStrIndex(SpectrumSpeciesLevel5, Trim(arr(2)))
                If specind < 0 Then
                    NSpecLevel5 = NSpecLevel5 + 1
                    ReDim Preserve SpectrumSpeciesLevel5(NSpecLevel5)
                    SpectrumSpeciesLevel5(NSpecLevel5) = Trim(arr(2))
                End If
                specind = GetStrIndex(SpectrumSpeciesDFC, Trim(arr(3)))
                If specind < 0 Then
                    NSpecDFC = NSpecDFC + 1
                    ReDim Preserve SpectrumSpeciesDFC(NSpecDFC)
                    SpectrumSpeciesDFC(NSpecDFC) = Trim(arr(3))
                End If
            End If
        Loop
        'populate defaults...
        For jhab As Integer = 1 To NHabGroup '0 contains the defaults...
            For jspc As Integer = 0 To NSpecies
                If SpectrumSpeciesLevel5LUT(jhab, jspc) = "" Then
                    SpectrumSpeciesLevel5LUT(jhab, jspc) = SpectrumSpeciesLevel5LUT(0, jspc)
                End If
                If SpectrumSpeciesDFCLUT(jhab, jspc) = "" Then
                    SpectrumSpeciesDFCLUT(jhab, jspc) = SpectrumSpeciesDFCLUT(0, jspc)
                End If
            Next

        Next
        FileClose(fnum)

        'load OwnershipLUTFN
        MaxIndex = 0
        ReDim OwnershipName(MaxIndex)
        FileOpen(fnum, OwnershipXWFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                If arr(0) > MaxIndex Then
                    MaxIndex = arr(0)
                    ReDim Preserve OwnershipName(MaxIndex)
                End If
                OwnershipName(arr(0)) = CType(arr(1), String)
            End If
        Loop
        NOwnership = MaxIndex
        FileClose(fnum)

        'load SpecialAreaLUTFN
        MaxIndex = 0
        ReDim SpecialAreaName(MaxIndex)
        FileOpen(fnum, SpecialAreaXWFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                If arr(0) > MaxIndex Then
                    MaxIndex = arr(0)
                    ReDim Preserve SpecialAreaName(MaxIndex)
                End If
                SpecialAreaName(arr(0)) = CType(arr(1), String)
            End If
        Loop
        NSpecialArea = MaxIndex
        FileClose(fnum)

        'load up the Suitability LUT
        ReDim SuitabilityLUT(NOwnership, NSpecialArea)
        NSuitClasses = 0
        ReDim SuitabilityName(NSuitClasses)
        Dim f1ind As Integer
        Dim f2ind As Integer
        FileOpen(fnum, SuitabilityLUTFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                If arr(0) > MaxIndex Then MaxIndex = arr(0)
                tind = GetStrIndex(SuitabilityName, Trim(arr(3)))
                If tind < 0 Then
                    NSuitClasses = NSuitClasses + 1
                    ReDim Preserve SuitabilityName(NSuitClasses)
                    SuitabilityName(NSuitClasses) = Trim(arr(3))
                End If
                f1ind = GetStrIndex(OwnershipName, arr(1))
                f2ind = GetStrIndex(SpecialAreaName, arr(2))
                If f1ind >= 0 And f2ind >= 0 Then SuitabilityLUT(f1ind, f2ind) = Trim(arr(3))
            End If
        Loop
        'NSLINK = MaxIndex
        FileClose(fnum)


        'load TimeZero - first count the SLINKs
        MaxIndex = 0
        FileOpen(fnum, InitCondFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If Trim(dummy) = "END" Then Exit Do
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                If arr(0) > MaxIndex Then MaxIndex = arr(0)
            End If
        Loop
        NSLINK = MaxIndex
        FileClose(fnum)


        'static metrics - aa level
        ReDim AASLINK(NSLINK), AASTANDID(NSLINK), AAHabGroup(NSLINK), AAOwnership(NSLINK), AASpecialArea(NSLINK), AAHabGroup(NSLINK), PastProcessTimesteps(NSLINK)
        'by time step
        ReDim AALifeForm(NSLINK, NTimestep), AASpecies(NSLINK, NTimestep), AASizeClass(NSLINK, NTimestep)
        ReDim AAAgeClass(NSLINK, NTimestep), AADensity(NSLINK, NTimestep), AAProcess(NSLINK, NTimestep)

        'Then, get TimeZero metrics

        MaxIndex = 0
        FileOpen(fnum, InitCondFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "END" Or dummy.Length = 0 Then Exit Do
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                slink = arr(0)
                'If slink = 5240 Then Stop
                SIMAAAcres = arr(4) * WLHabAcreAdjust
                AASLINK(slink) = slink
                AASTANDID(slink) = arr(3)
                'AALifeForm(slink, 0) = GetIndex(LifeFormName, arr(5))
                AASpecies(slink, 0) = GetStrIndex(SpeciesName, arr(8))
                AASizeClass(slink, 0) = GetStrIndex(SizeClassNameXW, arr(9))
                AAHabGroup(slink) = GetStrIndex(HabGroupName, arr(5))
                If AAHabGroup(slink) < 0 Then AAHabGroup(slink) = 0 'default
                AAOwnership(slink) = GetStrIndex(OwnershipName, Trim(arr(17)))
                AASpecialArea(slink) = GetStrIndex(SpecialAreaName, Trim(arr(21)))
                If AASpecialArea(slink) < 0 Then AASpecialArea(slink) = 0 'default/unknown
                'AAAgeClass(slink, 0) = arr(8)
                AADensity(slink, 0) = -1 'default
                If arr(10) <> "None" Then AADensity(slink, 0) = arr(10) 'straight crosswalk - old:GetIndex(DensityNameLUT, arr(10))
                AAProcess(slink, 0) = -1 'default
                If arr(11) <> "?" Then AAProcess(slink, 0) = GetStrIndex(ProcessName, arr(11))
                PastProcessTimesteps(slink) = arr(12)
            End If
        Loop
        FileClose(fnum)

        'load the simulation data file
        FileOpen(fnum, SimpplleOutputFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                slink = arr(3)
                timestep = arr(1)
                AALifeForm(slink, timestep) = arr(5)
                AASpecies(slink, timestep) = arr(6)
                AASizeClass(slink, timestep) = arr(7)
                AAAgeClass(slink, timestep) = arr(8)
                AADensity(slink, timestep) = arr(9)
                AAProcess(slink, timestep) = arr(10)
            End If
        Loop
        FileClose(fnum)



    End Sub




    Public Sub RunFileCombiner(ByVal bIndexCol As Boolean, ByVal bStatSwitches() As Boolean, ByVal bOmitFirstCol As Boolean, ByVal NTextCols As Integer, ByVal NumericCols() As Integer, ByVal bPrintDetailedData As Boolean, ByVal bSort As Boolean)

        GetFileNames(FileListFN) ', bIndexCol)
        If bAddition = True Then AddFiles(OutputFile, NTextCols, NumericCols, bIndexCol)
        If bSummarize = True Then SummarizeFiles(OutputFile, bStatSwitches, bOmitFirstCol, NTextCols, NumericCols, bPrintDetailedData)
        If bConcatenate = True Then ConcatenateFiles(OutputFile, NTextCols, NumericCols, bSort)

        MsgBox("Done Combining")

    End Sub
    Private Sub AddFiles(ByVal OutputFileName As String, ByVal NTextCols As Integer, ByVal NumericCols() As Integer, ByVal bPrintRunIndex As Boolean)
        Dim fnum As Integer = FreeFile()
        Dim dummy As String
        Dim kindex As Integer
        Dim kcol As Integer
        Dim arr() As String
        Dim PeriodTotals(,,) As Double
        'Dim headerline As String

        Dim LineCount As Integer
        Dim jline As Integer
        Dim kline As Integer
        Dim LineVals() As String

        ReDim LineVals(LineCount)
        Dim NumberLineType() As Boolean
        ReDim NumberLineType(LineCount)
        Dim LineString() As String
        Dim BaseDir As String
        Dim BaseFN As String
        Dim OFName As String
        Dim fnum2 As Integer
        Dim MaxNumCol As Integer 'index of the last column to use in the summary
        Dim NSummCols As Integer = NumericCols.Length - 1

        'find the largest column index


        'count the lines to combine - ASSUMES ALL FILES ARE THE SAME LENGTH AND THAT THE FIRST FILE CAN BE USED TO REPRESENT THE REST
        FileOpen(fnum, FileNames(1), OpenMode.Input)
        LineCount = 0
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length > 0 Then
                LineCount = LineCount + 1
            End If
        Loop
        FileClose(fnum)

        ReDim LineVals(LineCount), LineString(LineCount)
        'once you figure out how big it is, populate the values
        FileOpen(fnum, FileNames(1), OpenMode.Input)
        kline = 0
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                kline = kline + 1
                For j As Integer = 0 To NTextCols - 1
                    LineVals(kline) = LineVals(kline) & arr(j) & ","
                Next
                LineString(kline) = dummy
            End If
        Loop
        FileClose(fnum)

        ReDim PeriodTotals(NIndices, LineCount, NSummCols), NumberLineType(LineCount)

        For jfile As Integer = 1 To NFiles
            FileOpen(fnum, FileNames(jfile), OpenMode.Input)
            kindex = FileIndex(jfile)
            jline = 0
            'If bHeaderCol Then
            '    headerline = LineInput(fnum)
            '    headerline = "Index," & headerline
            '    jline = 1
            'End If
            Do Until EOF(fnum)
                dummy = Trim(LineInput(fnum))
                If dummy.Length > 0 Then
                    jline = jline + 1
                    arr = Split(dummy, ",")
                    'determine if this is a text or numeric line...
                    If arr.Length > NTextCols Then NumberLineType(jline) = IsNumeric(Trim(arr(NTextCols))) 'otherwise, assume it's a placeholder text column
                    If NumberLineType(jline) = True Then
                        For j As Integer = 1 To NSummCols
                            kcol = NumericCols(j) + NTextCols - 1
                            PeriodTotals(kindex, jline, j) = PeriodTotals(kindex, jline, j) + CType(arr(kcol), Double)
                        Next
                    ElseIf jfile = 1 Then
                        For j As Integer = 1 To arr.Length - NTextCols - 1 ' NSummCols - 1
                            kcol = NumericCols(j) + NTextCols - 1
                            LineVals(jline) = LineVals(jline) & arr(kcol) & ","
                        Next
                        kcol = arr.Length - 1 'NumericCols(NSummCols) + NTextCols - 1
                        LineVals(jline) = LineVals(jline) & arr(kcol)

                    End If
                End If
            Loop
            FileClose(fnum)
        Next


        fnum = FreeFile()
        FileOpen(fnum, OutputFileName, OpenMode.Output)
        'If bSplitOutFiles = False Then If bHeaderCol Then PrintLine(fnum, headerline)

        For jindex As Integer = 1 To NIndices
            'HERE IS WHERE YOU WOULD OPEN A NEW FILE IF YOU HAD SPECIFIED TO SPLIT THE OUTPUT FILES
            'OFName = DirName(OutputFileName) & FName(OutputFileName) & "_" & jindex & ".txt"
            'FileOpen(fnum, OFName, OpenMode.Output)
            If bSplitOutFiles = True Then
                fnum2 = FreeFile()
                OFName = DirName(OutputFileName) & FName(OutputFileName) & "_" & jindex & ".txt"
                PrintLine(fnum, OFName)
                FileOpen(fnum2, OFName, OpenMode.Output)
                'if bHeaderCol Then PrintLine(fnum2, headerline)
            End If
            Dim startline As Integer = 1
            'If bHeaderCol Then startline = 2
            For jline = startline To LineCount
                If NumberLineType(jline) = True Then
                    dummy = LineVals(jline)
                    If bPrintRunIndex = True Then dummy = jindex & "," & LineVals(jline)

                    For j As Integer = 1 To NSummCols - 1
                        dummy = dummy & PeriodTotals(jindex, jline, j) & ","
                    Next
                    dummy = dummy & PeriodTotals(jindex, jline, NSummCols) 'don't put the comma after the last one
                Else
                    dummy = LineVals(jline)
                    If bPrintRunIndex = True Then dummy = jindex & "," & LineVals(jline) 'LineString(jline)
                End If
                If bSplitOutFiles = False Then PrintLine(fnum, dummy)
                If bSplitOutFiles = True Then PrintLine(fnum2, dummy)
            Next
            If bSplitOutFiles = True Then FileClose(fnum2)
        Next jindex
        FileClose(fnum)
    End Sub

    Private Sub ConcatenateFiles(ByVal OutputFileName As String, ByVal NTextCols As Integer, ByVal NumericCols() As Integer, ByVal bSortIndex As Boolean)
        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String
        Dim OFName As String
        Dim fnum2 As Integer
        Dim NSummCols As Integer = NumericCols.Length - 1
        Dim MaxInd As Integer = 0
        Dim fnumarr() As Integer
        Dim NOpenFiles As Integer
        Dim LastLine() As String
        Dim NextLine() As String
        Dim currper As Integer 'the period you are currently writing out
        Dim bFirstLineNumeric As Boolean
        Dim bEndOfLastFile As Boolean


        ReDim fnumarr(NFiles)
        fnum = FreeFile()
        FileOpen(fnum, OutputFileName, OpenMode.Output)
        For jind As Integer = 1 To NIndices
            fnum2 = FreeFile()
            OFName = DirName(OutputFileName) & FName(OutputFileName) & "_" & jind & ".txt"
            PrintLine(fnum, OFName)
            FileOpen(fnum2, OFName, OpenMode.Output)
            NOpenFiles = 0
            For jfile As Integer = 1 To NFiles
                If FileIndex(jfile) = jind Then
                    NOpenFiles = NOpenFiles + 1
                    fnumarr(NOpenFiles) = FreeFile()
                    FileOpen(fnumarr(NOpenFiles), FileNames(jfile), OpenMode.Input)
                End If
            Next
            ReDim LastLine(NOpenFiles), NextLine(NOpenFiles)
            'check to see whether the first line is numeric or text
            For jfile As Integer = 1 To NOpenFiles
                LastLine(jfile) = LineInput(fnumarr(jfile))
                NextLine(jfile) = LastLine(jfile)
            Next
            arr = Split(LastLine(1), ",")
            bFirstLineNumeric = IsNumeric(Trim(arr(arr.Length - 1)))
            If bFirstLineNumeric = False Then
                PrintLine(fnum2, LastLine(1))
                For jfile As Integer = 1 To NOpenFiles
                    LastLine(jfile) = ""
                    If EOF(fnumarr(jfile)) = False Then
                        LastLine(jfile) = LineInput(fnumarr(jfile))
                        NextLine(jfile) = LastLine(jfile)
                    End If
                Next
            End If

            'now concatenate
            If bSortIndex = False Then
                For jfile As Integer = 1 To NOpenFiles
                    If LastLine(jfile) <> "" Then PrintLine(fnum2, LastLine(jfile))
                    Do Until EOF(fnumarr(jfile))
                        LastLine(jfile) = LineInput(fnumarr(jfile))
                        If Trim(LastLine(jfile)).Length > 0 Then
                            PrintLine(fnum2, LastLine(jfile))
                        End If
                    Loop
                Next
            Else
                'you are sorting by the time periods
                'find the currper
                currper = 9999999
                For jfile As Integer = 1 To NOpenFiles
                    If LastLine(jfile) <> "" Then
                        arr = Split(LastLine(jfile), ",")
                        currper = Math.Min(CType(arr(0), Integer), currper)
                    End If
                Next jfile

                Do
                    For jfile As Integer = 1 To NOpenFiles
                        Do Until EOF(fnumarr(jfile))
                            If LastLine(jfile) <> "" Then
                                bEndOfLastFile = True
                                arr = Split(LastLine(jfile), ",")
                                If arr(0) = currper Then
                                    PrintLine(fnum2, LastLine(jfile))
                                    LastLine(jfile) = LineInput(fnumarr(jfile))
                                Else
                                    bEndOfLastFile = False
                                    Exit Do

                                End If
                            End If
                        Loop
                        If EOF(fnumarr(jfile)) Then
                            If LastLine(jfile) <> "" Then
                                bEndOfLastFile = True
                                arr = Split(LastLine(jfile), ",")
                                If arr(0) = currper Then
                                    PrintLine(fnum2, LastLine(jfile))
                                    LastLine(jfile) = ""
                                Else
                                    bEndOfLastFile = False
                                End If
                            End If
                        End If

                    Next
                    'need to flag if you reached the end of the last file
                    If bEndOfLastFile = True Then Exit Do
                    currper = 9999999
                    For jfile As Integer = 1 To NOpenFiles
                        If LastLine(jfile) <> "" Then
                            arr = Split(LastLine(jfile), ",")
                            currper = Math.Min(CType(arr(0), Integer), currper)
                        End If
                    Next jfile
                Loop

            End If

            For jfile As Integer = 1 To NOpenFiles
                FileClose(fnumarr(jfile))
            Next
            FileClose(fnum2)
        Next
        FileClose(fnum)







    End Sub

    Private Sub SummarizeFiles(ByVal OutputFileName As String, ByVal bStatSwitches() As Boolean, ByVal bOmitFirstCol As Boolean, ByVal NTextCols As Integer, ByVal NumericCols() As Integer, ByVal bPrintDetailedData As Boolean)
        Dim fnum As Integer = FreeFile()
        Dim kcol As Integer
        Dim dummy As String
        Dim dummy1 As String
        Dim dummy2 As String
        Dim dummy3 As String
        Dim dummy4 As String
        'Dim kindex As Integer
        Dim arr() As String
        Dim PeriodTotals(,) As Double
        Dim PeriodMin(,) As Double 'by line, by column
        Dim PeriodMax(,) As Double 'by line, by column
        Dim sNumber As String
        Dim Number As Double
        Dim GlobalMin() As Double 'by line
        Dim GlobalMax() As Double 'by line
        Dim GlobalTotal() As Double 'by line
        Dim GlobalTotalVals As Integer
        Dim PeriodVals(,,) As Double 'by line,column,val
        'Dim GlobalVals(,) As Double 'by line,val
        Dim Vals() As Double 'collected from period or global vals

        Dim headerline As String

        Dim LineCount As Integer
        Dim jline As Integer
        Dim LineVals() As String
        LineCount = 0
        ReDim LineVals(LineCount)
        Dim NumberLineType() As Boolean
        ReDim NumberLineType(LineCount)
        Dim LineString(,) As String
        Dim k As Integer

        Dim Q1 As Double
        Dim Q2 As Double
        Dim Q3 As Double

        Dim NSummCols As Integer = NumericCols.Length - 1

        'count the lines to combine - ASSUMES ALL FILES ARE THE SAME LENGTH AND THAT THE FIRST FILE CAN BE USED TO REPRESENT THE REST
        FileOpen(fnum, FileNames(1), OpenMode.Input)
        'If bHeaderCol Then
        '    headerline = LineInput(fnum)
        '    headerline = headerline & ",Totals"
        '    LineCount = LineCount + 1
        'End If

        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                LineCount = LineCount + 1
                ReDim Preserve LineVals(LineCount) ', LineString(LineCount)
                For j As Integer = 0 To NTextCols - 1
                    LineVals(LineCount) = LineVals(LineCount) & arr(j) & ","
                Next
                'LineString(LineCount) = dummy
            End If
        Loop
        FileClose(fnum)

        ReDim LineString(NFiles, LineCount), GlobalMin(LineCount), GlobalMax(LineCount), GlobalTotal(LineCount)
        ReDim PeriodTotals(LineCount, NSummCols), NumberLineType(LineCount)
        ReDim PeriodMin(LineCount, NSummCols), PeriodMax(LineCount, NSummCols)
        ReDim PeriodVals(LineCount, NSummCols, NFiles)
        For jline2 As Integer = 1 To LineCount
            GlobalMin(jline2) = 9999999999 'really big number
            GlobalMax(jline2) = -9999999999 'really small number
            For jcol As Integer = 1 To NSummCols
                PeriodMin(jline2, jcol) = 9999999999 'really big number
                PeriodMax(jline2, jcol) = -9999999999 'really small number
            Next
        Next

        GlobalTotalVals = NFiles * NSummCols
        If bOmitFirstCol = True Then GlobalTotalVals = GlobalTotalVals - NFiles 'there are NFiles values in the first column you don't want to count

        For jfile As Integer = 1 To NFiles
            FileOpen(fnum, FileNames(jfile), OpenMode.Input)
            'kindex = FileIndex(jfile)
            jline = 0
            'If bHeaderCol Then
            '    headerline = LineInput(fnum) & ",Totals"
            '    jline = 1
            'End If
            Do Until EOF(fnum)
                dummy = Trim(LineInput(fnum))
                If dummy.Length > 0 Then
                    jline = jline + 1
                    'LineString(jfile, jline) = dummy
                    arr = Split(dummy, ",")
                    'determine if this is a text or numeric line...
                    NumberLineType(jline) = IsNumeric(Trim(arr(NTextCols)))
                    For j As Integer = 1 To NTextCols
                        LineString(jfile, jline) = LineString(jfile, jline) & arr(j - 1) & ","
                    Next
                    'we are adding a metric column
                    LineString(jfile, jline) = LineString(jfile, jline) & "Metric"

                    For j As Integer = 1 To NSummCols
                        kcol = NumericCols(j)
                        sNumber = Trim(arr(kcol + (NTextCols - 1)))
                        LineString(jfile, jline) = LineString(jfile, jline) & "," & sNumber
                        If NumberLineType(jline) = True Then
                            Number = CType(sNumber, Double)
                            PeriodVals(jline, j, jfile) = Number
                            PeriodTotals(jline, j) = PeriodTotals(jline, j) + Number
                            If Number < PeriodMin(jline, j) Then PeriodMin(jline, j) = Number
                            If Number > PeriodMax(jline, j) Then PeriodMax(jline, j) = Number
                            If bOmitFirstCol = False Or j > 1 Then
                                GlobalMin(jline) = Math.Min(GlobalMin(jline), Number)
                                GlobalMax(jline) = Math.Max(GlobalMax(jline), Number)
                                GlobalTotal(jline) = GlobalTotal(jline) + Number
                            End If
                        End If
                    Next
                    If NumberLineType(jline) = False Then LineString(jfile, jline) = LineString(jfile, jline) & ",Totals"
                End If
            Loop
            FileClose(fnum)
        Next


        FileOpen(fnum, OutputFileName, OpenMode.Output)
        'If bHeaderCol Then PrintLine(fnum, "," & headerline)
        'For jindex As Integer = 1 To NIndices
        'OFName = DirName(OutputFileName) & FName(OutputFileName) & "_" & jindex & ".txt"
        'FileOpen(fnum, OFName, OpenMode.Output)
        Dim startline As Integer = 1
        'If bHeaderCol Then startline = 2
        'print out just summary information
        For jline = startline To LineCount
            If NumberLineType(jline) = True Then
                If bStatSwitches(1) = True Then
                    dummy = LineVals(jline) & "Average"
                    For j As Integer = 1 To NSummCols
                        dummy = dummy & "," & Math.Round(PeriodTotals(jline, j) / NFiles, 0)
                    Next
                    dummy = dummy & "," & Math.Round(GlobalTotal(jline) / GlobalTotalVals, 0)
                    PrintLine(fnum, dummy)
                End If
                'min line
                'PrintLine(fnum, "Minimum")
                If bStatSwitches(2) = True And bStatSwitches(5) = False Then
                    dummy = LineVals(jline) & "Minimum"
                    For j As Integer = 1 To NSummCols
                        dummy = dummy & "," & PeriodMin(jline, j)
                    Next
                    dummy = dummy & "," & GlobalMin(jline)
                    PrintLine(fnum, dummy)
                    'max line
                    'PrintLine(fnum, "Maximum"
                    dummy = LineVals(jline) & "Maximum"
                    For j As Integer = 1 To NSummCols
                        dummy = dummy & "," & PeriodMax(jline, j)
                    Next
                    dummy = dummy & "," & GlobalMax(jline)
                    PrintLine(fnum, dummy)
                End If
                If bStatSwitches(3) = True Then
                    'standard deviation line - includes stuff at the end
                    dummy = LineVals(jline) & "StDev"
                    For j As Integer = 1 To NSummCols
                        ReDim Vals(NFiles)
                        For jfile As Integer = 1 To NFiles
                            Vals(jfile) = Math.Round(PeriodVals(jline, j, jfile), 0)
                        Next
                        dummy = dummy & "," & Math.Round(CalcStdDev(Vals), 0)
                    Next
                    'now find the global standard deviation
                    ReDim Vals(GlobalTotalVals)
                    k = 0
                    For jfile As Integer = 1 To NFiles
                        For j As Integer = 1 To NSummCols
                            If bOmitFirstCol = False Or j > 1 Then
                                k = k + 1
                                Vals(k) = PeriodVals(jline, j, jfile)
                            End If
                        Next
                    Next
                    dummy = dummy & "," & CalcStdDev(Vals)
                    PrintLine(fnum, dummy)
                End If
                If bStatSwitches(4) = True And bStatSwitches(5) = False Then
                    'print out the quartiles
                    dummy1 = LineVals(jline) & "Q1"
                    dummy2 = LineVals(jline) & "Q2"
                    dummy3 = LineVals(jline) & "Q3"
                    For j As Integer = 1 To NSummCols
                        ReDim Vals(NFiles - 1)
                        For jfile As Integer = 1 To NFiles
                            Vals(jfile - 1) = Math.Round(PeriodVals(jline, j, jfile), 0) 'populate the 0 place in Vals
                        Next
                        CalcQuartiles(Vals, Q1, Q2, Q3)
                        dummy1 = dummy1 & "," & Q1
                        dummy2 = dummy2 & "," & Q2
                        dummy3 = dummy3 & "," & Q3
                    Next
                    'now find the global standard deviation
                    ReDim Vals(GlobalTotalVals - 1)
                    k = -1 'populates vals(0) with the first code
                    For jfile As Integer = 1 To NFiles
                        For j As Integer = 1 To NSummCols
                            If bOmitFirstCol = False Or j > 1 Then
                                k = k + 1
                                Vals(k) = PeriodVals(jline, j, jfile)
                            End If
                        Next
                    Next
                    CalcQuartiles(Vals, Q1, Q2, Q3)
                    dummy1 = dummy1 & "," & Q1
                    dummy2 = dummy2 & "," & Q2
                    dummy3 = dummy3 & "," & Q3
                    PrintLine(fnum, dummy1)
                    PrintLine(fnum, dummy2)
                    PrintLine(fnum, dummy3)
                End If
                If bStatSwitches(5) = True Then
                    dummy = LineVals(jline) & "Minimum"
                    dummy4 = LineVals(jline) & "Maximum"
                    For j As Integer = 1 To NSummCols
                        dummy = dummy & "," & PeriodMin(jline, j)
                    Next
                    dummy = dummy & "," & GlobalMin(jline)
                    PrintLine(fnum, dummy)

                    'print out the quartiles
                    dummy1 = LineVals(jline) & "Q1"
                    dummy2 = LineVals(jline) & "Q2"
                    dummy3 = LineVals(jline) & "Q3"
                    For j As Integer = 1 To NSummCols
                        ReDim Vals(NFiles - 1)
                        For jfile As Integer = 1 To NFiles
                            Vals(jfile - 1) = PeriodVals(jline, j, jfile) 'populate the 0 place in Vals
                        Next
                        CalcQuartiles(Vals, Q1, Q2, Q3)
                        dummy1 = dummy1 & "," & Q1 - PeriodMin(jline, j)
                        dummy2 = dummy2 & "," & Q2 - Q1
                        dummy3 = dummy3 & "," & Q3 - Q2
                        dummy4 = dummy4 & "," & PeriodMax(jline, j) - Q3
                    Next
                    'now find the global standard deviation
                    ReDim Vals(GlobalTotalVals - 1)
                    k = -1 'populates vals(0) with the first code
                    For jfile As Integer = 1 To NFiles
                        For j As Integer = 1 To NSummCols
                            If bOmitFirstCol = False Or j > 1 Then
                                k = k + 1
                                Vals(k) = PeriodVals(jline, j, jfile)
                            End If
                        Next
                    Next
                    CalcQuartiles(Vals, Q1, Q2, Q3)
                    dummy1 = dummy1 & "," & Q1 - GlobalMin(jline)
                    dummy2 = dummy2 & "," & Q2 - Q1
                    dummy3 = dummy3 & "," & Q3 - Q2
                    dummy4 = dummy4 & "," & GlobalMax(jline) - Q3
                    PrintLine(fnum, dummy1)
                    PrintLine(fnum, dummy2)
                    PrintLine(fnum, dummy3)
                    PrintLine(fnum, dummy4)
                End If
            Else
                dummy = LineString(1, jline)
                PrintLine(fnum, dummy)
            End If

        Next
        If bPrintDetailedData = True Then
            'print out stuff from all files
            For jline = startline To LineCount
                If NumberLineType(jline) = True Then
                    For jsum As Integer = 1 To NFiles
                        PrintLine(fnum, LineString(jsum, jline))
                    Next jsum
                    'average line
                    'PrintLine(fnum, )
                    'dummy = LineVals(jline) & "Average,"
                    'For j As Integer = 1 To NSummCols - 1
                    '    dummy = dummy & Math.Round(PeriodTotals(jline, j) / NFiles, 0) & ","
                    'Next
                    'dummy = dummy & Math.Round(PeriodTotals(jline, NSummCols) / NFiles, 0) 'don't put the comma after the last one
                    'PrintLine(fnum, dummy)
                    ''min line
                    ''PrintLine(fnum, "Minimum")
                    'dummy = LineVals(jline) & "Minimum,"
                    'For j As Integer = 1 To NSummCols - 1
                    '    dummy = dummy & PeriodMin(jline, j) & ","
                    'Next
                    'dummy = dummy & PeriodMin(jline, NSummCols)  'don't put the comma after the last one
                    'PrintLine(fnum, dummy)
                    ''max line
                    ''PrintLine(fnum, "Maximum")
                    'dummy = LineVals(jline) & "Maximum,"
                    'For j As Integer = 1 To NSummCols - 1
                    '    dummy = dummy & PeriodMax(jline, j) & ","
                    'Next
                    'dummy = dummy & PeriodMax(jline, NSummCols)  'don't put the comma after the last one
                    'PrintLine(fnum, dummy)
                Else
                    dummy = LineString(1, jline)
                    PrintLine(fnum, dummy)
                End If

            Next
        End If
        'Next jindex
        FileClose(fnum)
    End Sub

    Private Sub GetFileNames(ByVal file As String) ', ByVal bFirstColRunIndex As Boolean)
        Dim fnum As Integer = FreeFile()
        Dim dummy As String
        Dim arr() As String
        'Dim MaxIndex As Integer

        'default 
        'If bFirstColRunIndex = False Then
        NIndices = 1
        'MaxIndex = NIndices
        'End If


        FileOpen(fnum, file, OpenMode.Input)
        NFiles = 0
        ReDim FileNames(NFiles)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                NFiles = NFiles + 1
                ReDim Preserve FileNames(NFiles), FileIndex(NFiles)
                'If bFirstColRunIndex = False Then
                '    FileNames(NFiles) = Trim(dummy)
                '    FileIndex(NFiles) = 1
                'Else
                arr = Split(dummy, ",")
                If arr.Length = 2 Then
                    FileNames(NFiles) = Trim(arr(1))
                    FileIndex(NFiles) = Trim(arr(0))
                    If FileIndex(NFiles) > NIndices Then NIndices = FileIndex(NFiles)
                ElseIf arr.Length = 1 Then
                    FileNames(NFiles) = Trim(dummy)
                    FileIndex(NFiles) = 1
                End If
                'End If
            End If
        Loop
        FileClose(fnum)
    End Sub


    Public Sub ConvertFireSpreadtoMMaPPit(ByVal subFiles() As String, ByVal subNSLINK() As Long, ByVal subNPers() As Integer, ByVal PerFilter() As Integer)
        Dim fnum As Integer
        'Dim filename As String
        Dim ofnum As Integer
        Dim dummy As String
        Dim sdummy As StringBuilder
        'Dim NSLINK As Integer
        'Dim NPers As Integer
        Dim per As Integer
        Dim slink As Integer
        Dim slink2 As Integer
        Dim SLINKFireEventByPer(,) As Integer 'holds index of originating fire - dimmed by nslink,npers
        Dim arr() As String
        Dim arr2() As String
        Dim arr3() As String
        Dim ind As Integer
        Dim bFilterPer As Boolean
        bFilterPer = False
        If PerFilter.Length > 1 Then bFilterPer = True

        'GetFileNames(infile, False) 'returns FileNames and NFiles

        For jfile As Integer = 1 To subFiles.Length - 1 'NFiles 'process each of these files
            If System.IO.File.Exists(subFiles(jfile)) Then
                fnum = FreeFile()
                'arr = Split(FileNames(jfile), ",") 'first is file name, second is NSLINK, third is NPers
                'filename = arr(0)
                'NSLINK = arr(1)
                'NPers = arr(2)
                If bFilterPer = False Then ReDim PerFilter(subNPers(jfile)) 'to keep it from crashing on period filter test below
                ReDim SLINKFireEventByPer(subNSLINK(jfile), subNPers(jfile))
                FileOpen(fnum, subFiles(jfile), OpenMode.Input)
                Do Until EOF(fnum) 'first read the number of periods, number of SLINKs
                    dummy = LineInput(fnum)
                    If Trim(dummy) = "Fire Event Summary" Then Exit Do
                    arr = Split(dummy, " ")
                    ind = -1
                    ReDim arr3(arr.Length - 1)
                    For j As Integer = 0 To arr.Length - 1
                        If arr(j) <> "" Then
                            ind = ind + 1
                            arr3(ind) = arr(j)
                        End If
                    Next
                    If ind > 1 Then
                        'If arr3(2) = "CLASS-A" Then
                        '    slink = arr3(1)
                        '    per = arr3(0)
                        '    SLINKFireEventByPer(slink, per) = slink
                        'Else
                        If arr3(2) = "MSF" Or arr3(2) = "LSF" Or arr3(2) = "SRF" Then
                            slink = arr3(1)
                            per = arr3(0)
                            If per > subNPers(jfile) Then Exit Do 'you're not using all time periods in the file and have not dimmed slinkfireeenbyper big enough to handle
                            For j As Integer = 9 To arr3.Length - 1
                                If arr3(j) <> "" Then
                                    arr2 = Split(arr3(j), "-")
                                    slink2 = arr2(0)
                                    SLINKFireEventByPer(slink2, per) = slink 'index the fire by the origin slink
                                End If
                            Next
                        End If
                    End If
                Loop
                FileClose(fnum)
                ofnum = FreeFile()
                FileOpen(ofnum, DirName(subFiles(jfile)) & FName(subFiles(jfile)) & "_MMaPPit.txt", OpenMode.Output)
                sdummy = New StringBuilder("SLINK", 300)
                'dummy = "SLINK"
                For jper As Integer = 0 To subNPers(jfile)
                    If (bFilterPer = True And PerFilter(jper) = 1) Or bFilterPer = False Or jper = 0 Then
                        sdummy.Append("," & jper)
                        'dummy = dummy & "," & jper
                    End If
                Next
                PrintLine(ofnum, sdummy.ToString)
                sdummy = Nothing

                For jslink As Integer = 1 To subNSLINK(jfile)
                    sdummy = New StringBuilder(jslink.ToString, 300)
                    For jper As Integer = 0 To subNPers(jfile)
                        If (bFilterPer = True And PerFilter(jper) = 1) Or bFilterPer = False Or jper = 0 Then
                            sdummy.Append("," & SLINKFireEventByPer(jslink, jper))
                            'dummy = dummy & "," & SLINKFireEventByPer(jslink, jper)
                        End If
                    Next
                    PrintLine(ofnum, sdummy.ToString)
                    sdummy = Nothing
                Next
                FileClose(ofnum)
            End If
        Next

    End Sub

    Private Function CalcStdDev(ByVal Vals() As Double) As Double
        Dim DF As Integer 'degrees of freedom
        Dim NVals As Integer
        Dim SqDev As Double
        Dim avg As Double
        Dim total As Double
        'first find the average
        NVals = Vals.Length - 1
        DF = NVals - 1
        For j As Integer = 1 To NVals
            total = total + Vals(j)
        Next
        avg = total / NVals
        For j As Integer = 1 To NVals
            SqDev = SqDev + (Vals(j) - avg) ^ 2
        Next
        CalcStdDev = Math.Sqrt(SqDev / DF)

    End Function

    Private Sub CalcQuartiles(ByVal Vals() As Double, ByRef Q1 As Double, ByRef Q2 As Double, ByRef Q3 As Double)
        Dim Q2indU As Single 'upper index of quartile 2
        Dim Q2indL As Single 'lower index of quartile 2 - can be the same as the upper
        Dim Q1ind As Single
        Dim Q3ind As Single
        'assume unordered values, and assume the 0 value is populated
        System.Array.Sort(Vals)
        If Vals.Length < 3 Then
            Q1 = Vals(0)
            Q2 = Vals(0)
            Q3 = Vals(0)
            Exit Sub
        End If
        Q2indL = (Vals.Length / 2) - 1 'account for 0-based array
        If Q2indL - Math.Round(Q2indL, 0) = 0 Then
            Q2indU = Q2indL + 1
            Q2 = (Vals(Q2indL) + Vals(Q2indU)) / 2
        Else
            Q2indL = Math.Round(Q2indL + 0.5, 0)
            Q2indU = Q2indL
            Q2 = Vals(Q2indL)
        End If
        Q1ind = Q2indL / 2
        Q3ind = Q2indU + Q1ind
        Q1 = Vals(Q1ind)
        Q3 = Vals(Q3ind)


    End Sub



    Public Sub RunPatchStats(ByVal infile As String, ByVal outfile As String, ByVal minval As Single, ByVal minvalcol As Integer)
        Dim ofname As String

        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, outfile, OpenMode.Output)
        GetFileNames(infile) ', False)
        For jfile As Integer = 1 To NFiles
            ofname = DirName(outfile) & FName(outfile) & "_" & jfile & "_PatchMetrics.txt"
            PrintLine(fnum, ofname)
            PatchStats(FileNames(jfile), ofname, minval, minvalcol)
        Next

        FileClose(fnum)
        MsgBox("Done with patches")
    End Sub
    Private Sub PatchStats(ByVal infile As String, ByVal outfile As String, ByVal minval As Single, ByVal minvalcol As Integer)
        'WARNING - THIS IS INITIALLY CODED TO ONLY LOOK AT A FILE WITH METRICS FOR A SINGLE PATCH TYPE

        Dim NPatches(,) As Integer 'by tic, habitat type
        Dim TicTotalArea(,) As Double 'by tic, habitat type
        Dim AWM_Av_Size(,) As Double 'by tic,habitat type


        'for evaluating when the patches have been fully defined
        Dim matchper As Integer = 1
        Dim matchstand As Integer = 1

        Dim fnum As Integer
        Dim fnum2 As Integer

        Dim dummy As String
        Dim dummy2 As String
        Dim sdummy As StringBuilder
        Dim arr() As String
        Dim ktic As Integer
        Dim NTic As Integer
        Dim QName As String
        Dim QVal As String
        Dim areaval As Double
        Dim filterval As Single

        Dim WriteTimestep() As Integer

        'read the input file and count the number of time periods
        fnum2 = FreeFile()
        FileOpen(fnum2, infile, OpenMode.Input)
        Do Until EOF(fnum2)
            dummy = Trim(LineInput(fnum2))
            If dummy.Length > 0 Then dummy2 = dummy
        Loop
        FileClose(fnum2)
        arr = Split(dummy2, ",") 'key off of last line
        NTic = CType(arr(0), Integer)
        QName = arr(1)
        QVal = arr(2)
        ReDim WriteTimestep(NTic), NPatches(NTic, 1), TicTotalArea(NTic, 1), AWM_Av_Size(NTic, 1)

        'read to calculate the total area and npatches
        fnum2 = FreeFile()
        FileOpen(fnum2, infile, OpenMode.Input)
        'assume a header line
        dummy = LineInput(fnum2)
        Do Until EOF(fnum2)
            dummy = Trim(LineInput(fnum2))
            If dummy.Length = 0 Then Exit Do
            arr = Split(dummy, ",")
            'check for filter
            filterval = CType(arr(minvalcol), Single)
            If filterval > minval Then
                ktic = arr(0)
                NPatches(ktic, 1) = NPatches(ktic, 1) + 1
                TicTotalArea(ktic, 1) = TicTotalArea(ktic, 1) + arr(5)
                WriteTimestep(ktic) = 1
            End If
        Loop
        FileClose(fnum2)

        'read AGAIN to calculate the AWM
        fnum2 = FreeFile()
        FileOpen(fnum2, infile, OpenMode.Input)
        'assume a header line
        dummy = LineInput(fnum2)
        Do Until EOF(fnum2)
            dummy = Trim(LineInput(fnum2))
            If dummy.Length = 0 Then Exit Do
            arr = Split(dummy, ",")
            filterval = CType(arr(minvalcol), Single)
            If filterval > minval Then
                ktic = arr(0)
                areaval = CType(arr(5), Double)
                AWM_Av_Size(ktic, 1) = AWM_Av_Size(ktic, 1) + (areaval ^ 2) / TicTotalArea(ktic, 1)
            End If
        Loop
        FileClose(fnum2)


        'Dim fnum As Integer = FreeFile()
        fnum = FreeFile()
        FileOpen(fnum, outfile, OpenMode.Output)
        sdummy = New StringBuilder("Query_Name, Query_Value, Metric", 300)
        For jtic As Integer = 0 To NTic
            'If WriteTimestep(jtic) = 1 Then 
            sdummy.Append(", TIME_" & jtic) ' dummy = dummy & ", TIME_" & jtic
        Next
        PrintLine(fnum, sdummy.ToString)
        sdummy = Nothing
        'first write npatches line
        sdummy = New StringBuilder(QName & "," & QVal & ",NPatches", 300)
        For jtic As Integer = 0 To NTic
            'If WriteTimestep(jtic) = 1 Then 
            sdummy.Append("," & NPatches(jtic, 1))
        Next
        PrintLine(fnum, sdummy.ToString)
        sdummy = Nothing
        'then write totalarea line
        sdummy = New StringBuilder(QName & "," & QVal & ",TotalArea", 300)
        For jtic As Integer = 0 To NTic
            'If WriteTimestep(jtic) = 1 Then 
            sdummy.Append("," & Math.Round(TicTotalArea(jtic, 1), 2))
        Next
        PrintLine(fnum, sdummy.ToString)
        sdummy = Nothing

        'average size line
        sdummy = New StringBuilder(QName & "," & QVal & ",AverageSize", 300)
        For jtic As Integer = 0 To NTic
            If WriteTimestep(jtic) = 1 Then
                sdummy.Append("," & Math.Round(TicTotalArea(jtic, 1) / NPatches(jtic, 1), 2))
            Else
                sdummy.Append(",0")
            End If

        Next
        PrintLine(fnum, sdummy.ToString)
        sdummy = Nothing
        'AWM average size line
        sdummy = New StringBuilder(QName & "," & QVal & ",AWM_Av_Size", 300)
        For jtic As Integer = 0 To NTic
            'If WriteTimestep(jtic) = 1 Then 
            sdummy.Append("," & Math.Round(AWM_Av_Size(jtic, 1), 2))
        Next
        PrintLine(fnum, sdummy.ToString)
        sdummy = Nothing

        FileClose(fnum)




        'FileClose(dummy)
        'For jtic As Integer = 0 To NTimestep
        '    For jwltype As Integer = 1 To NWLSpeciesTypes(kspec)
        '        If NPatches(jtic, jwltype) > 0 Then
        '            'find the area weighted mean for this patch type
        '            awm = 0
        '            For jaa As Integer = 1 To NSLINK
        '                If PatchType(jaa, jtic) = jwltype Then awm = awm + (WLPatchSize(jaa, kspec, jtic) ^ 2) / TicTotalArea(jtic, jwltype)
        '            Next
        '            PrintLine(fnum, jtic & "," & WLSpeciesName(kspec) & "," & WLSpeciesTypeName(kspec, jwltype) & "," & NPatches(jtic, jwltype) & "," & _
        '               Math.Round(TicTotalArea(jtic, jwltype), 1) & "," & Math.Round(TicTotalArea(jtic, jwltype) / NPatches(jtic, jwltype), 1) & "," & Math.Round(awm, 1))
        '        End If
        '    Next jwltype
        'Next
        'FileClose(fnum)
    End Sub

End Class
