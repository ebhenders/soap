<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ProbabilitySurfaces
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.cbAcrossTime = New System.Windows.Forms.CheckBox
        Me.cbAcrossRuns = New System.Windows.Forms.CheckBox
        Me.cbAcrossTimeAndRuns = New System.Windows.Forms.CheckBox
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.rbTimePers = New System.Windows.Forms.RadioButton
        Me.RadioButton1 = New System.Windows.Forms.RadioButton
        Me.tbTimePers = New System.Windows.Forms.TextBox
        Me.cbWriteColorFile = New System.Windows.Forms.CheckBox
        Me.tbNBreaks = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.rbBlue = New System.Windows.Forms.RadioButton
        Me.rbGreen = New System.Windows.Forms.RadioButton
        Me.rbRed = New System.Windows.Forms.RadioButton
        Me.cbZeroClass = New System.Windows.Forms.CheckBox
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(159, 2)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(113, 33)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "What does this do?"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(85, 200)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(84, 53)
        Me.Button2.TabIndex = 8
        Me.Button2.Text = "GO"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'cbAcrossTime
        '
        Me.cbAcrossTime.AutoSize = True
        Me.cbAcrossTime.Location = New System.Drawing.Point(57, 41)
        Me.cbAcrossTime.Name = "cbAcrossTime"
        Me.cbAcrossTime.Size = New System.Drawing.Size(143, 17)
        Me.cbAcrossTime.TabIndex = 9
        Me.cbAcrossTime.Text = "Probabilities Across Time"
        Me.cbAcrossTime.UseVisualStyleBackColor = True
        '
        'cbAcrossRuns
        '
        Me.cbAcrossRuns.AutoSize = True
        Me.cbAcrossRuns.Location = New System.Drawing.Point(57, 81)
        Me.cbAcrossRuns.Name = "cbAcrossRuns"
        Me.cbAcrossRuns.Size = New System.Drawing.Size(145, 17)
        Me.cbAcrossRuns.TabIndex = 10
        Me.cbAcrossRuns.Text = "Probabilities Across Runs"
        Me.cbAcrossRuns.UseVisualStyleBackColor = True
        '
        'cbAcrossTimeAndRuns
        '
        Me.cbAcrossTimeAndRuns.AutoSize = True
        Me.cbAcrossTimeAndRuns.Location = New System.Drawing.Point(57, 118)
        Me.cbAcrossTimeAndRuns.Name = "cbAcrossTimeAndRuns"
        Me.cbAcrossTimeAndRuns.Size = New System.Drawing.Size(192, 17)
        Me.cbAcrossTimeAndRuns.TabIndex = 11
        Me.cbAcrossTimeAndRuns.Text = "Probabilities Across Time and Runs"
        Me.cbAcrossTimeAndRuns.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(20, 32)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(31, 32)
        Me.Button3.TabIndex = 12
        Me.Button3.Text = "?"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(20, 72)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(31, 32)
        Me.Button4.TabIndex = 13
        Me.Button4.Text = "?"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(20, 110)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(31, 32)
        Me.Button5.TabIndex = 14
        Me.Button5.Text = "?"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'rbTimePers
        '
        Me.rbTimePers.AutoSize = True
        Me.rbTimePers.Location = New System.Drawing.Point(20, 174)
        Me.rbTimePers.Name = "rbTimePers"
        Me.rbTimePers.Size = New System.Drawing.Size(111, 17)
        Me.rbTimePers.TabIndex = 15
        Me.rbTimePers.Text = "Use Time Periods:"
        Me.rbTimePers.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Checked = True
        Me.RadioButton1.Location = New System.Drawing.Point(20, 151)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(149, 17)
        Me.RadioButton1.TabIndex = 16
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Use All Time Periods in file"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'tbTimePers
        '
        Me.tbTimePers.Location = New System.Drawing.Point(127, 174)
        Me.tbTimePers.Name = "tbTimePers"
        Me.tbTimePers.Size = New System.Drawing.Size(145, 20)
        Me.tbTimePers.TabIndex = 17
        '
        'cbWriteColorFile
        '
        Me.cbWriteColorFile.AutoSize = True
        Me.cbWriteColorFile.Checked = True
        Me.cbWriteColorFile.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbWriteColorFile.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbWriteColorFile.Location = New System.Drawing.Point(15, 271)
        Me.cbWriteColorFile.Name = "cbWriteColorFile"
        Me.cbWriteColorFile.Size = New System.Drawing.Size(206, 20)
        Me.cbWriteColorFile.TabIndex = 19
        Me.cbWriteColorFile.Text = "Write Color File (Optional)"
        Me.cbWriteColorFile.UseVisualStyleBackColor = True
        '
        'tbNBreaks
        '
        Me.tbNBreaks.Location = New System.Drawing.Point(15, 296)
        Me.tbNBreaks.Name = "tbNBreaks"
        Me.tbNBreaks.Size = New System.Drawing.Size(38, 20)
        Me.tbNBreaks.TabIndex = 20
        Me.tbNBreaks.Text = "10"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(66, 298)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(81, 13)
        Me.Label2.TabIndex = 21
        Me.Label2.Text = "N Color Classes"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rbBlue)
        Me.GroupBox1.Controls.Add(Me.rbGreen)
        Me.GroupBox1.Controls.Add(Me.rbRed)
        Me.GroupBox1.Location = New System.Drawing.Point(-2, 314)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(149, 84)
        Me.GroupBox1.TabIndex = 25
        Me.GroupBox1.TabStop = False
        '
        'rbBlue
        '
        Me.rbBlue.AutoSize = True
        Me.rbBlue.Location = New System.Drawing.Point(32, 58)
        Me.rbBlue.Name = "rbBlue"
        Me.rbBlue.Size = New System.Drawing.Size(78, 17)
        Me.rbBlue.TabIndex = 27
        Me.rbBlue.Text = "Blue Colors"
        Me.rbBlue.UseVisualStyleBackColor = True
        '
        'rbGreen
        '
        Me.rbGreen.AutoSize = True
        Me.rbGreen.Location = New System.Drawing.Point(31, 35)
        Me.rbGreen.Name = "rbGreen"
        Me.rbGreen.Size = New System.Drawing.Size(86, 17)
        Me.rbGreen.TabIndex = 26
        Me.rbGreen.Text = "Green Colors"
        Me.rbGreen.UseVisualStyleBackColor = True
        '
        'rbRed
        '
        Me.rbRed.AutoSize = True
        Me.rbRed.Checked = True
        Me.rbRed.Location = New System.Drawing.Point(31, 12)
        Me.rbRed.Name = "rbRed"
        Me.rbRed.Size = New System.Drawing.Size(77, 17)
        Me.rbRed.TabIndex = 25
        Me.rbRed.TabStop = True
        Me.rbRed.Text = "Red Colors"
        Me.rbRed.UseVisualStyleBackColor = True
        '
        'cbZeroClass
        '
        Me.cbZeroClass.AutoSize = True
        Me.cbZeroClass.Location = New System.Drawing.Point(153, 299)
        Me.cbZeroClass.Name = "cbZeroClass"
        Me.cbZeroClass.Size = New System.Drawing.Size(121, 17)
        Me.cbZeroClass.TabIndex = 26
        Me.cbZeroClass.Text = "0 value is own class"
        Me.cbZeroClass.UseVisualStyleBackColor = True
        '
        'ProbabilitySurfaces
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 401)
        Me.Controls.Add(Me.cbZeroClass)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.tbNBreaks)
        Me.Controls.Add(Me.cbWriteColorFile)
        Me.Controls.Add(Me.tbTimePers)
        Me.Controls.Add(Me.RadioButton1)
        Me.Controls.Add(Me.rbTimePers)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.cbAcrossTimeAndRuns)
        Me.Controls.Add(Me.cbAcrossRuns)
        Me.Controls.Add(Me.cbAcrossTime)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Name = "ProbabilitySurfaces"
        Me.Text = "ProbabilitySurfaces"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents cbAcrossTime As System.Windows.Forms.CheckBox
    Friend WithEvents cbAcrossRuns As System.Windows.Forms.CheckBox
    Friend WithEvents cbAcrossTimeAndRuns As System.Windows.Forms.CheckBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents rbTimePers As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents tbTimePers As System.Windows.Forms.TextBox
    Friend WithEvents cbWriteColorFile As System.Windows.Forms.CheckBox
    Friend WithEvents tbNBreaks As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rbBlue As System.Windows.Forms.RadioButton
    Friend WithEvents rbGreen As System.Windows.Forms.RadioButton
    Friend WithEvents rbRed As System.Windows.Forms.RadioButton
    Friend WithEvents cbZeroClass As System.Windows.Forms.CheckBox
End Class
