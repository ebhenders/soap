Public Class frmPatchStats

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim minval As Single
        Dim minvalcol As Integer

        FileListFN = tbBatchFile.Text
        OutputFile = tbOutputFile.Text
        minval = CType(tbFilterValue.Text, Single)
        minvalcol = CType(tbFilterValueCol.Text, Integer)
        Dim patchstats As New clProcessSimpplle
        patchstats.RunPatchStats(FileListFN, OutputFile, minval, minvalcol)
        patchstats = Nothing

    End Sub

    Private Sub PatchStats_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim openfiledialog2 As New OpenFileDialog
        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select a batch file with paths to patches files"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        tbBatchFile.Text = openfiledialog2.FileName

        openfiledialog2 = Nothing

        Dim savefiledialog As New SaveFileDialog

        With savefiledialog
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Save the output file as"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            .FileName = "PatchOutput"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        tbOutputFile.Text = savefiledialog.FileName
        savefiledialog = Nothing

    End Sub
End Class