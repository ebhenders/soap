Public Class SpectrumToSIMPPLLE

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'IDENTIFY THE FILE WITH ACREAGE BY GEOGRAPHIC AREA
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select a file with Spectrum AA Acres by Geographic Area"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        tbAcresByGeoArea.Text = openfiledialog2.FileName
        openfiledialog2 = Nothing
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        'IDENTIFY THE FOLDER WITH TRANSLATION RULES
        Dim folder As New FolderBrowserDialog

        With folder
            '.InitialDirectory = "d:\Analysis\"
            .Description = "Select a file with Spectrum to SIMPPLLE Translation Rules"

            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        tbModelDefFolder.Text = folder.SelectedPath
        folder = Nothing
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'IDENTIFY THE FILE WITH SPECTRUM OUTPUTS - TREATMENT INFORMATION
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select a Spectrum Output File"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        tbSpectrumOutputFile.Text = openfiledialog2.FileName
        openfiledialog2 = Nothing
    End Sub

    Private Sub CompileButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CompileButton.Click
        Dim baseoutfn As String
        Dim ownsa As String
        'DEFINE THE FILE PATHS AND TIMESTEPS
        SpecToSIMModDefFolder = tbModelDefFolder.Text
        AAByGeoAreaFN = tbAcresByGeoArea.Text
        SpectrumOutputFN = tbSpectrumOutputFile.Text
        If rbOWN.Checked Then ownsa = "OWN"
        If rbSA.Checked Then ownsa = "SA"
        NTic = CType(tbNTic.Text, Integer)

        'FIGURE OUT WHERE TO DUMP THE OUTPUTS
        Dim savefiledialog1 As New SaveFileDialog

        With savefiledialog1
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Save the outputs to:"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            .FileName = "SIMPPLLE_Trts"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        baseoutfn = DirName(savefiledialog1.FileName) & FName(savefiledialog1.FileName)
        TranslateSpectrumToSIMPPLLE(baseoutfn, CType(tbNTic.Text, Integer), CType(tbSIMPPLLECellSize.Text, Integer), CType(tbGISCellSize.Text, Single), cbbZones.Text)

        savefiledialog1 = Nothing

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        MsgBox("SIMPPLLE requires a round-number acre input, whereas the cell sizes may be larger or smaller than this. These definitions allow the user to reconcile rounding differences in SIMPPLLE input")
    End Sub

    Private Sub SpectrumToSIMPPLLE_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cbbZones.Items.Add("Westside Region One")
        cbbZones.Items.Add("Eastside Region One")
        cbbZones.Text = cbbZones.Items(0)
    End Sub
End Class