Public Class UpdateAttributesall
    Dim OrigAttsAllFile As String
    Dim EVUSIMDATAfile As String
    Dim SLINKsToUpdateFile As String
    Dim NewAttsAllfile As String

    Dim NewCovertype() As String 'by SLINK
    Dim NewSizeClass() As String 'by SLINK
    Dim NewDensity() As Integer 'by SLINK
    Dim UpdateSLINK() As Byte '0/1 by SLINK whether to update this SLINK or not...
    Dim PastProcess() As String
    Dim NSLINKS As Integer

    'crosswalk files to use as look-ups
    Dim TreatmentNameXW() As String
    Dim NTreatmentXW As Integer
    Dim ProcessNameXW() As String
    Dim NProcessXW As Integer
    Dim SpeciesNameXW() As String
    Dim NSpeciesXW As Integer
    Dim SizeClassNameXW() As String
    Dim NSizeClassXW As Integer
    Dim LifeFormName() As String
    Dim NLifeForm As Integer
    Dim DensityNameXW() As String
    Dim NDensityXW As Integer

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'IDENTIFY THE ORIGINAL ATTRIBUTESALL FILE TO UPDATE
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Attributesall file to update"
            '.Title = "Select a folder that has the "
            .Filter = "Attributesall files (.attributesall) |*.attributesall"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        tbAttributesallFile.Text = openfiledialog2.FileName
        openfiledialog2 = Nothing


    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'EVU SIM DATA FILE WITH NEW INFORMATION ABOUT SELECT SLINKS
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select a EVU_SIM_DATA file to read for updated information"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        tbRunOutputToUse.Text = openfiledialog2.FileName
        openfiledialog2 = Nothing

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'FILE WITH SLINK INFORMATION TO USE IN CREATING THE NEW ATTRIBUTESALL FILE
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select the file with SLINKs to update"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        tbSLINKfile.Text = openfiledialog2.FileName
        openfiledialog2 = Nothing

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        SLINKsToUpdateFile = tbSLINKfile.Text
        EVUSIMDATAfile = tbRunOutputToUse.Text
        OrigAttsAllFile = tbAttributesallFile.Text

        'FIGURE OUT WHERE TO DUMP THE OUTPUTS
        Dim savefiledialog1 As New SaveFileDialog

        With savefiledialog1
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Save the outputs to:"
            .Filter = "Attributesall files (.attributesall) |*.attributesall"
            .FileName = "NewAtts"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        NewAttsAllfile = savefiledialog1.FileName
        savefiledialog1 = Nothing

        'now run the stuff
        CountSLINKS(OrigAttsAllFile)
        'look-up tables for this run
        DefineFileNames2(DirName(EVUSIMDATAfile))
        LoadSimCrosswalkFiles()
        ReadEVUSIMDATAfile(EVUSIMDATAfile)
        ReadSLINKStoUpdate(SLINKsToUpdateFile)
        WriteNewAttributesall()

        MsgBox("New .attributesall file written")


    End Sub

    Private Sub CountSLINKS(ByVal infile As String)
        'opens the original .attributesall file to count how many SLINKS there are in total
        Dim fnum As Integer = FreeFile()
        Dim dummy As String
        FileOpen(fnum, infile, OpenMode.Input)
        LineInput(fnum)
        nslinks = 0
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Or dummy = "END" Then Exit Do
            NSLINKS = NSLINKS + 1
        Loop
        FileClose(fnum)

        ReDim NewCovertype(NSLINKS)
        ReDim NewSizeClass(NSLINKS)
        ReDim NewDensity(NSLINKS)
        ReDim UpdateSLINK(NSLINKS)
        ReDim PastProcess(NSLINKS)

    End Sub

    Private Sub ReadEVUSIMDATAfile(ByVal InFile As String)
        'reads the EVUSIMDATA file and gets the updated type/size/densities.
        Dim MaxIndex As Integer
        Dim fnum As Integer = FreeFile()
        Dim dummy As String
        Dim arr() As String
        Dim slink As Integer
        Dim timestep As Integer
        Dim ktimestep As Integer
        Dim NTimestepsLoaded As Integer
        Dim TimestepsLoaded() As Integer

        'load the simulation data file
        FileOpen(fnum, InFile, OpenMode.Input)
        ReDim TimestepsLoaded(NTimestep)
        NTimestepsLoaded = 0
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                slink = Trim(arr(3))
                'If slink = 172834 Then Stop
                timestep = Trim(arr(1))
                If timestep > 1 Then Exit Do

                NewCovertype(slink) = SpeciesNameXW(arr(6))
                NewSizeClass(slink) = SizeClassNameXW(arr(7))
                NewDensity(slink) = DensityNameXW(arr(9))

            End If
        Loop
        FileClose(fnum)

    End Sub

    Private Sub ReadSLINKStoUpdate(ByVal InFile As String)
        'reads the list of SLINKs to update in the new .attributesall file
        Dim dummy As String
        Dim atts() As String
        Dim currprocess As String
        Dim kslink As Integer

        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, InFile, OpenMode.Input)
        dummy = LineInput(fnum)
        atts = Split(dummy, ",")
        currprocess = Trim(atts(1))
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            atts = Split(dummy, ",")
            If atts.Length = 1 Then
                kslink = Trim(atts(0))
                UpdateSLINK(kslink) = 1
                PastProcess(kslink) = currprocess
            Else
                currprocess = Trim(atts(1))
            End If
        Loop
        FileClose(fnum)


    End Sub


    Private Sub DefineFileNames2(ByVal BaseFolder As String)
        Dim letter As String


        letter = BaseFolder.Substring(BaseFolder.Length - 1, 1)
        If letter <> "\" Then BaseFolder = BaseFolder & "\"

        ProcessXWFN = BaseFolder & "PROCESS.txt"
        SpecialAreaXWFN = BaseFolder & "SPECIALAREA.txt"
        SpeciesXWFN = BaseFolder & "SPECIES.txt"
        OwnershipXWFN = BaseFolder & "OWNERSHIP.txt"
        LifeFormXWFN = BaseFolder & "LIFEFORM.txt"
        DensityXWFN = BaseFolder & "DENSITY.txt"
        HabGroupXWFN = BaseFolder & "HABGROUP.txt"
        SizeClassXWFN = BaseFolder & "SIZECLASS.txt"
        SpectrumSpeciesLUTFN = BaseFolder & "SPECIES_LUT.txt" 'different species lut files are used for wildlife queries vs. SIMPPLLE to Spectrum translation
    End Sub

    Private Sub LoadSimCrosswalkFiles()
        'in the TEXTFILE folder - unique to each simulation

        Dim MaxIndex As Integer = 0
        Dim dummy As String
        Dim arr() As String
        Dim fnum As Integer

        'load PROCESS
        MaxIndex = 0
        ReDim ProcessNameXW(MaxIndex)
        FNUM = FreeFile()
        FileOpen(fnum, ProcessXWFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                If arr(0) > MaxIndex Then
                    MaxIndex = arr(0)
                    ReDim Preserve ProcessNameXW(MaxIndex)
                End If
                ProcessNameXW(arr(0)) = CType(Trim(arr(1)), String)
            End If
        Loop
        NProcessXW = MaxIndex
        FileClose(fnum)

        'load TREATMENT
        MaxIndex = 0
        ReDim TreatmentNameXW(MaxIndex)
        fnum = FreeFile()
        If System.IO.File.Exists(TreatmentXWFN) Then
            FileOpen(fnum, TreatmentXWFN, OpenMode.Input)
            dummy = LineInput(fnum)
            Do Until EOF(fnum)
                dummy = LineInput(fnum)
                If dummy.Length > 0 Then
                    arr = Split(dummy, ",")
                    If arr(0) > MaxIndex Then
                        MaxIndex = arr(0)
                        ReDim Preserve TreatmentNameXW(MaxIndex)
                    End If
                    TreatmentNameXW(arr(0)) = CType(Trim(arr(1)), String)
                End If
            Loop
        End If
        NTreatmentXW = MaxIndex
        FileClose(fnum)

        'load SpeciesLUTFN
        MaxIndex = 0
        ReDim SpeciesNameXW(MaxIndex)
        fnum = FreeFile()

        FileOpen(fnum, SpeciesXWFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                If arr(0) > MaxIndex Then
                    MaxIndex = arr(0)
                    ReDim Preserve SpeciesNameXW(MaxIndex)
                End If
                SpeciesNameXW(arr(0)) = CType(arr(1), String)
            End If
        Loop
        NSpeciesXW = MaxIndex
        FileClose(fnum)

        'load SizeClassXW
        MaxIndex = 0
        ReDim SizeClassNameXW(MaxIndex)
        fnum = FreeFile()

        FileOpen(fnum, SizeClassXWFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                If arr(0) > MaxIndex Then
                    MaxIndex = arr(0)
                    ReDim Preserve SizeClassNameXW(MaxIndex)
                End If
                SizeClassNameXW(arr(0)) = CType(arr(1), String)
            End If
        Loop
        NSizeClassXW = MaxIndex
        FileClose(fnum)

        'load LifeformLUTFN
        MaxIndex = 0
        ReDim LifeFormName(MaxIndex)
        fnum = FreeFile()

        FileOpen(fnum, LifeFormXWFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                If arr(0) > MaxIndex Then
                    MaxIndex = arr(0)
                    ReDim Preserve LifeFormName(MaxIndex)
                End If
                LifeFormName(arr(0)) = CType(arr(1), String)
            End If
        Loop
        NLifeForm = MaxIndex
        FileClose(fnum)


        'load DensityXWFN
        MaxIndex = 0
        ReDim DensityNameXW(MaxIndex)
        fnum = FreeFile()

        FileOpen(fnum, DensityXWFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                If arr(0) > MaxIndex Then
                    MaxIndex = arr(0)
                    ReDim Preserve DensityNameXW(MaxIndex)
                End If
                DensityNameXW(arr(0)) = CType(arr(1), String)
            End If
        Loop
        NDensityXW = MaxIndex
        FileClose(fnum)


    End Sub


    Private Sub WriteNewAttributesall()
        'open the old; read line-by-line and write updated one at the same time.
        Dim atts() As String
        Dim writedum As String
        Dim indum As String
        Dim kslink As Integer

        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, OrigAttsAllFile, OpenMode.Input)
        Dim fnum2 As Integer = FreeFile()
        FileOpen(fnum2, NewAttsAllfile, OpenMode.Output)

        'header line
        writedum = LineInput(fnum)
        PrintLine(fnum2, writedum)

        For jslink As Integer = 1 To NSLINKS
            indum = LineInput(fnum)
            atts = Split(indum, ",")
            kslink = Trim(atts(0))

            If UpdateSLINK(kslink) = 0 Then
                PrintLine(fnum2, indum)
            Else
                writedum = ""
                For jatt As Integer = 0 To 7
                    writedum = writedum & atts(jatt) & ","
                Next
                writedum = writedum & NewCovertype(kslink) & "," & NewSizeClass(kslink) & "," & NewDensity(kslink) & "," & PastProcess(kslink)
                For jatt As Integer = 12 To atts.Length - 1
                    writedum = writedum & "," & atts(jatt)
                Next
                PrintLine(fnum2, writedum)
            End If
        Next

        'last line
        writedum = LineInput(fnum)
        PrintLine(fnum2, writedum)



        FileClose(fnum)
        FileClose(fnum2)
    End Sub

End Class