<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BuildBatchFile
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button
        Me.btnDefineICFiles = New System.Windows.Forms.Button
        Me.lblRunsToProcessFile = New System.Windows.Forms.Label
        Me.lblICFile = New System.Windows.Forms.Label
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.btnICFilesClear = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.cbBuildUnzipSIMPPLLE = New System.Windows.Forms.CheckBox
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(91, 64)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 44)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Runs To Process"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnDefineICFiles
        '
        Me.btnDefineICFiles.Enabled = False
        Me.btnDefineICFiles.Location = New System.Drawing.Point(91, 192)
        Me.btnDefineICFiles.Name = "btnDefineICFiles"
        Me.btnDefineICFiles.Size = New System.Drawing.Size(75, 45)
        Me.btnDefineICFiles.TabIndex = 1
        Me.btnDefineICFiles.Text = "Define IC Files"
        Me.btnDefineICFiles.UseVisualStyleBackColor = True
        '
        'lblRunsToProcessFile
        '
        Me.lblRunsToProcessFile.AutoSize = True
        Me.lblRunsToProcessFile.Location = New System.Drawing.Point(95, 121)
        Me.lblRunsToProcessFile.Name = "lblRunsToProcessFile"
        Me.lblRunsToProcessFile.Size = New System.Drawing.Size(58, 13)
        Me.lblRunsToProcessFile.TabIndex = 2
        Me.lblRunsToProcessFile.Text = "<File Built>"
        '
        'lblICFile
        '
        Me.lblICFile.AutoSize = True
        Me.lblICFile.Location = New System.Drawing.Point(95, 252)
        Me.lblICFile.Name = "lblICFile"
        Me.lblICFile.Size = New System.Drawing.Size(58, 13)
        Me.lblICFile.TabIndex = 3
        Me.lblICFile.Text = "<File Built>"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(12, 115)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(62, 25)
        Me.Button3.TabIndex = 4
        Me.Button3.Text = "Save"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(12, 146)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(62, 25)
        Me.Button5.TabIndex = 6
        Me.Button5.Text = "Clear"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'btnICFilesClear
        '
        Me.btnICFilesClear.Enabled = False
        Me.btnICFilesClear.Location = New System.Drawing.Point(12, 252)
        Me.btnICFilesClear.Name = "btnICFilesClear"
        Me.btnICFilesClear.Size = New System.Drawing.Size(62, 25)
        Me.btnICFilesClear.TabIndex = 7
        Me.btnICFilesClear.Text = "Clear"
        Me.btnICFilesClear.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(12, 12)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(50, 36)
        Me.Button2.TabIndex = 8
        Me.Button2.Text = "Help ?"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'cbBuildUnzipSIMPPLLE
        '
        Me.cbBuildUnzipSIMPPLLE.AutoSize = True
        Me.cbBuildUnzipSIMPPLLE.Location = New System.Drawing.Point(91, 41)
        Me.cbBuildUnzipSIMPPLLE.Name = "cbBuildUnzipSIMPPLLE"
        Me.cbBuildUnzipSIMPPLLE.Size = New System.Drawing.Size(171, 17)
        Me.cbBuildUnzipSIMPPLLE.TabIndex = 9
        Me.cbBuildUnzipSIMPPLLE.Text = "Build .bat for unzipping .gz files"
        Me.cbBuildUnzipSIMPPLLE.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(279, 36)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(27, 25)
        Me.Button4.TabIndex = 10
        Me.Button4.Text = "?"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(335, 146)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(72, 42)
        Me.Button6.TabIndex = 11
        Me.Button6.Text = "Finished"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'BuildBatchFile
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(453, 317)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.cbBuildUnzipSIMPPLLE)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.btnICFilesClear)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.lblICFile)
        Me.Controls.Add(Me.lblRunsToProcessFile)
        Me.Controls.Add(Me.btnDefineICFiles)
        Me.Controls.Add(Me.Button1)
        Me.Name = "BuildBatchFile"
        Me.Text = "BuildBatchFile"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btnDefineICFiles As System.Windows.Forms.Button
    Friend WithEvents lblRunsToProcessFile As System.Windows.Forms.Label
    Friend WithEvents lblICFile As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents btnICFilesClear As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents cbBuildUnzipSIMPPLLE As System.Windows.Forms.CheckBox
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
End Class
