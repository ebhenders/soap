<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class QuerySIMPPLLEoutput
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tbBatchFile = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.Button2 = New System.Windows.Forms.Button
        Me.tbModelDefsDir = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.tbInitCondFiles = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.cbWritePolyAtts = New System.Windows.Forms.CheckBox
        Me.cbWriteGrids = New System.Windows.Forms.CheckBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.tbTimesteps = New System.Windows.Forms.TextBox
        Me.cbWriteFireFile = New System.Windows.Forms.CheckBox
        Me.cbSplitMaps = New System.Windows.Forms.CheckBox
        Me.tbAcreAdjust = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.cbWriteGISMaps = New System.Windows.Forms.CheckBox
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.cbWriteFireSpread = New System.Windows.Forms.CheckBox
        Me.Button7 = New System.Windows.Forms.Button
        Me.Button8 = New System.Windows.Forms.Button
        Me.tbUnzipSIMPPLLE = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.cbWriteProcessStats = New System.Windows.Forms.CheckBox
        Me.Button9 = New System.Windows.Forms.Button
        Me.Button10 = New System.Windows.Forms.Button
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Button11 = New System.Windows.Forms.Button
        Me.Button12 = New System.Windows.Forms.Button
        Me.Button13 = New System.Windows.Forms.Button
        Me.Button14 = New System.Windows.Forms.Button
        Me.Button15 = New System.Windows.Forms.Button
        Me.Button16 = New System.Windows.Forms.Button
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SaveModelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.LoadModelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.gbOptionalOutputs = New System.Windows.Forms.GroupBox
        Me.cbPatchMaps = New System.Windows.Forms.CheckBox
        Me.MenuStrip1.SuspendLayout()
        Me.gbOptionalOutputs.SuspendLayout()
        Me.SuspendLayout()
        '
        'tbBatchFile
        '
        Me.tbBatchFile.Location = New System.Drawing.Point(156, 230)
        Me.tbBatchFile.Name = "tbBatchFile"
        Me.tbBatchFile.Size = New System.Drawing.Size(596, 20)
        Me.tbBatchFile.TabIndex = 2
        Me.tbBatchFile.Text = "D:\R1\Revisions\KIPZ\KIPZ_SIMPPLLE\Oct2011_Runs\Scenario1\BULLSIMEX_S1-simulation" & _
            "\textdata"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(58, 230)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(98, 31)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Batch File for Runs To Process"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(771, 231)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 19)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Browse"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(253, 37)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(462, 40)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "SIMPPLLE Output Queries"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(771, 151)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 19)
        Me.Button2.TabIndex = 8
        Me.Button2.Text = "Browse"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'tbModelDefsDir
        '
        Me.tbModelDefsDir.Location = New System.Drawing.Point(156, 151)
        Me.tbModelDefsDir.Name = "tbModelDefsDir"
        Me.tbModelDefsDir.Size = New System.Drawing.Size(596, 20)
        Me.tbModelDefsDir.TabIndex = 6
        Me.tbModelDefsDir.Text = "D:\R1\Revisions\KIPZ\KIPZ_SIMPPLLE\Oct2011_Runs\Scenario1\BULLSIMEX_S1-simulation" & _
            "\textdata"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(58, 155)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(88, 19)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Query Directory:"
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(254, 107)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(71, 54)
        Me.Button3.TabIndex = 9
        Me.Button3.Text = "RUN QUERY"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(771, 191)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 19)
        Me.Button4.TabIndex = 12
        Me.Button4.Text = "Browse"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'tbInitCondFiles
        '
        Me.tbInitCondFiles.Location = New System.Drawing.Point(156, 190)
        Me.tbInitCondFiles.Name = "tbInitCondFiles"
        Me.tbInitCondFiles.Size = New System.Drawing.Size(596, 20)
        Me.tbInitCondFiles.TabIndex = 10
        Me.tbInitCondFiles.Text = "D:\R1\Revisions\KIPZ\KIPZ_SIMPPLLE\Oct2011_Runs\Scenario1\BULLSIMEX_S1-simulation" & _
            "\textdata"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(58, 187)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 32)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Initial Conditions file"
        '
        'cbWritePolyAtts
        '
        Me.cbWritePolyAtts.AutoSize = True
        Me.cbWritePolyAtts.Location = New System.Drawing.Point(9, 23)
        Me.cbWritePolyAtts.Name = "cbWritePolyAtts"
        Me.cbWritePolyAtts.Size = New System.Drawing.Size(213, 17)
        Me.cbWritePolyAtts.TabIndex = 13
        Me.cbWritePolyAtts.Text = "Write Query Polygon Maps for MMaPPit"
        Me.cbWritePolyAtts.UseVisualStyleBackColor = True
        '
        'cbWriteGrids
        '
        Me.cbWriteGrids.AutoSize = True
        Me.cbWriteGrids.Location = New System.Drawing.Point(9, 131)
        Me.cbWriteGrids.Name = "cbWriteGrids"
        Me.cbWriteGrids.Size = New System.Drawing.Size(139, 17)
        Me.cbWriteGrids.TabIndex = 14
        Me.cbWriteGrids.Text = "Write Grids for Fragstats"
        Me.cbWriteGrids.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(55, 311)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(101, 34)
        Me.Label5.TabIndex = 15
        Me.Label5.Text = "Timesteps to Query:"
        '
        'tbTimesteps
        '
        Me.tbTimesteps.Location = New System.Drawing.Point(156, 311)
        Me.tbTimesteps.Name = "tbTimesteps"
        Me.tbTimesteps.Size = New System.Drawing.Size(596, 20)
        Me.tbTimesteps.TabIndex = 16
        Me.tbTimesteps.Text = "1,2,3-5"
        '
        'cbWriteFireFile
        '
        Me.cbWriteFireFile.AutoSize = True
        Me.cbWriteFireFile.Location = New System.Drawing.Point(24, 41)
        Me.cbWriteFireFile.Name = "cbWriteFireFile"
        Me.cbWriteFireFile.Size = New System.Drawing.Size(182, 17)
        Me.cbWriteFireFile.TabIndex = 17
        Me.cbWriteFireFile.Text = "Write Process Maps for MMaPPit"
        Me.cbWriteFireFile.UseVisualStyleBackColor = True
        '
        'cbSplitMaps
        '
        Me.cbSplitMaps.AutoSize = True
        Me.cbSplitMaps.Location = New System.Drawing.Point(228, 23)
        Me.cbSplitMaps.Name = "cbSplitMaps"
        Me.cbSplitMaps.Size = New System.Drawing.Size(213, 17)
        Me.cbSplitMaps.TabIndex = 18
        Me.cbSplitMaps.Text = "Split Out Special Area/Ownership Maps"
        Me.cbSplitMaps.UseVisualStyleBackColor = True
        '
        'tbAcreAdjust
        '
        Me.tbAcreAdjust.Location = New System.Drawing.Point(622, 337)
        Me.tbAcreAdjust.Name = "tbAcreAdjust"
        Me.tbAcreAdjust.Size = New System.Drawing.Size(154, 20)
        Me.tbAcreAdjust.TabIndex = 19
        Me.tbAcreAdjust.Text = "1"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(619, 361)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(143, 39)
        Me.Label6.TabIndex = 20
        Me.Label6.Text = "Factor to adjust acre values " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(optional, no adjustment = 1) " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(6 acre: 150m = .9" & _
            "266452)"
        '
        'cbWriteGISMaps
        '
        Me.cbWriteGISMaps.AutoSize = True
        Me.cbWriteGISMaps.Location = New System.Drawing.Point(9, 108)
        Me.cbWriteGISMaps.Name = "cbWriteGISMaps"
        Me.cbWriteGISMaps.Size = New System.Drawing.Size(193, 17)
        Me.cbWriteGISMaps.TabIndex = 21
        Me.cbWriteGISMaps.Text = "Write Polygon Atts for GIS Mapping"
        Me.cbWriteGISMaps.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(513, 70)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(74, 59)
        Me.Button5.TabIndex = 22
        Me.Button5.Text = "RUNS TO QUERY"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(295, 70)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(74, 59)
        Me.Button6.TabIndex = 23
        Me.Button6.Text = "SET UP QUERY"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'cbWriteFireSpread
        '
        Me.cbWriteFireSpread.AutoSize = True
        Me.cbWriteFireSpread.Location = New System.Drawing.Point(24, 63)
        Me.cbWriteFireSpread.Name = "cbWriteFireSpread"
        Me.cbWriteFireSpread.Size = New System.Drawing.Size(198, 17)
        Me.cbWriteFireSpread.TabIndex = 24
        Me.cbWriteFireSpread.Text = "Write Fire Spread Maps for MMaPPit"
        Me.cbWriteFireSpread.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(786, 338)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(25, 20)
        Me.Button7.TabIndex = 25
        Me.Button7.Text = "?"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(771, 272)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(75, 19)
        Me.Button8.TabIndex = 28
        Me.Button8.Text = "Browse"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'tbUnzipSIMPPLLE
        '
        Me.tbUnzipSIMPPLLE.Location = New System.Drawing.Point(156, 271)
        Me.tbUnzipSIMPPLLE.Name = "tbUnzipSIMPPLLE"
        Me.tbUnzipSIMPPLLE.Size = New System.Drawing.Size(596, 20)
        Me.tbUnzipSIMPPLLE.TabIndex = 26
        Me.tbUnzipSIMPPLLE.Text = "<optional>"
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(56, 268)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(98, 20)
        Me.Label7.TabIndex = 27
        Me.Label7.Text = "Unzipping .bat file:"
        '
        'cbWriteProcessStats
        '
        Me.cbWriteProcessStats.AutoSize = True
        Me.cbWriteProcessStats.Location = New System.Drawing.Point(9, 86)
        Me.cbWriteProcessStats.Name = "cbWriteProcessStats"
        Me.cbWriteProcessStats.Size = New System.Drawing.Size(174, 17)
        Me.cbWriteProcessStats.TabIndex = 29
        Me.cbWriteProcessStats.Text = "Write Process Stats to Text File"
        Me.cbWriteProcessStats.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(344, 109)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(25, 20)
        Me.Button9.TabIndex = 31
        Me.Button9.Text = "?"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(563, 109)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(25, 20)
        Me.Button10.TabIndex = 32
        Me.Button10.Text = "?"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(175, 77)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(120, 42)
        Me.Label8.TabIndex = 33
        Me.Label8.Text = "Start here if you have not yet defined your queries:"
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(387, 77)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(120, 42)
        Me.Label9.TabIndex = 34
        Me.Label9.Text = "Use this dialog to point to the SIMPPLLE runs you want to query:"
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(27, 154)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(25, 20)
        Me.Button11.TabIndex = 35
        Me.Button11.Text = "?"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button12
        '
        Me.Button12.Location = New System.Drawing.Point(27, 189)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(25, 20)
        Me.Button12.TabIndex = 36
        Me.Button12.Text = "?"
        Me.Button12.UseVisualStyleBackColor = True
        '
        'Button13
        '
        Me.Button13.Location = New System.Drawing.Point(27, 229)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(25, 20)
        Me.Button13.TabIndex = 37
        Me.Button13.Text = "?"
        Me.Button13.UseVisualStyleBackColor = True
        '
        'Button14
        '
        Me.Button14.Location = New System.Drawing.Point(27, 268)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(25, 20)
        Me.Button14.TabIndex = 38
        Me.Button14.Text = "?"
        Me.Button14.UseVisualStyleBackColor = True
        '
        'Button15
        '
        Me.Button15.Location = New System.Drawing.Point(27, 310)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(25, 20)
        Me.Button15.TabIndex = 39
        Me.Button15.Text = "?"
        Me.Button15.UseVisualStyleBackColor = True
        '
        'Button16
        '
        Me.Button16.Location = New System.Drawing.Point(82, 376)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(64, 52)
        Me.Button16.TabIndex = 40
        Me.Button16.Text = "Help with Optional Outputs"
        Me.Button16.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(918, 24)
        Me.MenuStrip1.TabIndex = 41
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SaveModelToolStripMenuItem, Me.LoadModelToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'SaveModelToolStripMenuItem
        '
        Me.SaveModelToolStripMenuItem.Name = "SaveModelToolStripMenuItem"
        Me.SaveModelToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.SaveModelToolStripMenuItem.Text = "Save Model"
        '
        'LoadModelToolStripMenuItem
        '
        Me.LoadModelToolStripMenuItem.Name = "LoadModelToolStripMenuItem"
        Me.LoadModelToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.LoadModelToolStripMenuItem.Text = "Load Model"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'gbOptionalOutputs
        '
        Me.gbOptionalOutputs.Controls.Add(Me.cbPatchMaps)
        Me.gbOptionalOutputs.Controls.Add(Me.cbWriteProcessStats)
        Me.gbOptionalOutputs.Controls.Add(Me.cbWriteFireSpread)
        Me.gbOptionalOutputs.Controls.Add(Me.cbWriteGISMaps)
        Me.gbOptionalOutputs.Controls.Add(Me.cbSplitMaps)
        Me.gbOptionalOutputs.Controls.Add(Me.cbWriteFireFile)
        Me.gbOptionalOutputs.Controls.Add(Me.cbWriteGrids)
        Me.gbOptionalOutputs.Controls.Add(Me.cbWritePolyAtts)
        Me.gbOptionalOutputs.Controls.Add(Me.Button3)
        Me.gbOptionalOutputs.Location = New System.Drawing.Point(154, 337)
        Me.gbOptionalOutputs.Name = "gbOptionalOutputs"
        Me.gbOptionalOutputs.Size = New System.Drawing.Size(453, 177)
        Me.gbOptionalOutputs.TabIndex = 42
        Me.gbOptionalOutputs.TabStop = False
        Me.gbOptionalOutputs.Text = "Optional Outputs"
        '
        'cbPatchMaps
        '
        Me.cbPatchMaps.AutoSize = True
        Me.cbPatchMaps.Location = New System.Drawing.Point(228, 43)
        Me.cbPatchMaps.Name = "cbPatchMaps"
        Me.cbPatchMaps.Size = New System.Drawing.Size(187, 17)
        Me.cbPatchMaps.TabIndex = 30
        Me.cbPatchMaps.Text = "Write Patch Maps (if spatial query)"
        Me.cbPatchMaps.UseVisualStyleBackColor = True
        '
        'QuerySIMPPLLEoutput
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(918, 536)
        Me.Controls.Add(Me.gbOptionalOutputs)
        Me.Controls.Add(Me.Button16)
        Me.Controls.Add(Me.Button15)
        Me.Controls.Add(Me.Button14)
        Me.Controls.Add(Me.Button13)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.Button11)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.tbUnzipSIMPPLLE)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.tbAcreAdjust)
        Me.Controls.Add(Me.tbTimesteps)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.tbInitCondFiles)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.tbModelDefsDir)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.tbBatchFile)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "QuerySIMPPLLEoutput"
        Me.Text = "Query SIMPPLLE Outputs"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.gbOptionalOutputs.ResumeLayout(False)
        Me.gbOptionalOutputs.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tbBatchFile As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents tbModelDefsDir As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents tbInitCondFiles As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cbWritePolyAtts As System.Windows.Forms.CheckBox
    Friend WithEvents cbWriteGrids As System.Windows.Forms.CheckBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tbTimesteps As System.Windows.Forms.TextBox
    Friend WithEvents cbWriteFireFile As System.Windows.Forms.CheckBox
    Friend WithEvents cbSplitMaps As System.Windows.Forms.CheckBox
    Friend WithEvents tbAcreAdjust As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cbWriteGISMaps As System.Windows.Forms.CheckBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents cbWriteFireSpread As System.Windows.Forms.CheckBox
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents tbUnzipSIMPPLLE As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cbWriteProcessStats As System.Windows.Forms.CheckBox
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents gbOptionalOutputs As System.Windows.Forms.GroupBox
    Friend WithEvents cbPatchMaps As System.Windows.Forms.CheckBox
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaveModelToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoadModelToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
