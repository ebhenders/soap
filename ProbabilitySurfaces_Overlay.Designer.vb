<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ProbabilitySurfaces_Overlay
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.rbTimePers = New System.Windows.Forms.RadioButton
        Me.RadioButton1 = New System.Windows.Forms.RadioButton
        Me.tbTimePers = New System.Windows.Forms.TextBox
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.tbIncreaseFactor = New System.Windows.Forms.TextBox
        Me.tbDecreaseFactor = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.tbThreshhold = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Button5 = New System.Windows.Forms.Button
        Me.tbCellSize = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Button6 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(159, 2)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(113, 33)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "What does this do?"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(85, 259)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(84, 53)
        Me.Button2.TabIndex = 8
        Me.Button2.Text = "GO"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'rbTimePers
        '
        Me.rbTimePers.AutoSize = True
        Me.rbTimePers.Enabled = False
        Me.rbTimePers.Location = New System.Drawing.Point(20, 233)
        Me.rbTimePers.Name = "rbTimePers"
        Me.rbTimePers.Size = New System.Drawing.Size(111, 17)
        Me.rbTimePers.TabIndex = 15
        Me.rbTimePers.Text = "Use Time Periods:"
        Me.rbTimePers.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Checked = True
        Me.RadioButton1.Location = New System.Drawing.Point(20, 210)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(149, 17)
        Me.RadioButton1.TabIndex = 16
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Use All Time Periods in file"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'tbTimePers
        '
        Me.tbTimePers.Enabled = False
        Me.tbTimePers.Location = New System.Drawing.Point(127, 233)
        Me.tbTimePers.Name = "tbTimePers"
        Me.tbTimePers.Size = New System.Drawing.Size(145, 20)
        Me.tbTimePers.TabIndex = 17
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(20, 79)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(31, 32)
        Me.Button4.TabIndex = 13
        Me.Button4.Text = "?"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(20, 39)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(31, 32)
        Me.Button3.TabIndex = 12
        Me.Button3.Text = "?"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(63, 51)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(81, 13)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Increase Factor"
        '
        'tbIncreaseFactor
        '
        Me.tbIncreaseFactor.Location = New System.Drawing.Point(159, 46)
        Me.tbIncreaseFactor.Name = "tbIncreaseFactor"
        Me.tbIncreaseFactor.Size = New System.Drawing.Size(66, 20)
        Me.tbIncreaseFactor.TabIndex = 19
        Me.tbIncreaseFactor.Text = "2"
        '
        'tbDecreaseFactor
        '
        Me.tbDecreaseFactor.Location = New System.Drawing.Point(159, 79)
        Me.tbDecreaseFactor.Name = "tbDecreaseFactor"
        Me.tbDecreaseFactor.Size = New System.Drawing.Size(66, 20)
        Me.tbDecreaseFactor.TabIndex = 21
        Me.tbDecreaseFactor.Text = ".5"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(63, 84)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 13)
        Me.Label2.TabIndex = 20
        Me.Label2.Text = "Decrease Factor"
        '
        'tbThreshhold
        '
        Me.tbThreshhold.Location = New System.Drawing.Point(159, 117)
        Me.tbThreshhold.Name = "tbThreshhold"
        Me.tbThreshhold.Size = New System.Drawing.Size(66, 20)
        Me.tbThreshhold.TabIndex = 24
        Me.tbThreshhold.Text = ".5"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(63, 122)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(97, 13)
        Me.Label3.TabIndex = 23
        Me.Label3.Text = "Habitat Threshhold"
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(20, 117)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(31, 32)
        Me.Button5.TabIndex = 22
        Me.Button5.Text = "?"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'tbCellSize
        '
        Me.tbCellSize.Location = New System.Drawing.Point(159, 162)
        Me.tbCellSize.Name = "tbCellSize"
        Me.tbCellSize.Size = New System.Drawing.Size(66, 20)
        Me.tbCellSize.TabIndex = 26
        Me.tbCellSize.Text = "5.55987"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(84, 165)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 13)
        Me.Label4.TabIndex = 25
        Me.Label4.Text = "Cell Size"
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(20, 155)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(31, 32)
        Me.Button6.TabIndex = 27
        Me.Button6.Text = "?"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'ProbabilitySurfaces_Overlay
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(312, 335)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.tbCellSize)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.tbThreshhold)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.tbDecreaseFactor)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.tbIncreaseFactor)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tbTimePers)
        Me.Controls.Add(Me.RadioButton1)
        Me.Controls.Add(Me.rbTimePers)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Name = "ProbabilitySurfaces_Overlay"
        Me.Text = "ProbabilitySurfaces_Overlay"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents rbTimePers As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents tbTimePers As System.Windows.Forms.TextBox
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tbIncreaseFactor As System.Windows.Forms.TextBox
    Friend WithEvents tbDecreaseFactor As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tbThreshhold As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents tbCellSize As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Button6 As System.Windows.Forms.Button
End Class
