<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPatchStats
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.tbBatchFile = New System.Windows.Forms.TextBox
        Me.tbOutputFile = New System.Windows.Forms.TextBox
        Me.tbFilterValue = New System.Windows.Forms.TextBox
        Me.tbFilterValueCol = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(20, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Batch File"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(20, 57)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Output File"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(20, 109)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(159, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Column for Filter Value (0-based)"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(20, 87)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(154, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Filter Value (optional; < omitted)"
        '
        'tbBatchFile
        '
        Me.tbBatchFile.Location = New System.Drawing.Point(155, 32)
        Me.tbBatchFile.Name = "tbBatchFile"
        Me.tbBatchFile.Size = New System.Drawing.Size(636, 20)
        Me.tbBatchFile.TabIndex = 4
        '
        'tbOutputFile
        '
        Me.tbOutputFile.Location = New System.Drawing.Point(155, 58)
        Me.tbOutputFile.Name = "tbOutputFile"
        Me.tbOutputFile.Size = New System.Drawing.Size(636, 20)
        Me.tbOutputFile.TabIndex = 5
        '
        'tbFilterValue
        '
        Me.tbFilterValue.Location = New System.Drawing.Point(180, 84)
        Me.tbFilterValue.Name = "tbFilterValue"
        Me.tbFilterValue.Size = New System.Drawing.Size(57, 20)
        Me.tbFilterValue.TabIndex = 6
        Me.tbFilterValue.Text = "-1"
        '
        'tbFilterValueCol
        '
        Me.tbFilterValueCol.Location = New System.Drawing.Point(180, 110)
        Me.tbFilterValueCol.Name = "tbFilterValueCol"
        Me.tbFilterValueCol.Size = New System.Drawing.Size(57, 20)
        Me.tbFilterValueCol.TabIndex = 7
        Me.tbFilterValueCol.Text = "0"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(301, 138)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 49)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "GO"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'frmPatchStats
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(803, 214)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.tbFilterValueCol)
        Me.Controls.Add(Me.tbFilterValue)
        Me.Controls.Add(Me.tbOutputFile)
        Me.Controls.Add(Me.tbBatchFile)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmPatchStats"
        Me.Text = "PatchStats"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tbBatchFile As System.Windows.Forms.TextBox
    Friend WithEvents tbOutputFile As System.Windows.Forms.TextBox
    Friend WithEvents tbFilterValue As System.Windows.Forms.TextBox
    Friend WithEvents tbFilterValueCol As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
