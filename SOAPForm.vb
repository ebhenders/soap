Public Class SOAPForm
    Inherits System.Windows.Forms.Form


    Dim BaseFolder As String 'the folder from the masterinputfile text box
    Dim PrintCombos() As String
    Dim NCombos As Long
    Dim ComboCount As Long
    Dim NFieldVals() As Integer

    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Button17 As System.Windows.Forms.Button
    Friend WithEvents Button18 As System.Windows.Forms.Button
    Friend WithEvents Button19 As System.Windows.Forms.Button
    Friend WithEvents Button20 As System.Windows.Forms.Button
    Friend WithEvents Button22 As System.Windows.Forms.Button
    Friend WithEvents Button21 As System.Windows.Forms.Button
    Friend WithEvents Button23 As System.Windows.Forms.Button
    Friend WithEvents Button25 As System.Windows.Forms.Button
    Friend WithEvents Button24 As System.Windows.Forms.Button
    Friend WithEvents Button27 As System.Windows.Forms.Button
    Friend WithEvents Button26 As System.Windows.Forms.Button
    Friend WithEvents Button29 As System.Windows.Forms.Button
    Friend WithEvents Button28 As System.Windows.Forms.Button
    Friend WithEvents Button30 As System.Windows.Forms.Button
    Friend WithEvents Button31 As System.Windows.Forms.Button
    Friend WithEvents Button32 As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Button33 As System.Windows.Forms.Button
    Friend WithEvents Button34 As System.Windows.Forms.Button
    Friend WithEvents Button35 As System.Windows.Forms.Button
    Friend WithEvents Button36 As System.Windows.Forms.Button
    Friend WithEvents Button37 As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    'the substring array to give back
    Dim SubStringCount As Integer 'the number of substrings counted



#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label2 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.Label2 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.Button7 = New System.Windows.Forms.Button
        Me.Button8 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button9 = New System.Windows.Forms.Button
        Me.Button10 = New System.Windows.Forms.Button
        Me.Button11 = New System.Windows.Forms.Button
        Me.Button12 = New System.Windows.Forms.Button
        Me.Button13 = New System.Windows.Forms.Button
        Me.Button14 = New System.Windows.Forms.Button
        Me.Button15 = New System.Windows.Forms.Button
        Me.Button16 = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Button33 = New System.Windows.Forms.Button
        Me.Button32 = New System.Windows.Forms.Button
        Me.Button31 = New System.Windows.Forms.Button
        Me.Button29 = New System.Windows.Forms.Button
        Me.Button28 = New System.Windows.Forms.Button
        Me.Button27 = New System.Windows.Forms.Button
        Me.Button26 = New System.Windows.Forms.Button
        Me.Button23 = New System.Windows.Forms.Button
        Me.Button18 = New System.Windows.Forms.Button
        Me.Button17 = New System.Windows.Forms.Button
        Me.Button25 = New System.Windows.Forms.Button
        Me.Button24 = New System.Windows.Forms.Button
        Me.Button22 = New System.Windows.Forms.Button
        Me.Button21 = New System.Windows.Forms.Button
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.Button35 = New System.Windows.Forms.Button
        Me.Button34 = New System.Windows.Forms.Button
        Me.Button19 = New System.Windows.Forms.Button
        Me.Button20 = New System.Windows.Forms.Button
        Me.Button30 = New System.Windows.Forms.Button
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.Button36 = New System.Windows.Forms.Button
        Me.Button37 = New System.Windows.Forms.Button
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(109, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(107, 40)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "SOAP"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(14, 82)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(93, 37)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "CombineText Files"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(14, 19)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(93, 48)
        Me.Button2.TabIndex = 9
        Me.Button2.Text = "Query SIMPPLLE Outputs"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(206, 158)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(62, 36)
        Me.Button3.TabIndex = 10
        Me.Button3.Text = "Attribute Combos"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(114, 20)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(86, 45)
        Me.Button6.TabIndex = 19
        Me.Button6.Text = "Spectrum Rx To SIMPPLLE"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(200, 18)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(325, 31)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "SIMPPLLE Output and Analytic Processor"
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(6, 69)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(82, 37)
        Me.Button7.TabIndex = 21
        Me.Button7.Text = "Treatment Feasibility"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.Location = New System.Drawing.Point(6, 20)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(90, 49)
        Me.Button8.TabIndex = 22
        Me.Button8.Text = "VMAP To SIMPPLLE"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(6, 136)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(111, 56)
        Me.Button4.TabIndex = 23
        Me.Button4.Text = "SIMPPLLE Disturbs To Spectrum"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(114, 171)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(90, 48)
        Me.Button5.TabIndex = 24
        Me.Button5.Text = "Locked Disturbances"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(114, 71)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(90, 48)
        Me.Button9.TabIndex = 25
        Me.Button9.Text = "Update .attributesall"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(120, 10)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(77, 37)
        Me.Button10.TabIndex = 26
        Me.Button10.Text = "Data Analysis"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(130, 19)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(111, 48)
        Me.Button11.TabIndex = 27
        Me.Button11.Text = "Output Probability Surfaces"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button12
        '
        Me.Button12.Location = New System.Drawing.Point(120, 53)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(56, 41)
        Me.Button12.TabIndex = 28
        Me.Button12.Text = "Cross Table"
        Me.Button12.UseVisualStyleBackColor = True
        '
        'Button13
        '
        Me.Button13.Location = New System.Drawing.Point(164, 74)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(24, 25)
        Me.Button13.TabIndex = 29
        Me.Button13.Text = "?"
        Me.Button13.UseVisualStyleBackColor = True
        '
        'Button14
        '
        Me.Button14.Location = New System.Drawing.Point(130, 77)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(111, 48)
        Me.Button14.TabIndex = 30
        Me.Button14.Text = "Output Prob Surface Overlay"
        Me.Button14.UseVisualStyleBackColor = True
        '
        'Button15
        '
        Me.Button15.Location = New System.Drawing.Point(130, 131)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(111, 56)
        Me.Button15.TabIndex = 31
        Me.Button15.Text = "Convert FireSpread to MMaPPit"
        Me.Button15.UseVisualStyleBackColor = True
        '
        'Button16
        '
        Me.Button16.Location = New System.Drawing.Point(217, 162)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(24, 25)
        Me.Button16.TabIndex = 32
        Me.Button16.Text = "?"
        Me.Button16.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button33)
        Me.GroupBox1.Controls.Add(Me.Button32)
        Me.GroupBox1.Controls.Add(Me.Button31)
        Me.GroupBox1.Controls.Add(Me.Button29)
        Me.GroupBox1.Controls.Add(Me.Button28)
        Me.GroupBox1.Controls.Add(Me.Button27)
        Me.GroupBox1.Controls.Add(Me.Button26)
        Me.GroupBox1.Controls.Add(Me.Button23)
        Me.GroupBox1.Controls.Add(Me.Button18)
        Me.GroupBox1.Controls.Add(Me.Button17)
        Me.GroupBox1.Controls.Add(Me.Button6)
        Me.GroupBox1.Controls.Add(Me.Button8)
        Me.GroupBox1.Controls.Add(Me.Button5)
        Me.GroupBox1.Controls.Add(Me.Button9)
        Me.GroupBox1.Location = New System.Drawing.Point(7, 74)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(242, 302)
        Me.GroupBox1.TabIndex = 34
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Pre Processors"
        '
        'Button33
        '
        Me.Button33.Location = New System.Drawing.Point(102, 140)
        Me.Button33.Name = "Button33"
        Me.Button33.Size = New System.Drawing.Size(24, 25)
        Me.Button33.TabIndex = 45
        Me.Button33.Text = "?"
        Me.Button33.UseVisualStyleBackColor = True
        '
        'Button32
        '
        Me.Button32.Font = New System.Drawing.Font("Agency FB", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button32.Location = New System.Drawing.Point(63, 127)
        Me.Button32.Name = "Button32"
        Me.Button32.Size = New System.Drawing.Size(52, 30)
        Me.Button32.TabIndex = 44
        Me.Button32.Text = "Scrubber"
        Me.Button32.UseVisualStyleBackColor = True
        '
        'Button31
        '
        Me.Button31.Location = New System.Drawing.Point(39, 140)
        Me.Button31.Name = "Button31"
        Me.Button31.Size = New System.Drawing.Size(24, 25)
        Me.Button31.TabIndex = 43
        Me.Button31.Text = "?"
        Me.Button31.UseVisualStyleBackColor = True
        '
        'Button29
        '
        Me.Button29.Location = New System.Drawing.Point(83, 203)
        Me.Button29.Name = "Button29"
        Me.Button29.Size = New System.Drawing.Size(24, 25)
        Me.Button29.TabIndex = 42
        Me.Button29.Text = "?"
        Me.Button29.UseVisualStyleBackColor = True
        '
        'Button28
        '
        Me.Button28.Location = New System.Drawing.Point(6, 171)
        Me.Button28.Name = "Button28"
        Me.Button28.Size = New System.Drawing.Size(90, 48)
        Me.Button28.TabIndex = 41
        Me.Button28.Text = "Clean Up Input Data"
        Me.Button28.UseVisualStyleBackColor = True
        '
        'Button27
        '
        Me.Button27.Location = New System.Drawing.Point(83, 257)
        Me.Button27.Name = "Button27"
        Me.Button27.Size = New System.Drawing.Size(24, 25)
        Me.Button27.TabIndex = 40
        Me.Button27.Text = "?"
        Me.Button27.UseVisualStyleBackColor = True
        '
        'Button26
        '
        Me.Button26.Location = New System.Drawing.Point(6, 234)
        Me.Button26.Name = "Button26"
        Me.Button26.Size = New System.Drawing.Size(90, 48)
        Me.Button26.TabIndex = 39
        Me.Button26.Text = "Redraw Map"
        Me.Button26.UseVisualStyleBackColor = True
        '
        'Button23
        '
        Me.Button23.Font = New System.Drawing.Font("Agency FB", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button23.Location = New System.Drawing.Point(6, 127)
        Me.Button23.Name = "Button23"
        Me.Button23.Size = New System.Drawing.Size(46, 30)
        Me.Button23.TabIndex = 36
        Me.Button23.Text = "Clean"
        Me.Button23.UseVisualStyleBackColor = True
        '
        'Button18
        '
        Me.Button18.Location = New System.Drawing.Point(81, 97)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(24, 25)
        Me.Button18.TabIndex = 33
        Me.Button18.Text = "?"
        Me.Button18.UseVisualStyleBackColor = True
        '
        'Button17
        '
        Me.Button17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button17.Location = New System.Drawing.Point(6, 73)
        Me.Button17.Name = "Button17"
        Me.Button17.Size = New System.Drawing.Size(90, 48)
        Me.Button17.TabIndex = 26
        Me.Button17.Text = "Make SIMPPLLE Input Files"
        Me.Button17.UseVisualStyleBackColor = True
        '
        'Button25
        '
        Me.Button25.Location = New System.Drawing.Point(82, 38)
        Me.Button25.Name = "Button25"
        Me.Button25.Size = New System.Drawing.Size(24, 25)
        Me.Button25.TabIndex = 38
        Me.Button25.Text = "?"
        Me.Button25.UseVisualStyleBackColor = True
        '
        'Button24
        '
        Me.Button24.Location = New System.Drawing.Point(6, 15)
        Me.Button24.Name = "Button24"
        Me.Button24.Size = New System.Drawing.Size(90, 48)
        Me.Button24.TabIndex = 37
        Me.Button24.Text = "Stand Size Class Distribution"
        Me.Button24.UseVisualStyleBackColor = True
        '
        'Button22
        '
        Me.Button22.Location = New System.Drawing.Point(64, 137)
        Me.Button22.Name = "Button22"
        Me.Button22.Size = New System.Drawing.Size(24, 25)
        Me.Button22.TabIndex = 35
        Me.Button22.Text = "?"
        Me.Button22.UseVisualStyleBackColor = True
        '
        'Button21
        '
        Me.Button21.Location = New System.Drawing.Point(6, 112)
        Me.Button21.Name = "Button21"
        Me.Button21.Size = New System.Drawing.Size(82, 43)
        Me.Button21.TabIndex = 34
        Me.Button21.Text = "VMAP with FIA"
        Me.Button21.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Button37)
        Me.GroupBox2.Controls.Add(Me.Button35)
        Me.GroupBox2.Controls.Add(Me.Button34)
        Me.GroupBox2.Controls.Add(Me.Button2)
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.Controls.Add(Me.Button16)
        Me.GroupBox2.Controls.Add(Me.Button11)
        Me.GroupBox2.Controls.Add(Me.Button15)
        Me.GroupBox2.Controls.Add(Me.Button14)
        Me.GroupBox2.Controls.Add(Me.Button4)
        Me.GroupBox2.Location = New System.Drawing.Point(259, 74)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(285, 257)
        Me.GroupBox2.TabIndex = 35
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Post Processors"
        '
        'Button35
        '
        Me.Button35.Location = New System.Drawing.Point(93, 99)
        Me.Button35.Name = "Button35"
        Me.Button35.Size = New System.Drawing.Size(24, 25)
        Me.Button35.TabIndex = 47
        Me.Button35.Text = "?"
        Me.Button35.UseVisualStyleBackColor = True
        '
        'Button34
        '
        Me.Button34.Location = New System.Drawing.Point(93, 46)
        Me.Button34.Name = "Button34"
        Me.Button34.Size = New System.Drawing.Size(24, 25)
        Me.Button34.TabIndex = 46
        Me.Button34.Text = "?"
        Me.Button34.UseVisualStyleBackColor = True
        '
        'Button19
        '
        Me.Button19.Location = New System.Drawing.Point(206, 60)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(61, 45)
        Me.Button19.TabIndex = 36
        Me.Button19.Text = "Test Fire Spread"
        Me.Button19.UseVisualStyleBackColor = True
        '
        'Button20
        '
        Me.Button20.Enabled = False
        Me.Button20.Location = New System.Drawing.Point(203, 10)
        Me.Button20.Name = "Button20"
        Me.Button20.Size = New System.Drawing.Size(64, 48)
        Me.Button20.TabIndex = 37
        Me.Button20.Text = "Generate Fire Spread IF"
        Me.Button20.UseVisualStyleBackColor = True
        '
        'Button30
        '
        Me.Button30.Location = New System.Drawing.Point(206, 106)
        Me.Button30.Name = "Button30"
        Me.Button30.Size = New System.Drawing.Size(61, 50)
        Me.Button30.TabIndex = 38
        Me.Button30.Text = "Test Texture Filter"
        Me.Button30.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Button36)
        Me.GroupBox3.Controls.Add(Me.Button25)
        Me.GroupBox3.Controls.Add(Me.Button19)
        Me.GroupBox3.Controls.Add(Me.Button3)
        Me.GroupBox3.Controls.Add(Me.Button13)
        Me.GroupBox3.Controls.Add(Me.Button20)
        Me.GroupBox3.Controls.Add(Me.Button12)
        Me.GroupBox3.Controls.Add(Me.Button30)
        Me.GroupBox3.Controls.Add(Me.Button22)
        Me.GroupBox3.Controls.Add(Me.Button7)
        Me.GroupBox3.Controls.Add(Me.Button21)
        Me.GroupBox3.Controls.Add(Me.Button24)
        Me.GroupBox3.Controls.Add(Me.Button10)
        Me.GroupBox3.Location = New System.Drawing.Point(556, 73)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(273, 201)
        Me.GroupBox3.TabIndex = 39
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Other Processors"
        '
        'Button36
        '
        Me.Button36.Location = New System.Drawing.Point(130, 141)
        Me.Button36.Name = "Button36"
        Me.Button36.Size = New System.Drawing.Size(58, 53)
        Me.Button36.TabIndex = 39
        Me.Button36.Text = "Test Keane Fire"
        Me.Button36.UseVisualStyleBackColor = True
        '
        'Button37
        '
        Me.Button37.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button37.Location = New System.Drawing.Point(78, 198)
        Me.Button37.Name = "Button37"
        Me.Button37.Size = New System.Drawing.Size(112, 48)
        Me.Button37.TabIndex = 48
        Me.Button37.Text = "Query OpenSIMPPLLE Outputs"
        Me.ToolTip1.SetToolTip(Me.Button37, "Query for OpenSIMPPLLE 1.4 +.")
        Me.Button37.UseVisualStyleBackColor = True
        '
        'SOAPForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(841, 420)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Name = "SOAPForm"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.Text = "SOAP 10/31/2017"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim CombineForm As New Form
        CombineForm = Combiner
        CombineForm.Show()
        CombineForm = Nothing
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim infile As String
        Dim outfile As String

        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select a file with unique definitions to combine"
            .Filter = "Comma-delimited files(.csv) |*.csv"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        infile = openfiledialog2.FileName

        openfiledialog2 = Nothing

        Dim savefiledialog As New SaveFileDialog

        With savefiledialog
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Save the output file as"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            .FileName = "output"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        outfile = savefiledialog.FileName

        savefiledialog = Nothing

        CombineUniqueValues(infile, outfile)
        MsgBox("Done")

    End Sub
    Private Sub CombineUniqueValues(ByVal infile As String, ByVal outfile As String)
        Dim fnum As Integer = FreeFile()
        Dim str As String
        Dim dummy As String
        Dim NFields As Integer
        Dim FieldNames() As String
        'number of values in each field
        Dim FieldVals(,) As String
        Dim arr() As String
        Dim Nlines As Integer

        FileOpen(fnum, infile, OpenMode.Input)
        dummy = Trim(LineInput(fnum))
        If dummy.Length = 0 Then
            FileClose(fnum)
            Exit Sub
        End If
        arr = Split(dummy, ",")
        NFields = arr.Length
        ReDim FieldNames(NFields)
        ReDim NFieldVals(NFields)
        For j As Integer = 1 To NFields
            FieldNames(j) = arr(j - 1)
        Next
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length = 0 Then Exit Do
            Nlines = Nlines + 1
            ReDim Preserve FieldVals(NFields, Nlines)
            arr = Split(dummy, ",")
            For j As Integer = 1 To NFields
                FieldVals(j, Nlines) = arr(j - 1)
                If FieldVals(j, Nlines) <> "" Then NFieldVals(j) = NFieldVals(j) + 1
            Next
        Loop

        FileClose(fnum)
        fnum = FreeFile()
        FileOpen(fnum, outfile, OpenMode.Output)
        str = ""
        For j As Integer = 1 To NFields - 1
            str = str & FieldNames(j) & ","
        Next
        str = str & FieldNames(NFields)
        PrintLine(fnum, str)

        NCombos = 1
        For jfield As Integer = 1 To NFields
            NCombos = NCombos * NFieldVals(jfield)
        Next
        ReDim PrintCombos(NCombos)

        str = ""
        ComboCount = 0
        CompileString(str, 1, 1, NFields, NFieldVals(0), FieldVals)


        For jcomb As Integer = 1 To NCombos
            PrintLine(fnum, PrintCombos(jcomb))
        Next

        FileClose(fnum)

    End Sub

    Private Sub CompileString(ByRef str As String, ByRef fieldind As Integer, ByRef valind As Integer, ByVal maxfieldind As Integer, ByVal maxvalind As Integer, ByVal values(,) As String)
        Dim basestring As String = str
        If fieldind < maxfieldind Then

            For jval As Integer = 1 To NFieldVals(fieldind)
                str = basestring & values(fieldind, jval) & ","
                fieldind = fieldind + 1
                CompileString(str, fieldind, jval, maxfieldind, NFieldVals(fieldind), values)
            Next jval
            fieldind = fieldind - 1
        Else
            For jval As Integer = 1 To maxvalind
                'str = str 
                ComboCount = ComboCount + 1
                'ReDim Preserve PrintCombos(NCombos)
                PrintCombos(ComboCount) = str & values(fieldind, jval)
            Next
            fieldind = fieldind - 1
        End If

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim wlform As New Form
        wlform = QuerySIMPPLLEoutput
        wlform.Show()

        wlform = Nothing
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim specsim As New Form
        specsim = SpectrumToSIMPPLLE
        specsim.Show()

        specsim = Nothing
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Dim vmapsim As New Form
        vmapsim = VMAPtoSIMPPLLE
        vmapsim.Show()

        vmapsim = Nothing
    End Sub

    Private Sub Button4_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim SimDisturbs As New Form
        SimDisturbs = ProcessSIMPPLLEDisturbances
        SimDisturbs.Show()

        SimDisturbs = Nothing
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Dim TreatFeas As New Form
        TreatFeas = SIMPPLLETreatmentFeas
        TreatFeas.Show()

        TreatFeas = Nothing
    End Sub

    Private Sub MainForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnLockedDisturbances(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim infile As String
        Dim outfile As String

        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select a file with disturbance information"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        infile = openfiledialog2.FileName

        openfiledialog2 = Nothing

        Dim savefiledialog As New SaveFileDialog

        With savefiledialog
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Save the output file as"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            .FileName = "output"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        outfile = savefiledialog.FileName

        savefiledialog = Nothing

        Dim clgen As New clGeneralProcesses
        clgen.LockedDisturbs(infile, outfile)
        clgen = Nothing


        MsgBox("Done")

    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Dim UpdateAtts As New Form
        UpdateAtts = UpdateAttributesall
        UpdateAtts.Show()

        UpdateAtts = Nothing
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        Dim DataAn As New Form
        DataAn = DataAnalysis
        DataAn.Show()

        DataAn = Nothing

    End Sub

    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        Dim ProbSurf As New Form
        ProbSurf = ProbabilitySurfaces
        ProbSurf.Show()

        ProbSurf = Nothing
    End Sub

    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        Dim CrossTab As New clCrossTable
        CrossTab.RunCrossTab()

        CrossTab = Nothing
    End Sub

    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        MsgBox("This function takes a text column with 3 columns - group, parent, and value - and creates a cross table per group from the number of occurrences in the values. Each parent code potentially occurs multiple times with unique values. There is a header row to label the columns. It is a csv file. The first column is the group, the second is the parent and the third contains the values")

    End Sub

    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click
        Dim ProbSurfOverlay As New Form
        ProbSurfOverlay = ProbabilitySurfaces_Overlay
        ProbSurfOverlay.Show()

        ProbSurfOverlay = Nothing
    End Sub

    Private Sub Button15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button15.Click
        Dim infile As String
        Dim fnum As Integer
        Dim arr() As String
        Dim filenames() As String
        Dim subNSLINK() As Long
        Dim subNPers() As Integer
        Dim sDummy As String
        Dim NFiles As Integer
        Dim PrdFilter() As Integer
        'Dim outfile As String

        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select a file with names of firespread files to convert"
            .Filter = "Text Files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        infile = openfiledialog2.FileName

        openfiledialog2 = Nothing

        Dim dummy() As Integer
        ReDim dummy(0)
        NFiles = 0
        ReDim subNSLINK(NFiles), subNPers(NFiles), filenames(NFiles)
        'GetFileNames(infile, False) 'returns FileNames and NFiles
        fnum = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        Do Until EOF(fnum)
            'For jfile As Integer = 1 To NFiles 'process each of these files
            sDummy = Trim(LineInput(fnum))
            If sDummy = "" Then Exit Do
            NFiles = NFiles + 1
            ReDim Preserve subNSLINK(NFiles), subNPers(NFiles), filenames(NFiles)
            arr = Split(sDummy, ",") 'first is file name, second is NSLINK, third is NPers
            filenames(NFiles) = arr(0)
            subNSLINK(NFiles) = CType(arr(1), Long)
            subNPers(NFiles) = arr(2)
            'Next jfile
        Loop
        FileClose(fnum)

        Dim convert As New clProcessSimpplle
        convert.ConvertFireSpreadtoMMaPPit(filenames, subNSLINK, subNPers, dummy)
        convert = Nothing
        MsgBox("Done")
    End Sub

    Private Sub Button16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button16.Click
        MsgBox("Input file for Convert firespread is full path and filename, #SLINK polygons, #Time Periods, not including time 0. Separated by ','")

    End Sub

    Private Sub Button18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button18.Click
        MsgBox("Alternate method to create .attributesall and .spatialrelate files using exported GIS table. Requires Elevation and polygon centroid x/y coordinates")
    End Sub

    Private Sub Button17_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button17.Click
        Dim SpatialRelate As New Form
        SpatialRelate = SpatialRelations
        SpatialRelate.Show()

        SpatialRelate = Nothing
    End Sub

    Private Sub Button19_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button19.Click
        Dim burn As New Form
        burn = FireSpread
        burn.Show()

        burn = Nothing
    End Sub

    Private Sub Button20_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button20.Click

    End Sub

    Private Sub Button22_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button22.Click
        MsgBox("This routine associates VMAP polygons with the closest FIA plot. 3 input files: VMAP poly text file with attributes and x/y coords, FIA text file with x/y coords, and a model file that defines matching trials, including a distance")

    End Sub

    Private Sub Button21_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button21.Click
        Dim vmapfile As String
        Dim fiafile As String
        Dim searchfile As String

        Dim Openfiledialog1 As New OpenFileDialog
        With Openfiledialog1
            .Title = "Choose the VMAP file with polygon attribute and centroid information"
            .Filter = "Text File (*.txt)|*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        vmapfile = Openfiledialog1.FileName

        With Openfiledialog1
            .Title = "Choose the FIA Plot file with plot attribute and centroid information"
            .Filter = "Text File (*.txt)|*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        fiafile = Openfiledialog1.FileName

        With Openfiledialog1
            .Title = "Choose the Search file to specify what qualifies as the closest plot"
            .Filter = "Text File (*.txt)|*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        searchfile = Openfiledialog1.FileName



        Openfiledialog1 = Nothing


        Dim clClosest As New clVMAPwithFIA
        clClosest.RunClosestPlot(vmapfile, fiafile, searchfile)

        clClosest = Nothing
        MsgBox("Closest Plots Identified")
    End Sub

    Private Sub Button23_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button23.Click
        Dim FileToClean As String
        Dim Openfiledialog1 As New OpenFileDialog
        With Openfiledialog1
            .Title = "Open the text file to clean"
            .Filter = "Text File (*.txt)|*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        FileToClean = Openfiledialog1.FileName
        Openfiledialog1 = Nothing
        CleanFile(FileToClean)

        MsgBox("File Cleaned")
    End Sub

    Private Sub Button25_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button25.Click
        MsgBox("Reads 2 ASCII files for each pixel - STAND ID and SIZE and outputs distribution metrics based on user-defined bins defined in a 3rd file")
    End Sub

    Private Sub Button24_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button24.Click
        Dim StandFile As String
        Dim ValueFile As String
        Dim BinFile As String

        Dim Openfiledialog1 As New OpenFileDialog
        With Openfiledialog1
            .Title = "Select the file with Stand ID information"
            .Filter = "Text File (*.txt)|*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        StandFile = Openfiledialog1.FileName

        With Openfiledialog1
            .Title = "Select the file with Value information per pixel"
            .Filter = "Text File (*.txt)|*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        ValueFile = Openfiledialog1.FileName

        With Openfiledialog1
            .Title = "Select the file with Bin information for values"
            .Filter = "Text File (*.txt)|*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        BinFile = Openfiledialog1.FileName

        Openfiledialog1 = Nothing
        Dim overlay As New clASCIIMapOverlay
        overlay.RunOverlay(StandFile, ValueFile, BinFile)
        overlay = Nothing

        MsgBox("Overlay Complete")
    End Sub

    Private Sub Button27_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button27.Click
        MsgBox("Reassigns values of an attribute based on over- or under-representation, adjacency to other polygons with the desirable attribute value, and whether the polygon has desirable other attribute values to be reassigned.")
    End Sub

    Private Sub Button26_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button26.Click
        Dim redraw As New frmRemapIters
        redraw.Show()
        redraw = Nothing


    End Sub

    Private Sub Button29_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button29.Click
        MsgBox("Allows you to check a .attributesall file or raw GIS table export for pathway compliance and apply a ruleset to fix mis-matches. Also allows you to copy pathways from one HTG to another.")
    End Sub

    Private Sub Button28_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button28.Click
        Dim pathedit As New PathwayEdit
        pathedit.Show()
        pathedit = Nothing
    End Sub

    Private Sub Button30_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button30.Click
        Dim threshvalsearch As New frmSearchForThreshVals
        threshvalsearch.Show()
        threshvalsearch = Nothing

    End Sub

    Private Sub Button31_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button31.Click
        MsgBox("Strips quotation marks and thousands commas out of a comma delimited text file. Automatically creates a new file with '_clean.txt' suffix. Maintains original .txt file")

    End Sub

    Private Sub Button32_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button32.Click
        Dim attscrubber As New clInputFileScrubber
        attscrubber.RunScrubber()
        attscrubber = Nothing
        MsgBox("Done scrubbing")
    End Sub

    Private Sub Button33_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button33.Click
        MsgBox("Scrubber reads an existing .attributesall file, renumbers SLINK starting at 1, then eliminates any polygon references in the .spatialrelate file that are not defined in .attributesall")
    End Sub

    Private Sub Button34_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button34.Click
        Dim msg As String
        msg = "The bread and butter of SOAP, this walks you through both setting up the queries you want to run as well as identifying all SIMPPLLE runs you want to run through the query"
        MsgBox(msg)
    End Sub

    Private Sub Button35_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button35.Click
        Dim msg As String
        msg = "After queries are run, this routine allows you to combine the results. You can combine multiple files from different Geographic areas into a single full-landscape run. You can look through multiple runs to search for min/max/average/standard deviation. You can concatenate and sort files. There is also access to a patch processing routine."
        MsgBox(msg)

    End Sub

    Private Sub Button36_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button36.Click
        Dim burn As New Form
        burn = FireSpread_Keane2
        burn.Show()

        burn = Nothing
    End Sub

    Private Sub Button37_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button37.Click
        Dim frm As New Form
        frm = frmQuerySIMPPLLEoutputNEW
        frm.Show()

        frm = Nothing
    End Sub
End Class
