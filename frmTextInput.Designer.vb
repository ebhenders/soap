<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTextInput
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblTextInput = New System.Windows.Forms.Label
        Me.tbTextInput = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'lblTextInput
        '
        Me.lblTextInput.AutoSize = True
        Me.lblTextInput.Location = New System.Drawing.Point(17, 4)
        Me.lblTextInput.Name = "lblTextInput"
        Me.lblTextInput.Size = New System.Drawing.Size(71, 13)
        Me.lblTextInput.TabIndex = 0
        Me.lblTextInput.Text = "<input value>"
        '
        'tbTextInput
        '
        Me.tbTextInput.Location = New System.Drawing.Point(16, 27)
        Me.tbTextInput.Name = "tbTextInput"
        Me.tbTextInput.Size = New System.Drawing.Size(160, 20)
        Me.tbTextInput.TabIndex = 1
        Me.tbTextInput.Text = "<input value>"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(60, 65)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(81, 41)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "OK"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'frmTextInput
        '
        Me.AcceptButton = Me.Button1
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.tbTextInput)
        Me.Controls.Add(Me.lblTextInput)
        Me.Name = "frmTextInput"
        Me.Text = "frmTextInput"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblTextInput As System.Windows.Forms.Label
    Friend WithEvents tbTextInput As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
