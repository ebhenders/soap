<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DataAnalysis
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.cbMinMax = New System.Windows.Forms.CheckBox
        Me.cbAverage = New System.Windows.Forms.CheckBox
        Me.cbQuartile = New System.Windows.Forms.CheckBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.rbFile = New System.Windows.Forms.RadioButton
        Me.rbMetric = New System.Windows.Forms.RadioButton
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.cbGlobalStats = New System.Windows.Forms.CheckBox
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 38)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(132, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Output Metrics:"
        '
        'cbMinMax
        '
        Me.cbMinMax.AutoSize = True
        Me.cbMinMax.Location = New System.Drawing.Point(44, 70)
        Me.cbMinMax.Name = "cbMinMax"
        Me.cbMinMax.Size = New System.Drawing.Size(69, 17)
        Me.cbMinMax.TabIndex = 1
        Me.cbMinMax.Text = "Min, Max"
        Me.cbMinMax.UseVisualStyleBackColor = True
        '
        'cbAverage
        '
        Me.cbAverage.AutoSize = True
        Me.cbAverage.Location = New System.Drawing.Point(44, 93)
        Me.cbAverage.Name = "cbAverage"
        Me.cbAverage.Size = New System.Drawing.Size(66, 17)
        Me.cbAverage.TabIndex = 2
        Me.cbAverage.Text = "Average"
        Me.cbAverage.UseVisualStyleBackColor = True
        '
        'cbQuartile
        '
        Me.cbQuartile.AutoSize = True
        Me.cbQuartile.Location = New System.Drawing.Point(44, 116)
        Me.cbQuartile.Name = "cbQuartile"
        Me.cbQuartile.Size = New System.Drawing.Size(62, 17)
        Me.cbQuartile.TabIndex = 3
        Me.cbQuartile.Text = "Quartile"
        Me.cbQuartile.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 158)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(171, 20)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Organize Output By:"
        '
        'rbFile
        '
        Me.rbFile.AutoSize = True
        Me.rbFile.Checked = True
        Me.rbFile.Location = New System.Drawing.Point(43, 191)
        Me.rbFile.Name = "rbFile"
        Me.rbFile.Size = New System.Drawing.Size(107, 17)
        Me.rbFile.TabIndex = 5
        Me.rbFile.TabStop = True
        Me.rbFile.Text = "Stats for each file"
        Me.rbFile.UseVisualStyleBackColor = True
        '
        'rbMetric
        '
        Me.rbMetric.AutoSize = True
        Me.rbMetric.Location = New System.Drawing.Point(44, 215)
        Me.rbMetric.Name = "rbMetric"
        Me.rbMetric.Size = New System.Drawing.Size(122, 17)
        Me.rbMetric.TabIndex = 6
        Me.rbMetric.TabStop = True
        Me.rbMetric.Text = "Stats for each metric"
        Me.rbMetric.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(81, 268)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(84, 53)
        Me.Button1.TabIndex = 7
        Me.Button1.Text = "GO"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(191, 40)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(37, 30)
        Me.Button2.TabIndex = 8
        Me.Button2.Text = "?"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'cbGlobalStats
        '
        Me.cbGlobalStats.AutoSize = True
        Me.cbGlobalStats.Enabled = False
        Me.cbGlobalStats.Location = New System.Drawing.Point(43, 238)
        Me.cbGlobalStats.Name = "cbGlobalStats"
        Me.cbGlobalStats.Size = New System.Drawing.Size(136, 17)
        Me.cbGlobalStats.TabIndex = 9
        Me.cbGlobalStats.Text = "Report Global Statistics"
        Me.cbGlobalStats.UseVisualStyleBackColor = True
        '
        'DataAnalysis
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 390)
        Me.Controls.Add(Me.cbGlobalStats)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.rbMetric)
        Me.Controls.Add(Me.rbFile)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cbQuartile)
        Me.Controls.Add(Me.cbAverage)
        Me.Controls.Add(Me.cbMinMax)
        Me.Controls.Add(Me.Label1)
        Me.Name = "DataAnalysis"
        Me.Text = "Data Analysis                "
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbMinMax As System.Windows.Forms.CheckBox
    Friend WithEvents cbAverage As System.Windows.Forms.CheckBox
    Friend WithEvents cbQuartile As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents rbFile As System.Windows.Forms.RadioButton
    Friend WithEvents rbMetric As System.Windows.Forms.RadioButton
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents cbGlobalStats As System.Windows.Forms.CheckBox
End Class
