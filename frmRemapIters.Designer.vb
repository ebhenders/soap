<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRemapIters
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.tbNIters = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.tbModelName = New System.Windows.Forms.TextBox
        Me.GO = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.tbMaxAreaSwitch = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(35, 79)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(210, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Specify the number of remapping iterations:"
        '
        'tbNIters
        '
        Me.tbNIters.Location = New System.Drawing.Point(103, 95)
        Me.tbNIters.Name = "tbNIters"
        Me.tbNIters.Size = New System.Drawing.Size(40, 20)
        Me.tbNIters.TabIndex = 1
        Me.tbNIters.Text = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(35, 23)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(124, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Remapping model name:"
        '
        'tbModelName
        '
        Me.tbModelName.Enabled = False
        Me.tbModelName.Location = New System.Drawing.Point(36, 44)
        Me.tbModelName.Name = "tbModelName"
        Me.tbModelName.Size = New System.Drawing.Size(197, 20)
        Me.tbModelName.TabIndex = 3
        '
        'GO
        '
        Me.GO.Location = New System.Drawing.Point(94, 195)
        Me.GO.Name = "GO"
        Me.GO.Size = New System.Drawing.Size(65, 41)
        Me.GO.TabIndex = 4
        Me.GO.Text = "GO"
        Me.GO.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(35, 139)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(239, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Specify the maximum area to change per iteration"
        '
        'tbMaxAreaSwitch
        '
        Me.tbMaxAreaSwitch.Location = New System.Drawing.Point(103, 155)
        Me.tbMaxAreaSwitch.Name = "tbMaxAreaSwitch"
        Me.tbMaxAreaSwitch.Size = New System.Drawing.Size(90, 20)
        Me.tbMaxAreaSwitch.TabIndex = 6
        Me.tbMaxAreaSwitch.Text = "1000000"
        '
        'frmRemapIters
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.tbMaxAreaSwitch)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.GO)
        Me.Controls.Add(Me.tbModelName)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.tbNIters)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmRemapIters"
        Me.Text = "Remapping Controls"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tbNIters As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tbModelName As System.Windows.Forms.TextBox
    Friend WithEvents GO As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tbMaxAreaSwitch As System.Windows.Forms.TextBox
End Class
