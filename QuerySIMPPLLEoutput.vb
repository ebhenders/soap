Public Class QuerySIMPPLLEoutput

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'IDENTIFY THE DIRECTORY WITH MODEL DEFINITION FILES IN IT
        Dim folderdialog2 As New FolderBrowserDialog

        With folderdialog2
            '.InitialDirectory = "d:\Analysis\"
            .Description = "Folder that contains query files"
            '.Title = "Select a folder that has the "
            '.Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        tbModelDefsDir.Text = folderdialog2.SelectedPath
        folderdialog2 = Nothing


    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'IDENTIFY THE FILE WITH INFORMATION ABOUT THE SIMPPLLE OUTPUT FILES TO PROCESS
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select a file with SIMPPLLE output files to process"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        tbBatchFile.Text = openfiledialog2.FileName
        openfiledialog2 = Nothing

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'IDENTIFY THE file OF INITIAL CONDITIONS file names
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select a file with initial condition file names"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        tbInitCondFiles.Text = openfiledialog2.FileName
        openfiledialog2 = Nothing

    End Sub
    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        'IDENTIFY THE .BAT FILE TO UNZIP THE .GZ SIMPPLLE OUTPUT FILES
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select a .bat file to unzip SIMPPLLE outputs"
            .Filter = "Windows Batch files(.bat) |*.bat"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        tbunzipsimpplle.Text = openfiledialog2.FileName
        openfiledialog2 = Nothing
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        ICDefFileName = tbInitCondFiles.Text
        OutFilesToProcessFileName = tbBatchFile.Text
        LUTBaseFolder = tbModelDefsDir.Text
        Dim arr() As String
        Dim arr2() As String

        'FIGURE OUT WHERE TO DUMP THE OUTPUTS
        Dim savefiledialog1 As New SaveFileDialog

        With savefiledialog1
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Save the outputs to:"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            .FileName = "HabOutputs"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        HabOutputFN = savefiledialog1.FileName
        savefiledialog1 = Nothing

        'run the batch file unzipper
        If IO.File.Exists(tbUnzipSIMPPLLE.Text) Then
            Shell(tbUnzipSIMPPLLE.Text, AppWinStyle.NormalFocus, True)
        End If


        bWritePolyAtts = cbWritePolyAtts.Checked
        bWriteProcessMaps = cbWriteFireFile.Checked
        'bWriteTreatmentMaps = cbWriteTreatmentMaps.Checked
        bWriteFireSpreadMaps = cbWriteFireSpread.Checked
        bWriteGrids = cbWriteGrids.Checked
        bWriteProcessStats = cbWriteProcessStats.Checked
        'bWriteTreatmentStats = cbWriteTreatmentStats.Checked
        bSplitMaps = cbSplitMaps.Checked
        bWriteGISMaps = cbWriteGISMaps.Checked
        bWritePatchMaps = cbPatchMaps.Checked
        WLHabAcreAdjust = CType(Trim(tbacreadjust.text), Double)

        'timestep stuff
        NTimestep = 0
        'NTimestep = arr.Length
        ReDim TimestepIndex(NTimestep)
        TimestepIndex(0) = 0
        MaxTimestep = 0
        arr = Split(tbTimesteps.Text, ",")
        For jstep As Integer = 0 To arr.Length - 1
            arr2 = Split(arr(jstep), "-")
            For jstep2 As Integer = CType(Trim(arr2(0)), Integer) To CType(Trim(arr2(arr2.Length - 1)), Integer)
                NTimestep = NTimestep + 1
                ReDim Preserve TimestepIndex(NTimestep)
                TimestepIndex(NTimestep) = jstep2 'CType(Trim(arr2(NTimestep - 1)), Integer)
                MaxTimestep = Math.Max(MaxTimestep, jstep2)
            Next
        Next

        'save this query file
        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, DirName(HabOutputFN) & FName(HabOutputFN) & "_model.log", OpenMode.Output)
        PrintLine(fnum, "Parameters for  SIMPPLLE Output Queries")
        PrintLine(fnum, "Query Directory, " & LUTBaseFolder)
        PrintLine(fnum, "Initial Conditions file, " & ICDefFileName)
        PrintLine(fnum, "Batch File for Runs To PRocess, " & OutFilesToProcessFileName)
        PrintLine(fnum, "Unzipping .bat file, " & tbUnzipSIMPPLLE.Text)
        PrintLine(fnum, "Timesteps to Query: " & tbTimesteps.Text)
        PrintLine(fnum, "Write Query Polygon Maps for MMaPPit, " & bWritePolyAtts)
        PrintLine(fnum, "Write Process Maps for MMaPPit, " & bWriteProcessMaps)
        PrintLine(fnum, "Write Fire Spread Maps for MMaPPit, " & bWriteFireSpreadMaps)
        PrintLine(fnum, "Write Process Stats to Text File, " & bWriteProcessStats)
        PrintLine(fnum, "WritePolygon Atts for GIS Mapping, " & bWriteGISMaps)
        PrintLine(fnum, "Write Grids for Fragstats, " & bWriteGrids)
        PrintLine(fnum, "Split Out Special Area/Ownership Maps, " & bSplitMaps)
        PrintLine(fnum, "Write Patch Maps, " & bWritePatchMaps)
        PrintLine(fnum, "Factor to adjust acre values, " & WLHabAcreAdjust)
        'PrintLine(fnum, "Write Treatment Maps for MMaPPit, " & bWriteTreatmentMaps)
        'PrintLine(fnum, "Write Treatment Stats to Text File, " & bWriteTreatmentStats)
        FileClose(fnum)


        Dim wlclass As New clSIMPPLLEQuery
        wlclass.RunQueryModule("SIMPPLLE")
        wlclass = Nothing

        savefiledialog1 = Nothing

    End Sub

    Private Sub cbWriteGrids_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbWriteGrids.CheckedChanged

    End Sub

    Private Sub WildlifeAnalysis_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim BatchWizard As New BuildBatchFile
        BatchWizard.Show()
        BatchWizard = Nothing
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim VerifyMD As New frmQuerySetUp
        VerifyMD.Show()
        VerifyMD = Nothing
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        MsgBox("Use the adjustment factor if your cell size is different from that in SIMPPLLE. SIMPPLLE only allows integer values, and true acreage may be more or less than what was entered into SIMPPLLE. This is important when adding up many cells to get forest-wide total figures.")
    End Sub

   
    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        MsgBox("This screen walks you through creating the files used to query SIMPPLLE results")
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        MsgBox("This will open a dialog that will help you select the SIMPPLLE runs you want to run the query on. You can define multiple runs across multiple Geographic Areas")
    End Sub

    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        MsgBox("Directory with query files. This is populated automatically if you use the Query Set Up dialog (above). If you are re-using a query, you can directly navigate to the folder with the 'Browse' button. WARNING: There are limited controls on checking the data if you do not use the Query Set Up dialog.")
    End Sub

    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        MsgBox("This file lists the appropriate .attributesall and .spatialrelate files associated with each Run you are querying. Suggest using the 'Runs to Query' dialog above to create this file.")
    End Sub

    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        MsgBox("Lists the SIMPPLLE runs to query. Suggest using the 'Runs to Query' button above to help create this. Or, you can navigate to one already created, but there is limited error-checking against that file.")
    End Sub

    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click
        MsgBox("Points to a batch file to unzip the SIMPPLLE outputs using the gzip program. Suggest using the 'Runs to Query' dialog above to create this file.")
    End Sub

    Private Sub Button15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button15.Click
        MsgBox("Specify which timesteps in the SIMPPLLE output files you want to query. Values may be separated by a comma , or dash - ")
    End Sub


    Private Sub Button16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button16.Click
        Dim msg As String
        msg = " The standard outputs from this tool are a set of comma-delimited files that summarize the queied values for each time step and delineated by the specified Special Area and Ownership groupings. There is no checkbox for standard outputs." & Chr(13) & Chr(13) _
               & " Polygon Maps give you a time-series map per Query column, per run. This means a query column with multiple values will have all of those values in a single map." & Chr(13) & Chr(13) _
               & "    Process Maps for MMaPPit produces a single map of all processes over time - 1 per run." & Chr(13) _
               & "    Fire Spread Maps use the -firespread files to produce a map of fire boundaries at each time period. These can be used to overlay on any output map." & Chr(13) & Chr(13) _
               & "Splitting by Special Area does not produce a map when checked by itself and can be useful when quantifying patches by Special Area and/or Ownership combination." & Chr(13) & Chr(13) _
               & " Writing the Process Stats to Text file does not produce maps, and is really only necessary with one of your queries." & Chr(13) & Chr(13) _
                & " Polygon Atts for GIS maps puts outputs for all queries into a single file for all timesteps of a run - one file per run." & Chr(13) & Chr(13) _
                & " Write Patch Maps will write a numeric output for the metric with a number for each polygon indicating which patch it is a part of." & Chr(13) & Chr(13) _
 & " Finally, the FRAGSTATS outputs generates a map per time period per Query per query value as well as a .fbt batch file that can be imported into Fragstats." _
                   & "Be careful with this one, as it can generate a LOT of data in a hurry."
        MsgBox(msg)
    End Sub

    Private Sub HelpToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HelpToolStripMenuItem.Click
        Dim msg As String = "This routine runs a query on all specified SIMPPLLE runs. The Query must first be defined - use the Query Set Up tool to help with that. Then, use the Runs To Query tool to point to the SIMPPLLE runs you want to query. " & Chr(13) & Chr(13) _
        & "The standard outputs from this tool are a set of comma-delimited files that summarize the queied values for each time step and delineated by the specified Special Area and Ownership groupings."
        MsgBox(msg)

    End Sub

    Private Sub tbAcreAdjust_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbAcreAdjust.TextChanged

    End Sub

    Private Sub SaveModelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveModelToolStripMenuItem.Click
        'FIGURE OUT WHERE TO DUMP THE OUTPUTS
        Dim savefiledialog1 As New SaveFileDialog
        Dim modelFN As String

        With savefiledialog1
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Save the model to:"
            .Filter = "Comma-delimited files(.log) |*.log"
            .FileName = "SIMPPLLE_Query_Model.log"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        modelFN = savefiledialog1.FileName
        savefiledialog1 = Nothing

        ICDefFileName = tbInitCondFiles.Text
        OutFilesToProcessFileName = tbBatchFile.Text
        LUTBaseFolder = tbModelDefsDir.Text

        bWritePolyAtts = cbWritePolyAtts.Checked
        bWriteProcessMaps = cbWriteFireFile.Checked
        'bWriteTreatmentMaps = cbWriteTreatmentMaps.Checked
        bWriteFireSpreadMaps = cbWriteFireSpread.Checked
        bWriteGrids = cbWriteGrids.Checked
        bWriteProcessStats = cbWriteProcessStats.Checked
        'bWriteTreatmentStats = cbWriteTreatmentStats.Checked
        bSplitMaps = cbSplitMaps.Checked
        bWriteGISMaps = cbWriteGISMaps.Checked
        bWritePatchMaps = cbPatchMaps.Checked
        WLHabAcreAdjust = CType(Trim(tbAcreAdjust.Text), Double)

        'save this query file
        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, modelFN, OpenMode.Output)
        PrintLine(fnum, "Parameters for  SIMPPLLE Output Queries")
        PrintLine(fnum, "Query Directory, " & LUTBaseFolder)
        PrintLine(fnum, "Initial Conditions file, " & ICDefFileName)
        PrintLine(fnum, "Batch File for Runs To PRocess, " & OutFilesToProcessFileName)
        PrintLine(fnum, "Unzipping .bat file, " & tbUnzipSIMPPLLE.Text)
        PrintLine(fnum, "Timesteps to Query: " & tbTimesteps.Text)
        PrintLine(fnum, "Write Query Polygon Maps for MMaPPit, " & bWritePolyAtts)
        PrintLine(fnum, "Write Process Maps for MMaPPit, " & bWriteProcessMaps)
        PrintLine(fnum, "Write Fire Spread Maps for MMaPPit, " & bWriteFireSpreadMaps)
        PrintLine(fnum, "Write Process Stats to Text File, " & bWriteProcessStats)
        PrintLine(fnum, "WritePolygon Atts for GIS Mapping, " & bWriteGISMaps)
        PrintLine(fnum, "Write Grids for Fragstats, " & bWriteGrids)
        PrintLine(fnum, "Split Out Special Area/Ownership Maps, " & bSplitMaps)
        PrintLine(fnum, "Write Patch Maps, " & bWritePatchMaps)
        PrintLine(fnum, "Factor to adjust acre values, " & WLHabAcreAdjust)
        'PrintLine(fnum, "Write Treatment Maps for MMaPPit, " & bWriteTreatmentMaps)
        'PrintLine(fnum, "Write Treatment Stats to Text File, " & bWriteTreatmentStats)
        FileClose(fnum)
    End Sub

    Private Sub LoadModelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoadModelToolStripMenuItem.Click
        'FIGURE OUT WHERE TO DUMP THE OUTPUTS
        Dim openfiledialog1 As New OpenFileDialog
        Dim modelFN As String
        Dim dummy As String
        Dim atts() As String

        With openfiledialog1
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Open the Query Model:"
            .Filter = "Comma-delimited files(.log) |*.log"
            '.FileName = "SIMPPLLE_Query_Model.log"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        modelFN = openfiledialog1.FileName
        openfiledialog1 = Nothing

        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, modelFN, OpenMode.Input)
        dummy = LineInput(fnum) 'header
        dummy = LineInput(fnum) 'query directory
        atts = Split(dummy, ",")
        tbModelDefsDir.Text = Trim(atts(1))

        dummy = LineInput(fnum) 'IC files directory
        atts = Split(dummy, ",")
        tbInitCondFiles.Text = Trim(atts(1))

        dummy = LineInput(fnum) 'Batch File For Processing
        atts = Split(dummy, ",")
        tbBatchFile.Text = Trim(atts(1))

        dummy = LineInput(fnum) 'Unzipping batch
        atts = Split(dummy, ",")
        tbUnzipSIMPPLLE.Text = Trim(atts(1))

        dummy = LineInput(fnum) 'Timesteps
        atts = Split(dummy, ":")
        tbTimesteps.Text = Trim(atts(1))

        dummy = LineInput(fnum) 'Write Poly Atts
        atts = Split(dummy, ",")
        cbWritePolyAtts.Checked = Trim(atts(1))

        dummy = LineInput(fnum) 'Write Process Maps
        atts = Split(dummy, ",")
        cbWriteFireFile.Checked = Trim(atts(1))

        dummy = LineInput(fnum) 'Write Fire Spread Maps
        atts = Split(dummy, ",")
        cbWriteFireSpread.Checked = Trim(atts(1))

        dummy = LineInput(fnum) 'Write Process Stats
        atts = Split(dummy, ",")
        cbWriteProcessStats.Checked = Trim(atts(1))

        dummy = LineInput(fnum) 'Write GIS maps
        atts = Split(dummy, ",")
        cbWriteGISMaps.Checked = Trim(atts(1))

        dummy = LineInput(fnum) 'Write Grids for Fragstats
        atts = Split(dummy, ",")
        cbWriteGrids.Checked = Trim(atts(1))

        dummy = LineInput(fnum) 'Split SpecAr/Own maps
        atts = Split(dummy, ",")
        cbSplitMaps.Checked = Trim(atts(1))

        dummy = LineInput(fnum) 'Write Patch Maps
        atts = Split(dummy, ",")
        cbPatchMaps.Checked = Trim(atts(1))

        dummy = LineInput(fnum) 'Acre Adjustment Factor
        atts = Split(dummy, ",")
        tbAcreAdjust.Text = Trim(atts(1))

        'defaults
        'cbWriteTreatmentMaps.Checked = False
        'cbWriteTreatmentStats.Checked = False
        'If EOF(fnum) = False Then
        '    dummy = LineInput(fnum) 'treatment maps
        '    atts = Split(dummy, ",")
        '    cbWriteTreatmentMaps.Checked = Trim(atts(1))

        '    dummy = LineInput(fnum) 'treatment stats
        '    atts = Split(dummy, ",")
        '    cbWriteTreatmentStats.Checked = Trim(atts(1))
        'End If

        FileClose(fnum)

    End Sub
End Class