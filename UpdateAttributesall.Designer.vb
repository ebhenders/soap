<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UpdateAttributesall
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tbRunOutputToUse = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.Button2 = New System.Windows.Forms.Button
        Me.tbAttributesallFile = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.tbSLINKfile = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'tbRunOutputToUse
        '
        Me.tbRunOutputToUse.Location = New System.Drawing.Point(144, 173)
        Me.tbRunOutputToUse.Name = "tbRunOutputToUse"
        Me.tbRunOutputToUse.Size = New System.Drawing.Size(596, 20)
        Me.tbRunOutputToUse.TabIndex = 2
        Me.tbRunOutputToUse.Text = "D:\R1\Revisions\KIPZ\KIPZ_SIMPPLLE\Oct2011_Runs\Scenario1\BULLSIMEX_S1-simulation" & _
            "\textdata"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(21, 170)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 31)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Run To use for updates:"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(759, 174)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 19)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Browse"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(232, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(462, 40)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Update Attributesall File"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(759, 94)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 19)
        Me.Button2.TabIndex = 8
        Me.Button2.Text = "Browse"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'tbAttributesallFile
        '
        Me.tbAttributesallFile.Location = New System.Drawing.Point(144, 94)
        Me.tbAttributesallFile.Name = "tbAttributesallFile"
        Me.tbAttributesallFile.Size = New System.Drawing.Size(596, 20)
        Me.tbAttributesallFile.TabIndex = 6
        Me.tbAttributesallFile.Text = "D:\R1\Revisions\KIPZ\KIPZ_SIMPPLLE\Oct2011_Runs\Scenario1\BULLSIMEX_S1-simulation" & _
            "\textdata"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(18, 133)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(120, 31)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "File of SLINK numbers to update:"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(383, 236)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(61, 43)
        Me.Button3.TabIndex = 9
        Me.Button3.Text = "GO"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(759, 134)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 19)
        Me.Button4.TabIndex = 12
        Me.Button4.Text = "Browse"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'tbSLINKfile
        '
        Me.tbSLINKfile.Location = New System.Drawing.Point(144, 133)
        Me.tbSLINKfile.Name = "tbSLINKfile"
        Me.tbSLINKfile.Size = New System.Drawing.Size(596, 20)
        Me.tbSLINKfile.TabIndex = 10
        Me.tbSLINKfile.Text = "D:\R1\Revisions\KIPZ\KIPZ_SIMPPLLE\Oct2011_Runs\Scenario1\BULLSIMEX_S1-simulation" & _
            "\textdata"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(18, 94)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(120, 31)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Attributesall file to update:"
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(141, 49)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(611, 31)
        Me.Label6.TabIndex = 19
        Me.Label6.Text = "Process updates specific SLINKs in the attributesall file based on Decade 1 resul" & _
            "ts from  a SIMPPLLE run"
        '
        'UpdateAttributesall
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(908, 298)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.tbSLINKfile)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.tbAttributesallFile)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.tbRunOutputToUse)
        Me.Controls.Add(Me.Label1)
        Me.Name = "UpdateAttributesall"
        Me.Text = "Update Attributesall file"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tbRunOutputToUse As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents tbAttributesallFile As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents tbSLINKfile As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
End Class
