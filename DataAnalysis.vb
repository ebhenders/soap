Public Class DataAnalysis
    Dim bMinMax As Boolean
    Dim bAvg As Boolean
    Dim bQuartile As Boolean
    Dim bOutByFile As Boolean
    Dim bGlobalStats As Boolean

    Dim Min(,) As Double
    Dim Max(,) As Double
    Dim Avg(,) As Double
    Dim UQuartile(,) As Double
    Dim LQuartile(,) As Double
    Dim NRecords() As Long 'number of records in each file - used to help calculate the global metrics

    Dim NFiles As Integer
    Dim FileNames() As String
    Dim NMetrics As Integer
    Dim MetricName() As String

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        MsgBox("This program will read a list of text files to process. Each file must be in comma-delimited format and contain the same number of columns. Each column is assumed to represent the same metric in each file")
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select a file with names of files to analyze"
            .Filter = "Text files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        FileListFN = openfiledialog2.FileName

        openfiledialog2 = Nothing

        Dim savefiledialog As New SaveFileDialog

        With savefiledialog
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Save the output file as"
            .Filter = "Comma-delimited files(.csv) |*.csv"
            .FileName = "output"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With



        OutputFile = savefiledialog.FileName
        savefiledialog = Nothing
        bMinMax = cbMinMax.Checked
        bAvg = cbAverage.Checked
        bQuartile = cbQuartile.Checked
        bGlobalStats = cbGlobalStats.Checked

        bOutByFile = rbFile.Checked

        ProcessMetrics()

    End Sub
    Private Sub ProcessMetrics()
        GetFileNames(FileListFN)
        CountMetrics(FileNames(1))
        ReDim NRecords(NFiles)
        If bMinMax = True Then
            ReDim Min(NFiles, NMetrics)
            ReDim Max(NFiles, NMetrics)
        End If
        If bAvg Then ReDim Avg(NFiles, NMetrics)
        If bQuartile Then
            ReDim UQuartile(NFiles, NMetrics)
            ReDim LQuartile(NFiles, NMetrics)
        End If
        CollectMetrics()
        WriteOutputs()
        MsgBox("Done")
    End Sub

    Private Sub GetFileNames(ByVal file As String)
        Dim fnum As Integer = FreeFile()
        Dim dummy As String
        Dim arr() As String

        FileOpen(fnum, file, OpenMode.Input)
        NFiles = 0
        ReDim FileNames(NFiles)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If dummy.Length > 0 Then
                NFiles = NFiles + 1
                ReDim Preserve FileNames(NFiles)
                FileNames(NFiles) = Trim(dummy)
            End If
        Loop
        FileClose(fnum)

    End Sub
    Private Sub CountMetrics(ByVal file As String)
        Dim fnum As Integer = FreeFile()
        Dim dummy As String
        Dim arr() As String

        FileOpen(fnum, file, OpenMode.Input)
        NMetrics = 0
    
        dummy = LineInput(fnum)
        arr = Split(dummy, ",")
        NMetrics = arr.Length
        ReDim MetricName(NMetrics)
        For j As Integer = 1 To NMetrics
            MetricName(j) = Trim(arr(j - 1))
        Next
        FileClose(fnum)
    End Sub

    Private Sub CollectMetrics()
        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String
        Dim Total() As Double
        Dim vals(,) As Double
        Dim val As Double
        Dim FigureQuartile() As Double
        Dim uq As Integer
        Dim lq As Integer
        ReDim Total(NMetrics)

        'set min max defaults


        For jfile As Integer = 1 To NFiles
            fnum = FreeFile()
            FileOpen(fnum, FileNames(jfile), OpenMode.Input)
            dummy = LineInput(fnum)
            'count the records in this file
            Do Until EOF(fnum)
                dummy = Trim(LineInput(fnum))
                If dummy.Length = 0 Then Exit Do
                NRecords(jfile) = NRecords(jfile) + 1
            Loop
            FileClose(fnum)
            If bQuartile = True Then ReDim vals(NRecords(jfile), NMetrics)
            If bAvg = True Then ReDim Total(NMetrics)
            'now get the attributes
            fnum = FreeFile()
            FileOpen(fnum, FileNames(jfile), OpenMode.Input)
            dummy = LineInput(fnum)
            For jline As Integer = 1 To NRecords(jfile)
                dummy = LineInput(fnum)
                arr = Split(dummy, ",")

                For jmetric As Integer = 1 To NMetrics
                    val = arr(jmetric - 1)
                    If bMinMax = True Then
                        If jline = 1 Then
                            Min(jfile, jmetric) = val
                            Max(jfile, jmetric) = val
                        Else
                            If val > Max(jfile, jmetric) Then Max(jfile, jmetric) = val
                            If val < Min(jfile, jmetric) Then Min(jfile, jmetric) = val
                        End If
                    End If
                    If bAvg = True Then
                        Total(jmetric) = Total(jmetric) + val
                    End If
                    If bQuartile = True Then vals(jline, jmetric) = val
                Next
            Next
            If bAvg = True Then
                For jmetric As Integer = 1 To NMetrics
                    Avg(jfile, jmetric) = Math.Round(Total(jmetric) / NRecords(jfile), 3)
                Next
            End If
            If bQuartile = True Then
                ReDim FigureQuartile(NRecords(jfile))
                uq = Math.Round(NRecords(jfile) * 0.75, 0)
                lq = Math.Round(NRecords(jfile) * 0.25, 0)
                For jmetric As Integer = 1 To NMetrics

                    For jrecord As Integer = 1 To NRecords(jfile)
                        FigureQuartile(jrecord) = vals(jrecord, jmetric)
                    Next
                    Array.Sort(FigureQuartile)
                    UQuartile(jfile, jmetric) = FigureQuartile(uq)
                    LQuartile(jfile, jmetric) = FigureQuartile(lq)
                Next jmetric
            End If
            FileClose(fnum)

        Next jfile
    End Sub

    Private Sub WriteOutputs()
        Dim fnum As Integer
        Dim dummy As String

        fnum = FreeFile()
        FileOpen(fnum, OutputFile, OpenMode.Output)

        If bOutByFile = True Then
            For jfile As Integer = 1 To NFiles
                dummy = "Metric" 'MetricName(1)
                For jmetric As Integer = 1 To NMetrics
                    dummy = dummy & "," & MetricName(jmetric)
                Next
                PrintLine(fnum, dummy)
                dummy = ""

                If bMinMax = True Then
                    dummy = "Min"
                    For jmetric As Integer = 1 To NMetrics
                        dummy = dummy & "," & Min(jfile, jmetric)
                    Next
                    PrintLine(fnum, dummy)
                    dummy = "Max"
                    For jmetric As Integer = 1 To NMetrics
                        dummy = dummy & "," & Max(jfile, jmetric)
                    Next
                    PrintLine(fnum, dummy)
                End If

                If bAvg = True Then
                    dummy = "Avg"
                    For jmetric As Integer = 1 To NMetrics
                        dummy = dummy & "," & Avg(jfile, jmetric)
                    Next
                    PrintLine(fnum, dummy)
                End If
                If bQuartile = True Then
                    dummy = "UQuartile"
                    For jmetric As Integer = 1 To NMetrics
                        dummy = dummy & "," & UQuartile(jfile, jmetric)
                    Next
                    PrintLine(fnum, dummy)
                    dummy = "LQuartile"
                    For jmetric As Integer = 1 To NMetrics
                        dummy = dummy & "," & LQuartile(jfile, jmetric)
                    Next
                    PrintLine(fnum, dummy)
                End If
                PrintLine(fnum, "")

            Next
        End If

        If bOutByFile = False Then
            For jmetric As Integer = 1 To NMetrics
                dummy = "Metric" 'MetricName(1)
                For jfile As Integer = 1 To NFiles
                    dummy = dummy & "," & MetricName(jmetric) & "_" & jfile
                Next
                PrintLine(fnum, dummy)
                dummy = ""

                If bMinMax = True Then
                    dummy = "Min"
                    For jfile As Integer = 1 To NFiles
                        dummy = dummy & "," & Min(jfile, jmetric)
                    Next
                    PrintLine(fnum, dummy)
                    dummy = "Max"
                    For jfile As Integer = 1 To NFiles
                        dummy = dummy & "," & Max(jfile, jmetric)
                    Next
                    PrintLine(fnum, dummy)
                End If

                If bAvg = True Then
                    dummy = "Avg"
                    For jfile As Integer = 1 To NFiles
                        dummy = dummy & "," & Avg(jfile, jmetric)
                    Next
                    PrintLine(fnum, dummy)
                End If
                If bQuartile = True Then
                    dummy = "UQuartile"
                    For jfile As Integer = 1 To NFiles
                        dummy = dummy & "," & UQuartile(jfile, jmetric)
                    Next
                    PrintLine(fnum, dummy)
                    dummy = "LQuartile"
                    For jfile As Integer = 1 To NFiles
                        dummy = dummy & "," & LQuartile(jfile, jmetric)
                    Next
                    PrintLine(fnum, dummy)
                End If
                PrintLine(fnum, "")

            Next
        End If
        FileClose(fnum)

    End Sub

End Class