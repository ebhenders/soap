Public Class frmRemapIters
    Dim ModelFile As String

    Private Sub frmRemapIters_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Openfiledialog1 As New OpenFileDialog
        With Openfiledialog1
            .Title = "Select the Redraw Model file that points to the polygon value and location files."
            .Filter = "Text File (*.txt)|*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        ModelFile = Openfiledialog1.FileName
        Openfiledialog1 = Nothing
        tbModelName.Text = FName(ModelFile)



    End Sub

    Private Sub GO_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GO.Click
        Dim clRedraw As New clSpatialReassign
        clRedraw.RunSpatialReassign(ModelFile, CType(tbNIters.Text, Integer), CType(tbMaxAreaSwitch.text, Single))
        clRedraw = Nothing


        MsgBox("Done Redrawing")
    End Sub
End Class