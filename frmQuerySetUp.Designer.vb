<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmQuerySetUp
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmQuerySetUp))
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.cbCheckModelDefs = New System.Windows.Forms.CheckBox
        Me.cbCheckSIMPPLLEOutputs = New System.Windows.Forms.CheckBox
        Me.cbCheckAttributesallFile = New System.Windows.Forms.CheckBox
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.cbbOwnership = New System.Windows.Forms.ComboBox
        Me.clbGroupNames = New System.Windows.Forms.CheckedListBox
        Me.cbbSpecialArea = New System.Windows.Forms.ComboBox
        Me.btnWriteQueryGroupFile = New System.Windows.Forms.Button
        Me.tbGroupName = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Button8 = New System.Windows.Forms.Button
        Me.rtbQueryDefs = New System.Windows.Forms.RichTextBox
        Me.Button9 = New System.Windows.Forms.Button
        Me.tbDeleteLine = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.pbCaution = New System.Windows.Forms.PictureBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Button11 = New System.Windows.Forms.Button
        Me.Label8 = New System.Windows.Forms.Label
        Me.Button10 = New System.Windows.Forms.Button
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Button12 = New System.Windows.Forms.Button
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Button13 = New System.Windows.Forms.Button
        Me.Button14 = New System.Windows.Forms.Button
        Me.Label13 = New System.Windows.Forms.Label
        Me.cbSpecies = New System.Windows.Forms.CheckBox
        Me.cbSpeciesHTG = New System.Windows.Forms.CheckBox
        Me.Button15 = New System.Windows.Forms.Button
        CType(Me.pbCaution, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(433, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(56, 29)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Help?"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(35, 24)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(84, 41)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "Collect Attributes"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(314, 25)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(39, 29)
        Me.Button3.TabIndex = 0
        Me.Button3.TabStop = False
        Me.Button3.Text = "?"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'cbCheckModelDefs
        '
        Me.cbCheckModelDefs.AutoSize = True
        Me.cbCheckModelDefs.Checked = True
        Me.cbCheckModelDefs.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbCheckModelDefs.Enabled = False
        Me.cbCheckModelDefs.Location = New System.Drawing.Point(129, 14)
        Me.cbCheckModelDefs.Name = "cbCheckModelDefs"
        Me.cbCheckModelDefs.Size = New System.Drawing.Size(145, 17)
        Me.cbCheckModelDefs.TabIndex = 0
        Me.cbCheckModelDefs.TabStop = False
        Me.cbCheckModelDefs.Text = "Check Query Defs Folder"
        Me.cbCheckModelDefs.UseVisualStyleBackColor = True
        '
        'cbCheckSIMPPLLEOutputs
        '
        Me.cbCheckSIMPPLLEOutputs.AutoSize = True
        Me.cbCheckSIMPPLLEOutputs.Checked = True
        Me.cbCheckSIMPPLLEOutputs.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbCheckSIMPPLLEOutputs.Location = New System.Drawing.Point(129, 37)
        Me.cbCheckSIMPPLLEOutputs.Name = "cbCheckSIMPPLLEOutputs"
        Me.cbCheckSIMPPLLEOutputs.Size = New System.Drawing.Size(179, 17)
        Me.cbCheckSIMPPLLEOutputs.TabIndex = 0
        Me.cbCheckSIMPPLLEOutputs.TabStop = False
        Me.cbCheckSIMPPLLEOutputs.Text = "Check SIMPPLLE outputs folder"
        Me.cbCheckSIMPPLLEOutputs.UseVisualStyleBackColor = True
        '
        'cbCheckAttributesallFile
        '
        Me.cbCheckAttributesallFile.AutoSize = True
        Me.cbCheckAttributesallFile.Checked = True
        Me.cbCheckAttributesallFile.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbCheckAttributesallFile.Location = New System.Drawing.Point(129, 60)
        Me.cbCheckAttributesallFile.Name = "cbCheckAttributesallFile"
        Me.cbCheckAttributesallFile.Size = New System.Drawing.Size(135, 17)
        Me.cbCheckAttributesallFile.TabIndex = 0
        Me.cbCheckAttributesallFile.TabStop = False
        Me.cbCheckAttributesallFile.Text = "Check .attributesall File"
        Me.cbCheckAttributesallFile.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(68, 227)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(84, 57)
        Me.Button4.TabIndex = 2
        Me.Button4.Text = "Define Processes for Disturbance Queries"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(68, 290)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(84, 41)
        Me.Button5.TabIndex = 3
        Me.Button5.Text = "Define Query Groupings"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(158, 238)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(28, 24)
        Me.Button6.TabIndex = 0
        Me.Button6.TabStop = False
        Me.Button6.Text = "?"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'cbbOwnership
        '
        Me.cbbOwnership.FormattingEnabled = True
        Me.cbbOwnership.Location = New System.Drawing.Point(424, 301)
        Me.cbbOwnership.Name = "cbbOwnership"
        Me.cbbOwnership.Size = New System.Drawing.Size(140, 21)
        Me.cbbOwnership.TabIndex = 6
        '
        'clbGroupNames
        '
        Me.clbGroupNames.CheckOnClick = True
        Me.clbGroupNames.FormattingEnabled = True
        Me.clbGroupNames.Location = New System.Drawing.Point(158, 274)
        Me.clbGroupNames.Name = "clbGroupNames"
        Me.clbGroupNames.Size = New System.Drawing.Size(163, 184)
        Me.clbGroupNames.TabIndex = 4
        '
        'cbbSpecialArea
        '
        Me.cbbSpecialArea.FormattingEnabled = True
        Me.cbbSpecialArea.Location = New System.Drawing.Point(424, 328)
        Me.cbbSpecialArea.Name = "cbbSpecialArea"
        Me.cbbSpecialArea.Size = New System.Drawing.Size(140, 21)
        Me.cbbSpecialArea.TabIndex = 7
        '
        'btnWriteQueryGroupFile
        '
        Me.btnWriteQueryGroupFile.BackColor = System.Drawing.Color.Lime
        Me.btnWriteQueryGroupFile.Location = New System.Drawing.Point(443, 418)
        Me.btnWriteQueryGroupFile.Name = "btnWriteQueryGroupFile"
        Me.btnWriteQueryGroupFile.Size = New System.Drawing.Size(84, 41)
        Me.btnWriteQueryGroupFile.TabIndex = 9
        Me.btnWriteQueryGroupFile.Text = "Write Query Grouping File"
        Me.btnWriteQueryGroupFile.UseVisualStyleBackColor = False
        '
        'tbGroupName
        '
        Me.tbGroupName.Location = New System.Drawing.Point(424, 275)
        Me.tbGroupName.Name = "tbGroupName"
        Me.tbGroupName.Size = New System.Drawing.Size(140, 20)
        Me.tbGroupName.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(332, 277)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(70, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Group Name:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(332, 304)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Ownership: "
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(332, 331)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Special Area:"
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(443, 371)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(84, 41)
        Me.Button8.TabIndex = 8
        Me.Button8.Text = "Add"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'rtbQueryDefs
        '
        Me.rtbQueryDefs.Font = New System.Drawing.Font("Lucida Console", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rtbQueryDefs.Location = New System.Drawing.Point(162, 463)
        Me.rtbQueryDefs.Name = "rtbQueryDefs"
        Me.rtbQueryDefs.ReadOnly = True
        Me.rtbQueryDefs.Size = New System.Drawing.Size(425, 284)
        Me.rtbQueryDefs.TabIndex = 0
        Me.rtbQueryDefs.TabStop = False
        Me.rtbQueryDefs.Text = ""
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(556, 423)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(61, 30)
        Me.Button9.TabIndex = 11
        Me.Button9.Text = "Delete"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'tbDeleteLine
        '
        Me.tbDeleteLine.Location = New System.Drawing.Point(556, 397)
        Me.tbDeleteLine.Name = "tbDeleteLine"
        Me.tbDeleteLine.Size = New System.Drawing.Size(71, 20)
        Me.tbDeleteLine.TabIndex = 10
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(559, 381)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Delete Line:"
        '
        'pbCaution
        '
        Me.pbCaution.BackColor = System.Drawing.SystemColors.Control
        Me.pbCaution.Image = CType(resources.GetObject("pbCaution.Image"), System.Drawing.Image)
        Me.pbCaution.Location = New System.Drawing.Point(374, 373)
        Me.pbCaution.Name = "pbCaution"
        Me.pbCaution.Size = New System.Drawing.Size(63, 86)
        Me.pbCaution.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pbCaution.TabIndex = 24
        Me.pbCaution.TabStop = False
        Me.pbCaution.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(41, 76)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(21, 24)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "1"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(8, 30)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(21, 24)
        Me.Label6.TabIndex = 26
        Me.Label6.Text = "2"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(41, 238)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(21, 24)
        Me.Label7.TabIndex = 27
        Me.Label7.Text = "3"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cbCheckAttributesallFile)
        Me.GroupBox1.Controls.Add(Me.cbCheckSIMPPLLEOutputs)
        Me.GroupBox1.Controls.Add(Me.cbCheckModelDefs)
        Me.GroupBox1.Controls.Add(Me.Button3)
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Location = New System.Drawing.Point(33, 135)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(368, 86)
        Me.GroupBox1.TabIndex = 28
        Me.GroupBox1.TabStop = False
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(158, 80)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(31, 27)
        Me.Button11.TabIndex = 27
        Me.Button11.TabStop = False
        Me.Button11.Text = "?"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(409, 149)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(236, 15)
        Me.Label8.TabIndex = 29
        Me.Label8.Text = "Optional: Update Special Area Field"
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(504, 167)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(51, 25)
        Me.Button10.TabIndex = 30
        Me.Button10.Text = "GO"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(240, 4)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(161, 24)
        Me.Label9.TabIndex = 26
        Me.Label9.Text = "QUERY SET-UP"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(41, 301)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(21, 24)
        Me.Label10.TabIndex = 31
        Me.Label10.Text = "4"
        '
        'Button12
        '
        Me.Button12.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Button12.Location = New System.Drawing.Point(68, 63)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(84, 51)
        Me.Button12.TabIndex = 28
        Me.Button12.Text = "Create Query Definition Files"
        Me.Button12.UseVisualStyleBackColor = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(30, 45)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(384, 15)
        Me.Label11.TabIndex = 32
        Me.Label11.Text = "Step 1 is Required if you have not previously set up a query"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(30, 117)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(488, 15)
        Me.Label12.TabIndex = 33
        Me.Label12.Text = "Steps 2-4 are optional. If not completed now, SOAP will determine defaults."
        '
        'Button13
        '
        Me.Button13.Location = New System.Drawing.Point(60, 676)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(69, 49)
        Me.Button13.TabIndex = 34
        Me.Button13.Text = "Finished"
        Me.Button13.UseVisualStyleBackColor = True
        '
        'Button14
        '
        Me.Button14.Location = New System.Drawing.Point(215, 82)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(51, 25)
        Me.Button14.TabIndex = 36
        Me.Button14.Text = "GO"
        Me.Button14.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(212, 64)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(286, 15)
        Me.Label13.TabIndex = 35
        Me.Label13.Text = "Create Some Calibration Queries (Optional)"
        '
        'cbSpecies
        '
        Me.cbSpecies.AutoSize = True
        Me.cbSpecies.Checked = True
        Me.cbSpecies.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbSpecies.Location = New System.Drawing.Point(284, 82)
        Me.cbSpecies.Name = "cbSpecies"
        Me.cbSpecies.Size = New System.Drawing.Size(64, 17)
        Me.cbSpecies.TabIndex = 37
        Me.cbSpecies.Text = "Species"
        Me.cbSpecies.UseVisualStyleBackColor = True
        '
        'cbSpeciesHTG
        '
        Me.cbSpeciesHTG.AutoSize = True
        Me.cbSpeciesHTG.Location = New System.Drawing.Point(284, 97)
        Me.cbSpeciesHTG.Name = "cbSpeciesHTG"
        Me.cbSpeciesHTG.Size = New System.Drawing.Size(104, 17)
        Me.cbSpeciesHTG.TabIndex = 38
        Me.cbSpeciesHTG.Text = "Species by HTG"
        Me.cbSpeciesHTG.UseVisualStyleBackColor = True
        '
        'Button15
        '
        Me.Button15.Location = New System.Drawing.Point(394, 82)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(31, 27)
        Me.Button15.TabIndex = 39
        Me.Button15.TabStop = False
        Me.Button15.Text = "?"
        Me.Button15.UseVisualStyleBackColor = True
        '
        'frmQuerySetUp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(655, 759)
        Me.Controls.Add(Me.Button15)
        Me.Controls.Add(Me.cbSpeciesHTG)
        Me.Controls.Add(Me.cbSpecies)
        Me.Controls.Add(Me.Button14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Button13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Button11)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.pbCaution)
        Me.Controls.Add(Me.tbDeleteLine)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.rtbQueryDefs)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tbGroupName)
        Me.Controls.Add(Me.btnWriteQueryGroupFile)
        Me.Controls.Add(Me.cbbSpecialArea)
        Me.Controls.Add(Me.clbGroupNames)
        Me.Controls.Add(Me.cbbOwnership)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button1)
        Me.Name = "frmQuerySetUp"
        Me.Text = "Query Set Up"
        CType(Me.pbCaution, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents cbCheckModelDefs As System.Windows.Forms.CheckBox
    Friend WithEvents cbCheckSIMPPLLEOutputs As System.Windows.Forms.CheckBox
    Friend WithEvents cbCheckAttributesallFile As System.Windows.Forms.CheckBox
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents cbbOwnership As System.Windows.Forms.ComboBox
    Friend WithEvents clbGroupNames As System.Windows.Forms.CheckedListBox
    Friend WithEvents cbbSpecialArea As System.Windows.Forms.ComboBox
    Friend WithEvents btnWriteQueryGroupFile As System.Windows.Forms.Button
    Friend WithEvents tbGroupName As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents rtbQueryDefs As System.Windows.Forms.RichTextBox
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents tbDeleteLine As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents pbCaution As System.Windows.Forms.PictureBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cbSpecies As System.Windows.Forms.CheckBox
    Friend WithEvents cbSpeciesHTG As System.Windows.Forms.CheckBox
    Friend WithEvents Button15 As System.Windows.Forms.Button
End Class
