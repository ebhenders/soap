Imports System.Text
Public Class VMAPtoSIMPPLLE

    Private Sub CompileButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CompileButton.Click
        Dim NIterations As Integer
        bKeepGoodCodes = cbKeepGoodCodes.Checked
        bIterate = cbIterate.Checked
        NIterations = 0
        NSaveIterations = 1
        If tbNIterations.Text <> "" Then
            NIterations = tbNIterations.Text
        End If
        If tbNSaveIterations.Text <> "" Then
            NSaveIterations = tbNSaveIterations.Text
        End If
        If NIterations = 0 Then
            bIterate = False
            cbIterate.Checked = False
        Else
            'check to make sure you are not saving more than you are iterating
            If NSaveIterations > NIterations Then NSaveIterations = NIterations
        End If

        FN_VMAPtoSIMPPLLE = tbVMAPtoSIMPPLLEMasterFile.Text
        RunVMAPtoSIMPPLLE(FN_VMAPtoSIMPPLLE, tbGoalTolerance.Text, NIterations)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'IDENTIFY THE FILE WITH TRANSLATION RULES
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select a model master file with definitions"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        tbVMAPtoSIMPPLLEMasterFile.Text = openfiledialog2.FileName
        openfiledialog2 = Nothing
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        MsgBox("Create Pathway Crosstable reads a list of pathway files and creates a matrix of HTGs (columns) and Species (rows) to be used by this VMAP to SIMPPLLE routine")
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'IDENTIFY THE FILE WITH TRANSLATION RULES
        Dim savefiledialog As New SaveFileDialog
        Dim outfilename As String

        With savefiledialog
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Save the Pathway File As:"
            .Filter = "Comma-delimited files(.csv) |*.csv"
            .FileName = "PathwayCrossTable"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        outfilename = savefiledialog.FileName
        savefiledialog = Nothing

        FillCombosUsedRunOutput(outfilename)
        MsgBox("File Saved To:" & Chr(13) & outfilename)
    End Sub

    Private Sub FillCombosUsedRunOutput(ByVal outfile As String)
        Dim dummy As String
        Dim arr() As String
        Dim arr2() As String
        Dim khtg As Integer
        Dim kspec As Integer

        Dim HTG As String
        Dim i As Integer
        Dim NFiles As Integer
        Dim FileName() As String
        Dim fnum As Integer
        Dim fnum2 As Integer
        Dim folder As String
        Dim UseCombo(,) As Byte

        Dim NHTGLUTq As Integer
        Dim HTGNameLUTq() As String
        Dim NSpeciesLUTq As Integer
        Dim SpeciesNameLUTq() As String
        Dim strb As StringBuilder


        'do a folderbrowser to get the folder with pathway files
        Dim folderdialog2 As New FolderBrowserDialog
        With folderdialog2
            '.SelectedPath = foldername
            .Description = "Folder that has exported .txt pathway files (1 per EcoGroup)"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Exit Sub
        End With

        folder = folderdialog2.SelectedPath

        Dim fldr As New System.IO.DirectoryInfo(folder)
        Dim files As System.IO.FileInfo() = fldr.GetFiles '("*EVU_SIM_DATA*")
        Dim fi As System.IO.FileInfo


        'initiate these arrays
        NHTGLUTq = 0
        ReDim HTGNameLUTq(NHTGLUTq)
        NSpeciesLUTq = 0
        ReDim SpeciesNameLUTq(NSpeciesLUTq)


        NFiles = 0
        ReDim FileName(NFiles)
        For Each fi In files
            i = i + 1
            If fi.Extension = ".txt" Then
                NFiles = NFiles + 1
                ReDim Preserve FileName(NFiles)
                FileName(NFiles) = fi.FullName
            End If
        Next

        'first identify all possible species, size, and density values from the pathway files
        For jfile As Integer = 1 To NFiles
            fnum = FreeFile()
            FileOpen(fnum, FileName(jfile), OpenMode.Input)
            'header line contains the habitat type group
            dummy = LineInput(fnum)
            arr = Split(dummy, " ")
            HTG = Trim(arr(1))
            khtg = GetStrIndex(HTGNameLUTq, HTG)
            If khtg < 0 Then
                NHTGLUTq = NHTGLUTq + 1
                ReDim Preserve HTGNameLUTq(NHTGLUTq)
                HTGNameLUTq(NHTGLUTq) = HTG
            End If
            Do Until EOF(fnum)
                dummy = Trim(LineInput(fnum))
                If dummy.Length > 0 Then
                    arr = Split(dummy, " ")
                    If arr(0) = "VEGETATIVE-TYPE" Then
                        arr2 = Split(arr(1), "/")
                        kspec = GetStrIndex(SpeciesNameLUTq, arr2(0))
                        If kspec < 0 Then
                            NSpeciesLUTq = NSpeciesLUTq + 1
                            ReDim Preserve SpeciesNameLUTq(NSpeciesLUTq)
                            SpeciesNameLUTq(NSpeciesLUTq) = arr2(0)
                        End If
                    End If
                End If
            Loop
            FileClose(fnum)
        Next
        Array.Sort(HTGNameLUTq)
        Array.Sort(SpeciesNameLUTq)

        'then, identify the pathway HTG/Species/Size/Density combinations that are in the model.

        ReDim UseCombo(NHTGLUTq, NSpeciesLUTq) ', NSizeClassLUTq, NDensityLUTq)
        For jfile As Integer = 1 To NFiles
            fnum = FreeFile()
            FileOpen(fnum, FileName(jfile), OpenMode.Input)
            'header line contains the habitat type group
            dummy = LineInput(fnum)
            arr = Split(dummy, " ")
            HTG = Trim(arr(1))
            khtg = GetStrIndex(HTGNameLUTq, HTG)

            Do Until EOF(fnum)
                dummy = Trim(LineInput(fnum))
                If dummy.Length > 0 Then
                    arr = Split(dummy, " ")
                    If arr(0) = "VEGETATIVE-TYPE" Then
                        arr2 = Split(arr(1), "/")
                        kspec = GetStrIndex(SpeciesNameLUTq, arr2(0))
                        UseCombo(khtg, kspec) = 1 ', ksize, kdensity) = 1
                    End If
                End If
            Loop
            FileClose(fnum)

        Next

        'write outputs
        fnum2 = FreeFile()
        FileOpen(fnum2, outfile, OpenMode.Output)
        PrintLine(fnum2, "HABITAT TYPE GROUPS")
        strb = New StringBuilder("", 200)
        For jhtg As Integer = 1 To NHTGLUTq
            strb.Append("," & HTGNameLUTq(jhtg))
        Next
        PrintLine(fnum2, strb.ToString)
        strb = Nothing
        For jspec As Integer = 1 To NSpeciesLUTq
            strb = New StringBuilder(SpeciesNameLUTq(jspec), 300)
            For jhtg As Integer = 1 To NHTGLUTq

                If UseCombo(jhtg, jspec) = 1 Then
                    strb.Append(",X")
                Else
                    strb.Append(",")
                End If

            Next jhtg
            PrintLine(fnum2, strb.ToString)
            strb = Nothing
        Next
        FileClose(fnum2)



    End Sub
End Class