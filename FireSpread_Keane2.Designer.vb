<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FireSpread_Keane2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button
        Me.tbTimeSteps = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.tbNFires = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.rbRandomStarts = New System.Windows.Forms.RadioButton
        Me.rbCellsToStart = New System.Windows.Forms.RadioButton
        Me.tbCellsToStart = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(156, 214)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(47, 37)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "GO"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'tbTimeSteps
        '
        Me.tbTimeSteps.Location = New System.Drawing.Point(158, 144)
        Me.tbTimeSteps.Name = "tbTimeSteps"
        Me.tbTimeSteps.Size = New System.Drawing.Size(51, 20)
        Me.tbTimeSteps.TabIndex = 1
        Me.tbTimeSteps.Text = "20"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(20, 147)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(110, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Timesteps to Simulate"
        '
        'tbNFires
        '
        Me.tbNFires.Location = New System.Drawing.Point(158, 82)
        Me.tbNFires.Name = "tbNFires"
        Me.tbNFires.Size = New System.Drawing.Size(51, 20)
        Me.tbNFires.TabIndex = 3
        Me.tbNFires.Text = "5"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(96, 35)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(180, 25)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Fires To Simulate"
        '
        'rbRandomStarts
        '
        Me.rbRandomStarts.AutoSize = True
        Me.rbRandomStarts.Checked = True
        Me.rbRandomStarts.Location = New System.Drawing.Point(23, 81)
        Me.rbRandomStarts.Name = "rbRandomStarts"
        Me.rbRandomStarts.Size = New System.Drawing.Size(114, 17)
        Me.rbRandomStarts.TabIndex = 5
        Me.rbRandomStarts.TabStop = True
        Me.rbRandomStarts.Text = "Random Starts (#):"
        Me.rbRandomStarts.UseVisualStyleBackColor = True
        '
        'rbCellsToStart
        '
        Me.rbCellsToStart.AutoSize = True
        Me.rbCellsToStart.Location = New System.Drawing.Point(23, 104)
        Me.rbCellsToStart.Name = "rbCellsToStart"
        Me.rbCellsToStart.Size = New System.Drawing.Size(126, 17)
        Me.rbCellsToStart.TabIndex = 7
        Me.rbCellsToStart.Text = "Cells to Start (cell #'s)"
        Me.rbCellsToStart.UseVisualStyleBackColor = True
        '
        'tbCellsToStart
        '
        Me.tbCellsToStart.Location = New System.Drawing.Point(158, 105)
        Me.tbCellsToStart.Name = "tbCellsToStart"
        Me.tbCellsToStart.Size = New System.Drawing.Size(265, 20)
        Me.tbCellsToStart.TabIndex = 6
        Me.tbCellsToStart.Text = "5"
        '
        'FireSpread_Keane
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(435, 302)
        Me.Controls.Add(Me.rbCellsToStart)
        Me.Controls.Add(Me.tbCellsToStart)
        Me.Controls.Add(Me.rbRandomStarts)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.tbNFires)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tbTimeSteps)
        Me.Controls.Add(Me.Button1)
        Me.Name = "FireSpread_Keane"
        Me.Text = "FireSpread Keane"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents tbTimeSteps As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tbNFires As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents rbRandomStarts As System.Windows.Forms.RadioButton
    Friend WithEvents rbCellsToStart As System.Windows.Forms.RadioButton
    Friend WithEvents tbCellsToStart As System.Windows.Forms.TextBox
End Class
