Public Class ProcessSIMPPLLEDisturbances

    Private Sub CompileButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CompileButton.Click
        Dim clProcess As New clProcessSimpplle
        clProcess.RunSummarizer(tbInitCondFiles.Text, tbBatchFile.Text)
        clProcess = Nothing
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        'IDENTIFY THE FILE WITH INFORMATION ABOUT THE SIMPPLLE OUTPUT FILES TO PROCESS
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select a file with SIMPPLLE output files to process"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        tbBatchFile.Text = openfiledialog2.FileName
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'IDENTIFY THE file OF INITIAL CONDITIONS file names
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select a file with initial condition file names"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        tbInitCondFiles.Text = openfiledialog2.FileName
    End Sub
End Class