Imports System.Text
Public Class PathwayEdit
    Dim foldername As String

    'Dim HTGNameLUTq() As String
    'Dim NHTGLUTq As Integer
    Dim SizeClassNameLUTq() As String
    Dim NSizeClassLUTq As Integer
    Dim DensityNameLUTq() As String
    Dim NDensityLUTq As Integer
    Dim HabGroupNameLUTq() As String
    Dim NHabGroupLUTq As Integer
    Dim SpeciesNameLUTq() As String
    Dim NSpeciesLUTq As Integer
    Dim LegalCombo(,,,) As Byte 'whether the htg/species/size/density is in the pathways
    'Dim DeadStart(,,,) As Byte 'whether the htg/species/size/density can be populated by succession (0) or is a dead start (1)

    Dim NSizeRules As Integer
    Dim NDensityRules As Integer
    Dim NSpeciesRules As Integer
    Dim NHTGRules As Integer

    Dim LegalSizeChange(,,,,) As Byte 'by rule, htg, species, from size, to size
    Dim LegalDensityChange(,,,,) As Byte 'by rule, htg, species, from density, to density
    Dim LegalSpeciesChange(,,,) As Byte 'by rule, htg, from species, to species
    Dim LegalHTGChange(,,,) As Byte 'by rule, from htg, tohtg, species

    Dim ForceSizeChange() As Integer 'by SizeChangeRules
    Dim ForceDensityChange() As Integer 'by DensityChangeRules
    Dim ForceSpeciesChange() As Integer 'by SpeciesChangeRules
    Dim ForceHTGChange() As Integer 'by HTGChangeRules

    Dim FromPathwaysAvailable() As String
    Dim NFromPathwaysAvailable As Integer
    Dim NPathwaysToCopy As Integer
    Dim PathwaysToCopy() As String
    Dim NPathwaysToDelete As Integer
    Dim PathwaysToDelete() As String

    Dim ToPathwaysAvailable() As String
    Dim NToPathwaysAvailable As Integer
    Dim PathwayToPaste() As String

    Dim NStates As Integer 'number of unique states in the pathways
    Dim FromState() As String 'by NStates - full htg/species/size/density name
    Dim DeadStartState() As Byte
    Dim SuccToState() As String 'by NStates - full htg/species/size/density name

    Dim fieldLUTfile As String 'look-up table with fields to use to define species/size/density
    Dim legalchangesfile As String 'rules to test/try when reassigning attributes
    Dim pathwayFolder As String 'folder directory where pathway .txt files are stored



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click


        'need to choose two files:
        '1. attributesall file to re-assign
        '2. list of legal changes to make
        'Dim legalchangesfile As String
        Dim attributesallfile As String
        Dim rawdatafile As String
        'Dim fieldLUTfile As String
        Dim Openfiledialog1 As New OpenFileDialog
        Dim FieldIndices() As Integer

        If rbAttributesall.Checked Then
            With Openfiledialog1
                .Title = ".attributesall file to check"
                .Filter = "Text File (*.attributesall)|*.attributesall"
                If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
            End With

            attributesallfile = Openfiledialog1.FileName
            foldername = DirName(attributesallfile)
        Else
            With Openfiledialog1
                .Title = "Comma Separated Text raw input file to check"
                .Filter = "Text File (*.txt)|*.txt"
                If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
            End With

            rawdatafile = Openfiledialog1.FileName
            foldername = DirName(rawdatafile)

            With Openfiledialog1
                .Title = "Field look-up file for SIMPPLLE fields"
                .Filter = "Text File (*.txt)|*.txt"
                If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
            End With
            fieldLUTfile = Openfiledialog1.FileName
        End If


        With Openfiledialog1
            .Title = "File with list of Changes to make"
            .Filter = "Text File (*.csv)|*.csv"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        legalchangesfile = Openfiledialog1.FileName
        Openfiledialog1 = Nothing

        'also need a directory with the pathway information
        FillLegalPathwayCombosA(legalchangesfile) 'now includes FillLegalChanges
        'FillLegalChanges(legalchangesfile)

        If rbAttributesall.Checked Then
            FillFieldIndicesAttributesall(FieldIndices)
            ChangeIllegals(attributesallfile, FieldIndices)
        Else
            FillFieldIndicesRawData(FieldIndices, rawdatafile, fieldLUTfile)
            ChangeIllegals(rawdatafile, FieldIndices)
        End If



        MsgBox("Done. See RunStats.txt for list of changes.")

    End Sub
    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        'need to choose file:
        '1. attributesall file to re-assign


        Dim rawdatafile As String
        Dim fieldLUTfile As String
        Dim Openfiledialog1 As New OpenFileDialog
        Dim FieldIndices() As Integer


        With Openfiledialog1
            .Title = "Comma Separated Text raw input file to check"
            .Filter = "Text File (*.txt)|*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        rawdatafile = Openfiledialog1.FileName
        foldername = DirName(rawdatafile)

        With Openfiledialog1
            .Title = "Field look-up file for SIMPPLLE fields. These are the fields used to create the .attributesall/.spatialrelate"
            .Filter = "Text File (*.txt)|*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        fieldLUTfile = Openfiledialog1.FileName


        'also need a directory with the pathway information
        FillLegalPathwayCombos()

        FillFieldIndicesRawData(FieldIndices, rawdatafile, fieldLUTfile)
        ListIllegals(rawdatafile, FieldIndices)

        MsgBox("Done")
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'SELECT THE PATHWAY TO COPY 
        'SELECT where to paste
        'describe what to paste-as
        Dim copyfromfile As String
        Dim copytofile As String
        Dim Openfiledialog1 As New OpenFileDialog
        With Openfiledialog1
            .Title = "File to copy the pathway FROM"
            .Filter = "Text File (*.txt)|*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        copyfromfile = Openfiledialog1.FileName

        With Openfiledialog1
            .Title = "File to copy the pathway INTO"
            .Filter = "Text File (*.txt)|*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        copytofile = Openfiledialog1.FileName

        Openfiledialog1 = Nothing
        CopyPathway(copyfromfile, copytofile)

        MsgBox("Done")



    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'SELECT THE PATHWAY TO DELETE 
        'SELECT where to paste
        'describe what to paste-as
        Dim deletefromfile As String
        'Dim copytofile As String
        Dim Openfiledialog1 As New OpenFileDialog
        With Openfiledialog1
            .Title = "File to DELETE the pathway FROM"
            .Filter = "Text File (*.txt)|*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        deletefromfile = Openfiledialog1.FileName

        'With Openfiledialog1
        '    .Title = "File to copy the pathway INTO"
        '    .Filter = "Text File (*.txt)|*.txt"
        '    If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        'End With
        'copytofile = Openfiledialog1.FileName

        Openfiledialog1 = Nothing
        DeletePathway(deletefromfile)

        MsgBox("Done")
    End Sub

    Private Sub FillFieldIndicesAttributesall(ByRef FieldIndices() As Integer)
        'FieldIndices(1) = HTG Field Index
        'FieldIndices(2) = Species Field Index
        'FieldIndices(3) = Size Field Index
        'FieldIndices(4) = Density Field Index
        ReDim FieldIndices(4)
        FieldIndices(1) = 5
        FieldIndices(2) = 8
        FieldIndices(3) = 9
        FieldIndices(4) = 10

    End Sub
    Private Sub FillFieldIndicesRawData(ByRef FieldIndices() As Integer, ByVal RawDataFile As String, ByVal FieldLUTFile As String)
        'FieldIndices(1) = HTG Field Index
        'FieldIndices(2) = Species Field Index
        'FieldIndices(3) = Size Field Index
        'FieldIndices(4) = Density Field Index
        ReDim FieldIndices(4)

        On Error GoTo ErrorHandler

        Dim dummy As String
        Dim fieldnames As String
        Dim arr() As String

        Dim fnum2 As Integer = FreeFile()
        FileOpen(fnum2, RawDataFile, OpenMode.Input)
        fieldnames = LineInput(fnum2) 'header row
        FileClose(fnum2)

        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, FieldLUTFile, OpenMode.Input)
        dummy = LineInput(fnum)

        '        Ecological(stratification, HT_GRP)
        arr = Split(LineInput(fnum), ",")
        FieldIndices(1) = StringInt(Trim(arr(1)), fieldnames)
        '        Acres, Acres
        dummy = LineInput(fnum)
        'arr = Split(LineInput(fnum), ",")
        'FieldIndices(5) = StringInt(Trim(arr(1)), fieldnames)
        'ACRES_Field = StringInt(Trim(arr(1)), fieldnames)
        '        Stand(ID, Stand_ID)
        dummy = LineInput(fnum)
        'arr = Split(LineInput(fnum), ",")
        'STAND_ID_Field = StringInt(Trim(arr(1)), fieldnames)
        '        Ownership, Ownership
        dummy = LineInput(fnum)
        'arr = Split(LineInput(fnum), ",")
        'Ownership_Field = StringInt(Trim(arr(1)), fieldnames)
        '        Roads, NA
        dummy = LineInput(fnum)
        'arr = Split(LineInput(fnum), ",")
        'Roads_Field = StringInt(Trim(arr(1)), fieldnames)
        '        Elevation, ELEV
        dummy = LineInput(fnum)
        'arr = Split(LineInput(fnum), ",")
        'Elevation_Field = StringInt(Trim(arr(1)), fieldnames)
        'Fire Management Zone,FMZ
        dummy = LineInput(fnum)
        'arr = Split(LineInput(fnum), ",")
        'Fire_Management_Zone_Field = StringInt(Trim(arr(1)), fieldnames)
        '        Special(Area, SP_AREA)
        dummy = LineInput(fnum)
        'arr = Split(LineInput(fnum), ",")
        'Special_Area_Field = StringInt(Trim(arr(1)), fieldnames)
        '        Species, SPECIES
        arr = Split(LineInput(fnum), ",")
        FieldIndices(2) = StringInt(Trim(arr(1)), fieldnames)
        'Size Class,SIZE_CLASS
        arr = Split(LineInput(fnum), ",")
        FieldIndices(3) = StringInt(Trim(arr(1)), fieldnames)
        'Density (Canopy Cover %),DENSITY
        arr = Split(LineInput(fnum), ",")
        FieldIndices(4) = StringInt(Trim(arr(1)), fieldnames)


        FileClose(fnum)
        Exit Sub

ErrorHandler:
        FileClose(fnum)

        MsgBox("Error reading field names file")

    End Sub
    Private Sub ChangeIllegals(ByVal infile As String, ByVal FieldIndices() As Integer)
        'goes through an attributesall file, looks for illegal combinations, checks for legal reassignment. Performs the re-do, tracks and reports reassignments and infeasibilities.
        Dim fnum As Integer
        Dim ofnum As Integer 'output file
        Dim dummy As String
        Dim pdummy As String
        Dim header As String
        'Dim odummy As String
        Dim arr() As String
        Dim kspec As Integer
        Dim khtg As Integer
        Dim ksize As Integer
        Dim kdensity As Integer
        Dim NInfeasibilities As Integer
        Dim Infeasibility() As String
        Dim NChanges As Integer
        Dim NChangesTotal As Integer
        Dim NChangesLine As Integer
        Dim kredim As Integer = 5000
        Dim bchanged As Boolean
        Dim bprinted As Boolean
        'Dim bforcedchange As Boolean
        Dim nForcedChange As Integer
        Dim kline As Integer
        Dim NIllegalCombos(,,,) As Integer 'by htg,species,size,density
        'Dim thtg As Integer
        'Dim tspec As Integer
        'Dim tsize As Integer
        'Dim tdensity As Integer

        Dim strb As StringBuilder

        NChanges = 0
        NChangesTotal = 0
        kline = 0
        NChangesLine = 0
        NInfeasibilities = 0
        ReDim Infeasibility(kredim)
        ReDim NIllegalCombos(NHabGroupLUTq, NSpeciesLUTq, NSizeClassLUTq, NDensityLUTq)

        fnum = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        ofnum = FreeFile()
        FileOpen(ofnum, infile & ".out", OpenMode.Output)
        header = LineInput(fnum) 'header line
        PrintLine(ofnum, header)
        Do Until EOF(fnum)
            kline += 1
            NChangesLine = 0
            bchanged = False
            bprinted = False
            dummy = LineInput(fnum)
            If Trim(dummy) = "END" Then Exit Do
            arr = Split(dummy, ",")
            'If arr(0) = 55 Then Stop
            khtg = GetStrIndex(HabGroupNameLUTq, arr(FieldIndices(1)))
            kspec = GetStrIndex(SpeciesNameLUTq, arr(FieldIndices(2)))
            ksize = GetStrIndex(SizeClassNameLUTq, arr(FieldIndices(3)))
            kdensity = GetStrIndex(DensityNameLUTq, arr(FieldIndices(4)))

            khtg = Math.Max(0, khtg)
            kspec = Math.Max(0, kspec)
            ksize = Math.Max(0, ksize)
            kdensity = Math.Max(0, kdensity)
            'If arr(FieldIndices(2)) = "AF-WB" Then Stop
            'If khtg > 0 And kspec > 0 And ksize > 0 And kdensity > 0 Then
            If LegalCombo(khtg, kspec, ksize, kdensity) = 1 Then
                PrintLine(ofnum, dummy)
                bchanged = True
                bprinted = True
            Else ' you have something illegal - search for a change
                nForcedChange = 1
                Do Until nForcedChange = 0
                    nForcedChange = 0
                    'first, search through the HTG change rules
                    For jrule As Integer = 1 To NHTGRules
                        'For jfromsize As Integer = 1 To NSizeClassLUTq
                        For jtohtg As Integer = 1 To NHabGroupLUTq

                            'has to be both a legal change and a change to something that is feasible
                            If LegalHTGChange(jrule, khtg, jtohtg, kspec) = 1 And LegalCombo(jtohtg, kspec, ksize, kdensity) = 1 Then
                                bchanged = True
                                NChanges = NChanges + 1
                                NChangesTotal += 1
                                NChangesLine += 1
                                nForcedChange = 0
                                khtg = jtohtg

                                PrintNewLine(ofnum, arr, khtg, kspec, ksize, kdensity, FieldIndices, dummy)

                                bprinted = True
                                Exit For
                            End If
                            'is this a forced change?
                            If ForceHTGChange(jrule) = 1 And LegalHTGChange(jrule, khtg, jtohtg, kspec) = 1 And khtg <> jtohtg Then
                                khtg = jtohtg
                                nForcedChange += 1
                                NChangesTotal += 1
                                NChangesLine += 1
                                Exit For
                            End If
                        Next
                        If bchanged = True Then Exit For
                        'Next
                    Next jrule

                    'THEN, search through the SPECIES change rules
                    If bchanged = False Then
                        For jrule As Integer = 1 To NSpeciesRules
                            For jtospec As Integer = 1 To NSpeciesLUTq

                                'has to be both a legal change and a change to something that is feasible
                                If LegalSpeciesChange(jrule, khtg, kspec, jtospec) = 1 And LegalCombo(khtg, jtospec, ksize, kdensity) = 1 Then
                                    bchanged = True
                                    NChanges = NChanges + 1
                                    NChangesTotal += 1
                                    NChangesLine += 1
                                    nForcedChange = 0
                                    kspec = jtospec
                                    PrintNewLine(ofnum, arr, khtg, kspec, ksize, kdensity, FieldIndices, dummy)

                                    bprinted = True
                                    Exit For
                                End If
                                'is this a forced change?
                                If ForceSpeciesChange(jrule) = 1 And LegalSpeciesChange(jrule, khtg, kspec, jtospec) = 1 And kspec <> jtospec Then
                                    kspec = jtospec
                                    nForcedChange += 1
                                    NChangesTotal += 1
                                    NChangesLine += 1
                                    Exit For
                                End If
                            Next
                            If bchanged = True Then Exit For
                            'Next
                        Next jrule
                    End If


                    'next search for legal size changes
                    If bchanged = False Then
                        For jrule As Integer = 1 To NSizeRules
                            For jtosize As Integer = 1 To NSizeClassLUTq

                                'has to be both a legal change and a change to something that is feasible
                                If LegalSizeChange(jrule, khtg, kspec, ksize, jtosize) = 1 And LegalCombo(khtg, kspec, jtosize, kdensity) = 1 Then
                                    bchanged = True
                                    NChanges = NChanges + 1
                                    NChangesTotal += 1
                                    NChangesLine += 1
                                    nForcedChange = 0
                                    ksize = jtosize
                                    PrintNewLine(ofnum, arr, khtg, kspec, ksize, kdensity, FieldIndices, dummy)

                                    bprinted = True
                                    Exit For
                                End If
                                'is this a forced change?
                                If ForceSizeChange(jrule) = 1 And LegalSizeChange(jrule, khtg, kspec, ksize, jtosize) = 1 And ksize <> jtosize Then
                                    ksize = jtosize
                                    nForcedChange += 1
                                    NChangesTotal += 1
                                    NChangesLine += 1
                                    Exit For
                                End If
                            Next
                            If bchanged = True Then Exit For
                            'Next
                        Next jrule
                    End If


                    'if you get to here,  you have not identified a change in size that results in a legal value
                    If bchanged = False Then
                        For jrule As Integer = 1 To NDensityRules
                            'is this a forced change?

                            For jtodensity As Integer = 1 To NDensityLUTq
                                If LegalDensityChange(jrule, khtg, kspec, kdensity, jtodensity) = 1 And LegalCombo(khtg, kspec, ksize, jtodensity) Then
                                    bchanged = True
                                    kdensity = jtodensity
                                    NChanges = NChanges + 1
                                    NChangesTotal += 1
                                    NChangesLine += 1
                                    nForcedChange = 0
                                    PrintNewLine(ofnum, arr, khtg, kspec, ksize, kdensity, FieldIndices, dummy)

                                    bprinted = True
                                    Exit For
                                End If
                                If ForceDensityChange(jrule) = 1 Then
                                    If LegalDensityChange(jrule, khtg, kspec, kdensity, jtodensity) = 1 And kdensity <> jtodensity Then
                                        kdensity = jtodensity
                                        nForcedChange += 1
                                        NChangesTotal += 1
                                        NChangesLine += 1
                                        Exit For
                                    End If
                                End If
                            Next jtodensity
                            If bchanged = True Then Exit For
                        Next jrule
                    End If
                Loop

                'check to see if the line printed
                If bprinted = False Then
                    PrintNewLine(ofnum, arr, khtg, kspec, ksize, kdensity, FieldIndices, dummy)

                    bprinted = True

                    If LegalCombo(khtg, kspec, ksize, kdensity) = 1 Then
                        bchanged = True 'mandatory reassignments have made this a good combo and not illegal anymore.
                        NChanges = NChanges + 1
                        NChangesTotal += 1
                        NChangesLine += 1
                    End If

                End If
            End If

            If bchanged = False Then
                NInfeasibilities = NInfeasibilities + 1
                If NInfeasibilities > Infeasibility.Length - 1 Then
                    ReDim Preserve Infeasibility(Infeasibility.Length + kredim)
                End If

                ' ''arr = Split(dummy, ",") 'update this if need-be
                ' ''khtg = GetStrIndex(HabGroupNameLUTq, arr(FieldIndices(1)))
                ' ''kspec = GetStrIndex(SpeciesNameLUTq, arr(FieldIndices(2)))
                ' ''ksize = GetStrIndex(SizeClassNameLUTq, arr(FieldIndices(3)))
                ' ''kdensity = GetStrIndex(DensityNameLUTq, arr(FieldIndices(4)))

                ' ''NIllegalCombos(khtg, kspec, ksize, kdensity) += 1

                Infeasibility(NInfeasibilities) = arr(0) & "," & arr(FieldIndices(1)) & "," & arr(FieldIndices(2)) & "," & arr(FieldIndices(3)) & "," & arr(FieldIndices(4)) & "," & NChangesLine & ",----" & dummy 'the line

                If bprinted = False Then PrintLine(ofnum, dummy)
            End If


        Loop

        FileClose(fnum)
        FileClose(ofnum)

        System.IO.File.Copy(infile, infile & ".bak", True)
        System.IO.File.Copy(infile & ".out", infile, True)
        System.IO.File.Delete(infile & ".out")
        'write output stats 
        fnum = FreeFile()
        FileOpen(fnum, DirName(infile) & "RunStats_ChangesMade.txt", OpenMode.Output)
        PrintLine(fnum, "Output statics for cleaning " & infile)
        PrintLine(fnum, "Change Rules:       " & legalchangesfile)
        PrintLine(fnum, "Fields With Values: " & fieldLUTFile)
        PrintLine(fnum, "Pathways Used:      " & pathwayFolder)
        PrintLine(fnum, "")
        PrintLine(fnum, "Successful Lines  : " & NChanges)
        PrintLine(fnum, "")
        PrintLine(fnum, "Total Changes     : " & NChangesTotal)
        PrintLine(fnum, "")
        PrintLine(fnum, "Unsuccessful lines: " & NInfeasibilities)
        PrintLine(fnum, "SLINK,HTG,SPECIES,SIZE,DENSITY,CHANGES_THIS_ITERATION,----" & header)
        For jinf As Integer = 1 To NInfeasibilities
            PrintLine(fnum, Infeasibility(jinf))
        Next
        FileClose(fnum)

        '' ''write output stats 
        ' ''fnum = FreeFile()
        ' ''FileOpen(fnum, DirName(infile) & "RunStats_Num_IllegalCombos.txt", OpenMode.Output)
        ' ''PrintLine(fnum, "Unique Illegal Combinations for " & infile)
        ' ''PrintLine(fnum, "")
        ' ''PrintLine(fnum, "NIllegalRecords,HTG,SPECIES,SIZE,DENSITY")
        '' '' ReDim NIllegalCombos(NHabGroupLUTq, NSpeciesLUTq, NSizeClassLUTq, NDensityLUTq)
        ' ''For jhtg As Integer = 1 To NHabGroupLUTq
        ' ''    For jspec As Integer = 1 To NSpeciesLUTq
        ' ''        For jsize As Integer = 1 To NSizeClassLUTq
        ' ''            For jdensity As Integer = 1 To NDensityLUTq
        ' ''                If NIllegalCombos(jhtg, jspec, jsize, jdensity) > 0 Then
        ' ''                    pdummy = NIllegalCombos(jhtg, jspec, jsize, jdensity) & "," & HabGroupNameLUTq(jhtg) & "," & SpeciesNameLUTq(jspec) & "," & SizeClassNameLUTq(jsize) & "," & DensityNameLUTq(jdensity)
        ' ''                    PrintLine(fnum, pdummy)
        ' ''                End If
        ' ''            Next
        ' ''        Next
        ' ''    Next
        ' ''Next
        ' ''FileClose(fnum)
    End Sub
    Private Sub ListIllegals(ByVal infile As String, ByVal FieldIndices() As Integer)
        'goes through an attributesall file, looks for illegal combinations, reports infeasibilities.
        Dim fnum As Integer
        'Dim ofnum As Integer 'output file
        Dim dummy As String
        'Dim odummy As String
        Dim arr() As String
        Dim kspec As Integer
        Dim khtg As Integer
        Dim ksize As Integer
        Dim kdensity As Integer
        Dim NInfeasibilities As Integer
        Dim Infeasibility() As String
        Dim NChanges As Integer
        Dim kredim As Integer = 1000
        Dim bchanged As Boolean

        NChanges = 0
        NInfeasibilities = 0
        ReDim Infeasibility(kredim)

        fnum = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        'ofnum = FreeFile()
        'FileOpen(ofnum, infile & ".out", OpenMode.Output)
        dummy = LineInput(fnum) 'header line
        'PrintLine(ofnum, dummy)
        Do Until EOF(fnum)
            bchanged = False
            dummy = LineInput(fnum)
            If Trim(dummy) = "END" Then Exit Do
            arr = Split(dummy, ",")
            'If arr(0) = 43993 Then Stop
            khtg = GetStrIndex(HabGroupNameLUTq, arr(FieldIndices(1)))
            kspec = GetStrIndex(SpeciesNameLUTq, arr(FieldIndices(2)))
            ksize = GetStrIndex(SizeClassNameLUTq, arr(FieldIndices(3)))
            kdensity = GetStrIndex(DensityNameLUTq, arr(FieldIndices(4)))
            If khtg > 0 And kspec > 0 And ksize > 0 And kdensity > 0 Then
                If LegalCombo(khtg, kspec, ksize, kdensity) = 1 Then
                    'PrintLine(ofnum, dummy)
                    bchanged = True
                End If

            End If

            If bchanged = False Then
                NInfeasibilities = NInfeasibilities + 1
                If NInfeasibilities > Infeasibility.Length - 1 Then
                    ReDim Preserve Infeasibility(Infeasibility.Length + kredim)
                End If
                Infeasibility(NInfeasibilities) = arr(0) & "," & arr(FieldIndices(1)) & "," & arr(FieldIndices(2)) & "," & arr(FieldIndices(3)) & "," & arr(FieldIndices(4)) 'dummy 'the line
                ' PrintLine(ofnum, dummy)
            End If


        Loop

        FileClose(fnum)
        'FileClose(ofnum)

        'System.IO.File.Copy(infile, infile & ".bak", True)
        'System.IO.File.Copy(infile & ".out", infile, True)
        'System.IO.File.Delete(infile & ".out")
        'write output stats
        fnum = FreeFile()
        FileOpen(fnum, DirName(infile) & "RunStats_Illegal_Combos.txt", OpenMode.Output)
        PrintLine(fnum, "Output statics for checking " & infile)
        PrintLine(fnum, "")
        'PrintLine(fnum, "Successful Changes: " & NChanges)
        'PrintLine(fnum, "")
        PrintLine(fnum, "Unsuccessful lines:" & NInfeasibilities)
        PrintLine(fnum, "SLINK,HTG,SPECIES,SIZE,DENSITY")
        For jinf As Integer = 1 To NInfeasibilities
            PrintLine(fnum, Infeasibility(jinf))
        Next
        FileClose(fnum)
    End Sub
    Private Sub FillLegalPathwayCombos()

        Dim dummy As String
        Dim arr() As String
        Dim arr2() As String
        Dim kspec As Integer
        Dim ksize As Integer
        Dim kdensity As Integer
        Dim khtg As Integer
        Dim HTG As String
        Dim i As Integer
        Dim NFiles As Integer
        Dim FileName() As String
        Dim fnum As Integer
        Dim folder As String

        'do a folderbrowser to get the folder with pathway files
        Dim folderdialog2 As New FolderBrowserDialog
        With folderdialog2
            .SelectedPath = foldername
            .Description = "Folder that has exported .txt pathway files (1 per EcoGroup)"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Exit Sub
        End With

        folder = folderdialog2.SelectedPath

        Dim fldr As New System.IO.DirectoryInfo(folder)
        Dim files As System.IO.FileInfo() = fldr.GetFiles '("*EVU_SIM_DATA*")
        Dim fi As System.IO.FileInfo

        'initiate these arrays
        NSizeClassLUTq = 0
        ReDim SizeClassNameLUTq(NSizeClassLUTq)
        SizeClassNameLUTq(0) = "" 'have to give it a value...
        NDensityLUTq = 0
        ReDim DensityNameLUTq(NDensityLUTq)
        NHabGroupLUTq = 0
        ReDim HabGroupNameLUTq(NHabGroupLUTq)
        NSpeciesLUTq = 0
        ReDim SpeciesNameLUTq(NSpeciesLUTq)

        NFiles = 0
        ReDim FileName(NFiles)
        For Each fi In files
            i = i + 1
            If fi.Extension = ".txt" Then
                NFiles = NFiles + 1
                ReDim Preserve FileName(NFiles)
                FileName(NFiles) = fi.FullName
            End If
        Next
        NHabGroupLUTq = NFiles
        ReDim HabGroupNameLUTq(NHabGroupLUTq)

        'first identify all possible species, size, and density values from the pathway files
        For jfile As Integer = 1 To NFiles
            fnum = FreeFile()
            FileOpen(fnum, FileName(jfile), OpenMode.Input)
            'header line contains the habitat type group
            dummy = LineInput(fnum)
            arr = Split(dummy, " ")
            HTG = Trim(arr(1))
            HabGroupNameLUTq(jfile) = HTG
            Do Until EOF(fnum)
                dummy = Trim(LineInput(fnum))
                If dummy.Length > 0 Then
                    arr = Split(dummy, " ")
                    If arr(0) = "VEGETATIVE-TYPE" Then
                        arr2 = Split(arr(1), "/")
                        kspec = GetStrIndex(SpeciesNameLUTq, arr2(0))
                        If kspec < 0 Then
                            NSpeciesLUTq = NSpeciesLUTq + 1
                            ReDim Preserve SpeciesNameLUTq(NSpeciesLUTq)
                            SpeciesNameLUTq(NSpeciesLUTq) = arr2(0)
                        End If

                        ksize = GetSizeInd(SizeClassNameLUTq, arr2(1))
                        If ksize < 0 Then
                            NSizeClassLUTq = NSizeClassLUTq + 1
                            ReDim Preserve SizeClassNameLUTq(NSizeClassLUTq)
                            SizeClassNameLUTq(NSizeClassLUTq) = arr2(1)
                        End If

                        kdensity = GetStrIndex(DensityNameLUTq, arr2(2))
                        If kdensity < 0 Then
                            NDensityLUTq = NDensityLUTq + 1
                            ReDim Preserve DensityNameLUTq(NDensityLUTq)
                            DensityNameLUTq(NDensityLUTq) = arr2(2)
                        End If
                    End If
                End If
            Loop
            FileClose(fnum)
        Next

        'then, identify the pathway HTG/Species/Size/Density combinations that are in the model.
        ReDim LegalCombo(NHabGroupLUTq, NSpeciesLUTq, NSizeClassLUTq, NDensityLUTq)
        For jfile As Integer = 1 To NFiles
            fnum = FreeFile()
            FileOpen(fnum, FileName(jfile), OpenMode.Input)
            'header line contains the habitat type group
            dummy = LineInput(fnum)
            arr = Split(dummy, " ")
            HTG = Trim(arr(1))
            khtg = jfile

            Do Until EOF(fnum)
                dummy = Trim(LineInput(fnum))
                If dummy.Length > 0 Then
                    arr = Split(dummy, " ")
                    If arr(0) = "VEGETATIVE-TYPE" Then
                        'If HTG = "B2" And arr(1) = "PP-DF/VERY-LARGE/2" Then Stop
                        arr2 = Split(arr(1), "/")
                        kspec = GetStrIndex(SpeciesNameLUTq, arr2(0))
                        ksize = GetSizeInd(SizeClassNameLUTq, arr2(1))
                        kdensity = GetStrIndex(DensityNameLUTq, arr2(2))
                        LegalCombo(khtg, kspec, ksize, kdensity) = 1
                    End If
                End If
            Loop
            FileClose(fnum)

        Next


    End Sub
    Private Sub FillLegalPathwayCombosA(ByVal infile As String)

        Dim dummy As String
        Dim arr() As String
        Dim arr2() As String
        Dim kspec As Integer
        Dim ksize As Integer
        Dim kdensity As Integer
        Dim khtg As Integer
        Dim HTG As String
        Dim i As Integer
        Dim NFiles As Integer
        Dim FileName() As String
        Dim fnum As Integer
        'Dim folder As String

        'do a folderbrowser to get the folder with pathway files
        Dim folderdialog2 As New FolderBrowserDialog
        With folderdialog2
            .SelectedPath = foldername
            .Description = "Folder that has exported .txt pathway files (1 per EcoGroup)"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Exit Sub
        End With

        pathwayFolder = folderdialog2.SelectedPath

        Dim fldr As New System.IO.DirectoryInfo(pathwayFolder)
        Dim files As System.IO.FileInfo() = fldr.GetFiles '("*EVU_SIM_DATA*")
        Dim fi As System.IO.FileInfo

        'initiate these arrays
        NSizeClassLUTq = 0
        ReDim SizeClassNameLUTq(NSizeClassLUTq)
        SizeClassNameLUTq(0) = "" 'have to give it a value...
        NDensityLUTq = 0
        ReDim DensityNameLUTq(NDensityLUTq)
        NHabGroupLUTq = 0
        ReDim HabGroupNameLUTq(NHabGroupLUTq)
        NSpeciesLUTq = 0
        ReDim SpeciesNameLUTq(NSpeciesLUTq)

        NFiles = 0
        ReDim FileName(NFiles)
        For Each fi In files
            i = i + 1
            If fi.Extension = ".txt" Then
                NFiles = NFiles + 1
                ReDim Preserve FileName(NFiles)
                FileName(NFiles) = fi.FullName
            End If
        Next
        NHabGroupLUTq = NFiles
        ReDim HabGroupNameLUTq(NHabGroupLUTq)

        'first identify all possible species, size, and density values from the pathway files
        For jfile As Integer = 1 To NFiles
            fnum = FreeFile()
            FileOpen(fnum, FileName(jfile), OpenMode.Input)
            'header line contains the habitat type group
            dummy = LineInput(fnum)
            arr = Split(dummy, " ")
            HTG = Trim(arr(1))
            HabGroupNameLUTq(jfile) = HTG
            Do Until EOF(fnum)
                dummy = Trim(LineInput(fnum))
                If dummy.Length > 0 Then
                    arr = Split(dummy, " ")
                    If arr(0) = "VEGETATIVE-TYPE" Then
                        arr2 = Split(arr(1), "/")
                        kspec = GetStrIndex(SpeciesNameLUTq, arr2(0))
                        If kspec < 0 Then
                            NSpeciesLUTq = NSpeciesLUTq + 1
                            ReDim Preserve SpeciesNameLUTq(NSpeciesLUTq)
                            SpeciesNameLUTq(NSpeciesLUTq) = arr2(0)
                        End If

                        ksize = GetSizeInd(SizeClassNameLUTq, arr2(1))
                        If ksize < 0 Then
                            NSizeClassLUTq = NSizeClassLUTq + 1
                            ReDim Preserve SizeClassNameLUTq(NSizeClassLUTq)
                            SizeClassNameLUTq(NSizeClassLUTq) = arr2(1)
                        End If

                        kdensity = GetStrIndex(DensityNameLUTq, arr2(2))
                        If kdensity < 0 Then
                            NDensityLUTq = NDensityLUTq + 1
                            ReDim Preserve DensityNameLUTq(NDensityLUTq)
                            DensityNameLUTq(NDensityLUTq) = arr2(2)
                        End If
                    End If
                End If
            Loop
            FileClose(fnum)
        Next

        'see if there are any additional hab groups or species to change
        FillLegalChanges(infile)

        'then, identify the pathway HTG/Species/Size/Density combinations that are in the model.
        ReDim LegalCombo(NHabGroupLUTq, NSpeciesLUTq, NSizeClassLUTq, NDensityLUTq)
        For jfile As Integer = 1 To NFiles
            fnum = FreeFile()
            FileOpen(fnum, FileName(jfile), OpenMode.Input)
            'header line contains the habitat type group
            dummy = LineInput(fnum)
            arr = Split(dummy, " ")
            HTG = Trim(arr(1))
            khtg = jfile

            Do Until EOF(fnum)
                dummy = Trim(LineInput(fnum))
                If dummy.Length > 0 Then
                    arr = Split(dummy, " ")
                    If arr(0) = "VEGETATIVE-TYPE" Then
                        'If HTG = "B2" And arr(1) = "PP-DF/VERY-LARGE/2" Then Stop
                        arr2 = Split(arr(1), "/")
                        kspec = GetStrIndex(SpeciesNameLUTq, arr2(0))
                        ksize = GetSizeInd(SizeClassNameLUTq, arr2(1))
                        kdensity = GetStrIndex(DensityNameLUTq, arr2(2))
                        LegalCombo(khtg, kspec, ksize, kdensity) = 1
                    End If
                End If
            Loop
            FileClose(fnum)

        Next


    End Sub
    Private Sub FillLegalChanges(ByVal infile As String)
        'reads a file that shows the HTG, SPECIES, type of switch that's legal and the from -> to switch that can happen
        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String
        Dim kspec As Integer
        Dim khtg As Integer
        Dim kfromsize As Integer
        Dim ktosize As Integer
        Dim kfromdensity As Integer
        Dim ktodensity As Integer
        Dim kfromspec As Integer
        Dim ktospec As Integer
        Dim kfromhtg As Integer
        Dim ktohtg As Integer

        Dim kbeghtg As Integer
        Dim kendhtg As Integer
        Dim kbegspec As Integer
        Dim kendspec As Integer
        Dim kbegsize As Integer
        Dim kendsize As Integer
        Dim kbegdensity As Integer
        Dim kenddensity As Integer

        Dim ksizerule As Integer
        Dim kdensityrule As Integer
        Dim kspeciesrule As Integer
        Dim khtgrule As Integer

        Dim ruletype As String


        NSizeRules = 0
        NDensityRules = 0
        NSpeciesRules = 0
        NHTGRules = 0
        fnum = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        dummy = LineInput(fnum) 'header line
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            arr = Split(dummy, ",")
            If arr(2) = "SIZE" Then
                NSizeRules = NSizeRules + 1
                kfromsize = GetStrIndex(SizeClassNameLUTq, arr(3))
                If arr(3) <> "" And kfromsize < 0 Then
                    NSizeClassLUTq = NSizeClassLUTq + 1
                    ReDim Preserve SizeClassNameLUTq(NSizeClassLUTq)
                    SizeClassNameLUTq(NSizeClassLUTq) = arr(3)
                End If
            ElseIf arr(2) = "SPECIES" Then
                NSpeciesRules += 1
                kspec = GetStrIndex(SpeciesNameLUTq, arr(3))
                If arr(3) <> "" And kspec < 0 Then
                    NSpeciesLUTq = NSpeciesLUTq + 1
                    ReDim Preserve SpeciesNameLUTq(NSpeciesLUTq)
                    SpeciesNameLUTq(NSpeciesLUTq) = arr(3)
                End If
            ElseIf arr(2) = "HTG" Then
                NHTGRules += 1
                khtg = GetStrIndex(HabGroupNameLUTq, arr(3))
                If arr(3) <> "" And khtg < 0 Then
                    NHabGroupLUTq += 1
                    ReDim Preserve HabGroupNameLUTq(NHabGroupLUTq)
                    HabGroupNameLUTq(NHabGroupLUTq) = arr(3)
                End If
            ElseIf arr(2) = "DENSITY" Then
                NDensityRules = NDensityRules + 1
                kfromdensity = GetStrIndex(DensityNameLUTq, arr(3))
                If arr(3) <> "" And kfromsize < 0 Then
                    NDensityLUTq = NDensityLUTq + 1
                    ReDim Preserve DensityNameLUTq(NDensityLUTq)
                    DensityNameLUTq(NDensityLUTq) = arr(3)
                End If
            Else
                MsgBox("Rule " & Chr(13) & dummy & Chr(13) & " is not defined correctly. Relevant values: 'SIZE, SPECIES, HTG, DENSITY'")
                FileClose(fnum)
                Return
            End If

            khtg = GetStrIndex(HabGroupNameLUTq, arr(0))
            If arr(0) <> "" And khtg < 0 Then
                NHabGroupLUTq += 1
                ReDim Preserve HabGroupNameLUTq(NHabGroupLUTq)
                HabGroupNameLUTq(NHabGroupLUTq) = arr(0)
            End If
            kspec = GetStrIndex(SpeciesNameLUTq, arr(1))
            If arr(1) <> "" And kspec < 0 Then
                NSpeciesLUTq = NSpeciesLUTq + 1
                ReDim Preserve SpeciesNameLUTq(NSpeciesLUTq)
                SpeciesNameLUTq(NSpeciesLUTq) = arr(1)
            End If

        Loop
        FileClose(fnum)

        ReDim LegalSizeChange(NSizeRules, NHabGroupLUTq, NSpeciesLUTq, NSizeClassLUTq, NSizeClassLUTq)
        ReDim LegalDensityChange(NDensityRules, NHabGroupLUTq, NSpeciesLUTq, NDensityLUTq, NDensityLUTq)
        ReDim LegalSpeciesChange(NSpeciesRules, NHabGroupLUTq, NSpeciesLUTq, NSpeciesLUTq)
        ReDim LegalHTGChange(NHTGRules, NHabGroupLUTq, NHabGroupLUTq, NSpeciesLUTq)
        ReDim ForceSizeChange(NSizeRules), ForceDensityChange(NDensityRules), ForceSpeciesChange(NSpeciesRules), ForceHTGChange(NHTGRules)
        ksizerule = 0
        kdensityrule = 0
        kspeciesrule = 0
        khtgrule = 0

        fnum = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        dummy = LineInput(fnum) 'header line
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            arr = Split(dummy, ",")
            khtg = GetStrIndex(HabGroupNameLUTq, arr(0))
            kbeghtg = khtg
            kendhtg = khtg 'just one
            If khtg = 0 Then 'defined for all htgs
                kbeghtg = 0
                kendhtg = NHabGroupLUTq
            End If
            kspec = GetStrIndex(SpeciesNameLUTq, arr(1))
            kbegspec = kspec
            kendspec = kspec 'just one
            If kspec = 0 Then 'defined for all htgs
                kbegspec = 0
                kendspec = NSpeciesLUTq
            End If
            ruletype = arr(2)
            If kbeghtg >= 0 And kendhtg >= 0 And kbegspec >= 0 And kendspec >= 0 Then
                If ruletype = "SIZE" Then
                    ksizerule = ksizerule + 1
                    kfromsize = GetStrIndex(SizeClassNameLUTq, arr(3))
                    ktosize = GetStrIndex(SizeClassNameLUTq, arr(4))
                    If kfromsize <= 0 Then
                        kbegsize = 0
                        kendsize = NSizeClassLUTq
                    Else
                        kbegsize = kfromsize
                        kendsize = kfromsize
                    End If
                    'CONTROL FOR ILLEGALLY RECOGNIZED SIZES...
                    If ktosize > 0 Then
                        For jhtg As Integer = kbeghtg To kendhtg
                            For jspec As Integer = kbegspec To kendspec
                                For jsize As Integer = kbegsize To kendsize
                                    LegalSizeChange(ksizerule, jhtg, jspec, jsize, ktosize) = 1
                                Next jsize
                            Next
                        Next
                    End If
                    ForceSizeChange(ksizerule) = 0 'default
                    If arr.Length >= 6 Then
                        If Trim(arr(5)) = "yes" Then ForceSizeChange(ksizerule) = 1
                    End If
                ElseIf ruletype = "DENSITY" Then
                    kdensityrule = kdensityrule + 1
                    kfromdensity = GetStrIndex(DensityNameLUTq, arr(3))
                    ktodensity = GetStrIndex(DensityNameLUTq, arr(4))
                    If kfromdensity <= 0 Then
                        kbegdensity = 0
                        kenddensity = NDensityLUTq
                    Else
                        kbegdensity = kfromdensity
                        kenddensity = kfromdensity
                    End If
                    'CONTROL FOR ILLEGALLY RECOGNIZED DENSITIES
                    If ktodensity > 0 Then
                        For jhtg As Integer = kbeghtg To kendhtg
                            For jspec As Integer = kbegspec To kendspec
                                For jdensity As Integer = kbegdensity To kenddensity
                                    LegalDensityChange(kdensityrule, jhtg, jspec, jdensity, ktodensity) = 1
                                Next jdensity
                            Next
                        Next
                    End If
                    ForceDensityChange(kdensityrule) = 0 'default
                    If arr.Length >= 6 Then
                        If Trim(arr(5)) = "yes" Then ForceDensityChange(kdensityrule) = 1
                    End If

                ElseIf ruletype = "SPECIES" Then
                    kspeciesrule = kspeciesrule + 1
                    kfromspec = GetStrIndex(SpeciesNameLUTq, arr(3))
                    ktospec = GetStrIndex(SpeciesNameLUTq, arr(4))
                    If kfromspec <= 0 Then
                        kbegspec = 0
                        kendspec = NSpeciesLUTq
                    Else
                        kbegspec = kfromspec
                        kendspec = kfromspec
                    End If
                    'CONTROL FOR ILLEGALLY RECOGNIZED DENSITIES
                    If ktospec > 0 Then
                        For jhtg As Integer = kbeghtg To kendhtg
                            For jspec As Integer = kbegspec To kendspec
                                'For jdensity As Integer = kbegdensity To kenddensity
                                LegalSpeciesChange(kspeciesrule, jhtg, jspec, ktospec) = 1
                                'Next jdensity
                            Next
                        Next
                    End If
                    ForceSpeciesChange(kspeciesrule) = 0 'default
                    If arr.Length >= 6 Then
                        If Trim(arr(5)) = "yes" Then ForceSpeciesChange(kspeciesrule) = 1
                    End If

                ElseIf ruletype = "HTG" Then
                    khtgrule = khtgrule + 1
                    kfromhtg = GetStrIndex(HabGroupNameLUTq, arr(3))
                    ktohtg = GetStrIndex(HabGroupNameLUTq, arr(4))
                    If kfromhtg <= 0 Then
                        kbeghtg = 0
                        kendhtg = NHabGroupLUTq
                    Else
                        kbeghtg = kfromhtg
                        kendhtg = kfromhtg
                    End If
                    'CONTROL FOR ILLEGALLY RECOGNIZED DENSITIES
                    If ktohtg > 0 Then
                        For jhtg As Integer = kbeghtg To kendhtg
                            For jspec As Integer = kbegspec To kendspec
                                'For jdensity As Integer = kbegdensity To kenddensity
                                LegalHTGChange(khtgrule, jhtg, ktohtg, jspec) = 1
                                'Next jdensity
                            Next
                        Next
                    End If
                    ForceHTGChange(khtgrule) = 0 'default
                    If arr.Length >= 6 Then
                        If Trim(arr(5)) = "yes" Then ForceHTGChange(khtgrule) = 1
                    End If
                End If
            End If
        Loop

        FileClose(fnum)

    End Sub

    Private Sub FindDeadStarts()
        'reads a file that finds states that start a successional path (no succession entry to the state)
        Dim dummy As String
        Dim arr() As String
        Dim arr2() As String
        Dim kspec As Integer
        Dim ksize As Integer
        Dim kdensity As Integer
        Dim khtg As Integer
        Dim HTG As String
        Dim i As Integer
        Dim NFiles As Integer
        Dim FileName() As String
        Dim fnum As Integer
        Dim folder As String
        Dim succtrigger As Integer
        Dim kstate As Integer = 0
        Dim kkstate As Integer
        'Dim NDeadStarts As Integer = 0
        Dim FromStateTemp() As String
        Dim NTempStates As Integer = 500000
        Dim tstate As String
        Dim currstate As String
        Dim nextstate As String
        Dim SizeCounter() As Integer
        Dim statecounter As Integer
        Dim printstring As String
        Dim tottime As Integer
        Dim NSpecies As Integer
        Dim Species() As String
        Dim infloop As String 'flags whether the species persists or whether it is lost to succession

        'do a folderbrowser to get the folder with pathway files
        Dim folderdialog2 As New FolderBrowserDialog
        With folderdialog2
            .SelectedPath = foldername
            .Description = "Folder that has exported .txt pathway files (1 per EcoGroup)"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Exit Sub
        End With

        folder = folderdialog2.SelectedPath

        Dim fldr As New System.IO.DirectoryInfo(folder)
        Dim files As System.IO.FileInfo() = fldr.GetFiles '("*EVU_SIM_DATA*")
        Dim fi As System.IO.FileInfo

        'initiate these arrays
        NSizeClassLUTq = 0
        ReDim SizeClassNameLUTq(NSizeClassLUTq)
        SizeClassNameLUTq(0) = "" 'have to give it a value...
        NDensityLUTq = 0
        ReDim DensityNameLUTq(NDensityLUTq)
        NHabGroupLUTq = 0
        ReDim HabGroupNameLUTq(NHabGroupLUTq)
        NSpeciesLUTq = 0
        ReDim SpeciesNameLUTq(NSpeciesLUTq)

        NFiles = 0
        ReDim FileName(NFiles)
        For Each fi In files
            i = i + 1
            If fi.Extension = ".txt" Then
                NFiles = NFiles + 1
                ReDim Preserve FileName(NFiles)
                FileName(NFiles) = fi.FullName
            End If
        Next
        NHabGroupLUTq = NFiles
        ReDim HabGroupNameLUTq(NHabGroupLUTq)

        NStates = 0
        ReDim FromStateTemp(NTempStates)
        'first identify all possible species, size, and density values from the pathway files
        For jfile As Integer = 1 To NFiles
            fnum = FreeFile()
            FileOpen(fnum, FileName(jfile), OpenMode.Input)
            'header line contains the habitat type group
            dummy = LineInput(fnum)
            arr = Split(dummy, " ")
            HTG = Trim(arr(1))
            HabGroupNameLUTq(jfile) = HTG
            Do Until EOF(fnum)
                dummy = Trim(LineInput(fnum))
                If dummy.Length > 0 Then
                    arr = Split(dummy, " ")
                    If arr(0) = "VEGETATIVE-TYPE" Then
                        NStates = NStates + 1
                        If NStates > NTempStates Then
                            NTempStates = NTempStates + 50000
                            ReDim Preserve FromStateTemp(NTempStates)
                        End If
                        FromStateTemp(NStates) = HTG & "/" & arr(1)
                        arr2 = Split(arr(1), "/")
                        kspec = GetStrIndex(SpeciesNameLUTq, arr2(0))
                        If kspec < 0 Then
                            NSpeciesLUTq = NSpeciesLUTq + 1
                            ReDim Preserve SpeciesNameLUTq(NSpeciesLUTq)
                            SpeciesNameLUTq(NSpeciesLUTq) = arr2(0)
                        End If

                        ksize = Array.IndexOf(SizeClassNameLUTq, arr2(1)) 'here we include the age of the size class 'GetSizeInd(SizeClassNameLUTq, arr2(1))
                        If ksize < 0 Then
                            NSizeClassLUTq = NSizeClassLUTq + 1
                            ReDim Preserve SizeClassNameLUTq(NSizeClassLUTq)
                            SizeClassNameLUTq(NSizeClassLUTq) = arr2(1)
                        End If

                        kdensity = GetStrIndex(DensityNameLUTq, arr2(2))
                        If kdensity < 0 Then
                            NDensityLUTq = NDensityLUTq + 1
                            ReDim Preserve DensityNameLUTq(NDensityLUTq)
                            DensityNameLUTq(NDensityLUTq) = arr2(2)
                        End If
                    End If
                End If
            Loop
            FileClose(fnum)
        Next

        ReDim FromState(NStates), SuccToState(NStates), DeadStartState(NStates)
        For jstate As Integer = 1 To NStates
            FromState(jstate) = FromStateTemp(jstate)
        Next
        FromStateTemp = Nothing


        'NDeadStarts = NStates
        kstate = 0
        For jfile As Integer = 1 To NFiles
            fnum = FreeFile()
            FileOpen(fnum, FileName(jfile), OpenMode.Input)
            'header line contains the habitat type group
            dummy = LineInput(fnum)
            arr = Split(dummy, " ")
            HTG = Trim(arr(1))
            khtg = jfile

            Do Until EOF(fnum)
                dummy = Trim(LineInput(fnum))
                If dummy.Length > 0 Then
                    arr = Split(dummy, " ")
                    If arr(0) = "VEGETATIVE-TYPE" Then
                        arr2 = Split(arr(1), "/")
                        succtrigger = 0
                        kkstate = Array.IndexOf(FromState, HTG & "/" & arr(1))

                    Else
                        For j As Integer = 0 To arr.Length - 1
                            arr2 = Split(arr(j), "/")
                            For jj As Integer = 0 To arr2.Length - 1
                                If arr2(jj) = "SUCCESSION" Then
                                    succtrigger = 1
                                ElseIf succtrigger = 1 And arr2.Length > 1 Then

                                    tstate = HTG & "/" & arr(j)
                                    kstate = Array.IndexOf(FromState, tstate)
                                    If kstate > 0 Then
                                        DeadStartState(kstate) = 1
                                    End If
                                    SuccToState(kkstate) = HTG & "/" & arr(j)
                                    succtrigger = 0
                                    Exit For
                                End If
                            Next jj
                        Next

                    End If
                End If
            Loop
            FileClose(fnum)
        Next

        fnum = FreeFile()
        FileOpen(fnum, DirName(FileName(1)) & "StateCounter.csv", OpenMode.Output)
        PrintLine(fnum, "START_STATE,SPECIES,HTG,SPECIES_GRP,SIZE,DENSITY,N_OTHER,N_SS,N_POLE,N_MED,N_LARGE,N_VL,TOTAL_TIME,END_STATE")
        For jstate As Integer = 1 To NStates
            If DeadStartState(jstate) = 0 Then
                'go through a do loop to count the number of time steps in each size class
                currstate = FromState(jstate)
                nextstate = SuccToState(jstate)
                statecounter = 1
                ReDim SizeCounter(5)

                'HOW MANY SPECIES IN THIS STATE?
                arr = Split(currstate, "/")
                Species = Split(arr(1), "-")
                NSpecies = Species.Length
                For jspec As Integer = 0 To NSpecies - 1
                    currstate = FromState(jstate)
                    nextstate = SuccToState(jstate)
                    statecounter = 1
                    ReDim SizeCounter(5)
                    infloop = ""
                    Do While nextstate <> ""
                        arr = Split(currstate, "/")
                        ksize = GetGenSize(arr(2))
                        SizeCounter(ksize) = SizeCounter(ksize) + 1
                        If currstate = nextstate Then
                            infloop = "*"
                            Exit Do
                        End If

                        If statecounter > 100 Then Exit Do
                        'check to see if the species is in the next state - if not, then exit do
                        arr = Split(nextstate, "/")
                        arr2 = Split(arr(1), "-")
                        For j As Integer = 0 To arr2.Length - 1
                            If Species(jspec) = arr2(j) Then Exit For
                            If Species(jspec) <> arr2(j) And j = arr2.Length - 1 Then Exit Do 'haven't found it by the last element so get out!
                        Next

                        currstate = nextstate
                        kstate = Array.IndexOf(FromState, currstate)
                        nextstate = SuccToState(kstate)
                        statecounter = statecounter + 1
                    Loop

                    printstring = FromState(jstate) & "," & Species(jspec)
                    arr = Split(FromState(jstate), "/")
                    For j As Integer = 0 To 3
                        printstring = printstring & "," & arr(j)
                    Next
                    tottime = 0
                    For jsize As Integer = 0 To 5
                        printstring = printstring & "," & SizeCounter(jsize)
                        'can count either total time species persists or total time it takes to grow to very-large
                        If jsize > 0 And jsize < 6 Then '5 Then
                            tottime = tottime + SizeCounter(jsize)
                            'ElseIf jsize = 5 And SizeCounter(jsize) > 0 Then
                            '    tottime = tottime + 1
                        End If
                    Next
                    If nextstate <> "" Then
                        printstring = printstring & "," & tottime & infloop & "," & currstate
                    Else
                        printstring = printstring & ",*error in pathway - no succession defined*"
                    End If

                    PrintLine(fnum, printstring)
                Next jspec

            End If

        Next
        FileClose(fnum)



    End Sub

    Private Function GetGenSize(ByVal sizstr As String) As Integer
        GetGenSize = 0 'default
        If InStr(sizstr, "SS") Then
            GetGenSize = 1
            Exit Function
        ElseIf InStr(sizstr, "POLE") Or InStr(sizstr, "PTS") Or InStr(sizstr, "PMU") Then
            GetGenSize = 2
            Exit Function
        ElseIf InStr(sizstr, "MEDIUM") Or InStr(sizstr, "MTS") Or InStr(sizstr, "MMU") Then
            GetGenSize = 3
            Exit Function
        ElseIf InStr(sizstr, "VERY-LARGE") Or InStr(sizstr, "VLTS") Or InStr(sizstr, "VLMU") Then
            GetGenSize = 5
            Exit Function
        ElseIf InStr(sizstr, "LARGE") Or InStr(sizstr, "LTS") Or InStr(sizstr, "LMU") Then
            GetGenSize = 4
            Exit Function

        End If

    End Function
    Private Function GetSizeInd(ByVal CompareArr() As String, ByVal strval As String) As Integer
        Dim secondcompareval As String
        GetSizeInd = -1

        For j As Integer = 0 To CompareArr.Length - 1
            If CompareArr(j).Length >= strval.Length - 2 And strval.Length >= CompareArr(j).Length And CompareArr(j).Length > 0 Then 'gets you through 99 size classes
                secondcompareval = strval.Substring(0, CompareArr(j).Length)
                If CompareArr(j) = secondcompareval Then
                    GetSizeInd = j
                    Exit Function
                End If
            End If
        Next
    End Function


    Private Sub CopyPathway(ByVal fromfile As String, ByVal tofile As String)
        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String
        Dim arr2() As String
        Dim kspec As Integer
        Dim CopyLines() As String
        Dim kcopylines As Integer = 1000
        Dim NCopyLines As Integer
        Dim CopySwitch As Byte
        Dim PasteLines() As String
        Dim NPasteLines As Integer
        Dim kpathway As Integer

        Dim jj As Integer


        fnum = FreeFile()
        FileOpen(fnum, fromfile, OpenMode.Input)
        'header line contains the habitat type group

        'HTG = Trim(arr(1))
        'HabGroupNameLUTq(jfile) = HTG
        NFromPathwaysAvailable = 0
        ReDim FromPathwaysAvailable(NFromPathwaysAvailable)

        dummy = LineInput(fnum)
        arr = Split(dummy, " ")
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length > 0 Then
                arr = Split(dummy, " ")
                If arr(0) = "VEGETATIVE-TYPE" Then
                    arr2 = Split(arr(1), "/")
                    kspec = GetStrIndex(FromPathwaysAvailable, arr2(0))
                    If kspec < 0 Then
                        NFromPathwaysAvailable = NFromPathwaysAvailable + 1
                        ReDim Preserve FromPathwaysAvailable(NFromPathwaysAvailable)
                        FromPathwaysAvailable(NFromPathwaysAvailable) = arr2(0)
                    End If
                End If
            End If
        Loop
        FileClose(fnum)

        NumItems = NFromPathwaysAvailable
        ReDim ItemName(NumItems)
        For jitem As Integer = 1 To NumItems
            ItemName(jitem) = FromPathwaysAvailable(jitem)
        Next


        Dim SelectorFrm As New Selector
        SelectorText = "Choose Pathway To Copy"
        SelectorFrm.ShowDialog()
        SelectorFrm = Nothing

        If FrmCancel = False Then
            NPathwaysToCopy = ItemsToRead.Length - 1
            ReDim PathwaysToCopy(NPathwaysToCopy)
            For jcopy As Integer = 1 To NPathwaysToCopy
                PathwaysToCopy(jcopy) = ItemsToRead(jcopy)
            Next jcopy
        Else
            Exit Sub
        End If

        'figure the save-as value
        ReDim PathwayToPaste(NPathwaysToCopy)
        If NPathwaysToCopy = 1 Then
            txtInputLabel = "Save the Pathway as:"
            txtInputText = PathwaysToCopy(1)
            Dim txtfrm As New frmTextInput
            txtfrm.ShowDialog()
            txtfrm = Nothing
            PathwayToPaste(1) = txtReturnTextValue
        Else
            For jcopy As Integer = 1 To NPathwaysToCopy
                PathwayToPaste(jcopy) = PathwaysToCopy(jcopy)
            Next
        End If


        'NOW, READ IN THE LINES TO COPY
        ReDim CopyLines(kcopylines)

        fnum = FreeFile()
        FileOpen(fnum, fromfile, OpenMode.Input)
        'header line contains the habitat type group
        dummy = LineInput(fnum)
        arr = Split(dummy, " ")
        CopySwitch = 0
        NCopyLines = 0
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If Trim(dummy).Length > 0 Then
                arr = Split(dummy, " ")
                If arr(0) = "VEGETATIVE-TYPE" Then
                    CopySwitch = 0
                    arr2 = Split(arr(1), "/")
                    For jcopy As Integer = 1 To NPathwaysToCopy
                        If arr2(0) = PathwaysToCopy(jcopy) Then
                            CopySwitch = 1
                            Exit For
                        End If
                    Next jcopy
                End If
            End If
            If CopySwitch = 1 Then
                NCopyLines = NCopyLines + 1
                If NCopyLines > CopyLines.Length - 1 Then ReDim Preserve CopyLines(CopyLines.Length + kcopylines)
                CopyLines(NCopyLines) = dummy
            End If
        Loop
        FileClose(fnum)

        'populate the paste lines
        NPasteLines = NCopyLines
        ReDim PasteLines(NPasteLines)
        For jline As Integer = 1 To NCopyLines
            arr = Split(CopyLines(jline), " ")
            For j As Integer = 0 To arr.Length - 1
                arr2 = Split(arr(j), "/")
                For jj = 0 To arr2.Length - 1
                    kpathway = GetStrIndex(PathwaysToCopy, arr2(jj))
                    If kpathway > 0 Then
                        PasteLines(jline) = PasteLines(jline) & PathwayToPaste(kpathway) '& "/"
                    Else
                        PasteLines(jline) = PasteLines(jline) & arr2(jj) '& "/"
                    End If
                    If jj < arr2.Length - 1 Then PasteLines(jline) = PasteLines(jline) & "/"
                Next
                If jj = 1 And j < arr.Length - 1 Then PasteLines(jline) = PasteLines(jline) & " "
            Next
            'PasteLines(jline) = CopyLines(jline).Replace(PathwayToCopy, PathwayToPaste)
        Next

        fnum = FreeFile()
        FileOpen(fnum, tofile, OpenMode.Append)
        For jline As Integer = 1 To NPasteLines
            PrintLine(fnum, PasteLines(jline))
        Next
        FileClose(fnum)

    End Sub

    Private Sub DeletePathway(ByVal fromfile As String)
        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String
        Dim arr2() As String
        Dim kspec As Integer
        Dim CopyLines() As String
        Dim kcopylines As Integer = 1000
        Dim NCopyLines As Integer
        Dim CopySwitch As Byte


        fnum = FreeFile()
        FileOpen(fnum, fromfile, OpenMode.Input)
        'header line contains the habitat type group

        NFromPathwaysAvailable = 0
        ReDim FromPathwaysAvailable(NFromPathwaysAvailable)

        dummy = LineInput(fnum)
        arr = Split(dummy, " ")
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length > 0 Then
                arr = Split(dummy, " ")
                If arr(0) = "VEGETATIVE-TYPE" Then
                    arr2 = Split(arr(1), "/")
                    kspec = GetStrIndex(FromPathwaysAvailable, arr2(0))
                    If kspec < 0 Then
                        NFromPathwaysAvailable = NFromPathwaysAvailable + 1
                        ReDim Preserve FromPathwaysAvailable(NFromPathwaysAvailable)
                        FromPathwaysAvailable(NFromPathwaysAvailable) = arr2(0)
                    End If
                End If
            End If
        Loop
        FileClose(fnum)

        NumItems = NFromPathwaysAvailable
        ReDim ItemName(NumItems)
        For jitem As Integer = 1 To NumItems
            ItemName(jitem) = FromPathwaysAvailable(jitem)
        Next


        Dim SelectorFrm As New Selector
        SelectorText = "Choose Pathway To Delete"
        SelectorFrm.ShowDialog()
        SelectorFrm = Nothing

        If FrmCancel = False Then
            NPathwaysToDelete = ItemsToRead.Length - 1
            ReDim PathwaysToDelete(NPathwaysToDelete)
            For jpath As Integer = 1 To NPathwaysToDelete
                PathwaysToDelete(jpath) = ItemsToRead(jpath)
            Next
        Else
            Exit Sub
        End If

        'NOW, READ IN THE LINES TO COPY
        ReDim CopyLines(kcopylines)

        fnum = FreeFile()
        FileOpen(fnum, fromfile, OpenMode.Input)
        CopySwitch = 1
        NCopyLines = 0
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If Trim(dummy).Length > 0 Then
                arr = Split(dummy, " ")
                If arr(0) = "VEGETATIVE-TYPE" Then
                    CopySwitch = 1 'default to copying the line
                    arr2 = Split(arr(1), "/")
                    For jpath As Integer = 1 To NPathwaysToDelete
                        If arr2(0) = PathwaysToDelete(jpath) Then
                            CopySwitch = 0 'you have a match  means don't copy it!
                            Exit For
                        End If
                    Next
                End If
            End If
            If CopySwitch = 1 Then
                NCopyLines = NCopyLines + 1
                If NCopyLines > CopyLines.Length - 1 Then ReDim Preserve CopyLines(CopyLines.Length + kcopylines)
                CopyLines(NCopyLines) = dummy
            End If
        Loop
        FileClose(fnum)

        fnum = FreeFile()
        FileOpen(fnum, fromfile, OpenMode.Output)
        For jline As Integer = 1 To NCopyLines
            PrintLine(fnum, CopyLines(jline))
        Next
        FileClose(fnum)

    End Sub

    Private Sub CopyProcess(ByVal fromfile As String, ByVal tofile As String)
        Dim fnum As Integer
        Dim fnum2 As Integer
        Dim dummy As String
        Dim pastestring As String
        Dim arr() As String
        Dim arr2() As String
        Dim kspec As Integer
        Dim CopyLines() As String
        Dim kcopylines As Integer = 1000
        Dim NCopyLines As Integer
        Dim CopySwitch As Byte
        Dim PasteLines() As String
        Dim NPasteLines As Integer
        Dim kpathway As Integer

        Dim NFromStates As Integer
        Dim kstate As Integer
        Dim kfromstate As Integer
        Dim NToStates As Integer
        Dim ktostate As Integer

        Dim Npasted As Integer
        Dim Nnotpasted As Integer

        Dim FromStateCovertype() As String
        Dim FromStateSize() As String
        Dim FromStateDensity() As String

        Dim ToStateCovertype() As String
        Dim ToStateSize() As String
        Dim ToStateDensity() As String

        Dim FromProcesses() As String
        Dim NFromProcesses As Integer
        Dim kprocess As Integer
        Dim ProcessToCopy As String
        Dim ProcessToPaste As String 'can re-label with different name
        Dim PathwayToPasteInto As String

        Dim FromLines() As String
        Dim NFromLines As Integer
        Dim ToLines() As String
        Dim NToLines As Integer
        Dim kline As Integer

        Dim CopyState() As String 'state with the process tocopy from
        Dim CopyProcess() As String 'by number of source states - whether this particular one has a process to copy and what that happens to be!

        Dim jj As Integer

        Dim LinesPasted() As String
        Dim NLinesPasted As Integer
        Dim LinesNotPasted() As String
        Dim NLinesNotPasted As Integer

        Dim sinkCT As String
        Dim sinkSIZE As String
        Dim sinkDENSITY As String
        Dim padrightlen As Integer

        Dim bMatchPaste As Boolean 'whether you can legally paste the process into the sink file


        fnum = FreeFile()
        FileOpen(fnum, fromfile, OpenMode.Input)
        'header line contains the habitat type group

        'HTG = Trim(arr(1))
        'HabGroupNameLUTq(jfile) = HTG
        NFromPathwaysAvailable = 0
        ReDim FromPathwaysAvailable(NFromPathwaysAvailable)
        NFromStates = 0
        NFromLines = 0
        dummy = LineInput(fnum)
        arr = Split(dummy, " ")
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length > 0 Then
                NFromLines = NFromLines + 1
                arr = Split(dummy, " ")
                If arr(0) = "VEGETATIVE-TYPE" Then
                    NFromStates = NFromStates + 1
                    arr2 = Split(arr(1), "/")
                    kspec = GetStrIndex(FromPathwaysAvailable, arr2(0))
                    If kspec < 0 Then
                        NFromPathwaysAvailable = NFromPathwaysAvailable + 1
                        ReDim Preserve FromPathwaysAvailable(NFromPathwaysAvailable)
                        FromPathwaysAvailable(NFromPathwaysAvailable) = arr2(0)
                    End If
                End If
            End If
        Loop
        FileClose(fnum)


        ReDim FromStateCovertype(NFromStates), FromStateSize(NFromStates), FromStateDensity(NFromStates)
        ReDim FromLines(NFromLines)

        fnum = FreeFile()
        FileOpen(fnum, fromfile, OpenMode.Input)
        kstate = 0
        kline = 0
        dummy = LineInput(fnum)
        arr = Split(dummy, " ")
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length > 0 Then
                kline = kline + 1
                FromLines(kline) = dummy
                arr = Split(dummy, " ")
                If arr(0) = "VEGETATIVE-TYPE" Then
                    arr2 = Split(arr(1), "/")
                    kstate = kstate + 1
                    FromStateCovertype(kstate) = arr2(0)
                    FromStateSize(kstate) = arr2(1)
                    FromStateDensity(kstate) = arr2(2)
                ElseIf arr(0) <> "" And arr(0) <> "XY-COORDINATE" Then
                    'you have a process!
                End If
            End If
        Loop
        FileClose(fnum)

        NumItems = NFromPathwaysAvailable
        ReDim ItemName(NumItems)
        For jitem As Integer = 1 To NumItems
            ItemName(jitem) = FromPathwaysAvailable(jitem)
        Next

        Dim SelectorFrm As New Selector
        SelectorText = "Choose Pathway With Process To Copy (select one)"
        SelectorFrm.ShowDialog()
        SelectorFrm = Nothing

        If FrmCancel = False Then
            NPathwaysToCopy = 1 'ItemsToRead.Length - 1
            ReDim PathwaysToCopy(NPathwaysToCopy)
            PathwaysToCopy(1) = ItemsToRead(1)
        Else
            Exit Sub
        End If

        'once you have identified the pathway to copy from, you have to identify the potential processes it has to copy

        CopySwitch = 0
        NFromProcesses = 0
        ReDim FromProcesses(NFromProcesses)
        For jline As Integer = 1 To NFromLines
            arr = Split(FromLines(jline), " ")
            If arr(0) = "VEGETATIVE-TYPE" Then
                arr2 = Split(arr(1), "/")
                If arr2(0) = PathwaysToCopy(1) Then CopySwitch = 1
            End If
            If arr(0) <> "" And arr(0) <> "VEGETATIVE-TYPE" And arr(0) <> "XY-COORDINATE" And CopySwitch = 1 Then
                'you have a potential process to copy
                kprocess = GetStrIndex(FromProcesses, arr(0))
                If kprocess < 0 Then
                    NFromProcesses = NFromProcesses + 1
                    ReDim Preserve FromProcesses(NFromProcesses)
                    FromProcesses(NFromProcesses) = arr(0)
                End If
            End If
        Next

        NumItems = NFromProcesses
        ReDim ItemName(NumItems)
        For jitem As Integer = 1 To NumItems
            ItemName(jitem) = FromProcesses(jitem)
        Next

        Dim SelectorFrm2 As New Selector
        SelectorText = "Choose Process To Copy from " & PathwaysToCopy(1) & " (select one)"
        SelectorFrm2.ShowDialog()
        SelectorFrm2 = Nothing

        If FrmCancel = False Then
            ProcessToCopy = ItemsToRead(1)
        Else
            Exit Sub
        End If

        'populate an array with the lines to copy
        ReDim CopyProcess(NFromStates)
        CopySwitch = 0
        kstate = 0
        For jline As Integer = 1 To NFromLines
            arr = Split(FromLines(jline), " ")
            If arr(0) = "VEGETATIVE-TYPE" Then
                kstate = kstate + 1
                arr2 = Split(arr(1), "/")
                If arr2(0) = PathwaysToCopy(1) Then CopySwitch = 1
            End If
            If arr(0) <> "" And arr(0) <> "VEGETATIVE-TYPE" And arr(0) <> "XY-COORDINATE" And CopySwitch = 1 Then
                'you have a potential process to copy
                kprocess = GetStrIndex(FromProcesses, arr(0))
                If FromProcesses(kprocess) = ProcessToCopy Then
                    CopyProcess(kstate) = FromLines(jline)
                End If
            End If
        Next

        'NOW IDENTIFY THE PATHWAY TO COPY THE PROCESS INTO
        'Dim ToPathwaysAvailable() As String
        'Dim NToPathwaysAvailable As Integer
        'Dim PathwayToPaste() As String
        fnum = FreeFile()
        FileOpen(fnum, tofile, OpenMode.Input)
        NToPathwaysAvailable = 0
        ReDim ToPathwaysAvailable(NToPathwaysAvailable)
        NToStates = 0
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length > 0 Then
                NToLines = NToLines + 1
                arr = Split(dummy, " ")
                If arr(0) = "VEGETATIVE-TYPE" Then
                    NToStates = NToStates + 1
                    arr2 = Split(arr(1), "/")
                    kspec = GetStrIndex(ToPathwaysAvailable, arr2(0))
                    If kspec < 0 Then
                        NToPathwaysAvailable = NToPathwaysAvailable + 1
                        ReDim Preserve ToPathwaysAvailable(NToPathwaysAvailable)
                        ToPathwaysAvailable(NToPathwaysAvailable) = arr2(0)
                    End If
                End If
            End If
        Loop
        FileClose(fnum)


        NumItems = NToPathwaysAvailable
        ReDim ItemName(NumItems)
        For jitem As Integer = 1 To NumItems
            ItemName(jitem) = ToPathwaysAvailable(jitem)
        Next
        Dim SelectorFrm3 As New Selector
        SelectorText = "Choose Pathway To Copy " & PathwaysToCopy(1) & " " & ProcessToCopy & " into:"
        SelectorFrm3.ShowDialog()
        SelectorFrm3 = Nothing

        If FrmCancel = False Then
            PathwayToPasteInto = ItemsToRead(1)
        Else
            Exit Sub
        End If

        'figure the save-as value

        txtInputLabel = "Save the Process as:"
        txtInputText = ProcessToCopy
        Dim txtfrm As New frmTextInput
        txtfrm.ShowDialog()
        txtfrm = Nothing
        ProcessToPaste = txtReturnTextValue
        padrightlen = 26
        If ProcessToPaste.Length >= 26 Then padrightlen = ProcessToPaste.Length + 1


        'Now, populate the "To" state arrays to discern the legal states for processes to result in.
        ReDim ToStateCovertype(NToStates), ToStateSize(NToStates), ToStateDensity(NToStates)
        ReDim ToLines(NToLines)

        fnum = FreeFile()
        FileOpen(fnum, tofile, OpenMode.Input)
        kstate = 0
        kline = 0
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length > 0 Then
                kline = kline + 1
                ToLines(kline) = dummy
                arr = Split(dummy, " ")
                If arr(0) = "VEGETATIVE-TYPE" Then
                    arr2 = Split(arr(1), "/")
                    kstate = kstate + 1
                    ToStateCovertype(kstate) = arr2(0)
                    ToStateSize(kstate) = arr2(1)
                    ToStateDensity(kstate) = arr2(2)
                End If
            End If
        Loop
        FileClose(fnum)


        'open 2 files - the file to past into and a new file to hold the pasted values. Rename them later
        ReDim LinesPasted(NToStates), LinesNotPasted(NToStates)
        Npasted = 0
        Nnotpasted = 0
        fnum = FreeFile()
        FileOpen(fnum, tofile, OpenMode.Input)
        fnum2 = FreeFile()
        FileOpen(fnum2, DirName(tofile) & FName(tofile) & "_paste.txt", OpenMode.Output)

        Do Until EOF(fnum)
            bMatchPaste = False
            pastestring = LineInput(fnum)
            dummy = Trim(pastestring)
            arr = Split(dummy, " ")
            If arr(0) <> "VEGETATIVE-TYPE" Then
                PrintLine(fnum2, pastestring)
            Else
                PrintLine(fnum2, pastestring)
                'check to see if this is a PastInto state
                arr2 = Split(arr(1), "/")
                If arr2(0) = PathwayToPasteInto Then

                    For jstate As Integer = 1 To NFromStates
                        If FromStateCovertype(jstate) = PathwaysToCopy(1) And FromStateSize(jstate) = arr2(1) And FromStateDensity(jstate) = arr2(2) Then
                            'catch the jstate ID
                            kfromstate = jstate
                            Exit For
                        End If
                    Next
                    If CopyProcess(kfromstate) <> "" Then
                        arr = Split(CopyProcess(kfromstate), " ")
                        arr2 = Split(arr(arr.Length - 1), "/")
                        sinkCT = arr2(0)
                        sinkSIZE = arr2(1)
                        sinkDENSITY = arr2(2)
                        'determine whether then sink state covertype is the same as the source state covertype
                        If FromStateCovertype(kfromstate) = sinkCT Then
                            For jjstate As Integer = 1 To NToStates
                                If ToStateCovertype(jjstate) = PathwayToPasteInto And ToStateSize(jjstate) = sinkSIZE And ToStateDensity(jjstate) = sinkDENSITY Then
                                    ktostate = jjstate
                                    bMatchPaste = True
                                    pastestring = "  " & ProcessToPaste.PadRight(padrightlen) & ToStateCovertype(ktostate) & "/" & ToStateSize(ktostate) & "/" & ToStateDensity(ktostate)
                                    Npasted = Npasted + 1
                                    LinesPasted(Npasted) = pastestring
                                    Exit For
                                End If
                            Next
                            If bMatchPaste = False Then
                                Nnotpasted = Nnotpasted + 1
                                LinesNotPasted(Nnotpasted) = CopyProcess(kfromstate)
                            End If
                        ElseIf FromStateCovertype(kfromstate) <> sinkCT Then
                            '################
                            'could actually add logic to see what's different. So, if it goes from a DF-LP to a DF, and you are pasting the process into a DF-PP-LP, perhaps note that just the LP is different and paste as DF-PP (???)
                            For jjstate As Integer = 1 To NToStates
                                If ToStateCovertype(jjstate) = sinkCT And ToStateSize(jjstate) = sinkSIZE And ToStateDensity(jjstate) = sinkDENSITY Then
                                    ktostate = jjstate
                                    bMatchPaste = True
                                    pastestring = "  " & ProcessToPaste.PadRight(padrightlen) & sinkCT & "/" & ToStateSize(ktostate) & "/" & ToStateDensity(ktostate)
                                    Npasted = Npasted + 1
                                    LinesPasted(Npasted) = pastestring
                                    Exit For
                                End If
                            Next
                            If bMatchPaste = False Then
                                Nnotpasted = Nnotpasted + 1
                                LinesNotPasted(Nnotpasted) = CopyProcess(kfromstate)
                            End If
                        End If
                    End If
                    'if you find a match, then have to figure out where to put it in - as well as take out the same process if it is in there already so you don't have a duplicate process
                    'otherwise, you can go back to copying and pasting lines as usual...
                    Do Until EOF(fnum)
                        If bMatchPaste = True Then
                            dummy = LineInput(fnum)
                            arr = Split(Trim(dummy), " ")
                            If Trim(arr(0)) = ProcessToPaste Then
                                'overwrite this process
                                PrintLine(fnum2, pastestring)
                                Exit Do
                            ElseIf Trim(arr(0)) = "XY-COORDINATE" Or Trim(arr(0)) = "" Then
                                PrintLine(fnum2, pastestring)
                                PrintLine(fnum2, dummy)
                                Exit Do
                            Else
                                PrintLine(fnum2, dummy)
                            End If
                        Else
                            Exit Do
                        End If
                    Loop
                End If
            End If
        Loop
        FileClose(fnum)
        FileClose(fnum2)

        'rename these files
        System.IO.File.Copy(tofile, DirName(tofile) & FName(tofile) & "_orig.txt", True)
        System.IO.File.Copy(DirName(tofile) & FName(tofile) & "_paste.txt", tofile, True)
        System.IO.File.Delete(DirName(tofile) & FName(tofile) & "_paste.txt")

        fnum = FreeFile()
        FileOpen(fnum, DirName(tofile) & FName(tofile) & "_pastestats.txt", OpenMode.Output)
        PrintLine(fnum, "LINES PASTED")
        For jline As Integer = 1 To Npasted
            PrintLine(fnum, LinesPasted(jline))
        Next
        PrintLine(fnum, "")
        PrintLine(fnum, "LINES NOT PASTED")
        For jline As Integer = 1 To Nnotpasted
            PrintLine(fnum, LinesNotPasted(jline))
        Next
        FileClose(fnum)




    End Sub




    Private Function StringInt(ByVal str As String, ByVal list As String) As Integer
        StringInt = -1
        Dim arr() As String
        arr = Split(list, ",")
        For j As Integer = 0 To arr.Length - 1
            If str = Trim(arr(j)) Then
                StringInt = j
                Exit Function
            End If
        Next
    End Function


    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'SELECT THE PATHWAY TO COPY FROM
        'SELECT THE PROCESS TO COPY
        'SELECT THE PATHWAY TO PASTE INTO
        'SELECT THE PASTE ATTRIBUTES (SAME AS SOURCE, SAME AS SINK)
        'RECTIFY INCONSISTENCIES - CANT FIND THE SINK STATE, E.G.

        Dim copyfromfile As String
        Dim copytofile As String
        Dim Openfiledialog1 As New OpenFileDialog
        With Openfiledialog1
            .Title = "File with the Pathway/Process to Copy FROM"
            .Filter = "Text File (*.txt)|*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        copyfromfile = Openfiledialog1.FileName

        With Openfiledialog1
            .Title = "File with the Pathway to Copy the Process INTO"
            .Filter = "Text File (*.txt)|*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        copytofile = Openfiledialog1.FileName

        Openfiledialog1 = Nothing
        CopyProcess(copyfromfile, copytofile)

        MsgBox("Done")


    End Sub

    'private sub Determinestate(byval


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim dbfileoutput As String

        Dim savefiledialog1 As New SaveFileDialog
        With savefiledialog1
            .Title = "File to write Pathway DB to:"
            .Filter = "CSV File (*.csv)|*.csv"
            .FileName = "OutFile.csv"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        dbfileoutput = savefiledialog1.FileName


        FillCombosUsedRunOutput(dbfileoutput)
        MsgBox("Done")
    End Sub
    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        'IDENTIFY THE DIRECTORY WITH MODEL DEFINITION FILES IN IT
        Dim folderdialog2 As New FolderBrowserDialog
        Dim TempItems() As String
        Dim arr() As String
        Dim csvtxt As String
        Dim filename As String

        NumItems = 0
        Dim i As Integer = 0

        With folderdialog2
            .SelectedPath = foldername
            .Description = "Folder that contains run outputs to process (usually 'textdata')"
            '.Title = "Select a folder that has the "
            '.Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        foldername = folderdialog2.SelectedPath
        folderdialog2 = Nothing

        'populate the possible runs to process for the selector and then call the selector if you find a list of them...
        Dim fldr As New System.IO.DirectoryInfo(foldername)
        Dim files As System.IO.FileInfo() = fldr.GetFiles("*EVU_SIM_DATA*")
        Dim fi As System.IO.FileInfo
        NumItems = files.Length
        ReDim TempItems(NumItems)
        i = 0
        For Each fi In files
            arr = Split(fi.Name, ".")
            If arr(arr.Length - 1) <> "gz" And arr(arr.Length - 1) <> "clr" Then 'filter out the zip files for the selector box
                i = i + 1
                TempItems(i) = fi.Name
                csvtxt = arr(arr.Length - 1)
            End If

        Next
        NumItems = i
        If NumItems = 0 Then
            MsgBox("Folder contains no EVUSIMDATA files. Make sure they are unzipped.")
            Exit Sub
        End If

        ReDim ItemName(NumItems)
        For i = 1 To NumItems
            ItemName(i) = TempItems(i)
        Next

        Dim SelectorFrm As New Selector
        SelectorText = "Choose 1 SIMPPLLE output file to count states:"
        SelectorFrm.ShowDialog()
        SelectorFrm = Nothing

        If FrmCancel = False Then
            filename = ItemsToRead(1)
            CountPathways(foldername, filename, csvtxt)
            'ItemsToRead.Length - 1)
        End If

        MsgBox("Done")
    End Sub

    Private Sub FillCombosUsedRunOutput(ByVal outfile As String)
        'creates a comma delimited of pathway files in DB-ready format
        Dim cHTG As String
        Dim cSourceType As String
        Dim cSourceSize As String
        Dim cSourceAge As String
        Dim iSourceAgeLen As Integer
        Dim cSourceDensity As String
        Dim s As String
        Dim cAge As String
        Dim i As Integer

        Dim cProcess As String
        Dim cSinkType As String
        Dim cSinkSize As String
        Dim cSinkAge As String
        Dim cSinkDensity As String

        Dim folder As String
        Dim NFiles As Integer
        Dim FileName() As String

        Dim fnum As Integer
        Dim fnum2 As Integer
        Dim dummy As String
        Dim arr() As String
        Dim arr2() As String

        Dim CopySwitch As Byte

        Dim pstring As String

        'do a folderbrowser to get the folder with pathway files
        Dim folderdialog2 As New FolderBrowserDialog
        With folderdialog2
            .SelectedPath = foldername
            .Description = "Folder that has exported .txt pathway files (1 per EcoGroup)"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Exit Sub
        End With

        folder = folderdialog2.SelectedPath

        Dim fldr As New System.IO.DirectoryInfo(folder)
        Dim files As System.IO.FileInfo() = fldr.GetFiles '("*EVU_SIM_DATA*")
        Dim fi As System.IO.FileInfo

        NFiles = 0
        ReDim FileName(NFiles)
        For Each fi In files
            If fi.Extension = ".txt" Then
                NFiles = NFiles + 1
                ReDim Preserve FileName(NFiles)
                FileName(NFiles) = fi.FullName
            End If
        Next

        'outputfile
        fnum2 = FreeFile()
        FileOpen(fnum2, outfile, OpenMode.Output)
        pstring = "HabTypeGroup,CoverType,Size,SizeAge,Density,Process,SinkCoverType,SinkSize,SinkAge,SinkDensity"
        PrintLine(fnum2, pstring)
        'first identify all possible species, size, and density values from the pathway files
        For jfile As Integer = 1 To NFiles
            CopySwitch = 0
            fnum = FreeFile()
            FileOpen(fnum, FileName(jfile), OpenMode.Input)
            'header line contains the habitat type group
            dummy = LineInput(fnum)
            arr = Split(dummy, " ")
            cHTG = Trim(arr(1))

            Do Until EOF(fnum)
                dummy = Trim(LineInput(fnum))
                If dummy.Length > 0 Then
                    arr = Split(dummy, " ")
                    If arr(0) = "VEGETATIVE-TYPE" Then
                        CopySwitch = 1
                        arr2 = Split(arr(1), "/")
                        cSourceType = arr2(0)
                        'NEED TO SPLIT OUT ANY KIND OF AGE AT THE END
                        cSourceSize = arr2(1)
                        i = 0
                        cAge = ""
                        For j As Integer = 1 To cSourceSize.Length
                            s = cSourceSize.Substring(cSourceSize.Length - 1 - i, 1)
                            If IsNumeric(s) = True Then
                                cAge = s & cAge
                                i = i + 1
                            Else
                                Exit For
                            End If
                        Next
                        cSourceSize = cSourceSize.Substring(0, cSourceSize.Length - i)
                        If cAge = "" Then cAge = 0
                        cSourceAge = CType(cAge, Integer)
                        cSourceDensity = arr2(2)

                        'now find the sink stuff
                    ElseIf arr(0) <> "" And arr(0) <> "VEGETATIVE-TYPE" And arr(0) <> "XY-COORDINATE" And CopySwitch = 1 Then
                        'you have a potential process to copy
                        cProcess = arr(0)


                        cSinkType = arr2(0)
                        'NEED TO SPLIT OUT ANY KIND OF AGE AT THE END
                        For j As Integer = 1 To arr.Length - 1
                            If arr(j).Length > 1 Then
                                arr2 = Split(arr(j), "/")
                                Exit For
                            End If
                        Next
                        cSinkSize = arr2(1)
                        i = 0
                        cAge = ""
                        For j As Integer = 1 To cSinkSize.Length
                            s = cSinkSize.Substring(cSinkSize.Length - 1 - i, 1)
                            If IsNumeric(s) = True Then
                                cAge = s & cAge
                                i = i + 1
                            Else
                                Exit For
                            End If
                        Next
                        cSinkSize = cSinkSize.Substring(0, cSinkSize.Length - i)
                        If cAge = "" Then cAge = 0
                        cSinkAge = CType(cAge, Integer)
                        cSinkDensity = arr2(2)
                        'print it all!!!
                        pstring = cHTG & "," & cSourceType & "," & cSourceSize & "," & cSourceAge & "," & cSourceDensity & "," & cProcess & "," & cSinkType & "," & cSinkSize & "," & cSinkAge & "," & cSinkDensity
                        PrintLine(fnum2, pstring)
                    End If
                End If

            Loop
            FileClose(fnum)
        Next
        FileClose(fnum2)







    End Sub
    Private Sub CountPathways(ByVal foldername As String, ByVal filename As String, ByVal csvtxt As String)
        Dim PathwaysByTime(,,) As Integer
        Dim TotalPathways() As Integer
        Dim PathwayName() As String
        Dim PathwayIndex() As Integer
        Dim MaxTimesteps As Integer
        Dim NPathways As Integer
        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String
        Dim slinkind As Integer = 0
        Dim htgind As Integer = 4
        Dim kpathway As Integer
        Dim speciescol As Integer
        Dim timestepcol As Integer
        Dim slinkcol As Integer
        Dim ktime As Integer
        Dim kspecies As Integer
        Dim maxtimeused As Integer
        Dim sr As IO.StreamReader
        Dim SLINKHTG() As Integer
        Dim NSLINK As Integer
        Dim HTGName() As String
        Dim NHTG As Integer
        Dim khtg As Integer
        Dim PrintPathway(,) As Boolean

        fnum = FreeFile()
        FileOpen(fnum, foldername & "/SPECIES." & csvtxt, OpenMode.Input)
        dummy = LineInput(fnum)
        NPathways = 0
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            NPathways += 1
        Loop
        FileClose(fnum)
        ReDim PathwayName(NPathways), PathwayIndex(NPathways)

        'read again to populate the array
        fnum = FreeFile()
        FileOpen(fnum, foldername & "/SPECIES." & csvtxt, OpenMode.Input)
        dummy = LineInput(fnum) 'header
        kpathway = 0
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            arr = Split(dummy, ",")
            kpathway += 1
            PathwayIndex(kpathway) = arr(0)
            PathwayName(kpathway) = arr(1)
        Loop
        FileClose(fnum)

        'populate the htg by Slink
        If System.IO.File.Exists(foldername & "/SLINKMETRICS.csv") Then
            NHTG = 0
            NSLINK = 0
            ReDim HTGName(NHTG)
            sr = New IO.StreamReader(foldername & "/SLINKMETRICS.csv")
            dummy = sr.ReadLine
            Do While Not sr.EndOfStream
                dummy = sr.ReadLine
                NSLINK += 1
                arr = Split(dummy, ",")
                khtg = Array.IndexOf(HTGName, arr(htgind))
                If khtg < 1 Then
                    NHTG += 1
                    ReDim Preserve HTGName(NHTG)
                    HTGName(NHTG) = arr(htgind)
                End If
                NSLINK = Math.Max(NSLINK, CType(arr(slinkind), Integer))
            Loop
            sr.Close()
            sr = Nothing

            ReDim SLINKHTG(NSLINK)
            sr = New IO.StreamReader(foldername & "/SLINKMETRICS.csv")
            dummy = sr.ReadLine
            Do While Not sr.EndOfStream
                dummy = sr.ReadLine
                arr = Split(dummy, ",")
                khtg = Array.IndexOf(HTGName, arr(htgind))
                SLINKHTG(arr(slinkind)) = khtg
            Loop
            sr.Close()
            sr = Nothing
        Else
            NHTG = 1
            ReDim HTGName(NHTG)
            HTGName(NHTG) = "ALL"
        End If

        ReDim PrintPathway(NPathways, NHTG)
        MaxTimesteps = 200
        maxtimeused = -1
        ReDim PathwaysByTime(MaxTimesteps, NPathways, NHTG), TotalPathways(NPathways)


        'now read the SIMPPLLE output
        fnum = FreeFile()
        sr = New IO.StreamReader(foldername & "/" & filename)
        dummy = sr.ReadLine
        'FileOpen(fnum, foldername & "/" & filename, OpenMode.Input)
        'dummy = LineInput(fnum) 'header
        arr = Split(dummy, ",")
        speciescol = -1
        timestepcol = -1
        For j As Integer = 1 To arr.Length - 1
            If arr(j) = "TIMESTEP" Then timestepcol = j
            If arr(j) = "SPECIES_ID" Then speciescol = j
            If arr(j) = "SLINK" Then slinkcol = j
        Next

        Do While Not sr.EndOfStream 'Do Until EOF(fnum)
            dummy = sr.ReadLine ' dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            arr = Split(dummy, ",")
            ktime = arr(timestepcol)
            For j As Integer = 1 To NPathways
                If arr(speciescol) = PathwayIndex(j) Then
                    kspecies = j
                    Exit For
                End If
            Next
            khtg = 1

            If arr(slinkcol) <= NSLINK Then khtg = SLINKHTG(arr(slinkcol))
            If ktime > MaxTimesteps Then Exit Do
            maxtimeused = Math.Max(ktime, maxtimeused)
            PathwaysByTime(ktime, kspecies, khtg) += 1
            PrintPathway(kspecies, khtg) = True
            TotalPathways(kspecies) += 1
        Loop
        sr.Close()
        sr = Nothing 'FileClose(fnum)

        fnum = FreeFile()
        FileOpen(fnum, foldername & "/" & "PATHWAYCOUNT.csv", OpenMode.Output)
        dummy = "HTG,SPECIES,PATHWAY"
        For Jtime As Integer = 1 To maxtimeused
            dummy = dummy & ",Time_" & Jtime
        Next
        dummy = dummy & ",TOTAL"
        PrintLine(fnum, dummy)
        For jpathway As Integer = 1 To NPathways
            For jhtg As Integer = 1 To NHTG
                If PrintPathway(jpathway, jhtg) = True Then
                    dummy = HTGName(jhtg) & "," & PathwayName(jpathway) & "," & HTGName(jhtg) & "_" & PathwayName(jpathway)
                    For jtime As Integer = 1 To maxtimeused
                        dummy = dummy & "," & PathwaysByTime(jtime, jpathway, jhtg)
                    Next
                    dummy = dummy & "," & TotalPathways(jpathway)
                    PrintLine(fnum, dummy)
                End If
            Next
        Next
        FileClose(fnum)






    End Sub
    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        FindDeadStarts()
        MsgBox("Done")
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        MsgBox("Requires you to create a .csv file with the following fields" & Chr(13) & "FromHTG, SPECIES, HTG/SPECIES/SIZE/DENSITY, FROMSTATE, TOSTATE, FORCE(yes/no)" & Chr(13) & Chr(13) & "Also requires a field Look-up Table which can be created in the 'Make SIMPPLLE Input Files' Screen")
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        MsgBox("This will search through pathway files (.txt) and list the time it takes to grow through the successional stages")
    End Sub


    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        MsgBox("query a SIMPPLLE output file to count the number of time each pathway is used in a simulation.")
    End Sub


    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click

        Dim selectfrm As New frmSpatialRelationsFields
        selectfrm.Show()
        selectfrm = Nothing


    End Sub

    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        Dim fnum As Integer
        Dim outfilename As String

        Dim savefiledialog As New SaveFileDialog
        'need to have a save file dialog
        'outfilename = DirName(tbPolygonTableFN.Text) & "FieldLUT_" & FName(tbPolygonTableFN.Text) & ".txt"
        With savefiledialog
            .Title = "Save the Field LUT file as"
            .Filter = "TextFile(.csv) |*.csv"
            .FileName = "Changes.csv"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        outfilename = savefiledialog.FileName
        savefiledialog = Nothing

        fnum = FreeFile()
        FileOpen(fnum, outfilename, OpenMode.Output)
        PrintLine(fnum, "FromHTG,SPECIES,HTG/SPECIES/SIZE/DENSITY,FROMSTATE,TOSTATE,FORCE(yes/no)")

        FileClose(fnum)
    End Sub

    Private Sub PrintNewLine(ByVal ofnum As Integer, ByVal arr() As String, ByVal khtg As Integer, ByVal kspec As Integer, ByVal ksize As Integer, ByVal kdensity As Integer, ByVal FieldIndices() As Integer, ByRef dummy As String)
        Dim strb As StringBuilder
        Dim pcom As String

        strb = New StringBuilder("", 500)
        pcom = ","
        For j As Integer = 0 To arr.Length - 1
            If j = arr.Length - 1 Then pcom = ","
            If j = FieldIndices(1) And khtg > 0 Then
                strb.Append(HabGroupNameLUTq(khtg) & pcom)
            ElseIf j = FieldIndices(2) And kspec > 0 Then
                strb.Append(SpeciesNameLUTq(kspec) & pcom)
            ElseIf j = FieldIndices(3) And ksize > 0 Then
                strb.Append(SizeClassNameLUTq(ksize) & pcom)
            ElseIf j = FieldIndices(4) And kdensity > 0 Then
                strb.Append(DensityNameLUTq(kdensity) & pcom)
            Else
                strb.Append(arr(j) & pcom)
            End If
        Next

        'update the line to reflect the new information
        dummy = strb.ToString

        PrintLine(ofnum, strb.ToString)
    End Sub
End Class