Module modVMAPtoSIMPPLLE1
#Region "Declarations"

    Dim CTGroup(,) As Integer 'by # unique groups, # Unique Species
    Dim NSpecies As Integer 'number of unique species
    Dim SpeciesName() As String 'name of the species
    Dim CTGName() As String
    'Dim CTGroupAcres() As Single 'number of acres in each covertype group
    'Dim SpeciesAcres() As Single 'count the number of acres where the species is part of any CTG
    Dim NCTG As Integer 'number of the covertype groups
    Dim CTGNSpecies() As Integer 'number of species in the covertype group
    Dim HabCTGFeasibility(,) As Integer 'by # CTG, #HabGroups
    Dim HabSpeciesFeasibility(,) As Integer 'by #Species, #Habgroups
    Dim HabGroupName() As String
    Dim NHabGroups As Integer
    Dim ExcessDev(,) As Double
    Dim DeficitDev(,) As Double
    Dim HTGSpeciesAcres(,,) As Single 'by Iteration NHTG, NSpecies
    Dim HTGCTGAcres(,) As Single 'by NHTG, NCTG


    Dim NPolys As Integer
    Dim Index() As String
    Dim LSGroup() As String
    Dim AspectGroup() As String
    Dim ElevationClass() As String
    Dim HT_GRP() As String
    Dim HT_GRP_Ind() As Integer
    Dim VMAP_Code() As String
    Dim Acres() As Single
    Dim FirstCutClass() As String
    Dim MinorPct(,) As Single 'by poly, minor species

    Dim GoodCodeKept() As String 'a good code found already
    Dim CodeToWrite() As String 'what the computer determines
    Dim CountChanges As Integer

    'Model File names
    Dim FN_CTGbyHTG As String
    Dim FN_ColIndices As String
    Dim FN_DataTable As String
    Dim FN_VMAPCrosswalk As String
    Dim FN_VMAPReclass As String
    Dim FN_TabooDefs As String
    Dim FN_MinorSpeciesDefs As String
    Dim FN_MandatoryMS As String
    Dim FN_SpeciesGoals As String


    Dim LSGroupName() As String
    Dim NLSGroups As Integer
    Dim AspectGroupName() As String
    Dim NAspectGroups As Integer
    Dim ElevationClassName() As String
    Dim NElevationClasses As Integer

    'indices
    Dim i_IndexCol As Integer
    Dim i_LSGroupCol As Integer
    Dim i_AspectGroupCol As Integer
    Dim i_ElevationClassCol As Integer
    Dim i_HT_GRPCol As Integer
    Dim i_VMAPCodeCol As Integer
    Dim i_AcresCol As Integer

    Dim i_MinorPctCol() As Integer
    Dim i_FirstCutClassCol As Integer

    Dim d_PolyCode() As String 'the money array - what the polygon gets coded as

    'minor species stuff
    Dim NMinorSpecies As Integer '= 7 'hardwired...for now
    Dim OrigNMinorSpecies As Integer '= 7 'hardwired...for now
    'Dim ms_Index() As Integer
    Dim ms_Name() As String
    Dim ms_MinPct(,) As Single 'min percentage the species has to have to be considered...can vary by HTG
    Dim ms_MinPctIter(,,) As Single 'min percentage for by HTG, Species, Iteration

    'Mandatory Minor Species
    Dim bMandatoryMS As Boolean
    Dim NMandatoryMS As Integer
    Dim mms_NMatchesNeeded() As Integer
    Dim mms_LSGroup() As String
    Dim mms_AspectGroup() As String
    Dim mms_ElevationClass() As String
    Dim mms_HT_GRP() As String
    Dim mms_VMAP() As String
    Dim mms_SPECIES() As String

    'Species goals by HTG
    Dim bSpeciesGoals As Boolean
    Dim HTGSpeciesGoal(,) As Single 'acres by HTG, species


    'VMAP Code stuff...load this stuff first????
    Dim v_VMAPCode() As String
    Dim NVMAPCodes As Integer
    Dim v_VMAP_SIMPPLLEDefault() As String

    'Manditory Reclass Stuff
    Dim bMandatoryReclass As Boolean
    Dim NMandatoryReclass As Integer
    Dim mr_NMatchesNeeded() As Integer
    Dim mr_LSGroup() As String
    Dim mr_AspectGroup() As String
    Dim mr_ElevationClass() As String
    Dim mr_HT_GRP() As String
    Dim mr_VMAP() As String
    Dim mr_RECLASS() As String

    'optional reclass stuff - check and use if all else fails!
    Dim bOptionalReclass As Boolean
    Dim NOptionalReclass As Integer
    Dim or_NMatchesNeeded() As String
    Dim or_LSGroup() As String
    Dim or_AspectGroup() As String
    Dim or_ElevationClass() As String
    Dim or_HT_GRP() As String
    Dim or_VMAP() As String
    Dim or_RECLASS() As String

    'taboo class stuff - check and use if all else fails!
    Dim NTabooDefs As Integer
    Dim bTabooDefs As Boolean
    Dim t_NMatchesNeeded() As String
    Dim t_LSGroup() As String
    Dim t_AspectGroup() As String
    Dim t_ElevationClass() As String
    Dim t_HT_GRP() As String
    Dim t_VMAP() As String
    Dim t_SPECIES() As String

    'Iteration Stuff
    Dim IterKIteration() As Integer
    Dim NIters As Integer 'number of iterations - default to 1

#End Region

    Public Sub RunVMAPtoSIMPPLLE(ByVal ModelFileName As String, ByVal GoalTolerance As Integer, ByVal NIterations As Integer)
        Dim bMissed As Boolean = False
        Dim kiteration As Integer
        NIters = NIterations

        LoadModelFile(ModelFileName)

        LoadCTGbyHTG(FN_CTGbyHTG)
        LoadMinorSpeciesDefs(FN_MinorSpeciesDefs)
        LoadColIndices(FN_ColIndices)
        LoadPolyFile(FN_DataTable)
        LoadVMAPCrosswalk(FN_VMAPCrosswalk)
        If bMandatoryReclass = True Then
            LoadManditoryOptionalReclass(FN_VMAPReclass)
        End If
        If bTabooDefs = True Then
            LoadTabooDefs(FN_TabooDefs)
        End If
        If bMandatoryMS = True Then
            LoadMandatoryMS(FN_MandatoryMS)
        End If

        ReDim HTGSpeciesAcres(NSaveIterations, NHabGroups, NSpecies)
        ReDim HTGSpeciesGoal(NHabGroups, NSpecies)
        If bIterate = False Then
            CodeDataTable(bMissed, 1, 1)
            WriteOutputTables(FN_DataTable, 1)
            MsgBox("Data Coded")
        Else
            LoadHTGSpeciesGoals(FN_SpeciesGoals)
            ReDim IterKIteration(NSaveIterations)
            kiteration = 0
            For jiter As Integer = 1 To NIterations
                'determine kiteration
                kiteration = kiteration + 1
                If kiteration > NSaveIterations Then kiteration = 1
                IterKIteration(kiteration) = jiter
                CodeDataTable(bMissed, jiter, kiteration)
                'If bMissed = True Then Exit For
                'If jiter < NIterations Then 'don't look at deviations for the final iteration
                Deviations(kiteration, GoalTolerance)
                If jiter < NIterations Then NewPct(jiter) 'don't adjust the last iteration
                'End If
            Next

            'CodeDataTable(FN_DataTable) 'this does the last iteration and writes out the results
            WriteOutputTables(FN_DataTable, kiteration)
            WriteMSMinPct(FN_MinorSpeciesDefs)
            If NSaveIterations > 0 Then WriteIterationResults(DirName(FN_DataTable) & FName(FN_DataTable) & "_iter.csv")
            If bMissed = True Then
                MsgBox("Data Coded, but there are errors that need to be fixed. Check output files")
            Else

                MsgBox("Data Coded")
            End If
        End If
        'Dim FN_DataTable As String

    End Sub

    Private Sub CodeDataTable(ByRef bMissedCode As Boolean, ByVal jiteration As Integer, ByVal kiteration As Integer) ' As String)
        'goes through line-by-line and makes a good code determination for each record

        '1. If bKeepGoodCodes, see if the existing code is good for the HabitatType
        '2. Figure the VMAP Default code for the primary species
        '3. Fix with manditory reclass if necessary
        '4. Check for Feasibility of primary CT - does it occur in ANY CTG of the HTG
        '5. If CT is infeasible for the HTG, check for Optional Reclass
        '6. If Optional Reclass CT is infeasible for the HTG, code the poly "NOT FOUND" and move on
        '7. If Primary CT is feasible, look at minor CTs that pass thresholds.
        '   a. Include search to filter out taboo species!
        '8. If 2 or more minor CTs make threshold, search 3-code CTGs in the HTG for best match - based on percentage
        '9. If no 3-code CTG is found, repeat for 2 Code
        '10. If no 2-code CTG is found, is the CT feasible by itself? If no code poly "NOT FOUND"



        Dim arr() As String
        Dim kctg As Integer
        Dim testctg As String
        Dim test1ctg As String
        Dim test2ctg As String
        Dim testreclass As String
        Dim testms() As String
        Dim primaryct As String
        Dim MSCode() As String
        Dim MSCodeUsed() As Integer
        Dim MSPct() As Single
        Dim maxms As Single
        Dim MSTaboo() As Integer
        Dim PctOrdered() As Single
        Dim MSCheckCode() As String 'ordered from highest to lowest percentage minor species to check

        Dim kms As Integer
        Dim countms As Integer
        Dim NMStoCheck As Integer
        Dim bMSFound As Boolean

        'input codes


        'Dim iFirstCutClass As Integer
        Dim bGoodCode As Boolean
        Dim bKeepDefault As Boolean
        Dim jfirstms As Integer

        ReDim GoodCodeKept(NPolys)
        ReDim CodeToWrite(NPolys)
        ReDim HTGCTGAcres(NHabGroups, NCTG)


        bMissedCode = False 'default
        CountChanges = 0 'reset

        'clear this array for the iteration
        For jhg As Integer = 1 To NHabGroups
            For jspec As Integer = 1 To NSpecies
                HTGSpeciesAcres(kiteration, jhg, jspec) = 0
            Next
        Next jhg


        For jpoly As Integer = 1 To NPolys

            'If jpoly = 6838 Then Stop
            CodeToWrite(jpoly) = "NOT FOUND" 'default

            '1. If bKeepGoodCodes, see if the existing code is good for the HabitatType
            bGoodCode = False 'default
            GoodCodeKept(jpoly) = "NOT FOUND" 'DEFAULT
            bKeepDefault = False 'default
            If bKeepGoodCodes Then 'And i_FirstCutClassCol - 1 >= 0 Then
                kctg = 0
                'iFirstCutClass = Trim(arr(i_FirstCutClassCol - 1))
                'find out if the currently coded name is a good one...
                bGoodCode = DetermineCTGFeasibility(FirstCutClass(jpoly), HT_GRP_Ind(jpoly), kctg)
                If bGoodCode = True Then
                    GoodCodeKept(jpoly) = CTGName(kctg)
                    CodeToWrite(jpoly) = GoodCodeKept(jpoly)
                    GoTo NextPoly
                    HTGCTGAcres(HT_GRP_Ind(jpoly), kctg) = HTGCTGAcres(HT_GRP_Ind(jpoly), kctg) + Acres(jpoly)
                End If
                bKeepDefault = bGoodCode
            End If

            '2. Figure the VMAP Default code for the primary species
            testctg = v_VMAP_SIMPPLLEDefault(DetermineStrInd(VMAP_Code(jpoly), v_VMAPCode))
            'is it defined in this HTG?
            If DetermineSpeciesFeasibility(testctg, HT_GRP_Ind(jpoly)) = False Then testctg = ""
            'is it taboo?
            If bTabooDefs Then
                If bTabooSpecies(LSGroup(jpoly), AspectGroup(jpoly), ElevationClass(jpoly), HT_GRP(jpoly), VMAP_Code(jpoly), testctg) = True Then testctg = ""
            End If
            'if there is no primary ctg, look for the non-taboo species with the highest percentage
            If testctg = "" Then
                'first see if you have a mandatory minor species that can be used
                countms = 0
                If bMandatoryMS = True Then
                    ReDim testms(0) 'default
                    CheckMandatoryMS(LSGroup(jpoly), AspectGroup(jpoly), ElevationClass(jpoly), HT_GRP(jpoly), VMAP_Code(jpoly), testms)
                    If testms.Length > 1 Then ' <> "" Then
                        For jms As Integer = 1 To testms.Length - 1
                            If bTabooDefs Then
                                If bTabooSpecies(LSGroup(jpoly), AspectGroup(jpoly), ElevationClass(jpoly), HT_GRP(jpoly), VMAP_Code(jpoly), testms(jms)) = False And DetermineSpeciesFeasibility(testms(jms), HT_GRP_Ind(jpoly)) = True Then
                                    testctg = testms(jms)
                                    countms = NMinorSpecies 'to keep it from going through 
                                    Exit For 'just get the first one that is a good match
                                End If
                            End If
                        Next jms
                    End If
                End If

                ReDim MSTaboo(NMinorSpecies)
                Do Until countms >= NMinorSpecies
                    countms = countms + 1
                    'search for minor species with the highest percentage
                    maxms = 0
                    kms = 0
                    For jms As Integer = 1 To OrigNMinorSpecies
                        If MSTaboo(jms) < 1 And MinorPct(jpoly, jms) > maxms And MinorPct(jpoly, jms) >= ms_MinPct(HT_GRP_Ind(jpoly), jms) And DetermineSpeciesFeasibility(ms_Name(jms), HT_GRP_Ind(jpoly)) = True Then
                            maxms = MinorPct(jpoly, jms)
                            kms = jms
                        End If
                    Next
                    If bTabooDefs Then
                        If bTabooSpecies(LSGroup(jpoly), AspectGroup(jpoly), ElevationClass(jpoly), HT_GRP(jpoly), VMAP_Code(jpoly), ms_Name(kms)) = False Then
                            testctg = ms_Name(kms)
                            Exit Do
                        Else
                            MSTaboo(kms) = 1
                        End If
                    Else
                        testctg = ms_Name(kms)
                        Exit Do
                    End If
                Loop
            End If

            primaryct = testctg

            '3. Fix with manditory reclass if necessary
            If bMandatoryReclass = True Then
                testreclass = primaryct
                CheckManditoryReclass(LSGroup(jpoly), AspectGroup(jpoly), ElevationClass(jpoly), HT_GRP(jpoly), VMAP_Code(jpoly), testreclass)
                testctg = testreclass
                primaryct = testreclass
            End If

            '4. Check for Feasibility of primary CT - does it occur in ANY CTG of the HTG
            bGoodCode = DetermineSpeciesFeasibility(primaryct, HT_GRP_Ind(jpoly))

            '#################SKIP THIS STEP AND MOVE TO THE NEXT STEP
            ' ''5. If CT is infeasible for the HTG, check for Optional Reclass
            ''If bGoodCode = False And bOptionalReclass = True Then
            ''    testreclass = primaryct
            ''    CheckOptionalReclass(LSGroup(jpoly), AspectGroup(jpoly), ElevationClass(jpoly), HT_GRP(jpoly), VMAP_Code(jpoly), testreclass)
            ''    testctg = testreclass
            ''    primaryct = testreclass
            ''End If

            ' ''6. If Optional Reclass CT is infeasible for the HTG, code the poly "NOT FOUND" and move on
            ''bGoodCode = DetermineSpeciesFeasibility(primaryct, HT_GRP_Ind(jpoly))
            ''If bGoodCode = False Then
            ''    CodeToWrite(jpoly) = "NOT FOUND"
            ''    GoTo NextPoly
            ''End If

            For j As Integer = 1 To 2
                'this is to check for optional feasibility if you get to pass 2
                If j = 2 Then
                    If bOptionalReclass = True Then
                        testreclass = primaryct
                        CheckOptionalReclass(LSGroup(jpoly), AspectGroup(jpoly), ElevationClass(jpoly), HT_GRP(jpoly), VMAP_Code(jpoly), testreclass)
                        testctg = testreclass
                        primaryct = testreclass
                    End If

                    '6. If Optional Reclass CT is infeasible for the HTG, code the poly "NOT FOUND" and move on
                    bGoodCode = DetermineSpeciesFeasibility(primaryct, HT_GRP_Ind(jpoly))
                    If bGoodCode = False Then
                        CodeToWrite(jpoly) = "NOT FOUND"
                        GoTo NextPoly
                    End If
                End If
                '7. If Primary CT is feasible, look at minor CTs that pass thresholds.
                ReDim MSCode(NMinorSpecies), MSPct(NMinorSpecies), MSCodeUsed(NMinorSpecies)
                NMStoCheck = 0
                For jms As Integer = 1 To OrigNMinorSpecies
                    'check the threshhold
                    If MinorPct(jpoly, jms) >= ms_MinPct(HT_GRP_Ind(jpoly), jms) And ms_Name(jms) <> primaryct And DetermineSpeciesFeasibility(ms_Name(jms), HT_GRP_Ind(jpoly)) = True Then
                        '   a. Include search to filter out taboo species!
                        If bTabooDefs Then
                            If bTabooSpecies(LSGroup(jpoly), AspectGroup(jpoly), ElevationClass(jpoly), HT_GRP(jpoly), VMAP_Code(jpoly), ms_Name(jms)) = False Then
                                NMStoCheck = NMStoCheck + 1
                                MSCode(jms) = ms_Name(jms)
                                MSPct(jms) = MinorPct(jpoly, jms)
                            End If
                        Else
                            NMStoCheck = NMStoCheck + 1
                            MSCode(jms) = ms_Name(jms)
                            MSPct(jms) = MinorPct(jpoly, jms)
                        End If
                    End If
                Next

                '   c. Check to see if there are mandatory minor species to use - if so, store it in the 0 slot of MStoCheck and add 1 to NMStoCheck
                ReDim testms(0) 'default
                jfirstms = 1 'default
                If bMandatoryMS = True Then
                    CheckMandatoryMS(LSGroup(jpoly), AspectGroup(jpoly), ElevationClass(jpoly), HT_GRP(jpoly), VMAP_Code(jpoly), testms)
                    If testms.Length > 1 Then '
                        For ims As Integer = 1 To testms.Length - 1
                            If testms(ims) <> primaryct Then
                                'see if it has already been included and if so, make its percentage included = 1
                                bMSFound = False
                                For jms As Integer = 1 To NMStoCheck
                                    If testms(ims) = MSCode(jms) Then
                                        MSPct(jms) = 100 'this has to be checked!
                                        bMSFound = True
                                    End If
                                Next jms
                                If bMSFound = False Then
                                    kms = GetStrIndex(ms_Name, testms(ims))
                                    If kms < 0 Then 'newly defined MS just for here
                                        NMinorSpecies = NMinorSpecies + 1
                                        ReDim Preserve ms_Name(NMinorSpecies), MSCode(NMinorSpecies), MSPct(NMinorSpecies), MSCodeUsed(NMinorSpecies), ms_MinPct(NHabGroups, NMinorSpecies), ms_MinPctIter(NIters + 1, NHabGroups, NMinorSpecies)
                                        kms = NMinorSpecies
                                        ms_Name(NMinorSpecies) = testms(ims)
                                    End If
                                    If bTabooDefs Then
                                        If bTabooSpecies(LSGroup(jpoly), AspectGroup(jpoly), ElevationClass(jpoly), HT_GRP(jpoly), VMAP_Code(jpoly), ms_Name(kms)) = False Then
                                            NMStoCheck = NMStoCheck + 1
                                            MSCode(kms) = ms_Name(kms)
                                            MSPct(kms) = 100 'MinorPct(jms)
                                        End If
                                    Else
                                        NMStoCheck = NMStoCheck + 1
                                        MSCode(kms) = ms_Name(kms)
                                        MSPct(kms) = 1 'MinorPct(jms)
                                    End If
                                End If

                            End If

                        Next ims
                    End If
                End If


                '    b. order these according to their percentage occurrences
                ReDim PctOrdered(NMinorSpecies)
                ReDim MSCheckCode(NMStoCheck)

                For jms As Integer = 1 To NMinorSpecies
                    PctOrdered(jms) = MSPct(jms)
                Next
                Array.Sort(PctOrdered)
                kms = 0
                Array.Reverse(PctOrdered)
                For jms As Integer = 1 To NMStoCheck
                    For jjms As Integer = 1 To NMinorSpecies
                        If PctOrdered(jms - 1) = MSPct(jjms) And MSCodeUsed(jjms) = 0 Then
                            kms = kms + 1
                            MSCheckCode(kms) = MSCode(jjms)
                            MSCodeUsed(jjms) = 1
                            Exit For 'found it
                        End If
                    Next
                Next

                '8a if 3 or more minor CTs make the threshold, search 4-code CTGs in the HTG for the best match
                If (jfirstms = 1 And NMStoCheck >= 1) Or (jfirstms = 0) Then 'the count gets thrown when you go to 0-based
                    For jj1 As Integer = jfirstms To NMStoCheck
                        test1ctg = primaryct & "-" & MSCheckCode(jj1)
                        For jj2 As Integer = jj1 + 1 To NMStoCheck
                            test2ctg = test1ctg & "-" & MSCheckCode(jj2)
                            For jj3 As Integer = jj2 + 1 To NMStoCheck
                                testctg = test2ctg & "-" & MSCheckCode(jj3)
                                bGoodCode = DetermineCTGFeasibility(testctg, HT_GRP_Ind(jpoly), kctg)
                                If bGoodCode = True Then
                                    CountChanges = CountChanges + 1
                                    CodeToWrite(jpoly) = CTGName(kctg)
                                    GoTo NextPoly
                                End If
                            Next jj3
                            '8b. If 2 or more minor CTs make threshold, search 3-code CTGs in the HTG for best match - based on percentage
                            '#########Test 3 species code here - so that your best minor species isn't trumped by 4 lesser 
                            bGoodCode = DetermineCTGFeasibility(test2ctg, HT_GRP_Ind(jpoly), kctg)
                            If bGoodCode = True Then
                                CountChanges = CountChanges + 1
                                CodeToWrite(jpoly) = CTGName(kctg)
                                GoTo NextPoly
                            End If
                        Next
                        '9. If no 3-code CTG is found, repeat for 2 Code
                        '#########Test 2 species code here - so that your best minor species isn't trumped by 3 lesser species
                        bGoodCode = DetermineCTGFeasibility(test1ctg, HT_GRP_Ind(jpoly), kctg)
                        If bGoodCode = True Then
                            CountChanges = CountChanges + 1
                            CodeToWrite(jpoly) = CTGName(kctg)
                            GoTo NextPoly
                        End If
                    Next

                End If


                '10. If no 2-code CTG is found, is the CT feasible by itself? If no code poly "NOT FOUND"
                testctg = primaryct
                bGoodCode = DetermineCTGFeasibility(testctg, HT_GRP_Ind(jpoly), kctg)
                If bGoodCode = True Then
                    CountChanges = CountChanges + 1
                    CodeToWrite(jpoly) = CTGName(kctg)
                    GoTo NextPoly
                Else
                    CodeToWrite(jpoly) = "NOT FOUND" 'SHOULD already be the default, but just to make sure
                End If
            Next j
NextPoly:
            If CodeToWrite(jpoly) = "NOT FOUND" Then bMissedCode = True
            If CodeToWrite(jpoly) <> "NOT FOUND" Then
                kctg = DetermineStrInd(CodeToWrite(jpoly), CTGName)
                For jspecies As Integer = 1 To NSpecies
                    If CTGroup(kctg, jspecies) = 1 Then
                        HTGSpeciesAcres(kiteration, HT_GRP_Ind(jpoly), jspecies) = HTGSpeciesAcres(kiteration, HT_GRP_Ind(jpoly), jspecies) + Acres(jpoly)
                        If jiteration = 1 Then HTGSpeciesAcres(0, HT_GRP_Ind(jpoly), jspecies) = HTGSpeciesAcres(0, HT_GRP_Ind(jpoly), jspecies) + Acres(jpoly)
                    End If
                Next
                HTGCTGAcres(HT_GRP_Ind(jpoly), kctg) = HTGCTGAcres(HT_GRP_Ind(jpoly), kctg) + Acres(jpoly)
            End If
        Next jpoly

    End Sub

    Private Sub LoadModelFile(ByVal Fname As String)
        'loads the species groups by habitat type definitions
        Dim fnum As Integer
        Dim dummy As String

        fnum = FreeFile()
        FileOpen(fnum, Fname, OpenMode.Input)
        dummy = LineInput(fnum)
        FN_CTGbyHTG = Trim(LineInput(fnum))
        dummy = LineInput(fnum)
        FN_DataTable = Trim(LineInput(fnum))
        dummy = LineInput(fnum)
        FN_ColIndices = Trim(LineInput(fnum))
        dummy = LineInput(fnum)
        FN_VMAPCrosswalk = Trim(LineInput(fnum))
        dummy = LineInput(fnum)
        FN_MinorSpeciesDefs = Trim(LineInput(fnum))

        dummy = LineInput(fnum)
        dummy = Trim(LineInput(fnum))
        bMandatoryReclass = False
        bOptionalReclass = False
        If System.IO.File.Exists(dummy) = True Then 'dummy <> "Not Used" Then
            bMandatoryReclass = True
            bOptionalReclass = True
            FN_VMAPReclass = dummy
        Else
        End If
        dummy = LineInput(fnum)
        dummy = Trim(LineInput(fnum))
        bTabooDefs = False 'default
        If System.IO.File.Exists(dummy) = True Then 'dummy <> "Not Used" Then
            bTabooDefs = True
            FN_TabooDefs = dummy
        End If

        dummy = LineInput(fnum)
        dummy = Trim(LineInput(fnum))
        bMandatoryMS = False 'default
        If System.IO.File.Exists(dummy) = True Then 'dummy <> "Not Used" Then
            bMandatoryMS = True
            FN_MandatoryMS = dummy
        End If

        dummy = LineInput(fnum)
        dummy = Trim(LineInput(fnum))
        bSpeciesGoals = False 'default
        If System.IO.File.Exists(dummy) = True Then 'dummy <> "Not Used" Then
            bSpeciesGoals = True
            FN_SpeciesGoals = dummy
        End If
        If bSpeciesGoals = False Then bIterate = False 'only reason to iterate is if you have goals you are trying to match
        FileClose(fnum)
    End Sub

    Private Sub LoadPolyFile(ByVal FileName As String)
        'loads the polygons to code file
        Dim fnum As Integer
        Dim dummy As String
        Dim linestring As String
        Dim arr() As String
        Dim stest As Single
        Dim kcol As Integer

        NPolys = 0
        fnum = FreeFile()
        FileOpen(fnum, FileName, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy <> "" Then NPolys = NPolys + 1
        Loop
        FileClose(fnum)
        ReDim Index(NPolys), LSGroup(NPolys), AspectGroup(NPolys), ElevationClass(NPolys), HT_GRP(NPolys), HT_GRP_Ind(NPolys)
        ReDim VMAP_Code(NPolys), Acres(NPolys), MinorPct(NPolys, NMinorSpecies)
        If i_FirstCutClassCol - 1 < 0 Then bKeepGoodCodes = False
        If bKeepGoodCodes = True Then ReDim FirstCutClass(NPolys)

        fnum = FreeFile()
        FileOpen(fnum, FileName, OpenMode.Input)
        dummy = LineInput(fnum)
        For jpoly As Integer = 1 To NPolys
            linestring = LineInput(fnum)
            arr = Split(linestring, ",")
            'get the codes of this record:
            Index(jpoly) = Trim(arr(i_IndexCol - 1))
            LSGroup(jpoly) = Trim(arr(i_LSGroupCol - 1))
            AspectGroup(jpoly) = Trim(arr(i_AspectGroupCol - 1))
            ElevationClass(jpoly) = Trim(arr(i_ElevationClassCol - 1))
            HT_GRP(jpoly) = Trim(arr(i_HT_GRPCol - 1))
            HT_GRP_Ind(jpoly) = DetermineStrInd(HT_GRP(jpoly), HabGroupName)
            VMAP_Code(jpoly) = Trim(arr(i_VMAPCodeCol - 1))
            'control for null values
            stest = 0
            If Trim(arr(i_AcresCol - 1)) <> "" Then stest = Trim(arr(i_AcresCol - 1))
            Acres(jpoly) = stest
            'minor species stuff

            For jms As Integer = 1 To OrigNMinorSpecies
                kcol = i_MinorPctCol(jms) - 1
                stest = 0
                If Trim(arr(kcol)) <> "" Then stest = Trim(arr(kcol))
                MinorPct(jpoly, jms) = stest
            Next
            If bKeepGoodCodes = True Then FirstCutClass(jpoly) = Trim(arr(i_FirstCutClassCol - 1))
        Next
        FileClose(fnum)
    End Sub

    Private Sub LoadCTGbyHTG(ByVal Fname As String)
        'loads the species groups by habitat type definitions
        Dim fnum As Integer
        Dim dummy As String
        Dim d2 As String
        Dim arr() As String
        Dim arr2() As String
        Dim kctg As Integer
        Dim bSpeciesMatch As Boolean

        fnum = FreeFile()
        FileOpen(fnum, Fname, OpenMode.Input)
        dummy = LineInput(fnum) 'there is a header line assumed in this file
        dummy = LineInput(fnum)
        arr = Split(dummy, ",")
        'count the HTGs and populate their names
        NHabGroups = 0
        For jhg As Integer = 1 To arr.Length - 1
            If Trim(arr(jhg)) <> "" Then NHabGroups = NHabGroups + 1
        Next
        ReDim HabGroupName(NHabGroups)
        For jhg As Integer = 1 To arr.Length - 1
            If Trim(arr(jhg)) <> "" Then HabGroupName(jhg) = Trim(arr(jhg))
        Next
        NCTG = 0
        NSpecies = 0
        ReDim SpeciesName(NSpecies)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            arr = Split(dummy, ",")
            d2 = Trim(arr(0))
            If d2 = "" Then Exit Do
            'assume each line is a new unique CTG
            NCTG = NCTG + 1
            'look for unique species in this thing!
            arr2 = Split(d2, "-")
            bSpeciesMatch = False
            For jparent As Integer = 0 To arr2.Length - 1
                d2 = Trim(arr2(jparent))
                bSpeciesMatch = False
                For jspec As Integer = 1 To NSpecies
                    If SpeciesName(jspec) = d2 Then
                        bSpeciesMatch = True
                        Exit For
                    End If
                Next
                If bSpeciesMatch = False Then
                    NSpecies = NSpecies + 1
                    ReDim Preserve SpeciesName(NSpecies)
                    SpeciesName(NSpecies) = Trim(arr2(jparent))
                End If
            Next
        Loop
        FileClose(fnum)

        ReDim CTGroup(NCTG, NSpecies), HabCTGFeasibility(NCTG, NHabGroups), CTGName(NCTG), CTGNSpecies(NCTG) ', CTGroupAcres(NCTG), SpeciesAcres(NSpecies)
        ReDim HabSpeciesFeasibility(NSpecies, NHabGroups)
        fnum = FreeFile()
        FileOpen(fnum, Fname, OpenMode.Input)
        dummy = LineInput(fnum)
        dummy = LineInput(fnum)
        kctg = 0
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            arr = Split(dummy, ",")
            If Trim(arr(0)) = "" Then Exit Do
            'assume each line is a new unique CTG
            'NCTG = NCTG + 1
            kctg = kctg + 1
            CTGName(kctg) = Trim(arr(0))
            For jhg As Integer = 1 To NHabGroups
                d2 = Trim(arr(jhg))
                If d2 <> "" Then HabCTGFeasibility(kctg, jhg) = 1
            Next
            arr2 = Split(arr(0), "-")
            CTGNSpecies(kctg) = arr2.Length
            For jparent As Integer = 0 To arr2.Length - 1
                d2 = Trim(arr2(jparent))
                For jspecies As Integer = 1 To NSpecies
                    If SpeciesName(jspecies) = d2 Then
                        CTGroup(kctg, jspecies) = 1
                        For jhg As Integer = 1 To NHabGroups
                            d2 = Trim(arr(jhg))
                            If d2 <> "" Then HabSpeciesFeasibility(jspecies, jhg) = 1
                        Next
                        Exit For
                    End If
                Next
            Next
        Loop
        FileClose(fnum)

    End Sub

    Private Sub LoadColIndices(ByVal FName As String)
        Dim fnum As Integer
        Dim dummy As String
        Dim d1 As String
        Dim d2 As String
        Dim arr() As String

        ReDim i_MinorPctCol(NMinorSpecies)
        fnum = FreeFile()
        FileOpen(fnum, FName, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            arr = Split(dummy, ",")
            d1 = Trim(arr(0))
            d2 = Trim(arr(1))
            If d1 = "Index" Then i_IndexCol = d2
            If d1 = "LSGroup" Then i_LSGroupCol = d2
            If d1 = "AspectGroup" Then i_AspectGroupCol = d2
            If d1 = "ElevationClass" Then i_ElevationClassCol = d2
            If d1 = "HT_GRP" Then i_HT_GRPCol = d2
            If d1 = "VMAP_Code" Then i_VMAPCodeCol = d2
            If d1 = "Acres" Then i_AcresCol = d2
            For j As Integer = 1 To NMinorSpecies
                If d1 = "Minor" & j & "Pct" Then i_MinorPctCol(j) = d2
            Next
            If d1 = "FirstCutClass" Then i_FirstCutClassCol = d2

        Loop
        FileClose(fnum)
    End Sub

    Private Sub LoadVMAPCrosswalk(ByVal FName As String)
        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String
        Dim kvcode As Integer

        fnum = FreeFile()
        FileOpen(fnum, FName, OpenMode.Input)
        dummy = LineInput(fnum)
        'first count stuff
        NVMAPCodes = 0
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            'arr = Split(dummy, ",")
            NVMAPCodes = NVMAPCodes + 1
        Loop
        FileClose(fnum)

        ReDim v_VMAPCode(NVMAPCodes), v_VMAP_SIMPPLLEDefault(NVMAPCodes)
        fnum = FreeFile()
        FileOpen(fnum, FName, OpenMode.Input)
        dummy = LineInput(fnum)
        'first count stuff
        kvcode = 0
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            kvcode = kvcode + 1
            arr = Split(dummy, ",")
            v_VMAPCode(kvcode) = Trim(arr(0))
            v_VMAP_SIMPPLLEDefault(kvcode) = Trim(arr(1))
        Loop
        FileClose(fnum)
    End Sub

    Private Sub LoadMinorSpeciesDefs(ByVal FName As String)
        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String
        Dim kms As Integer
        Dim khg As Integer

        'Dim NMinorSpecies As Integer = 7 'hardwired...for now
        'Dim ms_Index() As Integer
        'Dim ms_Name() As String
        'Dim ms_MinPct() As Single 'min percentage the species has to have to be considered...

        fnum = FreeFile()
        FileOpen(fnum, FName, OpenMode.Input)
        dummy = LineInput(fnum)
        'first count stuff
        NMinorSpecies = 0
        ReDim ms_Name(NMinorSpecies)
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            arr = Split(dummy, ",")
            kms = GetStrIndex(ms_Name, Trim(arr(1)))
            If kms < 0 Then
                NMinorSpecies = NMinorSpecies + 1
                ReDim Preserve ms_Name(NMinorSpecies)
                ms_Name(NMinorSpecies) = Trim(arr(1))
            End If
        Loop
        FileClose(fnum)

        ReDim ms_MinPct(NHabGroups, NMinorSpecies), ms_MinPctIter(NIters + 1, NHabGroups, NMinorSpecies) 'ms_Index(NMinorSpecies), 
        fnum = FreeFile()
        FileOpen(fnum, FName, OpenMode.Input)
        dummy = LineInput(fnum)
        'first count stuff
        kms = 0
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            arr = Split(dummy, ",")
            kms = GetStrIndex(ms_Name, Trim(arr(1)))
            khg = GetStrIndex(HabGroupName, Trim(arr(0)))
            'ms_Index(kms) = Trim(arr(0))
            'ms_Name(kms) = Trim(arr(1))
            If khg <= 0 Then 'apply this percentage to all HTG - unless it already has a value
                For jhg As Integer = 1 To NHabGroups
                    If ms_MinPct(jhg, kms) <= 0 Then
                        ms_MinPct(jhg, kms) = CType(Trim(arr(2)), Single)
                        ms_MinPctIter(0, jhg, kms) = ms_MinPct(jhg, kms)
                        ms_MinPctIter(1, jhg, kms) = ms_MinPct(jhg, kms)
                    End If

                Next
            Else
                ms_MinPct(khg, kms) = CType(Trim(arr(2)), Single) 'specific to this one
                ms_MinPctIter(0, khg, kms) = ms_MinPct(khg, kms)
                ms_MinPctIter(1, khg, kms) = ms_MinPct(khg, kms)
            End If

        Loop
        FileClose(fnum)
        OrigNMinorSpecies = NMinorSpecies
    End Sub

    Private Sub LoadManditoryOptionalReclass(ByVal FName As String)
        Dim fnum As Integer
        Dim dummy As String
        Dim d1 As String
        Dim arr() As String
        Dim MandOpt As String
        Dim kmand As Integer
        Dim kopt As Integer


        NMandatoryReclass = 0
        NOptionalReclass = 0
        fnum = FreeFile()
        FileOpen(fnum, FName, OpenMode.Input)
        dummy = LineInput(fnum)
        dummy = LineInput(fnum)
        arr = Split(dummy, ",")
        MandOpt = Trim(arr(0))
        'first count stuff
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            arr = Split(dummy, ",")
            d1 = Trim(arr(0))
            If d1 = "MandatoryReclasses" Or d1 = "OptionalReclasses" Then
                MandOpt = d1
                GoTo skipplace
            End If
            If MandOpt = "MandatoryReclasses" Then
                NMandatoryReclass = NMandatoryReclass + 1
            ElseIf MandOpt = "OptionalReclasses" Then
                NOptionalReclass = NOptionalReclass + 1
            End If
SkipPlace:
        Loop
        FileClose(fnum)

        bMandatoryReclass = False 'default
        If NMandatoryReclass > 0 Then
            bMandatoryReclass = True
            ReDim mr_LSGroup(NMandatoryReclass), mr_AspectGroup(NMandatoryReclass), mr_ElevationClass(NMandatoryReclass)
            ReDim mr_HT_GRP(NMandatoryReclass), mr_VMAP(NMandatoryReclass), mr_RECLASS(NMandatoryReclass), mr_NMatchesNeeded(NMandatoryReclass)
        End If
        bOptionalReclass = False 'default
        If NOptionalReclass > 0 Then
            bOptionalReclass = True
            ReDim or_LSGroup(NOptionalReclass), or_AspectGroup(NOptionalReclass), or_ElevationClass(NOptionalReclass)
            ReDim or_HT_GRP(NOptionalReclass), or_VMAP(NOptionalReclass), or_RECLASS(NOptionalReclass), or_NMatchesNeeded(NOptionalReclass)
        End If

        If bMandatoryReclass = True Or bOptionalReclass = True Then
            kmand = 0
            kopt = 0
            fnum = FreeFile()
            FileOpen(fnum, FName, OpenMode.Input)
            dummy = LineInput(fnum)
            dummy = LineInput(fnum)
            arr = Split(dummy, ",")
            MandOpt = Trim(arr(0))
            'first count stuff
            Do Until EOF(fnum)
                dummy = Trim(LineInput(fnum))
                If dummy = "" Then Exit Do
                arr = Split(dummy, ",")

                d1 = Trim(arr(0))
                If d1 = "MandatoryReclasses" Or d1 = "OptionalReclasses" Then
                    MandOpt = d1
                    GoTo skipplace2
                End If
                If MandOpt = "MandatoryReclasses" Then
                    kmand = kmand + 1
                    mr_LSGroup(kmand) = Trim(arr(0))
                    mr_AspectGroup(kmand) = Trim(arr(1))
                    mr_ElevationClass(kmand) = Trim(arr(2))
                    mr_HT_GRP(kmand) = Trim(arr(3))
                    mr_VMAP(kmand) = Trim(arr(4))
                    mr_RECLASS(kmand) = Trim(arr(5))
                    For jmcat As Integer = 0 To 4
                        If Trim(arr(jmcat)) <> "" Then mr_NMatchesNeeded(kmand) = mr_NMatchesNeeded(kmand) + 1
                    Next
                ElseIf MandOpt = "OptionalReclasses" Then
                    kopt = kopt + 1
                    or_LSGroup(kopt) = Trim(arr(0))
                    or_AspectGroup(kopt) = Trim(arr(1))
                    or_ElevationClass(kopt) = Trim(arr(2))
                    or_HT_GRP(kopt) = Trim(arr(3))
                    or_VMAP(kopt) = Trim(arr(4))
                    or_RECLASS(kopt) = Trim(arr(5))
                    For jocat As Integer = 0 To 4
                        If Trim(arr(jocat)) <> "" Then or_NMatchesNeeded(kopt) = or_NMatchesNeeded(kopt) + 1
                    Next
                End If
SkipPlace2:
            Loop

            FileClose(fnum)
        End If

    End Sub

    Private Sub LoadTabooDefs(ByVal FName As String)
        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String
        Dim ktab As Integer

        NTabooDefs = 0
        fnum = FreeFile()
        FileOpen(fnum, FName, OpenMode.Input)

        dummy = LineInput(fnum)
        dummy = LineInput(fnum)

        'first count stuff
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            NTabooDefs = NTabooDefs + 1
        Loop
        FileClose(fnum)

        bTabooDefs = False 'default
        If NTabooDefs > 0 Then
            bTabooDefs = True
            ReDim t_LSGroup(NTabooDefs), t_AspectGroup(NTabooDefs), t_ElevationClass(NTabooDefs), t_HT_GRP(NTabooDefs)
            ReDim t_VMAP(NTabooDefs), t_SPECIES(NTabooDefs), t_NMatchesNeeded(NTabooDefs)
        End If

        ktab = 0
        If bTabooDefs = True Then
            fnum = FreeFile()
            FileOpen(fnum, FName, OpenMode.Input)
            dummy = LineInput(fnum)
            dummy = LineInput(fnum)
            Do Until EOF(fnum)
                dummy = Trim(LineInput(fnum))
                If dummy = "" Then Exit Do
                arr = Split(dummy, ",")

                ktab = ktab + 1
                t_LSGroup(ktab) = Trim(arr(0))
                t_AspectGroup(ktab) = Trim(arr(1))
                t_ElevationClass(ktab) = Trim(arr(2))
                t_HT_GRP(ktab) = Trim(arr(3))
                t_VMAP(ktab) = Trim(arr(4))
                t_SPECIES(ktab) = Trim(arr(5))
                For jtcat As Integer = 0 To 4
                    If Trim(arr(jtcat)) <> "" Then t_NMatchesNeeded(ktab) = t_NMatchesNeeded(ktab) + 1
                Next

            Loop

            FileClose(fnum)
        End If

    End Sub

    Private Sub LoadMandatoryMS(ByVal FName As String)
        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String
        Dim kms As Integer

        NMandatoryMS = 0
        fnum = FreeFile()
        FileOpen(fnum, FName, OpenMode.Input)

        dummy = LineInput(fnum)
        dummy = LineInput(fnum)

        'first count stuff
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            NMandatoryMS = NMandatoryMS + 1
        Loop
        FileClose(fnum)

        bMandatoryMS = False 'default
        If NMandatoryMS > 0 Then
            bMandatoryMS = True
            ReDim mms_LSGroup(NMandatoryMS), mms_AspectGroup(NMandatoryMS), mms_ElevationClass(NMandatoryMS), mms_HT_GRP(NMandatoryMS)
            ReDim mms_VMAP(NMandatoryMS), mms_SPECIES(NMandatoryMS), mms_NMatchesNeeded(NMandatoryMS)
        End If

        kms = 0
        If bMandatoryMS = True Then
            fnum = FreeFile()
            FileOpen(fnum, FName, OpenMode.Input)
            dummy = LineInput(fnum)
            dummy = LineInput(fnum)
            Do Until EOF(fnum)
                dummy = Trim(LineInput(fnum))
                If dummy = "" Then Exit Do
                arr = Split(dummy, ",")

                kms = kms + 1
                mms_LSGroup(kms) = Trim(arr(0))
                mms_AspectGroup(kms) = Trim(arr(1))
                mms_ElevationClass(kms) = Trim(arr(2))
                mms_HT_GRP(kms) = Trim(arr(3))
                mms_VMAP(kms) = Trim(arr(4))
                mms_SPECIES(kms) = Trim(arr(5))
                For jtcat As Integer = 0 To 4
                    If Trim(arr(jtcat)) <> "" Then mms_NMatchesNeeded(kms) = mms_NMatchesNeeded(kms) + 1
                Next

            Loop

            FileClose(fnum)
        End If

    End Sub

    Private Sub LoadHTGSpeciesGoals(ByVal Filename As String)
        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String
        Dim ksp As Integer
        Dim khg As Integer


        'ms_Index(NMinorSpecies), 
        fnum = FreeFile()
        FileOpen(fnum, Filename, OpenMode.Input)
        dummy = LineInput(fnum)
        'first count stuff
        ksp = 0
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            arr = Split(dummy, ",")
            ksp = GetStrIndex(SpeciesName, Trim(arr(1)))
            khg = GetStrIndex(HabGroupName, Trim(arr(0)))
         
            If ksp > -1 And khg > -1 Then HTGSpeciesGoal(khg, ksp) = CType(Trim(arr(2)), Single) 'specific to this one


        Loop
        FileClose(fnum)

    End Sub

    Private Sub WriteMSMinPct(ByVal FilName As String)
        Dim fnum As Integer
        Dim dummy As String

        fnum = FreeFile()
        FileOpen(fnum, FilName, OpenMode.Output)
        PrintLine(fnum, "HTG,Species,Percentage")
        For jhg As Integer = 1 To NHabGroups
            For jspec As Integer = 1 To NMinorSpecies
                PrintLine(fnum, HabGroupName(jhg) & "," & ms_Name(jspec) & "," & ms_MinPct(jhg, jspec))
            Next
        Next

        FileClose(fnum)

        fnum = FreeFile()
        FileOpen(fnum, DirName(FilName) & FName(FilName) & "_iterReport.csv", OpenMode.Output)
        dummy = "HTG,Species,Initial_Pct"
        For jiter As Integer = 1 To NIters
            dummy = dummy & ",IterPct_" & jiter
        Next
        PrintLine(fnum, dummy)

        'print out the percentage used in each iteration
        For jhtg As Integer = 1 To NHabGroups
            For jspec As Integer = 1 To NMinorSpecies
                'If HTGSpeciesGoal(jhtg, jspec) > 0 Then
                dummy = HabGroupName(jhtg) & "," & ms_Name(jspec) & "," & Math.Round(ms_MinPctIter(0, jhtg, jspec), 0)
                For jiter As Integer = 1 To NIters
                    dummy = dummy & "," & Math.Round(ms_MinPctIter(jiter, jhtg, jspec), 0)
                    'PrintLine(fnum2, HabGroupName(jhtg) & "," & SpeciesName(jspec) & "," & Math.Round(HTGSpeciesAcres(kiteration, jhtg, jspec), 0))
                Next jiter
                PrintLine(fnum, dummy)
                'End If
            Next jspec
        Next jhtg

        FileClose(fnum)


    End Sub



    Private Sub WriteOutputTables(ByVal FileName As String, ByVal kiteration As Integer)
        Dim fnum As Integer
        Dim fnum2 As Integer
        Dim kctg As Integer
        Dim dtest As String
        Dim Different As Integer
        Dim CTGAcres() As Single
        Dim ChangedCTGAcres() As Single
        Dim SpeciesAcres() As Single
        Dim MissedAcres As Single
        Dim NMissedRecords As Integer
        'Dim FirstCutClass As String

        ReDim CTGAcres(NCTG), ChangedCTGAcres(NCTG)
        ReDim SpeciesAcres(NSpecies)

        NMissedRecords = 0
        MissedAcres = 0

        fnum = FreeFile()
        FileOpen(fnum, FileName, OpenMode.Input)
        dtest = LineInput(fnum) 'first line is the header

        fnum2 = FreeFile()
        dtest = DirName(FileName) & FName(FileName) & "_coded.csv"
        FileOpen(fnum2, dtest, OpenMode.Output)
        dtest = "Index,LSGroup,AspectGroup,ElevationClass,HT_GRP,VMAP_Code,Acres,"

        For jms As Integer = 1 To NMinorSpecies
            dtest = dtest & ms_Name(jms) & ","
        Next
        'If d1 = "FirstCutClass" Then i_FirstCutClassCol = d2
        If bKeepGoodCodes Then
            dtest = dtest & "FirstCutClass,GoodCodeKept,CodedVegType,Different"
        Else
            dtest = dtest & "CodedVegType"
        End If
        PrintLine(fnum2, dtest)

        For jpoly As Integer = 1 To NPolys

            dtest = Index(jpoly) & "," & LSGroup(jpoly) & "," & AspectGroup(jpoly) & "," & ElevationClass(jpoly) & "," & HT_GRP(jpoly) & "," & VMAP_Code(jpoly) & "," & Acres(jpoly) & ","
            For jms As Integer = 1 To OrigNMinorSpecies
                dtest = dtest & MinorPct(jpoly, jms) & ","
            Next
            Different = 0
            If bKeepGoodCodes = True Then
                'If bGoodCode = False Then Different = 1
                If GoodCodeKept(jpoly) = "NOT FOUND" Then
                    If CodeToWrite(jpoly) <> "NOT FOUND" Then
                        kctg = DetermineStrInd(CodeToWrite(jpoly), CTGName)
                        ChangedCTGAcres(kctg) = ChangedCTGAcres(kctg) + Acres(jpoly)
                        Different = 1
                    End If
                    GoodCodeKept(jpoly) = CodeToWrite(jpoly)
                End If
                If GoodCodeKept(jpoly) <> "NOT FOUND" Then
                    kctg = DetermineStrInd(GoodCodeKept(jpoly), CTGName)
                    CTGAcres(kctg) = CTGAcres(kctg) + Acres(jpoly)
                    For jspecies As Integer = 1 To NSpecies
                        If CTGroup(kctg, jspecies) = 1 Then
                            SpeciesAcres(jspecies) = SpeciesAcres(jspecies) + Acres(jpoly)
                            'this is calculated in the code polygon section
                            'HTGSpeciesAcres(HT_GRP_Ind(jpoly), jspecies) = HTGSpeciesAcres(HT_GRP_Ind(jpoly), jspecies) + Acres(jpoly)
                        End If
                    Next
                Else
                    NMissedRecords = NMissedRecords + 1
                    MissedAcres = MissedAcres + Acres(jpoly)
                End If
                'If GoodCodeKept = "NOT FOUND" Then Different = 1
                dtest = dtest & FirstCutClass(jpoly) & "," & GoodCodeKept(jpoly) & "," & CodeToWrite(jpoly) & "," & Different
            Else
                If CodeToWrite(jpoly) <> "NOT FOUND" Then
                    kctg = DetermineStrInd(CodeToWrite(jpoly), CTGName)
                    CTGAcres(kctg) = CTGAcres(kctg) + Acres(jpoly)
                    For jspecies As Integer = 1 To NSpecies
                        If CTGroup(kctg, jspecies) = 1 Then
                            SpeciesAcres(jspecies) = SpeciesAcres(jspecies) + Acres(jpoly)
                            'this is calculatd in the code polygon section
                            'HTGSpeciesAcres(HT_GRP_Ind(jpoly), jspecies) = HTGSpeciesAcres(HT_GRP_Ind(jpoly), jspecies) + Acres(jpoly)
                        End If
                    Next
                Else
                    NMissedRecords = NMissedRecords + 1
                    MissedAcres = MissedAcres + Acres(jpoly)
                End If
                'If CodeToWrite = "NOT FOUND" Then Different = 1
                dtest = dtest & CodeToWrite(jpoly) '& "," & Different
            End If
            PrintLine(fnum2, dtest)
            'Loop
        Next jpoly
        FileClose(fnum)
        FileClose(fnum2)

        'write out acres stuff

        fnum2 = FreeFile()
        dtest = DirName(FileName) & FName(FileName) & "_acretallies.csv"
        FileOpen(fnum2, dtest, OpenMode.Output)
        PrintLine(fnum2, "Missed_Records")
        PrintLine(fnum2, NMissedRecords)
        PrintLine(fnum2, "Missed_Acres")
        PrintLine(fnum2, MissedAcres)
        PrintLine(fnum2, "Changed_Records")
        PrintLine(fnum2, CountChanges)
        PrintLine(fnum2, "Covertype Group Changes")
        PrintLine(fnum2, "kCovertypeGroup,Covertype_Group_Name, Changed_Acres")
        For jctg As Integer = 1 To NCTG
            If ChangedCTGAcres(jctg) > 0 Then
                PrintLine(fnum2, jctg & "," & CTGName(jctg) & "," & ChangedCTGAcres(jctg))
            End If
        Next
        PrintLine(fnum2, "Total_CTG_Acres")
        PrintLine(fnum2, "kCovertypeGroup,Covertype_Group_Name,TotalAcres")
        For jctg As Integer = 1 To NCTG
            If CTGAcres(jctg) > 0 Then
                PrintLine(fnum2, jctg & "," & CTGName(jctg) & "," & Math.Round(CTGAcres(jctg), 0))
            End If
        Next
        PrintLine(fnum2, "Total_Species_Acres")
        PrintLine(fnum2, "kSpecies,Species_Name,TotalAcres")
        For jspec As Integer = 1 To NSpecies
            If SpeciesAcres(jspec) > 0 Then
                PrintLine(fnum2, jspec & "," & SpeciesName(jspec) & "," & Math.Round(SpeciesAcres(jspec), 0))
            End If
        Next
        PrintLine(fnum2, "Total_Species_Acres_By_HTG")
        PrintLine(fnum2, "HTG,Species_Name,Goal,TotalAcres")
        For jhtg As Integer = 1 To NHabGroups
            For jspec As Integer = 1 To NSpecies
                If HTGSpeciesAcres(kiteration, jhtg, jspec) > 0 Then
                    PrintLine(fnum2, HabGroupName(jhtg) & "," & SpeciesName(jspec) & "," & Math.Round(HTGSpeciesGoal(jhtg, jspec), 0) & "," & Math.Round(HTGSpeciesAcres(kiteration, jhtg, jspec), 0))
                End If
            Next
        Next jhtg
        PrintLine(fnum2, "Total_CTG_Acres_By_HTG")
        PrintLine(fnum2, "HTG,CTG_Name,TotalAcres")
        For jhtg As Integer = 1 To NHabGroups
            For jctg As Integer = 1 To NCTG
                If HTGCTGAcres(jhtg, jctg) > 0 Then
                    PrintLine(fnum2, HabGroupName(jhtg) & "," & CTGName(jctg) & "," & Math.Round(HTGCTGAcres(jhtg, jctg), 0))
                End If
            Next
        Next jhtg

        FileClose(fnum2)
    End Sub

    Private Sub WriteIterationResults(ByVal outfile As String)
        Dim fnum2 As Integer
        Dim dummy As String
        Dim kiteration As String
        Dim IterInd() As Integer
        ReDim IterInd(NSaveIterations)

        fnum2 = FreeFile()
        FileOpen(fnum2, outfile, OpenMode.Output)
        dummy = "HTG,Species,Goal,Initial_Acreage"
        For jsave As Integer = 1 To NSaveIterations
            dummy = dummy & ",Iteration_" & NIters - NSaveIterations + jsave
        Next
        PrintLine(fnum2, dummy)

        'find which index is associated with each iteration
        For jiter As Integer = 1 To NSaveIterations
            kiteration = NIters - NSaveIterations + jiter 'this is the iteration index you are searching for
            For jjiter As Integer = 1 To NSaveIterations
                If kiteration = IterKIteration(jjiter) Then
                    IterInd(jiter) = jjiter
                    Exit For
                End If
            Next

        Next

        For jhtg As Integer = 1 To NHabGroups
            For jspec As Integer = 1 To NSpecies
                If HTGSpeciesGoal(jhtg, jspec) > 0 Then
                    dummy = HabGroupName(jhtg) & "," & SpeciesName(jspec) & "," & Math.Round(HTGSpeciesGoal(jhtg, jspec), 0) & "," & Math.Round(HTGSpeciesAcres(0, jhtg, jspec), 0)
                    For jsave As Integer = 1 To NSaveIterations
                        kiteration = IterInd(jsave)
                        dummy = dummy & "," & Math.Round(HTGSpeciesAcres(kiteration, jhtg, jspec), 0)
                        'PrintLine(fnum2, HabGroupName(jhtg) & "," & SpeciesName(jspec) & "," & Math.Round(HTGSpeciesAcres(kiteration, jhtg, jspec), 0))
                    Next jsave
                    PrintLine(fnum2, dummy)
                End If
            Next jspec
        Next jhtg

        FileClose(fnum2)


    End Sub

    Private Function DetermineStrInd(ByVal val As String, ByVal Vals() As String) As Integer
        'searches the array to find the match
        DetermineStrInd = 0 'default
        For jind As Integer = 1 To Vals.Length - 1
            If val = Vals(jind) Then
                DetermineStrInd = jind
                Exit Function
            End If
        Next
    End Function

    Private Function DetermineCTGFeasibility(ByVal CTGStr As String, ByVal HTGInd As Integer, ByRef kctg As Integer) As Boolean
        Dim arr() As String
        'Dim kctg As Integer
        Dim nmatches As Integer
        Dim nmatchesneeded As Integer
        DetermineCTGFeasibility = False 'default
        arr = Split(CTGStr, "-")
        nmatchesneeded = arr.Length
        'find the CTGName based on the species
        kctg = 0
        For jctg As Integer = 1 To NCTG
            nmatches = 0
            If CTGNSpecies(jctg) = arr.Length Then 'test it
                For jspec As Integer = 0 To CTGNSpecies(jctg) - 1
                    For jspecies As Integer = 1 To NSpecies
                        If SpeciesName(jspecies) = arr(jspec) Then
                            'looking for jspecies index - found it
                            If CTGroup(jctg, jspecies) = 0 Then
                                DetermineCTGFeasibility = False
                                Exit For
                            Else
                                nmatches = nmatches + 1
                            End If
                        End If
                    Next
                Next jspec

            End If
            If nmatches = nmatchesneeded Then
                kctg = jctg
                'HabCTGFeasibility(kctg, jhg)
                If HabCTGFeasibility(kctg, HTGInd) = 1 Then
                    DetermineCTGFeasibility = True
                    Exit Function
                End If
            End If
        Next
    End Function

    Private Function DetermineSpeciesFeasibility(ByVal CTStr As String, ByVal HTGInd As Integer) As Boolean
        'looks to see if the the species occurs in any CTG in the Hab Group

        DetermineSpeciesFeasibility = False 'default

        For jspecies As Integer = 1 To NSpecies
            If CTStr = SpeciesName(jspecies) Then
                If HabSpeciesFeasibility(jspecies, HTGInd) = 1 Then
                    DetermineSpeciesFeasibility = True
                End If
                Exit For
            End If
        Next


    End Function

    Private Sub CheckManditoryReclass(ByVal tLSGroup As String, ByVal tAspectGroup As String, ByVal tElevationGroup As String, ByVal tHT_Group As String, ByVal tVMAP As String, ByRef NewCode As String)
        Dim nmatchesneeded As Integer
        Dim nmatches As Integer

        For jmr As Integer = 1 To NMandatoryReclass
            nmatches = 0
            nmatchesneeded = mr_NMatchesNeeded(jmr)
            If mr_LSGroup(jmr) <> "" And mr_LSGroup(jmr) = tLSGroup Then nmatches = nmatches + 1
            If mr_AspectGroup(jmr) <> "" And mr_AspectGroup(jmr) = tAspectGroup Then nmatches = nmatches + 1
            If mr_ElevationClass(jmr) <> "" And mr_ElevationClass(jmr) = tElevationGroup Then nmatches = nmatches + 1
            If mr_HT_GRP(jmr) <> "" And mr_HT_GRP(jmr) = tHT_Group Then nmatches = nmatches + 1
            If mr_VMAP(jmr) <> "" And mr_VMAP(jmr) = tVMAP Then nmatches = nmatches + 1
            If nmatches = nmatchesneeded Then
                NewCode = mr_RECLASS(jmr)
                Exit Sub
            End If
        Next
    End Sub

    Private Sub CheckOptionalReclass(ByVal tLSGroup As String, ByVal tAspectGroup As String, ByVal tElevationGroup As String, ByVal tHT_Group As String, ByVal tVMAP As String, ByRef NewCode As String)
        Dim nmatchesneeded As Integer
        Dim nmatches As Integer

        For jor As Integer = 1 To NOptionalReclass
            nmatches = 0
            nmatchesneeded = or_NMatchesNeeded(jor)
            If or_LSGroup(jor) <> "" And or_LSGroup(jor) = tLSGroup Then nmatches = nmatches + 1
            If or_AspectGroup(jor) <> "" And or_AspectGroup(jor) = tAspectGroup Then nmatches = nmatches + 1
            If or_ElevationClass(jor) <> "" And or_ElevationClass(jor) = tElevationGroup Then nmatches = nmatches + 1
            If or_HT_GRP(jor) <> "" And or_HT_GRP(jor) = tHT_Group Then nmatches = nmatches + 1
            If or_VMAP(jor) <> "" And or_VMAP(jor) = tVMAP Then nmatches = nmatches + 1
            If nmatches = nmatchesneeded Then
                NewCode = or_RECLASS(jor)
                Exit Sub
            End If
        Next
    End Sub

    Private Sub CheckMandatoryMS(ByVal tLSGroup As String, ByVal tAspectGroup As String, ByVal tElevationGroup As String, ByVal tHT_Group As String, ByVal tVMAP As String, ByRef NewCode() As String)
        Dim nmatchesneeded As Integer
        Dim nmatches As Integer
        Dim nms As Integer = 0
        Dim bnewcode As Boolean
        ReDim NewCode(nms)


        For jms As Integer = 1 To NMandatoryMS
            nmatches = 0
            nmatchesneeded = mms_NMatchesNeeded(jms)
            If mms_LSGroup(jms) <> "" And mms_LSGroup(jms) = tLSGroup Then nmatches = nmatches + 1
            If mms_AspectGroup(jms) <> "" And mms_AspectGroup(jms) = tAspectGroup Then nmatches = nmatches + 1
            If mms_ElevationClass(jms) <> "" And mms_ElevationClass(jms) = tElevationGroup Then nmatches = nmatches + 1
            If mms_HT_GRP(jms) <> "" And mms_HT_GRP(jms) = tHT_Group Then nmatches = nmatches + 1
            If mms_VMAP(jms) <> "" And mms_VMAP(jms) = tVMAP Then nmatches = nmatches + 1
            If nmatches = nmatchesneeded Then
                'is it new?
                bnewcode = True
                For jjms As Integer = 1 To nms
                    If NewCode(jjms) = mms_SPECIES(jms) Then
                        bnewcode = False
                        Exit For
                    End If
                Next
                If bnewcode = True Then
                    nms = nms + 1
                    ReDim Preserve NewCode(nms)
                    NewCode(nms) = mms_SPECIES(jms)
                End If
                'Exit Sub
            End If
        Next
    End Sub

    Private Function bTabooSpecies(ByVal tLSGroup As String, ByVal tAspectGroup As String, ByVal tElevationGroup As String, ByVal tHT_Group As String, ByVal tVMAP As String, ByVal CTCode As String) As Boolean
        Dim nmatchesneeded As Integer
        Dim nmatches As Integer

        'Dim NTabooDefs As Integer
        'Dim bTabooDefs As Boolean
        'Dim t_LSGroup() As String
        'Dim t_AspectGroup() As String
        'Dim t_ElevationClass() As String
        'Dim t_HT_GRP() As String
        'Dim t_VMAP() As String
        'Dim t_SPECIES() As String
        bTabooSpecies = False

        For jtd As Integer = 1 To NTabooDefs
            nmatches = 0
            nmatchesneeded = t_NMatchesNeeded(jtd)
            If t_LSGroup(jtd) <> "" And t_LSGroup(jtd) = tLSGroup Then nmatches = nmatches + 1
            If t_AspectGroup(jtd) <> "" And t_AspectGroup(jtd) = tAspectGroup Then nmatches = nmatches + 1
            If t_ElevationClass(jtd) <> "" And t_ElevationClass(jtd) = tElevationGroup Then nmatches = nmatches + 1
            If t_HT_GRP(jtd) <> "" And t_HT_GRP(jtd) = tHT_Group Then nmatches = nmatches + 1
            If t_VMAP(jtd) <> "" And t_VMAP(jtd) = tVMAP Then nmatches = nmatches + 1
            If nmatches = nmatchesneeded Then
                'have to for sure match the species that's taboo
                If CTCode = t_SPECIES(jtd) Then
                    'this is taboo
                    bTabooSpecies = True
                    Exit Function
                End If
            End If
        Next
    End Function


    Private Sub Deviations(ByVal kiteration As Integer, ByVal goaltolerance As Integer)
        'tallies up how far off you are from your goals
        ReDim ExcessDev(NHabGroups, NSpecies), DeficitDev(NHabGroups, NSpecies)
        For jhg As Integer = 1 To NHabGroups
            For jspec As Integer = 1 To NSpecies
                If HTGSpeciesGoal(jhg, jspec) > 0 Then
                    If HTGSpeciesAcres(kiteration, jhg, jspec) > HTGSpeciesGoal(jhg, jspec) * ((100 + goaltolerance) / 100) Then
                        ExcessDev(jhg, jspec) = Math.Abs(HTGSpeciesAcres(kiteration, jhg, jspec) - HTGSpeciesGoal(jhg, jspec))
                    ElseIf HTGSpeciesAcres(kiteration, jhg, jspec) < HTGSpeciesGoal(jhg, jspec) * ((100 - goaltolerance) / 100) Then
                        DeficitDev(jhg, jspec) = Math.Abs(HTGSpeciesAcres(kiteration, jhg, jspec) - HTGSpeciesGoal(jhg, jspec))
                    End If
                End If
            Next
        Next
    End Sub

    Private Sub NewPct(ByVal kiter As Integer)
        'adjusts the minor species percentages to try and better match the 
        Dim kspec As Integer
        Dim adj As Integer = 1 '0.01
        For jhg As Integer = 1 To NHabGroups
            For jms As Integer = 1 To NMinorSpecies
                kspec = GetStrIndex(SpeciesName, ms_Name(jms))
                If ExcessDev(jhg, kspec) > 0 Then
                    'HAVE TO RAISE THE MIN PERCENTAGE TO NOT GET AS MUCH
                    ms_MinPct(jhg, jms) = Math.Min(ms_MinPct(jhg, jms) + adj, 100)

                ElseIf DeficitDev(jhg, kspec) > 0 Then
                    'HAVE TO LOWER THE MIN PERCENTAGE TO GET MORE
                    ms_MinPct(jhg, jms) = Math.Max(ms_MinPct(jhg, jms) - adj, 0)
                End If
                ms_MinPctIter(kiter + 1, jhg, jms) = ms_MinPct(jhg, jms)
            Next
        Next
    End Sub





    

End Module
