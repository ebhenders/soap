Public Class clVMAPwithFIA
    'looks for the closest FIA plot with certain matching attributes
    Dim ClosestPlot() As String 'indexed by VMAP polygon ID
    Dim ClosestDistance() As Single 'the distance to the matched plot
    'Dim MaxAttMatches() As Integer
    'Dim MaxAttMatchesPlot() As Integer
    Dim NPolys As Integer
    Dim SearchAtts(,) As Integer 'indexed by attribute, iteration
    Dim NAttMatchesThisIter() As Integer 'number of matches required for the iteration
    Dim Atts() As String 'tally of all unique attributes to search on
    Dim IterMatch() As Integer 'the index of the iteration where the polygon was matched to a plot
    Dim NAtts As Integer
    Dim NIters As Integer
    Dim SearchDist() As Double 'indexed by iteration. Each iteration must have a max distance
    Dim DistanceFlag As String = "DISTANCE"
    Dim IterationFlag As String = "ITERATION"
    Dim XField As String = "POINT_X"
    Dim YField As String = "POINT_Y"

    Dim NPlots As Integer
    Dim PlotAtts(,) As String 'Indexed by Plot, Attribute. attribute values of the plots, including x and y coordinates. 
    Dim PolyAtts(,) As String 'indexed by Polygon, Attribute. 
    Dim PlotX() As Double
    Dim PlotY() As Double
    Dim PolyX() As Double
    Dim PolyY() As Double

    Dim NMissedPolys As Integer


    Public Sub RunClosestPlot(ByVal VMAPFile As String, ByVal FIAFile As String, ByVal SearchFile As String)
        Dim outfile As String
        outfile = DirName(VMAPFile) & FName(VMAPFile) & "_ClosestPlot.txt"
        ReadSearchFile(SearchFile)
        ReadPlotFile(FIAFile)
        ReadPolyFile(VMAPFile)
        FindClosestPoly()
        WriteClosestPlot(outfile)

    End Sub

    Private Sub FindClosestPoly()
        'once you have the input files read, we can loop through the iterations

        Dim NMatches As Integer
        'Dim NTMatches As Integer 'total matches; not just required for this iteration
        Dim dist As Double
        Dim mindist As Double
        Dim closestdist() As Double
        Dim bSearchIter() As Boolean
        'Dim IterMatch() As Integer

        NMissedPolys = NPolys
        ReDim ClosestPlot(NPolys), IterMatch(NPolys), ClosestDistance(NPolys), IterMatch(NPolys) ', MaxAttMatches(NPolys), MaxAttMatchesPlot(NPolys)



        For jpoly As Integer = 1 To NPolys
            ReDim closestdist(NIters), bSearchIter(NIters)
            For jiter As Integer = 1 To NIters
                closestdist(jiter) = SearchDist(jiter) ^ 2 + 2
                bSearchIter(jiter) = True
            Next
            'If ClosestPlot(jpoly) = 0 Then 'first test to see if you have a closest plot based on a previous iteration
            For jplot As Integer = 1 To NPlots
                dist = (PolyX(jpoly) - PlotX(jplot)) ^ 2 + (PolyY(jpoly) - PlotY(jplot)) ^ 2
                For jiter As Integer = 1 To NIters
                    'If NMissedPolys = 0 Then Exit For 'already have matched everything; don't have to try a lesser match
                    mindist = SearchDist(jiter) ^ 2 'do this so you don't have to calculate the square root of everything - hopefully speeds search 'is it close enough?
                    If dist < mindist And bSearchIter(jiter) = True Then
                        'now search through the attributes
                        NMatches = 0
                        'NTMatches = 0
                        For jatt As Integer = 1 To NAtts
                            If SearchAtts(jatt, jiter) = 1 Then
                                If PolyAtts(jpoly, jatt) = PlotAtts(jplot, jatt) Then
                                    NMatches = NMatches + 1
                                Else
                                    Exit For 'missed one - doesn't count - move along
                                End If
                            End If
                            'If PolyAtts(jpoly, jatt) = PlotAtts(jplot, jatt) Then NTMatches = NTMatches + 1
                        Next
                        If NAttMatchesThisIter(jiter) = NMatches Then
                            'plot matches all necessary criteria; is it the closest?
                            If dist < closestdist(jiter) Then
                                ClosestPlot(jpoly) = jplot
                                IterMatch(jpoly) = jiter
                                closestdist(jiter) = dist
                                For jjiter As Integer = jiter To NIters
                                    bSearchIter(jjiter) = False 'found a match on this iteration, so don't search the next one
                                Next
                            End If
                        End If
                    End If 'within the distance
                Next jiter
            Next

            If ClosestPlot(jpoly) <> 0 Then
                NMissedPolys = NMissedPolys - 1
                'IterMatch(jpoly) = jiter
                ClosestDistance(jpoly) = Math.Round(Math.Sqrt(closestdist(IterMatch(jpoly))), 0)
            End If

        Next

    End Sub

    Private Sub WriteClosestPlot(ByVal outfile As String)
        Dim fnum As Integer
        Dim dum As String

        fnum = FreeFile()
        FileOpen(fnum, outfile, OpenMode.Output)
        PrintLine(fnum, "Polygons without Closest Plot: " & NMissedPolys & " Out of " & NPolys)
        PrintLine(fnum, "")
        dum = "PolyNumber,ClosestPlotIndex,IterationRuleUsed,Distance"
        For jatt As Integer = 1 To NAtts
            dum = dum & "," & Atts(jatt)
        Next
        PrintLine(fnum, dum)
        For jpoly As Integer = 1 To NPolys
            dum = jpoly
            If ClosestPlot(jpoly) > 0 Then dum = dum & "," & ClosestPlot(jpoly) & "," & IterMatch(jpoly) & "," & ClosestDistance(jpoly)
            If ClosestPlot(jpoly) = 0 Then dum = dum & ",#,#,#"
            For jatt As Integer = 1 To NAtts
                dum = dum & "," & PolyAtts(jpoly, jatt)
            Next
            PrintLine(fnum, dum)
        Next
        FileClose(fnum)
    End Sub



    Private Sub ReadSearchFile(ByVal infile As String)
        'INPUT FILE FORMAT:
        '   Each iteration begins with a line "ITERATION" to match the IterationFlag
        '   Each line after ITERATION has a single name of the attribute to use for matching. This must match the field name in both VMAP and FIA input files
        '   The last lines of each ITERATION should be "DISTANCE" (one line) followed by a line with a number that defines the search radius in units the same as the x/y coordinates
        '   Blank lines are permitted to space the iterations out
        Dim fnum As Integer
        Dim dummy As String
        Dim katt As Integer
        Dim kiter As Integer


        NIters = 0
        NAtts = 0
        ReDim Atts(NAtts)
        fnum = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        'first search for the unique attributes that can be matched on
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = IterationFlag Then
                NIters = NIters + 1
            ElseIf dummy = DistanceFlag Then
                dummy = Trim(LineInput(fnum)) 'the actual distance is on the next line - just read it as a non-issue right now
            ElseIf dummy <> "" Then
                katt = GetStrIndex(Atts, dummy)
                If katt < 0 Then
                    NAtts = NAtts + 1
                    ReDim Preserve Atts(NAtts)
                    Atts(NAtts) = dummy
                End If
            End If
        Loop
        FileClose(fnum)

        fnum = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        'read this again to populate the iteration arrays
        ReDim SearchAtts(NAtts, NIters), SearchDist(NIters), NAttMatchesThisIter(NIters)
        kiter = 0
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = IterationFlag Then
                kiter = kiter + 1
            ElseIf dummy = DistanceFlag Then
                SearchDist(kiter) = Trim(LineInput(fnum)) 'the actual distance is on the next line - just read it as a non-issue right now
            ElseIf dummy <> "" Then
                katt = GetStrIndex(Atts, dummy)
                SearchAtts(katt, kiter) = 1 'use this attribute to match the search
                NAttMatchesThisIter(kiter) = NAttMatchesThisIter(kiter) + 1
            End If
        Loop
        FileClose(fnum)

    End Sub

    Private Sub ReadPolyFile(ByVal infile As String)
        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String
        Dim kpoly As Integer

        Dim iAttInd() As Integer 'field index for each attribute in the file
        Dim XCoordInd As Integer
        Dim YCoordInd As Integer

        ReDim iAttInd(NAtts)
        NPolys = 0

        fnum = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        dummy = LineInput(fnum)
        arr = Split(dummy, ",")
        'determine the column number for each attribute to match on...
        For j As Integer = 0 To arr.Length - 1
            For jatt As Integer = 1 To NAtts
                If arr(j) = Atts(jatt) Then
                    iAttInd(jatt) = j
                    Exit For
                End If
            Next
            If arr(j) = XField Then XCoordInd = j
            If arr(j) = YField Then YCoordInd = j
        Next
        'count the poly records
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            NPolys = NPolys + 1
        Loop
        FileClose(fnum)

        ReDim PolyAtts(NPolys, NAtts), PolyX(NPolys), PolyY(NPolys)

        fnum = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        dummy = LineInput(fnum) 'header line
        kpoly = 0
        'fill the attribute arrays
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            kpoly = kpoly + 1
            arr = Split(dummy, ",")
            For jatt As Integer = 1 To NAtts
                PolyAtts(kpoly, jatt) = arr(iAttInd(jatt))
            Next
            PolyX(kpoly) = arr(XCoordInd)
            PolyY(kpoly) = arr(YCoordInd)
        Loop
        FileClose(fnum)

    End Sub

    Private Sub ReadPlotFile(ByVal infile As String)
        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String
        Dim kplot As Integer

        Dim iAttInd() As Integer 'field index for each attribute in the file
        Dim XCoordInd As Integer
        Dim YCoordInd As Integer

        ReDim iAttInd(NAtts)
        NPlots = 0

        fnum = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        dummy = LineInput(fnum)
        arr = Split(dummy, ",")
        'determine the column number for each attribute to match on...
        For j As Integer = 0 To arr.Length - 1
            For jatt As Integer = 1 To NAtts
                If arr(j) = Atts(jatt) Then
                    iAttInd(jatt) = j
                    Exit For
                End If
            Next
            If arr(j) = XField Then XCoordInd = j
            If arr(j) = YField Then YCoordInd = j
        Next
        'count the plot records
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            NPlots = NPlots + 1
        Loop
        FileClose(fnum)

        ReDim PlotAtts(NPlots, NAtts), PlotX(NPlots), PlotY(NPlots)

        fnum = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        dummy = LineInput(fnum) 'header line
        kplot = 0
        'fill the attribute arrays
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            kplot = kplot + 1
            arr = Split(dummy, ",")
            For jatt As Integer = 1 To NAtts
                PlotAtts(kplot, jatt) = arr(iAttInd(jatt))
            Next
            PlotX(kplot) = arr(XCoordInd)
            PlotY(kplot) = arr(YCoordInd)
        Loop
        FileClose(fnum)

    End Sub
End Class
