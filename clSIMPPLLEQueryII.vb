Imports System.IO.Compression
Imports System.Text
Public Class clSIMPPLLEQueryII
#Region "Declarations"
    Dim bLoadSuccessful As Boolean
    Dim SpatialRelFileName() As String
    Dim NSRFiles As Integer

    Dim GridDefFileName() As String
    Dim NGridDefFiles As Integer

    Dim NSpecSizeClass As Integer

    Dim TreatmentNameXW() As String
    Dim NTreatmentXW As Integer
    Dim ProcessNameXW() As String
    Dim NProcessXW As Integer
    Dim ProcessNameLUT() As String
    Dim NProcessLUT As Integer

    Dim SizeClassNameLUT() As String
    Dim NSizeClassLUT As Integer
    Dim SizeClassNameXW() As String
    Dim NSizeClassXW As Integer

    Dim DensityNameLUT() As String
    Dim NDensityLUT As Integer
    Dim DensityNameXW() As String
    Dim NDensityXW As Integer

    Dim LifeFormName() As String
    Dim NLifeForm As Integer

    Dim SpeciesNameXW() As String
    Dim NSpeciesXW As Integer
    Dim SpeciesNameLUT() As String
    Dim NSpeciesLUT As Integer
    Dim FireProcessNameLUT() As String
    Dim NFireProcessLUT As Integer

    Dim HabGroupNameXW() As String
    Dim NHabGroupXW As Integer
    Dim HabGroupNameLUT() As String
    Dim NHabGroupLUT As Integer
    Dim OwnershipName() As String
    Dim SpecialAreaName() As String
    Dim NOwnerships As Integer
    Dim NSpecialAreas As Integer
    'Dim UseSuitCode(,) As Integer '0/1

    'Dim AAHabCall(,,) As Byte 'by slink, #wildlife species, time period

    Dim LastAdjFN As String
    Dim LastInitCondFN As String
    Dim LastGridDefFN As String

    'for fire spread
    Dim fireFiles() As String
    Dim fireNSLINK() As Long
    Dim fireNPers() As Integer
    Dim fireUseTimestep() As Integer

    Dim MinX() As Single
    Dim MaxX() As Single
    Dim MinY() As Single
    Dim MaxY() As Single

    Dim NSpatMetrics As Integer 'number of species that we are doing spatial metrics
    Dim SpatMetricSpeciesIndex() As Integer ' the associated index of each species with a spatial metric.


#End Region


    'Public Sub RunQueryModule()
    '    Dim outfilename As String
    '    Dim haboutfilename As String
    '    Dim patchbasefn As String
    '    Dim gridfilebasename As String
    '    Dim habsummfilename As String
    '    Dim processsummfilename As String
    '    Dim processoutfilename As String
    '    Dim arr() As String
    '    Dim arr2() As String
    '    Dim ind As Integer
    '    Dim firefilename As String
    '    Dim dummy As String
    '    Dim fnum As Integer
    '    Dim nrg As Integer
    '    Dim bWritePatchFile() As Boolean
    '    Dim PatchFileListName() As String
    '    Dim NPatchFileLists As Integer
    '    Dim kfile As Integer
    '    Dim txtcsv As String

    '    ReadICfileNames(ICDefFileName)
    '    ReadOutputsToPocess(OutFilesToProcessFileName)
    '    If NICFiles <> NOutFiles Then
    '        MsgBox("Check Definition Files - number of IC files does not match number of output files to process")
    '        Exit Sub
    '    End If
    '    Dim NRuns As Integer = NICFiles


    '    DefineLUTFileNames(LUTBaseFolder)
    '    'first, get basic LUT information
    '    ReDim DensityNameLUT(0), HabGroupNameLUT(0), ProcessNameLUT(0), OwnershipName(0), SpecialAreaName(0), SizeClassNameLUT(0), SpeciesNameLUT(0)
    '    LoadStandardLUTs()

    '    For jof As Integer = 1 To NOutFiles
    '        'determine the type of the output files - txt or csv
    '        txtcsv = "csv" 'default
    '        If System.IO.File.Exists(OutFilesToProcessDir(jof) & "\EVU_SIM_DATA" & OutFilesToProcessNumber(jof) & ".txt") Then txtcsv = "txt"
    '        FillCodesRunOutput(OutFilesToProcessDir(jof), txtcsv)
    '    Next
    '    LoadLUTs()
    '    If bLoadSuccessful = False Then Exit Sub
    '    Dim fnumhab As Integer '= FreeFile()
    '    habsummfilename = DirName(HabOutputFN) & "QueryFileList.txt"
    '    'this clears out any existing information that may have been in there
    '    fnumhab = FreeFile()
    '    FileOpen(fnumhab, habsummfilename, OpenMode.Output)
    '    FileClose(fnumhab)

    '    processoutfilename = "" 'default
    '    processsummfilename = DirName(HabOutputFN) & "ProcessFileList.txt" 'default
    '    If bWriteProcessStats = True Then 'this clears out any existing information that may have been in there
    '        fnumhab = FreeFile()
    '        FileOpen(fnumhab, processsummfilename, OpenMode.Output)
    '        FileClose(fnumhab)
    '    End If

    '    'print out batch file information
    '    Dim fnum2 As Integer
    '    If bWriteGrids Then
    '        fnum2 = FreeFile()
    '        FileOpen(fnum2, DirName(HabOutputFN) & "BatchGridForFragstats.fbt", OpenMode.Output)
    '    End If
    '    ReDim fireFiles(NRuns) 'default; even to leave it blank is OK for writing out map files
    '    If bWriteFireSpreadMaps Then
    '        ReDim fireFiles(NRuns), fireNSLINK(NRuns), fireNPers(NRuns), fireUseTimestep(MaxTimestep) 'timestep information set when you click "go"
    '        For jrun As Integer = 1 To NRuns
    '            'get the name of the prob file
    '            arr = Split(OutFilesToProcessDir(jrun), "\")
    '            ind = arr.Length - 2
    '            arr2 = Split(arr(ind), "-")
    '            firefilename = ""
    '            For j As Integer = 0 To ind
    '                firefilename = firefilename & arr(j) & "\"
    '            Next
    '            For j As Integer = 0 To arr2.Length - 2
    '                firefilename = firefilename & arr2(j) & "-"
    '            Next
    '            firefilename = firefilename & "firespread-" & OutFilesToProcessNumber(jrun) & ".txt"
    '            fireFiles(jrun) = firefilename
    '            fireNPers(jrun) = MaxTimestep ' set at form level when you click "go"
    '        Next
    '        'timestep information set when you click "go"
    '        For jtime As Integer = 1 To MaxTimestep
    '            For ttime As Integer = 1 To NTimestep
    '                If jtime = TimestepIndex(ttime) Then
    '                    'this timestep is used
    '                    fireUseTimestep(jtime) = 1
    '                    Exit For 'go to the next jtime
    '                End If
    '            Next
    '        Next

    '    End If

    '    'names of files with patch statistics
    '    ReDim bWritePatchFile(NWLSpecies)
    '    NPatchFileLists = 0
    '    ReDim PatchFileListName(NPatchFileLists)
    '    Dim fnumpatch As Integer
    '    For jspec As Integer = 1 To NWLSpecies
    '        'PatchFilter(jspec) = False 'default
    '        If WLSpeciesMinPatchSize(jspec) > 0 Or WLSpeciesMaxPatchSize(jspec) > 0 Then
    '            bWritePatchFile(jspec) = True
    '            If bSplitMaps = False Then
    '                NPatchFileLists = NPatchFileLists + 1
    '                ReDim Preserve PatchFileListName(NPatchFileLists)
    '                PatchFileListName(NPatchFileLists) = DirName(HabOutputFN) & "PatchFiles_" & WLSpeciesName(jspec) & ".txt"
    '                fnumpatch = FreeFile()
    '                FileOpen(fnumpatch, PatchFileListName(NPatchFileLists), OpenMode.Output)
    '                FileClose(fnumpatch)
    '            End If
    '            If bSplitMaps = True Then
    '                For jgroup As Integer = 1 To NReportGroups
    '                    NPatchFileLists = NPatchFileLists + 1
    '                    ReDim Preserve PatchFileListName(NPatchFileLists)
    '                    PatchFileListName(NPatchFileLists) = DirName(HabOutputFN) & "PatchFiles_" & ReportGroupName(jgroup) & WLSpeciesName(jspec) & ".txt"
    '                    fnumpatch = FreeFile()
    '                    FileOpen(fnumpatch, PatchFileListName(NPatchFileLists), OpenMode.Output)
    '                    FileClose(fnumpatch)
    '                Next
    '            End If
    '        End If
    '    Next jspec



    '    ReDim MinX(NRuns), MaxX(NRuns), MinY(NRuns), MaxY(NRuns)

    '    For jrun As Integer = 1 To NRuns
    '        txtcsv = "csv" 'default
    '        If System.IO.File.Exists(OutFilesToProcessDir(jrun) & "\EVU_SIM_DATA" & OutFilesToProcessNumber(jrun) & ".txt") Then txtcsv = "txt"

    '        DefineFileNames(OutFilesToProcessDir(jrun), OutFilesToProcessNumber(jrun), txtcsv)
    '        LoadSimCrosswalkFiles()
    '        If bWriteGrids = False Then
    '            LastGridDefFN = "dummy"
    '            LoadSIMPPLLEOutput(ICFileName(jrun), SpatialRelFileName(jrun), SimpplleOutputFN, "dummy") 'sets nslink
    '        ElseIf bWriteGrids = True Then
    '            LoadSIMPPLLEOutput(ICFileName(jrun), SpatialRelFileName(jrun), SimpplleOutputFN, GridDefFileName(jrun))
    '        End If
    '        MinX(jrun) = MinCol
    '        MaxX(jrun) = MaxCol
    '        MinY(jrun) = MinRow
    '        MaxY(jrun) = MaxRow

    '        If bWriteFireSpreadMaps Then fireNSLINK(jrun) = NSLINK
    '        outfilename = DirName(HabOutputFN) & OutFilesToProcessName(jrun) & OutFilesToProcessNumber(jrun) & ".txt"
    '        haboutfilename = DirName(HabOutputFN) & FName(HabOutputFN) & OutFilesToProcessName(jrun) & OutFilesToProcessNumber(jrun) & ".txt"
    '        fnumhab = FreeFile()
    '        FileOpen(fnumhab, habsummfilename, OpenMode.Append)
    '        PrintLine(fnumhab, OutFilesToProcessNumber(jrun) & "," & haboutfilename)
    '        FileClose(fnumhab)
    '        If bWriteProcessStats Then
    '            fnumhab = FreeFile()
    '            processoutfilename = DirName(outfilename) & FName(outfilename) & "_ProcessStats.txt"
    '            FileOpen(fnumhab, processsummfilename, OpenMode.Append)
    '            PrintLine(fnumhab, OutFilesToProcessNumber(jrun) & "," & processoutfilename)
    '            FileClose(fnumhab)
    '        End If
    '        patchbasefn = DirName(HabOutputFN) & FName(HabOutputFN) & OutFilesToProcessName(jrun) & OutFilesToProcessNumber(jrun)
    '        CodePolygonHabitats(outfilename, haboutfilename, patchbasefn, processoutfilename, jrun)
    '        If bWriteGrids Then
    '            gridfilebasename = DirName(HabOutputFN) & "GRID"
    '            WriteGridOF(gridfilebasename, OutFilesToProcessNumber(jrun), fnum2)
    '        End If

    '        'now write spatial file names

    '        'If bNeedSpatArrays = True Then
    '        kfile = 0
    '        For jspec As Integer = 1 To NWLSpecies
    '            'PatchFilter(jspec) = False 'default
    '            If bWritePatchFile(jspec) = True Then
    '                'collect the patch statistics for this species, this timestep
    '                'PatchFilter(jspec) = True
    '                If bSplitMaps = False Then
    '                    dummy = patchbasefn & "Patches" & WLSpeciesName(jspec) & ".txt"
    '                    kfile = kfile + 1
    '                    fnumhab = FreeFile()
    '                    FileOpen(fnumhab, PatchFileListName(kfile), OpenMode.Append)
    '                    PrintLine(fnumhab, OutFilesToProcessNumber(jrun) & "," & dummy)
    '                    FileClose(fnumhab)
    '                End If

    '                If bSplitMaps = True Then
    '                    For jgroup As Integer = 1 To NReportGroups
    '                        dummy = patchbasefn & "Patches" & ReportGroupName(jgroup) & WLSpeciesName(jspec) & ".txt"
    '                        kfile = kfile + 1
    '                        fnumhab = FreeFile()
    '                        FileOpen(fnumhab, PatchFileListName(kfile), OpenMode.Append)
    '                        PrintLine(fnumhab, OutFilesToProcessNumber(jrun) & "," & dummy)
    '                        FileClose(fnumhab)
    '                    Next
    '                End If
    '            End If
    '            If HomeRange(jspec) > 0 Then

    '            End If
    '        Next jspec
    '        'End If

    '    Next

    '    If bWriteFireSpreadMaps Then
    '        Dim convert As New clProcessSimpplle
    '        convert.ConvertFireSpreadtoMMaPPit(fireFiles, fireNSLINK, fireNPers, fireUseTimestep)
    '        convert = Nothing
    '    End If

    '    'write out map files for MMaPPit
    '    If bWritePolyAtts Then
    '        For jrun As Integer = 1 To NRuns
    '            For jspec As Integer = 1 To NWLSpecies
    '                nrg = NReportGroups
    '                If bSplitMaps = False Then nrg = 1
    '                For jgroup As Integer = 1 To nrg
    '                    If bSplitMaps = False Then haboutfilename = DirName(HabOutputFN) & OutFilesToProcessName(jrun) & OutFilesToProcessNumber(jrun) & "_" & WLSpeciesName(jspec) & ".txt"
    '                    If bSplitMaps = True Then haboutfilename = DirName(HabOutputFN) & OutFilesToProcessName(jrun) & OutFilesToProcessNumber(jrun) & "_" & ReportGroupName(jgroup) & "_" & WLSpeciesName(jspec) & ".txt"
    '                    fnum = FreeFile()
    '                    FileOpen(fnum, DirName(haboutfilename) & "MapFile_" & FName(haboutfilename) & ".mmpt", OpenMode.Output)
    '                    PrintLine(fnum, ICFileName(jrun)) 'attributesall ICFileName
    '                    PrintLine(fnum, haboutfilename) 'period by period
    '                    PrintLine(fnum, DirName(HabOutputFN) & "MapColors_" & WLSpeciesName(jspec) & ".clr") 'color file
    '                    PrintLine(fnum, "XY")
    '                    If System.IO.File.Exists(fireFiles(jrun)) = True Then
    '                        PrintLine(fnum, DirName(fireFiles(jrun)) & FName(fireFiles(jrun)) & "_MMaPPit.txt")
    '                    Else
    '                        PrintLine(fnum, "Fire File Not Defined")
    '                    End If
    '                    PrintLine(fnum, "FireEventColor,0,0,0")
    '                    PrintLine(fnum, "FireOriginColor,255,0,0")
    '                    PrintLine(fnum, "Extent," & MinX(jrun) & "," & MaxX(jrun) & "," & MinY(jrun) & "," & MaxY(jrun)) ')
    '                    PrintLine(fnum, "Window,0")
    '                    FileClose(fnum)
    '                Next jgroup
    '            Next
    '        Next jrun
    '    End If
    '    If bWriteProcessMaps Then 'write out .mmpt files for MMaPPit
    '        For jrun As Integer = 1 To NRuns

    '            fnum = FreeFile()
    '            FileOpen(fnum, DirName(HabOutputFN) & "MapFile_" & OutFilesToProcessName(jrun) & OutFilesToProcessNumber(jrun) & "_Process.mmpt", OpenMode.Output)
    '            PrintLine(fnum, ICFileName(jrun)) 'attributesall ICFileName
    '            PrintLine(fnum, DirName(HabOutputFN) & OutFilesToProcessName(jrun) & OutFilesToProcessNumber(jrun) & "_Process.txt") 'period by period
    '            PrintLine(fnum, DirName(HabOutputFN) & "MapColors_Process.clr") 'color file
    '            PrintLine(fnum, "XY")
    '            If System.IO.File.Exists(fireFiles(jrun)) = True Then
    '                PrintLine(fnum, DirName(fireFiles(jrun)) & FName(fireFiles(jrun)) & "_MMaPPit.txt")
    '            Else
    '                PrintLine(fnum, "Fire File Not Defined")
    '            End If
    '            PrintLine(fnum, "FireEventColor,0,0,0") ''
    '            PrintLine(fnum, "FireOriginColor,200,0,0")
    '            PrintLine(fnum, "Extent," & MinX(jrun) & "," & MaxX(jrun) & "," & MinY(jrun) & "," & MaxY(jrun))
    '            PrintLine(fnum, "Window,0")
    '            FileClose(fnum)

    '        Next jrun
    '        FileClose(fnum)

    '    End If

    '    If bWriteGrids Then FileClose(fnum2)
    '    MsgBox("Done")

    'End Sub

    'Private Sub ReadICfileNames(ByVal filename As String)
    '    Dim fnum As Integer = FreeFile()
    '    Dim dummy As String
    '    FileOpen(fnum, filename, OpenMode.Input)
    '    dummy = Trim(LineInput(fnum))
    '    NICFiles = 0
    '    Do Until EOF(fnum)
    '        dummy = Trim(LineInput(fnum))
    '        If dummy = "" Or Trim(dummy) = "Spatial Relate File Names" Then Exit Do
    '        NICFiles = NICFiles + 1
    '        ReDim Preserve ICFileName(NICFiles)
    '        ICFileName(NICFiles) = dummy
    '    Loop
    '    NSRFiles = 0
    '    Do Until EOF(fnum)
    '        dummy = Trim(LineInput(fnum))
    '        If dummy = "" Or Trim(dummy) = "Initial Conditions File - Database Export (For Optional Grid Writer)" Then Exit Do
    '        NSRFiles = NSRFiles + 1
    '        ReDim Preserve SpatialRelFileName(NSRFiles)
    '        SpatialRelFileName(NSRFiles) = dummy
    '    Loop
    '    If bWriteGrids = True Then
    '        'Do Until EOF(fnum)
    '        'dummy = Trim(LineInput(fnum))
    '        'If dummy = "" Then Exit Do
    '        'CHANGED BECAUSE YOU CAN GET THE x/y stuff from .attributesall
    '        NGridDefFiles = NICFiles
    '        ReDim GridDefFileName(NGridDefFiles)
    '        For j As Integer = 1 To NICFiles
    '            'NGridDefFiles = NGridDefFiles + 1
    '            'ReDim Preserve GridDefFileName(NGridDefFiles)
    '            GridDefFileName(j) = ICFileName(j)
    '        Next j
    '        'Loop
    '    End If

    '    FileClose(fnum)

    'End Sub
    'Private Sub ReadOutputsToPocess(ByVal filename As String)
    '    Dim fnum As Integer = FreeFile()
    '    Dim dummy As String
    '    Dim arr() As String

    '    'Dim OutFilesToProcess() As String
    '    'Dim NOutFiles As Integer
    '    FileOpen(fnum, filename, OpenMode.Input)
    '    NOutFiles = 0
    '    Do Until EOF(fnum)
    '        dummy = Trim(LineInput(fnum))
    '        If dummy = "" Then Exit Do
    '        arr = Split(dummy, ",")
    '        NOutFiles = NOutFiles + 1
    '        ReDim Preserve OutFilesToProcessDir(NOutFiles), OutFilesToProcessName(NOutFiles), OutFilesToProcessNumber(NOutFiles)
    '        OutFilesToProcessDir(NOutFiles) = arr(0)
    '        OutFilesToProcessName(NOutFiles) = arr(1)
    '        OutFilesToProcessNumber(NOutFiles) = arr(2)
    '    Loop
    '    FileClose(fnum)
    'End Sub

    'Private Sub CodePolygonHabitats(ByVal outfile1 As String, ByVal habsumoutfile As String, ByVal BasePatchFN As String, ByVal processoutfilename As String, ByVal krun As Integer)
    '    'Dim str As String
    '    Dim strb As StringBuilder
    '    Dim str1 As String
    '    'Dim strGIS As String
    '    Dim strGISb As StringBuilder
    '    Dim dumstr As String
    '    Dim khg As Integer
    '    Dim kct As Integer
    '    Dim ksc As Integer
    '    Dim kdn As Integer
    '    Dim twltype As Integer
    '    Dim kwltype As Integer
    '    Dim ktic As Integer
    '    Dim kburn As Integer

    '    Dim khabyesno As Integer
    '    Dim kpolyyesnoprint As Integer
    '    Dim fnum(,) As Integer
    '    Dim fnum1 As Integer
    '    Dim fnumfire As Integer
    '    Dim fnumprocess As Integer
    '    Dim fnumGIS As Integer
    '    Dim PatchFilter() As Boolean
    '    Dim WLSpeciesAcresPrint(,,,) As Double 'acres by group def, species, type, time step
    '    Dim ProcessAcresPrint(,,) As Double 'acres by group def, process, time step
    '    Dim wlspmultval As Single
    '    Dim kprocess As Integer
    '    'Dim bNeedSpatArrays As Boolean
    '    Dim NWriteGroups As Integer

    '    Dim kspec As Integer
    '    'Dim ndef(,) As Integer

    '    'tally the acres of habitat
    '    'GC.Collect()
    '    ReDim WLSpeciesAcres(NWLSpecies, MaxWLSpeciesTypeVal, NTimestep), PatchFilter(NWLSpecies), WLSpeciesAcresPrint(NReportGroups, NWLSpecies, MaxWLSpeciesTypeVal, NTimestep)
    '    ReDim ProcessAcresPrint(NReportGroups, NProcessLUT, NTimestep)
    '    'If krun = 1 Then
    '    ReDim AAWLHabitat(NSLINK, NWLSpecies, NTimestep), AAWLHabitatStr(NSLINK, NWLSpecies, NTimestep) ' AAWLHabitatMult(NSLINK, NWLSpecies, NTimestep),
    '    'Else
    '    '    ClearLargeArrays()
    '    'End If



    '    'NON-SPATIAL Calls
    '    For jaa As Integer = 1 To NSLINK
    '        ' If jaa = 45477 Or jaa = 45868 Then Stop
    '        'If jaa = 515 Then Stop
    '        khg = AAHabGroup(jaa) 'static
    '        For jspec As Integer = 1 To NWLSpecies
    '            'For jwltype As Integer = 1 To NWLSpeciesTypes(jspec)
    '            For jtic As Integer = 0 To NTimestep
    '                kct = AASpecies(jaa, jtic)
    '                ksc = AASizeClass(jaa, jtic)
    '                kdn = AADensity(jaa, jtic)

    '                If WLSpeciesUsePreBurnCond(jspec) = True Then 'find the most recent burn if necessary
    '                    kburn = -1
    '                    For jjtic As Integer = 0 To jtic
    '                        ktic = jtic - jjtic
    '                        If AAProcess(jaa, ktic) > 0 Then kburn = GetStrIndex(FireProcessNameLUT, ProcessNameLUT(AAProcess(jaa, ktic)))
    '                        If kburn >= 0 Then Exit For 'had a fire
    '                    Next
    '                    If kburn >= 0 And ktic > 0 Then
    '                        kct = AASpecies(jaa, ktic - 1) 'condition before fire
    '                        ksc = AASizeClass(jaa, ktic - 1)
    '                        kdn = AADensity(jaa, ktic - 1)
    '                    End If
    '                End If

    '                khabyesno = 0 'default
    '                AAWLHabitat(jaa, jspec, jtic) = 0 'khabyesno 'default
    '                AAWLHabitatStr(jaa, jspec, jtic) = 0 'khabyesno 'default
    '                'allow for undefined values
    '                If kct >= 0 And ksc >= 0 And kdn >= 0 And khg >= 0 Then khabyesno = WLSpeciesDefined(khg, kct, ksc, kdn, jspec)
    '                If khabyesno > 1 Then khabyesno = 1 'wlspeciesdefined may be >1
    '                If khabyesno > 0 Then
    '                    'check for continuous non-spatial variable values
    '                    khabyesno = ContinuousNSFilter(khabyesno, jspec, jaa, jtic)
    '                    'AAWLHabitatMult(jaa, jspec, jtic) = khabyesno
    '                    If khabyesno > 0 Then
    '                        AAWLHabitat(jaa, jspec, jtic) = 1
    '                        kwltype = 1 'default
    '                        twltype = WLSpeciesDefined(khg, kct, ksc, kdn, jspec)
    '                        If twltype >= 1 Then 'search for the kwltype
    '                            kwltype = GetIntIndex2(WLSpeciesTypeVal, jspec, NWLSpeciesTypes(jspec), twltype)
    '                        End If
    '                        AAWLHabitatStr(jaa, jspec, jtic) = WLSpeciesTypeVal(jspec, kwltype)
    '                    End If
    '                End If
    '            Next
    '        Next
    '    Next

    '    'NOW DO SPATIAL CALLS
    '    'bNeedSpatArrays = False
    '    NSpatMetrics = 0
    '    ReDim SpatMetricSpeciesIndex(NSpatMetrics)
    '    For jspec As Integer = 1 To NWLSpecies
    '        PatchFilter(jspec) = False 'default
    '        If WLSpeciesMinPatchSize(jspec) > 0 Or WLSpeciesMaxPatchSize(jspec) > 0 Then
    '            NSpatMetrics += 1
    '            ReDim Preserve SpatMetricSpeciesIndex(NSpatMetrics)
    '            SpatMetricSpeciesIndex(NSpatMetrics) = jspec
    '            'bNeedSpatArrays = True
    '            'Exit For
    '        End If
    '    Next jspec
    '    If NSpatMetrics > 0 Then 'bNeedSpatArrays = True Then
    '        'ReDim AAPatchIndex(NSLINK, NWLSpecies, NTimestep), WLPatchSize(NSLINK, NWLSpecies, NTimestep)
    '        ReDim AAPatchIndex(NSLINK, NSpatMetrics, NTimestep), WLPatchSize(NSLINK, NSpatMetrics, NTimestep)
    '        For jspec As Integer = 1 To NSpatMetrics 'NWLSpecies
    '            kspec = SpatMetricSpeciesIndex(jspec)
    '            PatchFilter(kspec) = False 'default
    '            If WLSpeciesMinPatchSize(kspec) > 0 Or WLSpeciesMaxPatchSize(kspec) > 0 Then
    '                'collect the patch statistics for this species, this timestep
    '                PatchFilter(kspec) = True
    '                If bSplitMaps = False Then PatchStats2(BasePatchFN & "AggPatch" & WLSpeciesName(kspec) & ".txt", BasePatchFN & "Patches" & WLSpeciesName(kspec) & ".txt", kspec, jspec, 0, bWritePatchMaps)
    '                If bSplitMaps = True Then
    '                    For jgroup As Integer = 1 To NReportGroups
    '                        PatchStats2(BasePatchFN & "AggPatch" & ReportGroupName(jgroup) & WLSpeciesName(kspec) & ".txt", BasePatchFN & "Patches" & ReportGroupName(jgroup) & WLSpeciesName(kspec) & ".txt", kspec, jspec, jgroup, bWritePatchMaps)
    '                    Next
    '                End If
    '            End If
    '            If HomeRange(kspec) > 0 Then

    '            End If
    '        Next jspec
    '    End If

    '    ReDim fnum(NWLSpecies, NReportGroups)
    '    If bWritePolyAtts = True Then
    '        For jspec As Integer = 1 To NWLSpecies
    '            strb = New StringBuilder("SLINK", 200) ' = "SLINK"
    '            For jtic As Integer = 0 To NTimestep
    '                strb.Append("," & WLSpeciesName(jspec) & "_" & TimestepIndex(jtic)) 'str = str & "," & WLSpeciesName(jspec) & "_" & TimestepIndex(jtic)
    '            Next
    '            If bSplitMaps = False Then
    '                fnum(jspec, 1) = FreeFile()
    '                NWriteGroups = 1
    '                FileOpen(fnum(jspec, 1), DirName(outfile1) & FName(outfile1) & "_" & WLSpeciesName(jspec) & ".txt", OpenMode.Output)
    '                PrintLine(fnum(jspec, 1), strb.ToString)
    '            Else
    '                For jgroup As Integer = 1 To NReportGroups
    '                    fnum(jspec, jgroup) = FreeFile()
    '                    ' If bSplitMaps = True Then haboutfilename = DirName(HabOutputFN) & OutFilesToProcessName(jrun) & OutFilesToProcessNumber(jrun) & "_" & ReportGroupName(jgroup) & "_" & jgroup & "_" & WLSpeciesName(jspec) & ".txt"
    '                    FileOpen(fnum(jspec, jgroup), DirName(outfile1) & FName(outfile1) & "_" & ReportGroupName(jgroup) & "_" & WLSpeciesName(jspec) & ".txt", OpenMode.Output)
    '                    PrintLine(fnum(jspec, jgroup), strb.ToString)

    '                Next
    '            End If
    '            strb = Nothing
    '        Next
    '    End If
    '    If bWriteGISMaps = True Then
    '        strb = New StringBuilder("SLINK,STANDID", 200)
    '        For jspec As Integer = 1 To NWLSpecies
    '            For jtic As Integer = 0 To NTimestep
    '                strb.Append("," & WLSpeciesName(jspec) & "_" & TimestepIndex(jtic)) ' str = str & "," & WLSpeciesName(jspec) & "_" & TimestepIndex(jtic)
    '            Next
    '        Next
    '        fnumGIS = FreeFile()
    '        FileOpen(fnumGIS, DirName(outfile1) & FName(outfile1) & "_WLOutputsCombinedForGIS.txt", OpenMode.Output)
    '        PrintLine(fnumGIS, strb.ToString)
    '        strb = Nothing
    '    End If

    '    If bWriteProcessMaps = True Then
    '        strb = New StringBuilder("SLINK", 200)
    '        For jtic As Integer = 0 To NTimestep
    '            strb.Append(",Process_" & TimestepIndex(jtic)) ' str = str & ",Process_" & TimestepIndex(jtic)
    '        Next
    '        fnumfire = FreeFile()
    '        FileOpen(fnumfire, DirName(outfile1) & FName(outfile1) & "_Process.txt", OpenMode.Output)
    '        PrintLine(fnumfire, strb.ToString)
    '        strb = Nothing
    '    End If



    '    '...AND FILTER FOR SPATIAL CALLS plus, print the files

    '    For jaa As Integer = 1 To NSLINK
    '        khg = AAHabGroup(jaa) 'static
    '        str1 = AASLINK(jaa)
    '        strGISb = New StringBuilder(AASLINK(jaa) & "," & Chr(34) & AASTANDID(jaa) & Chr(34), 300)
    '        kpolyyesnoprint = 1
    '        'If AAHabGroup(AASLINK(jaa)) < 0 Or AAOwnership(AASLINK(jaa)) < 0 Then kpolyyesnoprint = 0
    '        For jspec As Integer = 1 To NWLSpecies
    '            'For jtype As Integer = 1 To NWLSpeciesTypes(jspec)
    '            If bWritePolyAtts = True Then
    '                strb = New StringBuilder(str1, 200) 'str = str1
    '                dumstr = str1
    '            End If
    '            For jtic As Integer = 0 To NTimestep
    '                'kct = AASpecies(jaa, jtic)
    '                'ksc = AASizeClass(jaa, jtic)
    '                'kdn = AADensity(jaa, jtic)
    '                khabyesno = AAWLHabitat(jaa, jspec, jtic)
    '                kwltype = 0 'default

    '                If khabyesno = 1 Then
    '                    wlspmultval = 1 'AAWLHabitatMult(jaa, jspec, jtic) 'default
    '                    If wlspmultval > 1 Then wlspmultval = 1 '#'s >1 are used to define unique categories
    '                    'check for continuous spatial variable values
    '                    If PatchFilter(jspec) = True Then
    '                        For jjspec As Integer = 1 To NSpatMetrics
    '                            If SpatMetricSpeciesIndex(jjspec) = jspec Then
    '                                kspec = jjspec
    '                                Exit For
    '                            End If
    '                        Next
    '                        khabyesno = FilterPatch(jspec, kspec, jaa, jtic)
    '                    End If

    '                    AAWLHabitat(jaa, jspec, jtic) = khabyesno
    '                    kwltype = 1 'default
    '                    twltype = AAWLHabitatStr(jaa, jspec, jtic) 'WLSpeciesDefined(khg, kct, ksc, kdn, jspec)
    '                    If twltype >= 1 Then 'search for the kwltype
    '                        kwltype = GetIntIndex2(WLSpeciesTypeVal, jspec, NWLSpeciesTypes(jspec), twltype)
    '                    End If
    '                    WLSpeciesAcres(jspec, kwltype, jtic) = WLSpeciesAcres(jspec, kwltype, jtic) + SIMAAAcres * khabyesno * wlspmultval 'AAWLHabitatMult(jaa, jspec, jtic)
    '                    For jgroup As Integer = 1 To NReportGroups

    '                        kpolyyesnoprint = ReportDefinition(jgroup, AAOwnership(AASLINK(jaa)), AASpecialArea(AASLINK(jaa)))
    '                        'If jgroup = 2 And jspec = 1 And kwltype = 1 And SIMAAAcres * kpolyyesnoprint * khabyesno * wlspmultval > 0 Then Stop
    '                        WLSpeciesAcresPrint(jgroup, jspec, kwltype, jtic) = WLSpeciesAcresPrint(jgroup, jspec, kwltype, jtic) + SIMAAAcres * kpolyyesnoprint * khabyesno * wlspmultval 'AAWLHabitatMult(jaa, jspec, jtic)
    '                    Next
    '                End If
    '                If bWritePolyAtts = True Then
    '                    strb.Append("," & WLSpeciesTypeVal(jspec, kwltype)) 'str = str & "," & WLSpeciesTypeVal(jspec, kwltype)

    '                    dumstr = dumstr & ",0"
    '                End If
    '                'GIS MAPS
    '                If bWriteGISMaps Then strGISb.Append("," & Chr(34) & WLSpeciesTypeName(jspec, kwltype) & Chr(34)) ' strGIS= strGIS & "," & Chr(34) & WLSpeciesTypeName(jspec, kwltype) & Chr(34)
    '                AAWLHabitatStr(jaa, jspec, jtic) = WLSpeciesTypeVal(jspec, kwltype)

    '            Next jtic

    '            If bWritePolyAtts = True Then
    '                If bSplitMaps = False Then
    '                    PrintLine(fnum(jspec, 1), strb.ToString)
    '                Else
    '                    For jgroup As Integer = 1 To NReportGroups
    '                        kpolyyesnoprint = ReportDefinition(jgroup, AAOwnership(AASLINK(jaa)), AASpecialArea(AASLINK(jaa)))
    '                        If kpolyyesnoprint > 0 Then PrintLine(fnum(jspec, jgroup), strb.ToString)
    '                        If kpolyyesnoprint = 0 Then PrintLine(fnum(jspec, jgroup), dumstr)
    '                    Next
    '                End If
    '                strb = Nothing
    '            End If

    '        Next jspec

    '        If bWriteProcessMaps = True Or bWriteProcessStats = True Then
    '            strb = New StringBuilder(AASLINK(jaa).ToString, 200) 'Str = AASLINK(jaa)
    '            For jtic As Integer = 0 To NTimestep
    '                kprocess = 0 'default
    '                If AAProcess(jaa, jtic) > 0 Then kprocess = GetStrIndex(ProcessNameLUT, ProcessNameLUT(AAProcess(jaa, jtic)))
    '                If kprocess < 0 Then kprocess = 0
    '                strb.Append("," & kprocess) ' str = str & "," & kprocess
    '                If bWriteProcessStats = True Then
    '                    For jgroup As Integer = 1 To NReportGroups
    '                        kpolyyesnoprint = ReportDefinition(jgroup, AAOwnership(AASLINK(jaa)), AASpecialArea(AASLINK(jaa)))
    '                        'If jgroup = 2 And jspec = 1 And kwltype = 1 And SIMAAAcres * kpolyyesnoprint * khabyesno * wlspmultval > 0 Then Stop
    '                        ProcessAcresPrint(jgroup, kprocess, jtic) = ProcessAcresPrint(jgroup, kprocess, jtic) + SIMAAAcres * kpolyyesnoprint
    '                    Next
    '                End If
    '            Next
    '            If bWriteProcessMaps = True Then PrintLine(fnumfire, strb.ToString)
    '            strb = Nothing
    '        End If
    '        'GIS MAPS
    '        If bWriteGISMaps = True Then PrintLine(fnumGIS, strGISb.ToString)
    '        strGISb = Nothing
    '    Next jaa

    '    If bWritePolyAtts = True Then
    '        For jspec As Integer = 1 To NWLSpecies
    '            For jgroup As Integer = 1 To NReportGroups
    '                If fnum(jspec, jgroup) > 0 Then FileClose(fnum(jspec, jgroup))
    '            Next jgroup
    '        Next jspec
    '    End If
    '    If bWriteGISMaps = True Then FileClose(fnumGIS)
    '    If bWriteProcessMaps = True Then FileClose(fnumfire)


    '    If bWriteProcessStats = True Then
    '        fnumprocess = FreeFile()
    '        FileOpen(fnumprocess, processoutfilename, OpenMode.Output)
    '    End If
    '    'query output files
    '    fnum1 = FreeFile()
    '    FileOpen(fnum1, habsumoutfile, OpenMode.Output)

    '    For jgroup As Integer = 1 To NReportGroups
    '        'print out the habitat totals files header line
    '        strb = New StringBuilder("ReportGroup,OutputName", 200) 'str = "ReportGroup,OutputName"
    '        For jtic As Integer = 0 To NTimestep
    '            strb.Append(",Time" & TimestepIndex(jtic)) 'str = str & ",Time" & TimestepIndex(jtic)
    '        Next
    '        PrintLine(fnum1, strb.ToString)
    '        If bWriteProcessStats = True Then PrintLine(fnumprocess, strb.ToString)
    '        strb = Nothing
    '        'already have time 0 and simulation outputs loaded...go through and code them 
    '        For jspec As Integer = 1 To NWLSpecies
    '            For jtype As Integer = 1 To NWLSpeciesTypes(jspec)
    '                strb = New StringBuilder(ReportGroupName(jgroup) & "," & WLSpeciesName(jspec) & "_" & WLSpeciesTypeName(jspec, jtype), 200) ' str = ReportGroupName(jgroup) & "," & WLSpeciesName(jspec) & "_" & WLSpeciesTypeName(jspec, jtype)
    '                For jtic As Integer = 0 To NTimestep
    '                    strb.Append("," & Math.Round(WLSpeciesAcresPrint(jgroup, jspec, jtype, jtic), 0)) ' str = str & "," & Math.Round(WLSpeciesAcresPrint(jgroup, jspec, jtype, jtic), 0) '& "," & WLSpeciesAcres(jspec, jtic)
    '                Next
    '                PrintLine(fnum1, strb.ToString)
    '                strb = Nothing
    '            Next jtype
    '        Next
    '        'capture process acres
    '        If bWriteProcessStats Then
    '            For jprocess As Integer = 1 To NProcessLUT
    '                strb = New StringBuilder(ReportGroupName(jgroup) & "," & ProcessNameLUT(jprocess), 200) 'str = ReportGroupName(jgroup) & "," & ProcessNameLUT(jprocess)
    '                For jtic As Integer = 0 To NTimestep
    '                    strb.Append("," & Math.Round(ProcessAcresPrint(jgroup, jprocess, jtic), 0)) ' str = str & "," & Math.Round(ProcessAcresPrint(jgroup, jprocess, jtic), 0) '& "," & WLSpeciesAcres(jspec, jtic)
    '                Next
    '                PrintLine(fnumprocess, strb.ToString)
    '                strb = Nothing
    '            Next
    '        End If
    '    Next jgroup
    '    FileClose(fnum1)
    '    If bWriteProcessStats = True Then FileClose(fnumprocess)


    'End Sub
    'Private Sub WriteGridOF(ByVal GridOFBaseName As String, ByVal RunNum As Integer, ByVal BatchOFNum As Integer)
    '    'Dim str As String
    '    Dim strb As StringBuilder
    '    Dim OFName As String
    '    Dim krow As Integer
    '    Dim nullval As String = "-999"



    '    For jhab As Integer = 1 To NWLSpecies
    '        For jtic As Integer = 0 To NTimestep
    '            Dim fnum As Integer = FreeFile()
    '            OFName = GridOFBaseName & WLSpeciesName(jhab) & "_Run" & RunNum & "_Time" & TimestepIndex(jtic) & ".txt"
    '            FileOpen(fnum, OFName, OpenMode.Output)
    '            ' AAWLHabitat(jaa, jspec, jtic)
    '            For jrow As Integer = 1 To BiggestRow
    '                krow = BiggestRow + 1 - jrow
    '                strb = New StringBuilder("", 200)
    '                'str = ""
    '                For jcol As Integer = 1 To BiggestCol
    '                    If GridSlink(krow, jcol) > 0 Then
    '                        strb.Append(AAWLHabitatStr(GridSlink(krow, jcol), jhab, jtic))
    '                        'Str = Str & AAWLHabitatStr(GridSlink(krow, jcol), jhab, jtic)
    '                    Else
    '                        strb.Append(nullval)
    '                        'Str = Str & nullval
    '                    End If
    '                    If jcol < BiggestCol Then strb.Append(" ") 'str = str & " "
    '                Next
    '                PrintLine(fnum, strb.ToString)
    '                strb = Nothing
    '            Next
    '            FileClose(fnum)
    '            PrintLine(BatchOFNum, OFName & ", " & cellsize & ", " & nullval & ", " & BiggestRow & ", " & BiggestCol & ", 1, IDF_ASCII")
    '        Next
    '    Next


    'End Sub

    'Private Function ContinuousNSFilter(ByVal InitVal As Single, ByVal khab As Integer, ByVal kaa As Integer, ByVal ktic As Integer) As Single
    '    'filters out habitat calls by non-spatial continuous filters

    '    'default is what it is sent in as...
    '    ContinuousNSFilter = InitVal

    '    'ReDim WLSpeciesMinSinceBurn(NWLSpecies)
    '    'ReDim WLSpeciesMaxSinceBurn(NWLSpecies)

    '    'check elevation
    '    If WLSpeciesMinElev(khab) > 0 And AAElevation(kaa) < WLSpeciesMinElev(khab) Then ContinuousNSFilter = 0 'check if elevation is less than min
    '    If WLSpeciesMaxElev(khab) > 0 And AAElevation(kaa) > WLSpeciesMaxElev(khab) Then ContinuousNSFilter = 0 'check if elevation is more than max

    '    'check years since burn
    '    Dim kburn As Integer
    '    Dim bHadMinBurn As Boolean
    '    Dim bHadMaxBurn As Boolean
    '    Dim ticadj As Integer 'allows adjustment for processes in the past identified in time 0
    '    If WLSpeciesMinSinceBurn(khab) >= 0 Then
    '        'need to consider whether the burn happened more than X periods ago and not again within X periods

    '        bHadMinBurn = False 'flag whether burn happened more than X periods ago
    '        For jtic As Integer = 0 To ktic
    '            ticadj = 0
    '            kburn = -1 'default
    '            If AAProcess(kaa, jtic) > 0 Then kburn = GetStrIndex(FireProcessNameLUT, ProcessNameLUT(AAProcess(kaa, jtic)))
    '            If kburn >= 0 Then 'had a fire! - comes back with the burn index
    '                'was it longer ago since the min. since burn?
    '                'If jtic = 0 And PastProcessTimesteps(kaa) > 1 Then Stop
    '                ' If kaa = 21290 Then Stop
    '                If jtic = 0 Then ticadj = PastProcessTimesteps(kaa) - 1
    '                If ktic - jtic + ticadj >= WLSpeciesMinSinceBurn(khab) Then bHadMinBurn = True 'burn happened more than X periods ago
    '                If ktic - jtic + ticadj < WLSpeciesMinSinceBurn(khab) Then bHadMinBurn = False 'burn happened more recently
    '            End If
    '        Next
    '        If bHadMinBurn = False Then ContinuousNSFilter = 0 'didn't meet the min. since burn criteria
    '    End If

    '    If WLSpeciesMaxSinceBurn(khab) >= 0 Then
    '        'need to consider whether the burn happened within X periods...

    '        bHadMaxBurn = False 'flag whether burn happened less than X periods ago
    '        For jtic As Integer = 0 To ktic
    '            ticadj = 0
    '            kburn = -1 'default
    '            If AAProcess(kaa, jtic) > 0 Then kburn = GetStrIndex(FireProcessNameLUT, ProcessNameLUT(AAProcess(kaa, jtic)))
    '            If kburn >= 0 Then 'had a fire!
    '                'was it recent enough to meet the max. since burn?
    '                If jtic = 0 Then ticadj = PastProcessTimesteps(kaa) - 1
    '                If ktic - jtic - ticadj <= WLSpeciesMaxSinceBurn(khab) Then bHadMaxBurn = True 'burn happened recently enough
    '            End If
    '        Next
    '        If bHadMaxBurn = False Then ContinuousNSFilter = 0 'didn't meet the min. since burn criteria
    '    End If


    'End Function


    'Private Function FilterPatch(ByVal khab As Integer, ByVal kspatialhab As Integer, ByVal kaa As Integer, ByVal ktic As Integer) As Integer

    '    'default is what it is sent in as...
    '    FilterPatch = 1

    '    'check patch size
    '    If WLSpeciesMinPatchSize(khab) > 0 And WLPatchSize(AAPatchIndex(kaa, kspatialhab, ktic), kspatialhab, ktic) < WLSpeciesMinPatchSize(khab) Then FilterPatch = 0 'check if elevation is less than min
    '    If WLSpeciesMaxPatchSize(khab) > 0 And WLPatchSize(AAPatchIndex(kaa, kspatialhab, ktic), kspatialhab, ktic) > WLSpeciesMaxPatchSize(khab) Then FilterPatch = 0 'check if elevation is more than max




    'End Function

    'Private Sub LoadSimCrosswalkFiles()
    '    'in the TEXTFILE folder - unique to each simulation

    '    Dim MaxIndex As Integer = 0
    '    Dim fnum As Integer = FreeFile()
    '    Dim dummy As String
    '    Dim arr() As String


    '    'load PROCESS
    '    MaxIndex = 0
    '    ReDim ProcessNameXW(MaxIndex)
    '    FileOpen(fnum, ProcessXWFN, OpenMode.Input)
    '    dummy = LineInput(fnum)
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            If arr(0) > MaxIndex Then
    '                MaxIndex = arr(0)
    '                ReDim Preserve ProcessNameXW(MaxIndex)
    '            End If
    '            ProcessNameXW(arr(0)) = CType(Trim(arr(1)), String)
    '        End If
    '    Loop
    '    NProcessXW = MaxIndex
    '    FileClose(fnum)

    '    'load TREATMENT
    '    MaxIndex = 0
    '    ReDim TreatmentNameXW(MaxIndex)
    '    fnum = FreeFile()
    '    If System.IO.File.Exists(TreatmentXWFN) Then
    '        FileOpen(fnum, TreatmentXWFN, OpenMode.Input)
    '        dummy = LineInput(fnum)
    '        Do Until EOF(fnum)
    '            dummy = LineInput(fnum)
    '            If dummy.Length > 0 Then
    '                arr = Split(dummy, ",")
    '                If arr(0) > MaxIndex Then
    '                    MaxIndex = arr(0)
    '                    ReDim Preserve TreatmentNameXW(MaxIndex)
    '                End If
    '                TreatmentNameXW(arr(0)) = CType(Trim(arr(1)), String)
    '            End If
    '        Loop
    '    End If
    '    NProcessXW = MaxIndex
    '    FileClose(fnum)

    '    'load SpeciesLUTFN
    '    MaxIndex = 0
    '    ReDim SpeciesNameXW(MaxIndex)
    '    FileOpen(fnum, SpeciesXWFN, OpenMode.Input)
    '    dummy = LineInput(fnum)
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            If arr(0) > MaxIndex Then
    '                MaxIndex = arr(0)
    '                ReDim Preserve SpeciesNameXW(MaxIndex)
    '            End If
    '            SpeciesNameXW(arr(0)) = CType(arr(1), String)
    '        End If
    '    Loop
    '    NSpeciesXW = MaxIndex
    '    FileClose(fnum)

    '    'load SizeClassXW
    '    MaxIndex = 0
    '    ReDim SizeClassNameXW(MaxIndex)
    '    FileOpen(fnum, SizeClassXWFN, OpenMode.Input)
    '    dummy = LineInput(fnum)
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            If arr(0) > MaxIndex Then
    '                MaxIndex = arr(0)
    '                ReDim Preserve SizeClassNameXW(MaxIndex)
    '            End If
    '            SizeClassNameXW(arr(0)) = CType(arr(1), String)
    '        End If
    '    Loop
    '    NSizeClassXW = MaxIndex
    '    FileClose(fnum)

    '    'load LifeformLUTFN
    '    MaxIndex = 0
    '    ReDim LifeFormName(MaxIndex)
    '    FileOpen(fnum, LifeFormXWFN, OpenMode.Input)
    '    dummy = LineInput(fnum)
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            If arr(0) > MaxIndex Then
    '                MaxIndex = arr(0)
    '                ReDim Preserve LifeFormName(MaxIndex)
    '            End If
    '            LifeFormName(arr(0)) = CType(arr(1), String)
    '        End If
    '    Loop
    '    NLifeForm = MaxIndex
    '    FileClose(fnum)


    '    'load DensityXWFN
    '    MaxIndex = 0
    '    ReDim DensityNameXW(MaxIndex)
    '    FileOpen(fnum, DensityXWFN, OpenMode.Input)
    '    dummy = LineInput(fnum)
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            If arr(0) > MaxIndex Then
    '                MaxIndex = arr(0)
    '                ReDim Preserve DensityNameXW(MaxIndex)
    '            End If
    '            DensityNameXW(arr(0)) = CType(arr(1), String)
    '        End If
    '    Loop
    '    NDensityXW = MaxIndex
    '    FileClose(fnum)


    'End Sub

    'Private Sub LoadStandardLUTs()
    '    Dim MaxIndex As Integer
    '    Dim fnum As Integer
    '    Dim dummy As String
    '    Dim arr() As String

    '    bLoadSuccessful = False
    '    'load ALLPROCESSLUT
    '    MaxIndex = 0
    '    'ReDim ProcessNameLUT(MaxIndex)
    '    fnum = FreeFile()
    '    If System.IO.File.Exists(AllProcessLUTFN) Then
    '        FileOpen(fnum, AllProcessLUTFN, OpenMode.Input)
    '        dummy = LineInput(fnum)
    '        Do Until EOF(fnum)
    '            dummy = LineInput(fnum)
    '            If dummy.Length > 0 Then
    '                arr = Split(dummy, ",")
    '                MaxIndex = MaxIndex + 1
    '                ReDim Preserve ProcessNameLUT(MaxIndex)
    '                ProcessNameLUT(MaxIndex) = CType(Trim(arr(1)), String)
    '            End If
    '        Loop
    '        NProcessLUT = MaxIndex
    '        FileClose(fnum)
    '    End If

    '    'load SizeClassLUTFN
    '    MaxIndex = 0

    '    'NSizeClassLUT = 0
    '    'ReDim SizeClassNameLUT(MaxIndex) ', SizeClassSpecName(NSpecSizeClass) ', SizeClassSpecNameLUT(MaxIndex)
    '    fnum = FreeFile()
    '    If System.IO.File.Exists(SizeClassLUTFN) Then
    '        FileOpen(fnum, SizeClassLUTFN, OpenMode.Input)
    '        dummy = LineInput(fnum)
    '        Do Until EOF(fnum)
    '            dummy = LineInput(fnum)
    '            If dummy.Length > 0 Then
    '                arr = Split(dummy, ",")
    '                MaxIndex = MaxIndex + 1
    '                ReDim Preserve SizeClassNameLUT(MaxIndex) ', SizeClassSpecNameLUT(MaxIndex)
    '                SizeClassNameLUT(MaxIndex) = CType(arr(1), String)
    '            End If
    '        Loop
    '        NSizeClassLUT = MaxIndex
    '        FileClose(fnum)
    '    End If

    '    'LOAD DENSITY LUT
    '    MaxIndex = 0
    '    'ReDim DensityNameLUT(MaxIndex)
    '    fnum = FreeFile()
    '    If System.IO.File.Exists(DensityLUTFN) Then
    '        FileOpen(fnum, DensityLUTFN, OpenMode.Input)
    '        dummy = LineInput(fnum)
    '        Do Until EOF(fnum)
    '            dummy = LineInput(fnum)
    '            If dummy.Length > 0 Then
    '                arr = Split(dummy, ",")
    '                MaxIndex = MaxIndex + 1
    '                ReDim Preserve DensityNameLUT(MaxIndex)
    '                DensityNameLUT(MaxIndex) = CType(arr(1), String)
    '            End If
    '        Loop
    '        NDensityLUT = MaxIndex
    '        FileClose(fnum)
    '    End If

    '    'LOAD HABITAT GROUP LUT
    '    MaxIndex = 0
    '    'ReDim HabGroupNameLUT(MaxIndex)
    '    fnum = FreeFile()
    '    If System.IO.File.Exists(HabGroupLUTFN) Then
    '        FileOpen(fnum, HabGroupLUTFN, OpenMode.Input)
    '        dummy = LineInput(fnum)
    '        Do Until EOF(fnum)
    '            dummy = LineInput(fnum)
    '            If dummy.Length > 0 Then
    '                arr = Split(dummy, ",")
    '                MaxIndex = MaxIndex + 1
    '                ReDim Preserve HabGroupNameLUT(MaxIndex)
    '                HabGroupNameLUT(MaxIndex) = CType(arr(1), String)
    '            End If
    '        Loop
    '        NHabGroupLUT = MaxIndex
    '        FileClose(fnum)
    '    End If

    '    'LOAD SPECIES LUT
    '    MaxIndex = 0
    '    'ReDim SpeciesNameLUT(MaxIndex)
    '    fnum = FreeFile()
    '    If System.IO.File.Exists(SpeciesLUTFN) Then
    '        FileOpen(fnum, SpeciesLUTFN, OpenMode.Input)
    '        dummy = LineInput(fnum)
    '        Do Until EOF(fnum)
    '            dummy = LineInput(fnum)
    '            If dummy.Length > 0 Then
    '                arr = Split(dummy, ",")
    '                MaxIndex = MaxIndex + 1
    '                ReDim Preserve SpeciesNameLUT(MaxIndex)
    '                SpeciesNameLUT(MaxIndex) = CType(Trim(arr(1)), String)
    '            End If
    '        Loop
    '        NSpeciesLUT = MaxIndex
    '        FileClose(fnum)
    '    End If

    '    'LOAD OWNERSHIP LUT FILE
    '    MaxIndex = 0
    '    'ReDim OwnershipName(MaxIndex)
    '    fnum = FreeFile()
    '    If System.IO.File.Exists(OwnershipLUTFN) Then
    '        FileOpen(fnum, OwnershipLUTFN, OpenMode.Input)
    '        dummy = LineInput(fnum)
    '        Do Until EOF(fnum)
    '            dummy = LineInput(fnum)
    '            If dummy.Length > 0 Then
    '                arr = Split(dummy, ",")
    '                MaxIndex = MaxIndex + 1
    '                ReDim Preserve OwnershipName(MaxIndex)
    '                OwnershipName(MaxIndex) = CType(arr(1), String)
    '            End If
    '        Loop
    '        NOwnerships = MaxIndex
    '        FileClose(fnum)
    '    End If

    '    'LOAD SPECIAL AREA LUT FILE
    '    MaxIndex = 0
    '    'ReDim SpecialAreaName(MaxIndex)
    '    fnum = FreeFile()
    '    If System.IO.File.Exists(SpecialAreaLUTFN) Then
    '        FileOpen(fnum, SpecialAreaLUTFN, OpenMode.Input)
    '        dummy = LineInput(fnum)
    '        Do Until EOF(fnum)
    '            dummy = LineInput(fnum)
    '            If dummy.Length > 0 Then
    '                arr = Split(dummy, ",")
    '                MaxIndex = MaxIndex + 1
    '                ReDim Preserve SpecialAreaName(MaxIndex)
    '                SpecialAreaName(MaxIndex) = CType(Trim(arr(1)), String)
    '            End If
    '        Loop
    '        NSpecialAreas = MaxIndex
    '        FileClose(fnum)
    '    End If

    'End Sub
    'Private Sub LoadLUTs()
    '    'files with full lists of all potential values...

    '    Dim MaxIndex As Integer
    '    Dim dummy As String
    '    Dim arr() As String
    '    Dim tind As Integer
    '    Dim fnum As Integer = FreeFile()
    '    Dim str As String

    '    Dim nshort As Integer
    '    Dim origstr() As String

    '    Dim khg As Integer
    '    Dim kct As Integer
    '    Dim ksc As Integer
    '    Dim kdn As Integer
    '    Dim ktype As Integer
    '    Dim tsc As Integer
    '    Dim town As Integer
    '    'Dim tdef As Integer
    '    Dim tgroup As Integer
    '    Dim R As Integer
    '    Dim G As Integer
    '    Dim B As Integer
    '    Dim k As Integer

    '    bLoadSuccessful = False
    '    'load ALLPROCESSLUT
    '    MaxIndex = 0
    '    'ReDim ProcessNameLUT(MaxIndex)
    '    fnum = FreeFile()
    '    FileOpen(fnum, AllProcessLUTFN, OpenMode.Input)
    '    dummy = LineInput(fnum)
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            MaxIndex = MaxIndex + 1
    '            ReDim Preserve ProcessNameLUT(MaxIndex)
    '            ProcessNameLUT(MaxIndex) = CType(Trim(arr(1)), String)
    '        End If
    '    Loop
    '    NProcessLUT = MaxIndex
    '    FileClose(fnum)

    '    'load SizeClassLUTFN
    '    MaxIndex = NSizeClassLUT

    '    'NSizeClassLUT = 0
    '    'ReDim SizeClassNameLUT(MaxIndex) ', SizeClassSpecName(NSpecSizeClass) ', SizeClassSpecNameLUT(MaxIndex)
    '    fnum = FreeFile()
    '    FileOpen(fnum, SizeClassLUTFN, OpenMode.Input)
    '    dummy = LineInput(fnum)
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            dummy = Trim(arr(1))
    '            k = GetStrIndex(SizeClassNameLUT, dummy) 'CType(arr(3), Integer) 'GetIndex(DensityNameLUT, Trim(arr(3)))
    '            If k < 0 Then
    '                MaxIndex = MaxIndex + 1
    '                ReDim Preserve SizeClassNameLUT(MaxIndex) ', SizeClassSpecNameLUT(MaxIndex)
    '                SizeClassNameLUT(MaxIndex) = CType(arr(1), String)
    '            End If
    '        End If
    '    Loop
    '    NSizeClassLUT = MaxIndex
    '    FileClose(fnum)

    '    'LOAD DENSITY LUT
    '    MaxIndex = NDensityLUT
    '    'ReDim DensityNameLUT(MaxIndex)
    '    fnum = FreeFile()
    '    FileOpen(fnum, DensityLUTFN, OpenMode.Input)
    '    dummy = LineInput(fnum)
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            k = GetStrIndex(DensityNameLUT, dummy) 'CType(arr(3), Integer) 'GetIndex(DensityNameLUT, Trim(arr(3)))
    '            If k < 0 Then
    '                MaxIndex = MaxIndex + 1
    '                ReDim Preserve DensityNameLUT(MaxIndex)
    '                DensityNameLUT(MaxIndex) = CType(arr(1), String)
    '            End If
    '        End If
    '    Loop
    '    NDensityLUT = MaxIndex
    '    FileClose(fnum)

    '    'LOAD HABITAT GROUP LUT
    '    MaxIndex = NHabGroupLUT
    '    'ReDim HabGroupNameLUT(MaxIndex)
    '    fnum = FreeFile()
    '    FileOpen(fnum, HabGroupLUTFN, OpenMode.Input)
    '    dummy = LineInput(fnum)
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            k = GetStrIndex(HabGroupNameLUT, dummy) 'CType(arr(3), Integer) 'GetIndex(DensityNameLUT, Trim(arr(3)))
    '            If k < 0 Then
    '                MaxIndex = MaxIndex + 1
    '                ReDim Preserve HabGroupNameLUT(MaxIndex)
    '                HabGroupNameLUT(MaxIndex) = CType(arr(1), String)
    '            End If
    '        End If
    '    Loop
    '    NHabGroupLUT = MaxIndex
    '    FileClose(fnum)

    '    'LOAD SPECIES LUT
    '    MaxIndex = NSpeciesLUT
    '    'ReDim SpeciesNameLUT(MaxIndex)
    '    fnum = FreeFile()
    '    FileOpen(fnum, SpeciesLUTFN, OpenMode.Input)
    '    dummy = LineInput(fnum)
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            k = GetStrIndex(SpeciesNameLUT, dummy) 'CType(arr(3), Integer) 'GetIndex(DensityNameLUT, Trim(arr(3)))
    '            If k < 0 Then
    '                MaxIndex = MaxIndex + 1
    '                ReDim Preserve SpeciesNameLUT(MaxIndex)
    '                SpeciesNameLUT(MaxIndex) = CType(Trim(arr(1)), String)
    '            End If
    '        End If
    '    Loop
    '    NSpeciesLUT = MaxIndex
    '    FileClose(fnum)

    '    'LOAD FIRE PROCESS LUT
    '    MaxIndex = 0
    '    ReDim FireProcessNameLUT(MaxIndex)
    '    fnum = FreeFile()
    '    If System.IO.File.Exists(FireProcessLUTFN) = True Then
    '        FileOpen(fnum, FireProcessLUTFN, OpenMode.Input)
    '        dummy = LineInput(fnum)
    '        Do Until EOF(fnum)
    '            dummy = LineInput(fnum)
    '            If dummy.Length > 0 Then
    '                arr = Split(dummy, ",")
    '                MaxIndex = MaxIndex + 1
    '                ReDim Preserve FireProcessNameLUT(MaxIndex)
    '                FireProcessNameLUT(MaxIndex) = CType(arr(1), String)
    '            End If
    '        Loop
    '    Else
    '        ReDim FireProcessNameLUT(MaxIndex)
    '        FileOpen(fnum, FireProcessLUTFN, OpenMode.Output)
    '        PrintLine(fnum, "Index,FIRE_PROCESS")
    '    End If
    '    NFireProcessLUT = MaxIndex
    '    FileClose(fnum)


    '    'LOAD OWNERSHIP LUT FILE
    '    MaxIndex = NOwnerships
    '    'ReDim OwnershipName(MaxIndex)
    '    fnum = FreeFile()
    '    FileOpen(fnum, OwnershipLUTFN, OpenMode.Input)
    '    dummy = LineInput(fnum)
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            k = GetStrIndex(OwnershipName, dummy) 'CType(arr(3), Integer) 'GetIndex(DensityNameLUT, Trim(arr(3)))
    '            If k < 0 Then
    '                MaxIndex = MaxIndex + 1
    '                ReDim Preserve OwnershipName(MaxIndex)
    '                OwnershipName(MaxIndex) = CType(arr(1), String)
    '            End If
    '        End If
    '    Loop
    '    NOwnerships = MaxIndex
    '    FileClose(fnum)

    '    'LOAD SPECIAL AREA LUT FILE
    '    MaxIndex = NSpecialAreas
    '    'ReDim SpecialAreaName(MaxIndex)
    '    fnum = FreeFile()
    '    FileOpen(fnum, SpecialAreaLUTFN, OpenMode.Input)
    '    dummy = LineInput(fnum)
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            k = GetStrIndex(OwnershipName, dummy) 'CType(arr(3), Integer) 'GetIndex(DensityNameLUT, Trim(arr(3)))
    '            If k < 0 Then
    '                MaxIndex = MaxIndex + 1
    '                ReDim Preserve SpecialAreaName(MaxIndex)
    '                SpecialAreaName(MaxIndex) = CType(Trim(arr(1)), String)
    '            End If
    '        End If
    '    Loop
    '    NSpecialAreas = MaxIndex
    '    FileClose(fnum)

    '    'load SuitCodesToUse and their groups
    '    MaxIndex = 0
    '    fnum = FreeFile()
    '    NReportGroups = 0
    '    If System.IO.File.Exists(SuitCodesToUseLUTFN) = True Then
    '        FileOpen(fnum, SuitCodesToUseLUTFN, OpenMode.Input)
    '        dummy = LineInput(fnum)
    '        'NReportDefs = 0
    '        ReDim ReportGroupName(NReportGroups)
    '        Do Until EOF(fnum)
    '            dummy = LineInput(fnum)
    '            If dummy.Length > 0 Then
    '                arr = Split(dummy, ",")
    '                'NReportDefs = NReportDefs + 1
    '                If GetStrIndex(ReportGroupName, Trim(arr(2))) < 0 Then
    '                    NReportGroups = NReportGroups + 1
    '                    ReDim Preserve ReportGroupName(NReportGroups)
    '                    ReportGroupName(NReportGroups) = Trim(arr(2))
    '                End If
    '            Else
    '                Exit Do 'blank line signifies the end of the file
    '            End If
    '        Loop
    '    Else
    '        FileOpen(fnum, SuitCodesToUseLUTFN, OpenMode.Output)
    '        PrintLine(fnum, "Index,GroupInd,GroupName,OwnName,SpecialAreaName")
    '        PrintLine(fnum, "1,1,ALL,,")
    '        NReportGroups = NReportGroups + 1
    '        ReDim ReportGroupName(NReportGroups)
    '        ReportGroupName(NReportGroups) = "ALL"
    '    End If

    '    'control for a blank report group file that exists but has no logic data
    '    If NReportGroups = 0 Then
    '        FileOpen(fnum, SuitCodesToUseLUTFN, OpenMode.Output)
    '        PrintLine(fnum, "Index,GroupInd,GroupName,OwnName,SpecialAreaName")
    '        PrintLine(fnum, "1,1,ALL,,")
    '        NReportGroups = NReportGroups + 1
    '        ReDim ReportGroupName(NReportGroups)
    '        ReportGroupName(NReportGroups) = "ALL"
    '    End If

    '    ReDim ReportDefinition(NReportGroups, NOwnerships, NSpecialAreas)
    '    'ReDim ReportDefGroup(NReportDefs)
    '    FileClose(fnum)
    '    'do again to define relevant combinations
    '    fnum = FreeFile()
    '    FileOpen(fnum, SuitCodesToUseLUTFN, OpenMode.Input)
    '    'ReDim UseSuitCode(NOwnerships, NSpecialAreas)
    '    dummy = LineInput(fnum)
    '    'first count the report groups

    '    Do Until EOF(fnum)
    '        dummy = Trim(LineInput(fnum))
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            'tdef = Trim(arr(0))
    '            town = GetStrIndex(OwnershipName, Trim(arr(3)))
    '            tsc = GetStrIndex(SpecialAreaName, Trim(arr(4)))
    '            tgroup = GetStrIndex(ReportGroupName, Trim(arr(2)))
    '            'ReportDefGroup(tdef) = tgroup
    '            If town <= 0 And tsc <= 0 Then
    '                For jown As Integer = 0 To NOwnerships
    '                    For jsc As Integer = 0 To NSpecialAreas
    '                        ReportDefinition(tgroup, jown, jsc) = 1
    '                    Next
    '                Next

    '            ElseIf town <= 0 And tsc > 0 Then
    '                For jown As Integer = 0 To NOwnerships
    '                    'For jsc As Integer = 1 To NSpecialAreas
    '                    ReportDefinition(tgroup, jown, tsc) = 1
    '                    'Next
    '                Next
    '            ElseIf tsc <= 0 And town > 0 Then
    '                'For jown As Integer = 1 To NOwnerships
    '                For jsc As Integer = 0 To NSpecialAreas
    '                    ReportDefinition(tgroup, town, jsc) = 1
    '                Next
    '                'Next
    '            Else
    '                ReportDefinition(tgroup, town, tsc) = 1
    '            End If
    '            'If tsc > 0 And town > 0 Then UseSuitCode(tsc, town) = 1
    '        Else
    '            Exit Do 'blank line signifies the end of the file
    '        End If
    '    Loop
    '    FileClose(fnum)




    '    'LOAD WILDLIFE QUERY FILE - discrete values
    '    NWLSpecies = 0
    '    fnum = FreeFile()
    '    FileOpen(fnum, WLDiscreteDefsLUTFN, OpenMode.Input)
    '    dummy = LineInput(fnum)
    '    'count the number of species defined
    '    arr = Split(dummy, ",")
    '    For j As Integer = 4 To arr.Length - 1
    '        If Trim(arr(j)) <> "" Then
    '            NWLSpecies = NWLSpecies + 1
    '            ReDim Preserve WLSpeciesName(NWLSpecies)
    '            WLSpeciesName(NWLSpecies) = arr(j)
    '        End If
    '    Next
    '    'redim arrays to capture the attributes...
    '    ReDim WLSpeciesDefined(NHabGroupLUT, NSpeciesLUT, NSizeClassLUT, NDensityLUT, NWLSpecies)
    '    ReDim NWLSpeciesTypes(NWLSpecies)
    '    MaxWLSpeciesTypeVal = 0
    '    ReDim WLSpeciesTypeVal(NWLSpecies, MaxWLSpeciesTypeVal), WLSpeciesTypeName(NWLSpecies, MaxWLSpeciesTypeVal)
    '    For jspec As Integer = 1 To NWLSpecies
    '        NWLSpeciesTypes(jspec) = 0 'default
    '    Next

    '    Do Until EOF(fnum)
    '        dummy = Trim(LineInput(fnum))
    '        If dummy.Length = 0 Then Exit Do
    '        arr = Split(dummy, ",")
    '        khg = GetStrIndex(HabGroupNameLUT, Trim(arr(0)))
    '        kct = GetStrIndex(SpeciesNameLUT, Trim(arr(1)))
    '        ksc = GetStrIndex(SizeClassNameLUT, Trim(arr(2)))
    '        kdn = GetStrIndex(DensityNameLUT, Trim(arr(3)))
    '        ' kdn = CType(arr(3), Integer) 'GetIndex(DensityNameLUT, Trim(arr(3)))
    '        'In special cases, some covertypes, etc. may not have been used by the SIMPPLLE simulation
    '        If khg > 0 And kct > 0 And ksc > 0 And kdn > 0 Then
    '            For jspec As Integer = 1 To NWLSpecies
    '                WLSpeciesDefined(khg, kct, ksc, kdn, jspec) = 0 'default
    '                If arr(jspec + 3) <> "" Then
    '                    'WLSpeciesTypeVal(jspec, 1) = 1 'default
    '                    ktype = GetStrIndex2(WLSpeciesTypeName, jspec, MaxWLSpeciesTypeVal, Trim(arr(jspec + 3))) 'GetIntIndex2(WLSpeciesTypeVal, jspec, MaxWLSpeciesTypeVal, Trim(arr(jspec + 3)))
    '                    'If ktype >= 1 Then 'CType(Trim(arr(jspec + 3)), Single) >= 1 Then
    '                    'ktype = getstrindex2(WLSpeciesTypeName,jspec,maxwlspeciestypeval,trim(arr(jspec+3)) 'GetIntIndex2(WLSpeciesTypeVal, jspec, MaxWLSpeciesTypeVal, Trim(arr(jspec + 3)))
    '                    If ktype < 0 Then
    '                        NWLSpeciesTypes(jspec) = NWLSpeciesTypes(jspec) + 1
    '                        ktype = NWLSpeciesTypes(jspec)
    '                    End If
    '                    If NWLSpeciesTypes(jspec) > MaxWLSpeciesTypeVal Then
    '                        MaxWLSpeciesTypeVal = NWLSpeciesTypes(jspec)
    '                        ReDim Preserve WLSpeciesTypeVal(NWLSpecies, MaxWLSpeciesTypeVal), WLSpeciesTypeName(NWLSpecies, MaxWLSpeciesTypeVal)
    '                    End If
    '                    WLSpeciesTypeName(jspec, ktype) = Trim(arr(jspec + 3))
    '                    WLSpeciesTypeVal(jspec, ktype) = ktype 'CType(Trim(arr(jspec + 3)), Single)
    '                    'End If
    '                    WLSpeciesDefined(khg, kct, ksc, kdn, jspec) = ktype  ' CType(Trim(arr(jspec + 3)), Single)
    '                End If
    '            Next
    '        End If
    '    Loop
    '    FileClose(fnum)
    '    'write the LUT for these outputs
    '    fnum = FreeFile()
    '    FileOpen(fnum, DirName(HabOutputFN) & "HabLUT.txt", OpenMode.Output)
    '    PrintLine(fnum, "Val_Index,Habitat_Column,Habitat_Value,SOAP_Output_Value")
    '    For jspec As Integer = 1 To NWLSpecies
    '        For jval As Integer = 1 To NWLSpeciesTypes(jspec)
    '            PrintLine(fnum, jval & "," & WLSpeciesName(jspec) & "," & WLSpeciesTypeName(jspec, jval) & "," & WLSpeciesTypeVal(jspec, jval))
    '        Next
    '    Next
    '    FileClose(fnum)
    '    'write Fragstats LUT if writing GRID output files
    '    If bWriteGrids Then
    '        fnum = FreeFile()
    '        FileOpen(fnum, DirName(HabOutputFN) & "HabLUT_ClassDescriptor.fcd", OpenMode.Output)
    '        PrintLine(fnum, "ID,Name,Enabled,IsBackground")
    '        For jspec As Integer = 1 To NWLSpecies
    '            For jval As Integer = 1 To NWLSpeciesTypes(jspec)
    '                If WLSpeciesTypeName(jspec, jval) <> "" Then
    '                    PrintLine(fnum, WLSpeciesTypeVal(jspec, jval) & "," & WLSpeciesTypeName(jspec, jval) & ",true,false")
    '                End If
    '            Next
    '        Next
    '        FileClose(fnum)
    '    End If
    '    'IF WE ARE WRITING MAPS, PRODUCE A DEFAULT COLOR FILE!
    '    If bWritePolyAtts Then
    '        For jspec As Integer = 1 To NWLSpecies
    '            fnum = FreeFile()
    '            FileOpen(fnum, DirName(HabOutputFN) & "MapColors_" & WLSpeciesName(jspec) & ".clr", OpenMode.Output)
    '            PrintLine(fnum, "NColors")
    '            PrintLine(fnum, NWLSpeciesTypes(jspec))
    '            PrintLine(fnum, "LabelColorIndex(i),ColmnName(s),LabelString(s),LabelMinVal(i),LabelMaxVal(i),R,G,B")

    '            For jval As Integer = 1 To NWLSpeciesTypes(jspec)
    '                R = Math.Round(Rnd() * 255, 0)
    '                G = Math.Round(Rnd() * 255, 0)
    '                B = Math.Round(Rnd() * 255, 0)
    '                PrintLine(fnum, jval & "," & WLSpeciesName(jspec) & "," & WLSpeciesTypeName(jspec, jval) & "," & WLSpeciesTypeVal(jspec, jval) & "," & WLSpeciesTypeVal(jspec, jval) & "," & R & "," & G & "," & B)
    '            Next
    '            FileClose(fnum)
    '        Next
    '    End If
    '    If bWriteProcessMaps Then

    '        fnum = FreeFile()
    '        FileOpen(fnum, DirName(HabOutputFN) & "MapColors_Process.clr", OpenMode.Output)
    '        PrintLine(fnum, "NColors")
    '        PrintLine(fnum, NProcessLUT)
    '        PrintLine(fnum, "LabelColorIndex(i),ColmnName(s),LabelString(s),LabelMinVal(i),LabelMaxVal(i),R,G,B")

    '        For jprocess As Integer = 1 To NProcessLUT
    '            R = Math.Round(Rnd() * 255, 0)
    '            G = Math.Round(Rnd() * 255, 0)
    '            B = Math.Round(Rnd() * 255, 0)
    '            PrintLine(fnum, jprocess & ",Process," & ProcessNameLUT(jprocess) & "," & jprocess & "," & jprocess & "," & R & "," & G & "," & B)
    '        Next
    '        FileClose(fnum)

    '    End If

    '    'LOAD WILDLIFE QUERY FILE - continuous values
    '    'continuous query metrics
    '    ReDim WLSpeciesMinElev(NWLSpecies)
    '    ReDim WLSpeciesMaxElev(NWLSpecies)
    '    ReDim WLSpeciesMinPatchSize(NWLSpecies)
    '    ReDim WLSpeciesMaxPatchSize(NWLSpecies)
    '    ReDim HomeRange(NWLSpecies)
    '    ReDim WLSpeciesMinSinceBurn(NWLSpecies)
    '    ReDim WLSpeciesMaxSinceBurn(NWLSpecies)
    '    ReDim WLSpeciesUsePreBurnCond(NWLSpecies)

    '    fnum = FreeFile()
    '    FileOpen(fnum, WLContinuousDefsLUTFN, OpenMode.Input)
    '    dummy = LineInput(fnum)
    '    'min elevation
    '    dummy = Trim(LineInput(fnum))
    '    arr = Split(dummy, ",")
    '    'check to make sure the indices match on these
    '    If arr.Length - 1 < NWLSpecies Then
    '        nshort = NWLSpecies - (arr.Length - 1)
    '        ReDim origstr(7)
    '        origstr(1) = dummy
    '        For j As Integer = 2 To 7
    '            dummy = Trim(LineInput(fnum))
    '            origstr(j) = dummy
    '        Next
    '        FileClose(fnum)
    '        fnum = FreeFile()
    '        FileOpen(fnum, WLContinuousDefsLUTFN, OpenMode.Output)
    '        dummy = "Metric"
    '        For jhab As Integer = 1 To NWLSpecies
    '            dummy = dummy & "," & WLSpeciesName(jhab)
    '        Next
    '        PrintLine(fnum, dummy)
    '        For j As Integer = 1 To 7
    '            dummy = origstr(j)
    '            For jj As Integer = 1 To nshort
    '                dummy = dummy & ","
    '            Next
    '            PrintLine(fnum, dummy)
    '        Next
    '        FileClose(fnum)
    '        'MsgBox("Warning: the Continuous Definitions file did not match Continuous Definitions. Default codes were added and used. Check the 'WildlifeContinuousDefinitions' file for accuracy.")
    '        'now open it again to pick up where you left off
    '        fnum = FreeFile()
    '        FileOpen(fnum, WLContinuousDefsLUTFN, OpenMode.Input)
    '        dummy = LineInput(fnum)
    '        'min elevation
    '        dummy = Trim(LineInput(fnum))
    '        arr = Split(dummy, ",")
    '        'MsgBox("Make sure WildlifeContinuousDefinitions has the same number of query columns as WildlifeDiscreteDefinitions. Fix this problem before re-running SOAP")
    '        'Exit Sub
    '    End If

    '    For jhab As Integer = 1 To NWLSpecies
    '        str = 0 'default
    '        If arr(jhab) <> "" Then str = arr(jhab)
    '        WLSpeciesMinElev(jhab) = str
    '    Next
    '    'max elevation
    '    dummy = Trim(LineInput(fnum))
    '    arr = Split(dummy, ",")
    '    For jhab As Integer = 1 To NWLSpecies
    '        str = 0 'default
    '        If arr(jhab) <> "" Then str = arr(jhab)
    '        WLSpeciesMaxElev(jhab) = str
    '    Next
    '    'min patch size
    '    dummy = Trim(LineInput(fnum))
    '    arr = Split(dummy, ",")
    '    For jhab As Integer = 1 To NWLSpecies
    '        str = 0 'default
    '        If arr(jhab) <> "" Then str = arr(jhab)
    '        WLSpeciesMinPatchSize(jhab) = str
    '    Next
    '    'max patch size
    '    dummy = Trim(LineInput(fnum))
    '    arr = Split(dummy, ",")
    '    For jhab As Integer = 1 To NWLSpecies
    '        str = 0 'default
    '        If arr(jhab) <> "" Then str = arr(jhab)
    '        WLSpeciesMaxPatchSize(jhab) = str
    '    Next
    '    'min yrs since burn
    '    dummy = Trim(LineInput(fnum))
    '    arr = Split(dummy, ",")
    '    For jhab As Integer = 1 To NWLSpecies
    '        str = -1 'default
    '        If arr(jhab) <> "" Then str = arr(jhab)
    '        WLSpeciesMinSinceBurn(jhab) = str
    '    Next
    '    'max patch size
    '    dummy = Trim(LineInput(fnum))
    '    arr = Split(dummy, ",")
    '    For jhab As Integer = 1 To NWLSpecies
    '        str = -1 'default
    '        If arr(jhab) <> "" Then str = arr(jhab)
    '        WLSpeciesMaxSinceBurn(jhab) = str
    '    Next
    '    If EOF(fnum) = False Then
    '        'use pre-fire conditions
    '        dummy = Trim(LineInput(fnum))
    '        arr = Split(dummy, ",")
    '        For jhab As Integer = 1 To NWLSpecies
    '            str = 0 'default
    '            If arr(jhab) <> "" Then str = arr(jhab)
    '            WLSpeciesUsePreBurnCond(jhab) = str
    '        Next
    '    End If

    '    FileClose(fnum)
    '    bLoadSuccessful = True


    'End Sub

    'Private Sub LoadSIMPPLLEOutput(ByVal InitCondFN As String, ByVal AdjFN As String, ByVal SIMPPLLERunFN As String, ByVal GridDefFN As String)
    '    Dim MaxIndex As Integer
    '    Dim fnum As Integer = FreeFile()
    '    Dim dummy As String
    '    Dim rowstr() As String
    '    Dim arr() As String
    '    Dim slink As Integer
    '    Dim timestep As Integer
    '    Dim ktimestep As Integer
    '    Dim NTimestepsLoaded As Integer
    '    Dim TimestepsLoaded() As Integer
    '    Dim adj As Integer = 0 'column index adjustment
    '    Dim oldnew As String = "old" 'indicates format of the attributesall file
    '    Dim defaultacres As Integer = 10
    '    Dim sizestr As String
    '    Dim format As String 'old or new

    '    Dim sr As IO.StreamReader
    '    Dim txtreader As Microsoft.VisualBasic.FileIO.TextFieldParser


    '    ''Dim starttime As System.DateTime = System.DateTime.Now
    '    If InitCondFN <> LastInitCondFN Then
    '        LastInitCondFN = InitCondFN
    '        'load TimeZero - first count the SLINKs
    '        MaxIndex = 0
    '        'FileOpen(fnum, InitCondFN, OpenMode.Input)
    '        'dummy = LineInput(fnum)
    '        sr = New IO.StreamReader(InitCondFN)
    '        dummy = sr.ReadLine
    '        Do While Not sr.EndOfStream 'Until EOF(fnum)
    '            dummy = sr.ReadLine 'dummy = Trim(LineInput(fnum))
    '            If Trim(dummy) = "END" Or dummy.Length = 0 Then Exit Do
    '            If dummy.Length > 0 Then
    '                arr = Split(dummy, ",")
    '                If arr(0) > MaxIndex Then MaxIndex = arr(0)
    '            End If
    '        Loop
    '        NSLINK = MaxIndex
    '        ' FileClose(fnum)
    '        sr.Close()
    '        sr = Nothing


    '        'static metrics - aa level
    '        ReDim AASLINK(NSLINK), AASTANDID(NSLINK), AAHabGroup(NSLINK), AAOwnership(NSLINK), AASpecialArea(NSLINK), AAHabGroup(NSLINK), PastProcessTimesteps(NSLINK)
    '        'by time step
    '        ReDim AALifeForm(NSLINK, NTimestep), AAAgeClass(NSLINK, NTimestep)
    '        ReDim AASpecies(NSLINK, NTimestep), AASizeClass(NSLINK, NTimestep), AADensity(NSLINK, NTimestep), AAProcess(NSLINK, NTimestep)
    '        ReDim AARow(NSLINK), AACol(NSLINK)
    '        MaxRow = 0
    '        MaxCol = 0
    '        MinRow = 999999999
    '        MinCol = 999999999

    '        'determine file format
    '        'fnum = FreeFile()
    '        'FileOpen(fnum, InitCondFN, OpenMode.Input)
    '        'dummy = LineInput(fnum)
    '        'dummy = LineInput(fnum)
    '        sr = New IO.StreamReader(InitCondFN)
    '        dummy = sr.ReadLine
    '        dummy = sr.ReadLine
    '        arr = Split(dummy, ",")
    '        If Trim(arr(4)) <> "#" Then
    '            oldnew = "new"
    '            adj = 2
    '        End If
    '        sr.Close()
    '        sr = Nothing
    '        'FileClose(fnum)
    '        'Then, get TimeZero metrics

    '        MaxIndex = 0
    '        'fnum = FreeFile()

    '        'FileOpen(fnum, InitCondFN, OpenMode.Input)
    '        'dummy = LineInput(fnum)

    '        sr = New IO.StreamReader(InitCondFN)
    '        dummy = sr.ReadLine
    '        Do While Not sr.EndOfStream 'Until EOF(fnum)
    '            dummy = sr.ReadLine 'dummy = LineInput(fnum)
    '            If Trim(dummy) = "END" Then Exit Do
    '            If dummy.Length > 0 Then
    '                arr = Split(dummy, ",")
    '                slink = arr(0)
    '                AARow(slink) = Trim(arr(1))
    '                AACol(slink) = Trim(arr(2))
    '                AASTANDID(slink) = Trim(arr(3))
    '                MaxCol = Math.Max(AACol(slink), MaxCol)
    '                MaxRow = Math.Max(AARow(slink), MaxRow)
    '                MinCol = Math.Min(AACol(slink), MinCol)
    '                MinRow = Math.Min(AARow(slink), MinRow)

    '                SIMAAAcres = defaultacres
    '                If oldnew = "new" Then SIMAAAcres = arr(4) * WLHabAcreAdjust
    '                AASLINK(slink) = slink

    '                'AALifeForm(slink, 0) = GetIndex(LifeFormName, arr(5))
    '                AASpecies(slink, 0) = Array.IndexOf(SpeciesNameLUT, arr(6 + adj)) 'GetStrIndex(SpeciesNameLUT, arr(6 + adj))

    '                'sometimes the size has a number (age) attached
    '                sizestr = ""
    '                For Each ch As String In arr(7 + adj)
    '                    If Char.IsNumber(ch) = False Then
    '                        sizestr = sizestr & ch
    '                    End If
    '                Next
    '                AASizeClass(slink, 0) = Array.IndexOf(SizeClassNameLUT, sizestr) 'arr(7 + adj)) ' GetStrIndex(SizeClassNameLUT, arr(7 + adj))

    '                'If AASpecies(slink, 0) > 0 And AASizeClass(slink, 0) = 0 Then Stop
    '                'If AASpecies(slink, 0) = 0 And AASizeClass(slink, 0) > 0 Then Stop
    '                AAHabGroup(slink) = Array.IndexOf(HabGroupNameLUT, arr(3 + adj)) ' GetStrIndex(HabGroupNameLUT, arr(3 + adj))
    '                AAOwnership(slink) = Array.IndexOf(OwnershipName, arr(15 + adj)) ' GetStrIndex(OwnershipName, arr(15 + adj))
    '                AASpecialArea(slink) = Array.IndexOf(SpecialAreaName, arr(19 + adj)) ' GetStrIndex(SpecialAreaName, arr(19 + adj))
    '                If AASpecialArea(slink) < 0 Then AASpecialArea(slink) = 0 'default/unknown
    '                'AAAgeClass(slink, 0) = arr(8)
    '                AADensity(slink, 0) = -1 'default
    '                If arr(8 + adj) <> "None" Then AADensity(slink, 0) = Array.IndexOf(DensityNameLUT, arr(8 + adj)) ' GetStrIndex(DensityNameLUT, arr(8 + adj)) 'straight crosswalk - old:GetIndex(DensityNameLUT, arr(10))
    '                AAProcess(slink, 0) = -1 'default
    '                If arr(9 + adj) <> "?" Then AAProcess(slink, 0) = Array.IndexOf(ProcessNameLUT, arr(9 + adj)) ' GetStrIndex(ProcessNameLUT, arr(9 + adj))
    '                PastProcessTimesteps(slink) = 0
    '                If arr(10 + adj) <> "?" Then PastProcessTimesteps(slink) = CType(arr(10 + adj), Integer)
    '            End If
    '        Loop
    '        'FileClose(fnum)
    '        sr.Close()
    '        sr = Nothing
    '    End If
    '    ReDim ColRowSLINK(MaxCol + 1, MaxRow + 1) '+1 to enable searches that look for the next row and the next col of the biggest one
    '    For jaa As Integer = 1 To NSLINK
    '        ColRowSLINK(AACol(jaa), AARow(jaa)) = AASLINK(jaa)
    '    Next

    '    If AdjFN <> LastAdjFN Then
    '        MaxStandAdj = 0
    '        LastAdjFN = AdjFN
    '        'Load Spatial Relationships
    '        ReDim AAStandAdj(NSLINK, MaxAdj), AANStandAdj(NSLINK), AAElevation(NSLINK)
    '        'fnum = FreeFile()
    '        'FileOpen(fnum, AdjFN, OpenMode.Input)
    '        'dummy = LineInput(fnum)
    '        sr = New IO.StreamReader(AdjFN)
    '        dummy = sr.ReadLine
    '        Do While Not sr.EndOfStream 'Until EOF(fnum)
    '            dummy = sr.ReadLine 'dummy = LineInput(fnum)
    '            If Trim(dummy) = "END" Then Exit Do
    '            If dummy.Length > 0 Then
    '                arr = Split(dummy, ",")
    '                slink = arr(0)
    '                AANStandAdj(slink) = AANStandAdj(slink) + 1
    '                MaxStandAdj = Math.Max(AANStandAdj(slink), MaxStandAdj)
    '                AAStandAdj(slink, AANStandAdj(slink)) = arr(1)
    '                AAElevation(slink) = 1000 'default
    '                If oldnew = "new" Then AAElevation(slink) = arr(2)
    '            End If
    '        Loop
    '        'FileClose(fnum)
    '        sr.Close()
    '        sr = Nothing
    '    End If

    '    ''Dim stoptime As System.DateTime = System.DateTime.Now
    '    ''Dim tottime As System.TimeSpan = stoptime - starttime

    '    ''Stop

    '    'Load the grid definition file
    '    If GridDefFN <> LastGridDefFN Then
    '        LastGridDefFN = GridDefFN
    '        LoadGridDefFile(GridDefFN)
    '    End If

    '    'by time step

    '    'load the simulation data file
    '    'FileOpen(fnum, SIMPPLLERunFN, OpenMode.Input)
    '    ReDim TimestepsLoaded(NTimestep)
    '    NTimestepsLoaded = 0
    '    Dim arrRT() As String = Split(SIMPPLLERunFN, ".")

    '    If arrRT(arrRT.Length - 1) = "txt" Or arrRT(arrRT.Length - 1) = "csv" Then 'we have an unzipped text file

    '        sr = New IO.StreamReader(SIMPPLLERunFN)
    '        dummy = sr.ReadLine
    '        'we now have to key in on the file format - look at the 4th element of the header
    '        arr = Split(dummy, ",")
    '        If arr(4) = "ACRES" Then format = "old"
    '        If arr(4) = "LIFEFORM" Then format = "new"
    '        'dummy = LineInput(fnum)
    '        Do While Not sr.EndOfStream
    '            dummy = sr.ReadLine ' LineInput(fnum)
    '            If dummy.Length > 0 Then
    '                arr = Split(dummy, ",")
    '                slink = arr(3)
    '                'If slink = 70271 Then Stop
    '                timestep = arr(1)
    '                If timestep > TimestepIndex(NTimestep) And NTimestepsLoaded >= NTimestep Then Exit Do
    '                ktimestep = GetIntIndex(TimestepIndex, timestep)
    '                If ktimestep > -1 Then
    '                    If TimestepsLoaded(ktimestep) = 0 Then
    '                        TimestepsLoaded(ktimestep) = 1
    '                        NTimestepsLoaded = NTimestepsLoaded + 1
    '                    End If
    '                    If format = "old" Then
    '                        AASpecies(slink, ktimestep) = Array.IndexOf(SpeciesNameLUT, SpeciesNameXW(arr(6))) 'GetStrIndex(SpeciesNameLUT, SpeciesNameXW(arr(6)))
    '                        AASizeClass(slink, ktimestep) = Array.IndexOf(SizeClassNameLUT, SizeClassNameXW(arr(7))) 'GetStrIndex(SizeClassNameLUT, SizeClassNameXW(arr(7)))
    '                        AADensity(slink, ktimestep) = Array.IndexOf(DensityNameLUT, DensityNameXW(arr(9))) 'GetStrIndex(DensityNameLUT, DensityNameXW(arr(9))) 'DensityNameXW(arr(9)) '
    '                        AAProcess(slink, ktimestep) = Array.IndexOf(ProcessNameLUT, ProcessNameXW(arr(10))) 'GetStrIndex(ProcessNameLUT, ProcessNameXW(arr(10)))
    '                    Else
    '                        AASpecies(slink, ktimestep) = Array.IndexOf(SpeciesNameLUT, SpeciesNameXW(arr(5))) 'GetStrIndex(SpeciesNameLUT, SpeciesNameXW(arr(6)))
    '                        AASizeClass(slink, ktimestep) = Array.IndexOf(SizeClassNameLUT, SizeClassNameXW(arr(6))) 'GetStrIndex(SizeClassNameLUT, SizeClassNameXW(arr(7)))
    '                        AADensity(slink, ktimestep) = Array.IndexOf(DensityNameLUT, DensityNameXW(arr(8))) 'GetStrIndex(DensityNameLUT, DensityNameXW(arr(9))) 'DensityNameXW(arr(9)) '
    '                        AAProcess(slink, ktimestep) = Array.IndexOf(ProcessNameLUT, ProcessNameXW(arr(9))) 'GetStrIndex(ProcessNameLUT, ProcessNameXW(arr(10)))
    '                        If arr(12) >= 0 Then
    '                            AAProcess(slink, ktimestep) = Array.IndexOf(ProcessNameLUT, TreatmentNameXW(arr(12)))
    '                        End If
    '                    End If

    '                End If
    '            End If
    '        Loop
    '        sr.Close()
    '        sr = Nothing


    '    Else


    '        Using rawStream = New System.IO.FileStream(SIMPPLLERunFN, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read)
    '            Using cstream = New System.IO.Compression.GZipStream(rawStream, System.IO.Compression.CompressionMode.Decompress)
    '                Dim currLine As String = ""
    '                Dim currentByte As Integer

    '                'read in a dummy line
    '                currentByte = cstream.ReadByte()
    '                While currentByte <> -1
    '                    currLine += Convert.ToChar(currentByte)
    '                    If currLine.EndsWith(vbCrLf) Then
    '                        currLine = ""
    '                        Exit While
    '                    End If
    '                    currentByte = cstream.ReadByte()
    '                End While

    '                currentByte = cstream.ReadByte()
    '                While currentByte <> -1
    '                    currLine += Convert.ToChar(currentByte)
    '                    If currLine.EndsWith(vbCrLf) Then
    '                        dummy = Trim(currLine)
    '                        If dummy.Length > 0 Then
    '                            arr = Split(dummy, ",")
    '                            slink = arr(3)
    '                            'If slink = 70271 Then Stop
    '                            timestep = arr(1)
    '                            If timestep > TimestepIndex(NTimestep) And NTimestepsLoaded >= NTimestep Then Exit While
    '                            ktimestep = GetIntIndex(TimestepIndex, timestep)
    '                            If ktimestep > -1 Then
    '                                If TimestepsLoaded(ktimestep) = 0 Then
    '                                    TimestepsLoaded(ktimestep) = 1
    '                                    NTimestepsLoaded = NTimestepsLoaded + 1
    '                                End If
    '                                AASpecies(slink, ktimestep) = GetStrIndex(SpeciesNameLUT, SpeciesNameXW(arr(6)))
    '                                AASizeClass(slink, ktimestep) = GetStrIndex(SizeClassNameLUT, SizeClassNameXW(arr(7)))
    '                                AADensity(slink, ktimestep) = GetStrIndex(DensityNameLUT, DensityNameXW(arr(9))) 'DensityNameXW(arr(9)) '
    '                                AAProcess(slink, ktimestep) = GetStrIndex(ProcessNameLUT, ProcessNameXW(arr(10)))
    '                            End If
    '                        End If

    '                        currLine = ""
    '                        'Exit While
    '                    End If
    '                    currentByte = cstream.ReadByte()
    '                End While
    '            End Using
    '        End Using
    '    End If

    '    'FileClose(fnum)


    'End Sub

    'Private Sub LoadGridDefFile(ByVal Filename As String)
    '    'this loads the row/col indices for each polygon
    '    'if one wanted to, this could be incorporated into the "load attributsall" code to reduce double-reading that file
    '    Dim dummy As String
    '    Dim dummy2 As String
    '    Dim arr() As String
    '    Dim RowInd As Integer
    '    Dim ColInd As Integer
    '    Dim SLINKInd As Integer
    '    Dim AcresInd As Integer
    '    Dim row As Integer
    '    Dim col As Integer
    '    Dim slink As Integer

    '    Dim fnum As Integer = FreeFile()
    '    FileOpen(fnum, Filename, OpenMode.Input)

    '    'the header line - find the appropriate indices
    '    dummy = Trim(LineInput(fnum))


    '    'arr = Split(dummy, ",")
    '    'For j As Integer = 0 To arr.Length - 1
    '    '    dummy2 = StripQuotes(arr(j))
    '    '    If dummy2 = "ROW" Or dummy2 = "Row" Or dummy2 = "row" Then RowInd = j
    '    '    If dummy2 = "COL" Or dummy2 = "Col" Or dummy2 = "col" Then ColInd = j
    '    '    If dummy2 = "SLINK" Or dummy2 = "Slink" Or dummy2 = "slink" Then SLINKInd = j
    '    '    If dummy2 = "Acres" Or dummy2 = "ACRES" Or dummy2 = "acres" Then AcresInd = j
    '    'Next
    '    'changed since these column indices are constant in the .attributesall files
    '    RowInd = 2
    '    ColInd = 1
    '    SLINKInd = 0
    '    AcresInd = 4

    '    'find biggest row,col
    '    BiggestRow = -99
    '    BiggestCol = -99
    '    Do Until EOF(fnum)
    '        dummy = Trim(LineInput(fnum))
    '        If dummy.Length = 0 Or dummy = "END" Then Exit Do
    '        arr = Split(dummy, ",")
    '        If arr(RowInd) > BiggestRow Then BiggestRow = arr(RowInd)
    '        If arr(ColInd) > BiggestCol Then BiggestCol = arr(ColInd)
    '    Loop
    '    FileClose(fnum)

    '    cellsize = -100
    '    'populate the arrays
    '    ReDim GridSlink(BiggestRow, BiggestCol)
    '    fnum = FreeFile()
    '    FileOpen(fnum, Filename, OpenMode.Input)
    '    dummy = Trim(LineInput(fnum))
    '    Do Until EOF(fnum)
    '        dummy = Trim(LineInput(fnum))
    '        If dummy.Length = 0 Or dummy = "END" Then Exit Do
    '        arr = Split(dummy, ",")
    '        row = arr(RowInd)
    '        col = arr(ColInd)
    '        slink = arr(SLINKInd)
    '        If cellsize < 0 Then
    '            cellsize = Math.Round(Math.Sqrt(arr(AcresInd) * 4046.856 * WLHabAcreAdjust), 3)
    '        End If
    '        GridSlink(row, col) = slink
    '    Loop
    '    FileClose(fnum)
    'End Sub

    'Private Sub ClearLargeArrays()
    '    'ReDim WLSpeciesAcres(NWLSpecies, MaxWLSpeciesTypeVal, NTimestep)
    '    ' AAWLHabitat(NSLINK, NWLSpecies, NTimestep)
    '    ' AAWLHabitatMult(NSLINK, NWLSpecies, NTimestep)
    '    'For jaa As Integer = 1 To NSLINK
    '    '    For jwl As Integer = 1 To NWLSpecies
    '    '        For jtic As Integer = 1 To NTimestep
    '    '            AAWLHabitat(jaa, jwl, jtic) = 0
    '    '            AAWLHabitatMult(jaa, jwl, jtic) = 0
    '    '        Next
    '    '    Next
    '    'Next

    'End Sub

    'Private Sub FillCodesRunOutput(ByVal runoutfolder As String, ByVal ext As String) 'ByVal fnumnewcodes As Integer)
    '    'uses the output files from a run to fill in information about processes, species, density, habitat groups, size classes
    '    Dim dummy As String
    '    Dim arr() As String
    '    Dim dummyL As String
    '    Dim k As Integer
    '    Dim fnum As Integer

    '    'NDensityLUT = 0
    '    'NHabGroupLUT = 0


    '    'read the density file
    '    fnum = FreeFile()
    '    FileOpen(fnum, runoutfolder & "\" & "DENSITY." & ext, OpenMode.Input)
    '    dummy = LineInput(fnum)

    '    Do Until EOF(fnum)
    '        dummy = Trim(LineInput(fnum))
    '        If dummy.Length = 0 Then Exit Do
    '        arr = Split(dummy, ",")
    '        dummy = Trim(arr(1))
    '        k = GetStrIndex(DensityNameLUT, dummy) 'CType(arr(3), Integer) 'GetIndex(DensityNameLUT, Trim(arr(3)))
    '        If k < 0 Then
    '            NDensityLUT = NDensityLUT + 1
    '            'NNewCodes = NNewCodes + 1
    '            ReDim Preserve DensityNameLUT(NDensityLUT) ', NewCodes(NNewCodes)
    '            DensityNameLUT(NDensityLUT) = dummy
    '            ' NewCodes(NNewCodes) = dummy
    '            dummyL = NDensityLUT & "," & dummy
    '            AddLine(DensityLUTFN, dummyL)
    '            'PrintLine(fnumnewcodes, "DENSITY," & dummy)
    '        End If

    '    Loop
    '    FileClose(fnum)


    '    'read the treatments file - if it exists - and add it to the list of processes
    '    If System.IO.File.Exists(runoutfolder & "\" & "TREATMENT." & ext) Then
    '        fnum = FreeFile()
    '        FileOpen(fnum, runoutfolder & "\" & "TREATMENT." & ext, OpenMode.Input)
    '        dummy = LineInput(fnum)

    '        Do Until EOF(fnum)
    '            dummy = Trim(LineInput(fnum))
    '            If dummy.Length = 0 Then Exit Do
    '            arr = Split(dummy, ",")
    '            dummy = Trim(arr(1))
    '            k = GetStrIndex(ProcessNameLUT, dummy) 'CType(arr(3), Integer) 'GetIndex(DensityNameLUT, Trim(arr(3)))
    '            If k < 0 Then
    '                NProcessLUT = NProcessLUT + 1
    '                'NNewCodes = NNewCodes + 1
    '                ReDim Preserve ProcessNameLUT(NProcessLUT) ', NewCodes(NNewCodes)
    '                ProcessNameLUT(NProcessLUT) = dummy
    '                ' NewCodes(NNewCodes) = dummy
    '                dummyL = NProcessLUT & "," & dummy
    '                AddLine(AllProcessLUTFN, dummyL)
    '                'PrintLine(fnumnewcodes, "DENSITY," & dummy)
    '            End If

    '        Loop
    '        FileClose(fnum)
    '    End If

    '    ''read the ecogroup file
    '    'fnum = FreeFile()
    '    'FileOpen(fnum, runoutfolder & "\" & "ECOGROUP.txt", OpenMode.Input)
    '    'dummy = LineInput(fnum)

    '    'Do Until EOF(fnum)
    '    '    dummy = Trim(LineInput(fnum))
    '    '    If dummy.Length = 0 Then Exit Do
    '    '    arr = Split(dummy, ",")
    '    '    dummy = Trim(arr(1))
    '    '    k = GetStrIndex(HabGroupNameLUT, dummy)
    '    '    If k < 0 Then
    '    '        NHabGroupLUT = NHabGroupLUT + 1
    '    '        'NNewCodes = NNewCodes + 1
    '    '        ReDim Preserve HabGroupNameLUT(NHabGroupLUT) ', NewCodes(NNewCodes)
    '    '        HabGroupNameLUT(NHabGroupLUT) = dummy
    '    '        'NewCodes(NNewCodes) = dummy
    '    '        dummyL = NHabGroupLUT & "," & dummy
    '    '        AddLine(HabGroupLUTFN, dummyL)
    '    '        'PrintLine(fnumnewcodes, "ECOGROUP," & dummy)
    '    '    End If

    '    'Loop
    '    'FileClose(fnum)

    '    'read the process file
    '    fnum = FreeFile()
    '    FileOpen(fnum, runoutfolder & "\" & "PROCESS." & ext, OpenMode.Input)
    '    dummy = LineInput(fnum)

    '    Do Until EOF(fnum)
    '        dummy = Trim(LineInput(fnum))
    '        If dummy.Length = 0 Then Exit Do
    '        arr = Split(dummy, ",")
    '        dummy = Trim(arr(1))
    '        k = GetStrIndex(ProcessNameLUT, dummy)
    '        If k < 0 Then
    '            NProcessLUT = NProcessLUT + 1
    '            ' NNewCodes = NNewCodes + 1
    '            ReDim Preserve ProcessNameLUT(NProcessLUT) ', NewCodes(NNewCodes)
    '            ProcessNameLUT(NProcessLUT) = dummy
    '            'NewCodes(NNewCodes) = dummy
    '            dummyL = NProcessLUT & "," & dummy
    '            AddLine(AllProcessLUTFN, dummyL)
    '            'PrintLine(fnumnewcodes, "PROCESS," & dummy)
    '        End If

    '    Loop
    '    FileClose(fnum)

    '    'read the sizeclass file
    '    fnum = FreeFile()
    '    FileOpen(fnum, runoutfolder & "\" & "SIZECLASS." & ext, OpenMode.Input)
    '    dummy = LineInput(fnum)

    '    Do Until EOF(fnum)
    '        dummy = Trim(LineInput(fnum))
    '        If dummy.Length = 0 Then Exit Do
    '        arr = Split(dummy, ",")
    '        dummy = Trim(arr(1))
    '        k = GetStrIndex(SizeClassNameLUT, dummy)
    '        If k < 0 Then
    '            NSizeClassLUT = NSizeClassLUT + 1
    '            'NNewCodes = NNewCodes + 1
    '            ReDim Preserve SizeClassNameLUT(NSizeClassLUT) ', NewCodes(NNewCodes)
    '            SizeClassNameLUT(NSizeClassLUT) = dummy
    '            'NewCodes(NNewCodes) = dummy
    '            dummyL = NSizeClassLUT & "," & dummy
    '            AddLine(SizeClassLUTFN, dummyL)
    '            'PrintLine(fnumnewcodes, "SIZECLASS," & dummy)
    '        End If

    '    Loop
    '    FileClose(fnum)

    '    'read the species file
    '    fnum = FreeFile()
    '    FileOpen(fnum, runoutfolder & "\" & "SPECIES." & ext, OpenMode.Input)
    '    dummy = LineInput(fnum)

    '    Do Until EOF(fnum)
    '        dummy = Trim(LineInput(fnum))
    '        If dummy.Length = 0 Then Exit Do
    '        arr = Split(dummy, ",")
    '        dummy = Trim(arr(1))
    '        k = GetStrIndex(SpeciesNameLUT, dummy)
    '        If k < 0 Then
    '            NSpeciesLUT = NSpeciesLUT + 1
    '            'NNewCodes = NNewCodes + 1
    '            ReDim Preserve SpeciesNameLUT(NSpeciesLUT) ', NewCodes(NNewCodes)
    '            SpeciesNameLUT(NSpeciesLUT) = dummy
    '            'NewCodes(NNewCodes) = dummy
    '            dummyL = NSpeciesLUT & "," & dummy
    '            AddLine(SpeciesLUTFN, dummyL)
    '            'PrintLine(fnumnewcodes, "SPECIES," & dummy)
    '        End If
    '    Loop
    '    FileClose(fnum)

    '    'read the ownership file
    '    fnum = FreeFile()
    '    FileOpen(fnum, runoutfolder & "\" & "OWNERSHIP." & ext, OpenMode.Input)
    '    dummy = LineInput(fnum)

    '    Do Until EOF(fnum)
    '        dummy = Trim(LineInput(fnum))
    '        If dummy.Length = 0 Then Exit Do
    '        arr = Split(dummy, ",")
    '        dummy = Trim(arr(1))
    '        k = GetStrIndex(OwnershipName, dummy)
    '        If k < 0 Then
    '            NOwnerships = NOwnerships + 1
    '            'NNewCodes = NNewCodes + 1
    '            ReDim Preserve OwnershipName(NOwnerships) ', NewCodes(NNewCodes)
    '            OwnershipName(NOwnerships) = dummy
    '            'NewCodes(NNewCodes) = dummy
    '            dummyL = NOwnerships & "," & dummy
    '            AddLine(OwnershipLUTFN, dummyL)
    '            'PrintLine(fnumnewcodes, "SPECIES," & dummy)
    '        End If
    '    Loop
    '    FileClose(fnum)

    '    'read the special area
    '    fnum = FreeFile()
    '    FileOpen(fnum, runoutfolder & "\" & "SPECIALAREA." & ext, OpenMode.Input)
    '    dummy = LineInput(fnum)

    '    Do Until EOF(fnum)
    '        dummy = Trim(LineInput(fnum))
    '        If dummy.Length = 0 Then Exit Do
    '        arr = Split(dummy, ",")
    '        dummy = Trim(arr(1))
    '        k = GetStrIndex(SpecialAreaName, dummy)
    '        If k < 0 Then
    '            NSpecialAreas = NSpecialAreas + 1
    '            'NNewCodes = NNewCodes + 1
    '            ReDim Preserve SpecialAreaName(NSpecialAreas) ', NewCodes(NNewCodes)
    '            SpecialAreaName(NSpecialAreas) = dummy
    '            'NewCodes(NNewCodes) = dummy
    '            dummyL = NSpecialAreas & "," & dummy
    '            AddLine(SpecialAreaLUTFN, dummyL)
    '            'PrintLine(fnumnewcodes, "SPECIES," & dummy)
    '        End If
    '    Loop
    '    FileClose(fnum)

    'End Sub
    'Private Sub AddLine(ByVal filename As String, ByVal line As String)
    '    Dim fnum As Integer
    '    If System.IO.File.Exists(filename) = False Then
    '        fnum = FreeFile()
    '        FileOpen(fnum, filename, OpenMode.Output)
    '        PrintLine(fnum, "ID,ATTRIBUTE_NAME")
    '        FileClose(fnum)
    '    End If
    '    fnum = FreeFile()
    '    FileOpen(fnum, filename, OpenMode.Append)
    '    PrintLine(fnum, line)
    '    FileClose(fnum)
    'End Sub



    'Private Shared Function Trim(ByVal currLine As String) As String
    '    '\x00EF\x00BB\x00BF
    '    Return currLine.TrimEnd(ControlChars.Cr, ControlChars.Lf).TrimStart(Convert.ToChar(&HEF), Convert.ToChar(&HBB), Convert.ToChar(&HBF))
    'End Function
End Class
