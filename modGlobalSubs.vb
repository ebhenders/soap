Imports System.Text
Module modGlobalSubs

    'file name stuff and directories
    Public ICDefFileName As String
    Public OutFilesToProcessFileName As String
    Public LUTBaseFolder As String
    Public HabOutputFN As String
    Public FileListFN As String
    Public OutputFile As String

    Public ICFileName() As String
    Public NICFiles As Integer

    Public OutFilesToProcessDir() As String
    Public OutFilesToProcessName() As String
    Public OutFilesToProcessNumber() As String
    Public OutFilesTOProcessFullName() As String
    Public NOutFiles As Integer

    Public RunToUse As Integer
    Public GeoAreaName As String

    Public SimpplleOutputFN As String
    Public TimeZeroFN As String
    Public InitialCondFN As String
    Public ProcessXWFN As String
    Public TreatmentXWFN As String
    Public SpecialAreaXWFN As String
    Public SpeciesXWFN As String
    Public OwnershipXWFN As String
    Public LifeFormXWFN As String
    Public DensityXWFN As String
    Public HabGroupXWFN As String
    Public SizeClassXWFN As String

    Public SuitabilityLUTFN As String
    Public SuitCodesToUseLUTFN As String
    Public SpectrumSpeciesLUTFN As String
    Public AllProcessLUTFN As String
    Public SizeClassLUTFN As String
    Public WLDiscreteDefsLUTFN As String
    Public WLContinuousDefsLUTFN As String
    Public HabGroupLUTFN As String
    Public DensityLUTFN As String
    Public SpeciesLUTFN As String
    Public FireProcessLUTFN As String
    Public OwnershipLUTFN As String
    Public SpecialAreaLUTFN As String

    '         For creating SIMPPLLE input files
    'Public SpatialRelationsRawFN As String
    'Public SpatialRelationsFieldFN As String


    'other variables
    Public NTimestep As Integer ' = 5
    Public TimestepIndex() As Integer
    Public MaxTimestep As Integer
    Public NSLINK As Integer
    Public MaxAdj As Integer = 8 'max number of adjacent cells
    Public WLHabAcreAdjust As Double

    'static metrics - aa level
    'Public AAAcres() As Double 'dimmed by NSLINK
    Public bSplitMaps As Boolean
    Public SIMAAAcres As Double 'single value
    Public SpecAAAcres() As Double 'can vary by AA
    Public AASLINK() As Long
    Public AASTANDID() As String
    Public AAHabGroup() As Integer
    Public AAOwnership() As Integer
    Public AASpecialArea() As Integer
    Public AAElevation() As Double
    Public AACol() As Integer
    Public AARow() As Integer
    Public ColRowSLINK(,) As Long
    Public MaxRow As Integer
    Public MaxCol As Integer
    Public MinRow As Integer
    Public MinCol As Integer

    'by time step
    Public AALifeForm(,) As Integer
    Public AASpecies(,) As Integer
    Public AASizeClass(,) As Integer
    Public AAAgeClass(,) As Integer
    Public AADensity(,) As Integer
    Public AAProcess(,) As Single
    Public PastProcessTimesteps() As Integer 'by NSLINK
    'by WL Habitat
    Public AAWLHabitat(,,) As Byte 'by AA, WL habitat, timestep
    'Public AAWLHabitatMult(,,) As Single 'multiplier value - can be less than 1
    Public AAWLHabitatStr(,,) As Byte 'by AA, WL habitat, timestep - stores the value to print out as string. These are integer values, however
    Public bWritePolyAtts As Boolean
    Public bWriteGISMaps As Boolean
    Public bWriteProcessMaps As Boolean
    'Public bWriteTreatmentMaps As Boolean
    Public bWriteProcessStats As Boolean
    'Public bWriteTreatmentStats As Boolean
    Public bWriteFireSpreadMaps As Boolean
    Public bWriteGrids As Boolean
    Public bWritePatchMaps As Boolean

    'by adjacency
    Public AAStandAdj(,) As Single 'by jaa, MaxAdj - reflects Stand ID of adjacent stand
    Public AANStandAdj() As Integer 'count the number of adjacent stands for each poly
    Public MaxStandAdj As Integer = 0 'maximum number of adjacent stands for any given polygon

    'discrete value query definitions
    Public WLSpeciesDefined(,,,,) As Single 'by habgroup, covertype, sizeclass, crownclosure, WLSpecies
    Public WLSpeciesAcres(,,) As Double 'acres by species, type, time step
    Public WLSpeciesName() As String
    Public NWLSpecies As Integer
    Public NWLSpeciesTypes() As Integer 'by WL Species - for each species, you can specify different hab types
    Public WLSpeciesTypeVal(,) As Integer 'by species, maxWLSpeciesTypeVal
    Public WLSpeciesTypeName(,) As String 'by species, maxWLSpeciesTypeVal
    Public MaxWLSpeciesTypeVal As Integer

    'Report acre query definitions
    Public NReportGroups As Integer
    'Public NReportDefs As Integer
    Public ReportGroupName() As String
    'Public ReportDefGroup() As Integer 'by nreport defs - index of the associated report group
    Public ReportDefinition(,,) As Integer 'by NReportGroups,NSpecialArea,NOwnership

    'continuous query metrics
    Public WLSpeciesMinElev() As Double
    Public WLSpeciesMaxElev() As Double
    Public WLSpeciesMinPatchSize() As Double
    Public WLSpeciesMaxPatchSize() As Double
    Public HomeRange() As Single 'size in acres of the area of the home range - assumed to be a circle
    Public WLSpeciesMinSinceBurn() As Double
    Public WLSpeciesMaxSinceBurn() As Double
    Public WLSpeciesUsePreBurnCond() As Boolean

    'patch stuff
    Public AAPatchIndex(,,) As Single 'per poly, habitat, timestep - identifies the patch ID number
    Public WLPatchSize(,,) As Single 'by patch number, habitat, timestep - identifies the size of the patch

    'grid variables
    Public BiggestRow As Long
    Public BiggestCol As Long
    Public GridSlink(,) As Long 'Slink in a col,row - dimensioned by biggestrow, biggestcol
    Public cellsize As Double 'length in meters of the side of a cell

    'moving windows stuff
    Public WindowCellVal(,) As Double

    'stuff for VMAPtoSIMPPLLE
    Public FN_VMAPtoSIMPPLLE As String
    Public bKeepGoodCodes As Boolean
    Public bIterate As Boolean
    'Public NIterations As Integer
    Public NSaveIterations As Integer

    'stuff for combining files
    Public bAddition As Boolean
    Public bSummarize As Boolean
    Public bConcatenate As Boolean

    Public bSplitOutFiles As Boolean

    'stuff for constructing the batch file and Selector form
    Public FrmCancel As Boolean
    Public ItemsToRead() As String 'selections returned from Selector form
    Public NumItems As Integer
    Public SelectorText As String
    Public ItemName() As String 'used to populate the Selector Form

    'stuff for text input form
    Public txtInputLabel As String
    Public txtInputText As String
    Public txtReturnTextValue As String




    Public Sub DefineFileNames(ByVal BaseFolder As String, ByVal RunNum As Integer, ByVal ext As String)
        Dim letter As String


        letter = BaseFolder.Substring(BaseFolder.Length - 1, 1)
        If letter <> "\" Then BaseFolder = BaseFolder & "\"

        'RunToUse = CType(MainForm.tbRunToUse.Text, Integer)
        'GeoAreaName = Trim(MainForm.tbGeoAreaName.Text)

        SimpplleOutputFN = BaseFolder & "EVU_SIM_DATA" & RunNum & "." & ext '".txt"
        'SimpplleOutputFN = BaseFolder & "EVU_SIM_DATA" & RunNum & ".txt.gz"
        'TimeZeroFN = BaseFolder & "TIMEZERO_" & GeoAreaName & ".txt"
        'InitialCondFN = BaseFolder & GeoAreaName & "_IC.txt"
        ProcessXWFN = BaseFolder & "PROCESS." & ext 'txt"
        TreatmentXWFN = BaseFolder & "TREATMENT." & ext
        SpecialAreaXWFN = BaseFolder & "SPECIALAREA." & ext 'txt"
        SpeciesXWFN = BaseFolder & "SPECIES." & ext 'txt"
        OwnershipXWFN = BaseFolder & "OWNERSHIP." & ext 'txt"
        LifeFormXWFN = BaseFolder & "LIFEFORM." & ext 'txt"
        DensityXWFN = BaseFolder & "DENSITY." & ext 'txt"
        HabGroupXWFN = BaseFolder & "HABGROUP." & ext 'txt"
        SizeClassXWFN = BaseFolder & "SIZECLASS." & ext 'txt"
        SpectrumSpeciesLUTFN = BaseFolder & "SPECIES_LUT." & ext 'txt" 'different species lut files are used for wildlife queries vs. SIMPPLLE to Spectrum translation


    End Sub

    Public Sub DefineLUTFileNames(ByVal basefolder As String)
        Dim letter As String
        letter = basefolder.Substring(basefolder.Length - 1, 1)
        If letter <> "\" Then basefolder = basefolder & "\"

        SuitabilityLUTFN = basefolder & "SUITABILITY_LUT.txt"
        AllProcessLUTFN = basefolder & "PROCESS_LUT.txt"
        SizeClassLUTFN = basefolder & "SIZECLASS_LUT.txt"
        HabGroupLUTFN = basefolder & "HABGROUP_LUT.txt"
        DensityLUTFN = basefolder & "DENSITY_LUT.txt"
        SpeciesLUTFN = basefolder & "SPECIES_LUT.txt"
        FireProcessLUTFN = basefolder & "FIRE_PROCESS_LUT.txt"
        OwnershipLUTFN = basefolder & "OWNERSHIP_LUT.txt"
        SpecialAreaLUTFN = basefolder & "SPECIAL_AREA_LUT.txt"
        SuitCodesToUseLUTFN = basefolder & "SUIT_CODES_TO_USE_LUT.txt"

        WLDiscreteDefsLUTFN = basefolder & "WildlifeDiscreteDefinitions.csv"
        WLContinuousDefsLUTFN = basefolder & "WildlifeContinuousDefinitions.csv"
    End Sub


    Public Function GetStrIndex(ByVal Array() As String, ByVal CompareVal As String) As Integer
        GetStrIndex = -1 'default
        For j As Integer = 0 To Array.Length - 1
            If CompareVal = Array(j) Then
                GetStrIndex = j
                Exit Function
            End If
        Next
    End Function
    Public Function GetStrIndex2(ByVal Array(,) As String, ByVal kfirstind As Integer, ByVal len2ndind As Integer, ByVal CompareVal As String) As Integer
        GetStrIndex2 = -1 'default
        For j As Integer = 0 To len2ndind
            If CompareVal = Array(kfirstind, j) Then
                GetStrIndex2 = j
                Exit Function
            End If
        Next
    End Function
    Public Function GetIntIndex2(ByVal Array(,) As Integer, ByVal kfirstind As Integer, ByVal Len2ndInd As Integer, ByVal CompareVal As Integer) As Integer
        GetIntIndex2 = -1 'default
        For j As Integer = 0 To Len2ndInd
            If CompareVal = Array(kfirstind, j) Then
                GetIntIndex2 = j
                Exit Function
            End If
        Next
    End Function
    Public Function GetIntIndex(ByVal Array() As Integer, ByVal CompareVal As Integer) As Integer
        GetIntIndex = -1 'default
        For j As Integer = 0 To Array.Length - 1
            If CompareVal = Array(j) Then
                GetIntIndex = j
                Exit Function
            End If
        Next
    End Function

    Public Function StripQuotes(ByVal str As String) As String
        StripQuotes = str
        Dim s As String = ""
        For i As Integer = 0 To str.Length - 1
            If str.Substring(i, 1) <> Chr(34) Then s = s & str.Substring(i, 1) 'And str.Substring(i, 1) <> Chr(32)
        Next
        StripQuotes = s

    End Function

    Public Function DirName(ByVal FullName As String) As String
        'returns name of the directory with the "\"
        Dim i As Integer
        Dim dummy As String
        dummy = FullName
        i = 0
        DirName = ""
        'check for blank pass
        If Trim(FullName).Length = 0 Then Exit Function

        Do
            i = i + 1
            If dummy.Substring(Len(dummy) - i, 1) = "\" Then Exit Do
        Loop
        If i >= Len(dummy) Then
            DirName = ""
        Else
            DirName = dummy.Substring(0, (Len(dummy) - i))
        End If
        DirName = DirName & "\"
    End Function

    Public Function FName(ByVal FullName As String) As String
        Dim arr() As String
        Dim arr2() As String
        arr = Split(FullName, "\")
        arr2 = Split(arr(arr.Length - 1), ".")
        FName = arr2(0)

    End Function

    Public Sub CleanFile(ByVal infile As String)
        'this routine goes through and deletes commas out of strings
        Dim str As String
        Dim strb As StringBuilder
        Dim dummy As String
        'Dim outdummy As String
        Dim bswitch As Integer = 1
        Dim fnum As Integer
        Dim fnum2 As Integer

        Dim outfile As String
        outfile = DirName(infile) & FName(infile) & "_clean.txt"

        fnum = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        fnum2 = FreeFile()
        FileOpen(fnum2, outfile, OpenMode.Output)
        Do Until EOF(fnum)
            'outdummy = ""
            strb = New StringBuilder("", 500)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            bswitch = -1
            For j As Integer = 0 To dummy.Length - 1
                str = dummy.Substring(j, 1)
                If str = Chr(34) Then bswitch = bswitch * -1
                If (bswitch < 0 Or (bswitch > 0 And str <> Chr(44))) And str <> Chr(34) Then strb.Append(str)
            Next
            PrintLine(fnum2, strb.ToString)
        Loop



        FileClose(fnum)
        FileClose(fnum2)



    End Sub
End Module
