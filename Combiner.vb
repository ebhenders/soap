Public Class Combiner
    Dim bOmitFirstCol As Boolean
    'Dim bAverage As Boolean
    'Dim bMinMax As Boolean
    'Dim bStdDev As Boolean
    'Dim bQuartiles As Boolean
    Dim bStatSwitches() As Boolean '1=average, 2=min/max, 3=stddev, 4=quartiles, 5=BoxPlotFormat)
    Dim bPrintDetailedData As Boolean
    Dim bSort As Boolean


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim openfiledialog2 As New OpenFileDialog
        Dim arr() As String
        Dim arr2() As String

        If System.IO.File.Exists(FileListFN) = False Then
            With openfiledialog2
                '.InitialDirectory = "d:\Analysis\"
                .Title = "Select a file with names of files to combine (aka: Batch File)"
                .Filter = "Comma-delimited files(.txt) |*.txt"
                If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
            End With

            FileListFN = openfiledialog2.FileName
        End If

        openfiledialog2 = Nothing

        lblBatchFile.Text = "Batch File: " & FName(FileListFN)

        Dim savefiledialog As New SaveFileDialog

        With savefiledialog
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Save the output file as"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            .FileName = "CombineOutput"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        OutputFile = savefiledialog.FileName
        bAddition = rbAddition.Checked
        bSummarize = rbAverageAndRange.Checked
        bConcatenate = rbConcatenate.Checked
        bSplitOutFiles = cbSplitFiles.Checked * cbSplitFiles.Enabled
        bOmitFirstCol = cbOmitFirstCol.Checked
        bSort = cbFirstColSort.Checked
        ReDim bStatSwitches(5)
        bStatSwitches(1) = cbAverage.Checked
        bStatSwitches(2) = cbMinMax.Checked
        bStatSwitches(3) = cbStdDev.Checked
        bStatSwitches(4) = cbQuartiles.Checked
        bStatSwitches(5) = cbBoxPlotFormat.Checked
        bPrintDetailedData = cbPrintDetailedData.Checked

        If rbAllCols.Checked = True Then
            NTimestep = CType(tbNNumberCols.Text, Integer)
            ReDim TimestepIndex(NTimestep)
            For jstep As Integer = 1 To NTimestep
                TimestepIndex(jstep) = jstep
            Next
        ElseIf rbSelectCols.Checked = True Then
            'timestep stuff
            NTimestep = 0
            'NTimestep = arr.Length
            ReDim TimestepIndex(NTimestep)
            TimestepIndex(0) = 0
            MaxTimestep = 0
            arr = Split(tbTimesteps.Text, ",")
            For jstep As Integer = 0 To arr.Length - 1
                arr2 = Split(arr(jstep), "-")
                For jstep2 As Integer = CType(Trim(arr2(0)), Integer) To CType(Trim(arr2(arr2.Length - 1)), Integer)
                    NTimestep = NTimestep + 1
                    ReDim Preserve TimestepIndex(NTimestep)
                    TimestepIndex(NTimestep) = jstep2 'CType(Trim(arr2(NTimestep - 1)), Integer)
                    MaxTimestep = Math.Max(MaxTimestep, jstep2)
                Next
            Next
        End If

        savefiledialog = Nothing
        Dim filecombine As New clProcessSimpplle
        filecombine.RunFileCombiner(cbUseRunIndex.Checked, bStatSwitches, bOmitFirstCol, CType(tbNTextCols.Text, Integer), TimestepIndex, bPrintDetailedData, bSort)
        filecombine = Nothing

    End Sub

    Private Sub rbAverageAndRange_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbAverageAndRange.CheckedChanged
        If rbAverageAndRange.Checked = False Then cbOmitFirstCol.Checked = False
        cbOmitFirstCol.Enabled = rbAverageAndRange.Checked
        cbAverage.Enabled = rbAverageAndRange.Checked
        cbMinMax.Enabled = rbAverageAndRange.Checked
        cbstddev.Enabled = rbAverageAndRange.Checked
        cbQuartiles.Enabled = rbAverageAndRange.Checked
        cbPrintDetailedData.Enabled = rbAverageAndRange.Checked
        cbBoxPlotFormat.Enabled = rbAverageAndRange.Checked

    End Sub

    Private Sub cbSplitFiles_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbSplitFiles.CheckedChanged

    End Sub

    Private Sub cbUseRunIndex_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbUseRunIndex.CheckedChanged
        cbSplitFiles.Enabled = cbUseRunIndex.Checked
    End Sub

    Private Sub cbBoxPlotFormat_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbBoxPlotFormat.CheckedChanged
        If cbBoxPlotFormat.Checked = True Then
            cbMinMax.Checked = True
            cbAverage.Checked = True
            cbQuartiles.Checked = True
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        MsgBox("Specify the column indices of the subset of numeric columns to use in the combine. The first numeric column is assumed to be 1. Otherwise, all numeric columns will be used in the combine")
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        MsgBox("If the first data column is the same in all files, you can eliminate it from influencing the statistics of the rest of columns")
    End Sub

    Private Sub rbConcatenate_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbConcatenate.CheckedChanged
        cbFirstColSort.Enabled = rbConcatenate.Checked
    End Sub


    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim patchstats As New Form
        patchstats = frmPatchStats
        patchstats.Show()
        patchstats = Nothing

     
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        MsgBox("The input file is a list of files to process. Alternately, the first character of each line can be a run index (numeric) followed by a comma ',' followed by the file name with path.")
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        MsgBox("If the first column of the input file is a run index, checking this box will produce a unique output file for each unique run index found.")
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        MsgBox("The batch file is a list of the Number Files to process. No header line, each line contains the full path to the file to process, with an optional first column stating the run index.")
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        MsgBox("Typically, there are one or more columns at the beginning of each row that are text in nature and should not be treated as numbers. How many such columns are there?")
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        MsgBox("If you are not sure what you are doing, clicking this will look at your batch file to suggest settings for the input file parameters.")
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Dim openfiledialog2 As New OpenFileDialog
        Dim arr() As String
        Dim arr2() As String
        Dim fnum As Integer
        Dim FirstFileName As String
        Dim TotalCols As Integer
        Dim NStringCols As Integer
        Dim NNumericCols As Integer
        Dim kcol As Integer

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select a file with names of files to combine (aka: Batch File)"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        FileListFN = openfiledialog2.FileName
        cbSplitFiles.Checked = False
        cbUseRunIndex.Checked = False
        fnum = FreeFile()
        FileOpen(fnum, FileListFN, OpenMode.Input)
        arr = Split(LineInput(fnum), ",")
        If arr.Length > 1 Then
            FirstFileName = arr(1)
            cbSplitFiles.Checked = True
            cbUseRunIndex.Checked = True
        Else
            FirstFileName = arr(0)
        End If
        FileClose(fnum)

        fnum = FreeFile()
        FileOpen(fnum, FirstFileName, OpenMode.Input)
        arr = Split(LineInput(fnum), ",")
        TotalCols = arr.Length
        Do Until EOF(fnum)
            arr = Split(LineInput(fnum), ",")
            kcol = 0
            For j As Integer = 1 To arr.Length
                If IsNumeric(arr(j - 1)) = False Then kcol = j
            Next
            If kcol < arr.Length Then Exit Do
        Loop
        FileClose(fnum)
        NStringCols = kcol
        NNumericCols = TotalCols - NStringCols
        tbNTextCols.Text = NStringCols
        tbNNumberCols.Text = NNumericCols

        lblBatchFile.Text = "Batch File: " & FName(FileListFN)

    End Sub

    Private Sub Combiner_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FileListFN = ""
    End Sub

    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        Dim msg As String = "Addition Combine is typically used to add together results from multiple geographic areas into a forest-wide result. " & Chr(13) & Chr(13) _
        & "Statistical Combine is done to ferret out metrics between multiple files that all represent the same geographic extent. " & Chr(13) & Chr(13) _
        & "Concatenate is typically used for putting together patch files from multiple geographic areas into a single file. If you choose the 'Sort' option, all patches for all GAs for Time 1 will be listed first, followed by Period 2, etc."
        MsgBox(msg)

    End Sub
End Class