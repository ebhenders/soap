<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSearchForThreshVals
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.tbNIters = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.tbGoalFile = New System.Windows.Forms.TextBox
        Me.GO = New System.Windows.Forms.Button
        Me.tbDataFile = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.btnBrowse = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(35, 119)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(158, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Specify the number of iterations:"
        '
        'tbNIters
        '
        Me.tbNIters.Location = New System.Drawing.Point(103, 135)
        Me.tbNIters.Name = "tbNIters"
        Me.tbNIters.Size = New System.Drawing.Size(40, 20)
        Me.tbNIters.TabIndex = 1
        Me.tbNIters.Text = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(35, 23)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Goal File:"
        '
        'tbGoalFile
        '
        Me.tbGoalFile.Enabled = False
        Me.tbGoalFile.Location = New System.Drawing.Point(36, 44)
        Me.tbGoalFile.Name = "tbGoalFile"
        Me.tbGoalFile.Size = New System.Drawing.Size(157, 20)
        Me.tbGoalFile.TabIndex = 3
        '
        'GO
        '
        Me.GO.Location = New System.Drawing.Point(94, 195)
        Me.GO.Name = "GO"
        Me.GO.Size = New System.Drawing.Size(65, 41)
        Me.GO.TabIndex = 4
        Me.GO.Text = "GO"
        Me.GO.UseVisualStyleBackColor = True
        '
        'tbDataFile
        '
        Me.tbDataFile.Enabled = False
        Me.tbDataFile.Location = New System.Drawing.Point(36, 87)
        Me.tbDataFile.Name = "tbDataFile"
        Me.tbDataFile.Size = New System.Drawing.Size(157, 20)
        Me.tbDataFile.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(35, 66)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Data File:"
        '
        'btnBrowse
        '
        Me.btnBrowse.Location = New System.Drawing.Point(216, 44)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(65, 21)
        Me.btnBrowse.TabIndex = 7
        Me.btnBrowse.Text = "browse"
        Me.btnBrowse.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(216, 86)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(65, 21)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "browse"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'frmSearchForThreshVals
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(317, 262)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnBrowse)
        Me.Controls.Add(Me.tbDataFile)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.GO)
        Me.Controls.Add(Me.tbGoalFile)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.tbNIters)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmSearchForThreshVals"
        Me.Text = "Threshold Search Controls"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tbNIters As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tbGoalFile As System.Windows.Forms.TextBox
    Friend WithEvents GO As System.Windows.Forms.Button
    Friend WithEvents tbDataFile As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
