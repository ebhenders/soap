Public Class clInputFileScrubber
    'renumbers .attributesall and .spatialrelate SLINK indices beginning at 1
    Dim SimpleSLINK() As Long

    Public Sub RunScrubber()
        Dim infile As String
        'IDENTIFY THE ORIGINAL ATTRIBUTESALL FILE TO UPDATE
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Attributesall file to update"
            '.Title = "Select a folder that has the "
            .Filter = "Attributesall files (.attributesall) |*.attributesall"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        infile = openfiledialog2.FileName
        openfiledialog2 = Nothing
        CountSLINKS(infile)

    End Sub
    Private Sub CountSLINKS(ByVal infile As String)
        'opens the original .attributesall file to count how many SLINKS there are in total
        Dim fnum As Integer
        Dim fnumout As Integer
        Dim fnumLUT As Integer
        Dim MaxIndex As Long
        Dim dummy As String
        Dim dumout As String
        Dim dumLUT As String
        Dim arr() As String
        Dim kslink As Long
        'Dim kkslink As Integer
        Dim ind1 As Long
        Dim ind2 As Long
        Dim UseXY(,) As Byte
        Dim MaxX As Integer
        Dim MaxY As Integer
        Dim tx As Integer
        Dim ty As Integer
        Dim UseSLINK() As Byte
       

        'load TimeZero - first count the SLINKs
        fnum = FreeFile()
        MaxIndex = 0
        MaxX = 0
        MaxY = 0
        FileOpen(fnum, infile, OpenMode.Input)
        dummy = LineInput(fnum)
        'NSLINKS = 0
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If Trim(dummy) = "END" Then Exit Do
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                If arr(0) > MaxIndex Then MaxIndex = arr(0)
                'NSLINKS = NSLINKS + 1
                MaxX = Math.Max(MaxX, CType(arr(1), Integer))
                MaxY = Math.Max(MaxY, CType(arr(2), Integer))
            End If
        Loop
        FileClose(fnum)

        ReDim SimpleSLINK(MaxIndex), UseSLINK(MaxIndex)
        ReDim UseXY(MaxX, MaxY)

        'then do the mapping
        fnum = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        fnumout = FreeFile()
        FileOpen(fnumout, DirName(infile) & FName(infile) & "_scrub.attributesall", OpenMode.Output)
        fnumLUT = FreeFile()
        FileOpen(fnumLUT, DirName(infile) & FName(infile) & "_scrubLUT.txt", OpenMode.Output)
        PrintLine(fnumLUT, "SLINK,Original_ID,StandID")
        dummy = LineInput(fnum)
        PrintLine(fnumout, dummy)
        kslink = 0
        'kkslink = 0
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If Trim(dummy) = "END" Then Exit Do
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                'kkslink = kkslink + 1
                kslink = kslink + 1
                tx = CType(arr(1), Integer)
                ty = CType(arr(2), Integer)
                If UseXY(tx, ty) = 0 Then
                    ind1 = arr(0)
                    SimpleSLINK(ind1) = kslink
                    dumout = SimpleSLINK(ind1)
                    For j As Integer = 1 To arr.Length - 1
                        dumout = dumout & "," & arr(j)
                    Next
                    PrintLine(fnumout, dumout)
                    dumLUT = kslink & "," & ind1 & "," & arr(3)
                    PrintLine(fnumLUT, dumLUT)
                    UseXY(tx, ty) = 1
                    UseSLINK(kslink) = 1
                End If

            End If
        Loop
        PrintLine(fnumout, "END")
        FileClose(fnum)
        FileClose(fnumout)
        FileClose(fnumLUT)

        'now do the spatialrelate
        fnum = FreeFile()
        FileOpen(fnum, DirName(infile) & FName(infile) & ".spatialrelate", OpenMode.Input)
        fnumout = FreeFile()
        FileOpen(fnumout, DirName(infile) & FName(infile) & "_scrub.spatialrelate", OpenMode.Output)
        dummy = LineInput(fnum)
        PrintLine(fnumout, dummy)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If Trim(dummy) = "END" Then Exit Do
            If dummy.Length > 0 Then
                arr = Split(dummy, ",")
                ind1 = arr(0)
                ind2 = arr(1)
                If ind1 <= MaxIndex And ind2 <= MaxIndex And UseSLINK(ind1) = 1 And UseSLINK(ind2) = 1 Then
                    If SimpleSLINK(ind2) <> 0 Then
                        dumout = SimpleSLINK(ind1) & "," & SimpleSLINK(ind2)
                        For j As Integer = 2 To arr.Length - 1
                            dumout = dumout & "," & arr(j)
                        Next
                        PrintLine(fnumout, dumout)
                    End If
                End If
            End If
        Loop
        PrintLine(fnumout, "END")
        FileClose(fnum)
        FileClose(fnumout)

    End Sub
End Class
