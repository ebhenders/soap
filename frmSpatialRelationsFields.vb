Public Class frmSpatialRelationsFields
    Dim ItemArray As New ArrayList
    Private Sub frmSpatialRelationsFields_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String
        Dim filename As String

        filename = SpatialRelations.tbPolygonTableFN.Text
        If System.IO.File.Exists(filename) = False Then
            Dim openfiledialog2 As New OpenFileDialog

            With openfiledialog2
                '.InitialDirectory = "d:\Analysis\"
                .Title = "Select exported DB with SIMPPLLE fields including x/y coordinates of polygon centroids"
                .Filter = "Comma-delimited files(.txt) |*.txt"
                If .ShowDialog = Windows.Forms.DialogResult.Cancel Then
                    Me.Close()
                    Return
                End If

            End With

            tbPolygonTableFN.Text = openfiledialog2.FileName
            'SpatialRelationsRawFN = tbPolygonTableFN.Text
            SpatialRelations.tbPolygonTableFN.Text = tbPolygonTableFN.Text
            openfiledialog2 = Nothing
        Else
            tbPolygonTableFN.Text = filename
        End If



        fnum = FreeFile()
        FileOpen(fnum, tbPolygonTableFN.Text, OpenMode.Input)
        dummy = LineInput(fnum)
        arr = Split(dummy, ",")
        FileClose(fnum)


        Dim SelectorFrm As New Selector
        NumItems = arr.Length + 1


        For j As Integer = 2 To NumItems
            ItemArray.Add(arr(j - 2))
        Next
        ItemArray.Sort()
        ItemArray.Insert(0, "Not Defined")

        ComboBox1.Items.AddRange(ItemArray.ToArray)
        ComboBox2.Items.AddRange(ItemArray.ToArray)
        ComboBox3.Items.AddRange(ItemArray.ToArray)
        ComboBox4.Items.AddRange(ItemArray.ToArray)
        ComboBox5.Items.AddRange(ItemArray.ToArray)
        ComboBox6.Items.AddRange(ItemArray.ToArray)
        ComboBox7.Items.AddRange(ItemArray.ToArray)
        ComboBox8.Items.AddRange(ItemArray.ToArray)
        ComboBox9.Items.AddRange(ItemArray.ToArray)
        ComboBox10.Items.AddRange(ItemArray.ToArray)
        ComboBox11.Items.AddRange(ItemArray.ToArray)
        ComboBox12.Items.AddRange(ItemArray.ToArray)
        ComboBox13.Items.AddRange(ItemArray.ToArray)
        ComboBox14.Items.AddRange(ItemArray.ToArray)
        ComboBox15.Items.AddRange(ItemArray.ToArray)
        ComboBox16.Items.AddRange(ItemArray.ToArray)
        ComboBox17.Items.AddRange(ItemArray.ToArray)
        ComboBox18.Items.AddRange(ItemArray.ToArray)
        ComboBox19.Items.AddRange(ItemArray.ToArray)
        ComboBox20.Items.AddRange(ItemArray.ToArray)
        ComboBox21.Items.AddRange(ItemArray.ToArray)



    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim fnum As Integer
        Dim outfilename As String = DirName(tbPolygonTableFN.Text) & "FieldLUT_" & FName(tbPolygonTableFN.Text) & ".txt"

        fnum = FreeFile()
        FileOpen(fnum, outfilename, OpenMode.Output)
        PrintLine(fnum, "Fields to use for attributesall and spatialrelate")
        PrintLine(fnum, "Ecological stratification," & ComboBox1.Text)
        PrintLine(fnum, "Area," & ComboBox2.Text)
        PrintLine(fnum, "StandID," & ComboBox3.Text)
        PrintLine(fnum, "Ownership," & ComboBox4.Text)
        PrintLine(fnum, "Roads," & ComboBox5.Text)
        PrintLine(fnum, "Elevation," & ComboBox6.Text)
        PrintLine(fnum, "Fire Management Zone," & ComboBox7.Text)
        PrintLine(fnum, "Special Area," & ComboBox8.Text)
        PrintLine(fnum, "Species," & ComboBox9.Text)
        PrintLine(fnum, "Size Class," & ComboBox10.Text)
        PrintLine(fnum, "Density (Canopy Cover %)," & ComboBox11.Text)
        PrintLine(fnum, "Past Process," & ComboBox12.Text)
        PrintLine(fnum, "Past Treatment," & ComboBox13.Text)
        PrintLine(fnum, "Time Since Past Process (Decades)," & ComboBox14.Text)
        PrintLine(fnum, "Time Since Past Treatment (Decades)," & ComboBox15.Text)
        PrintLine(fnum, "X Centroid," & ComboBox16.Text)
        PrintLine(fnum, "Y Centroid," & ComboBox17.Text)
        PrintLine(fnum, "Future Process (optional)," & ComboBox18.Text)
        PrintLine(fnum, "Future Treatment (optional)," & ComboBox19.Text)
        PrintLine(fnum, "Geog. Area (optional)," & ComboBox20.Text)
        PrintLine(fnum, "Predef. SLINK (optional)," & ComboBox21.Text)
        FileClose(fnum)

        'SpatialRelationsFieldFN = outfilename
        SpatialRelations.tbColumnDefFN.Text = outfilename
        Me.Close()

    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        Dim filetoopen As String
        Dim dummy As String
        Dim arr() As String
        Dim kcbb As Integer
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select the file with field names defined"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        filetoopen = openfiledialog2.FileName
        'SpatialRelationsFieldFN = filetoopen
        openfiledialog2 = Nothing

        Dim fnum As Integer
        fnum = FreeFile()
        FileOpen(fnum, filetoopen, OpenMode.Input)
        dummy = LineInput(fnum)
        kcbb = 0
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            arr = Split(dummy, ",")
            If arr.Length < 2 Then Exit Do
            kcbb = kcbb + 1
            If kcbb = 1 Then ComboBox1.Text = ItemArray(GetItem(Trim(arr(1)), ItemArray))
            If kcbb = 2 Then ComboBox2.Text = ItemArray(GetItem(Trim(arr(1)), ItemArray))
            If kcbb = 3 Then ComboBox3.Text = ItemArray(GetItem(Trim(arr(1)), ItemArray))
            If kcbb = 4 Then ComboBox4.Text = ItemArray(GetItem(Trim(arr(1)), ItemArray))
            If kcbb = 5 Then ComboBox5.Text = ItemArray(GetItem(Trim(arr(1)), ItemArray))
            If kcbb = 6 Then ComboBox6.Text = ItemArray(GetItem(Trim(arr(1)), ItemArray))
            If kcbb = 7 Then ComboBox7.Text = ItemArray(GetItem(Trim(arr(1)), ItemArray))
            If kcbb = 8 Then ComboBox8.Text = ItemArray(GetItem(Trim(arr(1)), ItemArray))
            If kcbb = 9 Then ComboBox9.Text = ItemArray(GetItem(Trim(arr(1)), ItemArray))
            If kcbb = 10 Then ComboBox10.Text = ItemArray(GetItem(Trim(arr(1)), ItemArray))
            If kcbb = 11 Then ComboBox11.Text = ItemArray(GetItem(Trim(arr(1)), ItemArray))
            If kcbb = 12 Then ComboBox12.Text = ItemArray(GetItem(Trim(arr(1)), ItemArray))
            If kcbb = 13 Then ComboBox13.Text = ItemArray(GetItem(Trim(arr(1)), ItemArray))
            If kcbb = 14 Then ComboBox14.Text = ItemArray(GetItem(Trim(arr(1)), ItemArray))
            If kcbb = 15 Then ComboBox15.Text = ItemArray(GetItem(Trim(arr(1)), ItemArray))
            If kcbb = 16 Then ComboBox16.Text = ItemArray(GetItem(Trim(arr(1)), ItemArray))
            If kcbb = 17 Then ComboBox17.Text = ItemArray(GetItem(Trim(arr(1)), ItemArray))
            If kcbb = 18 Then ComboBox18.Text = ItemArray(GetItem(Trim(arr(1)), ItemArray))
            If kcbb = 19 Then ComboBox19.Text = ItemArray(GetItem(Trim(arr(1)), ItemArray))
            If kcbb = 20 Then ComboBox20.Text = ItemArray(GetItem(Trim(arr(1)), ItemArray))
            If kcbb = 21 Then ComboBox21.Text = ItemArray(GetItem(Trim(arr(1)), ItemArray))

        Loop
        FileClose(fnum)


    End Sub
    Private Function GetItem(ByVal ItemMatch As String, ByVal Items As ArrayList) As Integer
        GetItem = 0 'default
        For j As Integer = 1 To Items.Count - 1
            If ItemMatch = Items(j) Then
                GetItem = j
                Exit Function
            End If
        Next

    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select exported DB with SIMPPLLE fields including x/y coordinates of polygon centroids"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        tbPolygonTableFN.Text = openfiledialog2.FileName
        'SpatialRelationsRawFN = tbPolygonTableFN.Text
        openfiledialog2 = Nothing
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim fnum As Integer
        Dim outfilename As String

        Dim savefiledialog As New SaveFileDialog
        'need to have a save file dialog
        outfilename = DirName(tbPolygonTableFN.Text) & "FieldLUT_" & FName(tbPolygonTableFN.Text) & ".txt"
        With savefiledialog
            .Title = "Save the Field LUT file as"
            .Filter = "TextFile(.txt) |*.txt"
            .FileName = outfilename
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        outfilename = savefiledialog.FileName
        savefiledialog = Nothing

        fnum = FreeFile()
        FileOpen(fnum, outfilename, OpenMode.Output)
        PrintLine(fnum, "Fields to use for attributesall and spatialrelate")
        PrintLine(fnum, "Ecological stratification," & ComboBox1.Text)
        PrintLine(fnum, "Area," & ComboBox2.Text)
        PrintLine(fnum, "StandID," & ComboBox3.Text)
        PrintLine(fnum, "Ownership," & ComboBox4.Text)
        PrintLine(fnum, "Roads," & ComboBox5.Text)
        PrintLine(fnum, "Elevation," & ComboBox6.Text)
        PrintLine(fnum, "Fire Management Zone," & ComboBox7.Text)
        PrintLine(fnum, "Special Area," & ComboBox8.Text)
        PrintLine(fnum, "Species," & ComboBox9.Text)
        PrintLine(fnum, "Size Class," & ComboBox10.Text)
        PrintLine(fnum, "Density (Canopy Cover %)," & ComboBox11.Text)
        PrintLine(fnum, "Past Process," & ComboBox12.Text)
        PrintLine(fnum, "Past Treatment," & ComboBox13.Text)
        PrintLine(fnum, "Time Since Past Process (Decades)," & ComboBox14.Text)
        PrintLine(fnum, "Time Since Past Treatment (Decades)," & ComboBox15.Text)
        PrintLine(fnum, "X Centroid," & ComboBox16.Text)
        PrintLine(fnum, "Y Centroid," & ComboBox17.Text)
        PrintLine(fnum, "Future Process (optional)," & ComboBox18.Text)
        PrintLine(fnum, "Future Treatment (optional)," & ComboBox19.Text)
        PrintLine(fnum, "Geog. Area (optional)," & ComboBox20.Text)
        PrintLine(fnum, "Predef. SLINK (optional)," & ComboBox21.Text)
        FileClose(fnum)

        'SpatialRelationsFieldFN = outfilename
        SpatialRelations.tbColumnDefFN.Text = outfilename
        Me.Close()
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged

    End Sub
End Class