Public Class clFireSpreadInputFile
    Dim a(,) As Double
    Dim b(,) As Double
    Dim c(,) As Double
    Dim d(,) As Double
    Dim g1 As Double
    Dim g2 As Double
    Dim GTime(,,) As Double 'total time it takes to travel from center of center cell to center of adjacent cell. Dimmed by NCols, NRows, 7
    Dim NCols As Integer
    Dim NRows As Integer
    Dim NCells As Integer
    Dim NoDataVal As Double
    Dim beta() As Double 'angle to center of adjacent cell
    Dim alpha(,) As Double 'max spread direction/angle
    Dim SLINK(,) As Integer
    Dim GDist() As Double 'distance between cell centers in the 8 directions
    Dim BigM As Integer = 999999

    Public Sub RunFireSpreadIF()
        ReDim beta(7)
        beta(0) = 0
        For jbeta As Integer = 1 To 7
            beta(jbeta) = beta(jbeta - 1) + 45
        Next
        LoadABCDalpha("aFile", "bFile", "cFile", "alphaFile")
        CalcSpreadTimes("OutputFile")

    End Sub
    Private Sub CalcSpreadTimes(ByVal OutFileName As String)
        Dim theta As Double
        Dim GTimeCell() As Double
        Dim AddRow() As Integer
        Dim AddCol() As Integer
        Dim dummy As String
        Dim fnum As Integer

        ReDim AddRow(7), AddCol(7)
        'north
        AddRow(0) = -1
        AddCol(0) = 0
        'ne
        AddRow(1) = -1
        AddCol(1) = 1
        'e
        AddRow(2) = 0
        AddCol(2) = 1
        'se
        AddRow(3) = 1
        AddCol(3) = 1
        's
        AddRow(4) = 1
        AddCol(4) = 0
        'sw
        AddRow(5) = 1
        AddCol(5) = -1
        'w
        AddRow(6) = 0
        AddCol(6) = -1
        'nw
        AddRow(7) = -1
        AddCol(7) = -1

        'calculates the spread times from each cell center to the edges of the adjacent cells
        ReDim GTime(NCols + 1, NRows + 1, 7)
        For jcol As Integer = 1 To NCols + 1
            For jrow As Integer = 1 To NRows + 1
                'default
                For jdir As Integer = 0 To 7
                    GTime(jcol, jrow, jdir) = BigM
                Next
                If alpha(jcol, jrow) <> NoDataVal And a(jcol, jrow) <> NoDataVal And b(jcol, jrow) <> NoDataVal And c(jcol, jrow) <> NoDataVal Then
                    For jdir As Integer = 0 To 7
                        theta = Return_theta(alpha(jcol, jrow), beta(jdir))
                        'now have to figure out value of g relative to the input ellipse. Make sure you adjust for slope

                        'finally, you have to figure out the value of g relative to the time it would take to spread to the point on the cell's boundary you want

                    Next
                End If
            Next
        Next

        'now go through each cell and add the time to spread to the edge to the time from edge to center of the adjacent cell
        fnum = FreeFile()
        FileOpen(fnum, OutFileName, OpenMode.Output)
        For jcol As Integer = 1 To NCols
            For jrow As Integer = 1 To NRows
                dummy = SLINK(jcol, jrow) & "," & jcol & "," & jrow & ","
                ReDim GTimeCell(7)
                For jdir As Integer = 0 To 7
                    'DO WE HAVE TO GENERALIZE TO A DISCRETE SET OF TIMES???
                    GTimeCell(jdir) = GTime(jcol, jrow, jdir) + GTime(jcol + AddCol(jdir), jrow + AddRow(jdir), jdir)
                    dummy = dummy & "," & GTimeCell(jdir)
                Next
                PrintLine(fnum, dummy)
            Next
        Next
        FileClose(fnum)

    End Sub

    Private Sub LoadABCDalpha(ByVal aFile As String, ByVal bFile As String, ByVal cFile As String, ByVal alphaFile As String)
        Dim CellSize As Double
        Dim dummy As String
        Dim arr() As String
        Dim fnum As Integer

        'LOAD A FILE    
        fnum = FreeFile()
        FileOpen(fnum, aFile, OpenMode.Input)
        dummy = LineInput(fnum) 'ncols
        arr = Split(dummy, " ")
        NCols = arr(1)
        dummy = LineInput(fnum) 'nrows
        arr = Split(dummy, " ")
        NRows = arr(1)
        NCells = NCols * NRows
        dummy = LineInput(fnum) 'xllcorner
        dummy = LineInput(fnum) 'yllcorner
        dummy = LineInput(fnum) 'cellsize
        arr = Split(dummy, " ")
        CellSize = arr(1)
        dummy = LineInput(fnum) 'nodataval
        arr = Split(dummy, " ")
        NoDataVal = arr(1)
        'arr = Split(dummy, " ")
        'NCols = arr(1)

        ReDim a(NCols, NRows), b(NCols, NRows), c(NCols, NRows), d(NCols, NRows), alpha(NCols, NRows), SLINK(NCols, NRows), GDist(7)

        For jrow As Integer = 1 To NRows
            dummy = LineInput(fnum)
            arr = Split(dummy, " ")
            For jcol As Integer = 1 To NCols
                a(jcol, jrow) = arr(jcol - 1)
            Next
        Next
        FileClose(fnum)

        'CALC GDIST
        GDist(0) = CellSize 'north
        GDist(2) = CellSize
        GDist(4) = CellSize
        GDist(6) = CellSize
        GDist(1) = Math.Sqrt(CellSize ^ 2 + CellSize ^ 2)
        GDist(3) = Math.Sqrt(CellSize ^ 2 + CellSize ^ 2)
        GDist(5) = Math.Sqrt(CellSize ^ 2 + CellSize ^ 2)
        GDist(7) = Math.Sqrt(CellSize ^ 2 + CellSize ^ 2)

        'LOAD B FILE    
        fnum = FreeFile()
        FileOpen(fnum, bFile, OpenMode.Input)
        dummy = LineInput(fnum) 'NCOLS
        dummy = LineInput(fnum) 'NROWS
        dummy = LineInput(fnum) 'xllcorner
        dummy = LineInput(fnum) 'yllcorner
        dummy = LineInput(fnum) 'cellsize
        dummy = LineInput(fnum) 'nodataval


        For jrow As Integer = 1 To NRows
            dummy = LineInput(fnum)
            arr = Split(dummy, " ")
            For jcol As Integer = 1 To NCols
                b(jcol, jrow) = arr(jcol - 1)
            Next
        Next
        FileClose(fnum)

        'LOAD c FILE    
        fnum = FreeFile()
        FileOpen(fnum, cFile, OpenMode.Input)
        dummy = LineInput(fnum) 'NCOLS
        dummy = LineInput(fnum) 'NROWS
        dummy = LineInput(fnum) 'xllcorner
        dummy = LineInput(fnum) 'yllcorner
        dummy = LineInput(fnum) 'cellsize
        dummy = LineInput(fnum) 'nodataval

        For jrow As Integer = 1 To NRows
            dummy = LineInput(fnum)
            arr = Split(dummy, " ")
            For jcol As Integer = 1 To NCols
                c(jcol, jrow) = arr(jcol - 1)
            Next
        Next jrow
        FileClose(fnum)

        'LOAD alpha FILE    
        fnum = FreeFile()
        FileOpen(fnum, alphaFile, OpenMode.Input)
        dummy = LineInput(fnum) 'NCOLS
        dummy = LineInput(fnum) 'NROWS
        dummy = LineInput(fnum) 'xllcorner
        dummy = LineInput(fnum) 'yllcorner
        dummy = LineInput(fnum) 'cellsize
        dummy = LineInput(fnum) 'nodataval


        For jrow As Integer = 1 To NRows
            dummy = LineInput(fnum)
            arr = Split(dummy, " ")
            For jcol As Integer = 1 To NCols
                alpha(jcol, jrow) = arr(jcol - 1)
            Next
        Next
        FileClose(fnum)

        'NOW, CALCULATE D - the hypotenuse of AC
        For jrow As Integer = 1 To NRows
            For jcol As Integer = 1 To NCols
                d(jcol, jrow) = NoDataVal
                If a(jcol, jrow) <> NoDataVal And b(jcol, jrow) <> NoDataVal Then d(jcol, jrow) = Math.Sqrt(a(jcol, jrow) ^ 2 + c(jcol, jrow) ^ 2)
            Next
        Next

    End Sub

    Private Function Return_theta(ByVal alpha As Double, ByVal beta As Double) As Double
        'alpha is direction of travel, beta is direction from base cell center to center of adjacent cell
        Return_theta = Math.Abs(alpha - beta)
        If Return_theta > 180 Then
            Return_theta = Math.Abs(alpha + 360 - beta)
        End If

    End Function

    Private Function Return_g(ByVal c As Double, ByVal d As Double, ByVal theta As Double) As Double
        'g is the distance from the origin focus to the edge of the ellipse
        Return_g = ((c ^ 2) - (d ^ 2)) / ((c * Math.Cos(theta)) - d)
    End Function

End Class
