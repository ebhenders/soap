Public Class clGeneralProcesses
    Dim NDisturbs As Integer
    Dim DisturbName() As String
    Dim SLINKDisturb() As String
    Dim NSlink As Integer
    Public Sub LockedDisturbs(ByVal infile As String, ByVal outfile As String)
        ReadLockDistInFile(infile)
        WriteLockDistOutFile(outfile)
    End Sub

    Private Sub ReadLockDistInFile(ByVal infile As String)
        Dim dummy As String
        Dim slinkfield As Integer
        Dim disturbfield As Integer
        Dim fnum As Integer
        Dim kslink As Integer
        Dim tdist As Integer
        Dim arr() As String

        fnum = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        'header
        dummy = LineInput(fnum)
        'find the fields
        arr = Split(dummy, ",")
        For j As Integer = 0 To arr.Length - 1
            If StripQuotes(arr(j)) = "SLINK" Then slinkfield = j
            If StripQuotes(arr(j)) = "PROCESS_LOCK" Then disturbfield = j
        Next
        'count the lines
        NSlink = 0
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            arr = Split(dummy, ",")
            kslink = Trim(arr(slinkfield))
            If kslink > NSlink Then NSlink = kslink
        Loop
        FileClose(fnum)

        'read again to populate the disturbance array
        ReDim SLINKDisturb(NSlink)
        fnum = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        'header
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do
            arr = Split(dummy, ",")
            kslink = Trim(arr(slinkfield))
            SLINKDisturb(kslink) = Trim(StripQuotes(arr(disturbfield)))
        Loop
        FileClose(fnum)

        'populate the unique disturbance names
        NDisturbs = 0
        ReDim DisturbName(NDisturbs)
        For jslink As Integer = 1 To NSlink
            tdist = GetStrIndex(DisturbName, SLINKDisturb(jslink))
            If tdist < 0 Then
                NDisturbs = NDisturbs + 1
                ReDim Preserve DisturbName(NDisturbs)
                DisturbName(NDisturbs) = SLINKDisturb(jslink)
            End If
        Next

    End Sub

    Private Sub WriteLockDistOutFile(ByVal outfile As String)
        Dim fnum As Integer

        fnum = FreeFile()
        FileOpen(fnum, outfile, OpenMode.Output)
        For jdist As Integer = 1 To NDisturbs
            If Trim(DisturbName(jdist)) <> "" Then
                PrintLine(fnum, "1," & DisturbName(jdist))
                For jslink As Integer = 1 To NSlink
                    If SLINKDisturb(jslink) = DisturbName(jdist) Then
                        PrintLine(fnum, jslink)
                    End If
                Next
            End If
        Next
        FileClose(fnum)

    End Sub
End Class
