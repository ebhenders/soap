Public Class frmSearchForThreshVals
    Dim goalfile As String
    Dim datafile As String

    Private Sub frmRemapIters_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select a goal file"
            .Filter = "Text files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        goalfile = openfiledialog2.FileName
        tbGoalFile.Text = FName(goalfile)

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select a data file"
            .Filter = "Text files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        datafile = openfiledialog2.FileName
        tbDataFile.Text = FName(datafile)

        openfiledialog2 = Nothing


    End Sub

    Private Sub GO_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GO.Click


        Dim threshedit As New clSearchForThreshVals
        threshedit.RunThreshVals(goalfile, datafile, tbNIters.Text)
        threshedit = Nothing
        MsgBox("Done")
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select a goal file"
            .Filter = "Text files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        goalfile = openfiledialog2.FileName
        tbGoalFile.Text = FName(goalfile)
        openfiledialog2 = Nothing

        openfiledialog2 = Nothing
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select a data file"
            .Filter = "Text files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        datafile = openfiledialog2.FileName
        tbDataFile.Text = FName(datafile)

        openfiledialog2 = Nothing
    End Sub
End Class