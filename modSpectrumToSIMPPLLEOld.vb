


Module modSpectrumToSIMPPLLEOld
    ''This is code that writes out separate lines for each unique MA/Special area combination. SIMPPLLE code since modified to include 
    ''  multiple defs on this line PLUS the opportunity to put min/max size classes on the treatments.
    'Public SpecToSIMModDefFolderOld As String
    'Public AAByGeoAreaFNOld As String
    'Public SpectrumOutputFNOld As String


    ''for tracking suitability
    'Dim NSuit As Integer
    'Dim NSpecialArea As Integer
    'Dim TrtSpecialAreaSuit(,,) As Integer '0/1, indexed by NTrt, trtdefs, n special areas
    'Dim TrtSpecialAreaCode(,) As String 'the actual code in the input file BY TREATMENT, DEFINITION
    'Dim TrtSuitSuit(,,) As Integer '0/1 indexed by NTrt, trtdefs, NSuit
    'Dim TrtSuitCode(,) As String 'actual code in the treatment input file BY TREATMENT, DEFINITION
    'Dim TrtSizeSuit(,) As Integer '0/1 indexed by NTrt, NSize
    'Dim SuitName() As String
    'Dim SpecialAreaName() As String

    ''need to know the following information
    'Dim SpecSpeciesLUTFN As String
    'Dim SpecSizeLUTFN As String
    'Dim GALUTFN As String
    'Dim TrtLUTFN As String
    'Dim TrtDefLUTFN As String
    'Dim RptLUTFN As String
    'Dim SpecialAreaLUTFN As String
    'Dim SuitLUTFN As String
    'Dim FieldNameLUTFN As String
    'Dim SizeXWFN As String 'crosswalk from Spectrum to SIMPPLLE 
    'Dim SpeciesXWFN As String 'crosswalk from Spectrum to SIMPPLLE


    'Dim SpecL5L6TrtAcres(,,) As Single 'by level 5, level 6, treatment  - total acres in each category - treatment defines suitable acres available for the treatment
    'Dim SpecL5L6SpecSuitAcres(,,,) As Single 'by level 5, level 6, special area, suited  - total acres in each category - treatment defines suitable acres available for the treatment
    'Dim SpecL5L6GATrtAcres(,,,,) As Single 'by l5, l6, GA, treatment - to portion out treatment amounts
    'Dim SpecL5L6TrtAcresTreated(,,,) As Single 'by l5, l6, treatment type,decade - the spectrum solution

    'Dim GAL5AcresTreated(,,,,) As Single 'by GA,L5,treatment type,trt def, tic

    'Dim SpecSpeciesName() As String 'by nl5 - store the names
    'Dim SpecSizeName() As String 'by NL6
    'Dim GAName() As String
    'Dim SpecTrtName() As String
    'Dim SIMPPLLETrtName() As String
    'Dim RptName() As String
    'Dim RptL5() As String
    'Dim RptL6() As String
    'Dim RptTrt() As String

    'Dim SIMPPLLESpeciesName() As String
    'Dim SIMPPLLESpectrumSpecies() As String 'the SPECTRUM species name for the SIMPPLLE species
    'Dim SIMPPLLESizeName() As String
    'Dim SIMPPLLESpectrumSize() As String 'the SPECTRUM size name for the SIMPPLLE size

    'Dim NSpecies As Integer 'number of l5 categories
    'Dim NSize As Integer 'number of L6 categories
    'Dim NGA As Integer 'number of geographic areas
    'Dim NTrt As Integer 'number of unique treatment types
    'Dim MaxTrtDefs As Integer
    'Dim NTrtDefs() As Integer 'number of definitions associated with each treatment
    'Public NTicOld As Integer 'number of timesteps to 
    'Dim NRpt As Integer 'number of reports to search through
    'Dim NSIMPPLLESpecies As Integer
    'Dim NSIMPPLLESize As Integer

    'Dim NTrtsPerGA() As Integer

    ''field names
    'Dim GAfieldName As String
    'Dim SpecialAreaFieldName As String
    'Dim SuitFieldName As String
    'Dim SpeciesfieldName As String
    'Dim SizefieldName As String
    'Dim AcreFieldName As String

    'Public Sub TranslateSpectrumToSIMPPLLEOld(ByVal outputfilenameBase As String, ByVal NTimePers As Integer)
    '    NTic = NTimePers
    '    LUTFileNames(SpecToSIMModDefFolder)

    '    LoadLUTs()

    '    LoadAAFile()
    '    LoadSpecResultsFile()
    '    PortionGATreatmentAcres()

    '    WriteSIMPPLLEtrtfiles(outputfilenameBase)

    '    MsgBox("Done")

    'End Sub

    'Private Sub WriteSIMPPLLEtrtfiles(ByVal outputfilenamebase As String)
    '    Dim fnum As Integer
    '    Dim ktic As Integer
    '    Dim str As String
    '    Dim substr As String
    '    Dim teststr As String
    '    Dim trtacres As Single
    '    Dim testacres2 As Single
    '    Dim TrtmtAcres(,,) As Double
    '    Dim sizind As Integer

    '    ReDim TrtmtAcres(NTrt, MaxTrtDefs, NTic)

    '    For jga As Integer = 1 To NGA
    '        fnum = FreeFile()
    '        FileOpen(fnum, outputfilenamebase & GAName(jga) & "Treatments.txt", OpenMode.Output)
    '        PrintLine(fnum, "Westside Region One")
    '        PrintLine(fnum, Trim(NTrtsPerGA(jga)))
    '        ktic = NTic + 1
    '        For jtic As Integer = 1 To NTic
    '            ktic = ktic - 1
    '            For jtrt As Integer = 1 To NTrt
    '                For jdef As Integer = 1 To NTrtDefs(jtrt)
    '                    For jl5 As Integer = 1 To NSpecies
    '                        str = ""
    '                        If GAL5AcresTreated(jga, jl5, jtrt, jdef, ktic) > 0 Then
    '                            trtacres = Math.Round(GAL5AcresTreated(jga, jl5, jtrt, jdef, ktic), 0)
    '                            testacres2 = trtacres Mod 5
    '                            trtacres = trtacres + 5 - testacres2
    '                            TrtmtAcres(jtrt, jdef, ktic) = TrtmtAcres(jtrt, jdef, ktic) + trtacres
    '                            str = ktic & "," & SIMPPLLETrtName(jtrt) & "," & trtacres & "00,"
    '                            'determine special areas to apply this to
    '                            substr = ""
    '                            If TrtSpecialAreaCode(jtrt, jdef) <> "?" Then
    '                                For jspecarea As Integer = 1 To NSpecialArea
    '                                    If TrtSpecialAreaSuit(jtrt, jdef, jspecarea) = 1 Then
    '                                        substr = substr & SpecialAreaName(jspecarea) & ":"
    '                                    End If
    '                                Next
    '                            End If
    '                            If substr.Length > 0 Then substr = substr.Substring(0, substr.Length - 1) 'lop off the last ";"
    '                            If substr.Length = 0 Then substr = "?"
    '                            str = str & substr & ","
    '                            'road status, timesteps before follow-up treatment, t/f for a follow up treatment, t/f for retreatment, N Timesteps before unit is available for retreatment
    '                            str = str & "?,0,false,false,5,NO-TREATMENT,"
    '                            'determine ownership to apply this to
    '                            substr = ""
    '                            If TrtSuitCode(jtrt, jdef) <> "?" Then
    '                                For jsuit As Integer = 1 To NSuit
    '                                    If TrtSuitSuit(jtrt, jdef, jsuit) = 1 Then
    '                                        substr = substr & SuitName(jsuit) & ":"
    '                                    End If
    '                                Next
    '                            End If
    '                            If substr.Length > 0 Then substr = substr.Substring(0, substr.Length - 1) 'lop off the last ";"
    '                            If substr.Length = 0 Then substr = "?"
    '                            str = str & substr
    '                            PrintLine(fnum, str)

    '                            'SECOND LINE:
    '                            'Hab Group, Species, size class, density, previous process
    '                            str = "?,"
    '                            'determine species to apply this to
    '                            substr = ""
    '                            teststr = SpecSpeciesName(jl5)
    '                            For jspec As Integer = 1 To NSIMPPLLESpecies
    '                                If SIMPPLLESpectrumSpecies(jspec) = teststr Then
    '                                    substr = substr & SIMPPLLESpeciesName(jspec) & ":"
    '                                End If
    '                            Next
    '                            If substr.Length > 0 Then substr = substr.Substring(0, substr.Length - 1) 'lop off the last ";"
    '                            If substr.Length = 0 Then substr = "?"
    '                            str = str & substr & ","

    '                            'determine size class to apply this to
    '                            substr = ""
    '                            For jsize As Integer = 1 To NSize
    '                                teststr = SpecSizeName(jsize)
    '                                If TrtSizeSuit(jtrt, jsize) = 1 Then 'defined for this treatment, size
    '                                    For jsimsize As Integer = 1 To NSIMPPLLESize
    '                                        If SIMPPLLESpectrumSize(jsimsize) = teststr Then
    '                                            substr = substr & SIMPPLLESizeName(jsimsize) & ":"
    '                                        End If
    '                                    Next
    '                                End If
    '                            Next jsize
    '                            If substr.Length > 0 Then substr = substr.Substring(0, substr.Length - 1) 'lop off the last ";"
    '                            If substr.Length = 0 Then substr = "?"
    '                            str = str & substr & ","

    '                            'default density, previous process:
    '                            str = str & "2:3:4,?"
    '                            PrintLine(fnum, str)

    '                            'THIRD LINE - DEFAULT NIL
    '                            PrintLine(fnum, "NIL")
    '                            'FOURTH LINE - DEFAULT NIL
    '                            PrintLine(fnum, "NIL")
    '                        End If
    '                    Next
    '                Next jdef
    '            Next
    '        Next
    '        FileClose(fnum)
    '    Next
    '    'print out debug file
    '    fnum = FreeFile()
    '    FileOpen(fnum, outputfilenamebase & "TreatmentsSummary.txt", OpenMode.Output)
    '    PrintLine(fnum, "Treatment,tic1,tic2,tic3,tic4,tic5")
    '    For jtrt As Integer = 1 To NTrt
    '        For jdef As Integer = 1 To NTrtDefs(jtrt)
    '            str = SpecTrtName(jtrt)
    '            For jtic As Integer = 1 To NTic
    '                str = str & "," & TrtmtAcres(jtrt, jdef, jtic)
    '            Next
    '            PrintLine(fnum, str)
    '        Next jdef
    '    Next
    '    FileClose(fnum)

    '    'print out other debug files
    '    Dim bprintline As Boolean
    '    Dim bprintline2 As Boolean
    '    Dim str2 As String
    '    fnum = FreeFile()
    '    FileOpen(fnum, outputfilenamebase & "AcresSummary.txt", OpenMode.Output)
    '    PrintLine(fnum, "Species,Size,GA,Treatment,GAAcres,TotalAcres,Treat1,Treat2,Treat3,Treat4,Treat5")
    '    For jl5 As Integer = 1 To NSpecies
    '        For jl6 As Integer = 1 To NSize
    '            For jtrt As Integer = 1 To NTrt
    '                For jdef As Integer = 1 To NTrtDefs(jtrt)
    '                    For jga As Integer = 1 To NGA
    '                        bprintline = False
    '                        bprintline2 = False
    '                        str = SpecSpeciesName(jl5) & "," & SpecSizeName(jl6) & "," & SpecTrtName(jtrt) & "," & GAName(jga) & "," & _
    '                               SpecL5L6GATrtAcres(jl5, jl6, jga, jtrt, jdef) & "," & SpecL5L6TrtAcres(jl5, jl6, jtrt)
    '                        str2 = SpecSpeciesName(jl5) & "," & SpecSizeName(jl6) & "," & SpecTrtName(jtrt) & ","
    '                        For jtic As Integer = 1 To NTic
    '                            trtacres = SpecL5L6GATrtAcres(jl5, jl6, jga, jtrt, jdef) / SpecL5L6TrtAcres(jl5, jl6, jtrt) * SpecL5L6TrtAcresTreated(jl5, jl6, jtrt, jtic)
    '                            If trtacres > 0 Then bprintline = True
    '                            If SpecL5L6TrtAcresTreated(jl5, jl6, jtrt, jtic) > 0 Then bprintline2 = True
    '                            str = str & "," & trtacres & "," & SpecL5L6TrtAcresTreated(jl5, jl6, jtrt, jtic)
    '                            str2 = str2 & "," & SpecL5L6TrtAcresTreated(jl5, jl6, jtrt, jtic)
    '                        Next
    '                        If bprintline = True Then PrintLine(fnum, str)
    '                        'If bprintline2 = True Then PrintLine(fnum, str2)

    '                    Next
    '                Next jdef
    '            Next
    '        Next
    '    Next
    '    FileClose(fnum)


    'End Sub
    'Private Sub PortionGATreatmentAcres()
    '    'figures out how many acres to treat by GA
    '    'collapses the L6 code and portions out acres based on how many l5/l6 acres are in the GA compared to the total.
    '    'Rounds to 5 acres, since SIMPPLLE is in 5-acre blocks
    '    Dim trtacres As Single

    '    ReDim GAL5AcresTreated(NGA, NSpecies, NTrt, MaxTrtDefs, NTic)
    '    ReDim NTrtsPerGA(NGA)

    '    For jl5 As Integer = 1 To NSpecies
    '        For jl6 As Integer = 1 To NSize
    '            For jtrt As Integer = 1 To NTrt
    '                For jdef As Integer = 1 To NTrtDefs(jtrt)
    '                    For jga As Integer = 1 To NGA
    '                        For jtic As Integer = 1 To NTic
    '                            trtacres = 0
    '                            If SpecL5L6TrtAcres(jl5, jl6, jtrt) > 0 Then
    '                                trtacres = SpecL5L6GATrtAcres(jl5, jl6, jga, jtrt, jdef) / SpecL5L6TrtAcres(jl5, jl6, jtrt) * SpecL5L6TrtAcresTreated(jl5, jl6, jtrt, jtic)
    '                            End If
    '                            GAL5AcresTreated(jga, jl5, jtrt, jdef, jtic) = GAL5AcresTreated(jga, jl5, jtrt, jdef, jtic) + trtacres
    '                        Next jtic
    '                    Next jga
    '                Next jdef
    '            Next jtrt
    '        Next jl6
    '    Next jl5

    '    'count the number of treatments per GA
    '    For jl5 As Integer = 1 To NSpecies
    '        For jtrt As Integer = 1 To NTrt
    '            For jdef As Integer = 1 To NTrtDefs(jtrt)
    '                For jga As Integer = 1 To NGA
    '                    For jtic As Integer = 1 To NTic
    '                        If GAL5AcresTreated(jga, jl5, jtrt, jdef, jtic) > 0 Then
    '                            NTrtsPerGA(jga) = NTrtsPerGA(jga) + 1
    '                        End If
    '                    Next jtic
    '                Next jga
    '            Next jdef
    '        Next jtrt
    '    Next jl5


    'End Sub

    'Private Sub LoadAAFile()
    '    'loads the AA definition files to get acreage information for the AAs - to portion out by L5/L6
    '    Dim l5field As Integer
    '    Dim l6field As Integer
    '    Dim GAfield As Integer
    '    Dim AcreField As Integer
    '    Dim SuitField As Integer
    '    Dim SpecialAreaField As Integer

    '    Dim SuitableForTrt As Integer '1/0 based on whether the Special Area and Suitability fields align to be suitable for the treatment

    '    Dim fnum As Integer = FreeFile()
    '    Dim dummy As String
    '    Dim dum2 As String
    '    Dim arr() As String
    '    Dim l5ind As Integer
    '    Dim l6ind As Integer
    '    Dim gaind As Integer
    '    Dim suitind As Integer
    '    Dim specareaind As Integer
    '    Dim SASuit As Integer
    '    Dim SSuit As Integer
    '    Dim SACode As String
    '    Dim SCode As String

    '    'LOAD AA FILE

    '    ReDim SpecL5L6TrtAcres(NSpecies, NSize, NTrt) 'by level 5, level 6 - total acres in each category - need to add suitabilty dimension?
    '    ReDim SpecL5L6GATrtAcres(NSpecies, NSize, NGA, NTrt, MaxTrtDefs)  'by l5, l6, GA - to portion out treatment amounts
    '    FileOpen(fnum, AAByGeoAreaFN, OpenMode.Input)
    '    dummy = LineInput(fnum) 'header line
    '    'find the field names
    '    arr = Split(dummy, ",")
    '    For j As Integer = 0 To arr.Length - 1
    '        dum2 = Trim(StripQuotes(arr(j)))
    '        If dum2 = GAfieldName Then GAfield = j
    '        If dum2 = SpecialAreaFieldName Then SpecialAreaField = j
    '        If dum2 = SuitFieldName Then SuitField = j
    '        If dum2 = SpeciesfieldName Then l5field = j
    '        If dum2 = SizefieldName Then l6field = j
    '        If dum2 = AcreFieldName Then AcreField = j

    '    Next
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            l5ind = GetIndex(SpecSpeciesName, Trim(StripQuotes(arr(l5field))))
    '            l6ind = GetIndex(SpecSizeName, Trim(StripQuotes(arr(l6field))))
    '            gaind = GetIndex(GAName, Trim(StripQuotes(arr(GAfield))))
    '            SCode = Trim(StripQuotes(arr(SuitField)))
    '            suitind = GetIndex(SuitName, SCode)
    '            SACode = Trim(StripQuotes(arr(SpecialAreaField)))
    '            specareaind = GetIndex(SpecialAreaName, SACode)

    '            If l5ind >= 0 And l6ind >= 0 And gaind >= 0 And suitind >= 0 And specareaind >= 0 Then 'poly is defined
    '                For jtrt As Integer = 1 To NTrt
    '                    For jdef As Integer = 1 To NTrtDefs(jtrt)
    '                        SASuit = TrtSpecialAreaSuit(jtrt, jdef, specareaind)
    '                        SSuit = TrtSuitSuit(jtrt, jdef, suitind) 'has to be defined as suitable in both
    '                        SuitableForTrt = SASuit * SSuit
    '                        SpecL5L6TrtAcres(l5ind, l6ind, jtrt) = SpecL5L6TrtAcres(l5ind, l6ind, jtrt) + arr(AcreField) * SuitableForTrt
    '                        SpecL5L6GATrtAcres(l5ind, l6ind, gaind, jtrt, jdef) = SpecL5L6GATrtAcres(l5ind, l6ind, gaind, jtrt, jdef) + arr(AcreField) * SuitableForTrt
    '                    Next
    '                Next
    '            End If
    '        End If
    '    Loop

    '    FileClose(fnum)

    'End Sub

    'Private Sub LoadSpecResultsFile()
    '    'loads the Spectrum results file 
    '    Dim TotalPers As Integer = 25

    '    Dim MaxIndex As Integer = 0
    '    Dim fnum As Integer = FreeFile()
    '    Dim dummy As String
    '    Dim arr() As String
    '    Dim rptdefind As Integer
    '    Dim l5ind As Integer
    '    Dim l6ind As Integer
    '    Dim trtind As Integer

    '    'LOAD SPEC COMADEL

    '    ReDim SpecL5L6TrtAcresTreated(NSpecies, NSize, NTrt, NTic) 'goal is to populate this array
    '    FileOpen(fnum, SpectrumOutputFN, OpenMode.Input)
    '    dummy = LineInput(fnum) 'header line
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            rptdefind = GetIndex(RptName, Trim(StripQuotes(arr(2))))
    '            l5ind = GetIndex(SpecSpeciesName, RptL5(rptdefind))
    '            l6ind = GetIndex(SpecSizeName, RptL6(rptdefind))
    '            trtind = GetIndex(SpecTrtName, RptTrt(rptdefind))
    '            For jtic As Integer = 1 To NTic
    '                SpecL5L6TrtAcresTreated(l5ind, l6ind, trtind, jtic) = CType(Trim(arr(7 + jtic)), Single)
    '                'If jtic = 1 And trtind = 4 And SpecL5L6TrtAcresTreated(l5ind, l6ind, trtind, jtic) > 0 Then Stop
    '            Next

    '        End If
    '        If TotalPers > 20 Then dummy = LineInput(fnum) 'sloppy code; assume NTic < 20, etc.
    '        dummy = LineInput(fnum) 'amt
    '        If TotalPers > 20 Then dummy = LineInput(fnum) 'sloppy code; assume NTic < 20, etc.
    '    Loop

    '    FileClose(fnum)



    'End Sub


    'Private Sub LoadLUTs()

    '    Dim MaxIndex As Integer = 0
    '    Dim fnum As Integer = FreeFile()
    '    Dim dummy As String
    '    Dim arr() As String
    '    Dim arr2() As String
    '    Dim kind As Integer
    '    Dim ktrt As Integer
    '    Dim kdef As Integer


    '    'LOAD GA LUT
    '    MaxIndex = 0
    '    ReDim GAName(MaxIndex)
    '    FileOpen(fnum, GALUTFN, OpenMode.Input)
    '    dummy = LineInput(fnum) 'header line
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            MaxIndex = MaxIndex + 1
    '            ReDim Preserve GAName(MaxIndex)
    '            GAName(MaxIndex) = CType(arr(0), String)
    '        End If
    '    Loop
    '    NGA = MaxIndex
    '    FileClose(fnum)

    '    'LOAD FIELD NAMES
    '    FileOpen(fnum, FieldNameLUTFN, OpenMode.Input)
    '    dummy = LineInput(fnum)
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            If arr(0) = "GAfieldName" Then GAfieldName = arr(1)
    '            If arr(0) = "SpecialAreaFieldName" Then SpecialAreaFieldName = arr(1)
    '            If arr(0) = "SuitFieldName" Then SuitFieldName = arr(1)
    '            If arr(0) = "SpeciesfieldName" Then SpeciesfieldName = arr(1)
    '            If arr(0) = "SizefieldName" Then SizefieldName = arr(1)
    '            If arr(0) = "AcreFieldName" Then AcreFieldName = arr(1)
    '        End If
    '    Loop
    '    FileClose(fnum)


    '    'LOAD SPEC SPECIES LUT
    '    MaxIndex = 0
    '    ReDim SpecSpeciesName(MaxIndex)
    '    FileOpen(fnum, SpecSpeciesLUTFN, OpenMode.Input)
    '    dummy = LineInput(fnum) 'header line
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            MaxIndex = MaxIndex + 1
    '            ReDim Preserve SpecSpeciesName(MaxIndex)
    '            SpecSpeciesName(MaxIndex) = CType(arr(0), String)
    '        End If
    '    Loop
    '    NSpecies = MaxIndex
    '    FileClose(fnum)

    '    'LOAD SPEC SIZE LUT
    '    MaxIndex = 0
    '    ReDim SpecSizeName(MaxIndex)
    '    FileOpen(fnum, SpecSizeLUTFN, OpenMode.Input)
    '    dummy = LineInput(fnum) 'header line
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            MaxIndex = MaxIndex + 1
    '            ReDim Preserve SpecSizeName(MaxIndex)
    '            SpecSizeName(MaxIndex) = CType(arr(0), String)
    '        End If
    '    Loop
    '    NSize = MaxIndex
    '    FileClose(fnum)

    '    'LOAD SPECIAL AREA LUT
    '    MaxIndex = 0
    '    ReDim SpecialAreaName(MaxIndex)
    '    FileOpen(fnum, SpecialAreaLUTFN, OpenMode.Input)
    '    dummy = LineInput(fnum) 'header line
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            MaxIndex = MaxIndex + 1
    '            ReDim Preserve SpecialAreaName(MaxIndex)
    '            SpecialAreaName(MaxIndex) = CType(arr(0), String)
    '        End If
    '    Loop
    '    NSpecialArea = MaxIndex
    '    FileClose(fnum)

    '    'LOAD SUITABILITY LUT
    '    MaxIndex = 0
    '    ReDim SuitName(MaxIndex)
    '    FileOpen(fnum, SuitLUTFN, OpenMode.Input)
    '    dummy = LineInput(fnum) 'header line
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            MaxIndex = MaxIndex + 1
    '            ReDim Preserve SuitName(MaxIndex)
    '            SuitName(MaxIndex) = CType(arr(0), String)
    '        End If
    '    Loop
    '    NSuit = MaxIndex
    '    FileClose(fnum)

    '    'LOAD SPEC trt LUT
    '    MaxIndex = 0
    '    ReDim SpecTrtName(MaxIndex), SIMPPLLETrtName(MaxIndex)
    '    FileOpen(fnum, TrtLUTFN, OpenMode.Input)
    '    dummy = LineInput(fnum) 'header line
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            MaxIndex = MaxIndex + 1
    '            ReDim Preserve SpecTrtName(MaxIndex), SIMPPLLETrtName(MaxIndex)
    '            SpecTrtName(MaxIndex) = CType(arr(1), String)
    '            SIMPPLLETrtName(MaxIndex) = CType(arr(2), String)
    '        End If
    '    Loop
    '    NTrt = MaxIndex
    '    FileClose(fnum)

    '    'Load the Treatment definition LUT
    '    ReDim NTrtDefs(NTrt)
    '    FileOpen(fnum, TrtDefLUTFN, OpenMode.Input)
    '    dummy = LineInput(fnum) 'header line
    '    ktrt = 0
    '    kdef = 0

    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            ktrt = arr(0) 'first line is the index of the treatment
    '            NTrtDefs(ktrt) = NTrtDefs(ktrt) + 1
    '            If NTrtDefs(ktrt) > MaxTrtDefs Then MaxTrtDefs = NTrtDefs(ktrt)
    '        End If
    '    Loop
    '    FileClose(fnum)

    '    'loop again to get the definitions
    '    ReDim TrtSpecialAreaSuit(NTrt, MaxTrtDefs, NSpecialArea), TrtSuitSuit(NTrt, MaxTrtDefs, NSuit), TrtSpecialAreaCode(NTrt, MaxTrtDefs), TrtSuitCode(NTrt, MaxTrtDefs)
    '    ReDim NTrtDefs(NTrt)
    '    FileOpen(fnum, TrtDefLUTFN, OpenMode.Input)
    '    dummy = LineInput(fnum) 'HEADER
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            ktrt = arr(0) 'first line is the index of the treatment
    '            NTrtDefs(ktrt) = NTrtDefs(ktrt) + 1
    '            kdef = NTrtDefs(ktrt)
    '            'load special area info.
    '            TrtSpecialAreaCode(ktrt, kdef) = Trim(arr(2))
    '            arr2 = Split(arr(2), ";")
    '            For j2 As Integer = 0 To arr2.Length - 1
    '                kind = GetIndex(SpecialAreaName, (arr2(j2)))
    '                TrtSpecialAreaSuit(ktrt, kdef, kind) = 1
    '                'if you have a ? then all special areas are suitable
    '                If arr2(j2) = "?" Then
    '                    For jsa As Integer = 0 To NSpecialArea
    '                        TrtSpecialAreaSuit(ktrt, kdef, jsa) = 1
    '                    Next
    '                End If
    '            Next
    '            'load suitability info.
    '            TrtSuitCode(ktrt, kdef) = Trim(arr(3))
    '            arr2 = Split(arr(3), ";")
    '            For j2 As Integer = 0 To arr2.Length - 1
    '                kind = GetIndex(SuitName, (arr2(j2)))
    '                TrtSuitSuit(ktrt, kdef, kind) = 1
    '                'if you have a ? then all special areas are suitable
    '                If arr2(j2) = "?" Then
    '                    For jsa As Integer = 0 To NSuit
    '                        TrtSuitSuit(ktrt, kdef, jsa) = 1
    '                    Next
    '                End If
    '            Next
    '        End If
    '    Loop
    '    FileClose(fnum)

    '    'read again to get the good stuff about treatments and such...
    '    ReDim TrtSizeSuit(NTrt, NSize)
    '    FileOpen(fnum, TrtLUTFN, OpenMode.Input)
    '    dummy = LineInput(fnum) 'header line
    '    ktrt = 0
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            ktrt = ktrt + 1
    '            arr = Split(dummy, ",")
    '            'load size info.
    '            arr2 = Split(arr(4), ";")
    '            For j2 As Integer = 0 To arr2.Length - 1
    '                kind = GetIndex(SpecSizeName, (arr2(j2)))
    '                TrtSizeSuit(ktrt, kind) = 1
    '            Next
    '        End If
    '    Loop
    '    FileClose(fnum)



    '    'LOAD SPEC rpt LUT
    '    MaxIndex = 0
    '    ReDim RptName(MaxIndex), RptL5(MaxIndex), RptL6(MaxIndex), RptTrt(MaxIndex)
    '    FileOpen(fnum, RptLUTFN, OpenMode.Input)
    '    dummy = LineInput(fnum) 'header line
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            MaxIndex = MaxIndex + 1
    '            ReDim Preserve RptName(MaxIndex), RptL5(MaxIndex), RptL6(MaxIndex), RptTrt(MaxIndex)
    '            RptName(MaxIndex) = CType(arr(0), String)
    '            RptL5(MaxIndex) = CType(arr(1), String)
    '            RptL6(MaxIndex) = CType(arr(2), String)
    '            RptTrt(MaxIndex) = CType(arr(3), String)
    '        End If
    '    Loop
    '    NRpt = MaxIndex
    '    FileClose(fnum)

    '    'load SpeciesXWFN
    '    NSIMPPLLESpecies = 0
    '    ReDim SIMPPLLESpeciesName(NSIMPPLLESpecies), SIMPPLLESpectrumSpecies(NSIMPPLLESpecies)

    '    FileOpen(fnum, SpeciesXWFN, OpenMode.Input)
    '    dummy = LineInput(fnum)
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            NSIMPPLLESpecies = NSIMPPLLESpecies + 1
    '            ReDim Preserve SIMPPLLESpeciesName(NSIMPPLLESpecies), SIMPPLLESpectrumSpecies(NSIMPPLLESpecies)
    '            SIMPPLLESpeciesName(NSIMPPLLESpecies) = arr(0)
    '            SIMPPLLESpectrumSpecies(NSIMPPLLESpecies) = arr(1)
    '        End If
    '    Loop
    '    FileClose(fnum)

    '    'load SizeXWFN
    '    NSIMPPLLESize = 0
    '    ReDim SIMPPLLESizeName(NSIMPPLLESize), SIMPPLLESpectrumSize(NSIMPPLLESize)

    '    FileOpen(fnum, SizeXWFN, OpenMode.Input)
    '    dummy = LineInput(fnum)
    '    Do Until EOF(fnum)
    '        dummy = LineInput(fnum)
    '        If dummy.Length > 0 Then
    '            arr = Split(dummy, ",")
    '            NSIMPPLLESize = NSIMPPLLESize + 1
    '            ReDim Preserve SIMPPLLESizeName(NSIMPPLLESize), SIMPPLLESpectrumSize(NSIMPPLLESize)
    '            SIMPPLLESizeName(NSIMPPLLESize) = arr(0)
    '            SIMPPLLESpectrumSize(NSIMPPLLESize) = arr(1)
    '        End If
    '    Loop
    '    FileClose(fnum)


    'End Sub

    'Private Sub LUTFileNames(ByVal basefolder As String)

    '    Dim letter As String
    '    letter = basefolder.Substring(basefolder.Length - 1, 1)
    '    If letter <> "\" Then basefolder = basefolder & "\"

    '    SpecSpeciesLUTFN = basefolder & "SPEC_SPECIES_LUT.txt"
    '    SpecSizeLUTFN = basefolder & "SPEC_SIZE_LUT.txt"
    '    GALUTFN = basefolder & "GA_LUT.txt"
    '    TrtLUTFN = basefolder & "SPEC_TRT_LUT.txt"
    '    RptLUTFN = basefolder & "SPEC_RPT_LUT.txt"
    '    SuitLUTFN = basefolder & "SUIT_LUT.txt"
    '    SpecialAreaLUTFN = basefolder & "SPECIAL_AREA_LUT.txt"
    '    FieldNameLUTFN = basefolder & "FIELD_NAMES.txt"
    '    SpeciesXWFN = basefolder & "SPECIES_XW.txt"
    '    SizeXWFN = basefolder & "SIZE_XW.txt"
    '    TrtDefLUTFN = basefolder & "TRT_SPECAREA_SUIT_DEFS.txt"


    'End Sub

End Module
