<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Combiner
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.tbNTextCols = New System.Windows.Forms.TextBox
        Me.tbNNumberCols = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.cbUseRunIndex = New System.Windows.Forms.CheckBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.rbAddition = New System.Windows.Forms.RadioButton
        Me.cbSplitFiles = New System.Windows.Forms.CheckBox
        Me.rbAllCols = New System.Windows.Forms.RadioButton
        Me.rbSelectCols = New System.Windows.Forms.RadioButton
        Me.tbTimesteps = New System.Windows.Forms.TextBox
        Me.Button2 = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.rbConcatenate = New System.Windows.Forms.RadioButton
        Me.rbAverageAndRange = New System.Windows.Forms.RadioButton
        Me.cbOmitFirstCol = New System.Windows.Forms.CheckBox
        Me.cbAverage = New System.Windows.Forms.CheckBox
        Me.cbMinMax = New System.Windows.Forms.CheckBox
        Me.cbStdDev = New System.Windows.Forms.CheckBox
        Me.cbQuartiles = New System.Windows.Forms.CheckBox
        Me.cbPrintDetailedData = New System.Windows.Forms.CheckBox
        Me.cbBoxPlotFormat = New System.Windows.Forms.CheckBox
        Me.Button3 = New System.Windows.Forms.Button
        Me.cbFirstColSort = New System.Windows.Forms.CheckBox
        Me.Button4 = New System.Windows.Forms.Button
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.Button7 = New System.Windows.Forms.Button
        Me.Button8 = New System.Windows.Forms.Button
        Me.Button9 = New System.Windows.Forms.Button
        Me.Button10 = New System.Windows.Forms.Button
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.lblBatchFile = New System.Windows.Forms.Label
        Me.Button11 = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(162, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(216, 37)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "File Combiner"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(306, 41)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "# Text Colums (first)"
        '
        'tbNTextCols
        '
        Me.tbNTextCols.Location = New System.Drawing.Point(470, 38)
        Me.tbNTextCols.Name = "tbNTextCols"
        Me.tbNTextCols.Size = New System.Drawing.Size(74, 20)
        Me.tbNTextCols.TabIndex = 3
        Me.tbNTextCols.Text = "1"
        '
        'tbNNumberCols
        '
        Me.tbNNumberCols.Location = New System.Drawing.Point(470, 62)
        Me.tbNNumberCols.Name = "tbNNumberCols"
        Me.tbNNumberCols.Size = New System.Drawing.Size(74, 20)
        Me.tbNNumberCols.TabIndex = 5
        Me.tbNNumberCols.Text = "5"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(306, 62)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(149, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "# Numeric Columns (after text)"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(241, 437)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(64, 46)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "GO"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'cbUseRunIndex
        '
        Me.cbUseRunIndex.AutoSize = True
        Me.cbUseRunIndex.Location = New System.Drawing.Point(87, 38)
        Me.cbUseRunIndex.Name = "cbUseRunIndex"
        Me.cbUseRunIndex.Size = New System.Drawing.Size(145, 17)
        Me.cbUseRunIndex.TabIndex = 7
        Me.cbUseRunIndex.Text = "First Column is Run Index"
        Me.cbUseRunIndex.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(84, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(161, 16)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Batch File Parameters"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(295, 15)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(176, 16)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Number File Parameters"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(2, 281)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(105, 16)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "CombineType"
        '
        'rbAddition
        '
        Me.rbAddition.AutoSize = True
        Me.rbAddition.Checked = True
        Me.rbAddition.Location = New System.Drawing.Point(9, 305)
        Me.rbAddition.Name = "rbAddition"
        Me.rbAddition.Size = New System.Drawing.Size(63, 17)
        Me.rbAddition.TabIndex = 11
        Me.rbAddition.TabStop = True
        Me.rbAddition.Text = "Addition"
        Me.rbAddition.UseVisualStyleBackColor = True
        '
        'cbSplitFiles
        '
        Me.cbSplitFiles.AutoSize = True
        Me.cbSplitFiles.Enabled = False
        Me.cbSplitFiles.Location = New System.Drawing.Point(87, 59)
        Me.cbSplitFiles.Name = "cbSplitFiles"
        Me.cbSplitFiles.Size = New System.Drawing.Size(170, 17)
        Me.cbSplitFiles.TabIndex = 14
        Me.cbSplitFiles.Text = "Separate Output Files Per Run"
        Me.cbSplitFiles.UseVisualStyleBackColor = True
        '
        'rbAllCols
        '
        Me.rbAllCols.AutoSize = True
        Me.rbAllCols.Checked = True
        Me.rbAllCols.Location = New System.Drawing.Point(5, 16)
        Me.rbAllCols.Name = "rbAllCols"
        Me.rbAllCols.Size = New System.Drawing.Size(59, 17)
        Me.rbAllCols.TabIndex = 21
        Me.rbAllCols.TabStop = True
        Me.rbAllCols.Text = "All Cols"
        Me.rbAllCols.UseVisualStyleBackColor = True
        '
        'rbSelectCols
        '
        Me.rbSelectCols.AutoSize = True
        Me.rbSelectCols.Location = New System.Drawing.Point(5, 39)
        Me.rbSelectCols.Name = "rbSelectCols"
        Me.rbSelectCols.Size = New System.Drawing.Size(81, 17)
        Me.rbSelectCols.TabIndex = 22
        Me.rbSelectCols.Text = "Select Cols:"
        Me.rbSelectCols.UseVisualStyleBackColor = True
        '
        'tbTimesteps
        '
        Me.tbTimesteps.Location = New System.Drawing.Point(83, 39)
        Me.tbTimesteps.Name = "tbTimesteps"
        Me.tbTimesteps.Size = New System.Drawing.Size(293, 20)
        Me.tbTimesteps.TabIndex = 23
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(83, 16)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(25, 21)
        Me.Button2.TabIndex = 24
        Me.Button2.Text = "?"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.tbTimesteps)
        Me.GroupBox1.Controls.Add(Me.rbSelectCols)
        Me.GroupBox1.Controls.Add(Me.rbAllCols)
        Me.GroupBox1.Location = New System.Drawing.Point(81, 192)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(409, 68)
        Me.GroupBox1.TabIndex = 26
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Columns to consider"
        '
        'rbConcatenate
        '
        Me.rbConcatenate.AutoSize = True
        Me.rbConcatenate.Location = New System.Drawing.Point(9, 337)
        Me.rbConcatenate.Name = "rbConcatenate"
        Me.rbConcatenate.Size = New System.Drawing.Size(86, 17)
        Me.rbConcatenate.TabIndex = 27
        Me.rbConcatenate.Text = "Concatenate"
        Me.rbConcatenate.UseVisualStyleBackColor = True
        '
        'rbAverageAndRange
        '
        Me.rbAverageAndRange.AutoSize = True
        Me.rbAverageAndRange.Location = New System.Drawing.Point(133, 305)
        Me.rbAverageAndRange.Name = "rbAverageAndRange"
        Me.rbAverageAndRange.Size = New System.Drawing.Size(67, 17)
        Me.rbAverageAndRange.TabIndex = 12
        Me.rbAverageAndRange.Text = "Statistics"
        Me.rbAverageAndRange.UseVisualStyleBackColor = True
        '
        'cbOmitFirstCol
        '
        Me.cbOmitFirstCol.AutoSize = True
        Me.cbOmitFirstCol.Enabled = False
        Me.cbOmitFirstCol.Location = New System.Drawing.Point(356, 350)
        Me.cbOmitFirstCol.Name = "cbOmitFirstCol"
        Me.cbOmitFirstCol.Size = New System.Drawing.Size(177, 17)
        Me.cbOmitFirstCol.TabIndex = 13
        Me.cbOmitFirstCol.Text = "Omit first data column from Stats"
        Me.cbOmitFirstCol.UseVisualStyleBackColor = True
        '
        'cbAverage
        '
        Me.cbAverage.AutoSize = True
        Me.cbAverage.Enabled = False
        Me.cbAverage.Location = New System.Drawing.Point(216, 327)
        Me.cbAverage.Name = "cbAverage"
        Me.cbAverage.Size = New System.Drawing.Size(66, 17)
        Me.cbAverage.TabIndex = 15
        Me.cbAverage.Text = "Average"
        Me.cbAverage.UseVisualStyleBackColor = True
        '
        'cbMinMax
        '
        Me.cbMinMax.AutoSize = True
        Me.cbMinMax.Enabled = False
        Me.cbMinMax.Location = New System.Drawing.Point(216, 351)
        Me.cbMinMax.Name = "cbMinMax"
        Me.cbMinMax.Size = New System.Drawing.Size(68, 17)
        Me.cbMinMax.TabIndex = 16
        Me.cbMinMax.Text = "Min/Max"
        Me.cbMinMax.UseVisualStyleBackColor = True
        '
        'cbStdDev
        '
        Me.cbStdDev.AutoSize = True
        Me.cbStdDev.Enabled = False
        Me.cbStdDev.Location = New System.Drawing.Point(216, 372)
        Me.cbStdDev.Name = "cbStdDev"
        Me.cbStdDev.Size = New System.Drawing.Size(117, 17)
        Me.cbStdDev.TabIndex = 17
        Me.cbStdDev.Text = "Standard Deviation"
        Me.cbStdDev.UseVisualStyleBackColor = True
        '
        'cbQuartiles
        '
        Me.cbQuartiles.AutoSize = True
        Me.cbQuartiles.Enabled = False
        Me.cbQuartiles.Location = New System.Drawing.Point(216, 395)
        Me.cbQuartiles.Name = "cbQuartiles"
        Me.cbQuartiles.Size = New System.Drawing.Size(67, 17)
        Me.cbQuartiles.TabIndex = 18
        Me.cbQuartiles.Text = "Quartiles"
        Me.cbQuartiles.UseVisualStyleBackColor = True
        '
        'cbPrintDetailedData
        '
        Me.cbPrintDetailedData.AutoSize = True
        Me.cbPrintDetailedData.Enabled = False
        Me.cbPrintDetailedData.Location = New System.Drawing.Point(356, 373)
        Me.cbPrintDetailedData.Name = "cbPrintDetailedData"
        Me.cbPrintDetailedData.Size = New System.Drawing.Size(115, 17)
        Me.cbPrintDetailedData.TabIndex = 19
        Me.cbPrintDetailedData.Text = "Print Detailed Data"
        Me.cbPrintDetailedData.UseVisualStyleBackColor = True
        '
        'cbBoxPlotFormat
        '
        Me.cbBoxPlotFormat.AutoSize = True
        Me.cbBoxPlotFormat.Enabled = False
        Me.cbBoxPlotFormat.Location = New System.Drawing.Point(356, 327)
        Me.cbBoxPlotFormat.Name = "cbBoxPlotFormat"
        Me.cbBoxPlotFormat.Size = New System.Drawing.Size(100, 17)
        Me.cbBoxPlotFormat.TabIndex = 20
        Me.cbBoxPlotFormat.Text = "Box Plot Format"
        Me.cbBoxPlotFormat.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(329, 347)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(25, 21)
        Me.Button3.TabIndex = 25
        Me.Button3.Text = "?"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'cbFirstColSort
        '
        Me.cbFirstColSort.AutoSize = True
        Me.cbFirstColSort.Enabled = False
        Me.cbFirstColSort.Location = New System.Drawing.Point(9, 360)
        Me.cbFirstColSort.Name = "cbFirstColSort"
        Me.cbFirstColSort.Size = New System.Drawing.Size(161, 17)
        Me.cbFirstColSort.TabIndex = 28
        Me.cbFirstColSort.Text = "Sort by First Col Val (number)"
        Me.cbFirstColSort.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(483, 12)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(64, 46)
        Me.Button4.TabIndex = 29
        Me.Button4.Text = "Patch Stats"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox1.Location = New System.Drawing.Point(206, 298)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(341, 133)
        Me.PictureBox1.TabIndex = 30
        Me.PictureBox1.TabStop = False
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(257, 37)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(22, 20)
        Me.Button5.TabIndex = 31
        Me.Button5.Text = "?"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(257, 59)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(22, 20)
        Me.Button6.TabIndex = 32
        Me.Button6.Text = "?"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(257, 14)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(22, 20)
        Me.Button7.TabIndex = 33
        Me.Button7.Text = "?"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(560, 38)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(22, 20)
        Me.Button8.TabIndex = 34
        Me.Button8.Text = "?"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(0, 19)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(78, 38)
        Me.Button9.TabIndex = 35
        Me.Button9.Text = "Load Batch File"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(0, 48)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(22, 20)
        Me.Button10.TabIndex = 36
        Me.Button10.Text = "?"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblBatchFile)
        Me.GroupBox2.Controls.Add(Me.Button10)
        Me.GroupBox2.Controls.Add(Me.Button9)
        Me.GroupBox2.Controls.Add(Me.Button8)
        Me.GroupBox2.Controls.Add(Me.Button7)
        Me.GroupBox2.Controls.Add(Me.Button6)
        Me.GroupBox2.Controls.Add(Me.Button5)
        Me.GroupBox2.Controls.Add(Me.cbSplitFiles)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.cbUseRunIndex)
        Me.GroupBox2.Controls.Add(Me.tbNNumberCols)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.tbNTextCols)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Location = New System.Drawing.Point(5, 64)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(600, 112)
        Me.GroupBox2.TabIndex = 37
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Input File Parameters"
        '
        'lblBatchFile
        '
        Me.lblBatchFile.AutoSize = True
        Me.lblBatchFile.Location = New System.Drawing.Point(7, 89)
        Me.lblBatchFile.Name = "lblBatchFile"
        Me.lblBatchFile.Size = New System.Drawing.Size(62, 13)
        Me.lblBatchFile.TabIndex = 37
        Me.lblBatchFile.Text = "<batch file>"
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(113, 279)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(25, 21)
        Me.Button11.TabIndex = 38
        Me.Button11.Text = "?"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Combiner
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(611, 495)
        Me.Controls.Add(Me.Button11)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.cbBoxPlotFormat)
        Me.Controls.Add(Me.cbFirstColSort)
        Me.Controls.Add(Me.cbPrintDetailedData)
        Me.Controls.Add(Me.rbConcatenate)
        Me.Controls.Add(Me.cbQuartiles)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cbStdDev)
        Me.Controls.Add(Me.cbMinMax)
        Me.Controls.Add(Me.cbAverage)
        Me.Controls.Add(Me.rbAddition)
        Me.Controls.Add(Me.cbOmitFirstCol)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.rbAverageAndRange)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Name = "Combiner"
        Me.Text = "Combiner"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tbNTextCols As System.Windows.Forms.TextBox
    Friend WithEvents tbNNumberCols As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents cbUseRunIndex As System.Windows.Forms.CheckBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents rbAddition As System.Windows.Forms.RadioButton
    Friend WithEvents cbSplitFiles As System.Windows.Forms.CheckBox
    Friend WithEvents rbAllCols As System.Windows.Forms.RadioButton
    Friend WithEvents rbSelectCols As System.Windows.Forms.RadioButton
    Friend WithEvents tbTimesteps As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rbConcatenate As System.Windows.Forms.RadioButton
    Friend WithEvents rbAverageAndRange As System.Windows.Forms.RadioButton
    Friend WithEvents cbOmitFirstCol As System.Windows.Forms.CheckBox
    Friend WithEvents cbAverage As System.Windows.Forms.CheckBox
    Friend WithEvents cbMinMax As System.Windows.Forms.CheckBox
    Friend WithEvents cbStdDev As System.Windows.Forms.CheckBox
    Friend WithEvents cbQuartiles As System.Windows.Forms.CheckBox
    Friend WithEvents cbPrintDetailedData As System.Windows.Forms.CheckBox
    Friend WithEvents cbBoxPlotFormat As System.Windows.Forms.CheckBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents cbFirstColSort As System.Windows.Forms.CheckBox
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents lblBatchFile As System.Windows.Forms.Label
End Class
