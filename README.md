# SOAP #

The most current version of SOAP is maintained as a .exe and is available for download in the download section.

### Why use SOAP? ###

* SOAP (SIMPPLLE Outputs and Analytics Processor) is designed to handle many of the functions associated with both building input files, creating linkages between other models such as Spectrum, and querying results directly from the raw SIMPPLLE output files.
* Last updated October, 2016
*

### How do I get set up? ###

* SOAP may only be functional on Windows machines, but could have limited utility on other operating systems as well.
* The file should run directly by opening from its downloaded location
* There are no associated dependencies, licenses, or permissions that need to be created in order to run SOAP. It is primarily text-file driven

### Contribution guidelines ###

* No contribution guidelines have been set up at this time.

### Who do I talk to? ###

* Contact:
* Eric Henderson ehenderson@fs.fed.us