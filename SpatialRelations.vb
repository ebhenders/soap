Imports System.Text
Public Class SpatialRelations
#Region "Declarations"

    Dim bError As Boolean
    Dim ColumnDefFN As String
    Dim PolygonTableFN As String
    Dim WindDirectionFileFN As String
    Dim WindSpeedFileFN As String
    Dim MinExtremeWindSpeed As Integer
    Dim DefaultWindDirection As String

    'Dim SLINK As Integer
    Dim Row As Integer
    Dim Col As Integer
    Dim Stand_ID As String
    Dim Acres As Single
    Dim CellSizeMeters As Single
    Dim HTG As String
    Dim D1 As String = "#"
    Dim D2 As Integer = 1
    Dim Covertype As String
    Dim SizeClass As String
    Dim Density As String
    Dim PastProcess As String
    Dim PastProcess_T As String
    Dim PastTreatment As String
    Dim PastTreatment_T As String
    Dim D3 As String = "?"
    Dim D4 As String = "#"
    Dim Ownership As String
    Dim D5 As String = "?"
    Dim D6 As String = "?"
    Dim FMZ As String
    Dim SpecialArea As String
    Dim D7 As String = "?"
    Dim D8 As String = "?"
    Dim D9 As String = "?"

    Dim NSLINK As Integer
    Dim WindDir() As String 'describes "D" or "N" based on wind direction for the polygon
    Dim ThreeWindDir() As String 'upwind, next or downwind
    Dim WindAdj() As Byte ' this will flag whether to print the adjacency or not based on wind or can be used as a general adjacency filter
    Dim PolyWindDir() As Integer 'wind direction of the polygon in degrees - describes the direction wind is coming from (e.g. S=180, W = 270)
    Dim PolyWindSpeed() As Integer 'wind speed at the polygon
    Dim PolySLINK() As Long 'slink of the polygon - can be flexible

    Dim Ecological_stratification_Field As Integer
    Dim ACRES_Field As Integer
    Dim STAND_ID_Field As Integer
    Dim Ownership_Field As Integer
    Dim Roads_Field As Integer
    Dim Elevation_Field As Integer
    Dim Fire_Management_Zone_Field As Integer
    Dim Special_Area_Field As Integer
    Dim Species_Field As Integer
    Dim Size_Class_Field As Integer
    Dim Density_Field As Integer
    Dim Past_Process_Field As Integer
    Dim Past_Treatment_Field As Integer
    Dim Time_Since_Past_Process_Field As Integer
    Dim Time_Since_Past_Treatment_Field As Integer
    Dim X_Centroid_Field As Integer
    Dim Y_Centroid_Field As Integer
    Dim Fut_Process_Field As Integer
    Dim Fut_Treatment_Field As Integer
    Dim SLINK_Field As Integer

    Dim bWriteLockFiles As Boolean 'if you find the future process field, it will default to writing these files.

    'for writing different files per GA
    Dim NGA As Integer
    Dim GANames() As String
    Dim bSplitGAs As Boolean
    Dim GAFieldName As String
    Dim GA_Field As Integer

    Dim GenFactor As Integer

    Dim NRows As Integer
    Dim NCols As Integer
    Dim SLINKRow() As Integer
    Dim SLINKCol() As Integer
    Dim SLINKYCoord() As Double
    Dim SLINKXCoord() As Double
    Dim ColRowSLINK(,) As Integer ' col, row - returns the SLINK
    Dim RowMinSLINK() As Integer 'slink at the beginning of a row
    Dim RowMaxSLINK() As Integer 'slink at the end of a row
    Dim ColMinSLINK() As Integer 'slink at the beginning of a column
    Dim ColMaxSLINK() As Integer 'slink at the end of a column

    Dim UseColRow(,) As Byte
    Dim Elevation() As Long

    'wind stuff
    Dim wRowYVal() As Double
    Dim wColXVal() As Double

    Dim wColRowWindSpeed(,) As Single

    'random vegetation stuff
    Dim foldername As String
    Dim NHTGLUTq As Integer
    Dim HTGNameLUTq() As String
    Dim NSizeClassLUTq As Integer
    Dim SizeClassNameLUTq() As String
    Dim NDensityLUTq As Integer
    Dim DensityNameLUTq() As String
    'Dim NHabGroupLUTq As Integer
    'Dim HabGroupNameLUTq() As String
    Dim NSpeciesLUTq As Integer
    Dim SpeciesNameLUTq() As String
    Dim UseCombo(,,,) As Byte

    Dim NHTGpathways As Integer
    Dim NPathwaysInHTG() As Integer

    Dim PathwayString(,) As String 'by nhtgpathways, pathway index in the htg




#End Region
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        MsgBox("If you are using the optional wind speed file AND SIMPPLLE SPREAD, only values greater than this will be considered Extreme Spread. Values less than this will be given a 'N' code, meaning they will not be considered downwind during extreme events. Value is relative to the values in the wind speed input file (not necessarily MPH).")
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        MsgBox("If no wind direction file is specified, or a polygon is missed in that file, the default wind direction will be used to determine downwind for extreme spread")
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim dummy As String
        Dim arr() As String
        Dim savefiledialog As New SaveFileDialog
        Dim pOutputFile As String
        Dim bRandomVeg As Boolean
        Dim bHexAdj As Boolean
        Dim b4WayAdj As Boolean
        Dim bWriteLockFiles As Boolean 'if you find the future process field, it will default to writing these files.
        Dim bWriteFTrtFiles As Boolean 'whether to write out lock-in treatment files
        Dim bWiredSLINK As Boolean
        Dim DefWindSpeed As Integer = CType(tbWindSpeed.Text, Integer)
        Dim DefWindDir As Integer = CType(tbWindDir.Text, Integer)
        DefWindDir = Math.Max(DefWindDir, 1)
        DefWindDir = Math.Min(DefWindDir, 360)
        'Dim bLimitBackburn As Boolean
        Dim bReadPathways As Boolean = bRandomVeg

        'for using one file to write multiple input files
        bSplitGAs = cbSplitGAs.Checked
        bRandomVeg = cbRandomVeg.Checked
        bHexAdj = rbHexAdj.Checked
        b4WayAdj = rb4WayAdj.Checked
        'bLimitBackburn = rbLimitBackBurnAdj.Checked

        GenFactor = 1
        If rb4xGeneralize.Checked Then GenFactor = 4
        If rb9xGeneralize.Checked Then GenFactor = 9

        'Write the spatialrelate file
        bError = False
        'need to have a save file dialog

        With savefiledialog
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Save the output file as"
            .Filter = "TextFile() |*"
            .FileName = "outputfile"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        OutputFile = savefiledialog.FileName
        pOutputFile = OutputFile 'default
        savefiledialog = Nothing

        'otherwise read values of the input fields
        ColumnDefFN = tbColumnDefFN.Text
        PolygonTableFN = tbPolygonTableFN.Text
        WindDirectionFileFN = tbWindDirectionFN.Text
        WindSpeedFileFN = tbWindSpeedFN.Text
        MinExtremeWindSpeed = tbMinExtremeWindSpeed.Text 'this is only relevant with SIMPPLLE Spread logic
        If rbKeaneFireSpread.Checked = True Then
            MinExtremeWindSpeed = 0
            tbMinExtremeWindSpeed.Text = 0
        End If
        DefaultWindDirection = cbbDefaultWindDirection.Text
        If System.IO.File.Exists(PolygonTableFN) = False Then
            MsgBox("Please choose the polygon table with attribute information")
            Exit Sub
        End If
        If System.IO.File.Exists(ColumnDefFN) = False Then
            MsgBox("Please choose or make the column definition file")
            Exit Sub
        End If
        'set the field names
        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, PolygonTableFN, OpenMode.Input)
        dummy = LineInput(fnum) 'string of field names
        ReadFieldNames(ColumnDefFN, dummy) 'sets integer values for fields to read in...
        bWriteLockFiles = False
        bWriteFTrtFiles = False
        bWiredSLINK = False
        If Fut_Process_Field > -1 Then bWriteLockFiles = True
        If Fut_Treatment_Field > -1 Then bWriteFTrtFiles = True
        If SLINK_Field > -1 Then bWiredSLINK = True
        If bError = True Then
            FileClose(fnum)
            Exit Sub
        End If
        FileClose(fnum)
        If bSplitGAs Then
            NGA = 0
            ReDim GANames(NGA)
            arr = Split(dummy, ",")
            NumItems = arr.Length
            ReDim ItemName(NumItems)
            For j As Integer = 1 To NumItems
                ItemName(j) = arr(j - 1)
            Next

            If GA_Field < 0 Then
                SelectorText = "Choose the column with Geographic Area Names"
                Dim sel As New Selector
                sel.ShowDialog()
                sel = Nothing
                GAFieldName = ItemsToRead(1)
                tbGAField.Text = GAFieldName
            End If

            GAFieldName = tbGAField.Text
            GA_Field = StringInt(GAFieldName, dummy)
            GetUniqueGAs(PolygonTableFN, GAFieldName, GA_Field, GANames)

        Else
            NGA = 1
            ReDim GANames(NGA)
            GA_Field = 0
            GAFieldName = "all"
            GANames(1) = "all"
            tbGAField.Text = "all"
        End If

        For jga As Integer = 1 To NGA
            If bSplitGAs = True Then
                pOutputFile = DirName(OutputFile) & GANames(jga) & "_" & FName(OutputFile)
            End If
            If jga > 1 Then bReadPathways = False
            If bError = False Then WriteAttributesAll(PolygonTableFN, pOutputFile & ".attributesall", GANames(jga), bRandomVeg, bReadPathways, bWriteLockFiles, bWriteFTrtFiles, bWiredSLINK)
            If bError = False Then SetDefaultWindDirSpeed(DefWindSpeed, DefWindDir)
            If bError = False Then ProcessWindDir()
            If bError = False Then ProcessWindSpeed()
            If bError = False And rbDefaultFireSpread.Checked = True Then WriteSpatialRelateDefault(pOutputFile & ".spatialrelate", pOutputFile & ".coord", bHexAdj, b4WayAdj, False) 'rbSlopeWindAdj.Checked)
            If bError = False And rbKeaneFireSpread.Checked = True Then WriteSpatialRelateKeane(pOutputFile & ".spatialrelate", pOutputFile & ".coord", cbTorus.Checked) ', bHexAdj, b4WayAdj, rbSlopeWindAdj.Checked)

            tbSpatialrelateFN.Text = pOutputFile & ".spatialrelate"
        Next jga

        If bError = False Then MsgBox("files written successfully")
        If bError = True Then MsgBox("files NOT written successfully")

    End Sub
    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        'update the spatialrelate file
        bError = False
        Dim ks As String

        WindDirectionFileFN = tbWindDirectionFN.Text
        WindSpeedFileFN = tbWindSpeedFN.Text
        MinExtremeWindSpeed = tbMinExtremeWindSpeed.Text

        If bError = False Then ReadCoordFile(tbCoordinatesFN.Text, ks)
        'If bError = False Then SetDefaultWindDir()
        If bError = False Then ProcessWindDir()
        If bError = False Then ProcessWindSpeed()
        If bError = False And ks = "SIMPPLLE" Then WriteSpatialRelateDefault(tbSpatialrelateFN.Text, tbCoordinatesFN.Text, rbHexAdj.Checked, rb4WayAdj.Checked, False) 'rbSlopeWindAdj.Checked)
        If bError = False And ks = "KEANE" Then WriteSpatialRelateKeane(tbSpatialrelateFN.Text, tbCoordinatesFN.Text, cbTorus.Checked) ', rbHexAdj.Checked, rb4WayAdj.Checked, rbSlopeWindAdj.Checked)

        If bError = False Then MsgBox("files written successfully")
        If bError = True Then MsgBox("files NOT written successfully")

    End Sub

    Private Sub WriteSpatialRelateDefault(ByVal outfile As String, ByVal coordfn As String, ByVal bHex As Boolean, ByVal b4Way As Boolean, ByVal bSlopeWindAdj As Boolean)
        'default original spatialrelate 
        On Error GoTo ErrorHandler
        Dim tslink() As Integer 'queue of slinks in the direction of spread
        Dim Ntslink As Integer 'number of slinks in the direction of spread
        Dim pdummy As String
        Dim even As Byte '0=odd, 1 = even
        Dim k As Single
        Dim pluscol As Integer
        Dim plusrow As Integer
        Dim badjprinted As Boolean 'flags whether an adjacent polygon has been printed or not
        Dim DegAz As Integer
        Dim SlopeWindProb As Single
        Dim randTest As Single
        Dim BaseSpreadProb As Single = 0.3
        Dim UMod As Single = 0 'upwind probability enhancement
        Dim NMod As Single = 0.1 'next to probability enhancement
        Dim DMod As Single = 0.3 'downwind probability ehnancement
        Dim SlopeWeight As Single = 0.5 'how much weight slope has in determining probability
        Dim MaxProb As Single = 0.95 'never get 100% probability
        Dim MinProb As Single = 0.15 'never get 0% spread probability
        Dim windf As Double
        Dim slopef As Double
        Dim spix As Double
        Dim spartspix As String
        Dim partspix As Double
        Dim arrspix() As String
        Dim Slope As Single
        Dim diagfact As Double
        Dim tdir As Integer


        Dim fnum3 As Integer = FreeFile()
        FileOpen(fnum3, DirName(coordfn) & FName(coordfn) & "_U.coord", OpenMode.Output)
        tbCoordinatesFN.Text = DirName(coordfn) & FName(coordfn) & "_U.coord"
        PrintLine(fnum3, "NSLINK," & NSLINK & ",SIMPPLLE")
        PrintLine(fnum3, "SLINK,X_CENTROID,Y_CENTROID,Column,Row,Elevation,PolyWindDir,PolyWindSpeed")

        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, outfile, OpenMode.Output)
        PrintLine(fnum, "BEGIN VEGETATION-VEGETATION")
        If bHex = False Then
            For jslink As Integer = 1 To NSLINK
                badjprinted = False
                tdir = PolyWindDir(jslink) 'default
                If PolyWindSpeed(jslink) <= MinExtremeWindSpeed Then tdir = 999 'will cause all polys to be "next" - no downwind
                DetermineDownwind(tdir)
                'For jdir As Integer = 1 To 8
                For jtest As Integer = 1 To 2 'once with potential fire spread filter, once without
                    If jtest = 2 Then 'don't filter for wind adjacency the second time through
                        ReDim WindAdj(8)
                        For jdir As Integer = 1 To 8
                            WindAdj(jdir) = 1
                        Next
                    End If
                    '4-way filter
                    If jtest = 1 And b4Way = True Then 'don't print the corners. Use WindAdj to filter this
                        WindAdj(2) = 0
                        WindAdj(4) = 0
                        WindAdj(6) = 0
                        WindAdj(8) = 0
                    End If
                    For jdir As Integer = 1 To 8
                        diagfact = 1 'default
                        If jdir = 1 Then 'poly to the north
                            pluscol = 0
                            plusrow = 1
                            DegAz = 180 '0
                        ElseIf jdir = 2 Then 'northeast
                            pluscol = 1
                            plusrow = 1
                            DegAz = 225 '45
                            diagfact = 1 / Math.Sqrt(2)
                        ElseIf jdir = 3 Then 'east
                            pluscol = 1
                            plusrow = 0
                            DegAz = 270 '90
                        ElseIf jdir = 4 Then 'southeast
                            pluscol = 1
                            plusrow = -1
                            DegAz = 315 '135
                            diagfact = 1 / Math.Sqrt(2)
                        ElseIf jdir = 5 Then 'south
                            pluscol = 0
                            plusrow = -1
                            DegAz = 0 '180
                        ElseIf jdir = 6 Then 'southwest
                            pluscol = -1
                            plusrow = -1
                            DegAz = 45 '225
                            diagfact = 1 / Math.Sqrt(2)
                        ElseIf jdir = 7 Then 'west
                            pluscol = -1
                            plusrow = 0
                            DegAz = 90 '270
                        ElseIf jdir = 8 Then 'northwest
                            pluscol = -1
                            plusrow = 1
                            DegAz = 135 '315
                            diagfact = 1 / Math.Sqrt(2)
                        End If
                        ReDim tslink(100)
                        Ntslink = 1
                        tslink(1) = ColRowSLINK(SLINKCol(jslink) + pluscol, SLINKRow(jslink) + plusrow)
                        SlopeWindProb = 1 'default
                        If jtest = 1 And bSlopeWindAdj = True And tslink(1) > 0 Then
                            'calculate spix based on the Keane equation and populate the list of tslinks in this direction to write
                            Slope = ((Elevation(tslink(1)) - Elevation(jslink)) * diagfact) / CellSizeMeters
                            'calculate slopef
                            If Slope > 0 Then
                                slopef = 4 / (1 + 3.5 * Math.Exp(-10 * Slope))
                            Else
                                slopef = Math.Exp(-3 * (Slope) ^ 2)
                            End If
                            'calculate(windf)
                            windf = (1 + 0.125 * PolyWindSpeed(jslink)) * (((Math.Cos(Math.PI * (Math.Abs(DegAz - PolyWindDir(jslink)) / 180)) + 1) / 2) ^ (PolyWindSpeed(jslink) ^ 0.6))
                            spix = windf * slopef * diagfact
                            spartspix = spix.ToString
                            arrspix = Split(spartspix, ".")
                            spix = CType(arrspix(0), Double)
                            partspix = 0
                            If arrspix.Length > 1 Then
                                partspix = CType("." & arrspix(1), Double)
                            End If
                            randTest = Rnd(1)
                            If randTest < partspix Then spix = spix + 1
                            'control for no spread in this direction:
                            If spix = 0 Then Ntslink = 0
                            For jspix As Integer = 2 To spix
                                If (SLINKCol(jslink) + (pluscol * jspix)) >= 0 And (SLINKRow(jslink) + (plusrow * jspix)) >= 0 Then
                                    Ntslink = Ntslink + 1
                                    tslink(Ntslink) = ColRowSLINK(SLINKCol(jslink) + (pluscol * jspix), SLINKRow(jslink) + (plusrow * jspix))
                                End If

                            Next

                            'SlopeWindProb = BaseSpreadProb
                            'If ThreeWindDir(jdir) = "U" Then SlopeWindProb = SlopeWindProb + UMod
                            'If ThreeWindDir(jdir) = "N" Then SlopeWindProb = SlopeWindProb + NMod
                            'If ThreeWindDir(jdir) = "D" Then SlopeWindProb = SlopeWindProb + DMod
                            '
                            'SlopeWindProb = SlopeWindProb + Slope * SlopeWeight
                            'SlopeWindProb = Math.Max(SlopeWindProb, MinProb) 'make sure you are at least the minimum
                            'SlopeWindProb = Math.Min(SlopeWindProb, MaxProb) 'make sure you are no more than the maximum
                            'If SlopeWindProb < MinProb Then SlopeWindProb = MinProb
                            'If SlopeWindProb > MaxProb Then SlopeWindProb = MaxProb

                        End If

                        'randTest = Rnd(1)
                        For jtslink As Integer = 1 To Ntslink
                            If tslink(jtslink) > 0 And WindAdj(jdir) = 1 Then 'And randTest < SlopeWindProb Then
                                PrintLine(fnum, PolySLINK(jslink) & "," & PolySLINK(tslink(jtslink)) & "," & Elevation(jslink) & "," & WindDir(jdir))
                                badjprinted = True

                            End If
                        Next
                    Next
                    If badjprinted = True Then Exit For
                Next
                pdummy = PolySLINK(jslink) & "," & SLINKXCoord(jslink) & "," & SLINKYCoord(jslink) & "," & SLINKCol(jslink) & "," & SLINKRow(jslink) & "," & Elevation(jslink) & "," & PolyWindDir(jslink) & "," & PolyWindSpeed(jslink)
                PrintLine(fnum3, pdummy)
            Next
        Else 'determining hexagonal stuff


            For jslink As Integer = 1 To NSLINK
                badjprinted = False
                DetermineDownwindHex(PolyWindDir(jslink))
                'determine even
                even = 1 'true
                k = SLINKRow(jslink) Mod 2
                If k > 0 Then even = 0 'false
                'For jdir As Integer = 1 To 8
                For jtest As Integer = 1 To 2 'once with potential fire spread filter, once without
                    If jtest = 2 Then 'don't filter for wind adjacency the second time through
                        ReDim WindAdj(6)
                        For jdir As Integer = 1 To 6
                            WindAdj(jdir) = 1
                        Next
                    End If

                    For jdir As Integer = 1 To 6
                        ReDim tslink(1)
                        'poly to the northeast
                        If jdir = 1 Then tslink(1) = ColRowSLINK(SLINKCol(jslink) + even, SLINKRow(jslink) + 1)
                        'poly to the east
                        If jdir = 2 Then tslink(1) = ColRowSLINK(SLINKCol(jslink) + 1, SLINKRow(jslink))
                        'poly to the southeast
                        If jdir = 3 Then tslink(1) = ColRowSLINK(SLINKCol(jslink) + even, SLINKRow(jslink) - 1)
                        'poly to the southwest
                        If jdir = 4 Then tslink(1) = ColRowSLINK(SLINKCol(jslink) - (1 - even), SLINKRow(jslink) - 1)
                        'poly to the west
                        If jdir = 5 Then tslink(1) = ColRowSLINK(SLINKCol(jslink) - 1, SLINKRow(jslink))
                        'poly to the northwest
                        If jdir = 6 Then tslink(1) = ColRowSLINK(SLINKCol(jslink) - (1 - even), SLINKRow(jslink) + 1)

                        SlopeWindProb = 1 'default
                        If jtest = 1 And bSlopeWindAdj = True And tslink(1) > 0 Then 'only modify this if you are doing random adjacency probabilities
                            SlopeWindProb = 0.1
                            If ThreeWindDir(jdir) = "U" Then SlopeWindProb = SlopeWindProb + UMod
                            If ThreeWindDir(jdir) = "N" Then SlopeWindProb = SlopeWindProb + NMod
                            If ThreeWindDir(jdir) = "D" Then SlopeWindProb = SlopeWindProb + DMod
                            Slope = (Elevation(tslink(1)) - Elevation(jslink)) / CellSizeMeters
                            SlopeWindProb = SlopeWindProb + Slope * SlopeWeight
                            SlopeWindProb = Math.Max(SlopeWindProb, MinProb) 'make sure you are at least the minimum
                            SlopeWindProb = Math.Min(SlopeWindProb, MaxProb) 'make sure you are no more than the maximum
                            If SlopeWindProb < MinProb Then SlopeWindProb = MinProb
                            If SlopeWindProb > MaxProb Then SlopeWindProb = MaxProb

                        End If
                        randTest = Rnd(1)
                        If tslink(1) > 0 And WindAdj(jdir) = 1 And randTest < SlopeWindProb Then
                            PrintLine(fnum, PolySLINK(jslink) & "," & PolySLINK(tslink(1)) & "," & Elevation(jslink) & "," & WindDir(jdir))
                            badjprinted = True

                        End If
                    Next
                    If badjprinted = True Then Exit For
                Next
                pdummy = PolySLINK(jslink) & "," & SLINKXCoord(jslink) & "," & SLINKYCoord(jslink) & "," & SLINKCol(jslink) & "," & SLINKRow(jslink) & "," & Elevation(jslink) & "," & PolyWindDir(jslink) & "," & PolyWindSpeed(jslink)
                PrintLine(fnum3, pdummy)
            Next


        End If
        PrintLine(fnum, "END")
        FileClose(fnum)
        FileClose(fnum3)
        Exit Sub

ErrorHandler:
        FileClose(fnum)
        FileClose(fnum3)
        bError = True
        MsgBox("error in writing spatial relate file")


    End Sub

    Private Sub WriteSpatialRelateKeane(ByVal outfile As String, ByVal coordfn As String, ByVal bTorus As Boolean) ', ByVal bHex As Boolean, ByVal b4Way As Boolean, ByVal bSlopeWindAdj As Boolean)
        'default original spatialrelate 
        On Error GoTo ErrorHandler
        Dim tslink As Integer
        Dim pdummy As String
        Dim even As Byte '0=odd, 1 = even
        Dim k As Single
        Dim pluscol As Integer
        Dim plusrow As Integer
        Dim badjprinted As Boolean 'flags whether an adjacent polygon has been printed or not
        Dim DegAz As Integer
        'Dim SlopeWindProb As Single
        'Dim randTest As Single
        'Dim BaseSpreadProb As Single = 0.3
        'Dim UMod As Single = 0 'upwind probability enhancement
        'Dim NMod As Single = 0.1 'next to probability enhancement
        'Dim DMod As Single = 0.3 'downwind probability ehnancement
        'Dim SlopeWeight As Single = 0.5 'how much weight slope has in determining probability
        'Dim MaxProb As Single = 0.95 'never get 100% probability
        'Dim MinProb As Single = 0.15 'never get 0% spread probability
        Dim Slope As Single


        Dim fnum3 As Integer = FreeFile()
        FileOpen(fnum3, DirName(coordfn) & FName(coordfn) & "_U.coord", OpenMode.Output)
        tbCoordinatesFN.Text = DirName(coordfn) & FName(coordfn) & "_U.coord"
        PrintLine(fnum3, "NSLINK," & NSLINK & ",KEANE")
        PrintLine(fnum3, "SLINK,X_CENTROID,Y_CENTROID,Column,Row,Elevation,PolyWindDir,PolyWindSpeed")

        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, outfile, OpenMode.Output)
        PrintLine(fnum, "BEGIN VEGETATION-VEGETATION-KEANE")
        'If bHex = False Then
        For jslink As Integer = 1 To NSLINK
            badjprinted = False

            For jdir As Integer = 1 To 8
                If jdir = 1 Then 'poly to the north
                    pluscol = 0
                    plusrow = 1
                    DegAz = 180 '0
                ElseIf jdir = 2 Then 'northeast
                    pluscol = 1
                    plusrow = 1
                    DegAz = 225 '45
                ElseIf jdir = 3 Then 'east
                    pluscol = 1
                    plusrow = 0
                    DegAz = 270 '90
                ElseIf jdir = 4 Then 'southeast
                    pluscol = 1
                    plusrow = -1
                    DegAz = 315 '135
                ElseIf jdir = 5 Then 'south
                    pluscol = 0
                    plusrow = -1
                    DegAz = 0 '180
                ElseIf jdir = 6 Then 'southwest
                    pluscol = -1
                    plusrow = -1
                    DegAz = 45 '225
                ElseIf jdir = 7 Then 'west
                    pluscol = -1
                    plusrow = 0
                    DegAz = 90 '270
                ElseIf jdir = 8 Then 'northwest
                    pluscol = -1
                    plusrow = 1
                    DegAz = 135 '315
                End If
                tslink = ColRowSLINK(SLINKCol(jslink) + pluscol, SLINKRow(jslink) + plusrow)
                If tslink <= 0 And bTorus = True Then
                    If jdir = 1 Then
                        tslink = ColMinSLINK(SLINKCol(jslink))
                    ElseIf jdir = 3 Then
                        tslink = RowMinSLINK(SLINKRow(jslink))
                    ElseIf jdir = 5 Then
                        tslink = ColMaxSLINK(SLINKCol(jslink))
                    ElseIf jdir = 7 Then
                        tslink = RowMaxSLINK(SLINKRow(jslink))
                    End If
                    If tslink = jslink Then tslink = -1 'don't have it spread fire to itself
                End If

                    If tslink > 0 Then 'And WindAdj(jdir) = 1 And randTest < SlopeWindProb Then
                        'FROM_POLY, TO_POLY, ELEV, SPREAD_DEG, BASE_WIND_SPEED, BASE_WIND_DIR
                    PrintLine(fnum, PolySLINK(jslink) & "," & PolySLINK(tslink) & "," & Elevation(jslink) & "," & DegAz & "," & PolyWindSpeed(jslink) & "," & PolyWindDir(jslink))
                        badjprinted = True
                    End If
            Next
            pdummy = PolySLINK(jslink) & "," & SLINKXCoord(jslink) & "," & SLINKYCoord(jslink) & "," & SLINKCol(jslink) & "," & SLINKRow(jslink) & "," & Elevation(jslink) & "," & PolyWindDir(jslink) & "," & PolyWindSpeed(jslink)
            PrintLine(fnum3, pdummy)
        Next

        'Next

        PrintLine(fnum, "END")
        FileClose(fnum)
        FileClose(fnum3)
        Exit Sub

ErrorHandler:
        FileClose(fnum)
        FileClose(fnum3)
        bError = True
        MsgBox("error in writing spatial relate file")


    End Sub


    Private Sub SetDefaultWindDirSpeed(ByVal DefaultWindSpeed As Integer, ByVal DefaultWindDirection As Integer)

        'set the default wind direction for each polygon
        ReDim PolyWindDir(NSLINK), PolyWindSpeed(NSLINK)


        'SET DEFAULT WINDDIR
        For j As Integer = 1 To NSLINK
            PolyWindDir(j) = DefaultWindDirection
        Next
        'SET DEFAULT WINDSPEED
        For j As Integer = 1 To NSLINK
            PolyWindSpeed(j) = DefaultWindSpeed
        Next
    End Sub

    Private Sub ProcessWindDir()
        On Error GoTo ErrorHandler


        Dim fnum As Integer
        Dim wNRows As Integer
        Dim wNCols As Integer
        Dim XLL As Double
        Dim YLL As Double
        Dim wCellSize As Double
        Dim arr() As String
        Dim dummy As String
        Dim kcol As Integer
        Dim krow As Integer
        Dim wColRowWindDir(,) As Integer

        'if you have a wind direction file to load, this can override the PolyWindDir for some or all polygons....
        If System.IO.File.Exists(WindDirectionFileFN) Then
            fnum = FreeFile()
            FileOpen(fnum, WindDirectionFileFN, OpenMode.Input)

            arr = Split(LineInput(fnum), Chr(9))
            If arr.Length = 1 Then arr = Split(arr(0), Chr(32))
            wNCols = arr(arr.Length - 1)
            arr = Split(LineInput(fnum), Chr(9))
            If arr.Length = 1 Then arr = Split(arr(0), Chr(32))
            wNRows = arr(arr.Length - 1)
            arr = Split(LineInput(fnum), Chr(9))
            If arr.Length = 1 Then arr = Split(arr(0), Chr(32))
            XLL = arr(arr.Length - 1)
            arr = Split(LineInput(fnum), Chr(9))
            If arr.Length = 1 Then arr = Split(arr(0), Chr(32))
            YLL = arr(arr.Length - 1)
            arr = Split(LineInput(fnum), Chr(9))
            If arr.Length = 1 Then arr = Split(arr(0), Chr(32))
            wCellSize = arr(arr.Length - 1)
            dummy = LineInput(fnum) 'NODATA value

            'populate row and column coordinate values
            ReDim wRowYVal(wNRows), wColXVal(wNCols), wColRowWindDir(wNCols, wNRows)
            wRowYVal(1) = YLL
            For jrow As Integer = 2 To wNRows
                wRowYVal(jrow) = wRowYVal(jrow - 1) + wCellSize
            Next
            wColXVal(1) = XLL
            For jcol As Integer = 2 To wNCols
                wColXVal(jcol) = wColXVal(jcol - 1) + wCellSize
            Next
            krow = wNRows + 1
            For jrow As Integer = 1 To wNRows
                krow = krow - 1
                arr = Split(LineInput(fnum), Chr(9))
                If arr.Length = 1 Then arr = Split(arr(0), Chr(32))
                For jcol As Integer = 1 To wNCols
                    wColRowWindDir(jcol, krow) = arr(jcol - 1)
                Next
            Next
            FileClose(fnum)


            'now repopulate the polywinddir if you can find a defined direction that is in the file you loaded
            For j As Integer = 1 To NSLINK
                kcol = DetermineK(SLINKXCoord(j), wColXVal)
                krow = DetermineK(SLINKYCoord(j), wRowYVal)
                If kcol > 0 And krow > 0 Then PolyWindDir(j) = wColRowWindDir(kcol, krow)
            Next
        End If
        Exit Sub

ErrorHandler:
        FileClose(fnum)
        bError = True
        MsgBox("Error Processing Wind Direction File")

    End Sub

    Private Sub ProcessWindSpeed()
        'looks to see if wind speed is below a threshhold. If so, it is not downwind in an extreme event

        Dim fnum As Integer
        Dim wNRows As Integer
        Dim wNCols As Integer
        Dim XLL As Double
        Dim YLL As Double
        Dim wCellSize As Double
        Dim arr() As String
        Dim dummy As String
        Dim kcol As Integer
        Dim krow As Integer
        Dim ColRowWindSpeed(,) As Single
        'if you have a wind direction file to load, this can override the PolyWindDir for some or all polygons....
        If System.IO.File.Exists(WindSpeedFileFN) Then
            fnum = FreeFile()
            FileOpen(fnum, WindSpeedFileFN, OpenMode.Input)

            arr = Split(LineInput(fnum), Chr(9))
            If arr.Length = 1 Then arr = Split(arr(0), Chr(32))
            wNCols = arr(arr.Length - 1)
            arr = Split(LineInput(fnum), Chr(9))
            If arr.Length = 1 Then arr = Split(arr(0), Chr(32))
            wNRows = arr(arr.Length - 1)
            arr = Split(LineInput(fnum), Chr(9))
            If arr.Length = 1 Then arr = Split(arr(0), Chr(32))
            XLL = arr(arr.Length - 1)
            arr = Split(LineInput(fnum), Chr(9))
            If arr.Length = 1 Then arr = Split(arr(0), Chr(32))
            YLL = arr(arr.Length - 1)
            arr = Split(LineInput(fnum), Chr(9))
            If arr.Length = 1 Then arr = Split(arr(0), Chr(32))
            wCellSize = arr(arr.Length - 1)
            dummy = LineInput(fnum) 'NODATA value

            'populate row and column coordinate values
            ReDim wRowYVal(wNRows), wColXVal(wNCols), ColRowWindSpeed(wNCols, wNRows)
            wRowYVal(1) = YLL
            For jrow As Integer = 2 To wNRows
                wRowYVal(jrow) = wRowYVal(jrow - 1) + wCellSize
            Next
            wColXVal(1) = XLL
            For jcol As Integer = 2 To wNCols
                wColXVal(jcol) = wColXVal(jcol - 1) + wCellSize
            Next
            krow = wNRows + 1
            For jrow As Integer = 1 To wNRows
                krow = krow - 1
                arr = Split(LineInput(fnum), Chr(9))
                If arr.Length = 1 Then arr = Split(arr(0), Chr(32))
                For jcol As Integer = 1 To wNCols
                    ColRowWindSpeed(jcol, krow) = arr(jcol - 1)
                Next
            Next
            FileClose(fnum)


            'now repopulate the polywinddir if you can find a defined direction that is in the file you loaded
            For j As Integer = 1 To NSLINK
                kcol = DetermineK(SLINKXCoord(j), wColXVal)
                krow = DetermineK(SLINKYCoord(j), wRowYVal)
                If kcol > 0 And krow > 0 Then
                    If ColRowWindSpeed(kcol, krow) < MinExtremeWindSpeed Then
                        PolyWindDir(j) = 999 'will cause it to not find a value in DetermineDownwind, and all values for the poly will be "N"
                    End If
                    PolyWindSpeed(j) = ColRowWindSpeed(kcol, krow)
                End If
            Next
        End If
        Exit Sub

ErrorHandler:
        FileClose(fnum)
        bError = True
        MsgBox("Error Processing Wind Speed File")
    End Sub

    Private Sub WriteAttributesAll(ByVal infile As String, ByVal outfile As String, ByVal GAName As String, ByVal bRandomVeg As Boolean, ByVal bReadPathways As Boolean, ByVal bFutProcess As Boolean, ByVal bFutTreatment As Boolean, ByVal bHardwireSLINK As Boolean)

        'On Error GoTo ErrorHandler
        Dim MaxX As Double = -99999999
        Dim MaxXy As Double 'y val associated with the maxx
        Dim MaxY As Double = -99999999
        Dim MaxYx As Double
        Dim MinX As Double = 99999999
        Dim MinXy As Double
        Dim MinY As Double = 99999999
        Dim MinYx As Double

        Dim arr() As String
        Dim XVal As Double
        Dim YVal As Double
        Dim baseX As Double
        Dim baseY As Double
        Dim tint As Integer
        Dim YOffset As Double = 99999999 'difference in y coordinates of adjacent cells in the y direction
        Dim XOffset As Double = 99999999 'difference of x coordinates of adjacent cells in the x direction
        Dim cellwidth As Double = 99999999 'how wide the cell is
        Dim twidth As Double
        Dim bigM As Long = 500000
        Dim bsurrounded As Boolean
        Dim nadj As Integer
        Dim nsearchcells As Integer
        Dim centercellindex As Integer
        Dim adjcellind() As Integer

        Dim slopeX As Double 'slope of the x axis line for the grid cells
        Dim slopeY As Double 'slope of the y axis line for the grid cells
        Dim tslope As Double

        Dim YintX As Double 'y-intercept of the x axis line of the grid cells
        Dim YintY As Double 'y-intercept of the y axis line for the grid cells

        Dim Width As Double
        Dim Height As Double

        Dim dummy As String
        Dim pdummy As String

        'random veg stuff
        Dim khtg As Integer
        Dim kspec As Integer
        Dim ksize As Integer
        Dim kdensity As Integer
        Dim bUsed As Boolean
        Dim kpath As Integer
        Dim arr2() As String

        'each row and column has a min/max used index
        Dim RowMinIndex() As Integer 'by NCols
        Dim RowMaxIndex() As Integer 'by NCols
        Dim ColMinIndex() As Integer 'by NRows
        Dim ColMaxIndex() As Integer 'by NRows

        'for lock files - if you are doing these
        Dim tdist As String
        Dim NDisturbs As Integer
        Dim DisturbName() As String
        Dim SLINKDisturb() As String
        Dim lockfile As String

        'for treatment files - if you are doing these
        Dim ttrt As String
        Dim NFtrts As Integer
        Dim FtrtName() As String
        Dim SLINKFtrt() As String
        Dim Ftrtfile As String


        Dim kslink As Integer
        Dim SLINK As Integer

        If bRandomVeg And bReadPathways Then FillCombosUsedRunOutput()

        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        dummy = LineInput(fnum) 'string of field names

        ReDim SLINKXCoord(bigM), SLINKYCoord(bigM)
        NSLINK = 0
        'go through and figure out the number of polygons
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "" Then Exit Do

            arr = Split(dummy, ",")
            If arr(GA_Field) = GAName Or GAName = "all" Then
                NSLINK = NSLINK + 1
                'If NSLINK = 11389 Then Stop
                XVal = CType(arr(X_Centroid_Field), Double)
                YVal = CType(arr(Y_Centroid_Field), Double)
                If NSLINK <= bigM Then
                    SLINKXCoord(NSLINK) = XVal
                    SLINKYCoord(NSLINK) = YVal
                End If
                If XVal > MaxX Then
                    MaxX = XVal
                    MaxXy = YVal
                End If
                If XVal < MinX Then
                    MinX = XVal
                    MinXy = YVal
                End If
                If YVal > MaxY Then
                    MaxY = YVal
                    MaxYx = XVal
                End If
                If YVal < MinY Then
                    MinY = YVal
                    MinYx = XVal
                End If

                tint = Math.Round(Math.Abs(XVal - MinX), 2)
                If tint < XOffset And tint > 0 Then
                    XOffset = Math.Abs(XVal - MinX)
                    baseX = XVal
                    baseY = YVal
                End If
                tint = Math.Round(Math.Abs(XVal - MaxX), 2)
                If tint < XOffset And tint > 0 Then
                    XOffset = Math.Abs(XVal - MaxX)
                    baseX = XVal
                    baseY = YVal
                End If
                tint = Math.Round(Math.Abs(YVal - MinY), 2)
                If tint < YOffset And tint > 0 Then
                    YOffset = Math.Abs(YVal - MinY)
                    baseX = XVal
                    baseY = YVal
                End If
                tint = Math.Round(Math.Abs(YVal - MaxY), 2)
                If tint < YOffset And tint > 0 Then
                    YOffset = Math.Abs(YVal - MaxY)
                    baseX = XVal
                    baseY = YVal
                End If
                If baseX <> XVal Or baseY <> YVal Then
                    twidth = Math.Round(Math.Sqrt(((XVal - baseX) ^ 2 + (YVal - baseY) ^ 2)), 2)
                    cellwidth = Math.Min(cellwidth, twidth) '* Math.Sqrt(GenFactor)
                    'If cellwidth = 0 Then Stop
                End If
            End If
        Loop
        FileClose(fnum)

        'now, modify the cellwidth
        cellwidth = Math.Round(cellwidth * Math.Sqrt(GenFactor), 2)

        'search through the cells to see if you can find a surrounded cell and get your theta value
        bsurrounded = False
        nsearchcells = Math.Min(NSLINK, bigM)
        For jcell As Integer = 1 To nsearchcells
            nadj = 0
            ReDim adjcellind(4)
            For jjcell As Integer = 1 To nsearchcells
                If jcell <> jjcell Then
                    twidth = Math.Round(Math.Sqrt(((SLINKXCoord(jcell) - SLINKXCoord(jjcell)) ^ 2 + (SLINKYCoord(jcell) - SLINKYCoord(jjcell)) ^ 2)), 2)
                    If twidth = cellwidth Then
                        nadj = nadj + 1
                        adjcellind(nadj) = jjcell
                    End If
                    If nadj = 4 Then
                        centercellindex = jcell
                        bsurrounded = True
                        Exit For
                    End If
                End If
            Next
            If bsurrounded = True Then Exit For
        Next


        'determine the slopes
        slopeX = 9999999
        slopeY = 9999999
        For jadjcell As Integer = 1 To 4
            If SLINKXCoord(adjcellind(jadjcell)) >= SLINKXCoord(centercellindex) Then
                'Dim yo As Double = SLINKYCoord(centercellindex) - SLINKYCoord(adjcellind(jadjcell))
                'theta = Math.Asin(yo / cellwidth)
                If Math.Round(SLINKYCoord(adjcellind(jadjcell)), 2) = Math.Round(SLINKYCoord(centercellindex), 2) Then
                    'you have a perfect coordinate system already
                    slopeX = 0
                    slopeY = 9999999 'infinity
                    Exit For
                End If
                'smallest slope gets the x-axis assignmnet
                tslope = (SLINKYCoord(adjcellind(jadjcell)) - SLINKYCoord(centercellindex)) / (SLINKXCoord(adjcellind(jadjcell)) - SLINKXCoord(centercellindex))
                If Math.Abs(tslope) < Math.Abs(slopeX) Then
                    slopeY = slopeX
                    slopeX = tslope
                Else
                    slopeY = tslope
                End If
            End If
        Next

        'now find the y-intercepts, and thus line equations - of the 4 bordering lines
        'left line - y axis - goes through minX, minXy
        If slopeY = 9999999 Then
            YintY = 9999999
            Height = MaxY - MinY
        Else
            YintY = MinXy - slopeY * MinX
        End If
        'bottom line - x axis - goes through minY, minYx
        If slopeX = 0 Then
            YintX = MinY
            Width = MaxX - MinX
        Else
            YintX = MinY - slopeX * MinYx
        End If
        If slopeX <> 0 Then
            'calc height - distance from (maxY, maxYx) to line
            Height = PointToLineDist(MaxYx, MaxY, slopeX, YintX)
            'calc width - distance from (MaxX, MaxXy) to line
            Width = PointToLineDist(MaxX, MaxXy, slopeY, YintY)
        End If


        NRows = Math.Round(Height / cellwidth, 0) + 1
        NCols = Math.Round(Width / cellwidth, 0) + 1
        ReDim ColRowSLINK(NCols + 10, NRows + 10) 'to accommodate search later for 1 greater than biggest row and column
        ReDim UseColRow(NCols, NRows)
        ReDim RowMinIndex(NRows), RowMaxIndex(NRows), RowMinSLINK(NRows), RowMaxSLINK(NRows)
        ReDim ColMinIndex(NCols), ColMaxIndex(NCols), ColMinSLINK(NCols), ColMaxSLINK(NCols)
        For jrow As Integer = 0 To NRows
            RowMinIndex(jrow) = bigM
            RowMaxIndex(jrow) = -1
        Next
        For jcol As Integer = 0 To NCols
            ColMinIndex(jcol) = bigM
            ColMaxIndex(jcol) = -1
        Next

        ReDim SLINKXCoord(NSLINK), SLINKYCoord(NSLINK), PolySLINK(NSLINK)
        ReDim SLINKRow(NSLINK), SLINKCol(NSLINK), Elevation(NSLINK), SLINKDisturb(NSLINK), SLINKFtrt(NSLINK)

        'go through again to get all the atts and write the outputs
        Dim fnum2 As Integer = FreeFile()
        FileOpen(fnum2, outfile, OpenMode.Output)
        PrintLine(fnum2, "BEGIN VEGETATION")


        fnum = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        dummy = LineInput(fnum)
        'SLINK = 0
        kslink = 0
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If Trim(dummy) = "" Then Exit Do
            arr = Split(dummy, ",")
            If arr(GA_Field) = GAName Or GAName = "all" Then

                'If SLINK = 34603 Then Stop
                XVal = CType(arr(X_Centroid_Field), Double)
                YVal = CType(arr(Y_Centroid_Field), Double)

                'this transformation based on formula that rotates the coordinate system counter-clockwise. Linear Algebra p. 347-348

                If slopeX = 0 Then
                    Row = DetermineRowCol(YVal, MinY, cellwidth) + 1
                    Col = DetermineRowCol(XVal, MinX, cellwidth) + 1
                Else
                    Row = Math.Round(PointToLineDist(XVal, YVal, slopeX, YintX) / cellwidth, 0) + 1
                    Col = Math.Round(PointToLineDist(XVal, YVal, slopeY, YintY) / cellwidth, 0) + 1
                End If

                If UseColRow(Col, Row) = 1 Then NSLINK = NSLINK - 1
                If UseColRow(Col, Row) = 0 Then
                    UseColRow(Col, Row) = 1
                    kslink += 1
                    If bHardwireSLINK = False Then
                        SLINK = SLINK + 1
                    Else
                        SLINK = arr(SLINK_Field)
                    End If
                    PolySLINK(kslink) = SLINK

                    If RowMinIndex(Row) > Col Then
                        RowMinIndex(Row) = Col
                        RowMinSLINK(Row) = kslink
                    End If
                    If RowMaxIndex(Row) < Col Then
                        RowMaxIndex(Row) = Col
                        RowMaxSLINK(Row) = kslink
                    End If
                    If ColMinIndex(Col) > Row Then
                        ColMinIndex(Col) = Row
                        ColMinSLINK(Col) = kslink
                    End If
                    If ColMaxIndex(Col) < Row Then
                        ColMaxIndex(Col) = Row
                        ColMaxSLINK(Col) = kslink
                    End If

                    SLINKXCoord(kslink) = XVal 'XVal * Math.Cos(theta) + YVal * Math.Sin(theta)
                    SLINKYCoord(kslink) = YVal '(-1 * XVal) * Math.Sin(theta) + YVal * Math.Cos(theta)

                    SLINKRow(kslink) = Row
                    SLINKCol(kslink) = Col
                    ColRowSLINK(Col, Row) = kslink
                    Stand_ID = arr(STAND_ID_Field)
                    Acres = arr(ACRES_Field) * GenFactor
                    HTG = arr(Ecological_stratification_Field)
                    Covertype = arr(Species_Field)
                    SizeClass = arr(Size_Class_Field)
                    Density = arr(Density_Field)
                    If bFutProcess Then SLINKDisturb(kslink) = arr(Fut_Process_Field)
                    If bFutTreatment Then SLINKFtrt(kslink) = arr(Fut_Treatment_Field)
                    If bRandomVeg = True And Covertype <> "WATER" And Covertype <> "NF" Then
                        khtg = GetStrIndex(HTGNameLUTq, HTG)
                        kpath = CInt(Math.Ceiling(Rnd() * NPathwaysInHTG(khtg)))
                        arr2 = Split(PathwayString(khtg, kpath), "/")
                        kspec = GetStrIndex(SpeciesNameLUTq, arr2(0))
                        Covertype = SpeciesNameLUTq(kspec)
                        ksize = GetSizeInd(SizeClassNameLUTq, arr2(1))
                        SizeClass = SizeClassNameLUTq(ksize)
                        kdensity = GetStrIndex(DensityNameLUTq, arr2(2))
                        Density = DensityNameLUTq(kdensity)
                    End If
                    PastProcess = ""
                    If Past_Process_Field > -1 Then PastProcess = arr(Past_Process_Field)
                    If Trim(PastProcess) = "" Then PastProcess = "?"
                    PastProcess_T = ""
                    If Time_Since_Past_Process_Field > -1 Then PastProcess_T = arr(Time_Since_Past_Process_Field)
                    If PastProcess_T = "" Then PastProcess_T = "0"
                    PastTreatment = ""
                    If Past_Treatment_Field > -1 Then PastTreatment = arr(Past_Treatment_Field)
                    If Trim(PastTreatment) = "" Then PastTreatment = "?"
                    PastTreatment_T = ""
                    If Time_Since_Past_Treatment_Field > -1 Then PastTreatment_T = arr(Time_Since_Past_Treatment_Field)
                    If PastTreatment_T = "" Then PastTreatment_T = "0"
                    Ownership = arr(Ownership_Field)
                    FMZ = arr(Fire_Management_Zone_Field)
                    SpecialArea = arr(Special_Area_Field)
                    Elevation(kslink) = arr(Elevation_Field) 'used in spatialrelate file

                    pdummy = PolySLINK(kslink) & "," & Row & "," & Col & "," & Stand_ID & "," & Acres & "," & HTG & "," & D1 & "," & D2 & "," & Covertype & "," & SizeClass & "," & Density & "," & PastProcess & "," & PastProcess_T & "," & PastTreatment & "," & PastTreatment_T & "," & D3 & "," & D4 & "," & Ownership & "," & D5 & "," & D6 & "," & FMZ & "," & SpecialArea & "," & D7 & "," & SLINKYCoord(kslink) & "," & SLINKXCoord(kslink) '& D8 & "," & D9
                    PrintLine(fnum2, pdummy)
                End If 'usecolrow
                End If

        Loop
        CellSizeMeters = Math.Sqrt(Acres * 4046.856)
        FileClose(fnum)
        PrintLine(fnum2, "END")
        FileClose(fnum2)

        If bFutProcess Then
            'populate the unique disturbance names
            NDisturbs = 0
            ReDim DisturbName(NDisturbs)
            For jslink As Integer = 1 To NSLINK
                tdist = GetStrIndex(DisturbName, SLINKDisturb(jslink))
                If tdist < 0 Then
                    NDisturbs = NDisturbs + 1
                    ReDim Preserve DisturbName(NDisturbs)
                    DisturbName(NDisturbs) = SLINKDisturb(jslink)
                End If
            Next

            If NDisturbs > 0 Then
                'write out the file
                fnum = FreeFile()
                lockfile = DirName(outfile) & FName(outfile) & "_ProcessLock.txt"
                FileOpen(fnum, lockfile, OpenMode.Output)
                For jdist As Integer = 1 To NDisturbs
                    If Trim(DisturbName(jdist)) <> "" Then
                        PrintLine(fnum, "1," & DisturbName(jdist))
                        For jslink As Integer = 1 To NSLINK
                            If SLINKDisturb(jslink) = DisturbName(jdist) Then
                                PrintLine(fnum, PolySLINK(jslink))
                            End If
                        Next
                    End If
                Next
                FileClose(fnum)
            End If
        End If

        If bFutTreatment Then
            'populate the unique treatment names
            NFtrts = 0
            ReDim FtrtName(NFtrts)
            For jslink As Integer = 1 To NSLINK
                ttrt = GetStrIndex(FtrtName, SLINKFtrt(jslink))
                If ttrt < 0 Then
                    NFtrts += 1
                    ReDim Preserve FtrtName(NFtrts)
                    FtrtName(NFtrts) = SLINKFtrt(jslink)
                End If
            Next
            If NFtrts > 0 Then
                'write out the file
                fnum = FreeFile()
                lockfile = DirName(outfile) & FName(outfile) & "_FTreatmentLock.txt"
                FileOpen(fnum, lockfile, OpenMode.Output)
                For jftrt As Integer = 1 To NFtrts
                    If Trim(FtrtName(jftrt)) <> "" Then
                        PrintLine(fnum, "1," & FtrtName(jftrt))
                        For jslink As Integer = 1 To NSLINK
                            If SLINKFtrt(jslink) = FtrtName(jftrt) Then
                                PrintLine(fnum, PolySLINK(jslink))
                            End If
                        Next
                    End If
                Next
                FileClose(fnum)
            End If
        End If

        Exit Sub

ErrorHandler:
        FileClose(fnum)
        FileClose(fnum2)
        bError = True
        MsgBox("Error writing attributesall")

    End Sub

    Private Sub ReadFieldNames(ByVal filename As String, ByVal fieldnames As String)
        On Error GoTo ErrorHandler

        Dim dummy As String
        Dim arr() As String
        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, filename, OpenMode.Input)
        dummy = LineInput(fnum)

        '        Ecological(stratification, HT_GRP)
        arr = Split(LineInput(fnum), ",")
        Ecological_stratification_Field = StringInt(Trim(arr(1)), fieldnames)
        '        Acres, Acres
        arr = Split(LineInput(fnum), ",")
        ACRES_Field = StringInt(Trim(arr(1)), fieldnames)
        '        Stand(ID, Stand_ID)
        arr = Split(LineInput(fnum), ",")
        STAND_ID_Field = StringInt(Trim(arr(1)), fieldnames)
        '        Ownership, Ownership
        arr = Split(LineInput(fnum), ",")
        Ownership_Field = StringInt(Trim(arr(1)), fieldnames)
        '        Roads, NA
        arr = Split(LineInput(fnum), ",")
        Roads_Field = StringInt(Trim(arr(1)), fieldnames)
        '        Elevation, ELEV
        arr = Split(LineInput(fnum), ",")
        Elevation_Field = StringInt(Trim(arr(1)), fieldnames)
        'Fire Management Zone,FMZ
        arr = Split(LineInput(fnum), ",")
        Fire_Management_Zone_Field = StringInt(Trim(arr(1)), fieldnames)
        '        Special(Area, SP_AREA)
        arr = Split(LineInput(fnum), ",")
        Special_Area_Field = StringInt(Trim(arr(1)), fieldnames)
        '        Species, SPECIES
        arr = Split(LineInput(fnum), ",")
        Species_Field = StringInt(Trim(arr(1)), fieldnames)
        'Size Class,SIZE_CLASS
        arr = Split(LineInput(fnum), ",")
        Size_Class_Field = StringInt(Trim(arr(1)), fieldnames)
        'Density (Canopy Cover %),DENSITY
        arr = Split(LineInput(fnum), ",")
        Density_Field = StringInt(Trim(arr(1)), fieldnames)
        '        Past(Process, Process)
        arr = Split(LineInput(fnum), ",")
        Past_Process_Field = StringInt(Trim(arr(1)), fieldnames)
        '        Past(Treatment, TREATMENT)
        arr = Split(LineInput(fnum), ",")
        Past_Treatment_Field = StringInt(Trim(arr(1)), fieldnames)
        'Time Since Past Process (Decades),PROCESS_T
        arr = Split(LineInput(fnum), ",")
        Time_Since_Past_Process_Field = StringInt(Trim(arr(1)), fieldnames)
        'Time Since Past Treatment (Decades),TREATMENT_T
        arr = Split(LineInput(fnum), ",")
        Time_Since_Past_Treatment_Field = StringInt(Trim(arr(1)), fieldnames)
        '        X(Centroid, POINT_X)
        arr = Split(LineInput(fnum), ",")
        X_Centroid_Field = StringInt(Trim(arr(1)), fieldnames)
        '        Y(Centroid, POINT_Y)
        arr = Split(LineInput(fnum), ",")
        Y_Centroid_Field = StringInt(Trim(arr(1)), fieldnames)
        '        Future Process (Optional)
        arr = Split(LineInput(fnum), ",")

        Fut_Process_Field = -1
        Fut_Treatment_Field = -1
        GA_Field = -1
        SLINK_Field = -1

        If arr.Length > 1 Then
            Fut_Process_Field = StringInt(Trim(arr(1)), fieldnames)
        Else
            GoTo FileClose
        End If
        '        Future Treatment (Optional)
        arr = Split(LineInput(fnum), ",")
        If arr.Length > 1 Then
            Fut_Treatment_Field = StringInt(Trim(arr(1)), fieldnames)
        Else
            GoTo FileClose
        End If

        '        Geog. Area (Optional)
        arr = Split(LineInput(fnum), ",")
        If arr.Length > 1 Then
            GA_Field = StringInt(Trim(arr(1)), fieldnames)
            If GA_Field > -1 Then
                cbSplitGAs.Checked = True
                GAFieldName = Trim(arr(1))
                tbGAField.Text = GAFieldName
            End If
        Else
            GoTo FileClose
        End If
        '        slink (Optional)
        arr = Split(LineInput(fnum), ",")
        If arr.Length > 1 Then
            SLINK_Field = StringInt(Trim(arr(1)), fieldnames)
        End If

FileClose:
        FileClose(fnum)
        Exit Sub

ErrorHandler:
        FileClose(fnum)
        bError = True
        MsgBox("Error reading field names file")
    End Sub

    Private Sub ReadCoordFile(ByVal infile As String, ByRef KeaneSIMPPLLE As String)
        If System.IO.File.Exists(infile) = False Then
            bError = True
            Exit Sub
        End If
        Dim arr() As String
        Dim dummy As String

        NRows = 0
        NCols = 0

        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        arr = Split(LineInput(fnum), ",")
        NSLINK = arr(1)
        KeaneSIMPPLLE = arr(2)
        dummy = LineInput(fnum) 'dummy column headers
        ReDim SLINKRow(NSLINK), SLINKCol(NSLINK), Elevation(NSLINK) ', ColRowSLINK(NCols + 1, NRows + 1) 'to accommodate search later for 1 greater than biggest row and column
        ReDim SLINKXCoord(NSLINK), SLINKYCoord(NSLINK), PolyWindDir(NSLINK), PolyWindSpeed(NSLINK)
        'collect NSLINK
        For j As Integer = 1 To NSLINK
            arr = Split(LineInput(fnum), ",")
            SLINKXCoord(j) = arr(1)
            SLINKYCoord(j) = arr(2)
            SLINKCol(j) = arr(3)
            NCols = Math.Max(NCols, SLINKCol(j))
            SLINKRow(j) = arr(4)
            NRows = Math.Max(NRows, SLINKRow(j))
            Elevation(j) = arr(5)
            PolyWindDir(j) = arr(6)
            PolyWindSpeed(j) = arr(7)
        Next

        FileClose(fnum)

        ReDim ColRowSLINK(NCols + 1, NRows + 1)
        For j As Integer = 1 To NSLINK
            ColRowSLINK(SLINKCol(j), SLINKRow(j)) = j
        Next


    End Sub

    Private Sub GetUniqueGAs(ByVal infile As String, ByVal GAField As String, ByVal GAFieldInd As Integer, ByRef GANames() As String)
        Dim dummy As String
        Dim arr() As String
        Dim fnum As Integer
        Dim kga As Integer

        fnum = FreeFile()
        FileOpen(fnum, infile, OpenMode.Input)
        dummy = LineInput(fnum)
        ReDim GANames(0)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If Trim(dummy) = "" Then Exit Do
            arr = Split(dummy, ",")
            If arr.Length - 1 < GAFieldInd Then Exit Do
            kga = GetStrIndex(GANames, arr(GAFieldInd))
            If kga < 0 Then
                NGA = NGA + 1
                ReDim Preserve GANames(NGA)
                GANames(NGA) = arr(GAFieldInd)
            End If
        Loop
        FileClose(fnum)

    End Sub

    Private Sub DetermineDownwind(ByVal Direction As Integer)
        'SETS WindDir
        'Direction described in comments is the direction the wind comes FROM
        '1 is polygon to the north of center, then numbers proceed around clockwise until 8 which is the polygon NW of center
        ReDim WindDir(8)
        ReDim ThreeWindDir(8)
        ReDim WindAdj(8)
        For j As Integer = 1 To 8
            ThreeWindDir(j) = "N" 'default
            WindDir(j) = "N" 'default
            WindAdj(j) = 1
        Next


        If (Direction > 337.5 And Direction <= 360) Or Direction <= 22.5 Then
            'N=337.5 - 22.5
            WindDir(4) = "D"
            WindDir(5) = "D"
            WindDir(6) = "D"
            ThreeWindDir(4) = "D"
            ThreeWindDir(5) = "D"
            ThreeWindDir(6) = "D"
            ThreeWindDir(8) = "U"
            ThreeWindDir(1) = "U"
            ThreeWindDir(2) = "U"
            'If bLimitBackburn Then
            '    WindAdj(2) = 0
            '    WindAdj(3) = 0
            '    WindAdj(8) = 0
            '    WindAdj(7) = 0
            'End If
        ElseIf Direction > 22.5 And Direction <= 67.5 Then
            'NE = 22.5 - 67.5
            WindDir(5) = "D"
            WindDir(6) = "D"
            WindDir(7) = "D"
            ThreeWindDir(5) = "D"
            ThreeWindDir(6) = "D"
            ThreeWindDir(7) = "D"
            ThreeWindDir(1) = "U"
            ThreeWindDir(2) = "U"
            ThreeWindDir(3) = "U"
            'If bLimitBackburn Then
            '    WindAdj(1) = 0
            '    WindAdj(8) = 0
            '    WindAdj(3) = 0
            '    WindAdj(4) = 0
            'End If
        ElseIf Direction > 67.5 And Direction <= 112.5 Then
            'E = 67.5 - 112.5
            WindDir(6) = "D"
            WindDir(7) = "D"
            WindDir(8) = "D"
            ThreeWindDir(6) = "D"
            ThreeWindDir(7) = "D"
            ThreeWindDir(8) = "D"
            ThreeWindDir(2) = "U"
            ThreeWindDir(3) = "U"
            ThreeWindDir(4) = "U"
            'If bLimitBackburn Then
            '    WindAdj(1) = 0
            '    WindAdj(2) = 0
            '    WindAdj(4) = 0
            '    WindAdj(5) = 0
            'End If
        ElseIf Direction > 112.5 And Direction <= 157.5 Then
            'SE = 112.5 - 157.5
            WindDir(7) = "D"
            WindDir(8) = "D"
            WindDir(1) = "D"
            ThreeWindDir(7) = "D"
            ThreeWindDir(8) = "D"
            ThreeWindDir(1) = "D"
            ThreeWindDir(3) = "U"
            ThreeWindDir(4) = "U"
            ThreeWindDir(5) = "U"
            'If bLimitBackburn Then
            '    WindAdj(2) = 0
            '    WindAdj(3) = 0
            '    WindAdj(5) = 0
            '    WindAdj(6) = 0
            'End If
        ElseIf Direction > 157.5 And Direction <= 202.5 Then
            'S = 157.5 - 202.5
            WindDir(8) = "D"
            WindDir(1) = "D"
            WindDir(2) = "D"
            ThreeWindDir(8) = "D"
            ThreeWindDir(1) = "D"
            ThreeWindDir(2) = "D"
            ThreeWindDir(4) = "U"
            ThreeWindDir(5) = "U"
            ThreeWindDir(6) = "U"
            'If bLimitBackburn Then
            '    WindAdj(3) = 0
            '    WindAdj(4) = 0
            '    WindAdj(6) = 0
            '    WindAdj(7) = 0
            'End If
        ElseIf Direction > 202.5 And Direction <= 247.5 Then
            'SW = 202.5 - 247.5
            WindDir(1) = "D"
            WindDir(2) = "D"
            WindDir(3) = "D"
            ThreeWindDir(1) = "D"
            ThreeWindDir(2) = "D"
            ThreeWindDir(3) = "D"
            ThreeWindDir(5) = "U"
            ThreeWindDir(6) = "U"
            ThreeWindDir(7) = "U"
            'If bLimitBackburn Then
            '    WindAdj(4) = 0
            '    WindAdj(5) = 0
            '    WindAdj(7) = 0
            '    WindAdj(8) = 0
            'End If
        ElseIf Direction > 247.5 And Direction <= 292.5 Then
            'W = 247.5 - 292.5
            WindDir(2) = "D"
            WindDir(3) = "D"
            WindDir(4) = "D"
            ThreeWindDir(2) = "D"
            ThreeWindDir(3) = "D"
            ThreeWindDir(4) = "D"
            ThreeWindDir(6) = "U"
            ThreeWindDir(7) = "U"
            ThreeWindDir(8) = "U"
            'If bLimitBackburn Then
            '    WindAdj(8) = 0
            '    WindAdj(1) = 0
            '    WindAdj(5) = 0
            '    WindAdj(6) = 0
            'End If
        ElseIf Direction > 292.5 And Direction <= 337.5 Then
            'NW = 202.5 - 337.5
            WindDir(3) = "D"
            WindDir(4) = "D"
            WindDir(5) = "D"
            ThreeWindDir(3) = "D"
            ThreeWindDir(4) = "D"
            ThreeWindDir(5) = "D"
            ThreeWindDir(7) = "U"
            ThreeWindDir(8) = "U"
            ThreeWindDir(1) = "U"
            'If bLimitBackburn Then
            '    WindAdj(1) = 0
            '    WindAdj(2) = 0
            '    WindAdj(6) = 0
            '    WindAdj(7) = 0
            'End If
        End If

    End Sub

    Private Sub DetermineDownwindHex(ByVal Direction As Integer)
        'SETS WindDir
        'Direction described in comments is the direction the wind comes FROM
        '1 is polygon to the northeast of center, then numbers proceed around clockwise until 6 which is the polygon NW of center
        ReDim WindDir(6)
        ReDim WindAdj(6)
        ReDim ThreeWindDir(6)
        For j As Integer = 1 To 6
            WindDir(j) = "N" 'default
            ThreeWindDir(j) = "N" 'default
            WindAdj(j) = 1
        Next


        If (Direction > 337.5 And Direction <= 360) Or Direction <= 22.5 Then
            'N=337.5 - 22.5
            WindDir(3) = "D"
            WindDir(4) = "D"
            For j As Integer = 1 To 6
                ThreeWindDir(j) = WindDir(j)
            Next
            ThreeWindDir(6) = "U"
            ThreeWindDir(1) = "U"
            'If bLimitBackburn Then
            '    WindAdj(2) = 0
            '    WindAdj(5) = 0
            'End If
        ElseIf Direction > 22.5 And Direction <= 67.5 Then
            'NE = 22.5 - 67.5
            WindDir(3) = "D"
            WindDir(4) = "D"
            WindDir(5) = "D"
            For j As Integer = 1 To 6
                ThreeWindDir(j) = WindDir(j)
            Next
            ThreeWindDir(1) = "U"
            'If bLimitBackburn Then
            '    WindAdj(2) = 0
            '    WindAdj(6) = 0
            'End If
        ElseIf Direction > 67.5 And Direction <= 112.5 Then
            'E = 67.5 - 112.5
            WindDir(4) = "D"
            WindDir(5) = "D"
            WindDir(6) = "D"
            For j As Integer = 1 To 6
                ThreeWindDir(j) = WindDir(j)
            Next
            ThreeWindDir(2) = "U"
            'If bLimitBackburn Then
            '    WindAdj(1) = 0
            '    WindAdj(3) = 0
            'End If
        ElseIf Direction > 112.5 And Direction <= 157.5 Then
            'SE = 112.5 - 157.5
            WindDir(5) = "D"
            WindDir(6) = "D"
            WindDir(1) = "D"
            For j As Integer = 1 To 6
                ThreeWindDir(j) = WindDir(j)
            Next
            ThreeWindDir(3) = "U"
            'If bLimitBackburn Then
            '    WindAdj(2) = 0
            '    WindAdj(4) = 0
            'End If
        ElseIf Direction > 157.5 And Direction <= 202.5 Then
            'S = 157.5 - 202.5
            WindDir(6) = "D"
            WindDir(1) = "D"
            For j As Integer = 1 To 6
                ThreeWindDir(j) = WindDir(j)
            Next
            ThreeWindDir(3) = "U"
            ThreeWindDir(4) = "U"
            'If bLimitBackburn Then
            '    WindAdj(2) = 0
            '    WindAdj(5) = 0
            'End If
        ElseIf Direction > 202.5 And Direction <= 247.5 Then
            'SW = 202.5 - 247.5
            WindDir(6) = "D"
            WindDir(1) = "D"
            WindDir(2) = "D"
            For j As Integer = 1 To 6
                ThreeWindDir(j) = WindDir(j)
            Next
            ThreeWindDir(4) = "U"
            'If bLimitBackburn Then
            '    WindAdj(3) = 0
            '    WindAdj(5) = 0
            'End If
        ElseIf Direction > 247.5 And Direction <= 292.5 Then
            'W = 247.5 - 292.5
            WindDir(1) = "D"
            WindDir(2) = "D"
            WindDir(3) = "D"
            For j As Integer = 1 To 6
                ThreeWindDir(j) = WindDir(j)
            Next
            ThreeWindDir(5) = "U"
            'If bLimitBackburn Then
            '    WindAdj(4) = 0
            '    WindAdj(6) = 0
            'End If
        ElseIf Direction > 292.5 And Direction <= 337.5 Then
            'NW = 202.5 - 337.5
            WindDir(2) = "D"
            WindDir(3) = "D"
            WindDir(4) = "D"
            For j As Integer = 1 To 6
                ThreeWindDir(j) = WindDir(j)
            Next
            ThreeWindDir(6) = "U"
            'If bLimitBackburn Then
            '    WindAdj(1) = 0
            '    WindAdj(5) = 0
            'End If
        End If

    End Sub

    Private Function StringInt(ByVal str As String, ByVal list As String) As Integer
        StringInt = -1
        Dim arr() As String
        arr = Split(list, ",")
        For j As Integer = 0 To arr.Length - 1
            If str = Trim(arr(j)) Then
                StringInt = j
                Exit Function
            End If
        Next
    End Function

    'Private Sub TransformCoord(ByVal xcoord As Double, ByVal ycoord As Double, ByVal theta As Double, ByRef xprime As Double, ByRef yprime As Double)
    '    'figure the x or y coordinate value relative to a perfectly horizontal x axis and perfectly vertical y axis
    '    xprime = xcoord * Math.Cos(theta) + ycoord * Math.Sin(theta)
    '    yprime = (-1 * xcoord) * Math.Sin(theta) + ycoord * Math.Cos(theta)
    'End Sub
    Private Function DetermineRowCol(ByVal coordval As Double, ByVal minval As Double, ByVal cellsize As Double) As Integer
        DetermineRowCol = Math.Round((coordval - minval) / cellsize)
    End Function
    Private Function PointToLineDist(ByVal xcoord As Double, ByVal ycoord As Double, ByVal slope As Double, ByVal yintercept As Double) As Double
        Dim sqterm1 As Double
        Dim sqterm2 As Double

        sqterm1 = ((xcoord + (slope * ycoord) - (slope * yintercept)) / (slope ^ 2 + 1)) - xcoord
        sqterm1 = sqterm1 ^ 2

        sqterm2 = slope * ((xcoord + (slope * ycoord) - (slope * yintercept)) / (slope ^ 2 + 1)) + yintercept - ycoord
        sqterm2 = sqterm2 ^ 2

        PointToLineDist = Math.Sqrt(sqterm1 + sqterm2)
    End Function

    Private Function DetermineK(ByVal compval As Double, ByVal Values() As Double) As Integer
        DetermineK = -1 'default
        For j As Integer = 1 To Values.Length - 2
            If compval >= Values(j) And compval < Values(j + 1) Then
                DetermineK = j
                Exit Function
            End If
        Next

    End Function

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'File with field names to use
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select a file with field names to use for the .attributesall file"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        tbColumnDefFN.Text = openfiledialog2.FileName
        openfiledialog2 = Nothing
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        'Exported DB with SIMPPLLE fields including x/y coordinates of polygon centroids
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select exported DB with SIMPPLLE fields including x/y coordinates of polygon centroids"
            .Filter = "Comma-delimited files(.txt) |*.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        tbPolygonTableFN.Text = openfiledialog2.FileName
        openfiledialog2 = Nothing
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        'file with wind direction information
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select file with wind direction information"
            .Filter = "Wind Ninja Tab Delimited Outputs(.asc) |*.asc"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        tbWindDirectionFN.Text = openfiledialog2.FileName
        openfiledialog2 = Nothing
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        'file with wind speed information
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select file with wind speed information"
            .Filter = "Wind Ninja Tab Delimited Outputs(.asc) |*.asc"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        tbWindSpeedFN.Text = openfiledialog2.FileName
        openfiledialog2 = Nothing
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        MsgBox("Updates an existing spatialrelate file with wind direction and velocity information. Must specify these files in boxes above. If can't find wind direction or speed, uses default already in the spatialrelate file. Otherwise, uses the minimum speed specified above.")
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select the spatialrelate file to update"
            .Filter = "Spatialrelate files(.spatialrelate) |*.spatialrelate"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        tbSpatialrelateFN.Text = openfiledialog2.FileName
        openfiledialog2 = Nothing
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select the coordinates file with polygon centroid information"
            .Filter = "Coordinates files(.coord) |*.coord"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        tbCoordinatesFN.Text = openfiledialog2.FileName
        openfiledialog2 = Nothing
    End Sub


    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click

        Dim selectfrm As New frmSpatialRelationsFields
        selectfrm.Show()
        selectfrm = Nothing



    End Sub

    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        MsgBox("Random Vegetation Assignments - will look through exported pathway files and assign a random Species/Size/Density to a polygon based on its Habitat Type Group (PVT)")
    End Sub


    Private Sub FillCombosUsedRunOutput()

        Dim dummy As String
        Dim arr() As String
        Dim arr2() As String
        Dim khtg As Integer
        Dim kspec As Integer
        Dim ksize As Integer
        Dim kdensity As Integer
        Dim HTG As String
        Dim i As Integer
        Dim NFiles As Integer
        Dim FileName() As String
        Dim fnum As Integer
        Dim fnum2 As Integer
        Dim folder As String

        Dim cdfilename As String
        Dim MaxPathwaysInHTG As Integer
        Dim kpath As Integer

        'do a folderbrowser to get the folder with pathway files
        Dim folderdialog2 As New FolderBrowserDialog
        With folderdialog2
            .SelectedPath = foldername
            .Description = "Folder that has exported .txt pathway files (1 per EcoGroup)"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Exit Sub
        End With

        folder = folderdialog2.SelectedPath

        Dim fldr As New System.IO.DirectoryInfo(folder)
        Dim files As System.IO.FileInfo() = fldr.GetFiles '("*EVU_SIM_DATA*")
        Dim fi As System.IO.FileInfo


        'initiate these arrays
        NHTGLUTq = 0
        ReDim HTGNameLUTq(NHTGLUTq)
        NSizeClassLUTq = 0
        ReDim SizeClassNameLUTq(NSizeClassLUTq)
        SizeClassNameLUTq(0) = "" 'have to give it a value...
        NDensityLUTq = 0
        ReDim DensityNameLUTq(NDensityLUTq)
        'NHabGroupLUTq = 0
        'ReDim HabGroupNameLUTq(NHabGroupLUTq)
        NSpeciesLUTq = 0
        ReDim SpeciesNameLUTq(NSpeciesLUTq)


        NFiles = 0
        ReDim FileName(NFiles)
        For Each fi In files
            i = i + 1
            If fi.Extension = ".txt" Then
                NFiles = NFiles + 1
                ReDim Preserve FileName(NFiles)
                FileName(NFiles) = fi.FullName
            End If
        Next
        MaxPathwaysInHTG = 0
        NHTGpathways = NFiles
        ReDim NPathwaysInHTG(NHTGpathways)

        'first identify all possible species, size, and density values from the pathway files
        For jfile As Integer = 1 To NFiles
            fnum = FreeFile()
            FileOpen(fnum, FileName(jfile), OpenMode.Input)
            'header line contains the habitat type group
            dummy = LineInput(fnum)
            arr = Split(dummy, " ")
            HTG = Trim(arr(1))
            khtg = GetStrIndex(HTGNameLUTq, HTG)
            If khtg < 0 Then
                NHTGLUTq = NHTGLUTq + 1
                ReDim Preserve HTGNameLUTq(NHTGLUTq)
                HTGNameLUTq(NHTGLUTq) = HTG
            End If
            Do Until EOF(fnum)
                dummy = Trim(LineInput(fnum))
                If dummy.Length > 0 Then
                    arr = Split(dummy, " ")
                    If arr(0) = "VEGETATIVE-TYPE" Then
                        NPathwaysInHTG(jfile) = NPathwaysInHTG(jfile) + 1
                        MaxPathwaysInHTG = Math.Max(MaxPathwaysInHTG, NPathwaysInHTG(jfile))
                        arr2 = Split(arr(1), "/")
                        kspec = GetStrIndex(SpeciesNameLUTq, arr2(0))
                        If kspec < 0 Then
                            NSpeciesLUTq = NSpeciesLUTq + 1
                            ReDim Preserve SpeciesNameLUTq(NSpeciesLUTq)
                            SpeciesNameLUTq(NSpeciesLUTq) = arr2(0)
                        End If

                        ksize = GetSizeInd(SizeClassNameLUTq, arr2(1))
                        If ksize < 0 Then
                            NSizeClassLUTq = NSizeClassLUTq + 1
                            ReDim Preserve SizeClassNameLUTq(NSizeClassLUTq)
                            SizeClassNameLUTq(NSizeClassLUTq) = arr2(1)
                        End If

                        kdensity = GetStrIndex(DensityNameLUTq, arr2(2))
                        If kdensity < 0 Then
                            NDensityLUTq = NDensityLUTq + 1
                            ReDim Preserve DensityNameLUTq(NDensityLUTq)
                            DensityNameLUTq(NDensityLUTq) = arr2(2)
                        End If
                    End If
                End If
            Loop
            FileClose(fnum)
        Next

        'then, identify the pathway HTG/Species/Size/Density combinations that are in the model.

        'ReDim UseCombo(NHTGLUTq, NSpeciesLUTq, NSizeClassLUTq, NDensityLUTq)
        ReDim PathwayString(NHTGpathways, MaxPathwaysInHTG)
        For jfile As Integer = 1 To NFiles
            fnum = FreeFile()
            FileOpen(fnum, FileName(jfile), OpenMode.Input)
            'header line contains the habitat type group
            dummy = LineInput(fnum)
            arr = Split(dummy, " ")
            HTG = Trim(arr(1))
            khtg = GetStrIndex(HTGNameLUTq, HTG)
            kpath = 0
            Do Until EOF(fnum)
                dummy = Trim(LineInput(fnum))
                If dummy.Length > 0 Then
                    arr = Split(dummy, " ")
                    If arr(0) = "VEGETATIVE-TYPE" Then
                        kpath = kpath + 1
                        PathwayString(khtg, kpath) = Trim(arr(1))

                    End If
                End If
            Loop
            FileClose(fnum)
            'write outputs

        Next




    End Sub
    Private Function GetSizeInd(ByVal CompareArr() As String, ByVal strval As String) As Integer
        Dim secondcompareval As String
        GetSizeInd = -1
        'If strval = "SS" Then Stop
        For j As Integer = 0 To CompareArr.Length - 1
            If CompareArr(j).Length >= strval.Length - 2 And strval.Length >= CompareArr(j).Length And CompareArr(j).Length > 0 Then 'gets you through 99 size classes
                secondcompareval = strval.Substring(0, CompareArr(j).Length)
                If CompareArr(j) = secondcompareval Then
                    GetSizeInd = j
                    Exit Function
                End If
            End If
        Next
    End Function

    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click
        MsgBox("This processor reads a comma-delimited database to generate input files for the SIMPPLLE Model" & Chr(13) & Chr(13) _
        & "The rows in the database must represent similar-sized square polygons." & Chr(13) & Chr(13) _
        & "The columns in the database must contain relevant vegetation information (type/size/density, past treatment and disturbances) as well as Elevation (meters) and x/y coordinate of the centroid of the square." & Chr(13) & Chr(13) _
        & "Use the 'Define' button to help point to the correct columns for the required attributes." & Chr(13) _
        & "Alternately, you can read pathway files to assign random type/size/densities to each polygon. Habitat Type Group from the original dataset will be maintained.")

    End Sub

    Private Sub Button15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button15.Click
        MsgBox("If you choose to split Geographic Areas, you will be prompted for a column that contains the GA name. Each unique value in this column will produce a set of .attributesall/.spatialrelate input files.")
    End Sub

    Private Sub Button16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button16.Click
        MsgBox("Hex Adjacency: Will assume odd/even rows of squares are offset to reflect 6-way adjacency for each cell rather than 4- or 8-way adjacency of a regular grid of squares")
    End Sub

    Private Sub Button17_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        MsgBox("Limiting Back Burning means that fire will spread against the wind only for the side-adjacent cells. Applies only to 8-way and hex adjacency")
    End Sub

    Private Sub Button18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        MsgBox("Slope and Wind adjacency means that a pixel will be considered adjacent with a higher probability if it is downwind and/or uphill. Adjacency is assigned probabilistically.")
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Button19_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button19.Click
        MsgBox("If writing a Keane Fire Spread file, the default wind speed will be used to write the file. Otherwise, SIMPPLLE fire spread files ignore this value.")
    End Sub

    Private Sub Button21_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button21.Click
        MsgBox("Will convert .attributesall and .spatialrelate to a text-format input file and create a default Column Def File for the fields to use to re-process into .attributesall")
    End Sub

    Private Sub Button20_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button20.Click
        Dim convertfile As String
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select the attributesall file to convert"
            .Filter = "Attributesall files(.attributesall) |*.attributesall"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        convertfile = openfiledialog2.FileName
        openfiledialog2 = Nothing
        ConvertAttribuesAll(convertfile)
        MsgBox("Done")
    End Sub
    Private Sub ConvertAttribuesAll(ByVal convertfile As String)
        'Takes a .spatialrelate/attributesall and writes it to a standard table that can be used by other routines
        Dim convertspatfile As String
        Dim DataOF As String
        Dim ColumnDefFile As String
        Dim npolys As Integer
        Dim fnum As Integer
        Dim fnum2 As Integer
        Dim dummy As String
        Dim arr() As String
        Dim polyind() As Long
        Dim polyelev() As Double
        Dim largestpolyind As Long
        Dim bigM As Long = 800000
        Dim tind As Long

        convertspatfile = DirName(convertfile) & FName(convertfile) & ".spatialrelate"
        'test to see that both files are there...
        If System.IO.File.Exists(convertfile) = False Then
            MsgBox(convertfile & " does not exist. Try again")
            Exit Sub
        End If
        If System.IO.File.Exists(convertspatfile) = False Then
            MsgBox(convertspatfile & " does not exist. Try again")
            Exit Sub
        End If

        DataOF = DirName(convertfile) & FName(convertfile) & "_RAW.txt"
        ColumnDefFile = DirName(convertfile) & "FieldLUT_" & FName(convertfile) & ".txt"
        tbColumnDefFN.Text = ColumnDefFile
        tbPolygonTableFN.Text = DataOF

        'pdummy = SLINK & "," & Col & "," & Row & "," & Stand_ID & "," & Acres & "," & HTG & "," & D1 & "," & D2 & "," & Covertype & "," & SizeClass & "," & Density & "," & PastProcess & "," & PastProcess_T & "," & PastTreatment & "," & PastTreatment_T & "," & D3 & "," & D4 & "," & Ownership & "," & D5 & "," & D6 & "," & FMZ & "," & SpecialArea & "," & D7 & "," & D8 & "," & D9

        'first count the number of polygons in the .attributesall file
        fnum = FreeFile()
        FileOpen(fnum, convertfile, OpenMode.Input)
        dummy = LineInput(fnum)
        npolys = 0

        ReDim polyind(bigM)
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "END" Then Exit Do
            arr = Split(dummy, ",")
            npolys = npolys + 1
            If npolys > bigM Then
                bigM = bigM + 250000
                ReDim Preserve polyind(bigM)
            End If
            polyind(npolys) = arr(0)
            largestpolyind = Math.Max(largestpolyind, polyind(npolys))
        Loop
        FileClose(fnum)
        ReDim polyelev(largestpolyind)

        'now read spatialrelate for elevation
        fnum = FreeFile()
        FileOpen(fnum, convertspatfile, OpenMode.Input)
        dummy = LineInput(fnum)

        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "END" Then Exit Do
            arr = Split(dummy, ",")
            tind = arr(0)
            polyelev(tind) = arr(2)
        Loop
        FileClose(fnum)

        'now write everything out
        fnum = FreeFile()
        FileOpen(fnum, DataOF, OpenMode.Output)
        fnum2 = FreeFile()
        FileOpen(fnum2, convertfile, OpenMode.Input)
        PrintLine(fnum, "SLINK,Row,Col,Stand_ID,Acres,HTG,D1,D2,Covertype,SizeClass,Density,PastProcess,PastProcess_T,PastTreatment,PastTreatment_T,D3,D4,Ownership,D5,D6,FMZ,SpecialArea,D7,D8,D9,ELEV")
        dummy = LineInput(fnum2)
        Do Until EOF(fnum2)
            dummy = Trim(LineInput(fnum2))
            If dummy = "END" Then Exit Do
            arr = Split(dummy, ",")
            tind = arr(0)
            PrintLine(fnum, dummy & "," & polyelev(tind))
        Loop

        FileClose(fnum)
        FileClose(fnum2)

        fnum = FreeFile()
        FileOpen(fnum, ColumnDefFile, OpenMode.Output)
        PrintLine(fnum, "Fields to use for attributesall and spatialrelate")
        PrintLine(fnum, "Ecological stratification,HTG")
        PrintLine(fnum, "Area,Acres")
        PrintLine(fnum, "StandID, Stand_ID")
        PrintLine(fnum, "Ownership,Ownership")
        PrintLine(fnum, "Roads,Not Defined")
        PrintLine(fnum, "Elevation,ELEV")
        PrintLine(fnum, "Fire Management Zone,FMZ")
        PrintLine(fnum, "Special Area,SpecialArea")
        PrintLine(fnum, "Species,Covertype")
        PrintLine(fnum, "Size Class,SizeClass")
        PrintLine(fnum, "Density (Canopy Cover %),Density")
        PrintLine(fnum, "Past Process,PastProcess")
        PrintLine(fnum, "Past Treatment,PastTreatment")
        PrintLine(fnum, "Time Since Past Process (Decades),PastProcess_T")
        PrintLine(fnum, "Time Since Past Treatment (Decades),PastTreatment_T")
        PrintLine(fnum, "X Centroid,Col")
        PrintLine(fnum, "Y Centroid,Row")
        FileClose(fnum)

    End Sub

    Private Sub Button18_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button18.Click
        MsgBox("4x or 9x Generalization can be used to simplify the landscape for rapid calibration. 4x will combine cells into squares 4 times as large as the input cell, and 9x will combine them into squares 9 times as large as the input.")
    End Sub

    Private Sub Button23_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button23.Click
        MsgBox("This will take elevation information from a .spatialrelate file (and row/col info. from a .attributesall) and create a ascii DEM that can be used by WindNinja")
    End Sub

    Private Sub Button22_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button22.Click
        Dim convertfile As String
        Dim openfiledialog2 As New OpenFileDialog

        With openfiledialog2
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select the attributesall file to convert to DEM"
            .Filter = "Attributesall files(.attributesall) |*.attributesall"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        convertfile = openfiledialog2.FileName
        openfiledialog2 = Nothing
        ConvertToDEM(convertfile)
        MsgBox("Done")
    End Sub
    Private Sub ConvertToDEM(ByVal convertfile As String)
        'Takes a .spatialrelate/attributesall and writes it to a standard table that can be used by other routines
        Dim convertspatfile As String
        Dim DataOF As String
        'Dim ColumnDefFile As String
        Dim npolys As Integer
        Dim NRows As Integer = -1
        Dim NCols As Integer = -1
        Dim fnum As Integer
        Dim fnum2 As Integer
        Dim dummy As String
        Dim arr() As String
        Dim polyind() As Long
        Dim polyelev() As Double
        Dim largestpolyind As Long
        Dim bigM As Long = 800000
        Dim bigRow As Integer = 1000
        Dim bigCol As Integer = 1000
        Dim tind As Long
        Dim trow As Integer
        Dim tcol As Integer
        Dim bval As Integer 'first non-zero cell value encountered
        Dim eval As Integer 'last non-zero cell value encountered
        Dim beval As Integer 'whether you are using the beginning or ending value
        Dim PolyRow() As Integer
        Dim PolyCol() As Integer
        Dim krow As Integer
        Dim sb As StringBuilder
        Dim cellsize As Double
        Dim pval As String
        Dim novalnum As String = "-9999"

        Dim RowColElev(,) As Integer

        convertspatfile = DirName(convertfile) & FName(convertfile) & ".spatialrelate"
        'test to see that both files are there...
        If System.IO.File.Exists(convertfile) = False Then
            MsgBox(convertfile & " does not exist. Try again")
            Exit Sub
        End If
        If System.IO.File.Exists(convertspatfile) = False Then
            MsgBox(convertspatfile & " does not exist. Try again")
            Exit Sub
        End If

        DataOF = DirName(convertfile) & FName(convertfile) & "_DEM.asc"

        'first count the number of polygons in the .attributesall file
        fnum = FreeFile()
        FileOpen(fnum, convertfile, OpenMode.Input)
        dummy = LineInput(fnum)
        npolys = 0

        ReDim polyind(bigM), PolyRow(bigM), PolyCol(bigM)
        'ReDim RowColElev(bigRow, bigCol)
        cellsize = 0
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "END" Then Exit Do
            arr = Split(dummy, ",")
            npolys = npolys + 1
            If npolys > bigM Then
                bigM = bigM + 250000
                ReDim Preserve polyind(bigM)
            End If
            tind = arr(0)
            polyind(npolys) = tind
            largestpolyind = Math.Max(largestpolyind, polyind(npolys))
            trow = CType(arr(1), Integer)
            tcol = CType(arr(2), Integer)
            NRows = Math.Max(NRows, trow)
            NCols = Math.Max(NCols, tcol)
            PolyRow(tind) = trow
            PolyCol(tind) = tcol
            If cellsize = 0 Then
                cellsize = CType(arr(4), Double)
                cellsize = cellsize * 4046.856
                cellsize = Math.Sqrt(cellsize)
                cellsize = Math.Round(cellsize, 3)
            End If
        Loop
        FileClose(fnum)
        ReDim polyelev(largestpolyind), RowColElev(NRows, NCols)
        'set default
        For jrow As Integer = 1 To NRows
            For jcol As Integer = 1 To NCols
                RowColElev(jrow, jcol) = novalnum
            Next
        Next

        'now read spatialrelate for elevation
        fnum = FreeFile()
        FileOpen(fnum, convertspatfile, OpenMode.Input)
        dummy = LineInput(fnum)

        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy = "END" Then Exit Do
            arr = Split(dummy, ",")
            tind = arr(0)
            polyelev(tind) = arr(2)
            RowColElev(PolyRow(tind), PolyCol(tind)) = polyelev(tind)
        Loop
        FileClose(fnum)

        'fill missing row/col values - required by WindNinja
        For jrow As Integer = 1 To NRows
            bval = novalnum
            eval = novalnum
            For jcol As Integer = 1 To NCols
                If RowColElev(jrow, jcol) <> novalnum And bval = novalnum Then bval = RowColElev(jrow, jcol)
                If RowColElev(jrow, jcol) <> novalnum And bval <> novalnum And eval = novalnum Then
                    eval = RowColElev(jrow, jcol)
                    Exit For
                End If
            Next
            beval = 0 'use beginning value
            For jcol As Integer = 1 To NCols
                If RowColElev(jrow, jcol) = bval Then beval = 1
                If RowColElev(jrow, jcol) = novalnum And beval = 0 Then RowColElev(jrow, jcol) = bval
                If RowColElev(jrow, jcol) = novalnum And beval = 1 Then RowColElev(jrow, jcol) = eval
            Next
        Next

        'now write everything out
        fnum = FreeFile()
        FileOpen(fnum, DataOF, OpenMode.Output)
        PrintLine(fnum, "ncols " & NCols)
        PrintLine(fnum, "nrows " & NRows)
        PrintLine(fnum, "xllcorner 1")
        PrintLine(fnum, "yllcorner 1")
        PrintLine(fnum, "cellsize " & cellsize)
        PrintLine(fnum, "NODATA_value " & novalnum)
        dummy = ""
        For jrow As Integer = 1 To NRows
            krow = NRows + 1 - jrow

            sb = New StringBuilder("", NCols * 3)
            For jcol As Integer = 1 To NCols
                pval = RowColElev(jrow, jcol)
                'If pval = 0 Then pval = novalnum
                sb.Append(pval & " ")
            Next
            PrintLine(fnum, sb.ToString)
        Next

        FileClose(fnum)

        RowColElev = Nothing



    End Sub

    Private Sub Button24_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button24.Click
        MsgBox("You can manually type in a number that will override the 8 cardinal direction choices. Make sure the value is 1-360.")
    End Sub

    Private Sub cbbDefaultWindDirection_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbbDefaultWindDirection.SelectedIndexChanged
        If cbbDefaultWindDirection.Text = "N" Then tbWindDir.Text = 0
        If cbbDefaultWindDirection.Text = "NE" Then tbWindDir.Text = 45
        If cbbDefaultWindDirection.Text = "E" Then tbWindDir.Text = 90
        If cbbDefaultWindDirection.Text = "SE" Then tbWindDir.Text = 135
        If cbbDefaultWindDirection.Text = "S" Then tbWindDir.Text = 180
        If cbbDefaultWindDirection.Text = "SW" Then tbWindDir.Text = 225
        If cbbDefaultWindDirection.Text = "W" Then tbWindDir.Text = 270
        If cbbDefaultWindDirection.Text = "NW" Then tbWindDir.Text = 315
    End Sub

    Private Sub Button25_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button25.Click
        MsgBox("This will extend adjacency from the edge of the landscape to the other side. " & Chr(13) & "Spread will only be in same row/column, not diagonally." & Chr(13) & "Best results when the landscape is filled in - no gaps to spread across.")
    End Sub
End Class

