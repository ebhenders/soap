Imports System.text
Module modSpatialStats


    'Public Sub PatchStats(ByVal Aggoutfile As String, ByVal Patchoutfile As String, ByVal kspec As Integer, ByVal GroupDef As Integer)

    '    'subroutine goes through the latest solution and tallies patches by coversite in each time period

    '    'logic - go through each Influence zone (another way of measuring adjacency; already set up
    '    'search for the max stand ID associated with each influence zone/patch

    '    'ultimately trying to populate the following two sets:
    '    Dim PatchSize() As Double 'by period, coversite, NStands (max possible patches) - the acres of the patch
    '    Dim PatchEdge() As Double
    '    Dim PatchNStands() As Integer 'by period,coversite , NStands (max possible patches) - number of stands in hte patch
    '    Dim PatchType(,) As Integer 'not all patches are the same flavor - index by NSLINK,NTimesteps
    '    Dim NPatches(,) As Integer 'by tic, habitat type
    '    Dim TicTotalArea(,) As Double 'by tic, habitat type

    '    'reassign a stand to the max ID associated with each patch
    '    Dim StandMaxID(,) As Long 'by period, stand...
    '    Dim mxid As Integer

    '    'for evaluating adjacent stands and such
    '    Dim kaa As Integer
    '    Dim saa As Integer
    '    Dim kcol As Integer
    '    Dim krow As Integer
    '    Dim taa As Integer
    '    Dim kaareportdef As Integer
    '    Dim jaareportdef As Integer
    '    Dim awm As Double
    '    Dim AdjStandPotPatch(,) As Byte 'by naa, max adj stands - used to do an up front evaluation of whether the adjacent stand can be added to the patch

    '    'only have to loop through those stands that have the habitat to potentially be a patch
    '    Dim NPotPatchSLINK As Integer
    '    Dim PotPatchSLINK() As Integer 'the list of SLINK indices that can be part of a patch

    '    'for evaluating when the patches have been fully defined
    '    Dim matchper As Integer = 1
    '    Dim matchstand As Integer = 1

    '    Dim fnum2 As Integer
    '    Dim strb As StringBuilder
    '    Dim str As String



    '    ReDim NPatches(NTimestep, NWLSpeciesTypes(kspec))
    '    ReDim TicTotalArea(NTimestep, NWLSpeciesTypes(kspec))
    '    ReDim StandMaxID(NTimestep, NSLINK)

    '    'default values for max stand ID is itself
    '    For jaa As Integer = 1 To NSLINK
    '        For jtic As Integer = 0 To NTimestep
    '            StandMaxID(jtic, jaa) = jaa
    '        Next jtic
    '    Next

    '    Dim starttime As System.DateTime = System.DateTime.Now

    '    Dim fnum As Integer = FreeFile()
    '    FileOpen(fnum, Patchoutfile, OpenMode.Output)
    '    PrintLine(fnum, "Timestep, WLSpecies, WLHab, Patch_ID, NStands, Size, Edge, Size:Edge_Ratio")
    '    ReDim PatchType(NSLINK, NTimestep)
    '    For jtic As Integer = 0 To NTimestep
    '        'fnum2 = FreeFile()
    '        'FileOpen(fnum2, DirName(Patchoutfile) & FName(Patchoutfile) & "Tic_" & jtic & ".txt", OpenMode.Output)
    '        'PrintLine(fnum2, "SLINK,PatchInd")
    '        ReDim PatchSize(NSLINK), PatchEdge(NSLINK)
    '        ReDim PatchNStands(NSLINK)
    '        ReDim AdjStandPotPatch(NSLINK, MaxStandAdj)
    '        ReDim PotPatchSLINK(NSLINK)
    '        NPotPatchSLINK = 0

    '        'Fill arrays with adjacency information for this period
    '        For jaa As Integer = 1 To NSLINK
    '            If AAWLHabitat(jaa, kspec, jtic) = 1 Then
    '                NPotPatchSLINK += 1
    '                PotPatchSLINK(NPotPatchSLINK) = jaa
    '            End If
    '            For jjaa As Integer = 1 To AANStandAdj(jaa)
    '                kaa = AAStandAdj(jaa, jjaa)
    '                'If kaa <> jaa Then
    '                'check to see if these are the same wildlife habitat for this time period
    '                If GroupDef = 0 And AAWLHabitatStr(jaa, kspec, jtic) = AAWLHabitatStr(kaa, kspec, jtic) And AAWLHabitat(jaa, kspec, jtic) = 1 Then
    '                    AdjStandPotPatch(jaa, jjaa) = 1
    '                ElseIf GroupDef > 0 Then
    '                    'For jgroup As Integer = 1 To NReportGroups
    '                    jaareportdef = ReportDefinition(GroupDef, AAOwnership(AASLINK(jaa)), AASpecialArea(AASLINK(jaa)))
    '                    kaareportdef = ReportDefinition(GroupDef, AAOwnership(AASLINK(kaa)), AASpecialArea(AASLINK(kaa)))
    '                    If jaareportdef * kaareportdef > 0 And AAWLHabitatStr(jaa, kspec, jtic) = AAWLHabitatStr(kaa, kspec, jtic) And AAWLHabitat(jaa, kspec, jtic) = 1 Then
    '                        AdjStandPotPatch(jaa, jjaa) = 1
    '                    End If
    '                End If
    '            Next
    '        Next





    '        matchper = 1
    '        Do Until matchper = 0
    '            matchper = 0
    '            For paa As Integer = 1 To NPotPatchSLINK 'NSLINK 'AASetNAA(kaaset)
    '                'Do Until AASLINK(jaa) > 0 'control for blank aa information
    '                '    jaa = jaa + 1
    '                'Loop
    '                saa = PotPatchSLINK(paa)
    '                'If AAWLHabitat(jaa, kspec, jtic) = 1 Then 'if not wl hab, can't be part of a patch
    '                mxid = StandMaxID(jtic, saa)
    '                For jjaa As Integer = 1 To AANStandAdj(saa)
    '                    kaa = AAStandAdj(saa, jjaa)
    '                    'first collect the max id for all of the stand and all its neighbors
    '                    mxid = Math.Max(mxid, StandMaxID(jtic, kaa) * AdjStandPotPatch(saa, jjaa))
    '                Next jjaa

    '                If mxid <> StandMaxID(jtic, saa) Then
    '                    'now do it again to assign all stands to the max ID found
    '                    matchper = matchper + 1
    '                    StandMaxID(jtic, saa) = mxid
    '                    For jjaa As Integer = 1 To AANStandAdj(saa)
    '                        kaa = AAStandAdj(saa, jjaa)
    '                        StandMaxID(jtic, kaa) = mxid * AdjStandPotPatch(saa, jjaa)
    '                    Next
    '                End If
    '                'End If
    '            Next paa
    '        Loop





    '        'record the max patch stats for this period
    '        For jaa As Integer = 1 To NSLINK 'AASetNAA(kaaset)
    '            kaa = StandMaxID(jtic, jaa)
    '            PatchType(kaa, jtic) = AAWLHabitatStr(jaa, kspec, jtic) 'probably redundantly set over and over
    '            PatchType(jaa, jtic) = AAWLHabitatStr(jaa, kspec, jtic)
    '            If AAWLHabitat(jaa, kspec, jtic) = 1 And (ReportDefinition(GroupDef, AAOwnership(AASLINK(kaa)), AASpecialArea(AASLINK(kaa))) > 0 Or GroupDef = 0) Then
    '                PatchSize(kaa) = PatchSize(kaa) + SIMAAAcres
    '                PatchNStands(kaa) = PatchNStands(kaa) + 1
    '                PatchEdge(kaa) = PatchEdge(kaa) + Math.Sqrt(SIMAAAcres) * 4 'perimeter in terms of acres
    '            End If
    '        Next
    '        'now count the real edge of the patch
    '        For jaa As Integer = 1 To NSLINK
    '            kaa = StandMaxID(jtic, jaa)
    '            kcol = AACol(jaa)
    '            krow = AARow(jaa)
    '            'east
    '            taa = StandMaxID(jtic, ColRowSLINK(kcol, krow + 1))
    '            If kaa = taa Then PatchEdge(kaa) = PatchEdge(kaa) - Math.Sqrt(SIMAAAcres) 'only a one sided subtraction, since eventually you will get to the next one...
    '            'north
    '            taa = StandMaxID(jtic, ColRowSLINK(kcol + 1, krow))
    '            If kaa = taa Then PatchEdge(kaa) = PatchEdge(kaa) - Math.Sqrt(SIMAAAcres) 'only a one sided subtraction, since eventually you will get to the next one...
    '            'west
    '            taa = StandMaxID(jtic, ColRowSLINK(kcol, krow - 1))
    '            If kaa = taa Then PatchEdge(kaa) = PatchEdge(kaa) - Math.Sqrt(SIMAAAcres) 'only a one sided subtraction, since eventually you will get to the next one...
    '            'south
    '            taa = StandMaxID(jtic, ColRowSLINK(kcol - 1, krow))
    '            If kaa = taa Then PatchEdge(kaa) = PatchEdge(kaa) - Math.Sqrt(SIMAAAcres) 'only a one sided subtraction, since eventually you will get to the next one...
    '        Next

    '        For jaa As Integer = 1 To NSLINK
    '            'track the AA and patch size metrics
    '            kaa = StandMaxID(jtic, jaa)
    '            AAPatchIndex(jaa, kspec, jtic) = 0 'default
    '            If AAWLHabitat(jaa, kspec, jtic) = 1 Then AAPatchIndex(jaa, kspec, jtic) = StandMaxID(jtic, jaa)
    '            WLPatchSize(jaa, kspec, jtic) = 0 'default
    '            If PatchNStands(jaa) > 0 Then 'only the jaa that IS the MaxID will have more than 0 patches
    '                NPatches(jtic, PatchType(jaa, jtic)) = NPatches(jtic, PatchType(jaa, jtic)) + 1
    '                'AAPatchIndex(jaa, kspec, jtic) = NPatches(jtic) 'numeric count
    '                WLPatchSize(jaa, kspec, jtic) = PatchSize(jaa) 'WLPatchSize(NPatches(jtic), kspec, jtic) = PatchSize(jaa)
    '                TicTotalArea(jtic, PatchType(jaa, jtic)) = TicTotalArea(jtic, PatchType(jaa, jtic)) + PatchSize(jaa)

    '                'strb = New StringBuilder(jtic.ToString, 200)
    '                'strb.Append("," & WLSpeciesName(kspec) & "," & PatchType(jaa, jtic) & "," & kaa & "," & PatchNStands(jaa) & "," & Math.Round(PatchSize(jaa), 1) & "," & Math.Round(PatchEdge(jaa), 1) & "," & Math.Round(PatchSize(jaa) / PatchEdge(jaa), 2))
    '                'PrintLine(fnum, strb.ToString) 'jtic & "," & WLSpeciesName(kspec) & "," & PatchType(jaa, jtic) & "," & kaa & "," & PatchNStands(jaa) & "," & Math.Round(PatchSize(jaa), 1) & "," & Math.Round(PatchEdge(jaa), 1) & "," & Math.Round(PatchSize(jaa) / PatchEdge(jaa), 2))
    '                PrintLine(fnum, jtic & "," & WLSpeciesName(kspec) & "," & PatchType(jaa, jtic) & "," & kaa & "," & PatchNStands(jaa) & "," & Math.Round(PatchSize(jaa), 1) & "," & Math.Round(PatchEdge(jaa), 1) & "," & Math.Round(PatchSize(jaa) / PatchEdge(jaa), 2))

    '                'strb = Nothing
    '            End If
    '            'PrintLine(fnum2, AASLINK(jaa) & "," & AAPatchIndex(jaa, kspec, jtic))
    '        Next jaa
    '        'FileClose(fnum2)
    '    Next jtic
    '    FileClose(fnum)

    '    Dim stoptime As System.DateTime = System.DateTime.Now
    '    Dim tottime As System.TimeSpan = stoptime - starttime

    '    Stop

    '    'Dim fnum As Integer = FreeFile()
    '    FileOpen(fnum, Aggoutfile, OpenMode.Output)

    '    PrintLine(fnum, "Timestep, Query_Name, QueryValue, NPatches, Total_Area, Average_Size, AWM_Av_Size")
    '    For jtic As Integer = 0 To NTimestep
    '        For jwltype As Integer = 1 To NWLSpeciesTypes(kspec)
    '            If NPatches(jtic, jwltype) > 0 Then
    '                'find the area weighted mean for this patch type
    '                awm = 0
    '                For jaa As Integer = 1 To NSLINK
    '                    If PatchType(jaa, jtic) = jwltype Then awm = awm + (WLPatchSize(jaa, kspec, jtic) ^ 2) / TicTotalArea(jtic, jwltype)
    '                Next
    '                PrintLine(fnum, jtic & "," & WLSpeciesName(kspec) & "," & WLSpeciesTypeName(kspec, jwltype) & "," & NPatches(jtic, jwltype) & "," & _
    '                   Math.Round(TicTotalArea(jtic, jwltype), 1) & "," & Math.Round(TicTotalArea(jtic, jwltype) / NPatches(jtic, jwltype), 1) & "," & Math.Round(awm, 1))
    '            End If
    '        Next jwltype
    '    Next
    '    FileClose(fnum)

    '    'get rid of these large arrays
    '    ReDim PatchSize(0)
    '    ReDim PatchNStands(0)
    '    ReDim NPatches(0, 0)
    '    ReDim TicTotalArea(0,0)
    '    ReDim StandMaxID(0, 0)

    'End Sub

    Public Sub MovingWindowMetrics(ByVal kspec As Integer, ByVal ktic As Integer, ByVal HomeRangeSize As Single)
        'assume you have GridSLink by row/col
        'assume you have AAWLHab(jaa,jspec,jtic)

        'identify the "kernel" - list of x,y values to populate
        Dim w As Integer
        Dim x As Integer 'x * y width of the grid to search
        Dim xx As Integer
        Dim yy As Integer
        Dim kx As Integer
        Dim ky As Integer
        Dim kaa As Integer
        Dim Area As Double = 0
        Dim InGrid(,) As Byte 'figure out whether each square in the grid is in the circle or not
        w = 1
        'find how many squares wide you have to have to get the home range
        Do Until Area > HomeRangeSize
            w = w + 2
            Area = (Math.PI / 4) * w ^ 2 * SIMAAAcres
        Loop
        ReDim InGrid(w, w), WindowCellVal(BiggestCol, BiggestRow)
        'figure out which squares in the grid are in the circle

        For x = 1 To BiggestCol
            For y As Integer = 1 To BiggestRow
                kaa = GridSlink(x, y)
                kx = 0
                For xx = x - w To x + w
                    ky = 0
                    kx = kx + 1 'ingrid index
                    For yy = y - w To y + w
                        ky = ky + 1 'ingrid index
                        WindowCellVal(x, y) = WindowCellVal(xx, yy) + InGrid(kx, ky) * AAWLHabitat(kaa, kspec, ktic) * SIMAAAcres
                    Next yy
                Next xx
            Next
        Next

    End Sub

    Public Sub PatchStats2(ByVal Aggoutfile As String, ByVal Patchoutfile As String, ByVal kspec As Integer, ByVal kpatchspec As Integer, ByVal GroupDef As Integer, ByVal bPatchMaps As Boolean)

        'subroutine goes through the latest solution and tallies patches by coversite in each time period

        'logic - go through each Influence zone (another way of measuring adjacency; already set up
        'search for the max stand ID associated with each influence zone/patch

        Dim PatchOutFN As String = DirName(Patchoutfile) & FName(Patchoutfile) & "PATCH.txt"
        Dim strb As New StringBuilder
        Dim pstr As String

        'ultimately trying to populate the following two sets:
        Dim PatchSize() As Double 'by period, coversite, NStands (max possible patches) - the acres of the patch
        Dim PatchEdge() As Double
        Dim PatchNStands() As Integer 'by period,coversite , NStands (max possible patches) - number of stands in hte patch
        Dim PatchType(,) As Integer 'not all patches are the same flavor - index by NSLINK,NTimesteps
        Dim NPatches(,) As Integer 'by tic, habitat type
        Dim TicTotalArea(,) As Double 'by tic, habitat type

        'reassign a stand to the max ID associated with each patch
        Dim StandMaxID(,) As Long 'by period, stand...
        Dim mxid As Integer

        'for evaluating adjacent stands and such
        Dim kaa As Integer
        Dim saa As Integer
        Dim kcol As Integer
        Dim krow As Integer
        Dim taa As Integer
        Dim kaareportdef As Integer
        Dim jaareportdef As Integer
        Dim awm As Double
        Dim AdjStandPotPatch(,) As Integer 'by naa, max adj stands - used to store the adjacent stand ID of adjacent stands that can be potential patch
        Dim NAdjStandPotPatch() As Integer 'by naa

        'only have to loop through those stands that have the habitat to potentially be a patch
        Dim NPotPatchSLINK As Integer
        Dim PotPatchSLINK() As Integer 'the list of SLINK indices that can be part of a patch
        Dim PotPatchPrintSLINK() As Integer
        Dim nPotPatchPrintSLINK As Integer
        'Dim Switched() As Byte

        'for evaluating when the patches have been fully defined
        Dim matchper As Integer = 1
        Dim matchstand As Integer = 1


        ReDim NPatches(NTimestep, NWLSpeciesTypes(kspec))
        ReDim TicTotalArea(NTimestep, NWLSpeciesTypes(kspec))
        ReDim StandMaxID(NTimestep, NSLINK)

        'default values for max stand ID is itself
        For jaa As Integer = 1 To NSLINK
            For jtic As Integer = 0 To NTimestep
                StandMaxID(jtic, jaa) = jaa
            Next jtic
        Next


        '##################
        'Dim starttime As System.DateTime = System.DateTime.Now

        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, Patchoutfile, OpenMode.Output)
        PrintLine(fnum, "Timestep, WLSpecies, WLHab, Patch_ID, NStands, Size, Edge, Size:Edge_Ratio")
        ReDim PatchType(NSLINK, NTimestep)
        For jtic As Integer = 0 To NTimestep
            'fnum2 = FreeFile()
            'FileOpen(fnum2, DirName(Patchoutfile) & FName(Patchoutfile) & "Tic_" & jtic & ".txt", OpenMode.Output)
            'PrintLine(fnum2, "SLINK,PatchInd")
            ReDim PatchSize(NSLINK), PatchEdge(NSLINK)
            ReDim PatchNStands(NSLINK)
            ReDim AdjStandPotPatch(NSLINK, MaxStandAdj), NAdjStandPotPatch(NSLINK)
            ReDim PotPatchSLINK(NSLINK), PotPatchPrintSLINK(NSLINK)
            NPotPatchSLINK = 0
            nPotPatchPrintSLINK = 0

            'Fill arrays with adjacency information for this period
            For jaa As Integer = 1 To NSLINK
                'If jaa = 7082 Then Stop
                For jjaa As Integer = 1 To AANStandAdj(jaa)
                    kaa = AAStandAdj(jaa, jjaa)
                    'If kaa <> jaa Then
                    'check to see if these are the same wildlife habitat for this time period
                    If GroupDef = 0 And AAWLHabitatStr(jaa, kspec, jtic) = AAWLHabitatStr(kaa, kspec, jtic) And AAWLHabitat(jaa, kspec, jtic) = 1 Then
                        NAdjStandPotPatch(jaa) += 1
                        AdjStandPotPatch(jaa, NAdjStandPotPatch(jaa)) = kaa
                    ElseIf GroupDef > 0 Then
                        'For jgroup As Integer = 1 To NReportGroups
                        jaareportdef = ReportDefinition(GroupDef, AAOwnership(AASLINK(jaa)), AASpecialArea(AASLINK(jaa)))
                        kaareportdef = ReportDefinition(GroupDef, AAOwnership(AASLINK(kaa)), AASpecialArea(AASLINK(kaa)))
                        If jaareportdef * kaareportdef > 0 And AAWLHabitatStr(jaa, kspec, jtic) = AAWLHabitatStr(kaa, kspec, jtic) And AAWLHabitat(jaa, kspec, jtic) = 1 Then
                            NAdjStandPotPatch(jaa) += 1
                            AdjStandPotPatch(jaa, NAdjStandPotPatch(jaa)) = kaa
                        End If
                    End If
                Next

                If AAWLHabitat(jaa, kspec, jtic) = 1 And NAdjStandPotPatch(jaa) > 0 Then 'don't need to evaluate single patch stands in next round
                    NPotPatchSLINK += 1
                    PotPatchSLINK(NPotPatchSLINK) = jaa
                    nPotPatchPrintSLINK += 1
                    PotPatchPrintSLINK(nPotPatchPrintSLINK) = jaa
                ElseIf AAWLHabitat(jaa, kspec, jtic) = 1 Then
                    nPotPatchPrintSLINK += 1
                    PotPatchPrintSLINK(nPotPatchPrintSLINK) = jaa
                ElseIf AAWLHabitat(jaa, kspec, jtic) = 0 Then
                    StandMaxID(jtic, jaa) = 0
                End If
            Next


            matchper = 1
            Do Until matchper = 0
                matchper = 0
                'ReDim Switched(NSLINK)
                For paa As Integer = 1 To NPotPatchSLINK 'NSLINK 'AASetNAA(kaaset)
                    saa = PotPatchSLINK(paa)
                    'If Switched(saa) = 0 Then
                    'If AAWLHabitat(jaa, kspec, jtic) = 1 Then 'if not wl hab, can't be part of a patch
                    mxid = StandMaxID(jtic, saa)
                    For jjaa As Integer = 1 To NAdjStandPotPatch(saa) 'AANStandAdj(saa)
                        kaa = AdjStandPotPatch(saa, jjaa) 'AAStandAdj(saa, jjaa)
                        'first collect the max id for all of the stand and all its neighbors
                        mxid = Math.Max(mxid, StandMaxID(jtic, kaa)) ' * AdjStandPotPatch(saa, jjaa))
                    Next jjaa

                    If mxid <> StandMaxID(jtic, saa) Then
                        'now do it again to assign all stands to the max ID found
                        matchper = matchper + 1
                        StandMaxID(jtic, saa) = mxid
                        'Switched(saa) = 1
                        For jjaa As Integer = 1 To NAdjStandPotPatch(saa) 'AANStandAdj(saa)
                            kaa = AdjStandPotPatch(saa, jjaa) 'AAStandAdj(saa, jjaa)
                            StandMaxID(jtic, kaa) = mxid '* AdjStandPotPatch(saa, jjaa)
                            'Switched(kaa) = 1
                        Next
                    End If
                    'End If
                Next paa
            Loop


            'record the max patch stats for this period
            For jaa As Integer = 1 To nPotPatchPrintSLINK 'NSLINK 'AASetNAA(kaaset)
                'If jaa = 7082 Then Stop
                saa = PotPatchPrintSLINK(jaa)
                kaa = StandMaxID(jtic, saa)
                PatchType(kaa, jtic) = AAWLHabitatStr(saa, kspec, jtic) 'probably redundantly set over and over
                PatchType(saa, jtic) = AAWLHabitatStr(saa, kspec, jtic)
                If AAWLHabitat(saa, kspec, jtic) = 1 And (ReportDefinition(GroupDef, AAOwnership(AASLINK(kaa)), AASpecialArea(AASLINK(kaa))) > 0 Or GroupDef = 0) Then
                    PatchSize(kaa) = PatchSize(kaa) + SIMAAAcres
                    PatchNStands(kaa) = PatchNStands(kaa) + 1
                    PatchEdge(kaa) = PatchEdge(kaa) + Math.Sqrt(SIMAAAcres) * 4 'perimeter in terms of acres
                End If
            Next
            'now count the real edge of the patch
            For jaa As Integer = 1 To nPotPatchPrintSLINK 'NSLINK
                saa = PotPatchPrintSLINK(jaa)
                kaa = StandMaxID(jtic, saa)
                kcol = AACol(saa)
                krow = AARow(saa)
                'east
                taa = StandMaxID(jtic, ColRowSLINK(kcol, krow + 1))
                If kaa = taa Then PatchEdge(kaa) = PatchEdge(kaa) - Math.Sqrt(SIMAAAcres) 'only a one sided subtraction, since eventually you will get to the next one...
                'north
                taa = StandMaxID(jtic, ColRowSLINK(kcol + 1, krow))
                If kaa = taa Then PatchEdge(kaa) = PatchEdge(kaa) - Math.Sqrt(SIMAAAcres) 'only a one sided subtraction, since eventually you will get to the next one...
                'west
                taa = StandMaxID(jtic, ColRowSLINK(kcol, krow - 1))
                If kaa = taa Then PatchEdge(kaa) = PatchEdge(kaa) - Math.Sqrt(SIMAAAcres) 'only a one sided subtraction, since eventually you will get to the next one...
                'south
                taa = StandMaxID(jtic, ColRowSLINK(kcol - 1, krow))
                If kaa = taa Then PatchEdge(kaa) = PatchEdge(kaa) - Math.Sqrt(SIMAAAcres) 'only a one sided subtraction, since eventually you will get to the next one...
            Next

            For jaa As Integer = 1 To nPotPatchPrintSLINK 'NSLINK
                'track the AA and patch size metrics
                saa = PotPatchPrintSLINK(jaa)
                kaa = StandMaxID(jtic, saa)
                AAPatchIndex(saa, kpatchspec, jtic) = 0 'default
                If AAWLHabitat(saa, kspec, jtic) = 1 Then AAPatchIndex(saa, kpatchspec, jtic) = StandMaxID(jtic, saa)
                WLPatchSize(saa, kpatchspec, jtic) = 0 'default
                If PatchNStands(saa) > 0 Then 'only the jaa that IS the MaxID will have more than 0 patches
                    NPatches(jtic, PatchType(saa, jtic)) = NPatches(jtic, PatchType(saa, jtic)) + 1
                    'AAPatchIndex(jaa, kspec, jtic) = NPatches(jtic) 'numeric count
                    WLPatchSize(saa, kpatchspec, jtic) = PatchSize(saa) 'WLPatchSize(NPatches(jtic), kspec, jtic) = PatchSize(jaa)
                    TicTotalArea(jtic, PatchType(saa, jtic)) = TicTotalArea(jtic, PatchType(saa, jtic)) + PatchSize(saa)

                    'strb = New StringBuilder(jtic.ToString, 200)
                    'strb.Append("," & WLSpeciesName(kspec) & "," & PatchType(jaa, jtic) & "," & kaa & "," & PatchNStands(jaa) & "," & Math.Round(PatchSize(jaa), 1) & "," & Math.Round(PatchEdge(jaa), 1) & "," & Math.Round(PatchSize(jaa) / PatchEdge(jaa), 2))
                    'PrintLine(fnum, strb.ToString) 'jtic & "," & WLSpeciesName(kspec) & "," & PatchType(jaa, jtic) & "," & kaa & "," & PatchNStands(jaa) & "," & Math.Round(PatchSize(jaa), 1) & "," & Math.Round(PatchEdge(jaa), 1) & "," & Math.Round(PatchSize(jaa) / PatchEdge(jaa), 2))
                    PrintLine(fnum, jtic & "," & WLSpeciesName(kspec) & "," & PatchType(saa, jtic) & "," & kaa & "," & PatchNStands(saa) & "," & Math.Round(PatchSize(saa), 1) & "," & Math.Round(PatchEdge(saa), 1) & "," & Math.Round(PatchSize(saa) / PatchEdge(saa), 2))

                    'strb = Nothing
                End If
                'PrintLine(fnum2, AASLINK(jaa) & "," & AAPatchIndex(jaa, kspec, jtic))
            Next
            'FileClose(fnum2)
        Next jtic
        FileClose(fnum)


        ''###################
        'Dim stoptime As System.DateTime = System.DateTime.Now
        'Dim tottime As System.TimeSpan = stoptime - starttime

        'Stop

        fnum = FreeFile()
        FileOpen(fnum, Aggoutfile, OpenMode.Output)

        PrintLine(fnum, "Timestep, Query_Name, QueryValue, NPatches, Total_Area, Average_Size, AWM_Av_Size")
        For jtic As Integer = 0 To NTimestep
            For jwltype As Integer = 1 To NWLSpeciesTypes(kspec)
                If NPatches(jtic, jwltype) > 0 Then
                    'find the area weighted mean for this patch type
                    awm = 0
                    For jaa As Integer = 1 To NSLINK
                        If PatchType(jaa, jtic) = jwltype Then awm = awm + (WLPatchSize(jaa, kpatchspec, jtic) ^ 2) / TicTotalArea(jtic, jwltype)
                    Next
                    PrintLine(fnum, jtic & "," & WLSpeciesName(kspec) & "," & WLSpeciesTypeName(kspec, jwltype) & "," & NPatches(jtic, jwltype) & "," & _
                       Math.Round(TicTotalArea(jtic, jwltype), 1) & "," & Math.Round(TicTotalArea(jtic, jwltype) / NPatches(jtic, jwltype), 1) & "," & Math.Round(awm, 1))
                End If
            Next jwltype
        Next
        FileClose(fnum)

        If bPatchMaps Then
            fnum = FreeFile()
            FileOpen(fnum, PatchOutFN, OpenMode.Output)
            strb = New StringBuilder("SLINK", 500)
            For jtic As Integer = 0 To NTimestep
                strb.Append("," & WLSpeciesName(kspec) & "_" & jtic)
            Next
            PrintLine(fnum, strb.ToString)
            strb = Nothing
            For jslink As Integer = 1 To NSLINK
                strb = New StringBuilder(jslink.ToString, 500)

                For jtic As Integer = 0 To NTimestep
                    kaa = StandMaxID(jtic, jslink)
                    'If PatchType(kaa, jtic) = AAWLHabitatStr(kaa, kspec, jtic) Then
                    strb.Append("," & StandMaxID(jtic, jslink))
                    'Else
                    'strb.Append(",0")
                    'End If
                Next
                PrintLine(fnum, strb.ToString)
                strb = Nothing
            Next

            FileClose(fnum)
        End If

        'get rid of these large arrays
        ReDim PatchSize(0)
        ReDim PatchNStands(0)
        ReDim NPatches(0, 0)
        ReDim TicTotalArea(0, 0)
        ReDim StandMaxID(0, 0)

    End Sub
End Module
